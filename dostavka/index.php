<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "доставка деликатесов, продукты на дом, качественная еда, вкусные продукты");
$APPLICATION->SetPageProperty("keywords_inner", "доставка деликатесов, продукты на дом, качественная еда, вкусные продукты");
$APPLICATION->SetPageProperty("title", "Доставка деликатесных продуктов на дом  | Интернет магазин Шеф Гурме");
$APPLICATION->SetPageProperty("keywords", "доставка деликатесов, продукты на дом, качественная еда, вкусные продукты");
$APPLICATION->SetPageProperty("description", "Описание условий доставки, развернутое описание опций доставки");
$APPLICATION->SetTitle("Доставка");

$cityList = array();
$cityList[0] = 'Санкт-Петербург';
$cityList[1] = 'Ленинградская область';
$cityList[2] = 'Москва';
$cityList[3] = 'Московская область';
$cityList[4] = 'Прочие регионы';
?><link rel="stylesheet" href="css/delivery-page-style.css">

<div class="container delivery-page">
	<h1 class="delivery-page-title">Доставка</h1>
	<h2 class="delivery-page-h2">Варианты доставки</h2>
	<p class="delivery-page-white-box-text">Обратите внимание, что в нашем магазине устанавливается минимальная сумма заказа, которая также зависит от вашего местоположения.</p>
	<div class="delivery-page-white-box-text2">
	<p class="abzac">Жителям Петербурга и области, а также Москвы и области доступны все варианты доставки.</p>
	<p>Однако мы хотим сделать наш сервис максимально удобным для всех клиентов, поэтому для других регионов тоже предлагаем выбор способа доставки.</p>
	</div>
	<!-- ТАБЛИЦА -->
	<table class="table table-deliveries">
	<tbody>
	<tr class="table-row">
		<td class="table-left-city">
			<?foreach($cityList as $id => $value):?>
				<a href="#" class="city-link" data-id="<?=$id;?>"><?=$value;?></a>
			<?endforeach;?>
		</td>
		
		<td class="city-deliveries">
		</td>
	</tr>
	</tbody>
	</table>
	<!-- -->
	
	<!-- АККОРДЕОН -->
	<div class="ac-container">
		<input type="radio" name="accordion-1" id="ac-1"/>
		<label for="ac-1" class="link"><?= $cityList[0]?></label>
		<div id="info_city_0">
			<p class='delivery-id0'>Доставка курьером «Яeda» по указанному адресу</p>
			<p class='delivery-id1'>Самовывоз из удобной вам точки самовывоза</p>
			<p class='delivery-id2'>Отправление почтовой службой EMS-экспресс</p>
			<p class='delivery-id3'>Отправление транспортной компанией</p>
		</div>
		<input type="radio" name="accordion-1" id="ac-2"/>
		<label for="ac-2"><?= $cityList[1]?></label>
		<div id="info_city_1">
			<p class='delivery-id0'>Доставка курьером «Яeda» по указанному адресу</p>
			<p class='delivery-id2'>Отправление почтовой службой EMS-экспресс</p>
			<p class='delivery-id3'>Отправление транспортной компанией</p>
		</div>
		<input type="radio" name="accordion-1" id="ac-3"/>
		<label for="ac-3"><?= $cityList[2]?></label>
		<div id="info_city_2">
		<p class='delivery-id0'>Доставка курьером «Яeda» по указанному адресу</p>
			<p class='delivery-id1'>Самовывоз из удобной вам точки самовывоза</p>
			<p class='delivery-id2'>Отправление почтовой службой EMS-экспресс</p>
			<p class='delivery-id3'>Отправление транспортной компанией</p>
		</div>
		<input type="radio" name="accordion-1" id="ac-4"/>
		<label for="ac-4"><?= $cityList[3]?></label>
		<div id="info_city_3">
			<p class='delivery-id0'>Доставка курьером «Яeda» по указанному адресу</p>
			<p class='delivery-id2'>Отправление почтовой службой EMS-экспресс</p>
			<p class='delivery-id3'>Отправление транспортной компанией</p>
		</div>
		<input type="radio" name="accordion-1" id="ac-5"/>
		<label for="ac-5"><?= $cityList[4]?></label>
		<div id="info_city_4">
			<p class='delivery-id2'>Отправление почтовой службой EMS-экспресс</p>
			<p class='delivery-id3'>Отправление транспортной компанией</p>
		</div>
	</div>
	<!-- -->
	
	<p>Если вы территориально находитесь в Московской, Ленинградской области или других регионах, при выборе доставки самовывозом вам потребуется приехать в один из этих городов.</p>
	<h2 class="delivery-page-h2">Стоимость доставки</h2>
	
	<p>
		 Расценки на доставку отличаются для регионов, а также для разных способов доставки.
	</p>
	<div class="table-for-full-screen">
	<h3 class="type" id="type0" style="margin-bottom:0px;">Курьерская доставка</h3>
	<!-- ШАПКА ТАБЛИЦЫ -->
	<table class="table table-deliveries2">
	<tbody>
	<tr class="row-one">
		<th style="width: 26%;">
			 Город\регион
		</th>
		<th style="width: 59%;">
			 Стоимость заказа
		</th>
		<th style="width: 15%;">
			 Стоимость доставки
		</th>
	</tr>
	</tbody>
	</table>
	<!-- спб 1 -->
	<table class="table table-deliveries3" style="margin-bottom:0px;">
		<tbody>
			<tr>
				<td class="cityname">Санкт-Петербург</td>
				<td class="value">Минимальная сумма заказа<b> от 3 000 руб.</b></td>
				<td class="price">300 руб.</td>
			</tr>
		</tbody>
	</table>
	<!-- спб 2 -->
	<table class="table table-deliveries3" style="margin-top:-20px; margin-bottom:0px;">
		<tbody>
			<tr>
				<td class="empty-cell"> </td>
				<td class="value">Сумма заказа для бесплатной доставки<b> от 5 000 руб.</b></td>
				<td class="price">Бесплатно</td>
			</tr>
		</tbody>
	</table>
	<!-- ло 1 -->
	<table class="table table-deliveries3" style="margin-bottom:0px;">
		<tbody>
			<tr>
				<td  class="cityname">Ленинградская область</td>
				<td class="value">Минимальная сумма заказа<b> от 5 000 руб.</b></td>
				<td class="price">700 руб.</td>
			</tr>
		</tbody>
	</table>
	<!-- ло 2 -->
	<table class="table table-deliveries3" style="margin-top:-20px; margin-bottom:0px;">
		<tbody>
			<tr>
				<td class="empty-cell"> </td>
				<td class="value">Сумма заказа для бесплатной доставки<b> от 10 000 руб.</b></td>
				<td class="price">Бесплатно</td>
			</tr>
		</tbody>
	</table>
	<!-- мск 1 -->
	<table class="table table-deliveries3" style="margin-bottom:0px;">
		<tbody>
			<tr>
				<td  class="cityname">Москва</td>
				<td class="value">Минимальная сумма заказа<b> от 3 000 руб.</b></td>
				<td class="price">300 руб.</td>
			</tr>
		</tbody>
	</table>
	<!-- мск 2 -->
	<table class="table table-deliveries3" style="margin-top:-20px; margin-bottom:0px;">
		<tbody>
			<tr>
				<td class="empty-cell"> </td>
				<td class="value">Сумма заказа для бесплатной доставки<b> от 5 000 руб.</b></td>
				<td class="price">Бесплатно</td>
			</tr>
		</tbody>
	</table>
	<!-- мск об 1 -->
	<table class="table table-deliveries3" style="margin-bottom:0px;">
		<tbody>
			<tr>
				<td class="cityname">Московская область</td>
				<td class="value">Минимальная сумма заказа<b> от 7 000 руб.</b></td>
				<td class="price">900 руб.</td>
			</tr>
		</tbody>
	</table>
	<!-- мск об 2 -->
	<table class="table table-deliveries3" style="margin-top:-20px; margin-bottom:10px;">
		<tbody>
			<tr>
				<td class="empty-cell"> </td>
				<td class="value">Сумма заказа для бесплатной доставки<b> от 10 000 руб.</b></td>
				<td class="price">Бесплатно</td>
			</tr>
		</tbody>
	</table>

	<p><b>Обратите внимание</b>, что купив товары на определенную сумму, вы можете получить услугу курьерской доставки бесплатно.</p>
	<h3 class="type" id="type1">Самовывоз</h3>
	<p class="box-white">
		 Если вы решили забрать товар самовывозом, то за доставку вы не платите. Привоз товара в точку самовывоза осуществляется нами бесплатно.
	</p>
	<h3 class="type" id="type3">Доставка почтовой службой или транспортной компанией</h3>
	<p style="margin-bottom: 25px;">
		 В этих случаях стоимость определяется в зависимости от веса вашего заказа и удаленности региона. Рассчитывается при оформлении заказа индивидуально в каждом случае.
	</p>
</div>
<!-- -->
<!-------------------------------------------------------------------------------------------------------------------------->
<div class="table-for-small-screen">
	<table class="table table-deliveries-small" cellpadding="5px">
	<tbody>
	<tr class="small-row">
		<th><a href="#" class="delivery-method" data-index="1">Самовывоз</a></th>
		<th><a href="#" class="delivery-method" data-index="2">Курьерская доставка</th>
		<th><a href="#" class="delivery-method" data-index="3">Почтовой службой/ТК</th>
	</tr>
	</tbody>
	</table>
	<table class="table table-deliveries-small" style="margin-bottom:0px;">
	<tbody>
	<tr>
		<td class="delivery-method-result"></td>
	</tr>
	</tbody>
	</table>
</div>
<!-------------------------------------------------------------------------------------------------------------------------->
	<div class="contacts">
		<p>С вопросами, пожеланиями и претензиями по поводу доставки обращайтесь к нам по следующим каналам связи:</p>

		<table style="margin:15px 0px;width:100%;">
			<tbody>
				<tr>
					<td id="phone1" align="center" style="width:45%"><p class="phone1">Служба доставки:<b> 8 (812) 607 38 37</b> (многоканальный).</p></td>
					<td id="email1" align="center" style="width:35%"><p class="email1">Электронная почта: <a href="mailto:info.yaeda@gmail.com" style="color:#402f2b"><b>info.yaeda@gmail.com</b></a></p></td>
					<td id="skype1" align="center" style="width:20%"><p class="skype1" >Skype:<b>live:info.yaeda</b></p></td>
				</tr>
			</tbody>
		</table>
		<p>Менеджер Службы доставки с радостью ответит на все ваши вопросы и даст вам необходимую информацию.</p>
	</div>
</div>
 <br>
 
 
<script>
BX.ready(function(){
	$('.city-link').click(function(){
	var deliv=$("#info_city_"+$(this).data("id")).html();
	$('.city-deliveries').html(deliv); 
	//console.log(deliv);
		/*$.ajax({
		  method: "POST",
		  url: "/ajax/get-city-deliveries.php",
		  data: { "id":$(this).data("id")  },
		  success: function(result){
			$('.city-deliveries').html(result); 
		  },
		  error: function(err){
			console.log(err);    
		  }
		});*/

		return false;
	});
	
	$('.city-link:eq(0)').trigger('click');
	$('.city-link:eq(0)').addClass('active-first');
	
	
	$('.city-link').click(function(){
	$('.city-link:eq(0)').removeClass('active-first');
	});
	
	$('.delivery-method').click(function(){
		$.ajax({
		  method: "POST",
		  url: "/ajax/get-delivery-method.php",
		  data: { "index":$(this).data("index")  },
		  success: function(result){
			$('.delivery-method-result').html(result); 
		  },
		  error: function(err){
			console.log(err);    
		  }
		});

		return false;
	});
	
	$('.delivery-method:eq(1)').trigger('click');
	
	$('.delivery-method:eq(1)').addClass('active-first2');
	$('.delivery-method:eq(1)').parent().addClass('active-first3');
	
	
	$('.delivery-method').click(function(){
	$('.delivery-method:eq(1)').removeClass('active-first2');
	$('.delivery-method:eq(1)').parent().removeClass('active-first3');
	});

});

</script> 
 
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
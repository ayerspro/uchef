<div class="row">
	<div class="col-md-5">
		<p class="h3">
 <a href="tel:88007757353">8-800-775-73-53</a> <a href="tel:88126073837">8-812-607-38-37</a>
		</p>
		<p>
 <a href="mailto:info@uchef.ru" class="h3">info.yaeda@gmail.com</a>
		</p>
		<p class="hidden-lg hidden-md">
 <a href="https://telegram.me/chefgourmet" target="_blank"><span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fab fa-telegram-plane fa-stack-1x fa-inverse"></i> </span></a> <a href="viber://chat?number=79816880284" target="_blank"><span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fab fa-viber fa-stack-1x fa-inverse"></i> </span></a> <a href="whatsapp://send?text=&phone=+79816880284" target="_blank"><span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fab fa-whatsapp fa-stack-1x fa-inverse"></i> </span></a>
		</p>
	</div>
	<div class="col-md-7">
		<p class="h3">
			 Санкт-Петербург,<br>
			 Коломяжский проспект, д. 10.
		</p>
		<p class="h3">
			 &nbsp;
		</p>
	</div>
	<div class="col-md-5">
 <br>
		<p>
			 Есть вопрос? Позвоните, или напишите нам письмо либо воспользуйтесь <a data-modal-wrap=".callback-form" href="#"><u>формой обратной связи</u></a>.
		</p>
	</div>
	<div class="col-md-7 soc-block">
 <br>
		<p>
 <a href="https://www.youtube.com/channel/UC7e2LPj_Q9nIB1XLh36dhkw/featured" target="_blank"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fab fa-youtube fa-stack-1x fa-inverse"></i> </span> </a> <a href="https://www.facebook.com/yaeda.russia" target="_blank"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i> </span> </a> <a href="https://plus.google.com/u/0/108211437729199343715" target="_blank"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fab fa-google-plus-g fa-stack-1x fa-inverse"></i> </span> </a> <a href="https://vk.com/yaeda_russia" target="_blank"> <span class="fa-stack fa-2x"> <i class="fab fa-vk fa-stack-2x"></i> </span> </a> <a href="https://twitter.com/yaeda_russia" target="_blank"> <span class="fa-stack fa-2x"> <i class="fab fa-twitter fa-stack-2x"></i> </span> </a>
		</p>
	</div>
</div>
 <br>
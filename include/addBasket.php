<?if($_SERVER['REQUEST_METHOD'] == 'POST') {
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

	if (CModule::IncludeModule("catalog") && CModule::IncludeModule("iblock") && CModule::IncludeModule("sale"))
	{
		$arBasketItems = array();
		$arr = array('ITEM' => array(), 'SEND' => 'NO');
		$quantity = 0;

		/*$res = CCatalogSKU::getOffersList(array($_REQUEST['itemID']), 0, array(), array(), array());
        $offers = "";
        foreach ($res as $key) {
        	foreach ($key as $value) {
        		$offers = $value['ID'];
        		break;
        	}
        }
	
		$dbBasketItems = CSaleBasket::GetList(
		        array(
		                "NAME" => "ASC",
		                "ID" => "ASC"
		            ),
		        array(
		                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
		                "LID" => SITE_ID,
		                "ORDER_ID" => "NULL"
		            ),
		        false,
		        false,
		        array()
		    );
		while ($arItems = $dbBasketItems->Fetch())
		{
		    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
		    {
		        CSaleBasket::UpdatePrice($arItems["ID"], 
		                                 $arItems["CALLBACK_FUNC"], 
		                                 $arItems["MODULE"], 
		                                 $arItems["PRODUCT_ID"], 
		                                 $arItems["QUANTITY"]);
		        $arItems = CSaleBasket::GetByID($arItems["ID"]);
		    }
	
		    $arBasketItems[$arItems['PRODUCT_ID']] = $arItems;
		}

		if(isset($arBasketItems[($offers)?$offers:$_REQUEST['itemID']])) {
			$quantity = $arBasketItems[($offers)?$offers:$_REQUEST['itemID']]['QUANTITY'];
		}
		
		if(empty($quantity)){
			$quantity = 0;
		}

        if($quantity < $_REQUEST['quantity']) {
        	$_REQUEST['quantity'] = $_REQUEST['quantity'] - $quantity;
			
        	if(Add2BasketByProductID((int)$_REQUEST['itemID'], (float)$_REQUEST['quantity'], array(), array())) {
	    		$el = CIBlockElement::GetByID($_REQUEST['itemID'])->GetNext();
	    		$arr['ITEM'] = $el;
	    		$arr['SEND'] = 'YES';
	    	} elseif(!empty($offers) && Add2BasketByProductID((int)$offers, (float)$_REQUEST['quantity'], array(), array())) {
	    			$el = CIBlockElement::GetByID($_REQUEST['itemID'])->GetNext();
	    			$arr['ITEM'] = $el;
	    			$arr['SEND'] = 'YES';
	    	} else {
	    		$ex = $APPLICATION->GetException();
				$arr['ERROR'] = $ex->GetString();
	    	}
        } elseif($quantity > $_REQUEST['quantity']) {
        	if(CSaleBasket::Delete($arBasketItems[($offers)?$offers:$_REQUEST['itemID']]['ID'])) {

        		if(Add2BasketByProductID((int)$_REQUEST['itemID'], (float)$_REQUEST['quantity'], array(), array())) {
	    			$el = CIBlockElement::GetByID($_REQUEST['itemID'])->GetNext();
	    			$arr['ITEM'] = $el;
	    			$arr['SEND'] = 'YES';
	    		} elseif(!empty($offers) && Add2BasketByProductID((int)$offers, (float)$_REQUEST['quantity'], array(), array())) {
	    			$el = CIBlockElement::GetByID($_REQUEST['itemID'])->GetNext();
	    			$arr['ITEM'] = $el;
	    			$arr['SEND'] = 'YES';
	    		} else {
        			$ex = $APPLICATION->GetException();
					$arr['ERROR'] = $ex->GetString();
        		}
        	}
        } else {
        	$ex = $APPLICATION->GetException();
			$arr['ERROR'] = $ex->GetString();
        }*/
		
		
		$dbBasketItems = CSaleBasket::GetList(
			array(
					"NAME" => "ASC",
					"ID" => "ASC"
				),
			array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(),
					"LID" => SITE_ID,
					"ORDER_ID" => "NULL"
				),
			false,
			false,
			array()
		);
			
		while ($arItems = $dbBasketItems->Fetch())
		{
		    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
		    {
		        CSaleBasket::UpdatePrice($arItems["ID"], 
		                                 $arItems["CALLBACK_FUNC"], 
		                                 $arItems["MODULE"], 
		                                 $arItems["PRODUCT_ID"], 
		                                 $arItems["QUANTITY"]);
		        $arItems = CSaleBasket::GetByID($arItems["ID"]);
		    }
	
		    $arBasketItems[$arItems['PRODUCT_ID']] = $arItems;
		}
		
		$quantity = $_REQUEST['quantity'];
		
		if(isset($arBasketItems[$_REQUEST['itemID']])){
			if($quantity == 0 && CSaleBasket::Delete($arBasketItems[$_REQUEST['itemID']]['ID'])){
				$el = CIBlockElement::GetByID($_REQUEST['itemID'])->GetNext();
				$arr['ITEM'] = $el;
				$arr['SEND'] = 'YES';
			}
			else if(CSaleBasket::Update($arBasketItems[$_REQUEST['itemID']]['ID'], array('QUANTITY' => $quantity))){
				$el = CIBlockElement::GetByID($_REQUEST['itemID'])->GetNext();
				$arr['ITEM'] = $el;
				$arr['SEND'] = 'YES';
			}
			else{
				$ex = $APPLICATION->GetException();
				$arr['ERROR'] = $ex->GetString();
			}
		}
		else{
			if(Add2BasketByProductID((int)$_REQUEST['itemID'], (float)$quantity, array(), array())) {
				$el = CIBlockElement::GetByID($_REQUEST['itemID'])->GetNext();
				$arr['ITEM'] = $el;
				$arr['SEND'] = 'YES';
			}
			else{
				$ex = $APPLICATION->GetException();
				$arr['ERROR'] = $ex->GetString();
			}
		}
		
		
	    echo json_encode($arr);
	}
}
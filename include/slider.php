<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
$APPLICATION->IncludeComponent(
	"kvadrate:kvadrate.cmslider", 
	".default", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "7",
		"CM_SETTINGS_AUTO" => "1",
		"CM_SETTINGS_TIME" => "5000",
		"ELEMENT_SORT_FIELD" => "",
		"ELEMENT_SORT_ORDER" => "",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
if(!CModule::IncludeModule("search")){die();}
if(!CModule::IncludeModule("iblock")){die();}
if(!CModule::IncludeModule("catalog")){die();}


$IBLOCK_ID = 5; 
$arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID); 

$obSearch = new CSearch;
$obSearch->Search(array(//при желании, фильтр можете еще сузить, см.документацию
   'QUERY' => $_REQUEST['term'],
   'SITE_ID' => SITE_ID,
   'MODULE_ID' => 'iblock',
   'PARAM2' => array(5)
),
array(),
array('LIMIT' => 50000)
);   
$count=0;
while ($row = $obSearch->fetch()) { 
	//echo'<pre>';print_r($row);echo'</pre>';
	if($count<50000){
		$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_PICTURE", "DETAIL_PICTURE", "DETAIL_PAGE_URL");
		
		$arFilter = Array("IBLOCK_ID"=>5, "ID"=>$row['ITEM_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement())
		{
			$isTP = false;
			
			$arFields = $ob->GetFields();
			$arrItems[$count]=$arFields;
			if(!empty($arFields['PREVIEW_PICTURE'])){
				
				$file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL, true);
				$arrItems[$count]['PICTURE'] = $file['src'];
				
			}elseif(!empty($arFields['DETAIL_PICTURE'])){
				
				$file = CFile::ResizeImageGet($arFields['DETAIL_PICTURE'], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL, true);
				$arrItems[$count]['PICTURE'] = $file['src'];
			}
			
			$arPrice = CCatalogProduct::GetOptimalPrice($arFields['ID'], 1, $USER->GetUserGroupArray(), 'N', SITE_ID);
			
			if(!$arPrice && $arInfo) {
				
				$rsOffers = CIBlockElement::GetList(array(),array('IBLOCK_ID' => $arInfo['IBLOCK_ID'], 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $arFields['ID'])); 
				while ($arOffer = $rsOffers->GetNext()) {
					
					$isTP = true;
					
					if(!$arPrice) {
						
						$arPrice = CCatalogProduct::GetOptimalPrice($arOffer['ID'], 1, $USER->GetUserGroupArray(), 'N', SITE_ID);
						continue;
					}
					
					$arPriceTmp = CCatalogProduct::GetOptimalPrice($arOffer['ID'], 1, $USER->GetUserGroupArray(), 'N', SITE_ID);
					
					if($arPriceTmp['DISCOUNT_PRICE'] > 0 && $arPriceTmp['DISCOUNT_PRICE'] < $arPrice['DISCOUNT_PRICE']) {
						
						$arPrice = $arPriceTmp;
					}
				}
			}
			
			$arPrice['IS_TP'] = $isTP;
			
			$arrItems[$count]["PRICES"] = $arPrice;
			
			
			//print_r($arPrice);
/*
			$dbPrice = CPrice::GetList(
				array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", 
					"SORT" => "ASC"),
				array("PRODUCT_ID" => $arFields['ID']),
				false,
				false,
				array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", 
					"QUANTITY_FROM", "QUANTITY_TO")
			);
			while ($arPrice = $dbPrice->Fetch())
			{
				$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
					$arPrice["ID"],
					$USER->GetUserGroupArray(),
					"N",
					SITE_ID
				);
				$discountPrice = CCatalogProduct::CountPriceWithDiscount(
					$arPrice["PRICE"],
					$arPrice["CURRENCY"],
					$arDiscounts
				);
			//echo'<pre>';print_r($discountPrice);echo'</pre>';
				$arPrice["DISCOUNT_PRICE"] = $discountPrice;
				$arrItems[$count]["PRICES"]=$arPrice;
				unset($arPrice);
			}*/

		}

		$count++;
	}else{
		break;
	}
}

$arItemsID = array();

foreach($arrItems as $item) {
	
	$arItemsID[] = $item['ID'];
}

$arItemsID = getProductsCurrentRegion(array('IBLOCK_ID' => $IBLOCK_ID, "ID" => $arItemsID));

foreach($arrItems as $k => $item) {
	
	if(!in_array($item['ID'], $arItemsID)) {
		
		unset($arrItems[$k]);
	}
}

//echo'<pre>';print_r($arrItems);echo'</pre>';
if(count($arrItems)>0){
?>
<div class="bx_searche">
	<div class="bx_item_block all_result" style="text-align: center;">
		<div>
			<span class="all_result_title"><a href="/catalog/?q=<?echo$_REQUEST['term']?>">Все результаты (<?echo count($arrItems);?>)</a></span>
		</div>
		<div style="clear:both;"></div>
	</div>
	<? foreach ($arrItems as $key => $arItem) {?>
		<div class="bx-basket-item-list-items">
			<?if (!empty($arItem["PICTURE"])):?>
			<div class="bx-basket-item-list-item-img">
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
					<img src="<?echo $arItem["PICTURE"]?>" alt="<?=$arItem['NAME']?>">
				</a>
			</div>
			<?endif;?>
			<div class="bx-basket-item-list-item-name">
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
				<div class="bx-basket-item-list-item-price-block">
				<?
		//print_r($arItem['PRICES']["DISCOUNT_PRICE"]);
		/*echo'<pre>';print_r($arItem['PRICES']["DISCOUNT_PRICE"]);echo'</pre>';
echo'<pre>';print_r($arItem['PRICES']);echo'</pre>';*/
						if(round($arItem['PRICES']["DISCOUNT_PRICE"]) < round($arItem['PRICES']["PRICE"]['PRICE'])):?>
							<div class="bx-basket-item-list-item-old-price">
								<? if($arItem['PRICES']['IS_TP']) { ?>от <? } ?><?=$arItem['PRICES']["PRICE"]['PRICE']?> <i class="fa fa-ruble-sign"></i>
							</div>
							<div class="bx-basket-item-list-item-price">
								<? if($arItem['PRICES']['IS_TP']) { ?>от <? } ?><?=numberFormat($arItem['PRICES']["DISCOUNT_PRICE"], 2, '.', ' ')?> <i class="fa fa-ruble-sign"></i>
							</div>
							<?else:?>
								<div class="bx-basket-item-list-item-price"><? if($arItem['PRICES']['IS_TP']) { ?>от <? } ?><?=$arItem['PRICES']["PRICE"]['PRICE']?> <i class="fa fa-ruble-sign"></i></div>
							<?endif;
					
					?>
				</div>
			</div>
				<div style="clear:both;"></div>
			</div>
	<?} ?>
</div>
<?}?>
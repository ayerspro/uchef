<?
if($_SERVER['REQUEST_METHOD'] == 'POST'):
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
	$arResult = array();
	if(isset($_POST['USER_EMAIL'])) {
		$email = trim($_POST['USER_EMAIL']);
		$password = $_POST['USER_PASSWORD'];

		if(!empty($email)) {
			$rsUsers = CUser::GetList($by="", $order="", array('=EMAIL' => $email));
			if($resUsers = $rsUsers->Fetch()) {
				global $USER;

				$arResult['parameter'] = $resUsers;
		
				$arUser["PASSWORD"] = $resUsers['PASSWORD'];
	 			$arParams["PASSWORD"] = $password;
			
	 			if(strlen($arUser["PASSWORD"]) > 32)
	 			{
	 			    $salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
	 			    $db_password = substr($arUser["PASSWORD"], -32);
	 			}
	 			else
	 			{
	 			    $salt = "";
	 			    $db_password = $arUser["PASSWORD"];
	 			}
			
	 			$user_password =  md5($salt.$password);

				if($db_password == $user_password) {
					$USER->Authorize($resUsers['ID']);
					$arResult['SUCCESS'] = "YES";
	 			} else {
	 				$arResult['ERROR'][] = 'Неверный пароль';
	 			}
	 		} else {
	 			$arResult['ERROR'][] = 'Неверный или некорректный email или пароль';
	 		}
	 		if(empty($password)) {
	 			$arResult['ERROR'][] = 'Не введен пароль';
	 		}
	 	} else {
	 		$arResult['ERROR'][] = 'Не введен email';
	 	}
	 	echo json_encode($arResult);
	}

	if(isset($_POST['FORGET_PASS'])) {
		$data = trim($_POST['USER_DATA']);

		if(empty($data))
			$arResult['ERROR'][] = 'Не заполнены данные';
		elseif(strlen($data) < 3)
			$arResult['ERROR'][] = 'Слишком короткие данные';

		if(!isset($arResult['ERROR'])) {
			$arResult['data'] = $data;
			
			$arrFilter = array(
				"LOGIC" => "OR",
				"EMAIL" => $data,
				"LOGIN" => $data
			 ); 
			
			$dbUser = CUser::GetList($by="", $order="", $arrFilter, array());
			
	
			if($arUser = $dbUser->Fetch()) {
				$arFields = array(
					'USER_ID' => $arUser['ID'],
					'LOGIN' => $arUser['LOGIN'],
					'EMAIL' => $arUser['EMAIL'],
					'EMAIL_TO' => $arUser['EMAIL'],
					'CHECKWORD' => md5($arUser['ID'].$arUser['EMAIL'])
				);
				

				if(CEvent::Send('USER_PASS_REQUEST', SITE_ID, $arFields)) $arResult['SUCCESS'] = 'ok';
	 		}
	 	}
		echo json_encode($arResult);
	}
endif;
?>
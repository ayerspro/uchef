<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
	
$arResult = array();

$couponsIBlock = 15;
global $USER;
$itemID = intval($_REQUEST['coupon']);

if($USER->IsAuthorized()) {
	
	$res = CIBlockElement::GetProperty($couponsIBlock, $itemID, "sort", "asc", array("CODE" => "COUPON"));
	
	if($ob = $res->GetNext()) {
		
		$coupon = $ob['VALUE'];
		
		$dbCoupon = \Bitrix\Sale\Internals\DiscountCouponTable::getList(array(
		
			'filter' => array('ID' => $coupon),
		)); 
			
		if($arCoupon = $dbCoupon->Fetch()) { 
			
			$arResult['success'] = true;
			$arResult['coupon'] = $arCoupon['COUPON'];
		}
	}
	
} else {
	
	$arResult['error'] = 'authorize';
}

echo json_encode($arResult);
die();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>
<?

use Bitrix\Sale\Location\LocationTable;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

	global $USER;
	if(!$USER->IsAuthorized()) return;

	$arResult = array(
		"ERROR" => array(),
		"SUCCESS" => false,
		"SET_CITY" => false
	);
	
	$request = array(
		"town" => $_REQUEST['town'],
		"location" => $_REQUEST['location'],
		"street" => $_REQUEST['street'],
		"num_home" => $_REQUEST['num_home'],
		"flat" => $_REQUEST['flat'],
		"phone" => $_REQUEST['phone'],
		"fio" => $_REQUEST['fio'],
		"txt" => $_REQUEST['txt'],
		"number_form" => $_REQUEST['number_form'],
		"favorite" => (isset($_REQUEST['favorite']) && !empty($_REQUEST['favorite']))?$_REQUEST['favorite']:''
	);
	
	
	/*if(empty($request['town']) && empty($request['location']))
		$arResult['ERROR'][] = 'Поле город обязательное для заполнения';
	elseif(strlen($request['town']) < 3 && empty($request['location'])) {
		$arResult['ERROR'][] = 'Поле город слишком короткое';
	}*/
	
	if(empty($request['location'])){
		$arResult['ERROR'][] = 'Поле местоположения обязательное для заполнения';
	}
	else{
		if(CModule::IncludeModule("sale")){
			$dbLocation = LocationTable::getById($request['location'], [
					'select' => [
						'ID',
						'CODE',
						'NAME.NAME',
						'PARENT.NAME.NAME',
					],
					'filter' => [
						'=NAME.LANGUAGE_ID' => LANGUAGE_ID,
						'=PARENT.NAME.LANGUAGE_ID' => LANGUAGE_ID,
					]
				]);
			 
				
			if($arLocation = $dbLocation->Fetch()){
				
				if(empty($arLocation["CITY_ID"])){
					$arResult["SET_CITY"] = true;
				}
			}	
		}
	}
	
	
	if($arResult["SET_CITY"]){
		if(empty($request['town'])){
			$arResult['ERROR'][] = 'Поле город обязательное для заполнения';
		}
		else if(strlen($request['town']) < 3) {
			$arResult['ERROR'][] = 'Поле город слишком короткое';
		}
	}
	else if(!$arResult["SET_CITY"]){
		$request['town'] = "";
	}
	
	if(empty($request['street'])){
		$arResult['ERROR'][] = 'Поле улица обязательное для заполнения';
	}else {
		if(strlen($request['street']) < 3) {
			$arResult['ERROR'][] = 'Поле улица слишком короткое';
		}
		if(empty($request['num_home']) && empty($request['flat']))
			$arResult['ERROR'][] = 'Нужно указать данные номера дома, или квартиры';
	}

	if(empty($request['phone'])){
		$arResult['ERROR'][] = 'Поле телефон обязательное для заполнения';
	}
	else if(strlen($request['phone']) < 10 && !is_int($request['phone'])) {
		$arResult['ERROR'][] = 'Поле телефон некорректное';
	}

	/*if(empty($request['fio'])){ 
		$arResult['ERROR'][] = 'Поле ФИО получателя обязательное для заполнения';
	}
	else if (strlen($request['fio']) < 3){
		$arResult['ERROR'][] = 'Слишком мало данных о ФИО получателя';
	}*/

	if (!empty($request['txt']) && (strlen($request['txt']) < 10)){
		$arResult['ERROR'][] = 'Обращение должно содержать не меньше 10 символов';
	}

	if(empty($arResult['ERROR'])){
		$ID = $USER->GetID();

		$rsUser = CUser::GetByID($ID);
		$arUser = $rsUser->Fetch();

		$objForms = false;
		$objProfilePros = false;
		
		
		if(!empty($arUser['UF_COUNT_FORM'])) {
			$objForms = json_decode($arUser['UF_COUNT_FORM'], true);
			if(isset($objForms[$_REQUEST['number_form']])){
				$objForms[$_REQUEST['number_form']] = $request;
			} 
			elseif(!isset($objForms[$_REQUEST['number_form']])) {
				$count = count($objForms) + 1;

				$request['number_form'] = 'form_'.$count;
				$objForms['form_'.$count] = $request;
			}
		}

		if(!$objForms) $objForms = array('form_1' => $request);
		
		
	
		
		/*
		if(!empty($arUser['UF_SALE_PROFILE'])) {
			$objProfilePros = json_decode($arUser['UF_SALE_PROFILE'], true);
		}

		//$arResult['ERROR'][] = $objForms;	
		
		
		if(!empty($arUser['UF_SALE_PROFILE'])) {
			$objProfilePros = json_decode($arUser['UF_SALE_PROFILE'], true);
		}

		//$arResult['ERROR'][] = $objForms;	
		
		if($objProfilePros && CModule::IncludeModule("sale")){
			foreach($objForms as $key => $form){

				$arrFormID = explode("_", $key);
				$formID = 0;
				$profileID = 0;
				$fieldValues = array();
				
				if(count($arrFormID) == 2){
					$formID = $arrFormID[1];
				}
				else{
					continue;
				}
				
				
				$db_sales = CSaleOrderUserProps::GetList(
								array("DATE_UPDATE" => "DESC"),
								array("USER_ID" => $ID, "NAME" => "Адрес доставки " . $formID, "PERSON_TYPE_ID" => $objProfilePros["PROFILE_TYPE"])
							);
				
				if($ar_sales = $db_sales->Fetch()){
					$profileID  = $ar_sales['ID'];
				}
				else{
					$profileID  = CSaleOrderUserProps::Add(array("NAME" => "Адрес доставки " . $formID, "USER_ID" => $ID, 'PERSON_TYPE_ID' => $objProfilePros["PROFILE_TYPE"]));
				}
				
				
				$orderPropertiesList = CSaleOrderProps::GetList(
					array("SORT" => "ASC", "NAME" => "ASC"),
					array(
						"PERSON_TYPE_ID" => $objProfilePros["PROFILE_TYPE"],
						"USER_PROPS" => "Y", "ACTIVE" => "Y", "UTIL" => "N"
					),
					false,
					false,
					array("ID", "NAME", "TYPE", "REQUIED", "MULTIPLE", "IS_LOCATION", "PROPS_GROUP_ID", "IS_EMAIL", "IS_PROFILE_NAME", "IS_PAYER", "IS_LOCATION4TAX", "CODE", "SORT")
				);

				
				while($orderProperty = $orderPropertiesList->GetNext()){
					$currentValue = "";

					if($objProfilePros["PROFILE_PROP_" . $orderProperty["ID"]]){
						$currentValue = $objProfilePros["PROFILE_PROP_" . $orderProperty["ID"]];
					}
					else{
						switch($orderProperty['CODE']){
							case "LOCATION":
								$currentValue = $form["location"];
								break;
							case "CITY":
								$currentValue = $form["town"];
								break;
							case "STREET":
								$currentValue = $form["street"];
								break;
							case "HOUSE":
								$currentValue = $form["num_home"];
								break;	
							case "APARTMENT":
								$currentValue = $form["flat"];
								break;
							case "PHONE":
								$currentValue = $form["phone"];
								break;
						}
					}
					
					
					if(checkProperty($orderProperty, $currentValue)){
						
						
						$fieldValues[$orderProperty["ID"]] = array(
							"USER_PROPS_ID" => $profileID,
							"ORDER_PROPS_ID" => $orderProperty["ID"],
							"NAME" => $orderProperty["NAME"],
							'MULTIPLE' => $orderProperty["MULTIPLE"]
						);

						if ($orderProperty['TYPE'] == "MULTISELECT")
						{
							$fieldValues[$orderProperty["ID"]]['VALUE'] = implode(',',$currentValue);
						}
						else
						{
							$fieldValues[$orderProperty["ID"]]['VALUE'] = $currentValue;
						}
						
					}

				}
				
				$updatedValues = array();
				$userPropertiesList = CSaleOrderUserPropsValue::GetList(
					array("SORT" => "ASC"),
					array("USER_PROPS_ID" => $profileID),
					false,
					false,
					array("ID", "ORDER_PROPS_ID", "VALUE", "SORT", "PROP_TYPE")
				);
				

				while ($propertyValues = $userPropertiesList->Fetch())
				{
					if (isset($fieldValues[$propertyValues["ORDER_PROPS_ID"]]['VALUE']))
					{
						CSaleOrderUserPropsValue::Update(
							$propertyValues["ID"],
							array("VALUE" => $fieldValues[$propertyValues["ORDER_PROPS_ID"]]['VALUE'])
						);
					}

					$updatedValues[$propertyValues["ORDER_PROPS_ID"]] = $fieldValues[$propertyValues["ORDER_PROPS_ID"]];
				}

				if ($newValues = array_diff_key($fieldValues, $updatedValues))
				{
					foreach ($newValues as $value)
					{
						unset($value['MULTIPLE']);
						CSaleOrderUserPropsValue::Add($value);
					}
				}
				
			}
		}
		*/

		$obUser = new CUser;
		$arFields = array(
			"UF_COUNT_FORM" => json_encode($objForms)
		);
	
		if(!$obUser->Update($ID, $arFields)){
			$arResult['ERROR'][] = $obUser->LAST_ERROR;
		}
		else{
			$arResult['SUCCESS'] = 'send';
		} 

	}
	echo json_encode($arResult);
}



function checkProperty($property, $currentValue)
{
		if ($property["TYPE"] == "LOCATION" && $property["IS_LOCATION"] == "Y")
		{
			if ((int)($currentValue) <= 0)
				return false;
		}
		elseif ($property["IS_PROFILE_NAME"] == "Y")
		{
			if (strlen(trim($currentValue)) <= 0)
				return false;
		}
		elseif ($property["IS_PAYER"] == "Y")
		{
			if (strlen(trim($currentValue)) <= 0)
				return false;
		}
		elseif ($property["IS_EMAIL"] == "Y")
		{
			if (strlen(trim($currentValue)) <= 0 || !check_email(trim($currentValue)))
				return false;
		}
		elseif ($property["REQUIED"] == "Y")
		{
			if ($property["TYPE"] == "LOCATION")
			{
				if ((int)($currentValue) <= 0)
					return false;
			}
			elseif ($property["TYPE"] == "MULTISELECT")
			{
				if (!is_array($currentValue) || count($currentValue) <= 0)
					return false;
			}
			else
			{
				if (strlen($currentValue) <= 0)
					return false;
			}
		}

		return true;
}


?>





<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

$module_id = "reaspekt.geobase";

if (!CModule::IncludeModule($module_id)) {
    ShowError("Error! Module no install");
	return;
}

$request = Application::getInstance()->getContext()->getRequest();

$strCityName = htmlspecialchars(trim($request->getPost("city_name")));

if(strlen($strCityName) >= 2):
    $arCity = ReaspGeoIP::SelectQueryCity($strCityName);
    
    //ќпределилс¤ город по умолчанию
    $arData = ReaspGeoIP::GetAddr();
    $arResult = array(
        'FIND' => 'N',
        'DATA' => array()
    );

	if(!empty($arCity)):
        $cell = 1;
		
		foreach($arCity as $valCity):
            if($cell > 5) break;
            if($arData["CITY"] == $valCity["CITY"]):
                $arResult['FIND'] = 'Y';
                $arResult['DATA'][] = $arData;
            else:
                $arResult['DATA'][] = $valCity;
            endif;
            $cell++;
        endforeach;
	else:
	endif;
    echo json_encode($arResult);
endif;
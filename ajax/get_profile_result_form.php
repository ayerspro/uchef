<?
if($_SERVER['REQUEST_METHOD'] == 'POST'){
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
	
	
	$formNum = htmlspecialchars($_REQUEST['num_deliv_form']);
	
?>
	<form>
		
		<input type="hidden" name="number_form" value="form_<?=$formNum?>">
		<input type="hidden" name="profile_id" value="">
		<div class="panel-o col-xs-12"></div>
		<div class="make-o active"><i class="fa fa-check" aria-hidden="true"></i> Сделать основным адресом</div>
		<div class="num-deliv-form"><?=$formNum?></div>
		<div class="cansel-deliv-form"><i class="fa fa-ban" aria-hidden="true"></i></div>
		<div class="form-group width-w">
			<div class="col-sm-12">
				<!--<input type="text" name="town" placeholder="Город" value="" />-->
				<?
				if(CModule::IncludeModule("sale")){
					$locationTemplate = ($arParams['USE_AJAX_LOCATIONS'] !== 'Y') ? "popup" : "";
					
					$locationValue = intval($currentValue) ? $currentValue : $property["DEFAULT_VALUE"];
					CSaleLocation::proxySaleAjaxLocationsComponent(
						array(
							"AJAX_CALL" => "Y",
							'CITY_OUT_LOCATION' => 'Y',
							'CITY_INPUT_NAME' => 'location',
						),
						array(
						),
						$locationTemplate,
						true,
						'location-block-wrapper'
					);

				}?>
			</div>
		</div>
		<div class="form-group width-w">
			<div class="col-sm-12">
				<input type="text" name="town" placeholder="Город" value="<?=$form->town?>" <?if(empty($form->town)):?>style="display:none;"<?endif;?> />
			</div>
		</div>
		<div class="form-group width-w">
			<div class="col-sm-6 col-p-l">
				<input type="text" name="street" placeholder="Улица" value="" />
			</div>
			<div class="col-sm-6 col-p-r">
				<input type="text" name="num_home" placeholder="Номер дома" value="" />
			</div>
		</div>
		<div class="form-group width-w">
			<div class="col-sm-12">
				<input type="text" name="flat" placeholder="Квартира" value="" />
			</div>
		</div>
		<div class="form-group width-w">
			<div class="col-sm-12">
				<input type="text" name="phone" placeholder="Телефон получателя заказа" value="" />
			</div>
		</div>
		<div class="form-group width-w">
			<div class="col-sm-12">
				<input type="text" name="fio" placeholder="ФИО получателя" value="" />
			</div>
		</div>
		<div class="form-group width-w">
			<div class="col-sm-12">
				<textarea name="txt" placeholder="Получатель - другой человек? К дому сложно подъехать? Пишите эти и другие важные примечания в этой форме"></textarea>
			</div>
		</div>
		<div class="suc"></div>
	</form>
			
<?} ?>
<?
if($_SERVER['REQUEST_METHOD'] == 'POST'):
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
		global $USER;
	CModule::IncludeModule('iblock'); CModule::IncludeModule('sale'); CModule::IncludeModule('catalog');

        $arResult = array();
        $arID = array();

        $countDay = 1;
    
        $dbBasketItems = CSaleBasket::GetList(
             array(
                        "NAME" => "ASC",
                        "ID" => "ASC"
                     ),
             array(
                        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                        "LID" => SITE_ID,
                        "ORDER_ID" => "NULL"
                     ),
             false,
             false,
             array("PRODUCT_ID"));

        while ($arItems = $dbBasketItems->Fetch())
        {
             if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"])
             {
                  CSaleBasket::UpdatePrice($arItems["ID"],
                                         $arItems["CALLBACK_FUNC"],
                                         $arItems["MODULE"],
                                         $arItems["PRODUCT_ID"],
                                         $arItems["QUANTITY"],
                                         "N",
                                         $arItems["PRODUCT_PROVIDER_CLASS"]
                                         );
                  $arID[] = $arItems["PRODUCT_ID"];
             }
        }
    $res = CIBlockElement::GetList(Array("property_TIME_OF_ORDER"=>"DESC", "SORT"=>"DESC"), Array('ID' => $arID, '!PROPERTY_TIME_OF_ORDER' => false), false, false, Array('PROPERTY_TIME_OF_ORDER'));
    if($result = $res->GetNext()) {
        if($result['PROPERTY_TIME_OF_ORDER_VALUE'] > 1) $countDay = $result['PROPERTY_TIME_OF_ORDER_VALUE'];
    }
    $arResult['COUNT_DAY'] = $countDay;
	echo json_encode($arResult);
endif;
?>
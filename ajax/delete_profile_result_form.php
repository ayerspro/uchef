<?
if($_SERVER['REQUEST_METHOD'] == 'POST'):
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
		global $USER;
		if(!$USER->IsAuthorized()) return;

		$arResult = array(
			"ERROR" => array()
		);

		$ID = $USER->GetID();

		$rsUser = CUser::GetByID($ID);
		$arUser = $rsUser->Fetch();

		if(!empty($arUser['UF_COUNT_FORM'])) {
			$arr = json_decode($arUser['UF_COUNT_FORM'], true);
			if(isset($arr[$_REQUEST['number_form']])) {
				unset($arr[$_REQUEST['number_form']]);
				$obUser = new CUser;
				if(count($arr) > 0) {
					$arFields = array(
						"UF_COUNT_FORM" => json_encode($arr)
					);
				} else {
					$arFields = array(
						"UF_COUNT_FORM" => ''
					);
				}
				
				
				/*
				$arrFormID = explode("_", $_REQUEST['number_form']);
				$formID = 0;
				
				if(count($arrFormID) == 2){
					
					$formID = strval($arrFormID[1]);
					
					if(CModule::IncludeModule("sale")){
						if(!empty($arUser['UF_SALE_PROFILE'])) {
							$objProfilePros = json_decode($arUser['UF_SALE_PROFILE']);
						
						
							$db_sales = CSaleOrderUserProps::GetList(
									array("DATE_UPDATE" => "DESC"),
									array("USER_ID" => $ID, "PERSON_TYPE_ID" => $objProfilePros->PROFILE_TYPE)
								);
				
							while($ar_sales = $db_sales->Fetch()){
								if("Адрес доставки " . $formID == $ar_sales['NAME']){
									CSaleOrderUserProps::Delete($ar_sales['ID']);
								}
								
							}
						}
			
					}
				}*/
			
			
				if(!$obUser->Update($ID, $arFields)){
					$arResult['ERROR'][] = $obUser->LAST_ERROR;
				}
			}
		}
		echo json_encode($arResult);
endif;
?>
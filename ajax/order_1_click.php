<?
if($_SERVER['REQUEST_METHOD'] == 'POST'):
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
	$phones = $_REQUEST['phone'];
	$arResult = array();
	$arResult[] = $phones;
	if(!empty($phones) && strlen($phones) >= 9) {
		global $USER;
		CModule::IncludeModule('sale'); CModule::IncludeModule('catalog');
		$ID = null;
		if($USER->IsAuthorized())
			$ID = $USER->GetID();
		else {
			$user = new CUser;
			$arFields = Array(
			  "LOGIN"             => "user_".time(),
			  "EMAIL"             => "user_".time()."@example.com",
			  "PERSONAL_PHONE"	  => $phones,
			  "LID"               => "ru",
			  "ACTIVE"            => "Y",
			  "PASSWORD"          => "123456",
			  "CONFIRM_PASSWORD"  => "123456"
			);
			
			$ID = $user->Add($arFields);
		}

		$arBasketItems = array();
		$arID = array();
		$price = 0;
	
		$dbBasketItems = CSaleBasket::GetList(
		     array(
		                "NAME" => "ASC",
		                "ID" => "ASC"
		             ),
		     array(
		                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
		                "LID" => SITE_ID,
		                "ORDER_ID" => "NULL"
		             ),
		     false,
		     false,
		     array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
		             );
		while ($arItems = $dbBasketItems->Fetch())
		{
		     if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"])
		     {
		          CSaleBasket::UpdatePrice($arItems["ID"],
		                                 $arItems["CALLBACK_FUNC"],
		                                 $arItems["MODULE"],
		                                 $arItems["PRODUCT_ID"],
		                                 $arItems["QUANTITY"],
		                                 "N",
		                                 $arItems["PRODUCT_PROVIDER_CLASS"]
		                                 );
		          $arID[] = $arItems["ID"];
		     }
		}
		$dbBasketItems = CSaleBasket::GetList(array(
	          "NAME" => "ASC",
	          "ID" => "ASC"
	          ),
	     array(
	        "ID" => $arID,
	        "ORDER_ID" => "NULL"),
	        false,
	        false,
	        array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT", "PRODUCT_PROVIDER_CLASS", "NAME")
        );
		while ($arItems = $dbBasketItems->Fetch())
		{
			$price += floatval($arItems['PRICE']) * round($arItems['QUANTITY']);
		    $arBasketItems[] = $arItems;
		}
		
		$order_id = CSaleOrder::Add(
    	array(
    	   'LID' => SITE_ID,
    	   'PERSON_TYPE_ID' => 1,
    	   'PAYED' => "N",
    	   'CANCELED' => "N",
    	   'STATUS_ID' => "N",
    	   'ALLOW_DELIVERY' => 'N',
    	   'PRICE' => $price,
    	   'CURRENCY' => "RUB",
    	   'USER_ID' => $ID,
    	   'PAY_SYSTEM_ID' => 5,
    	   'DELIVERY_ID' => 1,
    	   'USER_DESCRIPTION' => $_SESSION['ADDICTION_ORDER'],
    	   'ADDITIONAL_INFO' => 'Заказ в один клик',
    	   )
    	);
		CSaleBasket::OrderBasket($order_id, CSaleBasket::GetBasketUserID(), 's1');
		$propS = CSaleOrderProps::GetList(array(), array('CODE' => 'PHONE', 'PERSON_TYPE_ID' => 1), false, false, array());
		if($propSale = $propS->Fetch()) {
			$arResult['FIELDS'] = $propSale;
			$arFieldsPHONE = array(
    		   "ORDER_ID" => $order_id,
    		   "ORDER_PROPS_ID" => $propSale['ID'],
    		   "NAME" => $propSale['NAME'],
    		   "CODE" => $propSale['CODE'],
    		   "VALUE" => $phones
    		);
    		if(CSaleOrderPropsValue::Add($arFieldsPHONE)) $arResult['SUCCESS'] = 'ok';
		}
	}
	$arResult['NUM_ORDER'] = $order_id;
	echo json_encode($arResult);
endif;
?>
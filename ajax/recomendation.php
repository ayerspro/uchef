<?
if($_SERVER['REQUEST_METHOD'] == 'POST'):
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
		global $USER;
		if(!$USER->IsAuthorized()) return;
		$RECOMENDATION = trim($_REQUEST['recomendation_shef']);

		$arResult = array();

		$ID = $USER->GetID();

		$rsUser = CUser::GetByID($ID);
		$arUser = $rsUser->Fetch();
		$arFields = array(
			"UF_RECOMENDATION" => $RECOMENDATION
		);

		$obUser = new CUser;

		if(!$obUser->Update($ID, $arFields))
			$arResult['ERROR'] = $obUser->LAST_ERROR;
		else $arResult['SUCCESS'] = 'ok';

		echo json_encode($arResult);
endif;
?>
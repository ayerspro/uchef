<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");


if(!CModule::IncludeModule("search")){die();}
if(!CModule::IncludeModule("iblock")){die();}
if(!CModule::IncludeModule("catalog")){die();}


$IBLOCK_ID = 5; 
$arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID); 

$obSearch = new CSearch;
$obSearch->Search(
	array(
	   'QUERY' => $_REQUEST['term'],
	   'SITE_ID' => SITE_ID,
	   'MODULE_ID' => 'iblock',
	   'PARAM2' => array($IBLOCK_ID)
	)
);  

$arItems = array();

while ($row = $obSearch->fetch()) {  

	$arItems[] = $row['ITEM_ID'];
}

if(!$arItems) {
	
	$arItems = array(-1);
}


$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_PICTURE", "DETAIL_PICTURE", "DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ID"=>$arItems, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

while($ob = $res->GetNextElement()) {
	
	$isTP = false;
	
	$arFields = $ob->GetFields();
	$arrItems[$arFields['ID']]=$arFields;
	if(!empty($arFields['PREVIEW_PICTURE'])){
		
		$file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$arrItems[$arFields['ID']]['PICTURE'] = $file['src'];
		
	}elseif(!empty($arFields['DETAIL_PICTURE'])){
		
		$file = CFile::ResizeImageGet($arFields['DETAIL_PICTURE'], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$arrItems[$arFields['ID']]['PICTURE'] = $file['src'];
	}
	
	$arPrice = CCatalogProduct::GetOptimalPrice($arFields['ID'], 1, $USER->GetUserGroupArray(), 'N', SITE_ID);
	
	if(!$arPrice && $arInfo) {
		
		$rsOffers = CIBlockElement::GetList(array(),array('IBLOCK_ID' => $arInfo['IBLOCK_ID'], 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $arFields['ID'])); 
		while ($arOffer = $rsOffers->GetNext()) {
			
			$isTP = true;
			
			if(!$arPrice) {
				
				$arPrice = CCatalogProduct::GetOptimalPrice($arOffer['ID'], 1, $USER->GetUserGroupArray(), 'N', SITE_ID);
				continue;
			}
			
			$arPriceTmp = CCatalogProduct::GetOptimalPrice($arOffer['ID'], 1, $USER->GetUserGroupArray(), 'N', SITE_ID);
			
			if($arPriceTmp['DISCOUNT_PRICE'] > 0 && $arPriceTmp['DISCOUNT_PRICE'] < $arPrice['DISCOUNT_PRICE']) {
				
				$arPrice = $arPriceTmp;
			}
		}
	}
	
	$arPrice['IS_TP'] = $isTP;
	
	$arrItems[$arFields['ID']]["PRICES"] = $arPrice;
	
}

//echo'<pre>';print_r($arrItems);echo'</pre>';
if(count($arrItems)>0){
?>
<div class="bx_searche">
	<div class="bx_item_block all_result" style="text-align: center;">
		<div>
			<span class="all_result_title"><a href="/?q=<?echo$_REQUEST['term']?>">Все результаты (<?echo count($arrItems);?>)</a></span>
		</div>
		<div style="clear:both;"></div>
	</div>
	<? $arrItems = array_slice($arrItems, 0, 50); ?>
	<? foreach ($arrItems as $key => $arItem) {?>
		<div class="bx-basket-item-list-items">
			<?if (!empty($arItem["PICTURE"])):?>
			<div class="bx-basket-item-list-item-img">
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
					<img src="<?echo $arItem["PICTURE"]?>" alt="<?=$arItem['NAME']?>">
				</a>
			</div>
			<?endif;?>
			<div class="bx-basket-item-list-item-name">
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
				<div class="bx-basket-item-list-item-price-block">
				<?
		//print_r($arItem['PRICES']["DISCOUNT_PRICE"]);
		/*echo'<pre>';print_r($arItem['PRICES']["DISCOUNT_PRICE"]);echo'</pre>';
echo'<pre>';print_r($arItem['PRICES']);echo'</pre>';*/
						if(round($arItem['PRICES']["DISCOUNT_PRICE"]) < round($arItem['PRICES']["PRICE"]['PRICE'])):?>
							<div class="bx-basket-item-list-item-old-price">
								<? if($arItem['PRICES']['IS_TP']) { ?>от <? } ?><?=$arItem['PRICES']["PRICE"]['PRICE']?> <i class="fa fa-ruble-sign"></i>
							</div>
							<div class="bx-basket-item-list-item-price">
								<? if($arItem['PRICES']['IS_TP']) { ?>от <? } ?><?=numberFormat($arItem['PRICES']["DISCOUNT_PRICE"], 2, '.', ' ')?> <i class="fa fa-ruble-sign"></i>
							</div>
							<?else:?>
								<div class="bx-basket-item-list-item-price"><? if($arItem['PRICES']['IS_TP']) { ?>от <? } ?><?=$arItem['PRICES']["PRICE"]['PRICE']?> <i class="fa fa-ruble-sign"></i></div>
							<?endif;
					
					?>
				</div>
			</div>
				<div style="clear:both;"></div>
			</div>
	<?} ?>
</div>
<?}?>
<?if($_SERVER['REQUEST_METHOD'] == 'POST') {
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

	if (CModule::IncludeModule("catalog") && CModule::IncludeModule("iblock") && CModule::IncludeModule("sale"))
	{
		global $USER;
		
		$rsUser = CUser::GetByID($USER->GetID());
		$arUser = $rsUser->Fetch();
		
		if(!$arUser){
			$arr['ERROR'] = '�� ���������� ����� ������������';
			echo json_encode($arr);
			return;
		}
			
		$list = unserialize($arUser['UF_LISTS']);
		if(!empty($_REQUEST['listall'])){
			$arFilter = array(
				'IBLOCK_ID' =>"5",
				'ACTIVE' => 'Y'
			);
			$arFilter['PROPERTY_F_USER'] = $_REQUEST['listall'];
			$rsElements = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID"));
			while($arItem = $rsElements->GetNext()){

				$res="";
				$res = CCatalogSKU::getOffersList(array($arItem['ID']), 0, array(), array(), array());
				
				if(!empty($res)){
					//echo'<pre>';print_r($res);echo'</pre>';
					$arListItems[$_REQUEST['offers']]=$_REQUEST['offers'];
				}else{
					$arListItems[$arItem['ID']]=$arItem['ID'];
				}
				//echo'<pre>';print_r($arListItems);echo'</pre>';
			}
		}else{
			$arListItems = (!empty($list[$_REQUEST['list']])?$list[$_REQUEST['list']]:false);
		}
		//echo'<pre>';print_r($arListItems);echo'</pre>';

		$arBasketItems = array();
		$arr = array('ITEM' => array(), 'SEND' => 'NO');
		$quantity = 0;
		$itemQuantity = 1;
		

		$dbBasketItems = CSaleBasket::GetList(
		        array(
		                "NAME" => "ASC",
		                "ID" => "ASC"
		            ),
		        array(
		                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
		                "LID" => SITE_ID,
		                "ORDER_ID" => "NULL"
		            ),
		        false,
		        false,
		        array()
		    );
		while ($arItems = $dbBasketItems->Fetch())
		{
		    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
		    {
		        CSaleBasket::UpdatePrice($arItems["ID"], 
		                                 $arItems["CALLBACK_FUNC"], 
		                                 $arItems["MODULE"], 
		                                 $arItems["PRODUCT_ID"], 
		                                 $arItems["QUANTITY"]);
		        $arItems = CSaleBasket::GetByID($arItems["ID"]);
		    }
	
		    $arBasketItems[$arItems['PRODUCT_ID']] = $arItems;
		}
		
		foreach($arListItems as $listItemID){
			$res = CCatalogSKU::getOffersList(array($listItemID), 0, array(), array(), array());
			$offers = "";
			foreach ($res as $key) {
				foreach ($key as $value) {
					$offers = $value['ID'];
					break;
				}
			}
			
			
			if(isset($arBasketItems[($offers)?$offers:$listItemID])) {
				$quantity = $arBasketItems[($offers)?$offers:$itemQuantity];
			}

			if((!empty($quantity) && $quantity < $itemQuantity) || empty($quantity)) {
				if(!empty($quantity)) $itemQuantity = $quantity - $itemQuantity;

				if(Add2BasketByProductID((int)$listItemID, $itemQuantity, array(), array())) {
					$el = CIBlockElement::GetByID($listItemID)->GetNext();
					$arr['ITEM'][] = $el;
					$arr['SEND'] = 'YES';
				} elseif(!empty($offers) && Add2BasketByProductID((int)$offers, $itemQuantity, array(), array())) {
						$el = CIBlockElement::GetByID($listItemID)->GetNext();
						$arr['ITEM'][] = $el;
						$arr['SEND'] = 'YES';
				} else {
					$ex = $APPLICATION->GetException();
					$arr['ERROR'] = $ex->GetString();
				}
			} elseif(!empty($quantity) && $quantity > $itemQuantity) {
				if(CSaleBasket::Delete($arBasketItems[($offers)?$offers:$listItemID])) {
					if(Add2BasketByProductID((int)$listItemID, (int)$itemQuantity, array(), array())) {
						$el = CIBlockElement::GetByID($listItemID)->GetNext();
						$arr['ITEM'][] = $el;
						$arr['SEND'] = 'YES';
					} elseif(!empty($offers) && Add2BasketByProductID((int)$offers, $itemQuantity, array(), array())) {
						$el = CIBlockElement::GetByID($listItemID)->GetNext();
						$arr['ITEM'][] = $el;
						$arr['SEND'] = 'YES';
					} else {
						$ex = $APPLICATION->GetException();
						$arr['ERROR'] = $ex->GetString();
					}
				}
			} else {
				$ex = $APPLICATION->GetException();
				$arr['ERROR'] = $ex->GetString();
			}
		}
		
	    echo json_encode($arr);
	}
}
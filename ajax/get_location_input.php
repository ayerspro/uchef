<?
if($_SERVER['REQUEST_METHOD'] == 'POST'):
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
	
	$locationTemplate = ($_REQUEST['USE_AJAX_LOCATIONS'] !== 'Y') ? "popup" : "";
								
	CSaleLocation::proxySaleAjaxLocationsComponent(
		array(
			"AJAX_CALL" => "Y",
			"CITY_OUT_LOCATION" => "Y",
			"COUNTRY_INPUT_NAME" => $_REQUEST['LOCATION'].'_COUNTRY',
			"CITY_INPUT_NAME" => $_REQUEST['NAME'],
			"LOCATION_VALUE" => $_REQUEST['LOCATION'],
		),
		array(
		),
		$locationTemplate,
		true,
		'location-block-wrapper'
	);
?>		
<? endif; ?>
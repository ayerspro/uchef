<?php
$arUrlRewrite=array (
  1 => 
  array (
    'CONDITION' => '#^/sales/(.+?)/.*#',
    'RULE' => 'MAINSALES=Y&SEC=$1',
    'ID' => '',
    'PATH' => '/catalog/index.php',
    'SORT' => 50,
  ),
  11 => 
  array (
    'CONDITION' => '#^/bitrix/services/ymarket/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/bitrix/services/ymarket/index.php',
    'SORT' => 100,
  ),
  0 => 
  array (
    'CONDITION' => '#^/category-([\\w-_]+)/.*#',
    'RULE' => 'MAINCATEGORY=$1',
    'ID' => '',
    'PATH' => '/catalog/index.php',
    'SORT' => 100,
  ),
  3 => 
  array (
    'CONDITION' => '#^/personal/order/#',
    'RULE' => '',
    'ID' => 'bitrix:sale.personal.order',
    'PATH' => '/personal/order/index.php',
    'SORT' => 100,
  ),
  4 => 
  array (
    'CONDITION' => '#^/personal/#',
    'RULE' => '',
    'ID' => 'bitrix:sale.personal.section',
    'PATH' => '/personal/index.php',
    'SORT' => 100,
  ),
  2 => 
  array (
    'CONDITION' => '#^/sales/.*#',
    'RULE' => 'MAINSALES=Y',
    'ID' => '',
    'PATH' => '/catalog/index.php',
    'SORT' => 100,
  ),
  5 => 
  array (
    'CONDITION' => '#^/recepty/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/recepty/index.php',
    'SORT' => 100,
  ),
  8 => 
  array (
    'CONDITION' => '#^/recept/#',
    'RULE' => '',
    'ID' => 'bitrix:news.list',
    'PATH' => '/bitrix/templates/eshop_bootstrap_green/components/bitrix/catalog/catalog/element.php',
    'SORT' => 100,
  ),
  9 => 
  array (
    'CONDITION' => '#^/store/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog.store',
    'PATH' => '/store/index.php',
    'SORT' => 100,
  ),
  10 => 
  array (
    'CONDITION' => '#^/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/news/index.php',
    'SORT' => 100,
  ),
  7 => 
  array (
    'CONDITION' => '#^/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/catalog.php',
    'SORT' => 100,
  ),
);

<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");

$iBlockID = 5;
$novelitySection = 5018;
$curTime = time();

$arSelect = array("ID", "NAME");
$arFilter = array(
	"IBLOCK_ID" => $iBlockID, 
	'PROPERTY_CATEGORY' => $novelitySection, 
	'!PROPERTY_NOVELTY_DATE_END' => false, 
	'<PROPERTY_NOVELTY_DATE_END' => date('Y-m-d H:i:s', $curTime)
);

$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

while($ob = $res->GetNextElement()) {
	
	$arFields = $ob->GetFields();
	
	$arCurCategory = array();
	$resProp = CIBlockElement::GetProperty($iBlockID, $arFields['ID'], "sort", "asc", array("CODE" => "CATEGORY"));
	
	while($ob = $resProp->GetNext()) {
		
		$arCurCategory[$ob['VALUE']] = $ob['VALUE'];
	}
	
	unset($arCurCategory[$novelitySection]);
	CIBlockElement::SetPropertyValueCode($arFields['ID'], 'CATEGORY', $arCurCategory);
	CIBlockElement::SetPropertyValueCode($arFields['ID'], 'NOVELTY_DATE_END', false);
	
}

?>
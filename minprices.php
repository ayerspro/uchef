<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
// ID инфоблока  товаров
$ID_BLOCK = 5;

$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
$arFilter = Array("IBLOCK_ID"=>$ID_BLOCK,"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(
                  Array('ID',"NAME"),
      $arFilter, false,
      Array("nPageSize"=>5000),
      $arSelect);

while($ob = $res->GetNextElement()){
   $arFields = $ob->GetFields();

   $MIN_PRICE  = get_offer_min_price($ID_BLOCK,$arFields['ID']);
   $MAX_PRICE  = get_offer_max_price($ID_BLOCK,$arFields['ID']);
   if ($MIN_PRICE == 0 && $MAX_PRICE == 0) {
	$MIN_PRICE = $MAX_PRICE = GetCatalogProductPrice($arFields['ID'], 1);
	$MIN_PRICE = $MAX_PRICE = $MIN_PRICE['PRICE'];
   }
   echo $arFields['ID'].' min= '.$MIN_PRICE.' max='.$MAX_PRICE.' - '.$arFields['NAME'];echo '<br>';
   //Обновляем свойства товара
   CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('MINIMUM_PRICE' => $MIN_PRICE));
   CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('MAXIMUM_PRICE' => $MAX_PRICE));
}
//-------------------------------
/*
Возвращает минимальную цену товара из тп
*/
function get_offer_min_price($IBLOCK_ID,$item_id){
    $ret = 0;
    $arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID);
    if (is_array($arInfo)) {
        $res = CIBlockElement::GetList(Array("PRICE"=>"asc"),
           array('IBLOCK_ID'=>$arInfo['IBLOCK_ID'], 'ACTIVE'=>'Y', 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $item_id),
           false,
           false,
           array('ID', 'NAME'))->GetNext();
        if ($res){
            $ret = GetCatalogProductPrice($res["ID"], 1);
            if ($ret['PRICE']){
                $ret = $ret['PRICE'];
            }
        }
    }
    return $ret;
}
/*
Возвращает максимальную цену товара из тп
*/
function get_offer_max_price($IBLOCK_ID,$item_id){
    $ret = 0;
    $arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID);
    if (is_array($arInfo)) {
        $res = CIBlockElement::GetList(Array("PRICE"=>"desc"),
           array('IBLOCK_ID'=>$arInfo['IBLOCK_ID'], 'ACTIVE'=>'Y', 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $item_id),
           false,
           false,
           array('ID', 'NAME'))->GetNext();
        if ($res){
            $ret = GetCatalogProductPrice($res["ID"], 1);
            if ($ret['PRICE']){
                $ret = $ret['PRICE'];
            }
        }
    }
    return $ret;
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
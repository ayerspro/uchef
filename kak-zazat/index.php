<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "доставка деликатесов, продукты на дом, качественная еда, вкусные продукты.");
$APPLICATION->SetPageProperty("keywords_inner", "доставка деликатесов, продукты на дом, качественная еда, вкусные продукты");
$APPLICATION->SetPageProperty("title", "Способы оплаты | Интернет магазин Шеф Гурме");
$APPLICATION->SetPageProperty("keywords", "Способы оплаты шеф Гурме, как заказать шеф Гурме");
$APPLICATION->SetPageProperty("description", "Описание условий покупки, способов оплаты товаров в интернет магазине Шеф Гурме");
$APPLICATION->SetTitle("Как купить");

$cityList = array(
	'Санкт-Петербург',
	'Ленинградская область',
	'Москва',
	'Московская область',
	'Прочие регионы'
);

?><link rel="stylesheet" href="css/payment-page-style.css">


<div class="container payment-page">
	<h1 class="payment-page-title">Оплата</h1>
	
	<h2 class="payment-page-h2">Варианты оплаты</h2>
	<p class="payment-page-white-box-text">Обратите внимание: для каждого отдельного заказа возможен только один способ оплаты. Оплатить заказ по частям различными способами нельзя.</p>
	<div class="payment-page-white-box-text2">
	<p class="abzac">Клиентам нашего интернет-магазина доступен как наличный, так и безналичный расчет в разных вариациях, а для юридических лиц – выставление официального счета.</p>
	<p>Способ оплаты указывается при оформлении заказа в Корзине.</p>
	</div>
	<!-- ТАБЛИЦА -->
	<table class="table table-payment">
	<tbody>
	<tr class="table-row">
		<td class="table-left-city">
			<?foreach($cityList as $id => $value):?>
				<a href="#" class="city-link" data-id="<?=$id;?>"><?=$value;?></a>
			<?endforeach;?>
		</td>
		
		<td class="city-payment"></td>
	</tr>
	</tbody>
	</table>
	<!-- -->
		<!-- АККОРДЕОН -->
	<div class="ac-container">
		<input type="radio" name="accordion-1" id="ac-1"/>
		<label for="ac-1" class="link"><?= $cityList[0]?></label>
		<div id="info_pay_0">
			<p class='payment-id0'>Наличными курьеру</p>
			<p class='payment-id1'>Оплата картой курьеру</p>
			<p class='payment-id2'>Наличными в пунктах самовывоза</p>
			<p class='payment-id3'>Оплата картой на сайте</p>
			<p class='payment-id4'>Оплата Яндекс Деньгами</p>
			<p class='payment-id5'>Безналичная оплата для юридических лиц с выставлением стандартного счета</p>
		</div>
		<input type="radio" name="accordion-1" id="ac-2"/>
		<label for="ac-2"><?= $cityList[1]?></label>
		<div id="info_pay_1">
			<p class='payment-id0'>Наличными курьеру</p>
			<p class='payment-id2'>Наличными в пунктах самовывоза</p>
			<p class='payment-id3'>Оплата картой на сайте</p>
			<p class='payment-id4'>Оплата Яндекс Деньгами</p>
			<p class='payment-id5'>Безналичная оплата для юридических лиц с выставлением стандартного счета</p>
		</div>
		<input type="radio" name="accordion-1" id="ac-3"/>
		<label for="ac-3"><?= $cityList[2]?></label>
		<div id="info_pay_2">
		<p class='payment-id0'>Наличными курьеру</p>
			<p class='payment-id1'>Оплата картой курьеру</p>
			<p class='payment-id2'>Наличными в пунктах самовывоза</p>
			<p class='payment-id3'>Оплата картой на сайте</p>
			<p class='payment-id4'>Оплата Яндекс Деньгами</p>
			<p class='payment-id5'>Безналичная оплата для юридических лиц с выставлением стандартного счета</p>
		</div>
		<input type="radio" name="accordion-1" id="ac-4"/>
		<label for="ac-4"><?= $cityList[3]?></label>
		<div id="info_pay_3">
			<p class='payment-id0'>Наличными курьеру</p>
			<p class='payment-id2'>Наличными в пунктах самовывоза</p>
			<p class='payment-id3'>Оплата картой на сайте</p>
			<p class='payment-id4'>Оплата Яндекс Деньгами</p>
			<p class='payment-id5'>Безналичная оплата для юридических лиц с выставлением стандартного счета</p>
		</div>
		<input type="radio" name="accordion-1" id="ac-5"/>
		<label for="ac-5"><?= $cityList[4]?></label>
		<div id="info_pay_4">
			<p class='payment-id2'>Наличными в пунктах самовывоза</p>
			<p class='payment-id3'>Оплата картой на сайте</p>
			<p class='payment-id4'>Оплата Яндекс Деньгами</p>
			<p class='payment-id5'>Безналичная оплата для юридических лиц с выставлением стандартного счета</p>
		</div>
	</div>
	<!-- -->
	
	<!-- FOOTER -->
	<div class="contacts">
		<p> Для уточнения возникших вопросов вы можете связаться с нашими менеджерами:</p>

		<table style="margin:15px 0px;width:100%;">
			<tbody>
				<tr>
					<td id="phone1" align="center" style="width:45%"><p class="phone1">Телефон:<b> 8 (812) 607 38 37</b> (многоканальный).</p></td>
					<td id="email1" align="center" style="width:35%"><p class="email1">Электронная почта: <a href="mailto:info@uchef.ru" style="color:#402f2b"><b>info@uchef.ru</b></a></p></td>
					<td id="skype1" align="center" style="width:20%"><p class="skype1" ><b>Skype:</b> uchef.ru</p></td>
				</tr>
			</tbody>
		</table>
		<p> Мы с радостью дадим вам подробную информацию по всем видам оплаты.</p>
	</div>
	<!-- -->
</div>
<!--------------------------------------------------------------------------------------->
<script>
BX.ready(function(){
	$('.city-link').click(function(){
		console.log($(this));
		var deliv=$("#info_pay_"+$(this).data("id")).html();
		$('.city-payment').html(deliv); 
		/*$.ajax({
		  method: "POST",
		  url: "/ajax/get-city-payment.php",
		  data: { "id":$(this).data("id")  },
		  success: function(result){
			$('.city-payment').html(result); 
		  },
		  error: function(err){
			console.log(err);    
		  }
		});*/

		return false;
	});
	
	$('.city-link:eq(0)').trigger('click');
	$('.city-link:eq(0)').addClass('active-first');
	
	
	$('.city-link').click(function(){
	$('.city-link:eq(0)').removeClass('active-first');
	});
});
</script> 
<!--------------------------------------------------------------------------------------->
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
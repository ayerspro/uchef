<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контактная информация | Интернет магазин Шеф Гурме");
$APPLICATION->SetPageProperty("keywords", "контакты шеф гурме, 8 (800) 775-73-53");
$APPLICATION->SetPageProperty("description", "Телефоны и адреса интернет магазина Шеф Гурме в Санкт-Петербурге и Москве");
$APPLICATION->SetTitle("Контактная информация");
?><div id="contact-page" class="container">
	<div itemscope="" itemtype="http://schema.org/Organization">
		<h1 style="text-align:center;"><?=$APPLICATION->GetTitle();?></h1>
		<div class="row" style="margin-top:30px;">
			<div class="col-sm-6">
				 <iframe id="contact-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1995.2910614683221!2d30.296294816099202!3d59.99366708189617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4696340f9f441df7%3A0x83d2ecc8a839889c!2z0JrQvtC70L7QvNGP0LbRgdC60LjQuSDQv9GALiwgMTAsINCh0LDQvdC60YIt0J_QtdGC0LXRgNCx0YPRgNCzLCDQoNC-0YHRgdC40Y8sIDE5NzM0Mg!5e0!3m2!1sru!2sca!4v1508321687704" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="col-sm-6">
				<div id="contact-page-desc">
					<div class="row">
						<div class="col-xs-1">
 <i class="fa fa-map-marker"></i>
						</div>
						<div class="col-xs-11" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
 <span itemprop="streetAddress">Коломяжский проспект, 10</span> <span itemprop="addressLocality">Санкт-Петербург</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-1">
 <i class="fa fa-clock-o"></i>
						</div>
						<div class="col-xs-11">
							 Заказы товаров через сайт принимаются&nbsp;круглосуточно без обедов и выходных
						</div>
					</div>
					<div class="row">
						<div class="col-xs-1">
 <i class="fa fa-phone"></i>
						</div>
						<div class="col-xs-11">
 <span itemprop="telephone">8-800-775-73-53</span><br>
 <span itemprop="telephone">8-812-607-38-37</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-1">
 <i class="fa fa-envelope"></i>
						</div>
						<div class="col-xs-11">
 <span itemprop="email"><a href="mailto:info@uchef.ru">info.yaeda@gmail.com</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<h3>Прием заказов и служба поддержки клиентов:</h3>
				<div>
 <b>Заказ товаров через сайт</b> принимается круглосуточно (24/7).<br>
 <b>Заказ товаров по телефону</b> вы можете оформить ежедневно с 9.00 до 21.00.<br>
 <b>Самовывоз </b>товаров вы можете осуществить:<br>
					 пн-пт с 10.00 до 19.00<br>
					 сб, вс - выходной. Мы работаем над тем, чтобы открыть для вас магазин и в выходные!<br>
				</div>
				<div>
 <b>Телефон</b>: <br>
					 8-800-775-73-53<br>
					 8-812-607-38-37
				</div>
				<div>
					 Прием и обработка заказов и служба контроля качества:&nbsp;<a href="mailto:info.yaeda@gmail.com">info.yaeda@gmail.com</a><br>
				</div>
 <br>
 <br>
				<h3>Реквизиты организации</h3>
				<div>
					 Наименование организации: <span itemprop="name">ООО "СитиСервис"</span>.
				</div>
				<div>
					 Адрес юридический: Россия, Санкт-Петербург, Коломяжский пр-кт, д.10, литер Э, пом. 3Н
				</div>
 <br>
				<div>
					<table>
					<tbody>
					<tr style="height: 22px;">
						<td style="width: 146.458px; height: 22px;">
							 ИНН
						</td>
						<td style="width: 395.347px; height: 22px;">
							 7814640690
						</td>
					</tr>
					<tr style="height: 22px;">
						<td style="width: 146.458px; height: 22px;">
							 КПП
						</td>
						<td style="width: 395.347px; height: 22px;">
							 781401001
						</td>
					</tr>
					<tr style="height: 22.5695px;">
						<td style="width: 146.458px; height: 22.5695px;">
							 ОГРН
						</td>
						<td style="width: 395.347px; height: 22.5695px;">
							 1167847096340
						</td>
					</tr>
					<tr>
						<td colspan="1">
							 БИК
						</td>
						<td colspan="1">
							 044030707
						</td>
					</tr>
					<tr>
						<td colspan="1">
							 Банк
						</td>
						<td colspan="1">
							 Филиал №7806 ВТБ (ПАО)<br>
							 г. Санкт-Петербург
						</td>
					</tr>
					<tr>
						<td colspan="1">
							 Кор.сч
						</td>
						<td colspan="1">
							 30101810240300000707
						</td>
					</tr>
					<tr>
						<td colspan="1">
							 р/сч
						</td>
						<td colspan="1">
							 40702810435260005117
						</td>
					</tr>
					</tbody>
					</table>
 <br>
				</div>
			</div>
			<div class="col-sm-6">
				<h2>Задать вопрос</h2>
				 <?$APPLICATION->IncludeComponent(
	"devstrong:callback",
	"contacts-form",
	Array(
		"COMMENT_PH" => "Сообщение",
		"COMPONENT_ID" => "ds-contacts",
		"COMPONENT_TEMPLATE" => "contacts-form",
		"EMAIL_PH" => "",
		"EMAIL_TITLE" => "Ваш E-mail",
		"FIELDS" => array(0=>"DSCB_NAME",1=>"DSCB_EMAIL",2=>"DSCB_COMMENT",),
		"FORM_TITLE" => "",
		"IBLOCK_ID" => "",
		"INCLUDE_INPUTMASK" => "N",
		"INCLUDE_MAGNIFIC" => "N",
		"NAME_PH" => "",
		"NAME_TITLE" => "Ваше имя",
		"PHONE_PH" => "",
		"PHONE_TITLE" => "",
		"RECAPTCHA_ERROR" => "",
		"RECAPTCHA_SECRET" => "6Lcrz24UAAAAAEbMp9ghga43PHX9opbl6nm8-S2p",
		"REQUIRED_FIELDS" => array(0=>"DSCB_NAME",1=>"DSCB_EMAIL",2=>"DSCB_COMMENT",),
		"REQUIRED_FIELDS_ERR" => "",
		"REQUIRED_TITLE" => "",
		"SEND_BTN_TITLE" => "Отправить",
		"SUCCESS_FORM_TITLE" => "",
		"SUCCESS_MESS" => "Спасибо, ваше сообщение принято.",
		"USE_RECAPTCHA" => "N"
	)
);?>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>
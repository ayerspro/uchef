<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "интернет магазин продуктов");
$APPLICATION->SetPageProperty("description", "интернет магазин продуктов питания с доставкой");
$APPLICATION->SetPageProperty("title", "Интернет магазин Яеда");
$APPLICATION->SetTitle("Магазин продуктов и деликатесов");
$APPLICATION->SetAdditionalCSS('/bitrix/templates/eshop_bootstrap_green/css/main.css');
?><section class="section-slider" id="main-slider"><?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"main_slider",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("DETAIL_PICTURE",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "slider",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("SL_BUTTON_LINK","SL_BUTTON_TEXT",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?> </section>
<?

global $uchefSettings;

$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"coupons",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => $uchefSettings['COUPONS_IBLOCK'],
		"IBLOCK_TYPE" => "coupons",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"IS_SLIDER" => "Y",
		"IS_HOME" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "50",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("COUPON",""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "RAND",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?> <?$arUser;
if($USER->IsAuthorized()) {
	$ID = $USER->GetID();
	$rsUser = CUser::GetByID($ID);
	$arUser = $rsUser->Fetch();
}
?> <section class="section-main_recommend">
<div class="container">
	 <?$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>5, "CODE"=>"GURMAN"));?>
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="section-h2">
				 Советуем попробовать
			</div>
			<form id="formx" method="GET">
				<h4>Рекомендации для любителей <span class="select-wrapp">
				<?
				$catIdCurrent = 22;

				if(isset($_GET['gurman'])) {

					$catIdCurrent = $_GET['gurman'];

				} elseif($arUser['UF_RECOMENDATION']) {

					$catIdCurrent = $arUser['UF_RECOMENDATION'];
				}
				?>
				<select name="gurman">
					<?while($enum_fields = $property_enums->GetNext()){?>
						<option id="<?=$enum_fields["ID"]?>" value="<?=$enum_fields["ID"]?>" <?if($catIdCurrent == $enum_fields["ID"]) echo "selected"?>><?=$enum_fields["VALUE"]?></option>
					<?}?>
				</select>
 </span></h4>
			</form>
		</div>
	</div>
	<div class="row">
		 <? if(isset($_GET['gurman'])){
			$catId = $_GET['gurman'];
		} else {
			if(!empty($arUser['UF_RECOMENDATION'])){
				$catId = $arUser['UF_RECOMENDATION'];
			}else{
				$catId = 22;
			}

		}
		
		global $arFilterRecomend;
		
		$arFilterRecomend = array(
		
			array(
			
				"LOGIC" => "OR",
				array(
					array(
						"LOGIC" => "OR",
						"PROPERTY_REGION" => false,
					),
				),
				array(
					array(
						"LOGIC" => "OR",
						"PROPERTY_REGION" => $_SESSION['SOTBIT_REGIONS']['ID'],
					),
				),
			)
		);

		$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"main_gurman",
		array(
			"ACTION_VARIABLE" => "action",
			"ADD_PICT_PROP" => "-",
			"ADD_PROPERTIES_TO_BASKET" => "Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"ADD_TO_BASKET_ACTION" => "ADD",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "Y",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"BACKGROUND_IMAGE" => "-",
			"BASKET_URL" => "/personal/cart/",
			"BROWSER_TITLE" => "-",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COMPATIBLE_MODE" => "Y",
			"COMPONENT_TEMPLATE" => "gurman",
			"CONVERT_CURRENCY" => "N",
			"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[{\"CLASS_ID\":\"CondIBProp:5:60\",\"DATA\":{\"logic\":\"Equal\",\"value\":".(int)$catId."}}]}",
			"DETAIL_URL" => "/#SECTION_CODE#/#ELEMENT_CODE#/",
			"DISABLE_INIT_JS_IN_COMPONENT" => "N",
			"DISCOUNT_PERCENT_POSITION" => "top-right",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"DISPLAY_COMPARE" => "N",
			"DISPLAY_TOP_PAGER" => "N",
			"ELEMENT_SORT_FIELD" => "shows",
			"ELEMENT_SORT_FIELD2" => "shows",
			"ELEMENT_SORT_ORDER" => "asc",
			"ELEMENT_SORT_ORDER2" => "asc",
			"ENLARGE_PRODUCT" => "STRICT",
			"FILTER_NAME" => "arFilterRecomend",
			"HIDE_NOT_AVAILABLE" => "Y",
			"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
			"IBLOCK_ID" => "5",
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_TYPE_ID" => "product",
			"INCLUDE_SUBSECTIONS" => "Y",
			"LABEL_PROP" => array(
			),
			"LAZY_LOAD" => "Y",
			"LINE_ELEMENT_COUNT" => "3",
			"LOAD_ON_SCROLL" => "Y",
			"MESSAGE_404" => "",
			"MESS_BTN_ADD_TO_BASKET" => "В корзину",
			"MESS_BTN_BUY" => "Купить",
			"MESS_BTN_DETAIL" => "Подробнее",
			"MESS_BTN_SUBSCRIBE" => "Подписаться",
			"MESS_NOT_AVAILABLE" => "Нет в наличии",
			"META_DESCRIPTION" => "-",
			"META_KEYWORDS" => "-",
			"OFFERS_CART_PROPERTIES" => array( 0 =>"SIZE"
			),
			"OFFERS_FIELD_CODE" => array(
				0 => "SIZE"
			),
			"OFFERS_LIMIT" => "",
			"OFFERS_PROPERTY_CODE" => array(
				0 => "SIZE"
			),
			"OFFERS_SORT_FIELD" => "shows",
			"OFFERS_SORT_FIELD2" => "shows",
			"OFFERS_SORT_ORDER" => "asc",
			"OFFERS_SORT_ORDER2" => "asc",
			"OFFER_ADD_PICT_PROP" => "",
			"OFFER_TREE_PROPS" => array(
				0 => "SIZE"
			),
			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "Товары",
			"PAGE_ELEMENT_COUNT" => "12",
			"PARTIAL_PRODUCT_PROPERTIES" => "N",
			"PRICE_CODE" => array("BASE"
			),
			"PRICE_VAT_INCLUDE" => "Y",
			"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
			"PRODUCT_DISPLAY_MODE" => "N",
			"PRODUCT_ID_VARIABLE" => "id",
			"PRODUCT_PROPERTIES" => array(
			),
			"PRODUCT_PROPS_VARIABLE" => "prop",
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
			"PRODUCT_SUBSCRIPTION" => "N",
			"PROPERTY_CODE" => array(
				0 => "STIKERS",
				1 => "",
			),
			"PROPERTY_CODE_MOBILE" => array(
			),
			"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
			"RCM_TYPE" => "personal",
			"SECTION_CODE" => "",
			"SECTION_CODE_PATH" => "",
			"SECTION_ID" => $_REQUEST["SECTION_CODE"],
			"SECTION_ID_VARIABLE" => "SECTION_ID",
			"SECTION_URL" => "/",
			"SECTION_USER_FIELDS" => array(
				0 => "",
				1 => "",
			),
			"SEF_MODE" => "Y",
			"SEF_RULE" => "",
			"SET_BROWSER_TITLE" => "Y",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "Y",
			"SET_META_KEYWORDS" => "Y",
			"SET_STATUS_404" => "Y",
			"SET_TITLE" => "Y",
			"SHOW_404" => "N",
			"SHOW_ALL_WO_SECTION" => "Y",
			"SHOW_CLOSE_POPUP" => "N",
			"SHOW_DISCOUNT_PERCENT" => "Y",
			"SHOW_FROM_SECTION" => "N",
			"SHOW_MAX_QUANTITY" => "N",
			"SHOW_OLD_PRICE" => "Y",
			"SHOW_PRICE_COUNT" => "1",
			"SHOW_SLIDER" => "Y",
			"SLIDER_INTERVAL" => "1",
			"SLIDER_PROGRESS" => "N",
			"TEMPLATE_THEME" => "site",
			"USE_ENHANCED_ECOMMERCE" => "N",
			"USE_MAIN_ELEMENT_SECTION" => "N",
			"USE_PRICE_COUNT" => "N",
			"USE_PRODUCT_QUANTITY" => "Y",
			"MESS_BTN_LAZY_LOAD" => "Показать ещё"
		),
		false
	);?>
	</div>
</div>
 </section> <section class="section-main_recommend" style="padding-top:0px;">
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="section-h2">
				 Популярные товары
			</div>
		</div>
	</div>
	<div class="row">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:sale.bestsellers",
	"main_best",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADDITIONAL_PICT_PROP_13" => "",
		"ADDITIONAL_PICT_PROP_2" => "MORE_PHOTO",
		"ADDITIONAL_PICT_PROP_3" => "MORE_PHOTO",
		"ADDITIONAL_PICT_PROP_4" => "",
		"ADDITIONAL_PICT_PROP_5" => "CERTIF",
		"ADDITIONAL_PICT_PROP_6" => "",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"BY" => "QUANTITY",
		"CACHE_TIME" => "86400",
		"CACHE_TYPE" => "N",
		"CART_PROPERTIES_13" => array(0=>"",1=>"",),
		"CART_PROPERTIES_2" => array(),
		"CART_PROPERTIES_3" => array(0=>"",1=>"",),
		"CART_PROPERTIES_4" => array(),
		"CART_PROPERTIES_5" => array(0=>"",1=>"",),
		"CART_PROPERTIES_6" => array(0=>"SIZE",1=>"",),
		"COMPONENT_TEMPLATE" => "main_best",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_COMPARE" => "N",
		"FILTER" => array(0=>"F",),
		"HIDE_NOT_AVAILABLE" => "Y",
		"LABEL_PROP_13" => "-",
		"LABEL_PROP_2" => "-",
		"LABEL_PROP_4" => "-",
		"LABEL_PROP_5" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"OFFER_TREE_PROPS_3" => "",
		"OFFER_TREE_PROPS_6" => array(0=>"SIZE",),
		"PAGE_ELEMENT_COUNT" => "12",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PERIOD" => "0",
		"PRICE_CODE" => array(0=>"BASE",),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(0=>"",1=>"SIZE",2=>"NEWPRODUCT",3=>"SALELEADER",4=>"SPECIALOFFER",5=>"STIKERS",),
		"PROPERTY_CODE_13" => array(0=>"",1=>"",),
		"PROPERTY_CODE_2" => array(),
		"PROPERTY_CODE_3" => array(0=>"",1=>"",),
		"PROPERTY_CODE_4" => array(),
		"PROPERTY_CODE_5" => array(0=>"",1=>"",),
		"PROPERTY_CODE_6" => array(0=>"ARTNUMBER",1=>"SIZE",2=>"CML2_LINK",3=>"",),
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_IMAGE" => "N",
		"SHOW_NAME" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_PRODUCTS_13" => "N",
		"SHOW_PRODUCTS_2" => "N",
		"SHOW_PRODUCTS_4" => "N",
		"SHOW_PRODUCTS_5" => "Y",
		"TEMPLATE_THEME" => "site",
		"USE_PRODUCT_QUANTITY" => "N"
	)
);?>
	</div>
</div>
 </section> <section class="section-main_benefits">
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="section-h2">
				 Наши преимущества
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-md-3 text-center">
					<div class="benefits__col-wrap">
 <br>
 <img src="/images/section-main_benefits/1.png" class="img-responsive center-block" alt=""> <br>
						<h3>Демократичные цены</h3>
						<p>
							 Вам нужны ингредиенты для приготовления вкусных и изысканных блюд? Наш магазин предлагает только свежие натуральные продукты по доступным ценам.
						</p>
					</div>
				</div>
				<div class="col-md-3 text-center">
					<div class="benefits__col-wrap">
 <br>
 <img src="/images/section-main_benefits/2.png" class="img-responsive center-block" alt=""> <br>
						<h3><a href="/coupons/">Получи скидочный купон</a></h3>
						<p>
							 Подписавшись на наши обновления, вы получите скидочный купон на нашу продукцию! Получите!
						</p>
					</div>
				</div>
				<div class="col-md-3 text-center">
					<div class="benefits__col-wrap">
 <br>
 <img src="/images/section-main_benefits/3.png" class="img-responsive center-block" alt=""> <br>
						<h3>Только качественные продукты</h3>
						<p>
							 Предпочитаете вкусную и здоровую пищу? Мы предлагаем продукты только высшего качества, которые мы тщательно отбираем для вас.
						</p>
					</div>
				</div>
				<div class="col-md-3 text-center">
					<div class="benefits__col-wrap">
 <br>
 <img src="/images/section-main_benefits/4.png" class="img-responsive center-block" alt=""> <br>
						<h3>Экономия времени</h3>
						<p>
							 Предпочитаете вкусную и здоровую пищу? Мы предлагаем продукты только высшего качества, которые мы тщательно отбираем для вас.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
 <br>
 <br>
</div>
 </section> <section class="section-main_cta">
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 text-center">
			<h2>Не пропустите наши специальные предложения</h2>
			<h4>Подпишитесь на еженедельную рассылку - получите бесплатную доставку на первый заказ</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:sender.subscribe",
	"",
	Array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CONFIRMATION" => "Y",
		"SET_TITLE" => "N",
		"SHOW_HIDDEN" => "N",
		"USE_PERSONALIZATION" => "Y"
	)
);?><br>
		</div>
	</div>
</div>
 </section> <?/*?>
<!--<section class="section-main_recipes">
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<h2>Интересные рецепты</h2>
		</div>
	</div>
	 <?
	 $GLOBALS['arrFilter_recept'] = array("PROPERTY_TO_MAIN_VALUE"=>"Y");
	 $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"main_recepts",
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arrFilter_recept",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "9",
		"IBLOCK_TYPE" => "recept",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "PREPARE_TIME",
			1 => "DIFFICULTY",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "main_recepts"
	),
	false
);?>
</div>
 </section>-->
<?*/?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/about-company-txt.php"
	)
);?> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
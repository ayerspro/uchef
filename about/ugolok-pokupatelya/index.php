<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Уголок покупателя | Интернет магазин Шеф Гурме");
$APPLICATION->SetPageProperty("keywords", "уголок покупателя шеф гурме, уголок потребителя шеф гурме");
$APPLICATION->SetPageProperty("description", "Предлагаем ознакомиться с информацией для потребителей и документами интернет-магазина Шеф Гурме");
$APPLICATION->SetTitle("Уголок покупателя");
?><h1 class="h2">Документы магазина</h1>
 <script>
			$(document).ready(function(){
				$(".group1").colorbox({rel:'group1', width:"85%", height:"85%"});
			}
			);
</script>
<br>
<div class="row">
	<div class="col-md-3 col-sm-6">
		<p style="text-align: center">
 <a class="group1 cboxElement" href="/about/ugolok-pokupatelya/img/ogrn.jpg"> <img width="180" src="/upload/medialibrary/16f/16f5fe605d5ddcdf6b6f584a2646a1a7.jpg" height="255" title="ОГРН" alt="ОГРН"> </a>
		</p>
		<p>
			<br>
		</p>
		<p>
		</p>
	</div>
	<div class="col-md-3 col-sm-6">
		<p style="text-align: center">
			<a class="group1 cboxElement" href="/about/ugolok-pokupatelya/img/inn.jpg"> <img width="180" src="/upload/medialibrary/00e/00e6d576b779943989e02076762a0214.jpg" height="255" title="ИНН" alt="ИНН"> </a>
		</p>
	</div>
	<div class="col-md-3 col-sm-6">
		<p style="text-align: center">
			<a href="/about/ugolok-pokupatelya/oferta_p.pdf" target="_blank"> <img width="180" src="/about/ugolok-pokupatelya/img/1doc-1.jpg" height="255" title="Оферта" alt="Оферта"> </a>
		</p>
	</div>
	<div class="col-md-3 col-sm-6">
		<p style="text-align: center">
			<a href="/about/ugolok-pokupatelya/zakon-rf.pdf" target="_blank"> <img width="180" src="/about/ugolok-pokupatelya/img/2doc-1.jpg" height="255" title="Закон о правах потребителей" alt="Закон о правах потребителей"> </a>
		</p>
	</div>
</div>
<br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
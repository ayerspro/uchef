<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "О магазине - информация о компании | Интернет магазин Шеф Гурме");
$APPLICATION->SetPageProperty("keywords", "СитиСервис, Шеф Гурме, юшеф, uchef ru");
$APPLICATION->SetPageProperty("description", "Подробно о нашем интернет-магазине Шеф Гурме: история, миссия, наша команда, вакансии.");
$APPLICATION->SetTitle("О магазине");
?><div class="about_content">
	<div class="about_content_wrap_inner container">
		<div class="row ">
			<div class="col-md-4 about__left-col">
				<div class="about_content-block">
					<h1 class="h2">О нас</h1>
					
					<p>
						<strong>Несколько слов о нашем магазине и наших лучших специалистах</strong>
					</p>
					<p>
						Мы существенно отличаемся от наших конкурентов. Наши принципы – честность и прозрачность, мы делаем всё, чтобы обеспечить лучший сервис для наших клиентов. В нашу команду входят только профессионалы, способные находить самые эффективные решения в области кулинарии.
					</p>
					<p>
						Мы ценим наших клиентов и их предпочтения, включаем в наш ассортимент только самые свежие и качественные продукты, и при этом удерживаем цены на доступном уровне.
					</p>
					<div class="spacer">
					</div>
				</div>
				<div class="about_content-block">
					<h2>Наша цель</h2>
					<div class="content_box text_dropcap ">
						<div class="clear">
						</div>
					</div>
					<!-- .content_box (end) -->
					<div class="extra-wrap">
						<p>
							Наш магазин специализируется на продаже и доставке на дом продуктов питания высокого качества. Мы предлагаем только натуральные продукты в очень широком ассортименте, что предоставляет возможность для приготовления разнообразных вкуснейших блюд.
						</p>
						<p>
							Вся продукция проходит строжайший контроль качества, мы получаем ее напрямую от самых надежных и проверенных поставщиков. Приобретая продукты питания в нашем магазине, вы испытаете настоящее удовольствие от очень вкусной и полезной еды, а если вы не умеете готовить, но хотите угостить роскошным обедом своих друзей, то мы сделаем это для вас, и вам останется только пригласить их к себе в гости.
						</p>
						<p>
							В число наших клиентов входят известные торговые предприятия и рестораны, мы ежедневно отправляем огромное количество продуктов в разные районы Санкт-Петербурга. &nbsp;Мы отбираем для вас всё самое лучшее, вам остается только выбрать нужные продукты и сделать заказ, а мы доставим их вам по указанному адресу.
						</p>
						<p>
							Благодаря нам вы сможете:
						</p>
						<ul>
							<li>Сэкономить свое время – вам не нужно ходить по супермаркетам, сделать заказ вы можете, не выходя из дома;</li>
							<li>Сэкономить деньги – наши цены ниже, чем в других магазинах. Поддерживать их на этом уровне позволяют выгодные условия сотрудничества с нашими партнерами;</li>
							<li>Получить натуральные, вкусные и полезные продукты.</li>
						</ul>
						<p>
							У нас есть всё, что вам нужно! Побалуйте себя вкусными блюдами и деликатесами, вы этого заслуживаете. Вам достаточно сделать заказ и мы оперативно доставим его вам домой.<br>
						</p>
					</div>
					<div class="clear">
					</div>
					<!-- .clear (end) -->
					<div class="spacer">
					</div>
					<!-- .spacer (end) -->
				</div>
				<div class="about_content-block">
					<h2>Наша миссия</h2>
					<p>
						Мы считаем, что хорошие блюда должны быть легко доступными для каждого человека независимо от его статуса, и стремимся предоставить возможность получать их по первому требованию и по невысокой цене.
					</p>
					<div class="spacer">
					</div>
					<!-- .spacer (end) -->
				</div>
			</div>
			<div class="col-md-6 about__right-col">
				<div class="adv_testi">
					<div class="adv_testi_wrap_inner">
						<h2>Наши преимущества</h2>
						<div class="content_box adv_wrapper ">
							<figure class="alignleft aligntextleft "><i class="fa fa-calculator " style="color:#402f2b; font-size:48px; line-height:1.2em;"></i></figure>
							<div class="extra-wrap">
								<h3>Доступные цены</h3>
								<p>
									Мы удерживаем цены на доступном уровне даже в самое трудное время
								</p>
							</div>
							<div class="clear">
							</div>
						</div>
						<!-- .content_box (end) -->
						<div class="content_box adv_wrapper ">
							<figure class="alignleft aligntextleft "><i class="fa fa-thumbs-up " style="color:#402f2b; font-size:48px; line-height:1.2em;"></i></figure>
							<div class="extra-wrap">
								<h3>Продукты без ГМО</h3>
								<p>
									Все продукты в нашем ассортименте проходят жесткий контроль качества и являются не только безопасными, но и полезными.
								</p>
							</div>
							<div class="clear">
							</div>
						</div>
						<!-- .content_box (end) -->
						<div class="content_box adv_wrapper ">
							<figure class="alignleft aligntextleft "><i class="fa fa-star " style="color:#402f2b; font-size:48px; line-height:1.2em;"></i></figure>
							<div class="extra-wrap">
								<h3>Качество</h3>
								<p>
									Мы отбираем для вас продукты самого лучшего качества.
								</p>
							</div>
							<div class="clear">
							</div>
						</div>
						<!-- .content_box (end) -->
						<div class="content_box adv_wrapper ">
							<figure class="alignleft aligntextleft "><i class="fa fa-tag " style="color:#402f2b; font-size:48px; line-height:1.2em;"></i></figure>
							<div class="extra-wrap">
								<h3>Сервис</h3>
								<p>
									Мы ценим ваше время и планы, и оперативно доставляем заказ каждому клиенту.
								</p>
							</div>
							<div class="clear">
							</div>
						</div>
						<!-- .content_box (end) --></div>
				</div>
				<div class="about__reviews-block">
					<h2>Отзывы</h2>
				 <?$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"chef_reviews",
					Array(
						"ACTION_VARIABLE" => "action",
						"ADD_PROPERTIES_TO_BASKET" => "N",
						"ADD_SECTIONS_CHAIN" => "N",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"BASKET_URL" => "/personal/basket.php",
						"BROWSER_TITLE" => "-",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CONVERT_CURRENCY" => "N",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_COMPARE" => "N",
						"DISPLAY_TOP_PAGER" => "N",
						"ELEMENT_SORT_FIELD" => "ID",
						"ELEMENT_SORT_FIELD2" => "",
						"ELEMENT_SORT_ORDER" => "DESC",
						"ELEMENT_SORT_ORDER2" => "",
						"FILTER_NAME" => "",
						"HIDE_NOT_AVAILABLE" => "N",
						"IBLOCK_ID" => "14",
						"IBLOCK_TYPE" => "reviews",
						"INCLUDE_SUBSECTIONS" => "Y",
						"LINE_ELEMENT_COUNT" => "1",
						"META_DESCRIPTION" => "-",
						"META_KEYWORDS" => "-",
						"NEWS_COUNT" => "3",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => "arrows",
						"PAGER_TITLE" => "Отзывы",
						"PARTIAL_PRODUCT_PROPERTIES" => "N",
						"PRICE_CODE" => "",
						"PRICE_VAT_INCLUDE" => "N",
						"PRODUCT_ID_VARIABLE" => "id",
						"PRODUCT_PROPERTIES" => "",
						"PRODUCT_PROPS_VARIABLE" => "prop",
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",
						"PROPERTY_CODE" => array(0=>"",1=>"",),
						"SECTION_CODE" => "",
						"SECTION_ID" => $_REQUEST["SECTION_ID"],
						"SECTION_ID_VARIABLE" => "SECTION_ID",
						"SECTION_URL" => "",
						"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
						"SET_META_DESCRIPTION" => "N",
						"SET_META_KEYWORDS" => "Y",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"SHOW_ALL_WO_SECTION" => "Y",
						"SHOW_PRICE_COUNT" => "1",
						"USE_PRICE_COUNT" => "N",
						"USE_PRODUCT_QUANTITY" => "N"
					)
				);?> <a href="/reviews/" class="btn btn-default-chef">Смотреть все отзывы</a>
				</div>
			</div>
		</div>
		<!-- .row (end) -->
	</div>
</div>
<div class="adv_testi">
	<div class="adv_testi_wrap_inner container">
		<div class="row ">
			<div class="col-md-6">
				
			</div>
			<div class="col-md-6">
				
			</div>
		</div>
		<!-- .row (end) -->
	</div>
</div>
<div class="clear">
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
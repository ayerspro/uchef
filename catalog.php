<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("description", "В нашем интернет-магазине можно купить молочные и кисло-молочные продукты, мясо птицы, кролика, телятину, говядину, баранину, свинину, рыбу, морепродукты, сыр, крупы, макаронные изделия, сладости и другие продукты. Доставка на дом по Петербургу и Ленобласти.");
$APPLICATION->SetPageProperty("keywords", "купить продукты с доставкой на дом, купить продукты в интернет-магазине, купить продукты с доставкой спб и ленобласти");
$APPLICATION->SetPageProperty("title", "Купить мясо, рыбу, колбасы, молочные продукты в каталоге интернет-магазина Шеф Гурме");
$APPLICATION->SetTitle("Каталог");

$APPLICATION->SetPageProperty('BREADCRUMBS_STYLE', ' hide');

$sef_folder = "/";
$sef_url_section = "#SECTION_CODE_PATH#/";
$sef_url_element = "#SECTION_CODE_PATH#/#ELEMENT_CODE#/";
if (isset($_REQUEST['MAINSALES'])) {
	$sef_folder = "/catalog/sales/";
	$sef_url_section = "../#SECTION_CODE#/";
	$sef_url_element = "../#SECTION_CODE#/#ELEMENT_CODE#/";
	$_REQUEST['SALES'] = "Y";
	
	if($_REQUEST['SEC']) {
		
		$sef_folder = "/catalog/sales/" . $_REQUEST['SEC'] . '/';
		$sef_url_element = "../../#SECTION_CODE#/#ELEMENT_CODE#/";
	}
	
	
	$APPLICATION->SetPageProperty("description", "В нашем интернет-магазине можно купить молочные и кисло-молочные продукты, мясо птицы, кролика, телятину, говядину, баранину, свинину, рыбу, морепродукты, сыр, крупы, макаронные изделия, сладости и другие продукты. Доставка на дом по Петербургу и Ленобласти.");
	$APPLICATION->SetPageProperty("keywords", "купить продукты с доставкой на дом, купить продукты в интернет-магазине, купить продукты с доставкой спб и ленобласти");
	$APPLICATION->SetPageProperty("title", "Купить мясо, рыбу, колбасы, молочные продукты в каталоге интернет-магазина Шеф Гурме");
}
if (isset($_REQUEST['MAINCATEGORY'])) {
	$sef_folder = "/catalog/category-".$_REQUEST['MAINCATEGORY']."/";
	$sef_url_section = "../#SECTION_CODE#/";
	$sef_url_element = "../#SECTION_CODE#/#ELEMENT_CODE#/";
	$_REQUEST['CATEGORY'] = $_REQUEST['MAINCATEGORY'];
}

if(isset($_REQUEST['MAINCATEGORY']) || isset($_REQUEST['CATEGORY'])) {
	
	$catId = isset($_REQUEST['MAINCATEGORY']) ? $_REQUEST['MAINCATEGORY'] : $_REQUEST['CATEGORY'];
	CModule::IncludeModule('iblock');

	$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(

		12,
		$catId
	);

	$seoData = $ipropValues->getValues();
	
	if($seoData['ELEMENT_META_DESCRIPTION']) {
		
		$APPLICATION->SetPageProperty("description", $seoData['ELEMENT_META_DESCRIPTION']);
	}
	
	if($seoData['ELEMENT_META_KEYWORDS']) {
		
		$APPLICATION->SetPageProperty("keywords", $seoData['ELEMENT_META_KEYWORDS']);
	}
	
	if($seoData['ELEMENT_META_TITLE']) {
		
		$APPLICATION->SetPageProperty("title", $seoData['ELEMENT_META_TITLE']);
	}
	
	if($seoData['ELEMENT_PAGE_TITLE']) {
		
		$APPLICATION->SetTitle($seoData['ELEMENT_PAGE_TITLE']);
	}
}

?><?$APPLICATION->IncludeComponent(
	"bitrix:catalog", 
	"catalog", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"ALSO_BUY_ELEMENT_COUNT" => "3",
		"ALSO_BUY_MIN_BUYES" => "1",
		"BASKET_URL" => "/cart/",
		"BIG_DATA_RCM_TYPE" => "personal",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COMMON_ADD_TO_BASKET_ACTION" => "",
		"COMMON_SHOW_CLOSE_POPUP" => "Y",
		"COMPATIBLE_MODE" => "Y",
		"COMPONENT_TEMPLATE" => "catalog",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "Y",
		"DETAIL_ADD_TO_BASKET_ACTION" => array(
			0 => "BUY",
		),
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"DETAIL_BLOG_EMAIL_NOTIFY" => "N",
		"DETAIL_BLOG_URL" => "catalog_comments",
		"DETAIL_BLOG_USE" => "Y",
		"DETAIL_BRAND_PROP_CODE" => array(
			0 => "",
			1 => "BRAND_REF",
			2 => "",
		),
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_DISPLAY_NAME" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"DETAIL_FB_USE" => "N",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_TEXT",
			3 => "",
		),
		"DETAIL_OFFERS_PROPERTY_CODE" => array(
			0 => "ARTNUMBER",
			1 => "SIZE",
			2 => "ARTICUL",
			3 => "COLOR_REF",
			4 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "A_BRAND",
			1 => "STIKERS",
			2 => "ARTNUMBER",
			3 => "BGY",
			4 => "COMPOSITION",
			5 => "USE_TO",
			6 => "COUNTRY",
			7 => "STORAGE",
			8 => "A_VID_IKRY",
			9 => "A_HAMONA",
			10 => "A_VES",
			11 => "A_VID_ZERNA",
			12 => "A_VID_KRUPY",
			13 => "A_VID_MAKARON",
			14 => "A_VID_MASLA",
			15 => "A_VID_MOREPRODUKTOV",
			16 => "A_VID_MUKI",
			17 => "A_VID_NAPITKA",
			18 => "A_VID",
			19 => "A_VIDRAZDELKI",
			20 => "A_VID_SOKA",
			21 => "A_VID_SYRA",
			22 => "A_vid_khamona",
			23 => "A_VID_HLEBA",
			24 => "A_VID_ChAYa",
			25 => "A_VID_YAIC",
			26 => "A_VKUS",
			27 => "A_BIDERGKA",
			28 => "A_GAZACIYA",
			29 => "A_GIRNOST",
			30 => "A_KOLPAKETIKOV",
			31 => "A_NAZNACHENIYA",
			32 => "A_NACHINKA",
			33 => "A_NACHINKABLIN",
			34 => "A_NACHINKAVAREN",
			35 => "A_OBRABOTKA",
			36 => "A_OBEM",
			37 => "A_PRODUCT",
			38 => "A_PTICA",
			39 => "A_RAZMER",
			40 => "A_RIBA",
			41 => "A_KOSTOCHKA",
			42 => "A_SAHAR",
			43 => "A_SORT",
			44 => "A_SORTOVOGEY",
			45 => "A_SORTOLIVKI",
			46 => "A_SORTFRUCTOV",
			47 => "A_SORTYAGOD",
			48 => "A_STEPENOBGARKI",
			49 => "A_SIRE",
			50 => "A_TERMOBRABOTKA",
			51 => "A_TIP",
			52 => "A_TIP_KOFE",
			53 => "A_TIPNAREZKI",
			54 => "A_TIPOBRABOTKI",
			55 => "A_TIP_RYBY",
			56 => "A_TIPSTEYKA",
			57 => "A_TIP_ChAYa",
			58 => "A_UPAKOVKA",
			59 => "A_CVET",
			60 => "A_CVET_KRUPY",
			61 => "A_CHASTITUSHI",
			62 => "MANUFACTURER",
			63 => "TARE",
			64 => "NEWPRODUCT",
			65 => "MATERIAL",
			66 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "Y",
		"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
		"DETAIL_SHOW_BASIS_PRICE" => "Y",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"DETAIL_USE_COMMENTS" => "Y",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DETAIL_VK_USE" => "N",
		"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "desc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FIELDS" => array(
			0 => "SCHEDULE",
			1 => "STORE",
			2 => "",
		),
		"FILE_404" => "",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arrPropFilter",
		"FILTER_OFFERS_FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"FILTER_OFFERS_PROPERTY_CODE" => array(
			0 => "SIZE",
			1 => "",
		),
		"FILTER_PRICE_CODE" => array(
			0 => "BASE",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_VIEW_MODE" => "VERTICAL",
		"FORUM_ID" => "1",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
		"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"INSTANT_RELOAD" => "N",
		"LABEL_PROP" => "GURMAN",
		"LINE_ELEMENT_COUNT" => "5",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "5",
		"LINK_IBLOCK_TYPE" => "catalog",
		"LINK_PROPERTY_SID" => "RECEPT",
		"LIST_BROWSER_TITLE" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_META_KEYWORDS" => "-",
		"LIST_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"LIST_OFFERS_LIMIT" => "0",
		"LIST_OFFERS_PROPERTY_CODE" => array(
			0 => "ARTNUMBER",
			1 => "SIZE",
			2 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "STIKERS",
			1 => "SIZE",
			2 => "NEWPRODUCT",
			3 => "SALELEADER",
			4 => "SPECIALOFFER",
			5 => "",
		),
		"MAIN_TITLE" => "Наличие на складах",
		"MESSAGES_PER_PAGE" => "10",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "В корзину",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MIN_AMOUNT" => "10",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "desc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
			0 => "SIZE",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "round",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"QUANTITY_FLOAT" => "N",
		"REVIEW_AJAX_POST" => "Y",
		"SECTIONS_HIDE_SECTION_NAME" => "N",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"SECTIONS_VIEW_MODE" => "TILE",
		"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_TOP_DEPTH" => "1",
		"SEF_FOLDER" => $sef_folder,
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_EMPTY_STORE" => "Y",
		"SHOW_GENERAL_STORE_INFORMATION" => "N",
		"SHOW_LINK_TO_FORUM" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_TOP_ELEMENTS" => "N",
		"SIDEBAR_DETAIL_SHOW" => "N",
		"SIDEBAR_PATH" => "/catalog/sidebar.php",
		"SIDEBAR_SECTION_SHOW" => "Y",
		"STORES" => array(
			0 => "",
			1 => "",
		),
		"STORE_PATH" => "/store/#store_id#",
		"TEMPLATE_THEME" => "site",
		"TOP_ADD_TO_BASKET_ACTION" => "ADD",
		"URL_TEMPLATES_READ" => "",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"USE_ALSO_BUY" => "Y",
		"USE_BIG_DATA" => "Y",
		"USE_CAPTCHA" => "Y",
		"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
		"USE_COMPARE" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"USE_GIFTS_DETAIL" => "Y",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
		"USE_GIFTS_SECTION" => "Y",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_MIN_AMOUNT" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"USE_REVIEW" => "Y",
		"USE_SALE_BESTSELLERS" => "Y",
		"USE_STORE" => "Y",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => $sef_url_section,
			"element" => $sef_url_element,
			"compare" => "compare/",
			"smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
		)
	),
	false
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:asd.favorite.likes",
	"",
	Array(
		"ALLOW_MOVED" => "Y",
		"FAV_TYPE" => "product",
		"FOLDER_ID" => "",
		"NOT_SHOW_WITH_NOT_FOLDER" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGE_COUNT" => "10",
		"PREVIEW_HEIGHT" => "50",
		"PREVIEW_WIDTH" => "50",
		"USER_ID" => ""
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'N'
)
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
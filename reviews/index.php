<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "отзывы сити сервис, отзывы шеф гурме");
$APPLICATION->SetPageProperty("title", "Отзывы клиентов | Интернет магазин Шеф Гурме");
$APPLICATION->SetPageProperty("keywords", "отзывы сити сервис, отзывы uchef ru");
$APPLICATION->SetPageProperty("description", "Отзывы клиентов о качестве обслуживания в интернет магазине Шеф Гурме");
$APPLICATION->SetTitle("Отзывы клиентов");
?> 

<div class="container">
	<h1 class="text-center catalog-title">Отзывы клиентов</h1>
	<div class="reviews-container">

	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"chef_reviews", 
	array(
		"IBLOCK_TYPE" => "reviews",
		"IBLOCK_ID" => "14",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
		"ELEMENT_SORT_ORDER" => "DESC",
		"ELEMENT_SORT_FIELD2" => "",
		"ELEMENT_SORT_ORDER2" => "",
		"FILTER_NAME" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "10",
		"LINE_ELEMENT_COUNT" => "2",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_LIMIT" => "5",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => "",
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "N",
		"PAGER_TEMPLATE" => "arrows",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Отзывы",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => "",
		"COMPONENT_TEMPLATE" => "chef_reviews",
		"NEWS_COUNT" => "200",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"STRICT_SECTION_CHECK" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?> 
	</div>
	<div class="review-form-container">
		<h2>Оставьте свой отзыв</h2>
			<?$APPLICATION->IncludeComponent(
				"devstrong:reviews", 
				"reviews", 
				array(
					"COMPONENT_ID" => "ds-reviews",
					"FIELDS" => array(
						0 => "DSCB_NAME",
						1 => "DSCB_COMMENT",
					),
					"INCLUDE_INPUTMASK" => "N",
					"INCLUDE_MAGNIFIC" => "N",
					"REQUIRED_FIELDS" => array(
						0 => "DSCB_NAME",
						1 => "DSCB_COMMENT",
					),
					"SUCCESS_MESS" => "Спасибо! Ваш отзыв будет опубликован после модерации.",
					"COMPONENT_TEMPLATE" => "reviews",
					"IBLOCK_ID" => "14",
					"USE_RECAPTCHA" => "N",
					"RECAPTCHA_SECRET" => "6Lcrz24UAAAAAEbMp9ghga43PHX9opbl6nm8-S2p"
				),
				false
			);?>
		 <?/*$APPLICATION->IncludeComponent(
			"devstrong:iblock.element.add.form",
			".default",
			Array(
				"IBLOCK_TYPE" => "reviews",
				"IBLOCK_ID" => "14",
				"STATUS_NEW" => "NEW",
				"LIST_URL" => "",
				"USE_CAPTCHA" => "Y",
				"USER_MESSAGE_EDIT" => "",
				"USER_MESSAGE_ADD" => "Спасибо. Ваш отзыв будет опубликован после модерации.",
				"DEFAULT_INPUT_SIZE" => "30",
				"RESIZE_IMAGES" => "N",
				"PROPERTY_CODES" => array(0=>"NAME",1=>"DETAIL_TEXT",),
				"PROPERTY_CODES_REQUIRED" => array(0=>"NAME",1=>"DETAIL_TEXT",),
				"GROUPS" => array(0=>"2",),
				"STATUS" => "ANY",
				"ELEMENT_ASSOC" => "CREATED_BY",
				"MAX_USER_ENTRIES" => "100000",
				"MAX_LEVELS" => "100000",
				"LEVEL_LAST" => "Y",
				"MAX_FILE_SIZE" => "0",
				"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
				"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
				"SEF_MODE" => "N",
				"CUSTOM_TITLE_NAME" => "Ваше имя",
				"CUSTOM_TITLE_TAGS" => "",
				"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
				"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
				"CUSTOM_TITLE_IBLOCK_SECTION" => "",
				"CUSTOM_TITLE_PREVIEW_TEXT" => "",
				"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
				"CUSTOM_TITLE_DETAIL_TEXT" => "Ваш отзыв",
				"CUSTOM_TITLE_DETAIL_PICTURE" => ""
			)
		);*/?>
	</div>
</div>

 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "Шеф Гурме, интернет магазин, продукты на дом, доставка деликатесов");
$APPLICATION->SetPageProperty("keywords_inner", "Шеф Гурме, интернет магазин, продукты на дом, доставка деликатесов");
$APPLICATION->SetPageProperty("title", "Шеф Гурме, интернет магазин, продукты на дом, доставка деликатесов");
$APPLICATION->SetPageProperty("keywords", "Шеф Гурме, интернет магазин, продукты на дом, доставка деликатесов");
$APPLICATION->SetPageProperty("description", "Интернет магазин uchef.ru Шеф Гурме");

if (is_string($_REQUEST["backurl"]) && strpos($_REQUEST["backurl"], "/") === 0)
{
	LocalRedirect($_REQUEST["backurl"]);
}

$APPLICATION->SetTitle("Вход на сайт");
?><p class="notetext">Вы зарегистрированы и успешно авторизовались.</p>

<p><a href="/">Вернуться на главную страницу</a></p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
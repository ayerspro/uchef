<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");

if ($_GET["sort"] == "price")
{
	$arParams["ELEMENT_SORT_FIELD"] = "catalog_PRICE_1";
}
if ($_GET["order"] == "vozvr") 
	$arParams["ELEMENT_SORT_ORDER"]= "asc";
if ($_GET["order"] == "ubiv") 
	$arParams["ELEMENT_SORT_ORDER"]= "desc";
if ($_GET["sort"] == "name"){
	$arParams["ELEMENT_SORT_FIELD"] = "NAME";
}
$current_sort_type = $_GET['sort'];
$current_sort_order = $_GET['order'];

unset($_SESSION["CHECKLIST_PRODUCT_MAIN"]);

?>
<section style="padding: 0 0 15px; text-align: center;">
	<h1 class="text-center catalog-title">Избранное</h1>
</section>
<div class="container-fluid">
	<div class="flexible pull-left sort-line">
		<div class="select-wrapp">
			<div class="dropdown">
				Сортировать по 
			  <a data-target="#sort-dropdown" href="#sort-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			    <? 
			    	if($current_sort_type == NULL)
			    		echo "Дате";
			    	else 
			    		echo ($current_sort_type == "price")? "Цене " . (($current_sort_order == "ubiv")? "(убыв.)" : "(возвр.)") : "Наименованию " . (($current_sort_order == "ubiv")? "(убыв.)" : "(возвр.)");
			    ?>
			  </a>
			  <span class="caret"></span>

			  <ul class="dropdown-menu" id="sort-dropdown">
			  	<li><a href="?">Дате</a></li>
			    <li><a href="?sort=price&order=ubiv">Цене (убыв.)</a></li>
			    <li><a href="?sort=price&order=vozvr">Цене (возвр.)</a></li>
			    <li><a href="?sort=name&order=ubiv">Наименованию (убыв.)</a></li>
			    <li><a href="?sort=name&order=vozvr">Наименованию (возвр.)</a></li>
			  </ul>
			</div>
		</div>
		<div class="view">
			<a href="?view=block"><i class="fa fa-lg fa-th-large" aria-hidden="true"></i></a>
			<a href="?view=list"><i class="fa fa-lg fa-th-list" aria-hidden="true"></i></a>
		</div>
	</div>
	<div class="panel-pagination pull-right">
		<?$APPLICATION->ShowViewContent('pagination_section');?>
	</div>
</div>
<?if(isset($_REQUEST['show'])) $arParams["PAGE_ELEMENT_COUNT"] = $_REQUEST['show'];?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"favorite", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "round",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"PROPERTY_CODE" => array(
			0 => "SIZE",
			1 => "NEWPRODUCT",
			2 => "SALELEADER",
			3 => "SPECIALOFFER",
			4 => "",
		),
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_CODE_PATH" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SEF_RULE" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "green",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "favorite",
		"CUSTOM_FILTER" => "",
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PRODUCT_DISPLAY_MODE" => "N",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"MESS_BTN_COMPARE" => "Сравнить",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
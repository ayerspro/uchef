<?php

namespace Seoexpert\Redirects2;

use CSite;
use Bitrix\Main\Type\DateTime;

class Import
{
    const MODE_FULL = 1;
    const MODE_UPDATE = 2;

    const DELIMITER_DC = ';';
    const DELIMITER_C = ',';

    const TYPE_ERROR = 'error';
    const TYPE_SUCCESS = 'success';
    const TYPE_PROGRESS = 'progress';

    const TABLE_NAME = 'se_redirect';

    public static function init($fileName, $mode, $delimiter)
    {
        $_SESSION['import'] = array(
            'file'     => $fileName,
            'pos'      => 1,
            'percent'  => 0,
            'count'    => 0,
            'state'    => self::TYPE_PROGRESS,
            'messages' => array(),
            'mode'     => $mode,
            'delimiter' => $delimiter,
            'tmp_id'   => uniqid()
        );
    }

    public static function step()
    {
        $config = $_SESSION['import'];

        $tmpId = $config['tmp_id'];

        if (!file_exists($config['file'])) {
            return self::updateState(0, 0, self::TYPE_ERROR, '���� �� ������.');
        }

        //�������� �����������
        if ($config['delimiter'] != self::DELIMITER_DC || $config['delimiter'] != self::DELIMITER_C) {
            $delimiter = self::DELIMITER_DC;
        } else {
            $delimiter = $config['delimiter'];
        }

        $counter = 0;
        $handle = fopen($config['file'], "r");
        $file_array =  file ($config['file']);
        $num_str =  count($file_array); //���������� ����� � �����

        while (($data = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
            self::saveState($config);
            if ($counter == 10) {
                return self::updateState($counter, $num_str, self::TYPE_PROGRESS);
            }
            if ($data[0] && $data[1]) {
                if (stristr($data[0], '://')) {
                    $arData = explode('/', $data[0]);
                    $arDataLen = count($arData);
                    $newData = '/';
                    for ($i = 3; $i < $arDataLen; $i++) {
                        if ($arData[$i] != '') {
                            $newData .= $arData[$i] . '/';
                        }
                    }
                    $data[0] = $newData;
                }
                if (stristr($data[1], '://')) {
                    $arData = explode('/', $data[1]);
                    $arDataLen = count($arData);
                    $newData = '/';
                    for ($i = 3; $i < $arDataLen; $i++) {
                        if ($arData[$i] != '') {
                            $newData .= $arData[$i] . '/';
                        }
                    }
                    $data[1] = $newData;
                }
                $redirectId = self::getRedirect(htmlspecialchars($data[0]));
                if (!$redirectId) {
                    self::addRedirect(htmlspecialchars($data[0]), htmlspecialchars($data[1]));
                } elseif ($redirectId && $config['mode'] == self::MODE_FULL) {
                    self::updateRedirect($redirectId, htmlspecialchars($data[1]));
                }
            }
            $counter++;
        }
        fclose($handle);

        $config['state'] = self::TYPE_SUCCESS;

        return self::updateState($counter, $num_str, self::TYPE_SUCCESS);
    }

    private static function updateState($pos, $count, $state, $message = '')
    {
        $config = $_SESSION['import'];
        $config['pos'] = $pos;
        $config['count'] = $count;
        $config['percent'] = round(((float)$pos / (float)$count) * 100);
        $config['state'] = $state;
        $config['message'] = $message;
        $_SESSION['import'] = $config;
        if ($state == self::TYPE_ERROR) {
            unlink($config['file']);
        }

        return $config;
    }

    private static function saveState($config)
    {
        $_SESSION['import'] = $config;
    }

    private static function getRedirect($source) {
        global $DB;
        $err_mess = "<br>Function: GetByID<br>Line: ";
        $strSql = 'SELECT ID from ' . self::TABLE_NAME . ' WHERE SOURCE = \'' . $source . '\'';
        $res = $DB->Query($strSql, false, $err_mess.__LINE__);
        if ($row = $res->Fetch()) {
            return $row["ID"];
        }
        return false;
    }

    private static function updateRedirect($id, $address) {
        global $DB;
        $err_mess = "<br>Function: GetByID<br>Line: ";
        $strSql = 'UPDATE ' . self::TABLE_NAME . ' SET ACTIVE=\'Y\', REDIRECT=\'' . $address . '\' WHERE ID=' . $id;
        $res = $DB->Query($strSql, false, $err_mess.__LINE__);
    }

    private static function addRedirect($source, $redirect) {
        global $USER;
        $arSite = CSite::GetList()->GetNext();
        $hash = md5($redirect . $source);
        $arData = array(
            'CREATED'     => 'NOW()',
            'CHANGED'     => 'NOW()',
            'IBLOCK_ID'   => 0,
            'TYPE'        => '\'url\'',
            'SOURCE'      => '\'' . $source . '\'',
            'REDIRECT'    => '\'' . $redirect . '\'',
            'ACTIVE'      => '\'Y\'',
            'STATUS_CODE' => '\'301\'',
            'HASH'        => '\'' . $hash . '\'',
            'USER_ID'     => $USER->GetID(),
            'SITE_ID'     => '\'' . $arSite['ID'] . '\''
        );
        global $DB;
        $arValuesKeys = $arValues = '';
        $noFirst = true;
        foreach ($arData as $key => $item) {
            if ($noFirst) {
                $arValuesKeys .= $key;
                $arValues .= $item;
                $noFirst = false;
            } else {
                $arValuesKeys .= ', ' . $key;
                $arValues .= ', ' . $item;
            }
        }
        $err_mess = "<br>Function: GetByID<br>Line: ";
        $strSql = 'INSERT INTO ' . self::TABLE_NAME . ' (' . $arValuesKeys . ') VALUES (' . $arValues . ')';
        $res = $DB->Query($strSql, false, $err_mess.__LINE__);
    }
}
<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
namespace Seoexpert\Redirects2;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\Result;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use CIBlock;
use CSite;
use Bitrix\Main\Type\DateTime;


Loc::loadMessages(__FILE__);

/**
 * Class RedirectTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> CREATED datetime mandatory
 * <li> CHANGED datetime mandatory
 * <li> USER_ID int mandatory
 * <li> HASH string(64) mandatory
 * <li> ACTIVE unknown mandatory
 * <li> TYPE string(64) mandatory
 * <li> SOURCE string(255) mandatory
 * <li> SOURCE_OPTIONS string optional
 * <li> REDIRECT string(255) optional
 * <li> REDIRECT_OPTIONS string optional
 * <li> STATUS_CODE int mandatory
 * <li> COUNTER int optional
 * </ul>
 *
 * @package Bitrix\Redirect
 **/
class RedirectTable extends Entity\DataManager
{

    const TYPE_URL = 'url';
    const TYPE_REGEXP = 'regexp';
    const TYPE_IBLOCK_SECTION = 'iblock_section';
    const TYPE_IBLOCK_ELEMENT = 'iblock_element';

    const STATUS_CODE_301 = 301;
    const STATUS_CODE_404 = 404;

    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'se_redirect';
    }

    public static function getMap()
    {
        return array(
            'ID'               => array(
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
                'title'        => Loc::getMessage('REDIRECT_ENTITY_ID_FIELD'),
                'def'          => 'Y'
            ),
            'CREATED'          => array(
                'data_type' => 'datetime',
                'required'  => true,
                'title'     => Loc::getMessage('REDIRECT_ENTITY_CREATED_FIELD'),
                'def'       => 'Y',
                'hide'      => true
            ),
            'CHANGED'          => array(
                'data_type' => 'datetime',
                'required'  => true,
                'title'     => Loc::getMessage('REDIRECT_ENTITY_CHANGED_FIELD'),
                'hide'      => true
            ),
            'USER_ID'          => array(
                'data_type' => 'integer',
                'required'  => true,
                'title'     => Loc::getMessage('REDIRECT_ENTITY_USER_ID_FIELD'),
                'hide'      => true
            ),
            'HASH'             => array(
                'data_type'  => 'string',
                'validation' => array(__CLASS__, 'validateHash'),
                'title'      => Loc::getMessage('REDIRECT_ENTITY_HASH_FIELD'),
                'hide'       => true
            ),
            'ACTIVE'           => array(
                'data_type' => 'boolean',
                'values'    => array('N', 'Y'),
                'title'     => Loc::getMessage('REDIRECT_ENTITY_ACTIVE_FIELD'),
                'def'       => 'Y'
            ),
            'SITE_ID'          => array(
                'data_type' => 'string',
                'values'    => self::getListSites(),
                'title'     => Loc::getMessage('REDIRECT_ENTITY_SITE_ID_FIELD'),
                'def'       => 'Y'
            ),
            'TYPE'             => array(
                'data_type'  => 'string',
                'required'   => true,
                'validation' => array(__CLASS__, 'validateType'),
                'values'     => self::getListTypes(),
                'title'      => Loc::getMessage('REDIRECT_ENTITY_TYPE_FIELD'),
                'def'        => 'Y'
            ),
            'IBLOCK_ID'        => array(
                'data_type' => 'integer',
                'values'    => self::getListIblocks(),
                'title'     => Loc::getMessage('REDIRECT_ENTITY_IBLOCK_ID_FIELD')
            ),
            'SOURCE'           => array(
                'data_type'  => 'string',
                'required'   => true,
                'validation' => array(__CLASS__, 'validateSource'),
                'title'      => Loc::getMessage('REDIRECT_ENTITY_SOURCE_FIELD'),
                'def'        => 'Y',
                'size'       => 100
            ),
            'SOURCE_OPTIONS'   => array(
                'data_type' => 'text',
                'title'     => Loc::getMessage('REDIRECT_ENTITY_SOURCE_OPTIONS_FIELD'),
            ),
            'REDIRECT'         => array(
                'data_type'  => 'string',
                'validation' => array(__CLASS__, 'validateRedirect'),
                'title'      => Loc::getMessage('REDIRECT_ENTITY_REDIRECT_FIELD'),
                'def'        => 'Y',
                'size'       => 100
            ),
            'REDIRECT_OPTIONS' => array(
                'data_type' => 'text',
                'title'     => Loc::getMessage('REDIRECT_ENTITY_REDIRECT_OPTIONS_FIELD'),
            ),
            'STATUS_CODE'      => array(
                'data_type' => 'integer',
                'required'  => true,
                'title'     => Loc::getMessage('REDIRECT_ENTITY_STATUS_CODE_FIELD'),
                'def'       => 'Y',
                'values'    => self::getListStatusCodes(),
            ),
            'COUNTER'          => array(
                'data_type' => 'integer',
                'title'     => Loc::getMessage('REDIRECT_ENTITY_COUNTER_FIELD'),
                'hide'      => true,
                'def'       => 'Y'
            ),
        );
    }

    public static function validateHash()
    {
        return array(
            new Entity\Validator\Length(null, 64),
        );
    }

    public static function validateType()
    {
        return array(
            new Entity\Validator\Length(null, 64),
        );
    }

    public static function validateSource()
    {
        return array(
            new Entity\Validator\Length(null, 255),
        );
    }

    public static function validateRedirect()
    {
        return array(
            new Entity\Validator\Length(null, 255),
        );
    }

    public static function checkFields(Result $result, $primary, array $data)
    {
        parent::checkFields($result, $primary, $data);
        if (($data['TYPE'] == 'iblock_element' || $data['TYPE'] == 'iblock_section') && !$data['IBLOCK_ID']) {
            $field = static::getEntity()->getField('IBLOCK_ID');
            $result->addError(
                new Entity\FieldError($field, Loc::getMessage("MAIN_ENTITY_FIELD_REQUIRED", array("#FIELD#" => $field->getTitle())),
                    Entity\FieldError::EMPTY_REQUIRED));
        }
    }


    public static function getListTypes()
    {
        return array(
            self::TYPE_URL            => Loc::getMessage('REDIRECT_TYPE_URL'),
            self::TYPE_REGEXP         => Loc::getMessage('REDIRECT_TYPE_REGEXP'),
            self::TYPE_IBLOCK_SECTION => Loc::getMessage('REDIRECT_TYPE_IBLOCK_SECTION'),
            self::TYPE_IBLOCK_ELEMENT => Loc::getMessage('REDIRECT_TYPE_IBLOCK_ELEMENT'),
        );
    }

    public static function getListStatusCodes()
    {
        return array(
            self::STATUS_CODE_301 => 301
        );
    }

    public static function getListIblocks()
    {
        Loader::includeModule('iblock');
        $res = CIBlock::GetList(array('SORT' => 'ASC', 'NAME' => 'ASC'));
        $result = array(0 => '');
        while ($arIblock = $res->GetNext()) {
            $result[$arIblock['ID']] = $arIblock['NAME'];
        }

        return $result;
    }

    public static function getListSites()
    {
        $result = array();
        $res = CSite::GetList($by, $sort);
        while ($arSite = $res->Fetch()) {
            $result[$arSite['ID']] = $arSite['NAME'];
        }

        return $result;
    }

    public static function addRule($type, $source, $redirect, $status = RedirectTable::STATUS_CODE_301, $iblockId = false)
    {
        global $USER;

        $hash = md5($redirect . $source);

        $res = self::getList(array('filter' => array('HASH' => $hash)));
        if ($res->fetch()) {
            return;
        }

        $arSite = CSite::GetList($by, $order)->GetNext();

        RedirectTable::add(array(
                'CREATED'     => new DateTime(date('Y-m-d H:i:s'), 'Y-m-d H:i:s'),
                'CHANGED'     => new DateTime(date('Y-m-d H:i:s'), 'Y-m-d H:i:s'),
                'IBLOCK_ID'   => $iblockId,
                'TYPE'        => $type,
                'SOURCE'      => $source,
                'REDIRECT'    => $redirect,
                'ACTIVE'      => 'Y',
                'STATUS_CODE' => $status,
                'HASH'        => $hash,
                'USER_ID'     => $USER->GetID(),
                'SITE_ID'     => $arSite['ID']
            )
        );
    }

    public static function updateStat($ruleId)
    {
        if ($ruleId) {
            $rule = self::getById($ruleId)->fetch();
            self::update($ruleId, array('COUNTER' => $rule['COUNTER'] + 1));
        }
    }
}

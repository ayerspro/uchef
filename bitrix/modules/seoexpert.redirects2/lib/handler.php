<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace Seoexpert\Redirects2;

use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use CIBlock;
use CIBlockElement;
use CIBlockSection;
use COption;

/**
 * Class Handler
 * @package Seoexpert\Redirect
 */
class Handler
{
    const MODULE_ID = 'seoexpert.redirects2';

    /**
     * ����������� ������������
     */
    public static function onPageStart()
    {
        if (!defined('ADMIN_SECTION')) {
            if (COption::GetOptionString(self::MODULE_ID, 'active') == 'Y') {
                self::handleAddress();
            }
        }
        if (COption::GetOptionString(self::MODULE_ID, 'spy_iblock') == 'Y') {
            EventManager::getInstance()->registerEventHandler('iblock', 'OnBeforeIBlockUpdate', self::MODULE_ID, 'Seoexpert\Redirects2\Handler', 'onIBlockUpdate');
        } else {
            EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnBeforeIBlockUpdate', self::MODULE_ID, 'Seoexpert\Redirects2\Handler', 'onIBlockUpdate');
        }
        if (COption::GetOptionString(self::MODULE_ID, 'spy_iblock_section') == 'Y') {
            EventManager::getInstance()->registerEventHandler('iblock', 'OnBeforeIBlockSectionUpdate', self::MODULE_ID, 'Seoexpert\Redirects2\Handler', 'onIBlockSectionUpdate');
        } else {
            EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnBeforeIBlockSectionUpdate', self::MODULE_ID, 'Seoexpert\Redirects2\Handler', 'onIBlockSectionUpdate');
        }
        if (COption::GetOptionString(self::MODULE_ID, 'spy_iblock_element') == 'Y') {
            EventManager::getInstance()->registerEventHandler('iblock', 'OnBeforeIBlockElementUpdate', self::MODULE_ID, 'Seoexpert\Redirects2\Handler', 'onIBlockElementUpdate');
        } else {
            EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementUpdate', self::MODULE_ID, 'Seoexpert\Redirects2\Handler', 'onIBlockElementUpdate');
        }
    }

    /**
     * ������ url
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    private static function handleAddress()
    {

        $host = $_SERVER['HTTP_HOST'];
        $url = urldecode(self::htmlEncode($_SERVER['REQUEST_URI']));

        //��������� ������ ������������� http/https
        if (COption::GetOptionString(self::MODULE_ID, 'https') == 'Y' && $_SERVER["REQUEST_SCHEME"] != 'https') {
            self::redirect('http://'.$host.$_SERVER['REQUEST_URI'], 'https://'.$host.$_SERVER['REQUEST_URI'], 301, false);
        }

        //��������� ������ ������������� www/no www
        if (COption::GetOptionString(self::MODULE_ID, 'www') == 'www') {
            if (strpos($host, 'www.') !== 0) {
                self::redirect('http://'.$host.$_SERVER['REQUEST_URI'], 'http://www.'.$host.$_SERVER['REQUEST_URI'], 301, false);
            }
        } elseif (COption::GetOptionString(self::MODULE_ID, 'www') == 'no_www') {
            if (strpos($host, 'www.') === 0) {
                self::redirect('http://'.$host.$_SERVER['REQUEST_URI'], 'http://'.substr($host, 4).$_SERVER['REQUEST_URI'], 301, false);
            }
        }

        //��������� �������� ������� ����� url
        $arFilter = array('SOURCE' => $url, 'ACTIVE' => 'Y', 'SITE_ID' => SITE_ID);
        $rsData = RedirectTable::getList(array('filter' => $arFilter));
        if ($arData = $rsData->fetch()) {
            self::redirect($url, $arData['REDIRECT'], $arData['STATUS_CODE'], $arData['ID']);
        }

        //��������� ������� � �������������� ����������� ���������
        $arFilter = array('TYPE' => RedirectTable::TYPE_REGEXP, 'ACTIVE' => 'Y', 'SITE_ID' => SITE_ID);
        $rsData = RedirectTable::getList(array('filter' => $arFilter));
        while ($arData = $rsData->fetch()) {
            $rule = str_replace('/', '\/', trim($arData['SOURCE']));
            if (preg_match('/^'.$rule.'$/', $url, $matches)) {
                $params = array();
                foreach ($matches as $id => $value) {
                    $params['$'.$id] = $value;
                }
                self::redirect($url, strtr($arData['REDIRECT'], $params), $arData['STATUS_CODE'], $arData['ID']);
            }
        }

        //��������� ������ ������������� ��� �������� ���������
        $arFilter = array('TYPE' => RedirectTable::TYPE_IBLOCK_SECTION, 'ACTIVE' => 'Y', 'SITE_ID' => SITE_ID);
        $rsData = RedirectTable::getList(array('filter' => $arFilter));
        while ($arData = $rsData->fetch()) {
            Loader::includeModule('iblock');
            $codes = array(
                '#SITE_DIR#'          => '',
                '#ID#'                => '(?<id>.*?)',
                '#SECTION_ID#'        => '(?<id>.*?)',
                '#CODE#'              => '(?<code>.*?)',
                '#SECTION_CODE#'      => '(?<code>.*?)',
                '#SECTION_CODE_PATH#' => '(?<section_code_path>.*?)',
            );

            $rule = strtr($arData['SOURCE'], $codes);
            $rule = str_replace('/', '\/', $rule);

            if (preg_match('/^'.$rule.'$/', $url, $matches)) {
                $arFilter = array();
                if (isset($matches['code'])) {
                    $arFilter['CODE'] = $matches['code'];
                } elseif (isset($matches['id'])) {
                    $arFilter['ID'] = $matches['id'];
                } elseif (isset($matches['section_code_path'])) {
                    if ($sectionId = self::getSectionByPath($arData['IBLOCK_ID'], $matches['section_code_path'])) {
                        $arFilter['ID'] = $sectionId;
                    }
                }
                if (count($arFilter)) {
                    if ($arSection = CIBlockSection::GetList(array(), array_merge(array('IBLOCK_ID' => $arData['IBLOCK_ID']), $arFilter))->GetNext()) {

                        self::redirect($url, $arSection['SECTION_PAGE_URL'], $arData['STATUS_CODE'], $arData['ID']);
                    }
                }
            }
        }

        //��������� ������ ��� ��������� ���������
        $arFilter = array('TYPE' => RedirectTable::TYPE_IBLOCK_ELEMENT, 'ACTIVE' => 'Y', 'SITE_ID' => SITE_ID);
        $rsData = RedirectTable::getList(array('filter' => $arFilter));
        while ($arData = $rsData->fetch()) {
            Loader::includeModule('iblock');
            $codes = array(
                '#SITE_DIR#'          => '',
                '#ID#'                => '(?<id>.*?)',
                '#SECTION_ID#'        => '(?<section_id>.*?)',
                '#ELEMENT_CODE#'      => '(?<code>.*?)',
                '#CODE#'              => '(?<code>.*?)',
                '#SECTION_CODE#'      => '(?<section_code>.*?)',
                '#SECTION_CODE_PATH#' => '(?<section_code_path>.*?)',
            );

            $rule = strtr($arData['SOURCE'], $codes);
            $rule = str_replace('/', '\/', $rule);

            if (preg_match('/^'.$rule.'$/', $url, $matches)) {
                $arFilter = array();
                if (isset($matches['code'])) {
                    $arFilter['CODE'] = $matches['code'];
                } elseif (isset($matches['id'])) {
                    $arFilter['ID'] = $matches['id'];
                }
                if (count($arFilter)) {
                    if (isset($matches['section_code'])) {
                        $arFilter['SECTION_CODE'] = $matches['section_code'];
                    } elseif (isset($matches['section_id'])) {
                        $arFilter['SECTION_ID'] = $matches['section_id'];
                    } elseif (isset($matches['section_code_path'])) {
                        if ($sectionId = self::getSectionByPath($arData['IBLOCK_ID'], $matches['section_code_path'])) {
                            $arFilter['SECTION_ID'] = $sectionId;
                        }
                    }
                    if ($arElement = CIBlockElement::GetList(array(), array_merge(array('IBLOCK_ID' => $arData['IBLOCK_ID']), $arFilter))->GetNext()) {
                        self::redirect($url, $arElement['DETAIL_PAGE_URL'], $arData['STATUS_CODE'], $arData['ID']);
                    }
                }
            }
        }
    }

    /**
     * ��������
     * @param string $url ������� URL
     * @param string $redirect ����� URL
     * @param integer $status ���
     * @param integer $ruleId ����� �������
     */
    private static function redirect($url, $redirect, $status, $ruleId)
    {
        if ($url != $redirect) {

            RedirectTable::updateStat($ruleId);


            switch ($status) {
                case 301:
                    header("HTTP/1.1 301 Moved Permanently");
                    break;
            }
            header("Location: ".$redirect);
            exit();
        }
    }

    private static function getSectionByPath($iblockId, $sectionPath)
    {
        $codes = explode('/', $sectionPath);
        $parentId = 0;
        foreach ($codes as $code) {
            if ($arSection = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $iblockId, 'CODE' => $code, 'SECTION_ID' => $parentId))->Fetch()) {
                $parentId = $arSection['ID'];
            } else {
                return false;
            }
        }

        return $parentId;
    }

    public static function onIBlockUpdate(&$arFields)
    {
        $arIBlock = CIBlock::GetByID($arFields['ID'])->Fetch();
        if ($arFields['SECTION_PAGE_URL'] != $arIBlock['SECTION_PAGE_URL']) {
            RedirectTable::addRule(RedirectTable::TYPE_IBLOCK_SECTION, $arIBlock['SECTION_PAGE_URL'], '', RedirectTable::STATUS_CODE_301, $arFields['ID']);
        }
        if ($arFields['DETAIL_PAGE_URL'] != $arIBlock['DETAIL_PAGE_URL'] && $arIBlock['DETAIL_PAGE_URL'] != '#PRODUCT_URL#') {
            RedirectTable::addRule(RedirectTable::TYPE_IBLOCK_ELEMENT, $arIBlock['DETAIL_PAGE_URL'], '', RedirectTable::STATUS_CODE_301, $arFields['ID']);
        }
    }

    public static function onIBlockSectionUpdate(&$arFields)
    {
        if(!$arFields['CODE'] || $arFields['ACTIVE'] == 'N') {
			
			return;
		}
		
		$arIBlock = CIBlock::GetByID($arFields['IBLOCK_ID'])->Fetch();
        $sectionPageUrlTemplate = $arIBlock['SECTION_PAGE_URL'];
        $wrappedFields = array();
        $wrappedFields['#SITE_DIR#'] = SITE_DIR;
        $wrappedFields['#SERVER_NAME#'] = SITE_SERVER_NAME;
        foreach ($arFields as $code => $value) {
            if ($code == 'CODE') {
                $wrappedFields['#SECTION_CODE#'] = $value;
            }
            if ($code == 'ID') {
                $wrappedFields['#SECTION_ID#'] = $value;
            }
            $wrappedFields['#'.$code.'#'] = $value;
        }
        $sectionCodePath = '';
        $rs = CIBlockSection::GetNavChain($arFields["IBLOCK_ID"], $arFields["ID"], array("ID", "IBLOCK_SECTION_ID", "CODE"));
        while ($a = $rs->Fetch()) {
            if ($arFields["ID"] == $a['ID']) {
                $sectionCodePath .= urlencode($arFields["CODE"]);
            } else {
                $sectionCodePath .= urlencode($a["CODE"])."/";
            }
        }
        $wrappedFields['#SECTION_CODE_PATH#'] = $sectionCodePath;
        $newUrl = strtr($sectionPageUrlTemplate, $wrappedFields);


        $arSection = CIBlockSection::GetByID($arFields['ID'])->GetNext();
        if ($arSection['SECTION_PAGE_URL'] != $newUrl) {
            RedirectTable::addRule(RedirectTable::TYPE_REGEXP, $arSection['SECTION_PAGE_URL'].'(.*)', $newUrl.'$1');
        }
    }

    public static function onIBlockElementUpdate(&$arFields)
    {
		
		if(!$arFields['CODE'] || $arFields['ACTIVE'] == 'N') {
			
			return;
		}
		
        $arIBlock = CIBlock::GetByID($arFields['IBLOCK_ID'])->Fetch();
        $urlTemplate = $arIBlock['DETAIL_PAGE_URL'];
        $wrappedFields = array();
        $wrappedFields['#SITE_DIR#'] = SITE_DIR;
        $wrappedFields['#SERVER_NAME#'] = SITE_SERVER_NAME;
        foreach ($arFields as $code => $value) {
            if ($code == 'CODE') {
                $wrappedFields['#ELEMENT_CODE#'] = $value;
            }
            if ($code == 'ID') {
                $wrappedFields['#ELEMENT_ID#'] = $value;
            }
            $wrappedFields['#'.$code.'#'] = $value;
        }

        $arElement = CIBlockElement::GetByID($arFields['ID'])->GetNext();
        $sectionCodePath = '';
        $rs = CIBlockSection::GetNavChain($arFields['IBLOCK_ID'], $arElement["IBLOCK_SECTION_ID"], array("ID", "IBLOCK_SECTION_ID", "CODE"));
        while ($a = $rs->Fetch()) {
            $sectionCodePath .= urlencode($a["CODE"])."/";
        }
        $wrappedFields['#SECTION_CODE_PATH#'] = trim($sectionCodePath, '/');
		$wrappedFields['#SECTION_CODE#'] = '';
		
		if($arElement["IBLOCK_SECTION_ID"]) {
			
			$res = CIBlockSection::GetByID($arElement["IBLOCK_SECTION_ID"]);
			if($ar_res = $res->GetNext()) {
				
				$wrappedFields['#SECTION_CODE#'] = $ar_res['CODE'];
			}
		}
		
        $newUrl = strtr($urlTemplate, $wrappedFields);
		
        if ($arElement['DETAIL_PAGE_URL'] != $newUrl) {
            RedirectTable::addRule(RedirectTable::TYPE_URL, $arElement['DETAIL_PAGE_URL'], $newUrl);
        }
    }

    private static function htmlEncode($string, $flags = ENT_COMPAT)
    {
        return htmlspecialchars($string, $flags, (defined("BX_UTF") ? "UTF-8" : "ISO-8859-1"));
    }
} 
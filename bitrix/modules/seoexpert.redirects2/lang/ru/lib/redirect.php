<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

$MESS["REDIRECT_ENTITY_ID_FIELD"] = "Ид";
$MESS["REDIRECT_ENTITY_CREATED_FIELD"] = "Дата создания";
$MESS["REDIRECT_ENTITY_CHANGED_FIELD"] = "Дата изминения";
$MESS["REDIRECT_ENTITY_USER_ID_FIELD"] = "Пользователь";
$MESS["REDIRECT_ENTITY_HASH_FIELD"] = "Уникальный ключ";
$MESS["REDIRECT_ENTITY_ACTIVE_FIELD"] = "Активность";
$MESS["REDIRECT_ENTITY_SITE_ID_FIELD"] = "Сайт";
$MESS["REDIRECT_ENTITY_TYPE_FIELD"] = "Тип";
$MESS["REDIRECT_ENTITY_IBLOCK_ID_FIELD"] = "Информационный блок";
$MESS["REDIRECT_ENTITY_SOURCE_FIELD"] = "Старый адрес";
$MESS["REDIRECT_ENTITY_SOURCE_FIELD_TEMPLATE"] = "Шаблон старого адреса";
$MESS["REDIRECT_ENTITY_SOURCE_OPTIONS_FIELD"] = "Парамтеры источника";
$MESS["REDIRECT_ENTITY_REDIRECT_FIELD"] = "Новый адрес";
$MESS["REDIRECT_ENTITY_REDIRECT_OPTIONS_FIELD"] = "Параметры редиректа";
$MESS["REDIRECT_ENTITY_STATUS_CODE_FIELD"] = "Код статуса";
$MESS["REDIRECT_ENTITY_COUNTER_FIELD"] = "Количество использований";
$MESS['REDIRECT_TYPE_URL']='Адрес';
$MESS['REDIRECT_TYPE_REGEXP']='Регулярное выражение';
$MESS['REDIRECT_TYPE_IBLOCK_SECTION']='Разделы инфоблока';
$MESS['REDIRECT_TYPE_IBLOCK_ELEMENT']='Элементы инфоблока';
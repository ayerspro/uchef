<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
$MESS['REDIRECT_TITLE_MODULE'] = 'Редирект';
$MESS['REDIRECT_TITLE_LIST'] = 'Правила';
$MESS['REDIRECT_TITLE_SETTINGS'] = 'Настройки';
$MESS['REDIRECT_TITLE_IMPORT'] = 'Импорт из CSV';
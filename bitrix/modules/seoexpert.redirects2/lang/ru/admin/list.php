<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

$MESS["REDIRECT_TITLE"] = "Правила переадресации";
$MESS['REDIRECT_TITLE_CREATE'] = "Добавить правило";
$MESS['REDIRECT_TITLE_DELETE_CONFIRM'] = "Вы действительно хотите удалить запись?";
$MESS['REDIRECT_TITLE_EDIT'] = "Изменить";
$MESS['REDIRECT_TITLE_DELETE'] = "Удалить";
$MESS['REDIRECT_TITLE_PAGES'] = "Страницы";

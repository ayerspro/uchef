<?php
$MESS["REDIRECT_IMPORT_TAB_LABEL"] = "Импорт списка редиректов из CSV";
$MESS["REDIRECT_IMPORT_TITLE"] = "Загрузка списка редиректов из файла";
$MESS["REDIRECT_IMPORT_PAGE_TITLE"] = "Импорт списка редиректов из CSV";
$MESS["REDIRECT_IMPORT_FILE_ERROR"] = "Нe выбран файл или неверный формат файла.";
$MESS["REDIRECT_IMPORT_FILE_ERROR_UPLOAD"] = "Нe удалось загрузить файл.";
$MESS["REDIRECT_IMPORT_ADMIN_MES_TITLE"] = "Загрузка каталога";
$MESS["REDIRECT_IMPORT_ADMIN_MES_FILE_SIZE"] = "Позиций в файле:";
$MESS["REDIRECT_IMPORT_ADMIN_MES_FILE_SIZE_FROM"] = " из ";
$MESS["REDIRECT_IMPORT_ADMIN_MES_PERCENT"] = "%";
$MESS["REDIRECT_IMPORT_ADMIN_MES_PERCENT_2"] = "%;";
$MESS["REDIRECT_IMPORT_ADMIN_MES_IMPORT_SUCCESS"] = "Импорт закончен";
$MESS["REDIRECT_IMPORT_ADMIN_FORM_IMPORT_TYPE"] = "Тип импорта:";
$MESS["REDIRECT_IMPORT_ADMIN_FORM_IMPORT_TYPE_NEW"] = "Только новые";
$MESS["REDIRECT_IMPORT_ADMIN_FORM_IMPORT_TYPE_FULL"] = "Полная загрузка";
$MESS["REDIRECT_IMPORT_ADMIN_FORM_DELIMITER"] = "Разделитель:";
$MESS["REDIRECT_IMPORT_ADMIN_FORM_DELIMITER_DC"] = "\";\" - точка с запятой";
$MESS["REDIRECT_IMPORT_ADMIN_FORM_DELIMITER_C"] = "\",\" - запятая";
$MESS["REDIRECT_IMPORT_ADMIN_FORM_FILE"] = "Файл с редиректами:";
$MESS["REDIRECT_IMPORT_ADMIN_FORM_EARLIER_FILES"] = "Недавно загруженные файлы:";
$MESS["REDIRECT_IMPORT_ADMIN_FORM_UPLOAD"] = "Загрузить";
$MESS["REDIRECT_IMPORT_ADMIN_DESCRIPTION"] = "<b>ВАЖНО! Файл должен быть формата CSV (расширение .csv)!</b><br>
            В поле \"Тип импорта\" можно выбрать следующие варианты:<br>
            1. Только новые - будут добавлены только те редиректы, старый адрес которого не указывался в списке редиректов модуля.<br>
            2. Полная загрузка - все найденные совпадения будут перезаписаны на данные из загружаемого файла.<br>
            <br>
            Пример содержимого файла импорта:<br>
            /news_old/; /news/<br>
            /products_old/; /products/<br>
            http://site.com/catalog_old/; http://site.com/catalog/<br>
            http://site.com/articles_old/; /articles/<br>
            <br>
            Адреса, указаные с протоколом (http:// или https://) будут обрабатываться как относительные ссылки.<br>
            То есть адрес http://site.com/catalog_old/ равнозначен адресу /catalog_old/";
<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
$MESS['MODULE_REDIRECT_NAME'] = 'SEO редирект (перенаправление страниц)';
$MESS['MODULE_REDIRECT_DESC'] = 'Модуль «SEO редирект (перенаправление страниц)» позволяет легко управлять переадресацией страниц в Битриксе. Модуль позволяет создать как правило для одного адреса, так и загрузить список правил из Excel.';
$MESS['MODULE_REDIRECT_VENDOR_NAME'] = 'СЕО Эксперт, Россия';
$MESS['MODULE_REDIRECT_VENDOR_URL'] = 'http://seo-experts.com';
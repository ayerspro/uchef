<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 *
 * @var CMain $APPLICATION
 * @var CUser $USER
 */

// admin initialization
define("ADMIN_MODULE_NAME", "seoexpert.redirects2");

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use Seoexpert\Redirects2\RedirectTable;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!$USER->IsAdmin()) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

if (!CModule::IncludeModule(ADMIN_MODULE_NAME)) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}


$APPLICATION->SetTitle(Loc::getMessage('REDIRECT_TITLE'));

$entity_table_name = RedirectTable::getTableName();

$sTableID = $entity_table_name;
$oSort = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);


$arFilter = Array();

$arFields = RedirectTable::getMap();

$arHeaders = array();

foreach ($arFields as $cell => $value) {
    $arHeaders[] = array(
        'id'      => $cell,
        'content' => $value["title"],
        'sort'    => $cell,
        'default' => $value["def"] == "Y" ? true : false,
    );
}

$lAdmin->AddHeaders($arHeaders);


if (!in_array($by, $lAdmin->GetVisibleHeaderColumns(), true)) {
    $by = 'ID';
}

$rsData = RedirectTable::getList(array(
    "select" => $lAdmin->GetVisibleHeaderColumns(),
    "order"  => array($by => strtoupper($order)),
    'filter' => $arFilter
));

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

$aMenu = array(
    array(
        "TEXT"  => Loc::getMessage('REDIRECT_TITLE_CREATE'),
        "TITLE" => Loc::getMessage('REDIRECT_TITLE_CREATE'),
        "LINK"  => "seoexpert_redirect_row_edit.php?lang=" . LANGUAGE_ID,
        "ICON"  => "btn_new",
    )
);
$lAdmin->AddAdminContextMenu($aMenu);

$lAdmin->NavText($rsData->GetNavPrint( Loc::getMessage('REDIRECT_TITLE_PAGES')));
while ($arRes = $rsData->NavNext(true, "f_")) {

    $f_ID = $arRes['ID'];
    $row = $lAdmin->AddRow($f_ID, $arRes);


    $arActions = Array();
    $arActions[] = array(
        "ICON"    => "edit",
        "TEXT"    => Loc::getMessage('REDIRECT_TITLE_EDIT'),
        "ACTION"  => $lAdmin->ActionRedirect('seoexpert_redirect_row_edit.php?ID=' . $f_ID),
        "DEFAULT" => true
    );
    $arActions[] = array(
        "ICON"   => "delete",
        "TEXT"   => Loc::getMessage('REDIRECT_TITLE_DELETE'),
        "ACTION" => "if(confirm('".Loc::getMessage('REDIRECT_TITLE_DELETE_CONFIRM')."')) " .
            $lAdmin->ActionRedirect('seoexpert_redirect_row_edit.php?action=delete&ID=' . $f_ID . '&' . bitrix_sessid_get())
    );
    $row->AddActions($arActions);
}

$lAdmin->CheckListMode();


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
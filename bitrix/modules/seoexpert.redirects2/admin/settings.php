<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 *
 * @var CMain $APPLICATION
 * @var CUser $USER
 */
// admin initialization
define("ADMIN_MODULE_NAME", "seoexpert.redirects2");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!$USER->IsAdmin()) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

if (!CModule::IncludeModule(ADMIN_MODULE_NAME)) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}


$APPLICATION->SetTitle(Loc::getMessage('REDIRECT_TITLE_EDIT_SETTINGS'));


$aTabs = array(
    array("DIV" => "edit1", "TAB" => Loc::getMessage('REDIRECT_TITLE_EDIT_TAB_SETTINGS'), "ICON" => "ad_contract_edit", "TITLE" => Loc::getMessage('REDIRECT_TITLE_EDIT_TAB_INNER_SETTINGS'))
);

$tabControl = new CAdminForm("redirect_edit", $aTabs);


// save action
if ((isset($_POST['save']) || isset($_POST['apply'])) && check_bitrix_sessid()) {
    COption::SetOptionString('seoexpert.redirects2', 'active', isset($_POST['active']) ? 'Y' : 'N');
    COption::SetOptionString('seoexpert.redirects2', 'www', htmlspecialchars($_POST['www']));
    COption::SetOptionString('seoexpert.redirects2', 'https', isset($_POST['https']) ? 'Y' : 'N');
    COption::SetOptionString('seoexpert.redirects2', 'spy_iblock', isset($_POST['spy_iblock']) ? 'Y' : 'N');
    COption::SetOptionString('seoexpert.redirects2', 'spy_iblock_section', isset($_POST['spy_iblock_section']) ? 'Y' : 'N');
    COption::SetOptionString('seoexpert.redirects2', 'spy_iblock_element', isset($_POST['spy_iblock_element']) ? 'Y' : 'N');
}

$data['active'] = COption::GetOptionString('seoexpert.redirects2', 'active');
$data['www'] = COption::GetOptionString('seoexpert.redirects2', 'www');
$data['https'] = COption::GetOptionString('seoexpert.redirects2', 'https');
$data['spy_iblock'] = COption::GetOptionString('seoexpert.redirects2', 'spy_iblock');
$data['spy_iblock_section'] = COption::GetOptionString('seoexpert.redirects2', 'spy_iblock_section');
$data['spy_iblock_element'] = COption::GetOptionString('seoexpert.redirects2', 'spy_iblock_element');

//view

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

if (!empty($errors)) {
    CAdminMessage::ShowMessage(join("\n", $errors));
}

$tabControl->BeginPrologContent();
$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();


echo '<input type="hidden" name="lang" value="' . LANGUAGE_ID . '">';
echo bitrix_sessid_post();


$tabControl->EndEpilogContent();

$tabControl->Begin(array(
    "FORM_ACTION" => $APPLICATION->GetCurPage() . "?lang=" . LANG
));
$tabControl->BeginNextFormTab();
$tabControl->AddCheckBoxField('active', Loc::getMessage('REDIRECT_TITLE_ACTIVE') . ':', 'N', 'Y', $data['active'] == 'Y');
$tabControl->AddCheckBoxField('https', Loc::getMessage('REDIRECT_TITLE_HTTPS') . ':', 'N', 'Y', $data['https'] == 'Y');
$tabControl->AddDropDownField('www', Loc::getMessage('REDIRECT_TITLE_WWW'), false, array('default'=> Loc::getMessage('REDIRECT_TITLE_WWW_DEFAULT'),'www'=>Loc::getMessage('REDIRECT_TITLE_WWW_WWW'), 'no_www'=>Loc::getMessage('REDIRECT_TITLE_WWW_NO_WWW')), $data['www']);
$tabControl->AddCheckBoxField('spy_iblock', Loc::getMessage('REDIRECT_TITLE_SPY_IBLOCK') . ':', 'N', 'Y', $data['spy_iblock'] == 'Y');
$tabControl->AddCheckBoxField('spy_iblock_section', Loc::getMessage('REDIRECT_TITLE_SPY_IBLOCK_SECTION') . ':', 'N', 'Y', $data['spy_iblock_section'] == 'Y');
$tabControl->AddCheckBoxField('spy_iblock_element', Loc::getMessage('REDIRECT_TITLE_SPY_IBLOCK_ELEMENT') . ':', 'N', 'Y', $data['spy_iblock_element'] == 'Y');
$tabControl->Buttons(array());
$tabControl->Show();
?>
    <div class="adm-info-message">
        <?= Loc::getMessage('REDIRECT_SETTINGS_ADMIN_DESCRIPTION') ?>
    </div>
<?
$tabControl->End();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

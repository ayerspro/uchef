<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use Seoexpert\Redirects2\Import;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CUtil::InitJSCore(array('jquery'));
CModule::IncludeModule("iblock");

$isAjax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));

$arTabs = Array(
    Array("DIV" => "tab1", "TAB" => Loc::getMessage('REDIRECT_IMPORT_TAB_LABEL'), "ICON" => "main_user_edit", "TITLE" => Loc::getMessage('REDIRECT_IMPORT_TITLE')),
);

$tabControl = new CAdminTabControl("tabControl", $arTabs);

$APPLICATION->SetTitle(Loc::getMessage('REDIRECT_IMPORT_PAGE_TITLE'));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
$uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/import/';

if ($REQUEST_METHOD == "POST" && check_bitrix_sessid() && (isset($_REQUEST['import']))) {


    if (!file_exists($uploadDir)) {
        mkdir($uploadDir, 0777, true);
    }

    $uploadFile = $uploadDir . date('Ymd_His_') . $_FILES['csv']['name'];
    if (file_exists($uploadFile)) {
        unlink($uploadFile);
    }
    $uploadOk = false;
    $pathInfo = pathinfo($_FILES['csv']['name']);
    if ($pathInfo['extension'] != 'csv') {
        CAdminMessage::ShowMessage(Loc::getMessage('REDIRECT_IMPORT_FILE_ERROR'));
    } else {
        if (move_uploaded_file($_FILES['csv']['tmp_name'], $uploadFile)) {
            $uploadOk = true;
            Import::init($uploadFile, intval($_POST['mode']), $_POST['delimiter']);
        } else {
            CAdminMessage::ShowMessage(Loc::getMessage('REDIRECT_IMPORT_FILE_ERROR_UPLOAD'));
        }
    }
}

if ($uploadOk || ($REQUEST_METHOD == "POST" && (isset($_REQUEST['progress'])))) {
    $uploadOk = true;
    $stat = Import::step();
}

if (file_exists($uploadDir)) {
    $lastFiles = array();
    if ($handle = opendir($uploadDir)) {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != "..") {
                $lastFiles[filectime($uploadDir . $file)] = $file;

            }
        }
        closedir($handle);
    }
    krsort($lastFiles);
    $lastFiles = array_slice($lastFiles, 0, 5);
}
?>
<? if ($isAjax) {
    $APPLICATION->RestartBuffer();
} ?>
    <div id="import_container">
        <? if ($uploadOk && $stat['state'] == Import::TYPE_PROGRESS): ?>
            <? if ($stat['state'] == Import::TYPE_PROGRESS): ?>
            <div class="adm-info-message-wrap adm-info-message-gray">
                <div class="adm-info-message">
                    <div class="adm-info-message-title"><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_MES_TITLE') ?></div>
                    <?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_MES_FILE_SIZE') ?> <b><?= $stat['pos'] ?><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_MES_FILE_SIZE_FROM') ?><?= $stat['count'] ?></b>

                    <div class="adm-progress-bar-outer" style="width: 800px;">
                        <div class="adm-progress-bar-inner" style="width: <?= $stat['percent'] ?><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_MES_PERCENT_2') ?>">
                            <div class="adm-progress-bar-inner-text" style="width: 800px;"><?= $stat['percent'] ?><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_MES_PERCENT') ?></div>
                        </div>
                        <?= $stat['percent'] ?><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_MES_PERCENT') ?>
                    </div>
                    <div class="adm-info-message-buttons"></div>
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    $.ajax({
                        data: 'progress=y',
                        type: 'post'
                    }).done(function (response) {
                        $('#import_container').replaceWith(response)
                    });
                })
            </script>

        <? elseif ($stat['state'] == Import::TYPE_ERROR): ?>
            <? CAdminMessage::ShowMessage($stat['message']); ?>
        <?
        else: ?>
            <?
            CAdminMessage::ShowNote(Loc::getMessage('REDIRECT_IMPORT_ADMIN_MES_IMPORT_SUCCESS'));
            ?>
            <? foreach ($stat['messages'][Import::TYPE_ERROR] as $error): ?>
                <? var_dump($error) ?>
            <? endforeach; ?>
        <? endif; ?>

        <? else: ?>
        <? if ($stat['state'] == Import::TYPE_SUCCESS):
            CAdminMessage::ShowNote(Loc::getMessage('REDIRECT_IMPORT_ADMIN_MES_IMPORT_SUCCESS'));
        elseif ($stat['state'] == Import::TYPE_ERROR):
            CAdminMessage::ShowMessage($stat['message']);
        endif; ?>
            <form method="POST" action="<?= $APPLICATION->GetCurPage() ?>" enctype="multipart/form-data">
                <?= bitrix_sessid_post() ?>

                <? $tabControl->Begin() ?>
                <? $tabControl->BeginNextTab() ?>
                <tr>
                    <td width="30%"><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_FORM_IMPORT_TYPE') ?></td>
                    <td width="70%">
                        <select name="mode">
                            <option value="<?= Import::MODE_UPDATE ?>"><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_FORM_IMPORT_TYPE_NEW') ?></option>
                            <option value="<?= Import::MODE_FULL ?>"><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_FORM_IMPORT_TYPE_FULL') ?></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="30%"><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_FORM_DELIMITER') ?></td>
                    <td width="70%">
                        <select name="delimiter">
                            <option value="<?= Import::DELIMITER_DC ?>"><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_FORM_DELIMITER_DC') ?></option>
                            <option value="<?= Import::DELIMITER_C ?>"><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_FORM_DELIMITER_C') ?></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="30%"><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_FORM_FILE') ?></td>
                    <td width="70%">
                        <input name="csv" type="file"/>
                    </td>
                </tr>

                <? if (count($lastFiles) > 0): ?>
                    <tr>
                        <td width="30%" style="vertical-align: top"><?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_FORM_EARLIER_FILES') ?></td>
                        <td width="70%">
                            <? foreach ($lastFiles as $file): ?>
                                <a href="/upload/import/<?= $file ?>" target="_blank"><?= $file ?></a><br/>
                            <? endforeach; ?>
                        </td>
                    </tr>
                <? endif; ?>

                <? $tabControl->Buttons() ?>

                <input type="submit" name="import" value="<?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_FORM_UPLOAD') ?>" class="adm-btn-save">

                <? $tabControl->End(); ?>
            </form>

        <? endif; ?>
        <div class="adm-info-message">
            <?= Loc::getMessage('REDIRECT_IMPORT_ADMIN_DESCRIPTION') ?>
        </div>
    </div>
<? if ($isAjax) {
    die();
} ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>
<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 *
 * @var CMain $APPLICATION
 * @var CUser $USER
 */
// admin initialization
define("ADMIN_MODULE_NAME", "seoexpert.redirects2");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Localization\Loc;
use Seoexpert\Redirects2\RedirectTable;
use Bitrix\Main\Type\DateTime;

CUtil::InitJSCore(array('jquery'));

Loc::loadMessages(__FILE__);



if (!$USER->IsAdmin()) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

if (!CModule::IncludeModule(ADMIN_MODULE_NAME)) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}



$modelMap = RedirectTable::getMap();
$ID = intval($_REQUEST['ID']);

if (!$ID) {
    $APPLICATION->SetTitle(Loc::getMessage('REDIRECT_EDIT_TITLE_CREATE_RULE'));
} else {
    $APPLICATION->SetTitle(Loc::getMessage('REDIRECT_EDIT_TITLE_CHANGE_RULE'));
}


$aTabs = array(
    array("DIV" => "edit1", "TAB" =>Loc::getMessage('REDIRECT_EDIT_TITLE_TAB'), "ICON" => "ad_contract_edit", "TITLE" => Loc::getMessage('REDIRECT_EDIT_TITLE_TAB_INNER'))
);

$tabControl = new CAdminForm("redirect_edit", $aTabs);


if (isset($_REQUEST['action']) && $_REQUEST['action'] === 'delete' && check_bitrix_sessid()) {
    RedirectTable::delete($ID);
    LocalRedirect("seoexpert_redirect_list.php?lang=" . LANGUAGE_ID);
}

$data = array();

if ($ID) {
    $data = RedirectTable::getById($_REQUEST['ID'])->fetch();
}

// save action
if ((isset($_POST['save']) || isset($_POST['apply'])) && check_bitrix_sessid()) {

    $data = array();
    foreach ($modelMap as $code => $field) {
        if (isset($_POST[$code])) {
            $data[$code] = htmlspecialchars($_POST[$code]);
        } else {
            if ($field['data_type'] == 'boolean') {
                $data[$code] = 'N';
            }
        }
    }
    $data['HASH'] = md5($data['SOURCE'] . $data['REDIRECT']);
    $data['USER_ID'] = $USER->GetID();

    /** @param Bitrix\Main\Entity\AddResult $result */
    if ($ID) {
        $data['CHANGED'] = new DateTime(date('Y-m-d H:i:s'), 'Y-m-d H:i:s');
        $result = RedirectTable::update($ID, $data);
    } else {
        $data['CREATED'] = new DateTime(date('Y-m-d H:i:s'), 'Y-m-d H:i:s');
        $data['CHANGED'] = new DateTime(date('Y-m-d H:i:s'), 'Y-m-d H:i:s');
        $result = RedirectTable::add($data);
        $ID = $result->getId();
    }

    if ($result->isSuccess()) {
        if (isset($_POST['save'])) {
            LocalRedirect("seoexpert_redirect_list.php?lang=" . LANGUAGE_ID);
        } else {
            LocalRedirect("seoexpert_redirect_row_edit.php?ID=" . $ID . "&lang=" . LANGUAGE_ID . "&" . $tabControl->ActiveTabParam());
        }
    } else {
        $errors = $result->getErrorMessages();
    }
}


// menu
$aMenu = array(
    array(
        "TEXT"  => Loc::getMessage('REDIRECT_EDIT_TITLE_BACK_URL'),
        "TITLE" => Loc::getMessage('REDIRECT_EDIT_TITLE_BACK_URL'),
        "LINK"  => "seoexpert_redirect_list.php?lang=" . LANGUAGE_ID,
        "ICON"  => "btn_list",
    )
);

$context = new CAdminContextMenu($aMenu);

//view

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
$context->Show();

if (!empty($errors)) {
    CAdminMessage::ShowMessage(join("\n", $errors));
}

$tabControl->BeginPrologContent();
$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();

echo '<input type="hidden" name="ID" value="' . htmlspecialcharsbx($data['ID']) . '">';
echo '<input type="hidden" name="lang" value="' . LANGUAGE_ID . '">';
echo bitrix_sessid_post();


$tabControl->EndEpilogContent();

$tabControl->Begin(array(
    "FORM_ACTION" => $APPLICATION->GetCurPage() . "?ID=" . intval($ID) . "&lang=" . LANG
));
$tabControl->BeginNextFormTab();

foreach ($modelMap as $code => $field) {
    if ($field['hide']) {
        continue;
    }
    if ($field['autocomplete']) {
        $tabControl->AddViewField($code, $field['title'] . ':', $data[$code]);
    } elseif (isset($field['values'])) {
        if ($field['data_type'] == 'boolean') {
            $tabControl->AddCheckBoxField($code, $field['title'] . ':', $field['required'] ? 'Y' : 'N', 'Y', $data[$code] == 'Y');
        } else {

            $tabControl->AddDropDownField($code, $field['title'] . ':', $field['required'] ? 'Y' : 'N', $field['values'], $data[$code]);

        }
    } elseif ($field['data_type'] == 'string' || $field['data_type'] == 'integer') {
        $tabControl->AddEditField($code, $field['title'] . ':', $field['required'] ? 'Y' : 'N', array('size' => $field['size']), $data[$code]);
    }
}
$tabControl->Buttons(array());
$tabControl->Show();
$tabControl->End();
?>

    <div id="SOURCE_url" style="display: none">
        <div class="seoexpert-info-message">
            <?= Loc::getMessage('REDIRECT_EDIT_SEOEXPERT_INFO_SOURCE_URL') ?>
        </div>
    </div>

    <div id="SOURCE_regexp" style="display: none">
        <div class="seoexpert-info-message">
            <?= Loc::getMessage('REDIRECT_EDIT_SEOEXPERT_INFO_SOURCE_REGEXP') ?>
        </div>
    </div>

    <div id="SOURCE_iblock_section" style="display: none">
        <div class="seoexpert-info-message">
            <?= Loc::getMessage('REDIRECT_EDIT_SEOEXPERT_INFO_SOURCE_SECTION') ?>
        </div>
    </div>

    <div id="SOURCE_iblock_element" style="display: none">
        <div class="seoexpert-info-message">
            <?= Loc::getMessage('REDIRECT_EDIT_SEOEXPERT_INFO_SOURCE_ELEMENT') ?>
        </div>
    </div>

    <style>
        .seoexpert-info-message {
            background: -webkit-linear-gradient(top, rgba(244,233,141,.3), rgba(232,209,62,.3), rgba(225,194,40,.3));
            background: -moz-linear-gradient(top, rgba(244,233,141,.3), rgba(232,209,62,.3), rgba(225,194,40,.3));
            background: -ms-linear-gradient(top, rgba(244,233,141,.3), rgba(232,209,62,.3), rgba(225,194,40,.3));
            background: -o-linear-gradient(top, rgba(244,233,141,.3), rgba(232,209,62,.3), rgba(225,194,40,.3));
            background: linear-gradient(top, rgba(244,233,141,.3), rgba(232,209,62,.3), rgba(225,194,40,.3));
            margin: 5px 0;
            border: 1px solid;
            border-color: #d3c6a3 #cabc90 #c1b37f #c9bc8f;
            border-radius: 5px;
            padding: 5px;
            color: #706536;
        }
        .seoexpert-info-message b {
            font-weight: 700;
        }
        #tr_SOURCE .adm-detail-content-cell-l {
            vertical-align: text-bottom !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('#tr_SOURCE').find('.adm-detail-content-cell-r').append($('#SOURCE_url').html());
            $('#tr_TYPE').find('select').change(function () {
                $('#tr_SOURCE').find('.adm-detail-content-cell-r').find('.seoexpert-info-message').remove();
                $('#tr_SOURCE').find('.adm-detail-content-cell-r').append($('#SOURCE_' + $(this).val()).html());
                if ($(this).val() == 'iblock_section' || $(this).val() == 'iblock_element') {
                    $('#tr_IBLOCK_ID').show();
                    $('#tr_REDIRECT').hide();
                } else {
                    $('#tr_IBLOCK_ID').hide();
                    $('#tr_REDIRECT').show();
                };
                if ($(this).val() == 'url') {
                    $('#tr_SOURCE').find('span').text('<?= Loc::getMessage('REDIRECT_ENTITY_SOURCE_FIELD') ?>');
                } else {
                    $('#tr_SOURCE').find('span').text('<?= Loc::getMessage('REDIRECT_ENTITY_SOURCE_FIELD_TEMPLATE') ?>');
                };
            }).change();
        });
    </script>
<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aMenu[] = array(
    'parent_menu' => 'global_menu_settings',
    'sort'        => 400,
    'text'        => Loc::getMessage('REDIRECT_TITLE_MODULE'),
    'title'       => Loc::getMessage('REDIRECT_TITLE_MODULE'),
    'url'         => 'seoexpert_redirect_list.php',
    'items_id'    => 'menu_references',
    'icon'=>'workflow_menu_icon',
    'items'       => array(
        array(
            'text'     => Loc::getMessage('REDIRECT_TITLE_LIST'),
            'url'      => 'seoexpert_redirect_list.php?lang=' . LANGUAGE_ID,
            'more_url' => array('seoexpert_redirect_list.php?lang=' . LANGUAGE_ID),
            'title'    => Loc::getMessage('REDIRECT_TITLE_LIST'),
        ),
        array(
            'text'     => Loc::getMessage('REDIRECT_TITLE_SETTINGS'),
            'url'      => 'seoexpert_redirect_settings.php?lang=' . LANGUAGE_ID,
            'more_url' => array('seoexpert_redirect_settings.php?lang=' . LANGUAGE_ID),
            'title'    => Loc::getMessage('REDIRECT_TITLE_SETTINGS'),
        ),
        array(
            'text'     => Loc::getMessage('REDIRECT_TITLE_IMPORT'),
            'url'      => 'seoexpert_redirect_import.php?lang=' . LANGUAGE_ID,
            'more_url' => array('seoexpert_redirect_import.php?lang=' . LANGUAGE_ID),
            'title'    => Loc::getMessage('REDIRECT_TITLE_IMPORT'),
        ),
    )
);

return $aMenu;

<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/modules/seoexpert.redirects2/')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/modules/seoexpert.redirects2/admin/edit.php';
} else {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/seoexpert.redirects2/admin/edit.php';
}
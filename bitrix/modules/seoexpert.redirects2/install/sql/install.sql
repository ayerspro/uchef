CREATE TABLE se_redirect (
  ID int NOT NULL auto_increment,
  CREATED datetime NOT NULL,
  CHANGED datetime NOT NULL,
  USER_ID int NOT NULL,
  HASH varchar(64) NOT NULL ,
  ACTIVE varchar(1) NOT NULL,
  SITE_ID varchar(64) NOT NULL ,
  TYPE varchar(64) NOT NULL ,
  IBLOCK_ID int,
  SOURCE varchar(255) NOT NULL,
  SOURCE_OPTIONS text,
  REDIRECT varchar(255),
  REDIRECT_OPTIONS text,
  STATUS_CODE int NOT NULL ,
  COUNTER int,
  PRIMARY KEY (ID),
  UNIQUE KEY (HASH)
);
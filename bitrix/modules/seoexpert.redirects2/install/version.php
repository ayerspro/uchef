<?php
/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 */
$arModuleVersion = array(
    "VERSION"      => "1.0.1",
    "VERSION_DATE" => "2018-01-09 09:00:00"
);
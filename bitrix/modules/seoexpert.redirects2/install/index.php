<?php

use Bitrix\Main\EventManager;

IncludeModuleLangFile(__FILE__);


/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 */
class seoexpert_redirects2 extends CModule
{
    var $MODULE_ID = 'seoexpert.redirects2';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function __construct()
    {

        $arModuleVersion = array();
        include("version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = GetMessage('MODULE_REDIRECT_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('MODULE_REDIRECT_DESC');
        $this->PARTNER_NAME = GetMessage('MODULE_REDIRECT_VENDOR_NAME');
        $this->PARTNER_URI = GetMessage('MODULE_REDIRECT_VENDOR_URL');
    }

    function InstallDB()
    {
        global $DB;
        $DB->RunSQLBatch(__DIR__ . '/sql/install.sql');

        return true;
    }

    function UnInstallDB()
    {
        global $DB;
        $DB->RunSQLBatch(__DIR__ . '/sql/uninstall.sql');

        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin', true);

        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');

        return true;
    }

    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);
        $this->InstallDB();
        $this->InstallFiles();
        EventManager::getInstance()->registerEventHandler('main', 'OnPageStart', $this->MODULE_ID, 'Seoexpert\Redirects2\Handler', 'onPageStart');
    }

    function DoUninstall()
    {
        EventManager::getInstance()->unRegisterEventHandler('main', 'OnPageStart', $this->MODULE_ID, 'Seoexpert\Redirects2\Handler', 'onPageStart');
        $this->UnInstallDB();
        $this->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
    }
}
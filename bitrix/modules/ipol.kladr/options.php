<? 
#################################################
#        Company developer: IPOL
#        Developers: Karpov Sergey, Dmitry Pokrovskiy
#        Site: http://www.ipol.com
#        E-mail: info@ipolh.com
#        Copyright (c) 2014- IPOL
#################################################

IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");	
 
CModule::IncludeModule("sale");
CModule::includeModule("ipol.kladr");
$module_id = "ipol.kladr"; 

//�������
CJSCore::Init(array("jquery"));
include "js.php";
include "css.php"; 

//������ ��������
$arAllOptions = array(
	"main" => Array(
		Array("FUCK", GetMessage("OPTNAME_FUCK"), '', Array("checkbox")),
		Array("JQUERY", GetMessage("OPTNAME_JQUERY"), '', Array("checkbox")),
		Array("FORADMIN", GetMessage("OPTNAME_FORADMIN"), '', Array("checkbox")),
		Array("ADRCODE", GetMessage("OPTNAME_ADRCODE"), 'ADDRESS', Array("text")),
		//Array("HIDELOCATION", GetMessage("OPTNAME_HIDELOCATION"), 'Y', Array("checkbox")),
		
	),
	"kladr" => Array(
		Array("TOKEN", GetMessage("OPTNAME_TOKEN"), '', Array("text")),
		// Array("KEY", GetMessage("OPTNAME_KEY"), '', Array("text")),
	),
	"beauty" => Array(
		Array("NOTSHOWFORM", GetMessage("OPTNAME_NOTSHOWFORM"), 'Y', Array("checkbox")),	
		Array("SHOWMAP", GetMessage("OPTNAME_SHOWMAP"), 'Y', Array("checkbox")),
		Array("SHOWADDR", GetMessage("OPTNAME_SHOWADDR"), 'Y', Array("checkbox")),
		Array("MAKEFANCY", GetMessage("OPTNAME_MAKEFANCY"), '', Array("checkbox")),
	),
	"additional" => Array(
		Array("METRO", GetMessage("OPTNAME_METRO"), '', Array("checkbox")),
	),
	"error" => Array(
		Array("ERRWRONGANSWER", GetMessage("OPTNAME_ERRWRONGANSWER"), '', Array("text")),
		Array("ERRWRONGANSWERDATE", GetMessage("OPTNAME_ERRWRONGANSWERDATE"), '', Array("text")),
	),
);

//������ �����
$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
	array("DIV" => "edit2", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "ICON" => "vote_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")),
);

//Save options
if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$RestoreDefaults)>0 && check_bitrix_sessid())
{
	if(strlen($RestoreDefaults)>0)
		COption::RemoveOption("ipol.towns");
	else
	{
		foreach($arAllOptions as $aOptGroup)
			foreach($aOptGroup as $option)
			{
				__AdmSettingsSaveOption($module_id, $option);
			}
	}
	
	if($_REQUEST["back_url_settings"] <> "" && $_REQUEST["Apply"] == "")
		   echo '<script type="text/javascript">window.location="'.CUtil::addslashes($_REQUEST["back_url_settings"]).'";</script>';
}

function ShowParamsHTMLByArray($arParams)
{//����� ���������
	foreach($arParams as $Option)
		__AdmSettingsDrawRow($GLOBALS['module_id'], $Option);
}

$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>

<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&amp;lang=<?echo LANG?>">
<?


if($_REQUEST["METRO"]=='Y')
// if(true)
{	
	$group_id=1;
	$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array("ACTIVE"=>'Y',"TYPE" => "LOCATION",),false,false,array());
	if ($props = $db_props->Fetch())
		$group_id=$props["PROPS_GROUP_ID"];
	
	$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array("CODE"=>"IPOLKLADR_METRO",),false,false,array());
	if(intval($db_props->SelectedRowsCount())==0){
		$db_ptype = CSalePersonType::GetList(Array(), Array());
		$user_types=array();
		while ($ptype = $db_ptype->Fetch())
		{
		   $user_types[]=$ptype["ID"];
		}

		foreach($user_types as $user_type_id)
		{
			$arFields = array(
			   "PERSON_TYPE_ID" => $user_type_id,
			   "NAME" => GetMessage('METRO_PROP_NAME'),
			   // "NAME" => "��������� �����",
			   "TYPE" => "TEXT",
			   "REQUIED" => "N",
			   "UTIL" => "Y",
			   "USER_PROPS" => "Y",
			   "DEFAULT_VALUE" => "",
			   "SORT" => 100,
			   "CODE" => "IPOLKLADR_METRO",
			   "USER_PROPS" => "N",
			   "IS_LOCATION" => "N",
			   "IS_LOCATION4TAX" => "N",
			   "PROPS_GROUP_ID" => $group_id,
			   "SIZE1" => 0,
			   "SIZE2" => 0,
			   "DESCRIPTION" => "",
			   "IS_EMAIL" => "N",
			   "IS_PROFILE_NAME" => "N",
			   "IS_PAYER" => "N"
			);

			$ID = CSaleOrderProps::Add($arFields);
			if ($ID<=0)
			  echo GetMessage('METRO_PROP_ERROR');
		}
	}
}

$tabControl->Begin();
$tabControl->BeginNextTab();

?>
	
	<?if($_REQUEST["admin"]=='lock'){//������� ������?>
		<tr class="heading">
			<td colspan="2" valign="top" align="center">TEST for Admin</td>
		</tr> 
		<tr>
			<td colspan="2" valign="top" align="center">
				<?ShowParamsHTMLByArray($arAllOptions["hidden"]);?>
			</td>
		</tr>
		<tr class="heading">
			<td colspan="2" valign="top" align="center"><?=GetMessage('ST_ADDITIONAL')?></td>
		</tr> 
		<tr>
			<td colspan="2" valign="top" align="center">
				<?ShowParamsHTMLByArray($arAllOptions["additional"]);?>
			</td>
		</tr>
		<tr class="heading">
			<td colspan="2" valign="top" align="center">Errors</td>
		</tr> 
		<tr>
			<td colspan="2" valign="top" align="center">
				<?ShowParamsHTMLByArray($arAllOptions["error"]);?>
			</td>
		</tr>
	<?}?>
	<?
	$errAnswerdate = intval(COption::GetOptionString("ipol.kladr", "ERRWRONGANSWERDATE"));
	$fail=false;
	if($errAnswerdate>0)
		if(mktime()-$errAnswerdate<900){//���� ������ 15 ����� � ������
			$fail=true;//��� ����
		}
		else
		{//����� ���������
			$fail=false;
		}
	else//������ �� ����
		$fail=false;
		
	// $fail=true;
	if($fail){
	?> 
	<tr class="heading">
		<td colspan="2" valign="top" align="center"><?=GetMessage('ST_ERROR')?></td>
	</tr> 
	<tr>
		<td colspan="2" valign="top" align="center">
			<div style="color:red;"><?=GetMessage('ERROR')?><?=GetMessage('ERROR_0')?></div>
		</td>
	</tr>
	<?}?> 
	<tr class="heading">
		<td colspan="2" valign="top" align="center"><?=GetMessage('ST_HELP')?></td><?//������ ���������?>
	</tr> 
	<tr>
		<td colspan="2" valign="top" align="left">
			<?=GetMessage('ST_HELP_TEXT')?>
		</td>
	</tr> 
	
	<tr class="heading">
		<td colspan="2" valign="top" align="center"><?=GetMessage('ST_MAINSET')?></td>
	</tr> 
	<?
		ShowParamsHTMLByArray($arAllOptions["main"]);//�������� ���������
	?>
	<tr class="heading">
		<td colspan="2" valign="top" align="center"><?=GetMessage('ST_KLADR')?></td>
	</tr> 
	
	<?ShowParamsHTMLByArray($arAllOptions["kladr"]);//�������� ���������
	$token = COption::GetOptionString("ipol.kladr", "TOKEN");
	  // $key = COption::GetOptionString("ipol.kladr", "KEY");
	  if(!$token) {
	?>
	<tr class="">
		<td colspan="2" valign="top" align="center"><?=GetMessage('REG_KLADR')?></td>
	</tr><?}?>
	
	<tr class="heading">
		<td colspan="2" valign="top" align="center"><?=GetMessage('ST_BEAUTY')?></td>
	</tr> 	
	<tr class="">
		<td colspan="2" valign="top" align="left">
			<? echo GetMessage('CHANGE_CSS');//����� ��� ��������� ������� �����?>
		</td>
	</tr> 
	<?ShowParamsHTMLByArray($arAllOptions["beauty"]);?>
	
	<div id="pop-jQ" class="b-popup" style="display: none;">
		<div class="pop-text">
			<?=GetMessage('HINT_JQUERY')?>
		</div>
		<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
	</div>
	<div id="adrcode" class="b-popup" style="display: none;">
		<div class="pop-text">
			<?=GetMessage('HINT_ADRCODE')?>
		</div>
		<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
	</div>
	<div id="notshowform" class="b-popup" style="display: none;">
		<div class="pop-text">
			<?=GetMessage('HINT_NOTSHOWFORM')?>
		</div>
		<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
	</div>
	<div id="hidelocation" class="b-popup" style="display: none;">
		<div class="pop-text">
			<?=GetMessage('HINT_HIDELOCATION')?>
		</div>
		<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
	</div>
	<div id="fancy" class="b-popup" style="display: none;">
		<div class="pop-text">
			<?=GetMessage('HINT_MAKEFANCY')?>
		</div>
		<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
	</div>
	<?
$tabControl->BeginNextTab();
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
$tabControl->Buttons();
?>
	<div align="left">
		<input type="hidden" name="Update" value="Y">
		<input type="submit" <?if(!$USER->IsAdmin())echo " disabled ";?> name="Update" value="<?echo GetMessage("MAIN_SAVE")?>">
	</div>
<?$tabControl->End();?>
<?=bitrix_sessid_post();?>
</form>
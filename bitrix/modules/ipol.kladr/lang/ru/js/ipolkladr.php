<?
$MESS ['CHANGEADR'] = 'изменить адрес';
$MESS ['TAPENUMPER'] = 'введите номер дома';
$MESS ['TAPESTREET'] = 'введите улицу';
$MESS ['TAPETOWN'] = 'введите город/нас.пункт';
$MESS ['TAPEROOM'] = 'Квартира/офис';
$MESS ['OBJECT'] = 'Выбранный объект';
$MESS ['CODE'] = 'Код:';
$MESS ['POSTINDEX'] = 'Почтовый индекс:';
$MESS ['NAME'] = 'Название:';
$MESS ['TYPE'] = 'Тип:';
$MESS ['TYPESHORT'] = 'Тип короко:';
$MESS ['region'] = 'область';
$MESS ['gorod'] = 'город';
$MESS ['town'] = 'Город';
$MESS ['ylica'] = 'улица';
$MESS ['dom'] = 'дом';
$MESS ['g'] = 'г.';
$MESS ['yl'] = 'ул.';
$MESS ['d'] = 'д.';
$MESS ['kv'] = 'кв.';
$MESS ['RF'] = "Российская федерация";
$MESS ['RESP'] = "Республика";
$MESS ['nearesmetro'] = 'Ближайшее метро: ';

$MESS ['RUSSIA_NAME'] = 'Россия';
$MESS ['SAVEADR'] = 'Сохранить адрес';
$MESS ['CHANGE_RUSSIA'] = 'выбрать другую страну';


 
$MESS ['nojquery'] = 'Ошибка Kladr. Библиотека jquery не найдена. Возможно она отсутствует. В этом случае можно выставить галочку "подключать jquery" в настройках модуля. Или библиотека jquery подключается после подключения библиотеки kladr. В этом случае нужно перенести подключение библиотеки jquery в header.php шаблона над вызовом ShowHead(или ShowHeadScripts)';
$MESS ['nokladrobj'] = 'Ошибка Kladr. Расширение kladr для библиотеки jquery не найдено. Обычно это значит, что библиотека jquery подключается после подключения библиотеки kladr. В этом случае нужно перенести подключение библиотеки jquery в header.php шаблона над вызовом ShowHead(или ShowHeadScripts)';
$MESS ['RunFormnoktoblattr'] = 'Выберите город или область';
$MESS ['RunFormnoktobl'] = 'Ошибка Kladr. Местоположение введено неверно. Это может означать ошибку в компоненте bitrix:sale.order.ajax или неправильную логику модуля. Если поле выбора местоположения работает верно, обратитесь, пожалуйста, в нашу техподдержку - support@ipolh.com';

?>
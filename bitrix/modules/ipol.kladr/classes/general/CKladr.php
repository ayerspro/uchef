<? 
IncludeModuleLangFile(__FILE__);
// ���������� ����������� ����������
class CKladr
{
	static $MODULE_ID = "ipol.kladr";
	public static $townid;
	public static $NotRussia;
	public static $townobj;
	public static $contentType;
	public static $lastobject;
	public static $hidelocation;

	public static $towns=false;
	public static $padArray=false;
	public static $MacrArray=false;
	public static $thistown=false;
	public static $yatowns;
	public static $error=false;
	
	//�����-�� �������� ���� , ���� ����� ��� ���-�� ������������
	function zajsonit($handle){// � UTF
		if(LANG_CHARSET !== 'UTF-8'){
			if(is_array($handle))
				foreach($handle as $key => $val){
					unset($handle[$key]);
					$key=self::zajsonit($key);
					$handle[$key]=self::zajsonit($val);
				}
			else
				$handle=$GLOBALS['APPLICATION']->ConvertCharset($handle,LANG_CHARSET,'UTF-8');
		}
		return $handle;
	}
	function zaDEjsonit($handle){//�� UTF
		if(LANG_CHARSET !== 'UTF-8'){
			if(is_array($handle))
				foreach($handle as $key => $val){
					unset($handle[$key]);
					$key=self::zaDEjsonit($key);
					$handle[$key]=self::zaDEjsonit($val);
				}
			else
				$handle=$GLOBALS['APPLICATION']->ConvertCharset($handle,'UTF-8',LANG_CHARSET);
		}
		return $handle;
	}
		
	function PrintLog($somecontent, $rewrite=false,$err=false) {
		$filename = $_SERVER["DOCUMENT_ROOT"]."/KladrLog.txt";
		if($err) $filename = $_SERVER["DOCUMENT_ROOT"]."/KladrErrorLog.txt";
			if($rewrite) $rewrite = 'w'; else $rewrite = 'a';
			if(is_array($somecontent)) $somecontent = print_r($somecontent, true);
			if (!$handle = fopen($filename, $rewrite)) {
				 // echo "�� ���� ������� ���� ($filename)";
				 exit;
			}

			// ���������� $somecontent � ��� �������� ����.
			if (fwrite($handle, $somecontent) === FALSE) {
				// echo "�� ���� ���������� ������ � ���� ($filename)";
				exit;
			}

			// echo "���! �������� ($somecontent) � ���� ($filename)";

			fclose($handle);
	}
	
	function Error($err){
		self::PrintLog($err,false,true);
	}
	
	function toUpper($str,$lang){
		if(!$lang) $lang=LANG_CHARSET;
		$str = str_replace( //H8 ANSI
			array(
				GetMessage('IPOL_LANG_YO_S'),
				GetMessage('IPOL_LANG_CH_S'),
				GetMessage('IPOL_LANG_YA_S')
			),
			array(
				GetMessage('IPOL_LANG_YO_B'),
				GetMessage('IPOL_LANG_CH_B'),
				GetMessage('IPOL_LANG_YA_B')
			),
			$str
		);
		if(function_exists('mb_strtoupper'))
			return mb_strtoupper($str,$lang);
		else
			return strtoupper($str);
	}
	
	// RegisterModuleDependences("sale","OnSaleComponentOrderOneStepPersonType", "ipol.kladr", "CKladr", "SetJS");
	function SetJS(&$arResult, &$arUserResult, $arParams){// ���������� ��� ��� ���� ��������� order.ajax		
		if(COption::GetOptionString("ipol.kladr", "FUCK")=='Y') return;
		
		global $USER;
		$foradmin = COption::GetOptionString("ipol.kladr", "FORADMIN");	
		if ($foradmin=='Y') { $foradmin=$USER->IsAdmin();} else {$foradmin=true;}//�� ��������
		
		if($foradmin && $_REQUEST["PULL_UPDATE_STATE"] != 'Y' && $_REQUEST["PULL_AJAX_CALL"] != 'Y') // ���� �� �������� � �� ����
		{
			$jq = COption::GetOptionString("ipol.kladr", "JQUERY");
			if($jq=='Y')  CJSCore::Init(array("jquery"));
			//������ ���������
			$KladrSettings=array(
				"kladripoladmin"=>false,// ��� ������������� //
				"kladripoltoken"=> COption::GetOptionString("ipol.kladr", "TOKEN"),// ������ ����� //
				"notShowForm"=> (COption::GetOptionString("ipol.kladr", "NOTSHOWFORM")=='Y'),// �� ���������� ����� ��� ������ �������� //
				"HideLocation"=> false,//(COption::GetOptionString("ipol.kladr", "HIDELOCATION")=='Y'),// ����� ����� - �������� �������
				"code"=>COption::GetOptionString("ipol.kladr", "ADRCODE"),
				"arNames"=>array(),// �������� ���� ��� ������//
				"ShowMap"=>(COption::GetOptionString("ipol.kladr", "SHOWMAP")=='Y'),
				"ShowAddr"=>(COption::GetOptionString("ipol.kladr", "SHOWADDR")=='Y'),
				"MakeFancy"=>(COption::GetOptionString("ipol.kladr", "MAKEFANCY")=='Y'),
				"add"=>array( // ���.�������
					"addMetro"=>COption::GetOptionString("ipol.kladr", "METRO"),// ���������� �����
				),	
			);
			
			$KladrSettings["code"]=$KladrSettings["code"]?$KladrSettings["code"]:"ADDRESS";
			if(strpos($KladrSettings["code"], ',')!==false){
				$KladrSettings["code"]=explode(',',$KladrSettings["code"]);
			}
			
			if($USER->IsAdmin()) $KladrSettings["kladripoladmin"]=true;
				
			if($KladrSettings["kladripoltoken"]){
				//�������� ���������� ���� ������
				CModule::IncludeModule("sale");
				$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array("CODE"=>$KladrSettings["code"],),false,false,array("ID")); 
				while ($props = $db_props->Fetch()) 
				{
					$KladrSettings["arNames"][]="ORDER_PROP_".$props["ID"];
				}
				if(!is_array($KladrSettings["arNames"]) || empty($KladrSettings["arNames"]))
				{
					self::Error("TEXTAREA NOT FOUND");
					return;
				}
				
				/*
				$townArray=array();
				$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array("IS_LOCATION"=>'Y'),false,false,array("ID","IS_LOCATION","DEFAULT_VALUE"));
				while ($props = $db_props->Fetch()) 
				{
					$KladrSettings["arLocatinos"][]=$props["ID"];
					if($props["DEFAULT_VALUE"]){	
						$db_vars = CSaleLocation::GetList(array(),array("LID" => 'RU'),false,false,array());
						while ($vars = $db_vars->Fetch()){
							$townArray[]=$vars ;
							if($vars["ID"]==$props["DEFAULT_VALUE"]){
								$arNames[$vars["ID"]]=$vars["CITY_NAME"];
								$KladrSettings["arLocatinosDefVal"]["ORDER_PROP_".$props["ID"]]=$vars["CITY_NAME"];
							}
						}
					}
				}
				
				$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array("IS_ZIP"=>'Y'),false,false,array("ID"));
				while ($props = $db_props->Fetch()) 
				{
					$KladrSettings["arZips"][]="ORDER_PROP_".$props["ID"];
				}*/
				
				
				global $APPLICATION;
				//������� ������
				$APPLICATION->AddHeadString('<script>var KladrSettings;KladrSettings = '.json_encode($KladrSettings).';var KladrBitrixLocations;KladrBitrixLocations = '.json_encode($townArray).';</script>',true);
				
				$APPLICATION->AddHeadString('<script src="/bitrix/js/ipol.kladr/jquery.kladr.min.js" type="text/javascript"></script>',true);
				//css
				if(file_exists($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/css/kladr.css'))
				{
					$APPLICATION->AddHeadString('<link rel="stylesheet" href="'.SITE_TEMPLATE_PATH.'/css/kladr.css'.'">',true);
				}
				else
					$APPLICATION->AddHeadString('<link href="/bitrix/js/ipol.kladr/kladr.css" rel="stylesheet">',true);
				
				if($KladrSettings["ShowMap"]) $APPLICATION->AddHeadString('<script src="//api-maps.yandex.ru/2.1/?load=package.standard&mode=release&lang=ru-RU" type="text/javascript"></script>',true);

				
				if($KladrSettings["add"]["addMetro"]=='Y'){
					$APPLICATION->AddHeadString('<script type="text/javascript" src="//api-maps.yandex.ru/1.1/" charset="utf-8"></script>',true);
					$APPLICATION->AddHeadString('<script>YMaps.load("regions", function(){
					YMaps.load("metro", function(){KladrSettings.add.addMetro=true});});</script>',true);
					
					$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array("CODE"=>"IPOLKLADR_METRO",),false,false,array());
					while ($props = $db_props->Fetch())
					{
						$arMetroNames[]="ORDER_PROP_".$props["ID"];
					}
					$APPLICATION->AddHeadString('<script>var arMetroNames='.json_encode($arMetroNames).';</script>	',true);
				
				}
				$APPLICATION->AddHeadScript("/bitrix/js/ipol.kladr/ipolkladr.js");
				
				CJSCore::Init(array('ipolkladr'));
			}
		}
	}
	
	function retRegion($region){//$region - ������� � ��������� ����� 
		$arChange=GetMessage('CHANGE');
		if(in_array($region,array_keys($arChange))) $region=$arChange[$region]; 
		return $region;
	}
	
	// RegisterModuleDependences("main", "OnPageStart","ipol.kladr", "CKladr","SetLocation");
	function SetLocation(){//������ ������ �������, ����� ��������� �����
		// $_REQUEST["ipolkladrnewcity"]=self::zaDEjsonit($_REQUEST["ipolkladrnewcity"]);
		// $_REQUEST["ipolkladrnewregion"]=self::zaDEjsonit($_REQUEST["ipolkladrnewregion"]);
		if($_REQUEST["ipolkladrnewregion"]) $region=self::retRegion($_REQUEST["ipolkladrnewregion"]);			
		
		if($_REQUEST["ipolkladrlocation"]){
			if($_REQUEST["ipolkladrnewcity"])
			{
				CModule::IncludeModule("sale");
				//�������� ���������� id
				$arFilter=array("LID" => LANGUAGE_ID,"CITY_NAME"=>$_REQUEST["ipolkladrnewcity"]);
				if($region) $arFilter["REGION_NAME"]=$region;
				// self::PrintLog($arFilter,false);
				$db_vars = CSaleLocation::GetList(array(),$arFilter,false,false,array());
				if ($vars = $db_vars->Fetch()){
					// ��������� �������� �� ���������� �������
					//��������, ��� ��� ��� ����� � �� ������ ���������
					
					$locationid=$vars["ID"];	
				// self::Printlog($vars);				
				}
				if(!$locationid){
					//�������� ���������� �������
					if($_REQUEST["ipolkladrnewregion"])
						//�������
						$db_vars = CSaleLocation::GetList(
							array(
									"SORT" => "ASC",
									"COUNTRY_NAME_LANG" => "ASC",
									"CITY_NAME_LANG" => "ASC"
								),
							// array("LID" => LANGUAGE_ID,"CITY_NAME"=>$_REQUEST["newcity"]),
							array("LID" => LANGUAGE_ID,"REGION_NAME"=>$region),
							false,
							false,
							array()
						);
						if ($vars = $db_vars->Fetch()){
							$locationid=$vars["ID"];	
						}
				}
				
				//������ id � ��������������
				if($locationid){
					// ����� id ������
					$_REQUEST[$_REQUEST["ipolkladrlocation"]]=$locationid;
					$_POST[$_REQUEST["ipolkladrlocation"]]=$locationid;
					$_GET[$_REQUEST["ipolkladrlocation"]]=$locationid;
				} 
			}	
		}			
	}
	

	function OnSaleComponentOrderOneStepDeliveryHandler(&$arResult, &$arUserResult, $arParams)
	{//����� ����������� �������� ��������
		if((COption::GetOptionString("ipol.kladr", "FUCK")=='Y')) return;
		
		// if(COption::GetOptionString("ipol.kladr", "HIDELOCATION")!='Y'){// ���� ��-������� ��������
			
			$city=array();
			$token = COption::GetOptionString("ipol.kladr", "TOKEN");//MOD
			if(!$token) self::Error("Token lost");
				
			// echo "Sdad";
			//��������� ������ ���������� ������ � ������� ����� ������ �����
			if(is_array($arResult["ORDER_PROP"]["USER_PROPS_Y"])) 
			{
				$arPr=$arResult["ORDER_PROP"]["USER_PROPS_Y"];
				if(is_array($arResult["ORDER_PROP"]["USER_PROPS_N"]))
					$arPr=array_merge($arResult["ORDER_PROP"]["USER_PROPS_Y"],$arResult["ORDER_PROP"]["USER_PROPS_N"]);
			}
			elseif(is_array($arResult["ORDER_PROP"]["USER_PROPS_N"])) {
				$arPr=$arResult["ORDER_PROP"]["USER_PROPS_N"];
			}

			if(is_array($arPr))
			{	
				//������� ���������� ��������� ��������������, ���� ���� �������� (������ �����)
				foreach($arPr as $arropname=>$prop)
				{
					if($prop["TYPE"] == 'LOCATION')
					{
						foreach($prop["VARIANTS"] as $town){
							if($town["ID"] == $prop["VALUE"]) $city=$town;
							if($town["SELECTED"]== 'Y') $city=$town;
						}
					}
				}
			}
			else{//����� ����� �� $arUserResult
				$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array("IS_LOCATION"=>'Y',"PERSON_TYPE_ID"=>$arUserResult["PERSON_TYPE_ID"]),false,false,array("ID","IS_LOCATION","DEFAULT_VALUE"));
				if ($props = $db_props->Fetch()) 
				{
					if($arUserResult["ORDER_PROP"][$props["ID"]])
						$city=array("ID"=>$arUserResult["ORDER_PROP"][$props["ID"]]);
					else
						$city=array("ID"=>$props["DEFAULT_VALUE"]);
				}
			}
			
			//������� ������ �������� ������ � ��������-���������
			$regionname=false;
			$countryname=false;
			$townname=false;

			if($city["ID"]){//���� ��� ������ �����
				// $db_vars = CSaleLocation::GetList(array(),array("LID" => LANGUAGE_ID,"ID"=>$city["ID"]),false,false,array());
				// $vars = CSaleLocation::GetByID($city["ID"], LANGUAGE_ID);
				$vars = self::getCityNameByID($city["ID"]);
				// while ($vars = $db_vars->Fetch()){
				if (is_array($vars)){
					$city=$vars;
					$regionname=self::zajsonit($vars["REGION_NAME"]);
					$countryname=self::zajsonit($vars["COUNTRY_NAME"]);
					$townname=self::zajsonit($vars["CITY_NAME"]);
				}
			}
						
			if(is_array($city) && !empty($city)){
				if($city["COUNTRY_NAME"] != GetMessage("RUSSIA_NAME")) 
					CKladr::$NotRussia=true;
				else //if(COption::GetOptionString("ipol.kladr", "HIDELOCATION")!='Y')// ���� ��-������� ��������
				{

					CKladr::$NotRussia=false;
					//�������� ������, ��� �����, ���������, ��� � �������� � �������������� ���� ���������
					if($townname){
					//�����
						$contentType='city';
						$townname=trim(str_replace(explode(',',GetMessage("CHANGEINTOWN")), "", $townname));
						$query=urlencode($townname);
					}
					elseif($regionname){
					//������� 
						$contentType='region';
						$r = explode(" ",$regionname);
						$query=urlencode($r[0]);
					}
					else{
					//������? �� �� �������� ʊ���� �� ��������
					//���������������� ��� �������������� 2.0 ��� ������ ������
						$regionname=self::zajsonit($city["REGION_NAME"]);
						$contentType='region';
						$r = explode(" ",$regionname);
						$query=urlencode($r[0]);
					}
			 
					//������� ������ �� ������ � ������� �����
					// $query='';
					$timeOut=4;
					if(function_exists('curl_init') !== false){
						if( $curl = curl_init() )
						{//����� ����� �������
							$errAnswerdate = intval(COption::GetOptionString("ipol.kladr", "ERRWRONGANSWERDATE"));
							$fail=false;$code=false;
							
							if($errAnswerdate>0)
								if(mktime()-$errAnswerdate<900){//���� ������ 15 ����� � ������
									$fail=true;//��� ����
								}
								else
								{//����� �����
									$fail=false;
								}
							else//������ �� ����
								$fail=false;
								
							// self::PrintLog('http://kladr-api.ru/api.php?query='.$query.'&contentType='.$contentType.'&withParent=1&token='.$token);
							if(!$fail){//���� �� ���� ������
								curl_setopt($curl, CURLOPT_URL, 'http://kladr-api.ru/api.php?query='.$query.'&contentType='.$contentType.'&withParent=1&token='.$token);
								curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
								curl_setopt($curl, CURLOPT_TIMEOUT,$timeOut);
												
								$out = curl_exec($curl);
								$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
								curl_close($curl);
							}

							if($code != 200)//��� false ���� �� �� ���������� ������
							{
								// �����-�� ����
								self::Error("cUrl error. Wrong answer ".$code);
								if($code!==false){ 
									COption::SetOptionString("ipol.kladr","ERRWRONGANSWERDATE",mktime());
									COption::SetOptionString("ipol.kladr","ERRWRONGANSWER",$code);
								}
								
								CKladr::$error=true;
								//������� �����.
								return;
								
							}
							elseif($errAnswerdate){//���� ���� ������ �� ����� �� �������
								COption::SetOptionString("ipol.kladr","ERRWRONGANSWERDATE",false);
								COption::SetOptionString("ipol.kladr","ERRWRONGANSWER",false);
							}
							
							$a=(array)json_decode($out);
							$kladrid='';//����� ����� ����� �������.
							$done=false;

							if(is_array($a["result"]))
								foreach($a["result"] as $resulttown)
								{
									$resTown=(array)$resulttown;//������ � ������. ��������� ������ � ���
									// self::PrintLog($resTown);
									if($contentType=='city')
									{//� ������ ���� ��������� �������, � � �������� ���
										//1) �������� �� ���
										// if($resTown['name']!=self::zajsonit($city["CITY_NAME"])) continue;//��� �� �������
										if($resTown['name']!=self::zajsonit($townname)) continue;//��� �� �������

										//2)�������� �� ���
										// if($resTown['type']!=self::zajsonit(GetMessage("TYPE_TOWN"))) continue;//��� �� �������
											
										//3)�������� ���������
										if(!empty($resTown["parents"]))
										{
											foreach($resTown["parents"] as $parent){
												$parent=(array)$parent;
												
												//�������� �������� � ������� � �������� ����
												
												if(strpos($regionname, ' ')!==false)
												{
													$par[$parent["contentType"]."_short"]=self::toUpper($parent["name"].' '.$parent["typeShort"],'UTF-8');
													$par[$parent["contentType"]."_full"]=self::toUpper($parent["name"].' '.$parent["type"],'UTF-8');
													$par[$parent["contentType"]."_full2"]=self::toUpper($parent["type"].' '.$parent["name"],'UTF-8');
												}
												else{
													$par[$parent["contentType"]."_short"]=self::toUpper($parent["name"],'UTF-8');
													$par[$parent["contentType"]."_full"]=self::toUpper($parent["name"],'UTF-8');
													$par[$parent["contentType"]."_full2"]=self::toUpper($parent["type"],'UTF-8');
													
												}
												
												$regionname=self::toUpper($regionname,'UTF-8');
												
												$execp_city_id=array_search($regionname,GetMessage("NAME_BAD"));
												if($execp_city_id!==false){
													$ar_new_towns=GetMessage("NAME_KLADR");

													$regionname=$ar_new_towns[$execp_city_id];
												}
												
												/* self::PrintLog(
													array(
														$par[$parent["contentType"]."_short"],
														$par[$parent["contentType"]."_full"],
														$par[$parent["contentType"]."_full2"],
														$regionname, 
														$resTown['name'], 
														self::zajsonit($city["CITY_NAME"])
													),false
												); */
													
												if($par[$parent["contentType"]."_short"]==$regionname || 
													$par[$parent["contentType"]."_full"]==$regionname || 
													$par[$parent["contentType"]."_full2"]==$regionname) 
												{
													$kladrid=$resTown['id'];	
													$done=true;//������ ����������
												} 
												
											}//�� foreeach
										}	
										else
										{//��������� ��� 
											$emptyid=$resTown["id"];//������� ������
										}
									}		
									else
									{//������. ������ ������ ������ �� ����, ��� ��������� � ������� ���� �� �����, � ������ �� ����� ����
										$kladrid=$resTown["id"];break;
									}
									if($done) break;//���� ����� �����, �� �����
									
								}//endforeach	
								if(!$kladrid && $emptyid) $kladrid=$emptyid;// ���� ������� �� �������, �� ����� ��� ������� ���� ����
						}//���� ������ ������
						else{
							self::Error("cUrl error. Request lost.");
						}
					}
					else{//
						self::Error("cUrl error. cUrl not found.");
					}
					
					//��������� � ������
					// self::PrintLog($kladrid,false);
					if($kladrid)
					{
						foreach($a["result"] as $k=>$resulttown){
							
							$p=(array)$resulttown;
							if($p['id']==$kladrid) 
							{
								$townobj=json_encode($p);
							}
						}				
						self::$townid=$kladrid;
						self::$townobj=$townobj;
						self::$contentType=$contentType;
						// self::PrintLog($townobj,false);
					}
					else
					{//���� ������ �� ������ � ������, �� �� ������� // ���??
						self::Error("kladrid not found");
					}
				}
			}
			else{// $city ���� . ����� �� ������
				self::Error("Town not found");
				if((COption::GetOptionString("ipol.kladr", "NOTSHOWFORM")=='Y')) CKladr::$NotRussia=true;
			}
			
		$obj=self::GetKladrSetText('obj');
		if(!$obj) $obj='arg:{}';
		
		echo '<script>';
		echo '$(document).ready(function(){
				KladrJsObj.FormKladr({'.$obj.',"ajax":false});
				KladrJsObj.checkErrors();
			});';
		echo '</script>';
	}
	
	function OnEndBufferContentHandler(&$content)
	{//�������� ����� �������� �� ������ �����
		if((COption::GetOptionString("ipol.kladr", "FUCK")=='Y')) return;
		if (defined("ADMIN_SECTION") || ADMIN_SECTION===true || strpos($_SERVER['PHP_SELF'], "/bitrix/admin") === true) return false;
		
		$noJson=self::no_json($content);
		
		$error= CKladr::$error;
		if($error)$NotRussia=true;//������ ������ ������� �����
		if(/*($_REQUEST['is_ajax_post'] == 'Y' || $_REQUEST["AJAX_CALL"] == 'Y' || $_REQUEST["ORDER_AJAX"]) && */$noJson){//�� �������
			CKladr::$townid=false;
			$text=self::GetKladrSetText('text');
			// self::PrintLog($text);
			
			if($text)
				$content.= $text;
		}elseif($_REQUEST['action'] == 'refreshOrderAjax' && !$noJson){
			$text=self::GetKladrSetText('obj');
			// $text='"kladr":{}';
			
			if($text)
				$content = substr($content,0,strlen($content)-1).','.$text.'}';
		} 	
	}
	
	function GetKladrSetText($type){
		// return '"kladr":{}';
		$kltobl= CKladr::$townobj;//self::zajsonit(
		$NotRussia= CKladr::$NotRussia;
		
		$contentType= CKladr::$contentType;
		$kladrid= CKladr::$townid;

		$text='';
		if($kltobl || $NotRussia){
			if($type=='text'){
				if($NotRussia)
					$text .='<input type="hidden" value="'.$NotRussia.'" class="NotRussia" name="NotRussia"/>';
				if($kltobl)
					$text .='<div style="display:none;" class="kltobl" >'.$kltobl.'</div>';
			}
			elseif($type=='obj'){
				$text='"kladr":{"kltobl":"'.addslashes($kltobl).'","NotRussia":"'.$NotRussia.'"}';	
			}
		}

		return $text;		
	} 
	
	function ParseAddr($adr){
		$ans = array();
		$containment = explode(",",$adr);
		$numCon=count($containment);
		if(is_numeric(trim($containment[0]))) $start = 2;
		else $start = 1;		
		if($numCon-$start == 3){
			$ans['town'] = trim($containment[$start]);
			$start++;
		}
		if($containment[$start]){$ans['line'] = trim($containment[$start]);}
		if($containment[($start+1)]){ $containment[($start+1)] = trim($containment[($start+1)]); $ans['house'] = trim(substr($containment[($start+1)],strpos($containment[($start+1)]," ")));}
		if($containment[($start+2)]){ $containment[($start+2)] = trim($containment[($start+2)]); $ans['flat']  = trim(substr($containment[($start+2)],strpos($containment[($start+2)]," ")));}
		
		return $ans;
	}
	
	function no_json($wat){
		return is_null(json_decode(self::zajsonit($wat),true));
	}
	
	
	
	function getCityNameByID($locationID)
	{
		if(method_exists("CSaleLocation","isLocationProMigrated") && CSaleLocation::isLocationProMigrated())
		{//���� ������������� 2.0
			
			//�������� id �� ����, ���� ��� ���
			if (strlen($locationID) > 8)
				$cityID = CSaleLocation::getLocationIDbyCODE($locationID);
			else
				$cityID = $locationID;
			
			//�������� ��� �������
			$res = \Bitrix\Sale\Location\LocationTable::getList(array(
				'filter' => array(
					'=ID' => $cityID, 
					'=PARENTS.NAME.LANGUAGE_ID' => LANGUAGE_ID,
					'=PARENTS.TYPE.NAME.LANGUAGE_ID' => LANGUAGE_ID,
					'!PARENTS.TYPE.CODE' => 'COUNTRY_DISTRICT',
					'I_TYPE_CODE'=>array("COUNTRY","REGION","CITY"),
				),
				'select' => array(
					'I_ID' => 'PARENTS.ID',
					'I_NAME_RU' => 'PARENTS.NAME.NAME',
					'I_TYPE_CODE' => 'PARENTS.TYPE.CODE',
					'I_TYPE_NAME_RU' => 'PARENTS.TYPE.NAME.NAME'
				),
				'order' => array(
					'PARENTS.DEPTH_LEVEL' => 'asc'
				)
			));
			
			while($item = $res->fetch())
			{	
				$arCity[$item["I_TYPE_CODE"]."_NAME"]=$item["I_NAME_RU"];//���� ������ ���� 2, �� ����������� �� ���������
			}
			if(!array_key_exists ("CITY_NAME",$arCity))	
				$arCity["CITY_NAME"]=$arCity["REGION_NAME"];
			
		}
		else
			$arCity = CSaleLocation::GetByID($locationID);
	  
	    return $arCity;
	}
	
	
}//�� ����� 

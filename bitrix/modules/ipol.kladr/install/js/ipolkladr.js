var test=false;
var time = new Date();
// var kladripoladmin = false;

var KladrJsObj={
	kladrdivid:"ipolkladrform",//div в котором форма кладр
	container:false,
	city :false,
	street :false,
	building :false,
	room :false,
	noreload:false,
	arAdrSeq:['region','city','street','building'],
	arHiddenProps:[],
		
	ipolkladrfname:false,//имя поля адреса
	ipolkladrlocation:false,//имя поля местоположение
	roomnum:false,
	
	lastobject: {},//для хранения объектов кладр в время аякс запросов в компоненте
	
	map: null,
	map_created: false,
	placemark : null,
	
	kladrtownid:false,
	
	fancyForm:false,
	// needSubmit:false,
	
	FormKladr: function(Klobj){//вызывает функции которые установят кладр, ajax - true или false
		// return;
		ajax=false;
		if(Klobj && !$.isEmptyObject(Klobj) && Klobj.ajax)
			ajax=Klobj.ajax;
		
		// console.log($('.kltobl:last').text());
		// console.log(Klobj);
		
		var notRussia=false;
		if(!$.isEmptyObject(Klobj) && !$.isEmptyObject(Klobj.kladr)){
			notRussia=Klobj.kladr.NotRussia;	
		}
		else{
			notRussia=$('.NotRussia:last').val();
			$('.NotRussia:last').remove();
		}
		
		if(notRussia==true)//ушатываем кладр, если это не Россия
			{KladrJsObj.FuckKladr();return;}
		
		
		
		//Если не пришло по-новому, пробуем по старому
		if(typeof(Klobj.kladr)=='undefined'){
			Klobj.kladr={'kltobl':{}};
		}

		if($('.kltobl:last').length && $.isEmptyObject(Klobj.kladr.kltobl))
			{
				// console.log(Klobj);
				t=$('.kltobl:last').text();
				Klobj.kladr.kltobl=t?JSON.parse(t):{};
				$('.kltobl:last').remove();
			}
		else{
			if(typeof(Klobj.kladr.kltobl)=='string' && Klobj.kladr.kltobl!='')
				Klobj.kladr.kltobl=JSON.parse(Klobj.kladr.kltobl);
			}// Теперь город в Klobj.kladr.kltobl
		// console.log(Klobj.kladr.kltobl);
		
		//Итак ставим кладр для Klobj.kladr.kltobl
		
		
		//определяем поле адреса
		KladrJsObj.ipolkladrfname=false;
		KladrJsObj.prop_forms=new Array();
		
		if(typeof(KladrSettings.arNames)!="undefined"){
			KladrSettings.arNames.forEach(function(entry) {//определим какое поле из списка полей присутствует(поля для физиков и юриков)
				// if(!KladrJsObj.ipolkladrfname){//определим форму из массива форм
					// if($('[name ='+entry+']').length>0) KladrJsObj.ipolkladrfname=entry;
				// }
				if($('[name ='+entry+']').length>0) 
					KladrJsObj.prop_forms[KladrJsObj.prop_forms.length]=entry;
				
			});
			KladrJsObj.ipolkladrfname=KladrJsObj.prop_forms.shift();
		}
		else console.warn('ipol.kladr error: no address fields found');
		
		if(typeof(KladrJsObj.ipolkladrfname)=="undefined" || KladrJsObj.ipolkladrfname==false){
			console.warn('ipol.kladr error: ipolkladrfname not found');
			return;
		}
		
		//определяем поле местоположения
		KladrJsObj.ipolkladrlocation=false;
		/* if(typeof(KladrSettings.arLocatinos)!="undefined"){
			KladrSettings.arLocatinos.forEach(function(entry) {//определим какое поле из списка полей присутствует(поля для физиков и юриков)
				entryname="ORDER_PROP_"+entry;
				if(!KladrJsObj.ipolkladrlocation){//определим форму из массива форм
					if($('[name ='+entryname+']').length>0) 
					{
						KladrJsObj.ipolkladrlocation=entryname;
						KladrJsObj.ipolkladrlocationid=entry;
					}
				}
			});	
		}
		else console.warn('ipol.kladr error: location not found'); */
		
		//определяем поле индекса
		KladrJsObj.ipolkladrzip=false;
		/*if(typeof(KladrSettings.arZips)!="undefined"){
			KladrSettings.arZips.forEach(function(entry) {//определим какое поле из списка полей присутствует(поля для физиков и юриков)
				if(!KladrJsObj.ipolkladrzip){//определим форму из массива форм
					if($('[name ='+entry+']').length>0) KladrJsObj.ipolkladrzip=entry;
				}
			});		
		}*/
		
		
		
		if(!$.isEmptyObject(Klobj.kladr) && !$.isEmptyObject(Klobj.kladr.kltobl) )
		{//восстановление старого объекта 
			KladrJsObj.UnBlockAdrProps();//убирает readonly - срабатывает по аяксу если не было начального города
			
			obj=Klobj.kladr.kltobl;
			if($.isEmptyObject(KladrJsObj.lastobject) || obj.id!=KladrJsObj.kladrtownid ){
				//если это новая страница или если город поменялся

				KladrJsObj.lastobject={};
				KladrJsObj.lastobject[obj.contentType]=obj;
				KladrJsObj.roomnum='';
										
				if(KladrJsObj.kladrtownid) 
					KladrJsObj.CleanAdrProps();//чистим адрем
				
				KladrJsObj.kladrtownid=obj.id;
				KladrJsObj.contentType=obj.contentType;

			}	
			
		}
		else //если объект пуст
			if($.isEmptyObject(KladrJsObj.lastobject) && KladrSettings.notShowForm){
				//если не нашли город, и не надо показывать форму
				KladrJsObj.SetAdrProp(KladrJsObj.ipolkladrfname,BX.message('RunFormnoktoblattr'));
				KladrJsObj.BlockAdrProps();
				return;
			}
		
			
		
		//узнаем, что там с адресом
		var adr="";	
		adr=$('[name ='+KladrJsObj.ipolkladrfname+']').val(); // Смотрим по улице, она всегда есть
		if(adr==BX.message('RunFormnoktoblattr')){
			adr='';
			$('[name ='+KladrJsObj.ipolkladrfname+']').val('');
		}	
		
		if(adr) //если есть предустановленный адрес
		{	//тогда не делать кладр, а выводить кнопку "изменить адрес"
			KladrJsObj.prop_forms.forEach(function(entry) {
				adr+=', '+$('[name ='+entry+']').val();
			});
			
			KladrJsObj.BlockAdrProps();
			
			$('[name ='+KladrJsObj.ipolkladrfname+']').after('<br><a class="nobasemessage" href="javascript:KladrJsObj.nobasemessage();">'+BX.message('CHANGEADR')+'</a><br>');
			
			// if(KladrSettings.HideLocation) KladrJsObj.HideOldProps();

		}
		else{//адреса нет
			KladrJsObj.PrintForm();			
			KladrJsObj.RunForm();
		}
		

	},

	PrintForm: function(){//выводит html код формы	
		KladrJsObj.map_created=false;//карты пока еще нет
		
		var inpclass,//класс для улицы
			disabled;//запрет для улиц
		
		
		inpclass = KladrJsObj.contentType=='city' ? "top" : "middleinput";
			
		formK = '<div id="'+KladrJsObj.kladrdivid+'" class="ipolkladrform">';
        
		// if(KladrSettings.HideLocation) formK+='<div class="changeRussia">'+BX.message('RUSSIA_NAME')+' <a class="" href="javascript:KladrJsObj.FuckKladr();"> '+BX.message('CHANGE_RUSSIA')+'</a></div>';
        
		oncl = '';
		if(KladrSettings.MakeFancy) oncl = 'javascript:KladrJsObj.FancyForm();';
		
		formK+='<div class="fancyback">';
		formK+='	</div><!-- от fancy -->';
		
			formK+='<div class="fancyform">';
			formK+='	<form class="js-form-address" onclick="'+oncl+'">';
				//город
				if(KladrJsObj.contentType!='city'){
					formK+='<div class="top"><input name="city" type="text" placeholder="'+BX.message('TAPETOWN')+'"></div>';	
					disabled = 'disabled="disabled"';
				}
				
				//улица
				if(KladrJsObj.lastobject.street) disabled = '';
				if(KladrJsObj.prop_forms.length==3) disabled += ' name="'+KladrJsObj.ipolkladrfname+'" id="'+KladrJsObj.ipolkladrfname+'" ';
				formK+='   <div class="'+inpclass+'"><input name="street" type="text" placeholder="'+BX.message('TAPESTREET')+'" '+ disabled +'></div>';
				
				//дом
				disabled = 'disabled="disabled"';
				if(KladrJsObj.lastobject.building) disabled = '';
				if(KladrJsObj.prop_forms.length==3) disabled += ' name="'+KladrJsObj.prop_forms[0]+'" id="'+KladrJsObj.prop_forms[0]+'" ';
				formK+='   <div class="bottom"><input name="building" type="text" placeholder="'+BX.message('TAPENUMPER')+'" '+disabled+'>';
				
				//квартира
				disabled = 'disabled="disabled"';
				if(KladrJsObj.lastobject.room) disabled = '';
				if(KladrJsObj.prop_forms.length==3) disabled += ' name="'+KladrJsObj.prop_forms[2]+'" id="'+KladrJsObj.prop_forms[2]+'" ';
				// formK+='<input class="room" type="text" placeholder="'+BX.message('TAPEROOM')+'" name="room" disabled="disabled"></div>';
				formK+='<input class="room placeholdered" type="text" name="room" disabled="disabled" value="'+BX.message('TAPEROOM')+'"></div>';
			   
			formK+=' </form>';
			
			//карта 
			if(KladrSettings.ShowMap) formK+=' <div id="map" class="panel-map"></div>';
			if(KladrSettings.ShowAddr){	
				formK+=' <div class="addition">';
				formK+='   <div class="block">';
				// formK+='     <p class="title">Текстовое представление адреса</p>';
				formK+='     <p id="address" class="value"></p>   ';                 
				formK+='   </div>';
				formK+='   <div class="block" style="display:none;">';
				formK+='                 <p class="title">Выбранный объект</p>';
				formK+='                 <ul class="js-log">';
				formK+='                     <li id="id" style="display: none;"><span class="name">Код:</span> <span class="value"></span></li>';
				formK+='                     <li id="zip" style="display: none;"><span class="name">Почтовый индекс:</span> <span class="value"></span></li>';
				formK+='                     <li id="name" style="display: none;"><span class="name">Название:</span> <span class="value"></span></li>';
				formK+='                     <li id="type" style="display: none;"><span class="name">Подпись:</span> <span class="value"></span></li>';
				formK+='                     <li id="typeShort" style="display: none;"><span class="name">Подпись коротко:</span> <span class="value"></span></li>';
				formK+='                     <li id="contentType" style="display: none;"><span class="name">Тип объекта:</span> <span class="value"></span>';
				formK+='                     <li id="okato" style="display: none;"><span class="name">ОКАТО:</span> <span class="value"></span>';
				formK+='                 </ul>';
				formK+='   </div>';
				formK+='  </div>';
			}
			if(KladrSettings.MakeFancy) formK+='<div class="unfancybutton" onclick="KladrJsObj.UnFancyForm()">'+BX.message('SAVEADR')+'</div>';
		formK+='	</div><!-- от fancy -->';
		formK+='</div><!-- от '+KladrJsObj.kladrdivid+' -->';
		formK+='<input name="ipolkladrnewcity" class="ipolkladrnewcity" type="hidden">';
		formK+='<input name="ipolkladrnewregion" class="ipolkladrnewregion" type="hidden">';
		formK+='<input name="ipolkladrlocation" class="ipolkladrlocation" type="hidden">';

		$('[name ='+KladrJsObj.ipolkladrfname+']').after(formK);
		// ddelivery_DDeliveryObj.oldTemplate = $('#ORDER_FORM').length;
		// if(KladrSettings.HideLocation) KladrJsObj.HideOldProps();//убираем местоположение
		// $('[name ='+ KladrJsObj.ipolkladrfname+']').hide();//убираем адрес
		KladrJsObj.HideAdrProps();
	},

	RunForm: function(){
		//ищем вставленный html формы
		KladrJsObj.container = $('#'+KladrJsObj.kladrdivid);
		if(!KladrJsObj.container.length)
		{
			console.warn('kladrdivid not found');
			return;
		}
		
		//ищем поля в форме
		KladrJsObj.city = KladrJsObj.container.find('[name="city"]');
		KladrJsObj.street = KladrJsObj.container.find('[name="street"]');
		KladrJsObj.building = KladrJsObj.container.find('[name="building"]');
		KladrJsObj.room = KladrJsObj.container.find( '[name="room"]' );
		
		//первая установка
		$.kladr.setDefault({
			parentInput: '.js-form-address',
			verify: true,
			labelFormat: function (obj, query) {
				var label = '';

				var name = obj.name.toLowerCase();
				query = query.name.toLowerCase();

				var start = name.indexOf(query);
				start = start > 0 ? start : 0;

				if (obj.typeShort) {
					label += obj.typeShort + '. ';
				}

				if (query.length < obj.name.length) {
					label += obj.name.substr(0, start);
					label += '<strong>' + obj.name.substr(start, query.length) + '</strong>';
					label += obj.name.substr(start + query.length, obj.name.length - query.length - start);
				} else {
					label += '<strong>' + obj.name + '</strong>';
				}

				if (obj.parents) {
					for (var k = obj.parents.length - 1; k > -1; k--) {
						var parent = obj.parents[k];
						if (parent.name) {
							if (label) label += '<small>, </small>';
							label += '<small>' + parent.name + ' ' + parent.typeShort + '.</small>';
						}
					}
				}

				return label;
			},
			change: function (obj) {
				
				if(obj){
					//пишем объект
					$.kladr.getAddress('.js-form-address', function (objs) {
						$.extend(KladrJsObj.lastobject, objs);
						// KladrJsObj.lastobject = $.extend({},KladrJsObj.lastobject,objs);
						// KladrJsObj.lastobjectstring= KladrJsObj.lastobject;
						
						// KladrJsObj.lastobject=Object.assign(KladrJsObj.lastobject, objs);
						// KladrJsObj.lastobjectstring=Object.assign(KladrJsObj.lastobject, objs);
						

						// $('.ipolkladrlastobject').text(JSON.stringify(KladrJsObj.lastobject));
					});
					
					//изменен город
					if(obj.contentType==$.kladr.type.city ){
						if(!KladrJsObj.noreload && KladrSettings.HideLocation ){
							//если по аяксу перезагрузили то когда будем ставить умолчания пропустим это и попадем в обновления карты и адреса
							
							//чистить улицу и дом
							//if(true){// если город изменился
								KladrJsObj.lastobject={'city':KladrJsObj.lastobject.city};
								KladrJsObj.roomnum=false;
							//}
							if(KladrJsObj.ipolkladrlocation){//если есть куда писать
								$('.ipolkladrlocation').val(KladrJsObj.ipolkladrlocation);//чтобы php знал какой реквест ловить
								//запишем реквест
								$('.ipolkladrnewcity').val((obj.name));
								
								if(!$.isEmptyObject(obj.parents) && obj.parents[0].type!=BX.message('town')) 
								{//узнаем область				
									if(obj.parents[0].type==BX.message('RESP'))
										cityregion=obj.parents[0].type+' '+obj.parents[0].name;
									else
										cityregion=obj.parents[0].name+' '+obj.parents[0].type;
									$('.ipolkladrnewregion').val((cityregion));
								}
								
								if(!KladrJsObj.fancyForm) submitForm();
								// else KladrJsObj.needSubmit=true;
								// return;
							}	
						}
						else{
							KladrJsObj.street.removeAttr("disabled");
						}
					}
						
					/*убрать проверку на obj*/
					switch (obj.contentType) {
							case $.kladr.type.city:
							KladrJsObj.street.removeAttr("disabled");
							break;

						case $.kladr.type.street:
							KladrJsObj.building.removeAttr("disabled");
							break;

						case $.kladr.type.building:
							KladrJsObj.room.removeAttr("disabled");
							break;
					} 
			
					setLabel($(this), obj.type);
				}
				
				KladrJsObj.log(obj);
				KladrJsObj.addressUpdate();
				KladrJsObj.mapUpdate();

			},
			
			check: function (obj) {
				// console.log($(this).attr("name"));
				switch ($(this).attr("name")) {
					case $.kladr.type.city:
						if(!obj) $(this).val('');
						// else KladrJsObj.street.removeAttr("disabled");
					break;

					case $.kladr.type.street:
						KladrJsObj.building.removeAttr("disabled");
					break;

					case $.kladr.type.building:
						KladrJsObj.room.removeAttr("disabled");
					break;
					
				
				}
				if(!obj) KladrJsObj.addressUpdate();
			},
			checkBefore: function () {
				var $input = $(this);

				if (!$.trim($input.val())) {
					
					KladrJsObj.log(null);
					KladrJsObj.addressUpdate();
					KladrJsObj.mapUpdate();
					return false;
				}
				
			}
		});
		
		//иницируем форму
		if(KladrJsObj.city) KladrJsObj.city.kladr({'type': $.kladr.type.city});
		if(KladrJsObj.contentType=='region')//если тип есть и он область
			KladrJsObj.city.kladr({'parentType': 'region','parentId':KladrJsObj.kladrtownid});
		
		KladrJsObj.street.kladr('type', $.kladr.type.street); 
		if(KladrJsObj.contentType=='city')//если тип есть и он город
			KladrJsObj.street.kladr({'parentType': 'city','parentId':KladrJsObj.kladrtownid});
		
		KladrJsObj.building.kladr('type', $.kladr.type.building);
		
		// Включаем получение родительских объектов для населённых пунктов
		KladrJsObj.city.kladr('withParents', true);
		KladrJsObj.street.kladr('withParents', true);

		// Отключаем проверку введённых данных для строений
		// KladrJsObj.building.kladr('verify', false);
		
		//если карты еще нет поставим ее
		if(KladrSettings.ShowMap && !KladrJsObj.map_created){
			ymaps.ready(function () {
				if (KladrJsObj.map_created) return;
				KladrJsObj.map_created = true;
				
				KladrJsObj.map = new ymaps.Map('map', {
					center: [55.76, 37.64],
					zoom: 2,
					controls: []
				});

				KladrJsObj.map.controls.add('zoomControl', {
					position: {
						right: 10,
						top: 10
					}
				});
				
				//и расставляем ранее стоявшие, как карта загрузилась
				KladrJsObj.setFromDefaultObj();
			});
		}
		else{
			KladrJsObj.setFromDefaultObj();
		}
		
		// если менялся город давай выполним функцию
		// if(KladrJsObj.fancyForm)
		// {
			// KladrJsObj.fancyForm=false;
			// if(typeof(UnFancyKladr)=="function") UnFancyKladr();
		// }
		
		function setLabel($input, text) {
			text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
			$input.parent().find('label').text(text);
		}
		
		KladrJsObj.room.focusout(function(){//ввели номер квартиры
			// записать комнату
			KladrJsObj.roomnum = KladrJsObj.room.val();
			if(KladrJsObj.roomnum && KladrJsObj.roomnum!=BX.message('TAPEROOM')){
				KladrJsObj.addressUpdate();
			}
			else{
				KladrJsObj.room.val(BX.message('TAPEROOM'));
				KladrJsObj.room.addClass('placeholdered');
			}
				
			
		});
		
		KladrJsObj.room.focusin(function(){//ввели номер квартиры
			// записать комнату
			if(KladrJsObj.room.val()==BX.message('TAPEROOM')){
				KladrJsObj.room.val('');
				KladrJsObj.room.removeClass('placeholdered');
			}		
		});
	},//не RunForm
	
	
	log: function(obj) {
		var log, i;

		$('.js-log li').hide();

		for (i in obj) {
			log = $('#' + i);

			if (log.length) {
				log.find('.value').text(obj[i]);
				log.show();
			}
		}
	},
		
	mapUpdate: function() {
		if(!KladrSettings.ShowMap) return;
		var zoom = 2;
		
		var address = $.kladr.getAddress('.js-form-address', function (objs) {
			var result = '';
			
			if(!KladrSettings.HideLocation){
				//при работе в старом режиме нет городов или областей, поэтому сначала дописываем
				zoom = 7;
				// objs=Object.assign(KladrJsObj.lastobject, objs);
				objs = $.extend({},KladrJsObj.lastobject,objs);
			} 
			
			KladrJsObj.arAdrSeq.forEach(function(item, i, arr) {
				if(!$.isEmptyObject(objs[item])){
					obj=objs[item];
					
					var name = '',
						cityregion = '',
						type = '';
						
					if ($.type(obj) === 'object') {
						name = obj.name;
						type = ' ' + obj.type;

						switch (obj.contentType) {
							/* case $.kladr.type.region:
								zoom = 4;
								break;

							case $.kladr.type.district:
								zoom = 7;
								break; */

							case $.kladr.type.city:
								zoom = 10;
								break;

							case $.kladr.type.street:
								zoom = 13;
								break;

							case $.kladr.type.building:
								zoom = 16;
								break;
						}
					}
					else {
						name = obj;
					}
					
					if (obj.contentType == $.kladr.type.city && !$.isEmptyObject(obj.parents)) 
						cityregion=obj.parents[0].typeShort+'. '+obj.parents[0].name;
					
					
					if (result) result += ', ';
					if(cityregion) result += cityregion +', '
					result += type +' '+ name;
				}
			});

			return result;
		});			
		if(!address){
			address=BX.message('RF');
			zoom=2;
		}
		
		if (address && KladrJsObj.map_created) {
			var geocode = ymaps.geocode(address);
			geocode.then(function (res) {
				KladrJsObj.map.geoObjects.each(function (geoObject) {
					KladrJsObj.map.geoObjects.remove(geoObject);
				});

				var position = res.geoObjects.get(0).geometry.getCoordinates();
				
				KladrJsObj.placemark = new ymaps.Placemark(position, {}, {});
					
				KladrJsObj.map.geoObjects.add(KladrJsObj.placemark);
				KladrJsObj.map.setCenter(position, zoom);
			});
		}
	},

	addressUpdate: function() {
		var address = $.kladr.getAddress('.js-form-address',function (objs) {
			var result = '',
				zip='';
				
			if(!KladrSettings.HideLocation){
				//при работе в старом режиме нет городов или областей, поэтому сначала дописываем
				zoom = 7;
				// objs=Object.assign(KladrJsObj.lastobject, objs);
				objs = $.extend({},KladrJsObj.lastobject,objs);
			} 
			
			if(Object.keys(objs).length <=1) return ''; // не вписывать город в адрес
			
			KladrJsObj.arAdrSeq.forEach(function(item, i, arr) {
				if(!$.isEmptyObject(objs[item])){
					obj=objs[item];
					var name = '',
						cityregion = '',
						type = '';

					if ($.type(obj) === 'object') {
						name = obj.name;
						type = ' ' + obj.typeShort+'.';
						if(obj.zip)
							zip = obj.zip;
						
						if(KladrSettings.HideLocation){
							//дописать область
							if (obj.contentType == $.kladr.type.city && !$.isEmptyObject(obj.parents) && obj.parents[0].type!=BX.message('town')) 
								cityregion=obj.parents[0].typeShort+'. '+obj.parents[0].name;
						}
					}
					else {
						name = obj;
					}

					if (result) result += ', ';
					if(cityregion) result += cityregion +', '
					result += type +' '+ name;
				}
			});
			
			if(zip) 
			{
				if($('[name='+KladrJsObj.ipolkladrzip+']')) $('[name='+KladrJsObj.ipolkladrzip+']').val(zip);//фигня, нужно еще запускать смену скрипта
				result=zip+', '+result;
			}
			if(KladrJsObj.roomnum) result+=', '+BX.message('kv')+KladrJsObj.roomnum;
			
			return result;
		});
		
		KladrJsObj.WriteAdr(address);
		/* if($('textarea[name ='+KladrJsObj.ipolkladrfname+']').length){
			// if($('#ORDER_FORM').length)
				$('textarea[name ='+KladrJsObj.ipolkladrfname+']').text(address);
			// else
				$('textarea[name ='+KladrJsObj.ipolkladrfname+']').val(address);	
		}
		if($('input[name ='+KladrJsObj.ipolkladrfname+']').length) $('input[name ='+KladrJsObj.ipolkladrfname+']').val(address); */
		$('#address').text(address);
	},
		
	setFromDefaultObj: function(){
		if(!$.isEmptyObject(KladrJsObj.lastobject)){
			
			$.each(KladrJsObj.lastobject, function (i, obj){
				if(typeof(obj) =='string'){
					KladrJsObj["street"].val(obj);
				}
			});
			
			KladrJsObj.noreload=true;
			$.kladr.setValues(KladrJsObj.lastobject);
			KladrJsObj.noreload=false;
			
			if(!KladrSettings.HideLocation) 
			{//раньше не работало, может оно и не нужно?!!
				KladrJsObj.mapUpdate();
				KladrJsObj.addressUpdate();
			}
			
			if(KladrJsObj.roomnum) KladrJsObj.room.val(KladrJsObj.roomnum);
			
		}
		else{
			KladrJsObj.mapUpdate();
			KladrJsObj.addressUpdate();
		}
	},
		
	
	HideAdrProps: function(){
		$.each(KladrJsObj.prop_forms, function (i, obj){//адрес трем совсем
			// KladrJsObj.arHiddenProps
			// cl=$('[data-property-id-row='+obj.substr(11)+']').find('input').attr('name')+'_';
			// $('[data-property-id-row='+obj.substr(11)+']').find('input').attr('name',cl);
			
			
			// $('[data-property-id-row='+obj.substr(11)+']').find('input').remove();
			// KladrJsObj.arHiddenProps[KladrJsObj.arHiddenProps.length]=el;
			
			$('[data-property-id-row='+obj.substr(11)+']').hide();
		});
		$('[name ='+ KladrJsObj.ipolkladrfname+']').hide();//поле улицы только стираем
		
		
		$('[data-property-id-row='+KladrJsObj.ipolkladrfname.substr(11)+']').find('.label').text('Адрес доставки');	
		
	},
	
	ShowAdrProps: function(){
		if(typeof(KladrJsObj.prop_forms)!='undefined' && KladrJsObj.prop_forms.length){
			$.each(KladrJsObj.prop_forms, function (i, obj){
				
				// cl=$('[data-property-id-row='+obj.substr(11)+']').find('input').attr('name');
				// cl=cl.substr(0,cl.length-1);
				// $('[data-property-id-row='+obj.substr(11)+']').find('input').attr('id',cl);
				
				$('[data-property-id-row='+obj.substr(11)+']').show();	
			});
		}
		if(KladrJsObj.ipolkladrfname){
			$('[name ='+ KladrJsObj.ipolkladrfname+']').show();
			$('[data-property-id-row='+KladrJsObj.ipolkladrfname.substr(11)+']').find('.label').text('Улица');
		}
	},
	
	BlockAdrProps: function(){
		console.log('BlockAdrProps');
		a=$.merge([],KladrJsObj.prop_forms);
		a=$.merge(a,Array(KladrJsObj.ipolkladrfname));
		$.each(a, function (i, obj){
			$('[name ='+ obj+']').attr("readonly","readonly");
		});
	
	},
	UnBlockAdrProps: function(){
		a=[];
		if(typeof(KladrJsObj.ipolkladrfname)!='undefined' && KladrJsObj.ipolkladrfname)
			a=$.merge(a,Array(KladrJsObj.ipolkladrfname));
		
		if(typeof(KladrJsObj.prop_forms)!='undefined' && KladrJsObj.prop_forms.length)
			a=$.merge(a,KladrJsObj.prop_forms);
		
		if(a.length){	
			$.each(a, function (i, obj){
			// $.each($.merge([],KladrJsObj.prop_forms,Array(KladrJsObj.ipolkladrfname)), function (i, obj){
				console.log(obj);
				if($('[name ='+ obj+']').attr("readonly")=='readonly') $('[name ='+obj+']').removeAttr("readonly");
			});
		}
	},
	
	CleanAdrProps: function(){
		a=$.merge([],KladrJsObj.prop_forms);
		allProps=$.merge(a,Array(KladrJsObj.ipolkladrfname));
		$.each(allProps, function (i, obj){
		
		// $.each($.merge([],KladrJsObj.prop_forms,Array(KladrJsObj.ipolkladrfname)), function (i, obj){
			// console.log(obj);
			KladrJsObj.SetAdrProp(obj,'');
		});
	},
	SetAdrProp:function(prop,val){		
		if($('textarea[name ='+prop+']').length){
			// if($('#ORDER_FORM').length)
				$('textarea[name ='+prop+']').text(val);
			// else
				$('textarea[name ='+prop+']').val(val);	
		}
		if($('input[name = '+prop+']').length) $('input[name ='+prop+']').val(val);
	},
	
	WriteAdr: function(address){
		// console.log(address);
		if(KladrJsObj.prop_forms.length>0){
			adr=address.split(',');
			if(typeof(adr[2])!='undefined');
				KladrJsObj.SetAdrProp(KladrJsObj.ipolkladrfname,adr[2]);//substr(11)
			if(typeof(adr[3])!='undefined');
				KladrJsObj.SetAdrProp(KladrJsObj.prop_forms[0],adr[3]);
			if(typeof(adr[4])!='undefined');
				KladrJsObj.SetAdrProp(KladrJsObj.prop_forms[2],adr[4]);
		}
		else{
			KladrJsObj.SetAdrProp(KladrJsObj.ipolkladrfname,address)
		}
	},
	
	
	
	HideOldProps: function(){
		// if($('.KladrHideThisLocation').length) $('.KladrHideThisLocation').hide();
		// else $('[data-property-id-row='+KladrJsObj.ipolkladrlocationid+']').hide();
	},
	
	ShowOldProps: function(){
		// if($('.KladrHideThisLocation').length) $('.KladrHideThisLocation').show();
		// else $('[data-property-id-row='+KladrJsObj.ipolkladrlocationid+']').show();
	},
	
	FancyForm: function(){
		// $('#'+KladrJsObj.kladrdivid).find('form').addClass('fancyform');
		if(!KladrJsObj.fancyForm)
		{
			$('.fancyback').fadeIn();
			$('.addition').addClass('fancyadd');
			$('.unfancybutton').addClass('fancybut');
			$('.fancyform').css('zIndex',10000001);
			$('#kladr_autocomplete ul').css('zIndex',10000001);
			$('#kladr_autocomplete .spinner').css('zIndex',10000001);
					
			
			KladrJsObj.fancyForm=true;
		}
	},
	
	UnFancyForm: function(){
		if(KladrJsObj.fancyForm)
		{
			$('.fancyback').fadeOut('fast',function(){
				$('.addition.fancyadd').removeClass('fancyadd');
				$('.unfancybutton.fancybut').removeClass('fancybut');
				$('.fancyform').css('zIndex',1);
				$('#kladr_autocomplete ul').css('zIndex',9999);
			$('#kladr_autocomplete .spinner').css('zIndex',9999);
			
				if(typeof(UnFancyKladr)=="function") UnFancyKladr();
				KladrJsObj.fancyForm=false;
				// console.log($('.ipolkladrnewcity').val());
				
				// console.log(KladrJsObj.needSubmit);
				// if(KladrJsObj.needSubmit){
					// KladrJsObj.needSubmit=false;
					// если менялся город давай выполним функцию
					if(typeof(KladrJsObj.submitKladr)=='function')
						KladrJsObj.submitKladr({'fulladdr':$('#address').text(),"kladrobj":KladrJsObj.lastobject});	
					else
						submitForm();
					
				// }
			});
		}
	},
	
	
	nobasemessage: function(){
		//вызывается если нажать на кнопку "изменить адрес", убирает эту кнопку и вызывает форму
		KladrJsObj.UnBlockAdrProps(); // $('[name ='+ KladrJsObj.ipolkladrfname+']').removeAttr("readonly");
		KladrJsObj.CleanAdrProps(); // $('[name ='+ KladrJsObj.ipolkladrfname+']').val("");
		$('.nobasemessage').remove();
		$('.nobasemessage_adr').remove();
		KladrJsObj.FormKladr({"ajax":false});//перезагружаем стартовы с пустым адресом
	},
	
	checkErrors: function(){
		// if(typeof(kladripoladmin)!="undefined"){
			if(KladrSettings.kladripoladmin){
				if (!window.jQuery) {alert(BX.message('nojquery'));}
				if (typeof($.kladr)!="object") {alert(BX.message('nokladrobj'));}
			}
		//}
	},

	FuckKladr: function(){
		//убрать кнопку или форму
		if($('#'+KladrJsObj.kladrdivid).length) $('#'+KladrJsObj.kladrdivid).remove();//убрать форму
		else if($('.nobasemessage').length) $('.nobasemessage').remove();//убрать кнопку
		//вернуть локейшн
		// KladrJsObj.ShowOldProps();
		//вернуть адрес
		KladrJsObj.ShowAdrProps();
		KladrJsObj.UnBlockAdrProps();
	},
	
	setall: function(ajaxAns){//срабатывает по ajax
		// console.log(ajaxAns);
		// console.log(StartKladrObj);
		//вставить форму
		// return;
		if(typeof(StartKladrObj)!= 'undefined' && !$.isEmptyObject(StartKladrObj) ){
			obj = StartKladrObj;
			StartKladrObj=false;
		}
		else
			obj = {'ajax':true};
		
		var newTemplateAjax = (typeof(ajaxAns) != 'undefined' && ajaxAns !== null && typeof(ajaxAns.kladr) == 'object') ? true : false;
		if(newTemplateAjax){
			KladrJsObj.FuckKladr();
			obj['kladr']=ajaxAns.kladr;
		}
		
		
		
		if($('#'+KladrJsObj.kladrdivid).length || $('.nobasemessage').length) return;
		KladrJsObj.FormKladr(obj);
	},
}



$(document).ready(function(){
	if(typeof BX !== 'undefined' && BX.addCustomEvent)
		BX.addCustomEvent('onAjaxSuccess', KladrJsObj.setall);
});
<?
global $MESS, $DOCUMENT_ROOT;

CModule::AddAutoloadClasses(
    'av.mailfile',
    array(
        'C_AV_Mailfile' => 'classes/general/av_mailfile.php'
   )
);

if(!function_exists("custom_mail")) {
	function custom_mail($sTo, $sSubject, $sMessage, $sAdditionalHeaders, $sAdditionalParameters) {
		return C_AV_Mailfile::getInstance()->customMail($sTo, $sSubject, $sMessage, $sAdditionalHeaders, $sAdditionalParameters);
	}
	AddEventHandler('main', 'OnBeforeEventSend', array('C_AV_Mailfile', 'OnBeforeEventSendHandler'));
}

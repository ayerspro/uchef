<?
IncludeModuleLangFile(__FILE__);
Class av_mailfile extends CModule
{
	const MODULE_ID = 'av.mailfile';
	var $MODULE_ID = 'av.mailfile';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("AV_MAILFILE_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("AV_MAILFILE_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("AV_MAILFILE_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("AV_MAILFILE_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		return true;
	}

	function InstallModuleDependences()
	{
		RegisterModuleDependences('main', 'OnBeforeProlog', $this->MODULE_ID, '', '', 1);
		return true;
	}

	function UnInstallModuleDependences()
	{
		UnRegisterModuleDependences('main', 'OnBeforeProlog', $this->MODULE_ID, '', '');
		return true;
	}

	function InstallFiles($arParams = array())
	{
		return true;
	}

	function UnInstallFiles()
	{
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;

		if(function_exists('custom_mail')) {
			$this->ShowForm('ERROR', GetMessage('AV_MAILFILE_FUNC_EXISTS'));
		}

		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule(self::MODULE_ID);
		$this->InstallModuleDependences();
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
		$this->UnInstallModuleDependences();
	}

	private function ShowForm($type, $message)
	{
		IncludeModuleLangFile(__FILE__);

		$keys = array_keys($GLOBALS);
		for($i=0; $i<count($keys); $i++)
			if($keys[$i]!='i' && $keys[$i]!='GLOBALS' && $keys[$i]!='strTitle' && $keys[$i]!='filepath')
				global ${$keys[$i]};

		$APPLICATION->SetTitle(GetMessage('AV_MAILFILE_MODULE_NAME'));
		include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
		echo CAdminMessage::ShowMessage(array('MESSAGE' => $message, 'TYPE' => $type));
		?>
		<form action="<?=$APPLICATION->GetCurPage()?>" method="get">
		<p>
			<input type="hidden" name="lang" value="<?= LANG?>" />
			<input type="submit" value="<?=GetMessage('AV_MAILFILE_BACK')?>" />
		</p>
		</form>
		<?
		include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
		die();
	}

}
?>

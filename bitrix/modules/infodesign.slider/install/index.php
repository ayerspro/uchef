<?
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-18);
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));
IncludeModuleLangFile(__FILE__);

Class infodesign_slider extends CModule
{
	const MODULE_ID = 'infodesign.slider';
	var $MODULE_ID = "infodesign.slider";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;

	function infodesign_slider()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = GetMessage("NAME");
		$this->MODULE_DESCRIPTION = GetMessage("DESC");
		$this->PARTNER_NAME = "infodesign";
		$this->PARTNER_URI = "http://infodesign.ru/";
	}

	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/infodesign.slider/install/components",
			 $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFilesEx("/bitrix/components/infodesign");
		return true;
	}

	function DoInstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->InstallFiles();
		RegisterModule(self::MODULE_ID);
		$APPLICATION->IncludeAdminFile(GetMessage("INSTALL"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/infodesignmapsclaster/install/step.php");
	}

	function DoUninstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->UnInstallFiles();
		UnRegisterModule(self::MODULE_ID);
		$APPLICATION->IncludeAdminFile(GetMessage("UNINSTALL"), $DOCUMENT_ROOT."/bitrix/modules/infodesignmapsclaster/install/unstep.php");
	}
}
?>
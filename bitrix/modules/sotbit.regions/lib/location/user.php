<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 07-Feb-18
 * Time: 2:20 PM
 */
namespace Sotbit\Regions\Location;
use Sotbit\Regions\Config\Option;

class User
{
	protected $findUserMethod = 'ipgeobase';
	protected $ip;
	public function __construct()
	{
		if(Option::get('FIND_USER_METHOD',SITE_ID))
		{
			$this->findUserMethod = Option::get('FIND_USER_METHOD',SITE_ID);
		}
	}

	public function getUserCity()
	{
		if($this->findUserMethod == 'geoip')
		{
			$GeoIp = new User\GeoIp();
			return $GeoIp->getUserCity();
		}
		elseif($this->findUserMethod == 'statistic')
		{
			$Statistic = new User\Statistic();
			return $Statistic->getUserCity();
		}
		elseif($this->findUserMethod == 'ipgeobase')
		{
			$IpGeoBase = new User\IpGeoBase();
			return $IpGeoBase->getUserCity();
		}
	}
}
<?

namespace Sotbit\Regions\Location;

use Bitrix\Main\Application;
use Bitrix\Main\SystemException;
use Sotbit\Regions\Config\Option;
use Sotbit\Regions\Internals\FieldsTable;
use Sotbit\Regions\Internals\RegionsTable;

/**
 * Class Location
 * @package Sotbit\Regions
 * @author Sergey Danilkin <s.danilkin@sotbit.ru>
 */
class Domain
{
	/**
	 * @var null|string
	 */
	public static $props = null;

	/**
	 * @var Variables
	 */
	private $variables;

	/**
	 * Location constructor.
	 * @throws \Bitrix\Main\SystemException
	 */
	public function __construct()
	{
		if(!\SotbitRegions::isDemoEnd())
		{
			self::setLocation();
			$this->variables = Variables::getInstance();
		}
	}

	/**
	 * @return null|array
	 */
	public function getProps()
	{
		return self::$props;
	}

	public function getProp($code = '')
	{
		return self::$props[$code];
	}

	/**
	 * @throws SystemException
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 */
	private static function setLocation()
	{
		if($_SESSION['SOTBIT_REGIONS']['ID'] != $_COOKIE['sotbit_regions_id'])
		{;
			unset($_SESSION['SOTBIT_REGIONS']);
			self::$props = null;
		}
		if(isset($_SESSION['SOTBIT_REGIONS']))
		{
			self::$props = $_SESSION['SOTBIT_REGIONS'];
		}
		if(is_null(self::$props))
		{
			$context = Application::getInstance()->getContext();
			$request = $context->getRequest();
			if(!$request->isAdminSection())
			{
				$location = self::getCurrentLocation();

				if($location['ID'] > 0)
				{
					$rs = FieldsTable::getList(
						[
							'filter' => ['ID_REGION' => $location['ID']],
							'cache' => [
								'ttl' => 36000000,
							]
						]);
					while ($field = $rs->fetch())
					{
						$unserialized = unserialize($field['VALUE']);
						if(is_array($unserialized))
						{
							$field['VALUE'] = $unserialized;
						}
						$location[$field['CODE']] = $field['VALUE'];
					}
					$_SESSION['SOTBIT_REGIONS'] = self::$props = $location;
				}
				\Sotbit\Regions\Sale\Price::changeBasket();
			}
		}
	}

	/**
	 * @return array|false
	 * @throws SystemException
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getCurrentLocation()
	{
		$location = [];
		$singleDomain = Option::get('SINGLE_DOMAIN', SITE_ID);
		if($singleDomain == 'Y')
		{
			$idRegion = $_COOKIE['sotbit_regions_id'];
			if($idRegion > 0)
			{
				$location = RegionsTable::getList(
					[
						'filter' => ['ID' => $idRegion],
						'limit' => 1,
						'cache' => [
							'ttl' => 36000000,
						]
					]
				)->fetch();
			}
			else
			{
				$location = RegionsTable::getList(
					[
						'order' => ['SORT' => 'asc'],
						'limit' => 1,
						'cache' => [
							'ttl' => 36000000,
						]
					]
				)->fetch();
			}
		}
		else
		{
			$context = Application::getInstance()->getContext();
			$server = $context->getServer();
			$httpHost = $server->getHttpHost();
			$arHttpHost = explode(':',$httpHost);
			$currentDomain = $arHttpHost[0];
			$location = RegionsTable::getList(
				[
					'filter' => [
						'CODE' => [
							'http://' . $currentDomain,
							'https://' . $currentDomain
						]
					],
					'limit' => 1,
					'cache' => [
						'ttl' => 36000000,
					]
				]
			)->fetch();

			if(!$location['ID'])
			{
				$domains = [];
				$rs = RegionsTable::getList(
					[
						'order' => ['SORT' => 'asc'],
						'cache' => [
							'ttl' => 36000000,
						]
					]
				);
				while ($domain = $rs->fetch())
				{
					$domains[] = $domain;
					$url = str_replace([
						'http://',
						'https://'
					], '', $domain['CODE']);
					if(strpos($url, '.') === 0)
					{
						$location = $domain;
						break;
					}
				}
			}
			if(!$location['ID'])
			{
				if($domains[0])
				{
					$location = $domains[0];
				}
			}
		}

		return $location;
	}

	/**
	 * @return Variables
	 */
	public function getVariables()
	{
		return $this->variables;
	}
}

?>
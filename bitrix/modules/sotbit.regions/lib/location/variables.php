<?php
namespace Sotbit\Regions\Location;

use Sotbit\Regions\Config\Option;

class Variables
{
	/**
	 * @var null
	 */
	private static $instance = null;
	/**
	 * @var array
	 */
	private $codes = [];

	private function __clone() {}

	/**
	 * Variables constructor.
	 */
	private function __construct()
	{	}

	/**
	 * @return null|Variables
	 */
	public static function getInstance()
	{
		if (null === self::$instance)
		{
			self::$instance = new self();

			if($_SESSION['SOTBIT_REGIONS'] && is_array($_SESSION['SOTBIT_REGIONS']))
			{
				self::$instance->setCodes(array_keys($_SESSION['SOTBIT_REGIONS']));
			}
		}
		return self::$instance;
	}

	/**
	 * @param string $content
	 * @return string
	 */
	public function replaceContent($content = '')
	{
		$codes = $this->findVariables($content);
		$values = $this->getValues($codes);
		return str_replace(array_keys($values),array_values($values),$content);
	}

	/**
	 * @param array $codes
	 * @param bool|string $lid
	 * @return array
	 */
	public function getValues($codes = [],$lid = SITE_ID)
	{
		global $APPLICATION;
		$return = [];
		$delimiter = Option::get('MULTIPLE_DELIMITER', $lid);
		if($codes)
		{
			foreach ($codes as $code)
			{
				if(!isset($_SESSION['SOTBIT_REGIONS'][$code]))
				{
					continue;
				}
				$value = $_SESSION['SOTBIT_REGIONS'][$code];

				switch ($code)
				{
					case 'MAP_GOOGLE':
						$ll = explode(',', $value['VALUE']);
						ob_start();
						echo "<iframe src=\"https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d37625.66456378487!2d$ll[1]!3d$ll[0]!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sby!4v1532349415154\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>";
						$value = ob_get_contents();
						ob_end_clean();
						break;
					case 'MAP_YANDEX':
						$ll = explode(',', $value);
						$data = [
							'yandex_lat' => $ll[0],
							'yandex_lon' => $ll[1],
							'yandex_scale' => 10
						];
						ob_start();
						$APPLICATION->IncludeComponent(
							"bitrix:map.yandex.view",
							"",
							[
								"CONTROLS" => [
									"ZOOM",
									"MINIMAP",
									"TYPECONTROL",
									"SCALELINE"
								],
								"INIT_MAP_TYPE" => "MAP",
								"MAP_DATA" => serialize($data),
								"MAP_HEIGHT" => "500",
								"MAP_ID" => "2",
								"MAP_WIDTH" => "600",
								"OPTIONS" => [
									"ENABLE_SCROLL_ZOOM",
									"ENABLE_DBLCLICK_ZOOM",
									"ENABLE_DRAGGING"
								]
							]
						);
						$value = ob_get_contents();
						ob_end_clean();
						break;
					default:
						if(is_array($value))
						{
							$value = implode($value, $delimiter);
						}
						$return[\SotbitRegions::genCodeVariable($code)] = $value;
						break;
				}
			}
		}
		return $return;
	}

	/**
	 * @param string $text
	 * @return array
	 */
	private function findVariables($text = '')
	{
		$return = [];
		foreach ($this->getCodes() as $code)
		{
			if(strpos($text,\SotbitRegions::genCodeVariable($code)) !== false)
			{
				$return[] = $code;
			}
		}
		return $return;
	}

	/**
	 * @return array
	 */
	public function getCodes()
	{
		return $this->codes;
	}

	/**
	 * @param array $codes
	 */
	public function setCodes($codes)
	{
		$this->codes = $codes;
	}
}
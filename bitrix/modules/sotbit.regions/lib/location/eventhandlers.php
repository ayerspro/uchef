<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 24-Jan-18
 * Time: 4:28 PM
 */

namespace Sotbit\Regions\Location;

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Sotbit\Regions\Config\Option;

/**
 * Class EventHandlers
 * @package Sotbit\Regions\Location
 * @author Sergey Danilkin <s.danilkin@sotbit.ru>
 */
class EventHandlers
{
	/**
	 * @param $arUserResult
	 * @param $request
	 * @param $arParams
	 * @param $arResult
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function OnSaleComponentOrderPropertiesHandler(
		&$arUserResult,
		$request,
		&$arParams,
		&$arResult
	)
	{
		$context = Application::getInstance()->getContext();
		$request = $context->getRequest();
		if(!$request->isAdminSection())
		{
			if(!Loader::includeModule('sotbit.regions') || \SotbitRegions::isDemoEnd())
			{
				return true;
			}
			if(Option::get('INSERT_SALE_LOCATION',SITE_ID) != 'Y')
			{
				return true;
			}
			if(!Loader::includeModule('sale'))
			{
				return true;
			}
			$saleLocation = new Sale($arUserResult['PERSON_TYPE_ID']);
			$propertyId = $saleLocation->getPropertyId();
			if(!$propertyId)
			{
				return true;
			}
			$Id = $saleLocation->getId();
			if(!$Id)
			{
				return true;
			}

			$arUserResult['ORDER_PROP'][$propertyId] = $Id;
		}
	}

	/**
	 * @param $content
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function OnEndBufferContentHandler(&$content)
	{
		$context = Application::getInstance()->getContext();
		$request = $context->getRequest();
		if(!$request->isAdminSection() && !$request->isAjaxRequest() && Loader::includeModule('sotbit.regions') &&
			!\SotbitRegions::isDemoEnd())
		{
			$domain = new Domain();
			$content = $domain->getVariables()->replaceContent($content);
		}
	}

	/**
	 * @param $event
	 * @param $lid
	 * @param $arFields
	 * @param $message_id
	 * @throws \Bitrix\Main\SystemException
	 */
	public function OnBeforeEventAddHandler(&$event, &$lid, &$arFields, &$message_id)
	{
		if($event == 'STATISTIC_DAILY_REPORT')
		{
			return false;
		}
		$domain = new Domain();

		$codes = $domain->getVariables()->getCodes();
		unset($codes['MAP_YANDEX']);
		unset($codes['MAP_GOOGLE']);
		$variables = $domain->getVariables()->getValues($codes,$lid);

		if($variables)
		{
			foreach($variables as $key => $val)
			{
				$arFields[str_replace('#','',$key)] = $val;
			}
		}
	}
}
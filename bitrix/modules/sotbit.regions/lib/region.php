<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 20-Feb-18
 * Time: 11:48 AM
 */

namespace Sotbit\Regions;


class Region
{
	protected $id = 0;
	protected $domain = '';
	protected $name = '';
	protected $sort = 0;
	protected $priceCodes = array();
	protected $stores = array();
	protected $fields = array();
	public function __construct()
	{

	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getDomain()
	{
		return $this->domain;
	}

	/**
	 * @param string $domain
	 */
	public function setDomain(string $domain)
	{
		$this->domain = $domain;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getSort()
	{
		return $this->sort;
	}

	/**
	 * @param int $sort
	 */
	public function setSort(int $sort)
	{
		$this->sort = $sort;
	}

	/**
	 * @return array
	 */
	public function getPriceCodes()
	{
		return $this->priceCodes;
	}

	/**
	 * @param array $priceCodes
	 */
	public function setPriceCodes(array $priceCodes)
	{
		$this->priceCodes = $priceCodes;
	}

	/**
	 * @return array
	 */
	public function getStores()
	{
		return $this->stores;
	}

	/**
	 * @param array $stores
	 */
	public function setStores(array $stores)
	{
		$this->stores = $stores;
	}

	/**
	 * @return array
	 */
	public function getFields()
	{
		return $this->fields;
	}

	/**
	 * @param array $fields
	 */
	public function setFields(array $fields)
	{
		$this->fields = $fields;
	}
}
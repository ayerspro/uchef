<?php
namespace Sotbit\Regions\Sale;
/**
 * Class Price
 * @package Sotbit\Regions\Sale
 * @author Sergey Danilkin <s.danilkin@sotbit.ru>
 */
class Price
{
	/**
	 * get id prices for region
	 * @return array
	 */
	public function getPriceIds()
	{
		$return = [];
		if($_SESSION['SOTBIT_REGIONS']['PRICE_CODE'])
		{
			$rs = \CCatalogGroup::GetList(
				[],
				['NAME' => $_SESSION['SOTBIT_REGIONS']['PRICE_CODE']],
				false,
				false,
				['ID']
			);
			while ($priceType = $rs->Fetch())
			{
				$return[$priceType['ID']] = $priceType['ID'];
			}
		}

		return $return;
	}

	/**
	 * @param array $arResult
	 * @return array
	 */
	public function change($arResult = [])
	{
		if (isset( $arResult['ITEMS'] ))
		{
			foreach ( $arResult['ITEMS'] as $i => $Item )
			{
				if (isset( $Item['PRICES'] ) && sizeof( $Item['PRICES'] ) > 0 && isset( $Item['MIN_PRICE'] ))
				{
					$arResult['ITEMS'][$i]['MIN_PRICE'] = self::changeMin( $Item['MIN_PRICE']);

					foreach ( $Item['PRICES'] as $PriceCode => $Price )
					{
						if ($Price['PRICE_ID'] == $arResult['ITEMS'][$i]['MIN_PRICE']['PRICE_ID'])
						{
							$arResult['ITEMS'][$i]['PRICES'][$PriceCode]['VALUE'] = $arResult['ITEMS'][$i]['MIN_PRICE']['VALUE'];
							$arResult['ITEMS'][$i]['PRICES'][$PriceCode]['PRINT_VALUE'] = $arResult['ITEMS'][$i]['MIN_PRICE']['PRINT_VALUE'];
							$arResult['ITEMS'][$i]['PRICES'][$PriceCode]['DISCOUNT_VALUE'] = $arResult['ITEMS'][$i]['MIN_PRICE']['DISCOUNT_VALUE'];
							$arResult['ITEMS'][$i]['PRICES'][$PriceCode]['PRINT_DISCOUNT_VALUE'] = $arResult['ITEMS'][$i]['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
						}
						else
						{
							$arResult['ITEMS'][$i]['PRICES'][$PriceCode] = self::changeMin( $arResult['ITEMS'][$i]['PRICES'][$PriceCode] );
						}
					}
				}
				if (isset( $Item['OFFERS'] ))
				{
					foreach ( $Item['OFFERS'] as $k => $Offer )
					{
						if (isset( $Offer['PRICES'] ) && sizeof( $Offer['PRICES'] ) > 0 && isset( $Offer['MIN_PRICE'] ))
						{
							$arResult['ITEMS'][$i]['OFFERS'][$k]['MIN_PRICE'] = self::changeMin( $Offer['MIN_PRICE']);

							foreach ( $Offer['PRICES'] as $PriceCode => $Price )
							{
								if ($Price['PRICE_ID'] == $arResult['ITEMS'][$i]['OFFERS'][$k]['MIN_PRICE']['PRICE_ID'])
								{
									$arResult['ITEMS'][$i]['OFFERS'][$k]['PRICES'][$PriceCode]['VALUE'] = $arResult['ITEMS'][$i]['OFFERS'][$k]['MIN_PRICE']['VALUE'];
									$arResult['ITEMS'][$i]['OFFERS'][$k]['PRICES'][$PriceCode]['VALUE'] = $arResult['ITEMS'][$i]['OFFERS'][$k]['MIN_PRICE']['VALUE'];
									$arResult['ITEMS'][$i]['OFFERS'][$k]['PRICES'][$PriceCode]['PRINT_VALUE'] = $arResult['ITEMS'][$i]['OFFERS'][$k]['MIN_PRICE']['PRINT_VALUE'];
									$arResult['ITEMS'][$i]['OFFERS'][$k]['PRICES'][$PriceCode]['DISCOUNT_VALUE'] = $arResult['ITEMS'][$i]['OFFERS'][$k]['MIN_PRICE']['DISCOUNT_VALUE'];
									$arResult['ITEMS'][$i]['OFFERS'][$k]['PRICES'][$PriceCode]['PRINT_DISCOUNT_VALUE'] = $arResult['ITEMS'][$i]['OFFERS'][$k]['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
								}
								else
								{
									$arResult['ITEMS'][$i]['OFFERS'][$k]['PRICES'][$PriceCode] = self::changeMin(
										$arResult['ITEMS'][$i]['OFFERS'][$k]['PRICES'][$PriceCode]);
								}
							}
						}
					}
				}
			}
		}

		if (isset( $arResult['OFFERS'] ))
		{
			foreach ( $arResult['OFFERS'] as $k => $Offer )
			{
				if (isset( $Offer['PRICES'] ) && sizeof( $Offer['PRICES'] ) > 0 && isset( $Offer['MIN_PRICE'] ))
				{
					$arResult['OFFERS'][$k]['MIN_PRICE'] = self::changeMin( $Offer['MIN_PRICE'] );

					foreach ( $Offer['PRICES'] as $PriceCode => $Price )
					{
						if ($Price['PRICE_ID'] == $arResult['OFFERS'][$k]['MIN_PRICE']['PRICE_ID'])
						{
							$arResult['OFFERS'][$k]['PRICES'][$PriceCode]['VALUE'] = $arResult['OFFERS'][$k]['MIN_PRICE']['VALUE'];
							$arResult['OFFERS'][$k]['PRICES'][$PriceCode]['PRINT_VALUE'] = $arResult['OFFERS'][$k]['MIN_PRICE']['PRINT_VALUE'];
							$arResult['OFFERS'][$k]['PRICES'][$PriceCode]['DISCOUNT_VALUE'] = $arResult['OFFERS'][$k]['MIN_PRICE']['DISCOUNT_VALUE'];
							$arResult['OFFERS'][$k]['PRICES'][$PriceCode]['PRINT_DISCOUNT_VALUE'] = $arResult['OFFERS'][$k]['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
						}
						else
							$arResult['OFFERS'][$k]['PRICES'][$PriceCode] = self::changeMin( $arResult['OFFERS'][$k]['PRICES'][$PriceCode] );
					}
				}
			}
		}

		if (isset( $arResult['PRICES'] ) && sizeof( $arResult['PRICES'] ) > 0 && isset( $arResult['MIN_PRICE'] ))
		{
			$arResult['MIN_PRICE'] = self::changeMin( $arResult['MIN_PRICE'] );

			foreach ( $arResult['PRICES'] as $PriceCode => $Price )
			{
				if ($Price['PRICE_ID'] == $arResult['MIN_PRICE']['PRICE_ID'])
				{
					$arResult['PRICES'][$PriceCode]['VALUE'] = $arResult['MIN_PRICE']['VALUE'];
					$arResult['PRICES'][$PriceCode]['PRINT_VALUE'] = $arResult['MIN_PRICE']['PRINT_VALUE'];
					$arResult['PRICES'][$PriceCode]['DISCOUNT_VALUE'] = $arResult['MIN_PRICE']['DISCOUNT_VALUE'];
					$arResult['PRICES'][$PriceCode]['PRINT_DISCOUNT_VALUE'] = $arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
				}
				else
					$arResult['PRICES'][$PriceCode] = self::changeMin( $arResult['PRICES'][$PriceCode] );
			}
		}
		return $arResult;
	}

	/**
	 * @param array $price
	 * @return array
	 */
	private function changeMin($price = [])
	{
		if($_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'])
		{
			switch ($_SESSION['SOTBIT_REGIONS']['PRICE_VALUE_TYPE'])
			{
				case 'PROCENT_UP':
					$price['VALUE'] = $price['VALUE'] + ($price['VALUE'] / 100) * $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
					$price['PRINT_VALUE'] = \CurrencyFormat( $price['VALUE'], $price['CURRENCY'] );
					$price['DISCOUNT_VALUE'] = $price['DISCOUNT_VALUE'] + ($price['DISCOUNT_VALUE'] / 100) * $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
					$price['PRINT_DISCOUNT_VALUE'] = \CurrencyFormat( $price['DISCOUNT_VALUE'], $price['CURRENCY'] );
					break;
				case 'PROCENT_DOWN':
					$price['VALUE'] = $price['VALUE'] - ($price['VALUE'] / 100) * $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
					$price['PRINT_VALUE'] = \CurrencyFormat( $price['VALUE'], $price['CURRENCY'] );
					$price['DISCOUNT_VALUE'] = $price['DISCOUNT_VALUE'] - ($price['DISCOUNT_VALUE'] / 100) *
						$_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
					$price['PRINT_DISCOUNT_VALUE'] = \CurrencyFormat( $price['DISCOUNT_VALUE'], $price['CURRENCY'] );
					break;
				case 'FIX_UP':
					$price['VALUE'] = $price['VALUE'] + $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
					$price['PRINT_VALUE'] = \CurrencyFormat( $price['VALUE'], $price['CURRENCY'] );
					$price['DISCOUNT_VALUE'] = $price['DISCOUNT_VALUE'] + $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
					$price['PRINT_DISCOUNT_VALUE'] = \CurrencyFormat( $price['DISCOUNT_VALUE'], $price['CURRENCY'] );
					break;
				case 'FIX_DOWN':
					$price['VALUE'] = $price['VALUE'] - $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
					$price['PRINT_VALUE'] = \CurrencyFormat( $price['VALUE'], $price['CURRENCY'] );
					$price['DISCOUNT_VALUE'] = $price['DISCOUNT_VALUE'] - $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
					$price['PRINT_DISCOUNT_VALUE'] = \CurrencyFormat( $price['DISCOUNT_VALUE'], $price['CURRENCY'] );
					break;
			}
		}
		return $price;
	}

	/**
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function changeBasket()
	{
		if (\Bitrix\Main\Loader::includeModule( "sale" ) && \Bitrix\Main\Loader::includeModule( "catalog" ))
		{
			global $USER;

			$catalog_groups = array();
			$db_res = \CCatalogGroup::GetGroupsList(array("GROUP_ID"=>$USER->GetUserGroupArray(), "BUY"=>"Y"));
			while($arRes = $db_res->Fetch())
			{
				array_push($catalog_groups, $arRes['CATALOG_GROUP_ID']);
			}

			$result = \Bitrix\Sale\Internals\BasketTable::getList(
				array (
					'select' => array (
						'BASE_PRICE',
						'PRODUCT_ID',
						'LID',
						'ID'
					),
					'filter' => array (
						'LID' => SITE_ID,
						'FUSER_ID' => \CSaleBasket::GetBasketUserID(),
						"ORDER_ID" => "NULL"
					)
				) );

			while ( $BasketItem = $result->fetch() )
			{
				$Prices = array ();
				$dbPrice = \CPrice::GetList( array (
					"QUANTITY_FROM" => "ASC",
					"QUANTITY_TO" => "ASC",
					"SORT" => "ASC"
				), array (
					"PRODUCT_ID" => $BasketItem['PRODUCT_ID'],
					'CATALOG_GROUP_ID' => $catalog_groups
				), false, false, array (
					"ID",
					"CATALOG_GROUP_ID",
					"PRICE",
					"CURRENCY"
				) );
				while ( $arPrice = $dbPrice->Fetch() )
				{
					$arDiscounts = \CCatalogDiscount::GetDiscountByPrice( $arPrice["ID"],
						$USER->GetUserGroupArray(), "N", $BasketItem['LID'] );
					$discountPrice = \CCatalogProduct::CountPriceWithDiscount( $arPrice["PRICE"], $arPrice["CURRENCY"], $arDiscounts );
					$Prices[$arPrice['CATALOG_GROUP_ID']]["DISCOUNT_PRICE"] = $discountPrice;
					$Prices[$arPrice['CATALOG_GROUP_ID']]["PRICE"] = $arPrice;
					unset( $Prices[$arPrice['ID']]["PRICE"]['ID'] );
				}

				if($Prices)
				{
					foreach($Prices as $i => $price)
					{
						if($_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'])
						{
							switch ($_SESSION['SOTBIT_REGIONS']['PRICE_VALUE_TYPE'])
							{
								case 'PROCENT_UP':
									$price['DISCOUNT_PRICE'] = $price['DISCOUNT_PRICE'] + ($price['DISCOUNT_PRICE'] / 100) * $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
									$price['PRICE']['PRICE'] = $price['PRICE']['PRICE'] + ($price['PRICE']['PRICE'] / 100) * $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
									break;
								case 'PROCENT_DOWN':
									$price['DISCOUNT_PRICE'] = $price['DISCOUNT_PRICE'] - ($price['DISCOUNT_PRICE'] / 100) * $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
									$price['PRICE']['PRICE'] = $price['PRICE']['PRICE'] - ($price['PRICE']['PRICE'] / 100) * $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];									break;
								case 'FIX_UP':
									$price['DISCOUNT_PRICE'] = $price['DISCOUNT_PRICE'] + $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
									$price['PRICE']['PRICE'] = $price['PRICE']['PRICE'] + $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
									break;
								case 'FIX_DOWN':
									$price['DISCOUNT_PRICE'] = $price['DISCOUNT_PRICE'] - $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
									$price['PRICE']['PRICE'] = $price['PRICE']['PRICE'] - $_SESSION['SOTBIT_REGIONS']['PRICE_VALUE'];
									break;
							}
						}
						if($price['PRICE']['PRICE'] != $BasketItem['BASE_PRICE'])
						{
							\Bitrix\Sale\Internals\BasketTable::Update( $BasketItem['ID'], array (
								'PRICE' => $price['DISCOUNT_PRICE'],
								'CURRENCY' => $price['PRICE']['CURRENCY'],
								'CUSTOM_PRICE' => 'Y'
							) );
						}
					}
				}
			}
		}
	}
}
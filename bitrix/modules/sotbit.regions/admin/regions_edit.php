<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Sotbit\Regions\Internals\FieldsTable;
use Sotbit\Regions\Internals\RegionsTable;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

Loc::loadMessages(__FILE__);
?>

	<style>
		.adm-detail-content-cell-l img {
			display: none;
		}
	</style>

<?

if(!Loader::includeModule('sotbit.regions') || !Loader::includeModule('fileman'))
{
	return false;
}

if(\SotbitRegions::isDemoEnd())
{
	return false;
}

$action = isset($_REQUEST['action']) ? htmlspecialcharsbx($_REQUEST['action']) : 'add';

// get row
$row = null;
if(isset($_REQUEST['ID']) && $_REQUEST['ID'] > 0)
{
	$row = RegionsTable::getById($_REQUEST['ID'])->fetch();

	if($row['ID'] > 0)
	{
		$rs = FieldsTable::getList(['filter' => ['ID_REGION' => $row['ID']]]);
		while ($field = $rs->fetch())
		{
			$row[$field['CODE']] = $field['VALUE'];
		}
	}

	if(empty($row))
	{
		$row = null;
	}
}

if(isset($_REQUEST["CODE"]) && $_REQUEST["CODE"])
	$row["CODE"] = $_REQUEST["CODE"];
if(isset($_REQUEST["NAME"]) && $_REQUEST["NAME"])
	$row["NAME"] = $_REQUEST["NAME"];
if(isset($_REQUEST["SORT"]) && $_REQUEST["SORT"])
	$row["SORT"] = $_REQUEST["SORT"];
if(isset($_REQUEST["PRICE_CODE"]) && $_REQUEST["PRICE_CODE"])
	$row["PRICE_CODE"] = $_REQUEST["PRICE_CODE"];
if(isset($_REQUEST["STORE"]) && $_REQUEST["STORE"])
	$row["STORE"] = $_REQUEST["STORE"];
if(isset($_REQUEST["COUNTER"]) && $_REQUEST["COUNTER"])
	$row["COUNTER"] = $_REQUEST["COUNTER"];
if(isset($_REQUEST["MANAGER"]) && $_REQUEST["MANAGER"])
	$row["MANAGER"] = $_REQUEST["MANAGER"];
if(isset($_REQUEST["SITE_ID"]) && $_REQUEST["SITE_ID"])
	$row["SITE_ID"] = $_REQUEST["SITE_ID"];
if(isset($_REQUEST["PRICE_VALUE_TYPE"]) && $_REQUEST["PRICE_VALUE_TYPE"])
	$row["PRICE_VALUE_TYPE"] = $_REQUEST["PRICE_VALUE_TYPE"];
if(isset($_REQUEST["PRICE_VALUE"]) && $_REQUEST["PRICE_VALUE"])
	$row["PRICE_VALUE"] = $_REQUEST["PRICE_VALUE"];


if($_REQUEST['point_map_yandex_sotbit_regions_code_1__n0_lat'])
{
	$mapY = [];
	$val = '';
	foreach($_REQUEST as $k => $v)
	{
		if(strpos($k,'point_map_yandex_sotbit_regions') !== false && strpos($k,'lat') !== false && $v)
		{
			$val = $v.',';
		}
		if(strpos($k,'point_map_yandex_sotbit_regions') !== false && strpos($k,'lon') !== false && $v)
		{
			$val .= $v;
			$mapY[] = ['VALUE' => $val,'DESCRIPTION' => ''];
			$val = '';
		}
	}
	$row["MAP_YANDEX"] = $mapY;
}

if($_REQUEST['point_map_google_sotbit_regions_code2__n0_lat'])
{
	$mapG = [];
	$val = '';
	foreach($_REQUEST as $k => $v)
	{
		if(strpos($k,'point_map_google_sotbit_regions') !== false && strpos($k,'lat') !== false && $v)
		{
			$val = $v.',';
		}
		if(strpos($k,'point_map_google_sotbit_regions') !== false && strpos($k,'lon') !== false && $v)
		{
			$val .= $v;
			$mapG[] = ['VALUE' => $val,'DESCRIPTION' => ''];
			$val = '';
		}
	}
	$row["MAP_GOOGLE"] = ['VALUE' => $mapG, 'API_KEY' => $_REQUEST['API_KEY']];
}


if($_REQUEST['ID'] > 0)
{
	$APPLICATION->SetTitle(Loc::getMessage(\SotbitRegions::moduleId . '_EDIT',
		['#NAME#' => $row['NAME']])
	);

}
else
{
	$APPLICATION->SetTitle(Loc::getMessage(\SotbitRegions::moduleId . '_NEW'));
}

// form
$aTabs = [
	[
		'DIV' => 'edit1',
		'TAB' => Loc::getMessage(\SotbitRegions::moduleId . '_REGION'),
		'ICON' => 'ad_contract_edit',
		'TITLE' => htmlspecialcharsbx(Loc::getMessage(\SotbitRegions::moduleId . '_REGION'))
	],
	[
		'DIV' => 'edit2',
		'TAB' => Loc::getMessage(\SotbitRegions::moduleId . '_REGION_PRICE'),
		'ICON' => 'ad_contract_edit',
		'TITLE' => htmlspecialcharsbx(Loc::getMessage(\SotbitRegions::moduleId . '_REGION_PRICE'))
	],
];

$tabControl = new CAdminForm('sotbit_regions_edit', $aTabs);

// delete action
if($action == 'delete')
{
	$rs = FieldsTable::getList(['filter' => ['ID_REGION' => $ID]]);
	while ($field = $rs->fetch())
	{
		FieldsTable::delete($field['ID']);
	}
	RegionsTable::delete($ID);
	LocalRedirect('sotbit_regions.php?site=' . $site . '&lang=' . LANGUAGE_ID);
}

if($action == 'copy' && $_REQUEST['ID'] > 0)
{
	$row = RegionsTable::getById($_REQUEST['ID'])->fetch();
	unset($row['ID']);
	$result = RegionsTable::add($row);
	$id = $result->getId();
	if($id > 0)
	{
		$rs = FieldsTable::getList(['filter' => ['ID_REGION' => $ID]]);
		while ($field = $rs->fetch())
		{
			FieldsTable::add([
				'ID_REGION' => $id,
				'CODE' => $field['CODE'],
				'VALUE' => $field['VALUE']
			]);
		}
		LocalRedirect('sotbit_regions_edit.php?ID=' . $id . '&lang=' . LANGUAGE_ID);
	}
}

// save action
if((strlen($save) > 0 || strlen($apply) > 0) && $REQUEST_METHOD == 'POST' && check_bitrix_sessid())
{
	$data = [];

	$USER_FIELD_MANAGER->EditFormAddFields('SOTBIT_REGIONS', $data);

	if(!is_array($PRICE_CODE))
	{
		$PRICE_CODE = [];
	}
	if(!is_array($STORE))
	{
		$STORE = [];
	}
	/*
		$MAP_YANDEX = $_REQUEST['point_map_yandex_sotbit_regions_code_1__n0_lat'] . ',' . $_REQUEST['point_map_yandex_sotbit_regions_code_1__n0_lon'];
		$MAP_GOOGLE = [
			'VALUE' => ($_REQUEST['point_map_google_sotbit_regions_code2__n0_lat']) ? $_REQUEST['point_map_google_sotbit_regions_code2__n0_lat'] . ',' . $_REQUEST['point_map_google_sotbit_regions_code2__n0_lon'] : '',
			'API_KEY' => $_REQUEST['API_KEY']
		];
	*/
	$arFields = [
		'CODE' => $CODE,
		'NAME' => $NAME,
		'SORT' => $SORT,
		'SITE_ID' => $SITE_ID,
		'PRICE_CODE' => $PRICE_CODE,
		'STORE' => $STORE,
		'MAP_YANDEX' => $row["MAP_YANDEX"],
		'MAP_GOOGLE' => $row["MAP_GOOGLE"],
		'COUNTER' => $COUNTER,
		'MANAGER' => $MANAGER,
		'PRICE_VALUE_TYPE' => $PRICE_VALUE_TYPE,
		'PRICE_VALUE' => $PRICE_VALUE,
	];

	/** @param Bitrix\Main\Entity\AddResult $result */
	if($ID > 0)
	{
		$result = RegionsTable::update($ID, $arFields);
	}
	else
	{
		$result = RegionsTable::add($arFields);
		$ID = $result->getId();
	}

	if($ID > 0 && $data)
	{
		foreach (array_keys($data) as $code)
		{
			if(is_array($data[$code]))
			{
				$data[$code] = serialize($data[$code]);
			}
			$rs = FieldsTable::getList([
				'filter' => [
					'CODE' => $code,
					'ID_REGION' => $ID
				]
			]);
			while ($field = $rs->fetch())
			{
				FieldsTable::update($field['ID'], ['VALUE' => $data[$code]]);
				unset($data[$code]);
			}
		}
		if($data)
		{
			foreach (array_keys($data) as $code)
			{
				FieldsTable::add([
					'ID_REGION' => $ID,
					'CODE' => $code,
					'VALUE' => $data[$code]
				]);
			}
		}
	}

	if($result->isSuccess())
	{
		if(strlen($save) > 0)
		{
			LocalRedirect('sotbit_regions.php?lang=' . LANGUAGE_ID);
		}
		else
		{
			LocalRedirect('sotbit_regions_edit.php?ID=' . intval($ID) . '&lang=' . LANGUAGE_ID
				. '&' . $tabControl->ActiveTabParam());
		}
	}
	else
	{
		$errors = $result->getErrorMessages();

		// rewrite values
		foreach ($data as $k => $v)
		{
			if(isset($row[$k]))
			{
				$row[$k] = $v;
			}
		}
	}
}

// menu
$aMenu = [
	[
		'TEXT' => Loc::getMessage(\SotbitRegions::moduleId . '_RETURN'),
		'TITLE' => Loc::getMessage(\SotbitRegions::moduleId . '_RETURN'),
		'LINK' => 'sotbit_regions.php?lang=' . LANGUAGE_ID,
		'ICON' => 'btn_list',
	]
];

$aMenu[] = [
	'TEXT' => Loc::getMessage(\SotbitRegions::moduleId . '_ADD'),
	'TITLE' => Loc::getMessage(\SotbitRegions::moduleId . '_ADD'),
	'LINK' => $APPLICATION->getCurPageParam('ID=0', [
		'action',
		'ID'
	]),
	'ICON' => 'btn_new',
];

$aMenu[] = [
	'TEXT' => Loc::getMessage(\SotbitRegions::moduleId . '_COPY'),
	'TITLE' => Loc::getMessage(\SotbitRegions::moduleId . '_COPY'),
	'LINK' => $APPLICATION->getCurPageParam('action=copy', ['action']),
	'ICON' => 'btn_copy',
];

$aMenu[] = [
	'TEXT' => Loc::getMessage(\SotbitRegions::moduleId . '_DELETE'),
	'TITLE' => Loc::getMessage(\SotbitRegions::moduleId . '_DELETE'),
	'LINK' => $APPLICATION->getCurPageParam('action=delete', ['action']),
	'ICON' => 'delete',
];


$context = new CAdminContextMenu($aMenu);


require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');


if(\SotbitRegions::getDemo() == 2)
{
	CAdminMessage::ShowMessage([
		"MESSAGE" => Loc::getMessage(\SotbitRegions::moduleId . "_DEMO"),
		'HTML' => true
	]);
}
if(\SotbitRegions::getDemo() == 3)
{
	CAdminMessage::ShowMessage([
		"MESSAGE" => Loc::getMessage(\SotbitRegions::moduleId . "_DEMO_END"),
		'HTML' => true
	]);
}


$context->Show();


if(!empty($errors))
{
	$bVarsFromForm = true;
	CAdminMessage::ShowMessage(join("\n", $errors));
}
else
{
	$bVarsFromForm = false;
}

$tabControl->BeginPrologContent();

echo $USER_FIELD_MANAGER->ShowScript();

echo CAdminCalendar::ShowScript();

$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();
?>

<?= bitrix_sessid_post() ?>
	<input type="hidden" name="ID" value="<?= htmlspecialcharsbx(!empty($row) ? $row['ID'] : '') ?>">
	<input type="hidden" name="site" value="<?= strtoupper($site) ?>">
	<input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
	<input type="hidden" name="action" value="<?= $action ?>">

<? $tabControl->EndEpilogContent();

$tabControl->Begin([
	'FORM_ACTION' => $APPLICATION->GetCurPage() . '?ID=' . IntVal($ID) . '&lang=' . LANG
]);

$tabControl->BeginNextFormTab();




$tabControl->BeginCustomField("SITE_ID", Loc::getMessage(\SotbitRegions::moduleId . '_SITE_ID'), false);

$sites = [];
$rsSites = \Bitrix\Main\SiteTable::getList([
	'select' => [
		'*'
	],
	'filter' => ['ACTIVE' => 'Y'],
]);
while ($site = $rsSites->fetch())
{
	$sites[$site['LID']] = '[' . $site['LID'] . '] ' . $site['SITE_NAME'];
	if(!$row['SITE_ID'] && $site['DEF'] == 'Y')
	{
		$row['SITE_ID'] = [$site['LID']];
	}
}
?>
	<tr id="SITE_ID">
		<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
		<td width="60%">
			<?
			foreach ($sites as $lid => $site)
			{
				?>
				<input
				type="checkbox" <?= ($row['SITE_ID'] && is_array($row['SITE_ID']) && in_array($lid, $row['SITE_ID']))
				? 'checked' : '' ?> name="SITE_ID[]"
				value="<?= $lid ?>"><?= $site ?><br>
				<?
			}
			?>
		</td>
	</tr>
<? $tabControl->EndCustomField("SITE_ID");

$tabControl->AddEditField('CODE', Loc::getMessage(\SotbitRegions::moduleId . '_CODE'), false, ['size' => 50], $row['CODE']);
$tabControl->BeginCustomField('CODE_NOTE', '');
?>
	<tr class="tabcontent">
		<td width="40%"></td>
		<td width="60%">
			<div class="adm-info-message-wrap">
				<div class="adm-info-message">
					<?= Loc::getMessage(\SotbitRegions::moduleId . '_CODE_NOTE') ?>
				</div>
			</div>
		</td>
	</tr>
<?
$tabControl->EndCustomField("CODE_NOTE");
$tabControl->AddEditField('NAME', Loc::getMessage(\SotbitRegions::moduleId . '_NAME'), false, ['size' => 50], $row['NAME']);
$tabControl->BeginCustomField('NAME_NOTE', '');
?>
	<tr class="tabcontent">
		<td width="40%"></td>
		<td width="60%">
			<div class="adm-info-message-wrap">
				<div class="adm-info-message">
					<?= Loc::getMessage(\SotbitRegions::moduleId . '_NAME_NOTE') ?>
				</div>
			</div>
		</td>
	</tr>
<?


$tabControl->EndCustomField("NAME_NOTE");
$tabControl->AddEditField('SORT', Loc::getMessage(\SotbitRegions::moduleId . '_SORT'), false, ['size' => 5], ($row['SORT'] >
	0) ? $row['SORT'] : 100);
if(Loader::includeModule('catalog'))
{
	$tabControl->BeginCustomField("PRICE_CODE", Loc::getMessage(\SotbitRegions::moduleId . '_PRICE_CODE'), false);
	$priceCodes = [];
	$rs = CCatalogGroup::GetList([], ['ACTIVE' => 'Y']);
	while ($priceCode = $rs->fetch())
	{
		$priceCodes['REFERENCE_ID'][$priceCode['ID']] = $priceCode['NAME'];
		$priceCodes['REFERENCE'][$priceCode['ID']] = '[' . $priceCode['NAME'] . '] ' . $priceCode['NAME_LANG'];
	}
	?>
	<tr id="PRICE_CODE">
	<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
	<td width="60%">
		<?
		echo SelectBoxMFromArray("PRICE_CODE[]", $priceCodes, $row['PRICE_CODE'], "", false, 4,
			'style="min-width:200px"');
		?>
	</td>
	</tr><?
	$tabControl->EndCustomField("PRICE_CODE");
	$tabControl->BeginCustomField('PRICE_CODE_NOTE', '');
	?>
	<tr class="tabcontent">
		<td width="40%"></td>
		<td width="60%">
			<div class="adm-info-message-wrap">
				<div class="adm-info-message">
					<?= Loc::getMessage(\SotbitRegions::moduleId . '_PRICE_CODE_NOTE') ?>
				</div>
			</div>
		</td>
	</tr>
	<?
	$tabControl->EndCustomField("PRICE_CODE_NOTE");

	$tabControl->BeginCustomField("STORE", Loc::getMessage(\SotbitRegions::moduleId . '_STORE'), false);
	$stores = [];
	$rs = \CCatalogStore::GetList(
		[],
		[
			'ISSUING_CENTER' => 'Y',
			'ACTIVE' => 'Y'
		],
		false,
		false,
		[
			'ID',
			'TITLE'
		]
	);
	while ($store = $rs->fetch())
	{
		$stores['REFERENCE_ID'][$store['ID']] = $store['ID'];
		$stores['REFERENCE'][$store['ID']] = $store['TITLE'];
	}
	?>
	<tr id="STORE">
	<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
	<td width="60%">
		<?
		echo SelectBoxMFromArray("STORE[]", $stores, $row['STORE'], "", false, 4,
			'style="min-width:200px"');
		?>
	</td>
	</tr><?
	$tabControl->EndCustomField("STORE");
	$tabControl->BeginCustomField('STORE_NOTE', '');
	?>
	<tr class="tabcontent">
		<td width="40%"></td>
		<td width="60%">
			<div class="adm-info-message-wrap">
				<div class="adm-info-message">
					<?= Loc::getMessage(\SotbitRegions::moduleId . '_STORE_NOTE') ?>
				</div>
			</div>
		</td>
	</tr>
	<?
	$tabControl->EndCustomField("STORE_NOTE");
}
$tabControl->BeginCustomField("COUNTER", Loc::getMessage(\SotbitRegions::moduleId . '_COUNTER'), false);
?>
	<tr id="COUNTER">
		<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
		<td width="60%">
			<?
			\CFileMan::AddHTMLEditorFrame(
				"COUNTER",
				$row["COUNTER"],
				"COUNTER",
				"html",
				[
					'height' => 150,
					'width' => '100%'
				],
				"N",
				0,
				"",
				"",
				"ru"
			);
			?>
		</td>
	</tr>
<? $tabControl->EndCustomField("COUNTER");
$tabControl->BeginCustomField("MAP_YANDEX", Loc::getMessage(\SotbitRegions::moduleId . '_MAP_YANDEX'), false);
?>
	<tr id="MAP_YANDEX">
		<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
		<td width="60%">
			<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="MAP_YANDEX1"><tbody>
				<?
				for($i=0;$i<1;++$i)
				{
					?>

					<tr><td>
							<?
							echo call_user_func_array([
								'CIBlockPropertyMapYandex',
								'GetPropertyFieldHtml'
							],
								[
									[
										'MULTIPLE' => 'N',
										'ID' => 1,
										'CODE' => 'sotbit_regions_code'
									],
									['VALUE' => $row["MAP_YANDEX"][$i]['VALUE']],
									[
										"VALUE" => $row["MAP_YANDEX"][$i]['VALUE'],
										"FORM_NAME" => 'sotbit_regions_edit_form',
										"MODE" => "FORM_FILL",
										"COPY" => false,
									],
								]);
							?>
						</td></tr>
					<?
				}
				?>
				</tbody></table>
		</td>
	</tr>
<? $tabControl->EndCustomField("MAP_YANDEX");
$tabControl->BeginCustomField("MAP_GOOGLE", Loc::getMessage(\SotbitRegions::moduleId . '_MAP_GOOGLE'), false);
?>
	<tr id="MAP_GOOGLE">
		<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
		<td width="60%">
			<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="MAP_GOOGLE1"><tbody>
				<?
				for($i=0;$i<1;++$i)
				{
					?>
					<tr><td>
							<?
							echo call_user_func_array([
								'CIBlockPropertyMapGoogle',
								'GetPropertyFieldHtml'
							],
								[
									[
										'MULTIPLE' => 'N',
										'ID' => 2,
										'CODE' => 'sotbit_regions_code',
										'USER_TYPE_SETTINGS' => ['API_KEY' => $row["MAP_GOOGLE"]['API_KEY']]
									],
									['VALUE' => $row["MAP_GOOGLE"]['VALUE'][$i]['VALUE']],
									[
										"VALUE" => $row["MAP_GOOGLE"]['VALUE'][$i]['VALUE'],
										"FORM_NAME" => 'sotbit_regions_edit_form',
										"MODE" => "FORM_FILL",
										"COPY" => false,
									],
								]);
							?>
						</td></tr>
					<?
				}
				?>
				</tbody></table>
			<?= Loc::getMessage(\SotbitRegions::moduleId . '_GOOGLE_API_KEY') ?>
			<input type="text" name="API_KEY" value="<?= $row["MAP_GOOGLE"]['API_KEY'] ?>">
		</td>
	</tr>
<? $tabControl->EndCustomField("MAP_GOOGLE");
$tabControl->BeginCustomField("MANAGER", Loc::getMessage(\SotbitRegions::moduleId . '_MANAGER'), false);
?>
	<tr id="MANAGER">
		<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
		<td width="60%">
			<?
			echo FindUserID("MANAGER", $row["MANAGER"], "", "sotbit_regions_edit_form", 4);
			?>
		</td>
	</tr>
<? $tabControl->EndCustomField("MANAGER");


$ufields = $USER_FIELD_MANAGER->GetUserFields('SOTBIT_REGIONS');
$hasSomeFields = !empty($ufields);

//remove files for copy action
if($hasSomeFields && !empty($row))
{
	foreach ($ufields as $ufCode => $ufField)
	{
		if(
			isset($ufField['USER_TYPE_ID']) && $ufField['USER_TYPE_ID'] == 'file' ||
			(
				isset($ufField['USER_TYPE']) && is_array($ufField['USER_TYPE']) &&
				isset($ufField['USER_TYPE']['BASE_TYPE']) && $ufField['USER_TYPE']['BASE_TYPE'] == 'file'
			)
		)
		{
			$row[$ufCode] = null;
		}
	}
}

echo $tabControl->ShowUserFieldsWithReadyData('SOTBIT_REGIONS', $row, $bVarsFromForm, 'ID');
$tabControl->BeginNextFormTab();

$tabControl->BeginCustomField("REGION_PRICE", Loc::getMessage(\SotbitRegions::moduleId . '_REGION_PRICE'), false);

$arValueTypeSel["REFERENCE_ID"] = array (
	'0',
	'PROCENT_UP',
	'PROCENT_DOWN',
	'FIX_UP',
	'FIX_DOWN',
);
$arValueTypeSel["REFERENCE"] = array (
	'-',
	Loc::getMessage( \SotbitRegions::moduleId . "_PROCENT_UP" ),
	Loc::getMessage( \SotbitRegions::moduleId . "_PROCENT_DOWN" ),
	Loc::getMessage( \SotbitRegions::moduleId . "_FIX_UP" ),
	Loc::getMessage( \SotbitRegions::moduleId . "_FIX_DOWN" ),
);
?>
	<tr class="heading">
		<td colspan="2"><?=Loc::getMessage(\SotbitRegions::moduleId . '_REGION_PRICE_TITLE') ?></td>
	</tr>
	<tr id="tr_VALUE_TYPE" class="tabcontent ChangeValue">
		<td width="40%" class="left-side-wrapper top-left-side-wrapper"><div
					class="left-side top-left-side"><? echo $tabControl->GetCustomLabelHTML(); ?></div></td>
		<td width="60%" class="right-side-wrapper top-right-side-wrapper"><div
					class="right-side top-right-side">
				<?
				echo SelectBoxFromArray( 'PRICE_VALUE_TYPE', $arValueTypeSel, $row['PRICE_VALUE_TYPE'], '', 'style="min-width:320px;" onchange="ChangedValueType(this.value)"', false, '' );
				?>
			</div>
		</td>
	</tr>
	<tr id="tr_VALUE" class="tabcontent ChangeValue">
		<td width="40%" class="left-side-wrapper bottom-left-side-wrapper">
			<div class="left-side bottom-left-side">
				<?=Loc::getMessage(\SotbitRegions::moduleId . '_PRICE_VALUE') ?>
			</div>
		</td>
		<td width="60%" class="right-side-wrapper bottom-right-side-wrapper">
			<div class="right-side bottom-right-side">
				<input type="text" name="PRICE_VALUE"
				       value="<?=$row['PRICE_VALUE']?>"
				       size="25" maxlength="25" style="width: 310px;">
			</div>
		</td>
	</tr>
<? $tabControl->EndCustomField("REGION_PRICE");



$tabControl->Buttons([
	'disabled' => false,
	'back_url' => 'sotbit_regions.php?lang=' . LANGUAGE_ID
]);

$tabControl->Show();
?>
	</form>
<?

if($_REQUEST['mode'] == 'list')
	require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin_js.php');
else
	require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
?>
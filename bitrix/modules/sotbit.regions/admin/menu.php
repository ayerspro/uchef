<?
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if(!Loader::includeModule('sotbit.regions'))
{
	return false;
}

if($APPLICATION->GetGroupRight(SotbitRegions::moduleId) == "D")
{
	return false;
}

$sites = SotbitRegions::getSites();
$items = array();
$items[] = array(
	"text" => Loc::getMessage(SotbitRegions::moduleId . '_seofiles'),
	"url" => SotbitRegions::sitemapPath . '?lang=' . LANGUAGE_ID,
	"title" => Loc::getMessage(SotbitRegions::moduleId . '_seofiles'),
	"more_url" => Array(SotbitRegions::sitemapPath),
);
$items[] = array(
	"text" => Loc::getMessage(SotbitRegions::moduleId . '_regions'),
	"url" => SotbitRegions::regionsPath . '?lang=' . LANGUAGE_ID,
	"title" => Loc::getMessage(SotbitRegions::moduleId . '_regions'),
	"more_url" => Array(SotbitRegions::regionsPath),
);

$regions = array();
$settings = array();

foreach ($sites as $lid => $name)
{
	/*$regions[$lid] = array(
		"text" => Loc::getMessage(SotbitRegions::moduleId . '_regions'). ' [' . $lid . '] ' . $name,
		"url" => SotbitRegions::regionsPath . '?lang=' . LANGUAGE_ID . '&site=' . $lid,
		"title" => Loc::getMessage(SotbitRegions::moduleId . '_regions') .' [' . $lid . '] ' . $name,
		"more_url" => array(
			SotbitRegions::regionPath.'?lang=' . LANGUAGE_ID . '&site=' . $lid,
		),
	);*/
	$settings[$lid] = array(
		"text" => Loc::getMessage(SotbitRegions::moduleId . '_settings'). ' [' . $lid . '] ' . $name,
		"url" => SotbitRegions::settingsPath . '?lang=' . LANGUAGE_ID . '&site=' . $lid,
		"title" => Loc::getMessage(SotbitRegions::moduleId . '_settings') .' [' . $lid . '] ' . $name,
		"more_url" => Array(SotbitRegions::settingsPath),
	);
}
if(count($settings) > 1)
{
	/*$items[] = array(
		"text" => Loc::getMessage(SotbitRegions::moduleId . '_regions'),
		"title" => Loc::getMessage(SotbitRegions::moduleId . '_regions'),
		'items' => $regions
	);*/
	$items[] = array(
		"text" => Loc::getMessage(SotbitRegions::moduleId . '_settings'),
		"title" => Loc::getMessage(SotbitRegions::moduleId . '_settings'),
		'items' => $settings
	);
}
else
{
	//$items[] = reset($regions);
	$items[] = reset($settings);
}

$aMenu = array(
	"parent_menu" => SotbitRegions::getMenuParent('global_menu_store'),
	"section" => SotbitRegions::moduleId,
	"sort" => 300,
	"text" => Loc::getMessage(SotbitRegions::moduleId . "_menu_text"),
	"title" => Loc::getMessage(SotbitRegions::moduleId . "_menu_text"),
	"url" => SotbitRegions::sitemapPath . '?lang=' . LANGUAGE_ID . '&site=' . key($sites),
	"icon" => "sotbit_regions_menu_icon",
	"page_icon" => SotbitRegions::moduleId . "_page_icon",
	"items_id" => SotbitRegions::moduleId . "_menu",
	"items" => $items
);

return $aMenu;
?>
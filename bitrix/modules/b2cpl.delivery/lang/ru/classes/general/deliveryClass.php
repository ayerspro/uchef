<?
$MESS['B2CPL_DELIV_MSK'] = 'Москва';
$MESS['B2CPL_DELIV_SPB'] = 'Санкт-Петербург';

$MESS['B2CPL_WIDG_HEADER'] = 'Выберите желаемое время доставки';
$MESS['B2CPL_WIDG_BUTTON'] = 'Выбрать дату и время доставки';
$MESS['B2CPL_WIDG_CHOOSETIME'] = 'Интервал доставки:';
$MESS['B2CPL_WIDG_full'] = 'весь день';
$MESS['B2CPL_WIDG_first'] = '1 половина';
$MESS['B2CPL_WIDG_sec'] = '2 половина';
$MESS['B2CPL_WIDG_choose'] = 'Выбор';
$MESS['B2CPL_WIDG_dateDeliv'] = 'Желаемая дата доставки: ';
$MESS['B2CPL_WIDG_availible'] = 'Выбор даты';
$MESS['B2CPL_WIDG_yes'] = 'Да';
$MESS['B2CPL_WIDG_first_date'] = 'Первая дата';

$MESS['B2CPL_pickup_descr'] = 'Доставка заказа до пункта самовывоза B2CPL.';
$MESS['B2CPL_pickup'] = 'пвз';
$MESS['B2CPL_pickup_basic_pvz'] = "Базовый ПВЗ";

$MESS['B2CPL_WIDG_close'] = 'Закрыть';
$MESS['B2CPL_WIDG_prev'] = 'Прошлый';
$MESS['B2CPL_WIDG_next'] = 'Следующий';
$MESS['B2CPL_WIDG_today'] = 'Сегодня';
$MESS['B2CPL_WIDG_monthes'] = "'Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'";
$MESS['B2CPL_WIDG_montheShort'] = "'Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'";
$MESS['B2CPL_WIDG_dayNames'] = "'воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'";
$MESS['B2CPL_WIDG_dayNamesShort'] = "'вск','пнд','втр','срд','чтв','птн','сбт'";
$MESS['B2CPL_WIDG_dayNamesMin'] = "'Вс','Пн','Вт','Ср','Чт','Пт','Сб'";
$MESS['B2CPL_WIDG_weekHeader'] = "Нед";

$MESS["B2CPL_pickup_show_all"] = "Показать все ПВЗ";
$MESS['B2CPL_address'] = "Адрес";
$MESS['B2CPL_workTime'] = "Режим работы";

$MESS['B2CPL_pereofr'] = 'Заказ';
$MESS['B2CPL_cancel'] = 'Отказ';

$MESS['B2CPL_product_artnumber'] = 'Артикул';
$MESS['B2CPL_product_weight'] = 'Вес';

$MESS['B2CPL_cancel_order'] = "Отмена заказа агентом модуля. Причина: ";
$MESS['B2CPL_status_prop_not_found'] = "Свойство для записи статуса заказа для данного типа плательщика не найдено.";
?>

<?
$MESS['B2CPL_LBL_noPOST'] = 'Без Почты России';
$MESS['B2CPL_LBL_noCOURIER'] = 'Без курьера';
$MESS['B2CPL_LBL_noSHOW'] = 'Не отображать';
$MESS['B2CPL_LBL_onPVZ'] = 'Только ПВЗ';
$MESS['B2CPL_LBL_cacheCleared'] = 'Кэш модуля очищен';

$MESS['B2CPL_TABLE_SHOW'] = 'Показать';
$MESS['B2CPL_TABLE_index'] = 'Индекс';
$MESS['B2CPL_TABLE_city'] = 'Город';
$MESS['B2CPL_TABLE_address'] = 'Адрес';
$MESS['B2CPL_TABLE_fio'] = 'Контактное лицо';
$MESS['B2CPL_TABLE_mphone'] = 'Мобильный телефон';
$MESS['B2CPL_TABLE_uphone'] = 'Дополнительный телефон';
$MESS['B2CPL_TABLE_email'] = 'E-mail';
$MESS['B2CPL_TABLE_pack'] = 'Упаковка';
$MESS['B2CPL_TABLE_issue'] = 'Дата доставки';
$MESS['B2CPL_TABLE_term'] = 'Интервал';

$MESS['B2CPL_TABLE_TOORDR'] = 'К заказу';
$MESS['B2CPL_TABLE_DELETE'] = 'Удалить';
$MESS['B2CPL_TABLE_STICKER'] = 'Стикеры';

$MESS['B2CPL_TABLE_DELETED'] = 'Заявка удалена';

$MESS['B2CPL_FORM_SHOW'] = 'Показать';
$MESS['B2CPL_FORM_pack_omni'] = 'омни';
$MESS['B2CPL_FORM_pack_callomni'] = 'call омни';
$MESS['B2CPL_FORM_pack_call'] = 'call';
$MESS['B2CPL_FORM_pack_callIM'] = 'call ИМ';
$MESS['B2CPL_FORM_term_first'] = '1-я половина';
$MESS['B2CPL_FORM_term_sec']   = '2-я половина';
$MESS['B2CPL_FORM_term_full']  = 'весь день';
?>
<?
$MESS['B2CPL_FORM_BTNAME'] = 'B2CPL';
$MESS['B2CPL_FORM_STATUS'] = 'Статус';
$MESS['B2CPL_FORM_B2CCODE'] = 'Код B2CPL';
$MESS['B2CPL_FORM_NUMBER'] = 'Номер заказа';
$MESS['B2CPL_FORM_DATE'] = 'Дата заказа';
$MESS['B2CPL_FORM_ISPAYED'] = 'Заказ оплачен';
$MESS['B2CPL_FORM_index'] = 'Индекс';
$MESS['B2CPL_FORM_city'] = 'Город';
$MESS['B2CPL_FORM_address'] = 'Адрес';
$MESS['B2CPL_FORM_fio'] = 'Контактное лицо';
$MESS['B2CPL_FORM_mphone'] = 'Мобильный телефон';
$MESS['B2CPL_FORM_uphone'] = 'Дополнительный телефон';
$MESS['B2CPL_FORM_email'] = 'E-mail';
$MESS['B2CPL_FORM_pack'] = 'Упаковка';
$MESS['B2CPL_FORM_issue'] = 'Дата доставки';
$MESS['B2CPL_FORM_term'] = 'Интервал доставки';
$MESS['B2CPL_FORM_PVZ'] = "ПВЗ";
$MESS['B2CPL_FORM_PVZ_selected'] = "Выбранное ПВЗ";
$MESS['B2CPL_FORM_PVZ_availible'] = "Доступные ПВЗ по указанному индексу";
$MESS['B2CPL_FORM_PVZ_price'] = "стоимость доставки";

$MESS['B2CPL_FORM_term_first'] = '1-я половина';
$MESS['B2CPL_FORM_term_sec']   = '2-я половина';
$MESS['B2CPL_FORM_term_full']  = 'весь день';

$MESS['B2CPL_FORM_pack_omni'] = 'омни';
$MESS['B2CPL_FORM_pack_callomni'] = 'call омни';
$MESS['B2CPL_FORM_pack_call'] = 'call';
$MESS['B2CPL_FORM_pack_callIM'] = 'call ИМ';
$MESS['B2CPL_FORM_pack_reg'] = 'Поле используются, если подписаны договоры на комплектацию.';
$MESS['B2CPL_FORM_pack_noArt'] = 'Обязательное поле "Артикул" не заполнено у следующих товаров:<br>';

$MESS['B2CPL_FORM_errors'] = 'Ошибки';
$MESS['B2CPL_FORM_errorNOPVZ'] = 'Невозможно определить указанный ПВЗ: выбран первый из списка.';

$MESS['B2CPL_FORM_HINT_pack'] = '<strong>омни</strong><br>Заявка на Комплектацию если заключен договор на хранение и упаковку и данный заказ должен быть упакован Исполнителем.<br><br>
<strong>call омни</strong><br>Заявка на Актуализацию с автоматической передачей подтвержденных заказов на комплектацию в Омни.<br><br>
<strong>call</strong><br>Заявка на Актуализацию с автоматической передачей подтвержденных заказов на отправку в Директ сервис, но без автоматической передачи подтвержденных заказов на комплектацию в Омни.<br><br>
<strong>call ИМ</strong><br>Заявка на Актуализацию заказов без предачи на доставку в Директ Сервис. В таких заказах проверяется только корректность номера телефона, остальные проверки не осуществляются.';

$MESS['B2CPL_FORM_MESS_FILL'] = 'Заполните';

$MESS['B2CPL_FORM_badDate'] = 'Некорректная дата';

$MESS['B2CPL_FORM_HDR_FORM'] = 'Заявка на доставку B2CPL';
$MESS['B2CPL_FORM_HDR_PARAMS'] = 'Заявка';
$MESS['B2CPL_FORM_HDR_ADDRESS'] = 'Адрес';
$MESS['B2CPL_FORM_HDR_BUYER'] = 'Покупатель';
$MESS['B2CPL_FORM_HDR_ADDITIONAL'] = 'Дополнительно';
$MESS['B2CPL_FORM_HDR_DATE'] = 'Дата';

$MESS['B2CPL_FORM_OK'] = 'Заявка успешно отправлена';

$MESS['B2CPL_FORM_BTN_SAVESEND'] = 'Сохранить и отправить';
$MESS['B2CPL_FORM_BTN_SHOWRESULT'] = 'Протокол';
$MESS['B2CPL_FORM_BTN_DELETE'] = 'Удалить';
$MESS['B2CPL_FORM_BTN_STICKERS'] = 'Наклейки';

$MESS["B2CPL_FORM_PROTOKOL_CANT_GET_INFO"] = "Не удалось получить информацию";
$MESS["B2CPL_FORM_PROTOKOL_POPUP_TITLE"] = "Протокол";
$MESS["B2CPL_FORM_PROTOKOL_POPUP_LOADING"] = "Загрузка...";

$MESS["B2CPL_LOG_SEND_REQUEST"] = "Отправка запроса...";
$MESS["B2CPL_LOG_ORDER_CREATED"] = "Заказ отправлен.";

$MESS['B2CPL_UPDORD_index']   = 'Не указан индекс отправления.';
$MESS['B2CPL_UPDORD_city']    = 'Не указан город отправления.';
$MESS['B2CPL_UPDORD_address'] = 'Не указан адрес отправления.';
$MESS['B2CPL_UPDORD_fio']     = 'Не указано контактное лицо.';
$MESS['B2CPL_UPDORD_mphone']  = 'Не указан мобильный телефон.';
$MESS['B2CPL_UPDORD_orderif'] = 'Не указан номер заказа.';

$MESS['B2CPL_OREDEREDIT_paysysChenged'] = 'Платежная система была изменена. Для корректности расчета доставки нажмите "Применить", после чего - проверьте доступность выбранного способа доставки.';

$MESS['B2CPL_EXPORT_UNGIVEN'] = 'Не указано: ';

$MESS['B2CPL_EXPORT_ORDER_ID'] = 'Номер посылки';
$MESS['B2CPL_EXPORT_CLIENT_ID'] = 'Номер клиента';
$MESS['B2CPL_EXPORT_DATE'] = 'Дата заказа';
$MESS['B2CPL_EXPORT_F_ZIP'] = 'Индекс';
$MESS['B2CPL_EXPORT_F_CITY'] = 'Город';
$MESS['B2CPL_EXPORT_F_ADDRESS'] = 'Адрес';
$MESS['B2CPL_EXPORT_F_NAME'] = 'ФИО';
$MESS['B2CPL_EXPORT_F_PHONE'] = 'Телефон мобильный';
$MESS['B2CPL_EXPORT_F_APHONE'] = 'Телефон дополнительный';
$MESS['B2CPL_EXPORT_F_EMAIL'] = 'E-mail';
$MESS['B2CPL_EXPORT_DATE_DELIV'] = 'Дата доставки';
$MESS['B2CPL_EXPORT_INTERVAL'] = 'Интервал доставки';

$MESS['B2CPL_EXPORT_WEIGHT'] = 'Вес посылки';
$MESS['B2CPL_EXPORT_DELIVERY_PRICE'] = 'Полная стоимость доставки';
$MESS['B2CPL_EXPORT_DELIVERY_PRICE_BALANCE'] = 'Стоимость доставки к оплате';
$MESS['B2CPL_EXPORT_ARTNUMBER'] = 'Артикул';
$MESS['B2CPL_EXPORT_PRODUCT_NAME'] = 'Товар';
$MESS['B2CPL_EXPORT_QUANTITY'] = 'Кол-во ед. товара';
$MESS['B2CPL_EXPORT_PRICE'] = 'Полная стоимость ед. товара';
$MESS['B2CPL_EXPORT_PRICE_BALANCE'] = 'Стоимость ед. товара к оплате';
$MESS['B2CPL_EXPORT_PRODUCT_WEIGHT'] = 'Вес товара';
$MESS['B2CPL_EXPORT_DELIVERY_TYPE'] = 'Тип доставки';
$MESS['B2CPL_EXPORT_AVIA'] = 'авиа';
$MESS['B2CPL_EXPORT_PACK'] = 'Упаковка';
$MESS['B2CPL_EXPORT_ASSESSED'] = 'Оценочная стоимость посылки';
$MESS['B2CPL_EXPORT_ERROR'] = 'При выгрузке возникла ошибка';

$MESS['B2CPL_SAKHALINSK_1'] = 'южно';
$MESS['B2CPL_SAKHALINSK_2'] = 'сахалинск';

$MESS['B2CPL_EXPORT_REZ_SENDED'] = 'Заявка отослана';
$MESS['B2CPL_EXPORT_REZ_noAuth'] = 'Не заполнены логин / пароль.';
$MESS['B2CPL_EXPORT_REZ_noVals'] = 'Не удалось получить данные о заказе.';
$MESS['B2CPL_EXPORT_REZ_noGoods'] = 'Не удалось получить товары из заказа.';
$MESS['B2CPL_EXPORT_REZ_noUpdt'] = 'Не удалось сохранить информацию о заявке.';

$MESS ['B2CPL_REQ_STAT_NEW'] = "Заявка на доставку заказа еще не отсылалась.";
$MESS ['B2CPL_REQ_STAT_OK'] = "Заявка на доставку подтверждена.";
$MESS ['B2CPL_REQ_STAT_DELIVD'] = "Заказ уже доставлен клиенту.";
$MESS ['B2CPL_REQ_STAT_OTKAZ'] = "Клиент уже отказался от заказа.";
$MESS ['B2CPL_REQ_STAT_STORE'] = "Заказ уже на складе СДЭК.";
$MESS ['B2CPL_REQ_STAT_CORIER'] = "Заказ уже у курьера СДЭК.";
$MESS ['B2CPL_REQ_STAT_PVZ'] = "Заказ уже на пункте самовывоза СДЭК.";
$MESS ['B2CPL_REQ_STAT_TRANZT'] = "Заказ в пути.";
$MESS ['B2CPL_REQ_STAT_ERROR'] = "Заявка отклонена из-за ошибок в параметрах. Исправьте ошибки и отправьте ее заново.";

$MESS ['B2CPL_AUTH_ERROR'] = "Ошибка авторизации. Проверьте введенные данные и повторите попытку.";
$MESS ['B2CPL_AUTH_GOOD'] = "Вы успешно авторизовались.";

// печать
$MESS ['B2CPL_SIGN_PRINTSTICKERS'] = "Печать наклеек B2CPL";

$MESS["B2CPL_DELIVERY_OSIBKA"] = "Ошибка:";
$MESS["B2CPL_DELIVERY_USPEH"] = "Успешное оформление:";
$MESS["B2CPL_DELIVERY_POSLEDNAA_ZAPISQ"] = "Последняя запись";
$MESS["B2CPL_DELIVERY_OTPRAVKI_ZAKAZA_NE_O"] = "Отправки заказа не осуществлялось.";

$MESS["B2CPL_DELIVERY_wrong_pack"] = "Неправильный тип упаковки.";
?>

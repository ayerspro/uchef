<?
$MESS ['B2CPL_MODULE_NAME'] = "B2CPL Доставка";
$MESS ['B2CPL_MODULE_DESCRIPTION'] = "Модуль доставки от компании B2CPL.";
$MESS ['B2CPL_PARTNER_NAME'] = "B2CPL";
$MESS ['B2CPL_NEED_RIGHT_VER'] = "Для установки данного решения необходима версия главного модуля #NEED# или выше.";
$MESS ['B2CPL_NEED_MODULES'] = "Для установки данного решения необходимо наличие модуля #MODULE#.";
?>
<?php

IncludeModuleLangFile(__FILE__);
$db_type = strtolower($GLOBALS['DB']->type);
CModule::AddAutoloadClasses(
	'b2cpl.delivery',
	$a = array(
		'Cb2cplLogs'     => 'classes/' . $db_type . '/logs.php',
		'Cb2cplOrder'    => 'classes/' . $db_type . '/orders.php',
		'CB2CPLOpts'     => 'classes/general/optionClass.php',
		'Cb2cplDelivery' => 'classes/general/deliveryClass.php',
		'Cb2cplRequest'  => 'classes/general/requestClass.php',
		'Cb2cplHelper'   => 'classes/general/helperClass.php',
	)
);

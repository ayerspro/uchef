<?
#################################################
#        Company developer: IPOL
#        Developers: Nikta Egorov
#        Site: http://www.ipol.com
#        E-mail: om-sv2@mail.ru
#        Copyright (c) 2006-2015 IPOL
#################################################
?>
<?

IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");

$module_id = "b2cpl.delivery";
CModule::IncludeModule($module_id);
CModule::IncludeModule('sale');
CJSCore::Init(array("jquery"));
Cb2cplRequest::$MODULE_ID;

//�����������
$payers=CSalePersonType::GetList(array('ACTIVE'=>'Y'));
$arPayers=array();
while($payer=$payers->Fetch())
	$arPayers[$payer['ID']]=array('NAME'=>$payer['NAME']." [".$payer['LID']."]");

//��������
$arPacks = array();
foreach(array('','omni','callomni','call','callIM') as $pack)
	$arPacks[$pack] = GetMessage('B2CPL_FORM_pack_'.$pack);

//�������
$arStatusesList = array(''=>'');
if (CModule::IncludeModule('sale')) {
	$rsList = CSaleStatus::GetList();
	while($arStatus = $rsList->Fetch()) {
		// $arStatus['NAME'] = str_replace(array('/', GetMessage('B2CPL_DELIVERY_NUM')), array('-', 'N'), $arStatus['NAME']);
		$arStatusesList[$arStatus['ID']] = $arStatus['NAME'];
	}
}

//�������
$sendRegions = CB2CPLOpts::getRegions();

$arAllOptions = array(
	"auth" => array(
		array("logged","logged",false,array('text')),
		array("client",GetMessage("B2CPL_OTP_client"),false,array('text')),
		array("key",GetMessage("B2CPL_OTP_key"),false,array('text')),
	),
	"common" => array(
		array("howSend",GetMessage("B2CPL_OPT_howSend"),"F",array("selectbox"),array("F" => GetMessage('B2CPL_OPT_howSend_form'),"S" => GetMessage('B2CPL_OPT_howSend_status'),"O" => GetMessage('B2CPL_OPT_howSend_create'))),
		array("order_status_for_upload",GetMessage("B2CPL_OTP_order_status_for_upload"),false,array("selectbox"),$arStatusesList),
		array("showInOrders",GetMessage("B2CPL_OPT_showInOrders"),"Y",array("selectbox"),array("Y" => GetMessage('B2CPL_OPT_showInOrders_always'),"N" => GetMessage('B2CPL_OPT_showInOrders_delivery'))),
		array("order_prop_for_b2ccode",GetMessage("B2CPL_OTP_order_prop_for_b2ccode"),false,array("text")),
		array("senderRegion",GetMessage("B2CPL_OTP_senderRegion"),false,array("selectbox"),$sendRegions),
	),
	"delivery" => array(
		array("default_weight",GetMessage("B2CPL_OTP_default_weight"),false,array('text')),
		array("post",GetMessage("B2CPL_OTP_post"),"Y",array('checkbox')),
		//array("shortPVZ",GetMessage("B2CPL_OTP_shortPVZ"),"Y",array('checkbox')),
		array("around",GetMessage("B2CPL_OTP_around"),"N",array('checkbox')),
		array("assessed_no_include",GetMessage("B2CPL_OTP_assessed_no_include"),"N",array('checkbox')),
		array("property_v_code",GetMessage("B2CPL_OPT_property_v_code"),"",array("text")),
		array("termsOn",GetMessage("B2CPL_OTP_termsOn"),"N",array('checkbox')),
		array("termsInc",GetMessage("B2CPL_OPT_termsInc"),"",array("text")),
	),
	"commonWidjet" => array(
		// array("addJQ",GetMessage("B2CPL_OPT_addJQ"),"Y",array("checkbox")),
	),
	"timeWidget" => array(
		array("enTP",GetMessage("B2CPL_OPT_enTP"),"Y",array("checkbox")),
		array("addJQUI",GetMessage("B2CPL_OPT_addJQUI"),"Y",array("checkbox")),
		array("timePickerId",GetMessage("B2CPL_OPT_timePickerId"),"",array("text")),
	),
	"pvzWidget" => array(
		array("pvzPickerId",GetMessage("B2CPL_OPT_pvzPickerId"),"",array("text")),
	),
	"widget" => array(
		array("enTP",GetMessage("B2CPL_OPT_enTP"),"Y",array("checkbox")),
		array("addJQUI",GetMessage("B2CPL_OPT_addJQUI"),"Y",array("checkbox")),
		array("timePickerId",GetMessage("B2CPL_OPT_timePickerId"),"",array("text")),
	),
 	"payments" => array(
		array("freeDeliv",GetMessage("B2CPL_OPT_freeDeliv"),0,array("text")),
		array("dostModType",GetMessage("B2CPL_OPT_dostMod"),0,array("text")),
		array("dostModVal",GetMessage("B2CPL_OPT_dostMod"),0,array("text")),
		array("dostModKhar",GetMessage("B2CPL_OPT_dostMod"),0,array("text")),
	),
	"mps" => Array(
		array("citySetups",GetMessage("B2CPL_OPT_CITIES"),"a:{}",array("text")),
		array("gmpSetups",GetMessage("B2CPL_OPT_GROUPMP"),"a:{}",array("text")),
	),
	"elements" => array(
		array("articul",GetMessage("B2CPL_OPT_articul"),"ARTNUMBER",array("text")),
		array("weight",GetMessage("B2CPL_OPT_weight"),"",array("text")),
	),
	"props" => Array(
		array("index",GetMessage("B2CPL_OPT_index"),"ZIP",array("text")),
		array("city",GetMessage("B2CPL_OPT_city"),"LOCATION",array("text")),
		array("address",GetMessage("B2CPL_OPT_address"),"ADDRESS",array("text")),
		array("fio",GetMessage("B2CPL_OPT_fio"),"FIO",array("text")),
		array("mphone",GetMessage("B2CPL_OPT_mphone"),"PHONE",array("text")),
		array("uphone",GetMessage("B2CPL_OPT_uphone"),"",array("text")),
		array("email",GetMessage("B2CPL_OPT_email"),"EMAIL",array("text")),
	),
	"formSetups" => array(
		array("defaultPack",GetMessage("B2CPL_OPT_defaultPack"),"",array("selectbox"),$arPacks),
	  array("needSync",GetMessage("B2CPL_OPT_needSync"),"Y",array("checkbox"))
	),
	"paySystems" => array(
		array("paySystemsWatch",GetMessage("B2CPL_OPT_paySystemsWatch"),"Y",array('checkbox')),
		array("paySystems",GetMessage("B2CPL_OPT_paySystems"),"",array("text")),
	),
);

$isLogged = COption::GetOptionString($module_id,'logged',false);
$hasOld = CB2CPLOpts::checkOldPVZ();
$hasPickup = CB2CPLOpts::checkPickup();

if($isLogged){
	$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("B2CPL_TAB_FAQ"), "TITLE" => GetMessage("B2CPL_TAB_TITLE_FAQ")),
		array("DIV" => "edit2", "TAB" => GetMessage("MAIN_TAB_SET"), "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
		array("DIV" => "edit3", "TAB" => GetMessage("B2CPL_TAB_CITIES"), "TITLE" => GetMessage("B2CPL_TAB_TITLE_CITIES")),
		array("DIV" => "edit4", "TAB" => GetMessage("B2CPL_TAB_LIST"), "TITLE" => GetMessage("B2CPL_TAB_TITLE_LIST")),
	);
	if($hasOld)
		$aTabs[] = array("DIV" => "edit5", "TAB" => GetMessage("B2CPL_TAB_OLDDELIV"), "TITLE" => GetMessage("B2CPL_TAB_TITLE_OLDDELIV"));
}else
	$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("B2CPL_TAB_AUTH"), "TITLE" => GetMessage("B2CPL_TAB_TITLE_AUTH")),
	);

//Restore defaults
if ($USER->IsAdmin() && $_SERVER["REQUEST_METHOD"]=="GET" && strlen($RestoreDefaults)>0 && check_bitrix_sessid())
    COption::RemoveOption($module_id);

//Save options
if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$RestoreDefaults)>0 && check_bitrix_sessid()){
	rebuildRequestMP();	
	rebuildRequestCS();
	rebuildRequestPM();
	foreach(array('termsInc') as $key)
		if(!is_numeric($_REQUEST[$key]))
			$_REQUEST[$key] = '';
	if(strlen($RestoreDefaults)>0)
		COption::RemoveOption($module_id);
	else{
		$_REQUEST['paySystems'] = ($_REQUEST['paySystems']) ?  serialize($_REQUEST['paySystems']) : 'a:0:{}';
		foreach($arAllOptions as $aOptGroup)
			foreach($aOptGroup as $option)
				__AdmSettingsSaveOption($module_id, $option);
	}

	if($_REQUEST["back_url_settings"] <> "" && $_REQUEST["Apply"] == "")
		 echo '<script type="text/javascript">window.location="'.CUtil::addslashes($_REQUEST["back_url_settings"]).'";</script>';
}

function ShowParamsHTMLByArray($arParams){
	global $module_id;
	foreach($arParams as $Option){
		if($Option[3][0]!='selectbox')
			__AdmSettingsDrawRow($module_id, $Option);
		else{
			$optVal=COption::GetOptionString($module_id,$Option['0'],$Option['2']);
			$str='';
			foreach($Option[4] as $key => $val){
				$chkd='';
				if($optVal==$key)
					$chkd='selected';
				$str.='<option '.$chkd.' value="'.$key.'">'.$val.'</option>';
			}
			echo '<tr>
					<td width="50%" class="adm-detail-content-cell-l">'.$Option[1].'</td>  
					<td width="50%" class="adm-detail-content-cell-r"><select name="'.$Option['0'].'">'.$str.'</select></td>
				</tr>';
		}
	}
}

function rebuildRequestCS(){
	$arCS = array();
	if(!empty($_REQUEST['b2cpl_citysetup_setup']))
		foreach($_REQUEST['b2cpl_citysetup_setup'] as $ind => $val)
			if($val && $_REQUEST["b2cpl_citysetup_city_$ind"])
				$arCS[$_REQUEST["b2cpl_citysetup_city_$ind"]] = $val;
	$_REQUEST['citySetups'] = serialize($arCS);
}

function rebuildRequestMP(){
	$arMP = array();
	foreach($_REQUEST['b2cpl_mpsetup'] as $vals){
		if(!array_key_exists($vals['mp'],$arMP) && $vals['st'])
			$arMP[$vals['mp']] = $vals['st'];
	}
	$_REQUEST['gmpSetups'] = serialize($arMP);
}

function rebuildRequestPM(){
	if($_REQUEST['dostModType'] == 'exact'){
		$arVals = array();
		foreach(array('Courier','PVZ','POST') as $type)
			if($_REQUEST['fixPrice'.$type] > 0)
				$arVals[$type] = $_REQUEST['fixPrice'.$type];
		if(count($arVals))
			$_REQUEST['dostModVal'] = serialize($arVals);
		else
			$_REQUEST['dostModType'] = false;
	}
}

function showOrderOptions(){//������ ���������� ����� ��������� ������������
	global $module_id;
	global $arPayers;
	$arNomatterProps=array('uphone','email');
	foreach($GLOBALS['arAllOptions']['props'] as $orderProp){
		$value=COption::getOptionString($module_id,$orderProp[0],$orderProp[2]);
		if(!trim($value) && !in_array($orderProp[0],$arNomatterProps))
			$showErr=true;
		else
			$showErr=false;

		$arError=array(
			'noPr'=>false,
			'unAct'=>false,
			'str'=>false,
		);

		if($value){
			foreach($arPayers as $payId =>$payerInfo)
				if($curProp=CSaleOrderProps::GetList(array(),array('PERSON_TYPE_ID'=>$payId,'CODE'=>$value))->Fetch()){
					if($curProp['ACTIVE']!='Y')
						$arError['unAct'].="<br>".$payerInfo['NAME'];
				}
				else
					$arError['noPr'].="<br>".$payerInfo['NAME'];

			if($arError['noPr']){
				$arError['str']=GetMessage('B2CPL_LABEL_noPr')." <a href='#' class='PropHint' onclick='return ipol_popup_virt(\"pop-noPr_".$orderProp[0]."\",$(this));'></a> ";?>
				<div id="pop-noPr_<?=$orderProp[0]?>" class="b-popup" style="display: none; ">
					<div class="pop-text"><?=GetMessage('B2CPL_LABEL_Sign_noPr')?><br><br><?=substr($arError['noPr'],4)?></div>
					<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
				</div>
			<?}
			if($arError['unAct']){
				$arError['str'].=GetMessage('B2CPL_LABEL_unAct')." <a href='#' class='PropHint' onclick='return ipol_popup_virt(\"pop-unAct_".$orderProp[0]."\",$(this));'></a>";?>
				<div id="pop-unAct_<?=$orderProp[0]?>" class="b-popup" style="display: none; ">
					<div class="pop-text"><?=GetMessage('B2CPL_LABEL_Sign_unAct')?><br><br><?=substr($arError['unAct'],4)?></div>
					<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
				</div>
			<?}
			
			if($arError['str'])
				$showErr=true;
		}
		
		$styleTdStr = ($orderProp[0] == 'street')?'style="border-top: 1px solid #BCC2C4;"':'';
	?>
		<tr>
			<td width="50%" <?=$styleTdStr?> class="adm-detail-content-cell-l"><?=$orderProp[1]?></td>
			<td width="50%" <?=$styleTdStr?> class="adm-detail-content-cell-r">
				<input type="text" size="" maxlength="255" value="<?=$value?>" name="<?=$orderProp[0]?>">
				&nbsp;&nbsp;<span class='errorText' <?if(!$showErr){?>style='display:none'<?}?>><?=($arError['str'])?$arError['str']:GetMessage('B2CPL_LABEL_shPr')?></span>
			</td>
		</tr>
	<?}
}

if($hasOld)
	CB2CPLOpts::printOldDelivError();
if(!$hasPickup)
	CB2CPLOpts::printNpPickup();

$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>

<style>
	.errorText{
		color: red;
	}
	.PropHint { 
		background: url("/bitrix/images/<?=$module_id?>/hint.gif") no-repeat transparent;
		display: inline-block;
		height: 12px;
		position: relative;
		width: 12px;
	}
	.b-popup { 
		background-color: #FEFEFE;
		border: 1px solid #9A9B9B;
		box-shadow: 0px 0px 10px #B9B9B9;
		display: none;
		font-size: 12px;
		padding: 19px 13px 15px;
		position: absolute;
		top: 38px;
		width: 300px;
		z-index: 50;
	}
	.b-popup .pop-text { 
		margin-bottom: 10px;
		color:#000;
	}
	.pop-text i {color:#AC12B1;}
	.b-popup .close { 
		background: url("/bitrix/images/<?=$module_id?>/popup_close.gif") no-repeat transparent;
		cursor: pointer;
		height: 10px;
		position: absolute;
		right: 4px;
		top: 4px;
		width: 10px;
	}
	.subHeading td{
		padding: 8px 70px 10px !important;
		background-color: #EDF7F9;
		border-top: 11px solid #F5F9F9;
		border-bottom: 11px solid #F5F9F9;
		color: #4B6267;
		font-size: 14px;
		font-weight: bold;
		text-align: center !important;
		text-shadow: 0px 1px #FFF;
	}
</style>
<script>
	function ipol_popup_virt(code, info){
		var offset = $(info).position().top;
		var LEFT = $(info).offset().left;		
		
		var obj;
		if(code == 'next') 	obj = $(info).next();
		else  				obj = $('#'+code);
		
		LEFT -= parseInt( parseInt(obj.css('width'))/2 );
		
		obj.css({
			top: (offset+15)+'px',
			left: LEFT,
			display: 'block'
		});	
		return false;
	}
</script>

<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&amp;lang=<?echo LANG?>">
	<?
	$tabControl->Begin();
	$tabControl->BeginNextTab();
	if($isLogged){
		include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".$module_id ."/optionsInclude/faq.php");
		$tabControl->BeginNextTab();
		include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".$module_id ."/optionsInclude/setups.php");	
		$tabControl->BeginNextTab();
		include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".$module_id ."/optionsInclude/cities.php");
		$tabControl->BeginNextTab();
		include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".$module_id ."/optionsInclude/table.php");
		if($hasOld){
			$tabControl->BeginNextTab();
			include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".$module_id ."/optionsInclude/oldPVZ.php");
		}
	}else
		include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".$module_id ."/optionsInclude/login.php");
	$tabControl->Buttons();
	?>
	<div align="left">
		<input type="hidden" name="Update" value="Y">
		<input type="submit" <?if(!$USER->IsAdmin())echo " disabled ";?> name="Update" value="<?echo GetMessage("MAIN_SAVE")?>">
	</div>
	<?$tabControl->End();?>
	<?=bitrix_sessid_post();?>
</form>

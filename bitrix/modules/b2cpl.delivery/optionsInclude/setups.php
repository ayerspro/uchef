<?
//��������� �������
$PayDefault = COption::GetOptionString($module_id,'paySystems','Y');
if($PayDefault != 'Y')
	$tmpPaySys=unserialize($PayDefault);

$paySysS=CSalePaySystem::GetList(array(),array('ACTIVE'=>'Y'));
$paySysHtml='<select name="paySystems[]" multiple size="5">';
while($paySys=$paySysS->Fetch()){
	$paySysHtml.='<option value="'.$paySys['ID'].'" ';
	if($PayDefault == 'Y') {
		$name = strtolower($paySys['NAME']);
		if( strpos($name, GetMessage('B2CPL_nal1')) === false && 
			strpos($name, GetMessage('B2CPL_nal2')) === false && 
			strpos($name, GetMessage('B2CPL_nal3')) === false)
			$paySysHtml.='selected';
	}elseif(in_array($paySys['ID'],$tmpPaySys))
		$paySysHtml.='selected';
	$paySysHtml.='>'.$paySys['NAME'].'</option>';
}
$paySysHtml.="</select>";
?>

<script>
	function b2cpl_dostModChnge(wat){
		var val = wat.val();

		if(val == '' || val == 'exact') $('#dostModVal').css('display','none');
		else                            $('#dostModVal').css('display','inline');
		
		if(val == 'discount' || val == 'mark') $('#dostModKhar').css('display','inline');
		else								   $('#dostModKhar').css('display','none');
		
		if(val == 'exact') $('#fixPrice').css('display','table');
		else               $('#fixPrice').css('display','none');
		
		if(val == 'order') $('#proclbl').css('display','inline');
		else               $('#proclbl').css('display','none');
	}
	
	function b2cpl_blockNonNumbers(wat){
		var tmp = parseFloat(wat.val());
		if(isNaN(tmp))
			tmp = 0;
		wat.val(tmp);
	}
	
	function b2cpl_logoff(){
		$("[onclick='b2cpl_logoff()']").attr('disabled','disabled');
		$.post('/bitrix/tools/<?=$module_id?>/ajax.php',{action:'logoff'},function(){window.location.reload()});
	}

	function b2cpl_clrCache(){
		$("[onclick='b2cpl_clrCache()']").attr('disabled','disabled');
		$.post('/bitrix/tools/<?=$module_id?>/ajax.php',{action:'killCache'},function(data){alert(data);$("[onclick='b2cpl_clrCache()']").removeAttr('disabled');});
	}
	
	$(document).ready(function(){
		b2cpl_dostModChnge($("[name='dostModType']"));
		$('[name="termsInc"]').on('change',function(){b2cpl_blockNonNumbers($(this));});
	});
</script>

<?// �����?>
<?foreach(array('howSend','showInOrders','order_prop_for_b2ccode','addJQ','termsOn','articul','weight','timePickerId','pvzPickerId') as $id){?>
<div id="pop-<?=$id?>" class="b-popup" style="display: none; ">
	<div class="pop-text"><?=GetMessage("B2CPL_HELPER_".$id)?></div>
	<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
</div>
<?}?>

<?// �����������?>
<tr><td style='vertical-align:top;'><?=GetMessage('B2CPL_LABEL_YOID')?> <strong><?=COption::GetOptionString($module_id,'client',false)?></strong></td><td style='text-align:center'><input type='button' value='<?=GetMessage('B2CPL_LABEL_logoff')?>' onclick='b2cpl_logoff()'><br><br><input type='button' value='<?=GetMessage('B2CPL_LABEL_clrCache')?>' onclick='b2cpl_clrCache()'></td></tr>
<?// �����?>
<tr class="heading"><td colspan="2" valign="top" align="center"><?=GetMessage("B2CPL_HDR_common")?></td></tr>
<?ShowParamsHTMLByArray($arAllOptions['common'])?>

<?// ��������� ��������?>
<tr class="heading"><td colspan="2" valign="top" align="center"><?=GetMessage("B2CPL_HDR_delivery")?></td></tr>
<?ShowParamsHTMLByArray($arAllOptions['delivery'])?>

<?// ��������� �������?>
<tr class="heading"><td colspan="2" valign="top" align="center"><?=GetMessage("B2CPL_HDR_widget")?></td></tr>
<?ShowParamsHTMLByArray($arAllOptions['commonWidjet'])?>
		<?// FAQ?>
<tr><td style="color:#555;" colspan="2">
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_WIDGETTIME_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_WIDGETTIME_DESCR')?></div>
</td></tr>
<?ShowParamsHTMLByArray($arAllOptions['timeWidget'])?>		
		<?// FAQ?>
<tr><td style="color:#555;" colspan="2">
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_WIDGETPVZ_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_WIDGETPVZ_DESCR')?></div>
</td></tr>
<?ShowParamsHTMLByArray($arAllOptions['pvzWidget'])?>

<?// ���������� ������?>
<tr class="heading"><td colspan="2" valign="top" align="center"><?=GetMessage("B2CPL_HDR_forming")?></td></tr>

<tr class='subHeading'><td colspan="2" valign="top" align="center"><?=GetMessage("B2CPL_SUBHDR_goods")?></td></tr>
<?ShowParamsHTMLByArray($arAllOptions['elements'])?>

<tr class='subHeading'><td colspan="2" valign="top" align="center"><?=GetMessage("B2CPL_SUBHDR_props")?></td></tr>

	<?// FAQ?>
<tr><td style="color:#555;" colspan="2">
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_REQPROPS_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_REQPROPS_DESCR')?></div>
</td></tr>

<?showOrderOptions();?>

<tr class='subHeading'><td colspan="2" valign="top" align="center"><?=GetMessage("B2CPL_SUBHDR_form")?></td></tr>
<?ShowParamsHTMLByArray($arAllOptions['formSetups']);?>

<?// ��������� ��������?>

<tr class="heading"><td colspan="2" valign="top" align="center"><?=GetMessage("B2CPL_HDR_payments")?></td></tr>

	<?// FAQ?>
<tr><td style="color:#555;" colspan="2">
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_DP_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_DP_DESCR')?></div>
</td></tr>

	<?// ���������� ��������?>
<tr><td width='50%'><?=GetMessage("B2CPL_OPT_freeDeliv")?></td><td width='50%'><input size="3" value="<?=COption::GetOptionString($module_id,'freeDeliv','0')?>" name="freeDeliv" type="text" style="text-align:right" onchange='b2cpl_blockNonNumbers($(this))'> <?=GetMessage("B2CPL_LBL_RUB")?></td></tr>

	<?// ����������� ��������?>

<?
	$dostModType = COption::GetOptionString($module_id,'dostModType','');
	$dostModVal = COption::GetOptionString($module_id,'dostModVal','0');
	$dostModKhar = COption::GetOptionString($module_id,'dostModKhar','');
	if($dostModType == 'exact')
		$dostModVal = unserialize($dostModVal);
?>
<tr>
	<td width='50%' valign = 'top'><?=GetMessage("B2CPL_OPT_dostMod")?></td>
	<td width='50%'>
		<div style='float:left'>
			<select name='dostModType' onchange='b2cpl_dostModChnge($(this))'>
				<option value=''></option>
				<option value='discount' <?=($dostModType == "discount") ? 'selected' : ''?>><?=GetMessage("B2CPL_LBL_discount")?></option>
				<option value='mark'     <?=($dostModType == "mark")     ? 'selected' : ''?>><?=GetMessage("B2CPL_LBL_mark")?>    </option>
				<option value='order'    <?=($dostModType == "order")    ? 'selected' : ''?>><?=GetMessage("B2CPL_LBL_order")?>   </option>
				<option value='exact'    <?=($dostModType == "exact")    ? 'selected' : ''?>><?=GetMessage("B2CPL_LBL_exact")?>   </option>
			</select>
			<input size="3" value="<?=(is_array($dostModVal))?0:$dostModVal?>" id="dostModVal" name="dostModVal" type="text" style="text-align:right" onchange='b2cpl_blockNonNumbers($(this))'/>
			<select name='dostModKhar' id="dostModKhar">
				<option value='RUB'  <?=($dostModKhar == "RUB")  ? 'selected' : ''?>><?=GetMessage("B2CPL_LBL_RUB")?></option>
				<option value='proc' <?=($dostModKhar == "proc") ? 'selected' : ''?>>%</option>
			</select>
			<span id='proclbl'>%</span>
		</div>
		<table id='fixPrice' style='float:left; margin-left: 5px;'>
			<tr>
				<td><?=GetMessage("B2CPL_LBL_courier")?></td>
				<td><input size="3" value="<?=(is_array($dostModVal) && $dostModVal['Courier'])?$dostModVal['Courier']:0?>" id="fixPriceCourier" name="fixPriceCourier" type="text" style="text-align:right" onchange='b2cpl_blockNonNumbers($(this))'/></td>
				<td><?=GetMessage("B2CPL_LBL_RUB")?></td>
			</tr>			
			<tr>
				<td><?=GetMessage("B2CPL_LBL_PVZ")?></td>
				<td><input size="3" value="<?=(is_array($dostModVal) && $dostModVal['PVZ'])?$dostModVal['PVZ']:0?>" id="fixPricePVZ" name="fixPricePVZ" type="text" style="text-align:right" onchange='b2cpl_blockNonNumbers($(this))'/></td>
				<td><?=GetMessage("B2CPL_LBL_RUB")?></td>
			</tr>			
			<tr>
				<td><?=GetMessage("B2CPL_LBL_POST")?></td>
				<td><input size="3" value="<?=(is_array($dostModVal) && $dostModVal['POST'])?$dostModVal['POST']:0?>" id="fixPricePOST" name="fixPricePOST" type="text" style="text-align:right" onchange='b2cpl_blockNonNumbers($(this))'/></td>
				<td><?=GetMessage("B2CPL_LBL_RUB")?></td>
			</tr>
		</table>
	</td>
</tr>
<?// ��������� �������?>

<tr class="heading"><td colspan="2" valign="top" align="center"><?=GetMessage("B2CPL_OPT_paySystems")?></td></tr>
	<?// FAQ?>
<tr><td style="color:#555;" colspan="2">
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_PAYSYS_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_PAYSYS_DESCR')?></div>
</td></tr>
<tr>
	<td><label for="paySystemsWatch"><?=GetMessage("B2CPL_OPT_paySystemsWatch")?></label></td>
	<td><input id="paySystemsWatch" name="paySystemsWatch" value="Y" <?=(COption::getOptionString($module_id,'paySystemsWatch','Y')=='Y')?'checked':''?> type="checkbox"></td>
</tr>
<tr><td colspan="2" style='text-align:center'><?=$paySysHtml?></td></tr>
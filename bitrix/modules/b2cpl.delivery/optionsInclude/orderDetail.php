<?
global $USER;
if(!$USER->IsAdmin()){
	return;
}
$orderId = (int) $_REQUEST['ID'];

if ( $ordrVals = self::GetByOI($orderId) ) { // �������� ��������� ������ �� ��, ���� ��� ����
	$status = $ordrVals['STATUS'];
	$b2Code = $ordrVals['B2CCODE'];
	$stikersUrl = $ordrVals['STICKERS_URL'];
	$ordrVals = unserialize($ordrVals['PARAMS']); // ������ �������� ������, ���� �� ����� - ����������� �� ��������� �� ����������, ��������� � ������ � �����������
	$isLoaded = true;
} else {
	$isLoaded = false;
}

if ( !$status ) {
	$status = 'NEW';
}
CJSCore::Init(array("jquery"));

$orderFields = self::getOrderFields($orderId, $ordrVals);
$ordrVals = $orderFields[0];
$orderPropsMas = $orderFields[1];

if ( !$orderinfo ) // ����������� � requestClass
{
	$orderinfo = CSaleOrder::getById($orderId);
}//��������� ������

if (
	!$isLoaded &&
	array_key_exists('B2CPL_DATEDELIV', $orderPropsMas) &&
	$orderPropsMas['B2CPL_DATEDELIV'] &&
	$orderinfo['DELIVERY_ID'] == 'b2cpl:kurer'
) {
	$tmpVal = explode(" ", $orderPropsMas['B2CPL_DATEDELIV']);
	$ordrVals['issue'] = trim($tmpVal[0]);
	$ordrVals['term'] = trim($tmpVal[1]);
}
// if(!$ordrVals['comment'])
// $ordrVals['comment'] = $orderinfo['COMMENTS'];

$goods = self::getGoods($orderId);

//if ( strpos($orderinfo['DELIVERY_ID'], 'pickup') !== false ) {
//	$weight = ($goods["WEIGHT"] > 0) ? $goods["WEIGHT"] : 1;
//	$price = ($goods["PRICE"] > 0) ? $goods["PRICE"] : 0;
//	$pvz = self::getPVZForForm($ordrVals['index'], $weight, $price);
//} else {
	$pvz = false;
//}


preg_match('/(\d{4})-(\d{2})-(\d{2})/', $orderinfo['DATE_INSERT'], $orDate);

$lastRecord = Cb2cplLogs::getLastStatus($orderId);
$style = "";
if($lastRecord === "ERROR"){
	$style="color:#ff0000;";
}

$protokolBtn = '<input type=\"button\" style=\"'.$style.'\" value=\"';
$protokolBtn .= (!empty($style))?"! ".GetMessage('B2CPL_FORM_BTN_SHOWRESULT')." !":GetMessage('B2CPL_FORM_BTN_SHOWRESULT');
$protokolBtn .= '\" onclick=\"B2CPLobj.getProtokol()\"/>';
?>
<style type='text/css'>
	.PropHint {
		background: url('/bitrix/images/<?=self::$MODULE_ID?>/hint.gif') no-repeat transparent;
		display: inline-block;
		height: 12px;
		position: relative;
		width: 12px;
	}

	.PropHint:hover {
		background: url('/bitrix/images/<?=self::$MODULE_ID?>/hint.gif') no-repeat transparent !important;
	}

	.b-popup {
		background-color: #FEFEFE;
		border: 1px solid #9A9B9B;
		box-shadow: 0px 0px 10px #B9B9B9;
		display: none;
		font-size: 12px;
		padding: 19px 13px 15px;
		position: absolute;
		top: 38px;
		width: 300px;
		z-index: 10000;
	}

	.b-popup .pop-text {
		margin-bottom: 10px;
		color: #000;
	}

	.pop-text i {
		color: #AC12B1;
	}

	.b-popup .close {
		background: url('/bitrix/images/<?=self::$MODULE_ID?>/popup_close.gif') no-repeat transparent;
		cursor: pointer;
		height: 10px;
		position: absolute;
		right: 4px;
		top: 4px;
		width: 10px;
	}

	#B2CPL_wnd {
		width: 100%;
	}

	#B2CPL_wndOrder {
		width: 100%;
	}

	#B2CPL_tarifWarning {
		display: none;
	}

	.B2CPL_infoDiv {
		display: none;
	}

	#B2CPL_dateKiller {
		line-height: 2;
		color: red;
		text-decoration: none;
	}
</style>
<script>

	var B2CPLobj = {
		ordrId: "<?=$orderId?>",
		status: "<?=$status?>",
		wnd: false,

		onLoad: function() {
			if ($('#B2CPL_btn').length) {
				return;
			}
			$('.adm-detail-toolbar').find('.adm-detail-toolbar-right').prepend("<a href='javascript:void(0)' onclick='B2CPLobj.initWnd()' class='adm-btn' id='B2CPL_btn'><?=(!empty($style))?"!!! ".GetMessage('B2CPL_FORM_BTNAME')." !!!":GetMessage('B2CPL_FORM_BTNAME')?></a>");
			var btn = $('#B2CPL_btn');
			switch (B2CPLobj.status) {
				case 'NEW'    :
					break;
				case 'ERROR'  :
					btn.css('color', '#F13939');
					break;
				default       :
					btn.css('color', '#3A9640');
					break;
			}
			<?if(!empty($style)):?>
			btn.css('color', '#F13939');
			<?endif;?>
			B2CPLobj.packChange($('#B2CPL_pack'));
			B2CPLobj.onDataSelect();
		},

		isPayed: function() {
			$('#B2CPL_isPayed').html('---');
			$.post(
				"/bitrix/tools/<?=Cb2cplRequest::$MODULE_ID?>/ajax.php", {oid: B2CPLobj.ordrId, action: 'checkPayed'}, function(data) {
					$('#B2CPL_isPayed').html(data);
				}
			);
		},

		initWnd: function() {
			var savButStat = '';
			if (B2CPLobj.status != 'ERROR' && B2CPLobj.status != 'NEW') {
				savButStat = 'style="display:none"';
			}
			var delButStat = '';
			if (B2CPLobj.status != 'OK' && B2CPLobj.status != 'ERROR' && B2CPLobj.status != 'DELETD') {
				delButStat = 'style="display:none"';
			}
			var prntButStat = 'style="display:none"';
			if (B2CPLobj.status == 'OK') {
				prntButStat = '';
			}

			if (!B2CPLobj.wnd) {
				var html = $('#B2CPL_wnd').parent().html();
				$('#B2CPL_wnd').replaceWith('');
				B2CPLobj.wnd = new BX.CDialog(
					{
						title: "<?=GetMessage('B2CPL_FORM_HDR_FORM')?>",
						content: html,
						icon: 'head-block',
						resizable: true,
						draggable: true,
						height: '500',
						width: '475',
						buttons: [
							'<input type=\"button\" value=\"<?=GetMessage('B2CPL_FORM_BTN_SAVESEND')?>\"  ' + savButStat + 'onclick=\"B2CPLobj.send()\"/>', // ��������� � ���������
							'<?= $protokolBtn?>', //��������
							<?if($stikersUrl){?>'<a href="<?=$stikersUrl?>" target="_blank"><?=GetMessage("B2CPL_FORM_BTN_STICKERS")?></a>'<?}?>
						]
					}
				);
			}
			B2CPLobj.wnd.Show();
			B2CPLobj.isPayed();
		},

		check: function() {
			var dO = {};
			var reqFields = ['fio', 'mphone', 'index', 'city', 'address'];
			var notReq = {};
			if($("#B2CPL_pack").val().indexOf("call") === 0){
				notReq = {index:1, city:2, address:3};
			}

			for (var i in reqFields) { // ������������
				if (typeof(
						reqFields[i]
					) != 'string') {
					continue;
				}
				dO[reqFields[i]] = $('#B2CPL_' + reqFields[i]).val();
				if (!dO[reqFields[i]] && !notReq[reqFields[i]]) {
					return $('#B2CPL_' + reqFields[i]).closest('tr').children('td').html();
				}
			}

			reqFields = ['uphone', 'email', 'pack', 'issue', 'term'];
			for (var i in reqFields) {
				if (typeof(
						reqFields[i]
					) != 'string') {
					continue;
				}
				var hndl = $('#B2CPL_' + reqFields[i]).val();
				if (hndl && (
						reqFields[i] != 'term' || !$('#B2CPL_' + reqFields[i]).attr('disabled')
					)) {
					dO[reqFields[i]] = hndl;
				}
			}
			return dO;
		},

		getProtokol: function() {
			var dataObject = {};
			dataObject['action'] = 'getInfo';
			dataObject['orderId'] = B2CPLobj.ordrId;
			$.post(
				"/bitrix/tools/<?=Cb2cplRequest::$MODULE_ID?>/ajax.php",
				dataObject,
				function(data) {
					data = $.parseJSON(data);
					if(data.type === 'ok'){
						B2CPLobj.showProtokol(data.html);
					} else {
						alert("<?= GetMessage("B2CPL_FORM_PROTOKOL_CANT_GET_INFO")?>");
					}
				}
			);
		},

		showProtokol: function(html) {
			if(typeof html == "undefined"){
				return;
			}
			B2CPLobj.protokol = new BX.CDialog(
				{
					title: "<?= GetMessage("B2CPL_FORM_PROTOKOL_POPUP_TITLE")?>",
					content: html,
					icon: 'head-block',
					resizable: true,
					draggable: true,
					height: '500',
					width: '475'
				}
			);
			B2CPLobj.protokol.Show();
		},

		send: function() {
			var dataObject = B2CPLobj.check();
			if (typeof dataObject != 'object') {
				alert('<?=GetMessage('B2CPL_FORM_MESS_FILL')?> "' + dataObject + '"');
				return;
			}
			dataObject['action'] = 'saveAndSend';
			dataObject['orderId'] = B2CPLobj.ordrId;

			<?/*
			pvz = $("#B2CPL_pvz");

			dataObject['pvz'] = pvz.val();
			dataObject['pvz_price'] = pvz.find("option:selected").data("price");
			*/?>
			$('[onclick^="B2CPLobj.send("]').each(
				function() {
					$(this).css('display', 'none')
				}
			);
			$.post(
				"/bitrix/tools/<?=Cb2cplRequest::$MODULE_ID?>/ajax.php",
				dataObject,
				function(data) {
					if (data.indexOf('B2CPL_OK') === -1) {
						alert(data);
						$('[onclick^="B2CPLobj.send("]').each(
							function() {
								$(this).css('display', '')
							}
						);
					} else {
						alert("<?=GetMessage('B2CPL_FORM_OK')?>");
						B2CPLobj.wnd.Close();
					}
				}
			);
		},

		popup: function(code, info) {
			var offset = $(info).position().top;
			var LEFT = $(info).offset().left;

			var obj;
			if (code == 'next') {
				obj = $(info).next();
			} else {
				obj = $('#' + code);
			}
			LEFT -= parseInt(parseInt(obj.css('width')) / 2);

			obj.css(
				{
					top: offset + $(this).closest('.bx-core-adm-dialog-content').scrollTop() + 105 + 'px',
					left: LEFT,
					display: 'block'
				}
			);
			return false;
		},

		// ����� ��������
		articCheck: <?=(count($goods['FAIL'])) ? 'false' : 'true'?>,

		packChange: function(select) {
			var val = select.val();

			if (val) {
				$("#B2CPL_pack_reg").css('display', 'block');
			} else {
				$("#B2CPL_pack_reg").css('display', 'none');
			}

			if ((
					val == 'omni' || val == 'callomni'
				) && !B2CPLobj.articCheck) {
				$("#B2CPL_pack_noArt").css('display', 'block');
			} else {
				$("#B2CPL_pack_noArt").css('display', 'none');
			}
		},

		// ����

		onDataSelect: function() {
			var date = $('#B2CPL_issue').val();

			if (typeof(
					date
				) == 'undefined' || !date) {
				$('#B2CPL_term').attr('disabled', 'disabled');
				$('#B2CPL_dateKiller').css('visibility', 'hidden');
				return;
			}

			var pregDate = /(\d\d)\.(\d\d)\.([\d]{4})/;
			if (date.indexOf(':') !== -1) {
				pregDate = /(\d\d)\.(\d\d)\.([\d]{4}) ([\d]{1,2}):/;
			}
			var formDate = pregDate.exec(date);

			if (formDate) {
				$('#B2CPL_dateKiller').css('visibility', 'visible');
			}

			if (formDate && (
					formDate[3] < "<?=date("Y")?>" ||
					(
						formDate[2] < "<?=date("m")?>" && formDate[3] <= "<?=date("Y")?>"
					) ||
					(
						formDate[1] <= "<?=date("d")?>" && formDate[2] <= "<?=date("m")?>" && formDate[3] <= "<?=date("Y")?>"
					)
				)) {
				$('#B2CPL_badDate').css('display', 'block');
				$('#B2CPL_term').attr('disabled', 'disabled');
			} else {
				$('#B2CPL_badDate').css('display', 'none');
				if (formDate) {
					$('#B2CPL_term').removeAttr('disabled');
				}
			}
		},

		killDate: function() {
			$('#B2CPL_issue').val('');
			B2CPLobj.onDataSelect();
		}
	}

	$(document).ready(B2CPLobj.onLoad);
</script>
<? //<editor-fold �������� popup>?>
<div style='display:none'>
	<table id='B2CPL_wnd'>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_STATUS') ?></td>
			<td><?= $status . $satBut ?></td>
		</tr>
		<? /*!*/ ?>
		<tr>
			<td colspan='2'>
				<small><?= GetMessage('B2CPL_REQ_STAT_' . $status) ?></small><?= $message['number'] ?></td>
		</tr>
		<? if ( $b2Code ) { ?>
			<tr>
			<td><?= GetMessage('B2CPL_FORM_B2CCODE') ?></td>
			<td><?= $b2Code ?></td></tr><? } ?>

		<? //������?>

		<tr class='heading'>
			<td colspan='2'><?= GetMessage('B2CPL_FORM_HDR_PARAMS') ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_NUMBER') ?></td>
			<td><?= ($orderinfo['ACCOUNT_NUMBER']) ? $orderinfo['ACCOUNT_NUMBER'] : $orderId ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_DATE') ?></td>
			<td><?= "{$orDate[3]}.{$orDate[2]}.{$orDate[1]}" ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_ISPAYED') ?></td>
			<td id='B2CPL_isPayed'><?= ($orderinfo['PAYED'] == 'Y') ? GetMessage("MAIN_YES") : GetMessage("MAIN_NO") ?></td>
		</tr>
		<? //������?>
		<? if ( count($message['troubles']) ) { ?>
			<tr class='heading'>
				<td colspan='2'><?= GetMessage('B2CPL_FORM_errors') ?></td>
			</tr>
			<tr>
				<td colspan='2'><?= $message['troubles'] ?></td>
			</tr>
		<? } ?>

		<? //����, ������ ��� �������?>

		<? if ( $orderinfo['DELIVERY_ID'] == 'b2cpl:kurer' ) { ?>
			<tr class='heading'>
				<td colspan='2'><?= GetMessage('B2CPL_FORM_HDR_DATE') ?></td>
			</tr>
			<tr>
				<td><?= GetMessage('B2CPL_FORM_issue') ?></td>
				<td>
					<div class="adm-input-wrap adm-input-wrap-calendar">
						<input class="adm-input adm-input-calendar" id='B2CPL_issue' disabled type="text" name="B2CPL_issue" size="22" value="<?= $ordrVals['issue'] ?>">
						<span class="adm-calendar-icon" onclick="BX.calendar({node:this, field:'B2CPL_issue', form: '', bTime: false, bHideTime: true, callback_after: B2CPLobj.onDataSelect});"></span>

					</div>
					<a id='B2CPL_dateKiller' href='javascript:void(0)' onclick='B2CPLobj.killDate();'>X</a>&nbsp;
					<?= $message['issue'] ?>
				</td>
			</tr>
			<tr>
				<td><?= GetMessage('B2CPL_FORM_term') ?></td>
				<td>
					<select id='B2CPL_term' disabled='disabled'>
						<? foreach (array('full', 'first', 'sec') as $term) { ?>
							<option value='<?= $term ?>' <?= ($term == $ordrVals['term']) ? 'selected' : '' ?>><?= GetMessage("B2CPL_FORM_term_$term") ?></option>
						<? } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan='2'>
					<div id='B2CPL_badDate' class='B2CPL_infoDiv' style='color:red'><?= GetMessage("B2CPL_FORM_badDate") ?></div>
				</td>
			</tr>
		<? } ?>

		<? //�����?>

		<tr class='heading'>
			<td colspan='2'><?= GetMessage('B2CPL_FORM_HDR_ADDRESS') ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_index') ?></td>
			<td><input id='B2CPL_index' type='text' value='<?= $ordrVals['index'] ?>'><?= $message['index'] ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_city') ?></td>
			<td><input id='B2CPL_city' type='text' value='<?= $ordrVals['city'] ?>'><?= $message['city'] ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_address') ?></td>
			<td><textarea id='B2CPL_address'><?= $ordrVals['address'] ?></textarea><?= $message['address'] ?></td>
		</tr>

		<?//��� (���� ���)?>
		<?if($pvz):?>
			<tr class='heading'>
				<td colspan='2'><?= GetMessage('B2CPL_FORM_PVZ') ?></td>
			</tr>
			<tr>
				<td colspan="2"><?= GetMessage('B2CPL_FORM_PVZ_availible') ?></td>
			</tr>
			<tr>
				<td colspan="2">
					<select id='B2CPL_pvz' <?= ($status !== "NEW" || count($pvz) === 1) ? "disabled" : "" ?>>
						<? foreach ($pvz as $key => $pz) : ?>
							<? $sltd = ($orderPropsMas["B2CPL_PVZ"] === $key) ?>
							<option value='<?= $key ?>' <?= $sltd ? "selected" : "" ?> data-price="<?= $pz["stoimost_b2cpl"] ?>"><?= "{$pz["kod"]} ({$pz["adres"]}) (" . GetMessage('B2CPL_FORM_PVZ_price') . ": {$pz["stoimost_b2cpl"]})" ?></option>
						<? endforeach; ?>
					</select>
				</td>
			</tr>
		<?endif;?>

		<? //����������?>

		<tr class='heading'>
			<td colspan='2'><?= GetMessage('B2CPL_FORM_HDR_BUYER') ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_fio') ?></td>
			<td><input id='B2CPL_fio' type='text' value='<?= $ordrVals['fio'] ?>'><?= $message['fio'] ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_mphone') ?></td>
			<td><input id='B2CPL_mphone' type='text' value='<?= $ordrVals['mphone'] ?>'><?= $message['mphone'] ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_uphone') ?></td>
			<td><input id='B2CPL_uphone' type='text' value='<?= $ordrVals['uphone'] ?>'><?= $message['uphone'] ?></td>
		</tr>
		<tr>
			<td><?= GetMessage('B2CPL_FORM_email') ?></td>
			<td><input id='B2CPL_email' type='text' value='<?= $ordrVals['email'] ?>'><?= $message['email'] ?></td>
		</tr>

		<? //�������������?>

		<tr class='heading'>
			<td colspan='2'><?= GetMessage('B2CPL_FORM_HDR_ADDITIONAL') ?></td>
		</tr>
		<tr>
			<td valign='top'><?= GetMessage('B2CPL_FORM_pack') ?></td>
			<td>
				<select id='B2CPL_pack' onchange='B2CPLobj.packChange($(this))'>
					<?
					$def = ($ordrVals['pack']) ? $ordrVals['pack'] : (($isLoaded) ? "" : COption::GetOptionString(self::$MODULE_ID, "defaultPack", ''));

					foreach (array('', 'omni', 'callomni', 'call', 'callIM') as $pack) {
						?>
						<option value='<?= $pack ?>' <?= ($pack == $def) ? 'selected' : '' ?>><?= GetMessage('B2CPL_FORM_pack_' . $pack) ?></option>
					<? } ?>
				</select>&nbsp;<a href='#' class='PropHint' onclick='return B2CPLobj.popup("pop-pack",$(this));'></a>
				<?= $message['pack'] ?>
			</td>
		</tr>
		<tr>
			<td colspan='2'>
				<div id='B2CPL_pack_reg' class='B2CPL_infoDiv'><?= GetMessage("B2CPL_FORM_pack_reg") ?></div>
				<div id='B2CPL_pack_noArt' class='B2CPL_infoDiv' style='color:red'>
					<?
					echo GetMessage("B2CPL_FORM_pack_noArt");
					foreach ($goods['FAIL'] as $id => $good) {
						echo $good . " ($id)<br>";
					}
					?>
				</div>
			</td>
		</tr>
	</table>
</div>
<? //</editor-fold>?>

<div id="pop-pack" class="b-popup">
	<div class="pop-text"><?= GetMessage("B2CPL_FORM_HINT_pack") ?></div>
	<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$module_id = "b2cpl.delivery";
CModule::IncludeModule($module_id);

if($_POST["getPVZInfo"] === "y" || $_POST["checkTimePicker"] === "y"){
	$arData = array();

	$zipID = 0;
	$locationID = 0;
	foreach ($_POST["main"]["ORDER_PROP"]["properties"] as $prop){
		if($prop["IS_ZIP"] === "Y"){
			$zipID = $prop["ID"];
		} elseif ($prop["IS_LOCATION"] === "Y"){
			$locationID = $prop["ID"];
		}
	}

	foreach ($_POST["add"] as $formProp){
		if($formProp["name"] === "ORDER_PROP_".$zipID){
			$arData["LOCATION_ZIP"] = $formProp["value"];
		} elseif($formProp["name"] === "ORDER_PROP_".$locationID){
			$arData["LOCATION_TO"] = $formProp["value"];
		}
	}

	$arData["PRICE"] = $_POST["main"]["TOTAL"]["ORDER_PRICE"];

	$arResult = Cb2cplDelivery::GetPvrForComponent($arData, $_POST["checkTimePicker"] === "y");

	echo json_encode($arResult);

} elseif ( method_exists('CB2CPLOpts', $_POST['action']) ) {
	call_user_func_array(array("CB2CPLOpts", $_POST['action']), array($_POST));
} elseif ( method_exists('Cb2cplRequest', $_POST['action']) ) {
	call_user_func_array(array("Cb2cplRequest", $_POST['action']), array($_POST));
} elseif ( $_POST['action'] === "getInfo" && (int) $_POST["orderId"] > 0 ) {
	$html = Cb2cplRequest::getStatusLog();
	echo json_encode(array("type" => "ok", "html" => $html));
}

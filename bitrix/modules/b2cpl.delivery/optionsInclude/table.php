<style>
	.sortTr
	{
		cursor:pointer;
	}
	.sortTr:hover{opacity:0.7;}
	.mdTbl{overflow:hidden;}
	.B2CPL_TblStOk td{
		background-color:#E2FCE2!important;
	}
	.B2CPL_TblStErr td{
		background-color:#FFEDED!important;
	}
	.B2CPL_TblStTzt td{
		background-color:#FCFCBF!important;
	}	
	.B2CPL_TblStDel td{
		background-color:#E9E9E9!important;
	}

	.B2CPL_TblStStr td{
		background-color:#FCFFCE!important;
	}
	.B2CPL_TblStCor td{
		background-color:#D9FFCE!important;
	}	
	.B2CPL_TblStPVZ td{
		background-color:#D9FFCE!important;
	}	
	.B2CPL_TblStOtk td{
		background-color:#FFCECE!important;
	}	
	.B2CPL_TblStDvd td{
		background-color:#ABFFAB!important;
	}

	.B2CPL_TblStOk:hover td,.B2CPL_TblStErr:hover td, B2CPL_TblStTzt:hover td, B2CPL_TblStStr:hover td, B2CPL_TblStCor:hover td, B2CPL_TblStPVZ:hover td, B2CPL_TblStOtk:hover td, B2CPL_TblStDvd:hover td{
		background-color:#E0E9EC!important;
	}
	.B2CPL_crsPnt{
		cursor:pointer;
	}
	.mdTbl{
		border-bottom: 1px solid #DCE7ED;
		border-left: 1px solid #DCE7ED;
		border-right: 1px solid #DCE7ED;
		border-top: 1px solid #C4CED2;
	}
	#B2CPL_flrtTbl{
		background: url("/bitrix/panel/main/images/filter-bg.gif") transparent;
		border-bottom: 1px solid #A0ABB0;
		border-radius: 5px 5px 5px;
		text-overflow: ellipsis;
		text-shadow: 0px 1px rgba(255, 255, 255, 0.702);
	}
	.B2CPL_mrPd td{
		padding: 5px;
	}
</style>
<script type='text/javascript'>
	function B2CPL_getTable(params){
		if(typeof params == 'undefined')
			params={};

		var fltObj=B2CPL_setFilter();
		
		for(var i in fltObj)
			params[i]=fltObj[i];
		
		params['pgCnt']=(typeof params['pgCnt'] == 'undefined')?$('#B2CPL_tblPgr').val():params['pgCnt'];
		params['page']=(typeof params['page'] == 'undefined')?$('#B2CPL_crPg').html():params['page'];
		params['by']=(typeof params['by'] == 'undefined')?'ORDER_ID':params['by'];
		params['sort']=(typeof params['sort'] == 'undefined')?'DESC':params['sort'];
		params['action']='tableHandler';
		$('#B2CPL_tblPls').find('td').css('opacity','0.4');

		$.ajax({
			url:"/bitrix/tools/<?=$module_id?>/ajax.php",
			data:params,
			type:'POST',
			dataType: 'json',
			success:function(data){
				if(data['ttl']==0)
					$('#B2CPL_flrtTbl').parent().html('<?=GetMessage('B2CPL_TABLE_NO_REQ')?>');
				else
				{
					$('[onclick="B2CPL_nxtPg(-1)"]').css('visibility','visible');
					$('[onclick="B2CPL_nxtPg(1)"]').css('visibility','visible');
					if(data.cP==1)
						$('[onclick="B2CPL_nxtPg(-1)"]').css('visibility','hidden');
					if(data.cP>=data.mP)
						$('[onclick="B2CPL_nxtPg(1)"]').css('visibility','hidden');
					$('#B2CPL_crPg').html(data.cP);
					
					$('#B2CPL_ttlCls').html('<?=GetMessage('B2CPL_TABLE_COLS')?> '+((parseInt(data.cP)-1)*data.pC+1)+' - '+Math.min(parseInt(data.ttl),parseInt(data.cP)*data.pC)+' <?=GetMessage('B2CPL_TABLE_FRM')?> '+data.ttl);
					$('#B2CPL_tblPls').html(data.html);
				}
			}
		});
	}
	
	// function B2CPL_killSign(oid){ // ����� � �������� �����
		// if(confirm('<?=GetMessage("B2CPL_JSC_SOD_IFKILL")?>'))
			// $.post(
				// "/bitrix/js/<?=$module_id?>/ajax.php",
				// {'action':'killReqOD','oid':oid},
				// function(data){
					// if(data.indexOf('GD:')===0){
						// alert(data.substr(3));
						// B2CPL_getTable();
						// if(B2CPL_wndKillReq)
							// B2CPL_wndKillReq.Close();
					// }
					// else
						// alert(data);
				// }
			// );
	// }
	
	function B2CPL_delSign(oid){ // �������� �����
		if(confirm('<?=GetMessage("B2CPL_TABLE_IFDELETE")?>'))
			$.post(
				"/bitrix/tools/<?=$module_id?>/ajax.php",
				{'action':'delReqOD','oid':oid},
				function(data){
					alert(data);
					B2CPL_getTable();
				}
			);
	}
	
	// function IPOLSDEL_forseKillSign(oid){ // ���������� ����� ��������
		// if(confirm('<?=GetMessage("B2CPL_JSC_SOD_IFKILL")?>'))
			// $.post(
				// "/bitrix/js/<?=$module_id?>/ajax.php",
				// {'action':'killReqOD','oid':oid},
				// function(data){
					// if(data.indexOf('GD:')===0){
						// alert(data.substr(3));
						// B2CPL_getTable();
						// if(B2CPL_wndKillReq)
							// B2CPL_wndKillReq.Close();
					// }
					// else{
						// $.post(
							// "/bitrix/js/<?=$module_id?>/ajax.php",
							// {'action':'delReqOD','oid':oid},
							// function(data){
								// alert(data);
								// B2CPL_getTable();
								// if(B2CPL_wndKillReq)
									// B2CPL_wndKillReq.Close();
							// }
						// );
					// }
				// }
			// );
	// }

	// function B2CPL_printReq(wat){
		// $.ajax({
			// url  : "/bitrix/js/<?=$module_id?>/ajax.php",
			// type : 'POST',
			// data : {
				// action : 'printOrderInvoice',
				// oId    : wat
			// },
			// dataType : 'json',
			// success  : function(data){
				// if(data.result == 'ok')
					// window.open('/upload/<?=$module_id?>/'+data.file);
				// else
					// alert(data.error);
			// }
		// });
	// }

	B2CPL_wndKillReq=false;
	function B2CPL_callKillReq(){
		if(!B2CPL_wndKillReq){
			B2CPL_wndKillReq = new BX.CDialog({
				title: '<?=GetMessage('B2CPL_OTHR_killReq_TITLE')?>',
				content: "<div><a href='javascript:void(0)' onclick='$(this).next().toggle(); return false;'>?</a><small style='display:none'><?=GetMessage('B2CPL_OTHR_killReq_DESCR')?></small><br><?=GetMessage('B2CPL_OTHR_killReq_LABEL')?> <input size='3' type='text' id='B2CPL_delDeqOrId'><br><?=GetMessage('B2CPL_OTHR_killReq_HINT')?></div>",
				icon: 'head-block',
				resizable: false,
				draggable: true,
				height: '145',
				width: '200',
				buttons: ['<input type="button" value="<?=GetMessage('B2CPL_OTHR_killReq_BUTTON')?>" onclick="IPOLSDEL_forseKillSign($(\'#B2CPL_delDeqOrId\').val())"/>']
			});
		}
		else
			$('#B2CPL_delDeqOrId').val('');
		B2CPL_wndKillReq.Show();
	}
	
	function B2CPL_clrCls()
	{
		$('.adm-list-table-cell-sort-up').removeClass('adm-list-table-cell-sort-up');
		$('.adm-list-table-cell-sort-down').removeClass('adm-list-table-cell-sort-down');
	}

	function B2CPL_sort(wat,handle){
		if(handle.hasClass("adm-list-table-cell-sort-down")){
			B2CPL_clrCls();
			handle.addClass("adm-list-table-cell-sort-up");
			B2CPL_getTable({'by':wat,'sort':'ASC'});
		}else{
			if(handle.hasClass("adm-list-table-cell-sort-up")){
				B2CPL_clrCls();
				B2CPL_getTable();
			}else{
				B2CPL_clrCls();
				handle.addClass("adm-list-table-cell-sort-down");
				B2CPL_getTable({'by':wat,'sort':'DESC'});
			}
		}
	}

	function B2CPL_nxtPg(cntr){
		var page=parseInt($("#B2CPL_crPg").html())+cntr;
		if(page<1)
			page=1;
			
		if(page!=parseInt($("#B2CPL_crPg").html())){
			B2CPL_getTable({"page":page});
			$("#B2CPL_crPg").html(page);
		}
	}

	function B2CPL_shwPrms(handle){
		handle.siblings('a').hide();
		handle.css('height','auto');
		var height=handle.height();
		handle.css('height','0px');
		handle.animate({'height':height},500);
	}

	function B2CPL_setFilter(){
		var params={};
		$('[id^="B2CPL_Fltr_"]').each(function(){
			var crVal=$(this).val();
			if(crVal)
				params['F'+$(this).attr('id').substr(14)]=crVal;
		});
		return params;
	}

	function B2CPL_resFilter(){
		$('[id^="B2CPL_Fltr_"]').each(function(){
			$(this).val('');
		});
	}
	
	function B2CPL_GETSTICKER(wat){
		window.open(wat);
	}

	$(document).ready(function(){
		B2CPL_getTable();
	});
</script>

<div id="pop-statuses" class="b-popup" style="display: none; ">
	<div class="pop-text"><?=GetMessage("B2CPL_HELPER_statuses")?></div>
	<div class="close" onclick="$(this).closest('.b-popup').hide();"></div>
</div>

<tr><td colspan='2'>
		<table id='B2CPL_flrtTbl'>
		  <tbody>
			<tr class='B2CPL_mrPd'>
			  <td><?=GetMessage('B2CPL_TBL_ORDERNUM')?></td><td><input type='text' class='adm-workarea' id='B2CPL_Fltr_>=ORDER_ID'><span class="adm-filter-text-wrap" style='margin: 4px 12px 0px'>...</span><input type='text' class='adm-workarea' id='B2CPL_Fltr_<=ORDER_ID'></td>
			</tr>
			<tr class='B2CPL_mrPd'>
				<td><?=GetMessage('B2CPL_TBL_STATUS')?> <a href='#' class='PropHint' onclick='return ipol_popup_virt("pop-statuses", this);'></a></td>
				<td>
					<select id='B2CPL_Fltr_STATUS'>
						<option value=''      ></option>
						<option value='NEW'   >NEW</option>
						<option value='ERROR' >ERROR</option>
						<option value='OK'    >OK</option>
						<?/*<option value='STORE' >STORE</option>
						<option value='TRANZT'>TRANZT</option>
						<option value='CORIER'>CORIER</option>
						<option value='PVZ'   >PVZ</option>
						<option value='OTKAZ' >OTKAZ</option>
						<option value='DELIVD'>DELIVD</option>*/?>
					</select>
				</td>
			</tr>
			<tr class='B2CPL_mrPd'>
				<td><?=GetMessage('B2CPL_TABLE_UPTIME')?></td>
				<td>
					<div class="adm-input-wrap adm-input-wrap-calendar">
						<input type='text' class='adm-workarea' id='B2CPL_Fltr_>=UPTIME' name='B2CPLupF' disabled>
						<span class="adm-calendar-icon" onclick="BX.calendar({node:this, field:'B2CPLupF', form: '', bTime: true, bHideTime: false});"></span>
					</div>
					<span class="adm-filter-text-wrap" style='margin: 4px 12px 0px'>...</span>
					<div class="adm-input-wrap adm-input-wrap-calendar">
						<input type='text' class='adm-workarea' id='B2CPL_Fltr_<=UPTIME' name='B2CPLupD' disabled>
						<span class="adm-calendar-icon" onclick="BX.calendar({node:this, field:'B2CPLupD', form: '', bTime: true, bHideTime: false});"></span>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan='2'><div class="adm-filter-bottom-separate" style="margin-bottom:0px;"></div></td>
			</tr>
			<tr class='B2CPL_mrPd'>
				<td colspan='2'><input class="adm-btn" type="button" value="<?=GetMessage('MAIN_FIND')?>" onclick="B2CPL_getTable()">&nbsp;&nbsp;&nbsp;<input class="adm-btn" type="button" value="<?=GetMessage('MAIN_RESET')?>" onclick="B2CPL_resFilter()"></td>
			</tr>
		  </tbody>
		</table>
		<br><br>
		<table class="adm-list-table mdTbl">
			<thead>
				<tr class="adm-list-table-header">
					<td class="adm-list-table-cell"><div></div></td>
					<td class="adm-list-table-cell sortTr" style='width:50px;' onclick='B2CPL_sort("ORDER_ID",$(this))'><div class='adm-list-table-cell-inner'>ID</div></td>
					<td class="adm-list-table-cell sortTr" style='width:50px;' onclick='B2CPL_sort("B2CCODE",$(this))'><div class='adm-list-table-cell-inner'><?=GetMessage('B2CPL_TBL_B2CCODE')?></div></td>
					<td class="adm-list-table-cell sortTr" style='width:77px;' onclick='B2CPL_sort("STATUS",$(this))'><div class='adm-list-table-cell-inner'><?=GetMessage('B2CPL_TBL_STATUS')?></div></td>
					<td class="adm-list-table-cell"><div class='adm-list-table-cell-inner'><?=GetMessage('B2CPL_TABLE_PARAM')?></div></td>
					<td class="adm-list-table-cell sortTr" style='width:50px;' onclick='B2CPL_sort("UPTIME",$(this))'><div class='adm-list-table-cell-inner'><?=GetMessage('B2CPL_TABLE_UPTIME')?></div></td>
				</tr>
			</thead>
			<tbody id='B2CPL_tblPls'>
			</tbody>
		</table>
		<div class="adm-navigation">
			<div class="adm-nav-pages-block">
				<span class="adm-nav-page adm-nav-page-prev B2CPL_crsPnt" onclick='B2CPL_nxtPg(-1)'></span>
				<span class="adm-nav-page-active adm-nav-page" id='B2CPL_crPg'>1</span>
				<span class="adm-nav-page adm-nav-page-next B2CPL_crsPnt" onclick='B2CPL_nxtPg(1)'></span>
			</div>
			<div class="adm-nav-pages-total-block" id='B2CPL_ttlCls'><?=GetMessage('B2CPL_TABLE_COLS?')?> 1 - 5 <?=GetMessage('B2CPL_TABLE_FRM')?> 5</div>
			<div class="adm-nav-pages-number-block">
				<span class="adm-nav-pages-number">
					<span class="adm-nav-pages-number-text"><?=GetMessage('admin_lib_sett_rec')?></span>
					<select id='B2CPL_tblPgr' onchange='B2CPL_getTable()'>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="20" selected="selected">20</option>
						<option value="50">50</option>
						<option value="100">100</option>
						<option value="200">200</option>
						<option value="0"><?=GetMessage('MAIN_OPTION_CLEAR_CACHE_ALL')?></option>
					</select>
				</span>
			</div>
		</div>

		<?/*<input type='button' style='margin-top:20px' value='<?=GetMessage('B2CPL_TABLE_BUTTON_OT')?>' onclick='B2CPL_syncOutb()'/>*/?>
	</td></tr>
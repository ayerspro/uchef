<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/tools/'.Cb2cplDelivery::$MODULE_ID.'/jsloader.php');
if(coption::GetOptionString(Cb2cplDelivery::$MODULE_ID,'addJQUI','Y')=='Y'){
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/tools/'.Cb2cplDelivery::$MODULE_ID.'/jquery-ui.min.css');
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/tools/'.Cb2cplDelivery::$MODULE_ID.'/jquery-ui.structure.min.css');
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/tools/'.Cb2cplDelivery::$MODULE_ID.'/jquery-ui.theme.min.css');
}
$exist = COption::GetOptionString(Cb2cplDelivery::$MODULE_ID,'timePickerId',false);
if($exist)
	$profObj = array($exist => array(
		'tag' => false,
		'price' => false,
		'self' => true
	));
else{
	$profId = self::getDeliveryId('kurer','_');
	$profObj = array();
	foreach($profId as $id){
		$profObj[$id] = array(
			'tag' => false,
			'price' => false,
			'self' => false
		);
	}
}
$minDate = max(1,Cb2cplDelivery::$minDate);
?>
<style>
	#B2CPL_timepicker{
		position         : absolute;
		z-index          : 2100;
		display          : none;
		overflow         : hidden;
		font-family      : Arial, sans-serif;
		font-size        : 13px;
		color            : #000;
		background-color : white;
		text-align       : center;
		padding          : 15px;
		margin-top       : 5px;
		border           : 1px solid #d5dadc;
		box-shadow       : 0 2px 10px rgba(0, 0, 0, .15);
	}
	#B2CPL_datepicker{
		padding : 5px 0;
	}
	.B2CPL_floatBl{
		float : left;
		width : 50%;
	}
	.B2CPL_closer{
		width      : 10px;
		height     : 10px;
		position   : absolute;
		right      : 7px;
		top        : 7px;
		background : url("/bitrix/images/<?=Cb2cplDelivery::$MODULE_ID?>/close.png") 0 0 no-repeat;
		cursor     : pointer;
	}
	.B2CPL_closer:hover{
		background-position: 0 -11px;
	}
	#B2CPL_btn{
		border      : 1px solid #DDD;
		background  : #F6F6F6 url("/bitrix/tools/<?=Cb2cplDelivery::$MODULE_ID?>/images/ui-bg_highlight-soft_100_f6f6f6_1x100.png") repeat-x scroll 50% 50%;
		font-weight : bold;
		color       : #0073EA;
		cursor      : pointer;
	}
	#B2CPL_btn:hover{
		border      : 1px solid #0073ea;
		background  : #0073ea url("/bitrix/tools/<?=Cb2cplDelivery::$MODULE_ID?>/images/ui-bg_highlight-soft_25_0073ea_1x100.png") 50% 50% repeat-x;
		font-weight : bold;
		color       : #fff;
	}
	.b2cpl_delivLair div{
		font-weight: normal;
	}
</style>
<script>
	var B2CPL_timePicker = {
		button: '<a href="javascript:void(0);" id="B2CPL_chooseTime" onclick="B2CPL_timePicker.open(); return false;"><?=GetMessage("B2CPL_WIDG_BUTTON")?></a>',// html ������ "������� ����".

		isActive: false,

		chznDate: false,

		chznTime: false,

		dataSelect: false,

		today: '<?=date('d.m.Y',mktime()+86400)?>',

		minDate: '<?=$minDate?>',

		copyright: 'powered by Ipol',

		timePickers: <?=CUtil::PhpToJSObject($profObj)?>,

		newTemplate: false,

		init: function(){
			if(typeof($.datepicker) == 'undefined')
				return;
			// ==== ������������ �� ������������ �����
			if(typeof BX !== 'undefined' && BX.addCustomEvent)
				BX.addCustomEvent('onAjaxSuccess', B2CPL_timePicker.onLoad);

			// ������ JS-����
			if (window.jsAjaxUtil){
				jsAjaxUtil._CloseLocalWaitWindow = jsAjaxUtil.CloseLocalWaitWindow;
				jsAjaxUtil.CloseLocalWaitWindow = function (TID, cont){
					jsAjaxUtil._CloseLocalWaitWindow(TID, cont);
					B2CPL_timePicker.onLoad();
				}
			}
			// == END
			B2CPL_timePicker.onLoad();
		},

		onDatePick: function(date){
			B2CPL_timePicker.chznDate = date;
			B2CPL_timePicker.dataSelect = true;
		},

		onLoad: function(){
			B2CPL_timePicker.removePicker();

			if($('#b2cpl_minDate').length){
				if($('#b2cpl_minDate').val())
					B2CPL_timePicker.minDate = $('#b2cpl_minDate').val();
				else
					B2CPL_timePicker.minDate = 1;
				$("#B2CPL_datepicker").datepicker("option",'minDate',"+"+B2CPL_timePicker.minDate+"d");
			}

			for(var i in B2CPL_timePicker.timePickers){
				if(B2CPL_timePicker.timePickers[i].self) {
					B2CPL_timePicker.timePickers[i].tag = $('#' + i);
				}else{
					var id = (i.indexOf('_') === -1) ? 'ID_' + i : i;
					var parentNd=$('#ID_DELIVERY_'+id);
					if(parentNd.closest('td', '#ORDER_FORM').length>0)
						B2CPL_timePicker.timePickers[i].tag = parentNd.closest('td', '#ORDER_FORM').siblings('td:last');
					else
						if(parentNd.parents(".bx-soa-pp-company").hasClass("bx-selected")){
							B2CPL_timePicker.newTemplate = true;
							B2CPL_timePicker.timePickers[i].tag = parentNd.parents("#bx-soa-delivery").find(".bx-soa-pp-desc-container");
							if(B2CPL_timePicker.timePickers[i].tag.length === 0){
								B2CPL_timePicker.timePickers[i].tag = parentNd.parents("#bx-soa-delivery-hidden").find(".bx-soa-pp-desc-container");
							}
						} else {
							B2CPL_timePicker.timePickers[i].tag = parentNd.siblings('label').find('.bx_result_price');
						}
				}
				if(
					B2CPL_timePicker.timePickers[i].tag.length > 0/* &&
					B2CPL_timePicker.timePickers[i].tag.html().indexOf(B2CPL_timePicker.button)===-1*/
				){
					B2CPL_timePicker.timePickers[i].price = (B2CPL_timePicker.timePickers[i].tag.html()) ? B2CPL_timePicker.timePickers[i].tag.html() : false;
					B2CPL_timePicker.checkAvailibleTimePicker(i);
				}
			}
		},

		checkAvailibleTimePicker: function(i){
			data = {};
			data.main = BX.Sale.OrderAjaxComponent.result;
			data.add = $(document.forms.ORDER_FORM).serializeArray();
			data.checkTimePicker = 'y';
			$.ajax({
				url: "/bitrix/tools/<?= Cb2cplDelivery::$MODULE_ID?>/ajax.php",
				dataType: "json",
				type: "post",
				data: data,
				success: function(res) {
					if(res.deliv){
						B2CPL_timePicker.minDate = res.day;
						B2CPL_timePicker.addOrderInputs();
						B2CPL_timePicker.labelTime(i);

						B2CPL_timePicker.addLocs();

						$("#B2CPL_datepicker").datepicker({
							minDate: "+"+B2CPL_timePicker.minDate+"d",
							maxDate: "+2w",
							onSelect: B2CPL_timePicker.onDatePick
						});
					} else {
						B2CPL_timePicker.removePicker();
					}

				}
			})
		},

		open: function(){
			if(!B2CPL_timePicker.isActive){
				B2CPL_timePicker.isActive = true;
				var hndlr = $('#B2CPL_timepicker');
				var pickBtn = $('#B2CPL_chooseTime');
				hndlr.css({
					'display'   : 'block',
					'left'      : pickBtn.offset().left - hndlr.parent().offset().left
				});
				hndlr.css({
					'top'       : (pickBtn.offset().top - hndlr.parent().offset().top) + pickBtn.height()
				});
			}
		},

		close: function(){//��������� ����������
			$('#B2CPL_timepicker').css('display','none');
			B2CPL_timePicker.isActive = false;
		},

		removePicker: function(){
			$(".b2cpl_delivLair").remove();
			$("[name='B2CPL_date']").remove();
			$("[name='B2CPL_time']").remove();
		},

		addOrderInputs: function(){
			if(!B2CPL_timePicker.dataSelect){
				return;
			}
			var top = B2CPL_timePicker.getTop();
			if(!B2CPL_timePicker.chznDate) B2CPL_timePicker.chznDate = B2CPL_timePicker.today;
			if($('[name="B2CPL_date"]').length < 1)
				top.append('<input type="hidden" name="B2CPL_date">');
			$("[name='B2CPL_date']").val(B2CPL_timePicker.chznDate);
			if($('[name="B2CPL_time"]').length < 1)// ������ ����-���� ���� - ������
				top.append('<input type="hidden" name="B2CPL_time">');
			B2CPL_timePicker.chznTime = $('[name="B2CPL_interval"]').val();
			$("[name='B2CPL_time']").val(B2CPL_timePicker.chznTime);
		},

		choose: function(){
			B2CPL_timePicker.addOrderInputs();
			B2CPL_timePicker.labelTime();
			B2CPL_timePicker.close();
		},

		labelTime: function(i){
			if(arguments.length == 0)
				for(var i in B2CPL_timePicker.timePickers)
					B2CPL_timePicker.labelTime(i);
			else{
				var price = false;
				if(!B2CPL_timePicker.newTemplate){
					price = B2CPL_timePicker.timePickers[i].price;
				}
				var tmpHTML = "<div class='b2cpl_delivLair'>"+(price ? price :"")+"<div>"+B2CPL_timePicker.button;
				if($("[name='B2CPL_date']").val()){
					tmpHTML += "<br><span class='b2cpl_time'><?=GetMessage("B2CPL_WIDG_dateDeliv")?>" + $("[name='B2CPL_date']").val()+"</span><br><span class='b2cpl_time'><?=GetMessage("B2CPL_WIDG_CHOOSETIME")?> " + $("[name='B2CPL_interval'] [value='"+$("[name='B2CPL_interval']").val()+"']").html()+"</span>";
				} else if(B2CPL_timePicker.newTemplate && B2CPL_timePicker.chznDate){
					tmpHTML += "<br><span class='b2cpl_time'><?=GetMessage("B2CPL_WIDG_dateDeliv")?>" + B2CPL_timePicker.chznDate+"</span><br><span class='b2cpl_time'><?=GetMessage("B2CPL_WIDG_CHOOSETIME")?> " + $("[name='B2CPL_interval'] [value='"+B2CPL_timePicker.chznTime+"']").html()+"</span>";
				}
				tmpHTML+="</div></div>";
				if(!B2CPL_timePicker.newTemplate){
					B2CPL_timePicker.timePickers[i].tag.html(tmpHTML);
				} else {
					$(".b2cpl_delivLair").remove();
					B2CPL_timePicker.timePickers[i].tag.append(tmpHTML);
				}
			}
		},

		getTop: function(){
			for(var i in B2CPL_timePicker.timePickers)
				return B2CPL_timePicker.timePickers[i].tag.closest('form');
		},

		addLocs: function(){
			(function(factory){
				if(typeof define === "function" && define.amd)
					define([ "../datepicker" ],factory);
				else
					factory(jQuery.datepicker);
			}(function( datepicker ) {
				datepicker.regional['ru'] = {
					closeText: '<?=GetMessage('B2CPL_WIDG_close')?>',
					prevText: '<?=GetMessage('B2CPL_WIDG_prev')?>',
					nextText: '<?=GetMessage('B2CPL_WIDG_next')?>',
					currentText: '<?=GetMessage('B2CPL_WIDG_today')?>',
					monthNames: [<?=GetMessage('B2CPL_WIDG_monthes')?>],
					monthNamesShort: [<?=GetMessage('B2CPL_WIDG_montheShort')?>],
					dayNames: [<?=GetMessage('B2CPL_WIDG_dayNames')?>],
					dayNamesShort: [<?=GetMessage('B2CPL_WIDG_dayNamesShort')?>],
					dayNamesMin: [<?=GetMessage('B2CPL_WIDG_dayNamesMin')?>],
					weekHeader: '<?=GetMessage('B2CPL_WIDG_weekHeader')?>',
					dateFormat: 'dd.mm.yy',
					firstDay: 1,
					isRTL: false,
					showMonthAfterYear: false,
					yearSuffix: ''
				};
				datepicker.setDefaults(datepicker.regional['ru']);
				return datepicker.regional['ru'];
			}));
		},

		// ��������
		readySt: {
			dr: false,
			jqui: false
		},
		checkReady: function(wat){
			if(typeof(B2CPL_timePicker.readySt[wat]) !== 'undefined')
				B2CPL_timePicker.readySt[wat] = true;
			if(B2CPL_timePicker.readySt.dr && B2CPL_timePicker.readySt.jqui)
				B2CPL_timePicker.init();
		},

		jquiready: function(){B2CPL_timePicker.checkReady('jqui');},
		drready: function(){B2CPL_timePicker.checkReady('dr');},
	}
	<?if(coption::GetOptionString(Cb2cplDelivery::$MODULE_ID,'addJQUI','Y')=='Y'){?>
	IPOL_JSloader.checkScript('$("body").datepicker','/bitrix/tools/<?=Cb2cplDelivery::$MODULE_ID?>/jquery-ui.min.js',B2CPL_timePicker.jquiready);
	<?}else{?>
	if(typeof(IPOL_JSloader.ver) != 'undefined')
		IPOL_JSloader.checkLoadJQ(B2CPL_timePicker.jquiready);
	else
		B2CPL_timePicker.readySt.jqui = true;
	<?}?>
	IPOL_JSloader.bindReady(B2CPL_timePicker.drready);
</script>
<div id='B2CPL_timepicker'>
	<div class='B2CPL_closer' onclick='B2CPL_timePicker.close()'></div>
	<div><?=GetMessage('B2CPL_WIDG_HEADER')?></div>
	<div id="B2CPL_datepicker"></div>
	<div class='B2CPL_floatBl' style='text-align: left;'>
		<?=GetMessage('B2CPL_WIDG_CHOOSETIME')?>
		<br>
		<select name='B2CPL_interval'>
			<option value='full'><?=GetMessage('B2CPL_WIDG_full')?></option>
			<option value='first'><?=GetMessage('B2CPL_WIDG_first')?></option>
			<option value='sec'><?=GetMessage('B2CPL_WIDG_sec')?></option>
		</select>
	</div>
	<div class='B2CPL_floatBl' style='text-align: right;'>
		<br>
		<input id='B2CPL_btn' type='button' value='<?=GetMessage('B2CPL_WIDG_choose')?>' onclick='B2CPL_timePicker.choose()'/>
	</div>
	<div style='clear: both'></div>
</div>

<style>
	.ipol_header {
		font-size: 16px;
		cursor: pointer;
		display:block;
		color:#2E569C;
	}

	.ipol_inst {
		display:none; 
		margin-left:10px;
		margin-top:10px;
	}
	img{border: 1px dotted black;}
	.IPOLIML_optName{
		font-weight: bold;
	}

	.ipol_subheader{
		padding-left: 20px;
		font-weight: bold;
		font-size: 14px;
		margin: 10px;
	}
</style>

<tr class="heading"><td colspan="2" valign="top" align="center"><?=GetMessage('B2CPL_FAQ_HDR_SETUP')?></td></tr>
<tr><td style="color:#555;" colspan="2">
<?=GetMessage('B2CPL_FAQ_TODO_DESCR')?>
</td></tr>

<tr class="heading"><td colspan="2" valign="top" align="center"><?=GetMessage('B2CPL_FAQ_HDR_WORK')?></td></tr>
<tr><td style="color:#555;" colspan="2">

	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_DELIVERYSYSTEM_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_DELIVERYSYSTEM_DESCR')?></div>
	
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_DELIVERY_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_DELIVERY_DESCR')?></div>
	

	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_SENDING_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_SENDING_DESCR')?></div>
		
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_CHANGEDT_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_CHANGEDT_DESCR')?></div>
	
</td></tr>

<tr class="heading"><td colspan="2" valign="top" align="center"><?=GetMessage('B2CPL_FAQ_HDR_ADDFUNC')?></td></tr>
<tr><td style="color:#555;" colspan="2">

	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_LABELS_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_LABELS_DESCR')?></div>

	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_DELPRICE_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_DELPRICE_DESCR')?></div>

	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_DELLOCATIONS_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_DELLOCATIONS_DESCR')?></div>

	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_ERRORS_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_ERRORS_DESCR')?></div>
	
</td></tr>
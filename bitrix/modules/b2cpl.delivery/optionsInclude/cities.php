<style>
	.search-suggest{
		width: 300px;
	}
	.ipol_header {
		font-size: 16px;
		cursor: pointer;
		display:block;
		color:#2E569C;
	}
	.ipol_inst {
		display:none; 
		margin-left:10px;
		margin-top:10px;
	}
</style>
<script>
	function b2cpl_addCitySetup(){
		$("#b2cpl_addCity").attr('disabled','disabled');
		var ind = 1;
		while($('#b2cpl_citysetup_city_'+ind+'_val').length>0)
			ind++;
		$.ajax({
			url:'/bitrix/tools/<?=$module_id?>/ajax.php',
			type:'POST',
			data: 'action=addCitySetup&cName='+ind,
			success: function(data){
				$("#b2cpl_addCity").closest('tr').before(data)
				$("#b2cpl_addCity").removeAttr('disabled');
			}
		});
	}	
	
	function b2cpl_addGMPSetup(){
		$("#b2cpl_addGMP").attr('disabled','disabled');
		var ind = 1;
		while($('[name="b2cpl_mpsetup['+ind+'][mp]"]').length>0)
			ind++;
		$.ajax({
			url:'/bitrix/tools/<?=$module_id?>/ajax.php',
			type:'POST',
			data: 'action=addGMPSetup&mpName='+ind,
			success: function(data){
				$("#b2cpl_addGMP").closest('tr').before(data)
				$("#b2cpl_addGMP").removeAttr('disabled');
			}
		});
	}
</script>

<?// FAQ?>
<tr><td style="color:#555;" colspan="2">
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_MP_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_MP_DESCR')?></div>
</td></tr>

<?// НАСТРОЙКИ МЕСТОПОЛОЖЕНИЙ?>

<tr class="heading">
	<td colspan="2" valign="top" align="center"><?=GetMessage('B2CPL_OPT_GROUPMP')?></td>
</tr>
<tr><th style='text-align:center'><?=GetMessage('B2CPL_LBL_GMP')?></th><th style='text-align:center'><?=GetMessage('B2CPL_LBL_OPTION')?></th></tr>
<?
$savesMPSetups = unserialize(COption::GetOptionString($module_id,"gmpSetups","a:{}"));
if(!empty($savesMPSetups)){
	$ind = 1;
	foreach($savesMPSetups as $mpId => $setup)
		CB2CPLOpts::addGMPSetup(array('mpName'=>$ind++,'mpValue'=>$mpId,'mpSetup'=>$setup));
}else
	CB2CPLOpts::addGMPSetup();
?>
<tr><td style='text-align:center' colspan="2"><input type="button" value="<?=GetMessage('B2CPL_LBL_CITYADD')?>" id='b2cpl_addGMP' onclick='b2cpl_addGMPSetup()'></td></tr>

<?// НАСТРОЙКИ ГОРОДОВ?>

<tr class="heading">
	<td colspan="2" valign="top" align="center"><?=GetMessage('B2CPL_OPT_CITIES')?></td>
</tr>
<tr><th style='text-align:center'><?=GetMessage('B2CPL_LBL_CITY')?></th><th style='text-align:center'><?=GetMessage('B2CPL_LBL_OPTION')?></th></tr>
<?
$savesCitySetups = unserialize(COption::GetOptionString($module_id,"citySetups","a:{}"));
$ind = 1;
if(!empty($savesCitySetups))
	foreach($savesCitySetups as $cityId => $setup)
		CB2CPLOpts::addCitySetup(array('cName'=>$ind++,'cValue'=>$cityId,'cSetup'=>$setup));
CB2CPLOpts::addCitySetup(array('cName'=>$ind));
?>
<tr><td style='text-align:center' colspan="2"><input type="button" value="<?=GetMessage('B2CPL_LBL_CITYADD')?>" id='b2cpl_addCity' onclick='b2cpl_addCitySetup()'></td></tr>
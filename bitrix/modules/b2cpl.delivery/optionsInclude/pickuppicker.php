<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/tools/'.Cb2cplDelivery::$MODULE_ID.'/jsloader.php');
$exist = COption::GetOptionString(Cb2cplDelivery::$MODULE_ID,'pvzPickerId',false);
if(!empty($exist))
	$profObj = array($exist => array(
		'tag' => false,
		'price' => false,
		'self' => true
	));
else{
	$profId = Cb2cplDelivery::getDeliveryId('pickup','_');
	$profObj = array();
	foreach($profId as $id){
		$profObj[$id] = array(
			'tag' => false,
			'price' => false,
			'self' => false
		);
	}
}

$arPVZ = self::getPvz();

$curPVZ = Cb2cplDelivery::$curPvzs;
if(!$curPVZ)
	$curPVZ = array();
?>
<script>
	var B2CPL_PVZPicker = {
		chznPVZ: false,

		curPVZ: <?=CUtil::PhpToJSObject($curPVZ)?>,

		PVZ: <?=CUtil::PhpToJSObject($arPVZ)?>,

		pvzPickers: <?=CUtil::PhpToJSObject($profObj)?>,

		new_template: false,

		init: function(){
			// ==== ������������ �� ������������ �����
			if(typeof BX !== 'undefined' && BX.addCustomEvent)
				BX.addCustomEvent('onAjaxSuccess', B2CPL_PVZPicker.onLoad);
				$(document).on('focusout', '#zipProperty', function(event) {
					B2CPL_PVZPicker.refresh();
				});

			// ������ JS-����
			if (window.jsAjaxUtil){
				jsAjaxUtil._CloseLocalWaitWindow = jsAjaxUtil.CloseLocalWaitWindow;
				jsAjaxUtil.CloseLocalWaitWindow = function (TID, cont){
					jsAjaxUtil._CloseLocalWaitWindow(TID, cont);
					B2CPL_PVZPicker.onLoad();
				}
			}
			// == END
			B2CPL_PVZPicker.onLoad();
		},

		onLoad: function(){
			for(var i in B2CPL_PVZPicker.pvzPickers) {
				if (B2CPL_PVZPicker.pvzPickers[i].self){
					B2CPL_PVZPicker.pvzPickers[i].tag = $('#' + i);
				}else{
					var id = (i.indexOf('_') === -1) ? 'ID_' + i : i;
					selector = $('#ID_DELIVERY_'+id);
					B2CPL_PVZPicker.pvzPickers[i].tag = selector.siblings('label').find('.bx_description');
					if(typeof (B2CPL_PVZPicker.pvzPickers[i].tag.html()) === 'undefined'){
						B2CPL_PVZPicker.new_template = true;
						if(!selector.parents(".bx-soa-pp-company").hasClass("bx-selected")){
							return;
						}
						B2CPL_PVZPicker.pvzPickers[i].tag = selector.parents("#bx-soa-delivery").find(".bx-soa-pp-desc-container");
						if(B2CPL_PVZPicker.pvzPickers[i].tag.length === 0){
							B2CPL_PVZPicker.pvzPickers[i].tag = selector.parents("#bx-soa-delivery-hidden").find(".bx-soa-pp-desc-container");
						}
					}
				}
				if(
					B2CPL_PVZPicker.pvzPickers[i].tag.length > 0 &&
					B2CPL_PVZPicker.pvzPickers[i].tag.find('.B2CPL_pvzLair').length == 0
				) {
					B2CPL_PVZPicker.pvzPickers[i].tag.append('<div class="B2CPL_pvzLair"></div>');
					break;
				}
			}

			if(!B2CPL_PVZPicker.new_template) {
				B2CPL_PVZPicker.chznPVZ = (
					$("#b2cpl_pvz").length
				) ? $("#b2cpl_pvz").val() : false;
				if ($('#b2cpl_curpvzs').length && typeof(
						$('#b2cpl_curpvzs').val()
					) != 'undefined')
					B2CPL_PVZPicker.curPVZ = $.parseJSON($('#b2cpl_curpvzs').val().replace(/'/g, '"'));
			} else {
				B2CPL_PVZPicker.refreshPVZList();
			}

			B2CPL_PVZPicker.loadPVZ();

			B2CPL_PVZPicker.checkRadio();
		},

		refreshPVZList: function() {
			data = {};
			data.main = BX.Sale.OrderAjaxComponent.result;
			data.add = $(document.forms.ORDER_FORM).serializeArray();
			data.getPVZInfo = 'y';
			$.ajax({
				url: "/bitrix/tools/<?= Cb2cplDelivery::$MODULE_ID?>/ajax.php",
				dataType: "json",
				type: "post",
				data: data,
				success: function(res) {
					B2CPL_PVZPicker.PVZ = res.allPVZ;
					B2CPL_PVZPicker.curPVZ = res.curPVZ;
					B2CPL_PVZPicker.loadPVZ();

					B2CPL_PVZPicker.checkRadio();
				}
			})
		},

		loadPVZ: function(){
			var SOLOhtml = '<table>';
			var MULTIhtml = '<table>';
			var definer = 0;
			var needShowAll = false;

			var slted = 0;
			if(typeof(B2CPL_PVZPicker.curPVZ[B2CPL_PVZPicker.chznPVZ]) == 'undefined'){
				for (var i in B2CPL_PVZPicker.curPVZ){
					if(typeof(B2CPL_PVZPicker.PVZ[i]) !== "undefined" && B2CPL_PVZPicker.curPVZ[i]['BASIC']){
						slted = i;
						B2CPL_PVZPicker.chznPVZ = slted;
						break;
					}
				}
			} else {
				slted = B2CPL_PVZPicker.chznPVZ
			}
			// ��������� �� ���������������

			for(var i in B2CPL_PVZPicker.curPVZ){
				if(typeof(B2CPL_PVZPicker.PVZ[i]) == 'undefined' || !B2CPL_PVZPicker.curPVZ[i]['BASIC'])
					continue;
				var PVZtext = "<span class='B2CPL_address'>"+B2CPL_PVZPicker.PVZ[i]['id']+"</span><br><?=GetMessage('B2CPL_address')?>: "+B2CPL_PVZPicker.PVZ[i]['addr']+"<br><?=GetMessage('B2CPL_workTime')?>: "+B2CPL_PVZPicker.PVZ[i]['workTime'];

				SOLOhtml = "<tr><td><input type='hidden' name='B2CPL_pvz' value='"+i+"'>"+PVZtext+"</td></tr>";

				MULTIhtml += "<tr><td><input class='B2CPL_radio' id='B2CPL_pvz_"+i+"' type='radio' name='B2CPL_pvz' value='"+i+"' onclick='B2CPL_PVZPicker.refresh();'";
				if(!slted || slted == i){
					MULTIhtml += " checked";
					slted = "founded";
				}
				MULTIhtml += " #LINK#></td><td><label for='B2CPL_pvz_"+i+"'>"+PVZtext+"</td></tr>";
				definer++;
			}
			for(var i in B2CPL_PVZPicker.curPVZ){
				if(typeof(B2CPL_PVZPicker.PVZ[i]) == 'undefined' || B2CPL_PVZPicker.curPVZ[i]['BASIC'])
					continue;
				var PVZtext = "<span class='B2CPL_address'>"+B2CPL_PVZPicker.PVZ[i]['id']+"</span><br><?=GetMessage('B2CPL_address')?>: "+B2CPL_PVZPicker.PVZ[i]['addr']+"<br><?=GetMessage('B2CPL_workTime')?>: "+B2CPL_PVZPicker.PVZ[i]['workTime'];

				SOLOhtml = "<tr><td><input type='hidden' name='B2CPL_pvz' value='"+i+"'>"+PVZtext+"</td></tr>";

				MULTIhtml += "<tr class='js-hidden-pvz' style='display: none' ><td><input class='B2CPL_radio' id='B2CPL_pvz_"+i+"' type='radio' name='B2CPL_pvz' value='"+i+"' onclick='B2CPL_PVZPicker.refresh();'";
				if(slted == i){
					needShowAll = true;
					MULTIhtml += " checked";
					slted = "founded";
				}
				MULTIhtml += " #LINK#></td><td><label for='B2CPL_pvz_"+i+"'>"+PVZtext+"</td></tr>";
				definer++;
			}
			for(var i in B2CPL_PVZPicker.pvzPickers){
				var fnk = (typeof(submitForm) == 'undefined') ? '' : 'onclick="window.B2CPL_PVZPicker.checkRadio();$(\'#'+i+'\').prop(\'checked\',\'Y\');submitForm();"';
				if(B2CPL_PVZPicker.pvzPickers[i].tag){
					if(definer == 1) {
						B2CPL_PVZPicker.pvzPickers[i].tag.find('.B2CPL_pvzLair').html(SOLOhtml + "</table>");
					}else {
						showAllPVZlink = "<a href='#' class='js-show-link' onclick='B2CPL_PVZPicker.showAllPvz();return false;'><?= GetMessage('B2CPL_pickup_show_all')?></a>";

						B2CPL_PVZPicker.pvzPickers[i].tag.find('.B2CPL_pvzLair').html(MULTIhtml.replace(/#LINK#/g, fnk) + "</table><br>" + showAllPVZlink);
					}
				}
			}

			if(needShowAll){
				B2CPL_PVZPicker.showAllPvz();
			}
		},

		checkRadio: function(){
			$('.B2CPL_radio').each(function(){
				if(typeof($(this).attr('checked')) != 'undefined')
					$(this).siblings('.B2CPL_checkbox').addClass('B2CPL_checkboxSelected');
				else
					$(this).siblings('.B2CPL_checkbox').removeClass('B2CPL_checkboxSelected');
			});
		},

		showAllPvz: function() {
			$(".js-hidden-pvz").show();
			$(".js-show-link").hide();
		},

		refresh: function(){
			if(B2CPL_PVZPicker.new_template){
				B2CPL_PVZPicker.chznPVZ = $('.B2CPL_radio:checked').val();
				BX.Sale.OrderAjaxComponent.sendRequest();
			}
		},

		// ��������
		readySt: {
			jq: false,
			dr: false
		},
		checkReady: function(wat){
			if(typeof(B2CPL_PVZPicker.readySt[wat]) !== 'undefined')
				B2CPL_PVZPicker.readySt[wat] = true;
			if(B2CPL_PVZPicker.readySt.jq && B2CPL_PVZPicker.readySt.dr)
				B2CPL_PVZPicker.init();
		},

		jqready: function(){B2CPL_PVZPicker.checkReady('jq');},
		drready: function(){B2CPL_PVZPicker.checkReady('dr');},
	};

	if(typeof(IPOL_JSloader.ver) != 'undefined')
		IPOL_JSloader.checkLoadJQ(B2CPL_PVZPicker.jqready);
	else
		B2CPL_PVZPicker.readySt.jq = true;
	IPOL_JSloader.bindReady(B2CPL_PVZPicker.drready);
</script>

<style>
	.B2CPL_address{
		font-weight: bold;
		color: #333;
	}
	.B2CPL_checkbox{
		width: 12px;
		height: 12px;
		background-image: url('/bitrix/images/<?=Cb2cplDelivery::$MODULE_ID?>/checkBox.png');
		background-position: 0px 0px;
	}
	.B2CPL_checkboxSelected{
		background-position: 0px -12px;
	}
	.B2CPL_pvzLair{
		margin-top: 15px;
		font-size: 13px;
		color: #333;
	}
	.B2CPL_pvzLair td:first-child{
		vertical-align: top;
	}
	.B2CPL_pvzLair label{
		font-weight: normal;
		margin-bottom: 10px;
		padding-left: 5px;
		color: #a3a3a3;
		font-size: 11px;
	}
</style>

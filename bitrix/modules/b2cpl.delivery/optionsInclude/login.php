<style>
	.ipol_header {
		font-size: 16px;
		cursor: pointer;
		display:block;
		color:#2E569C;
	}

	.ipol_inst {
		display:none; 
		margin-left:10px;
		margin-top:10px;
	}
	img{border: 1px dotted black;}
</style>
<script>
	function B2CPL_auth(){
		$("[onclick='B2CPL_auth()']").attr('disabled','disabled');
		var client    = $('#B2CPL_client').val();
		var key = $('#B2CPL_key').val();
		
		if(!client){
			alert('<?=GetMessage("B2CPL_LABEL_NOClient")?>');
			$("[onclick='B2CPL_auth()']").removeAttr('disabled');
			return;
		}
		if(!key){
			alert('<?=GetMessage("B2CPL_LABEL_NOkey")?>');
			$("[onclick='B2CPL_auth()']").removeAttr('disabled');
			return;
		}
		$.post(
			"/bitrix/tools/<?=$module_id?>/ajax.php",
			{
				'action' : 'auth',
				'client' : client,
				'key'    : key,
			},
			function(data){
				if(data.trim().indexOf('G')===0){
					alert(data.trim().substr(1));
					window.location.reload();
				}
				else{
					alert(data);
					$("[onclick='B2CPL_auth()']").removeAttr('disabled');
					$('.ipol_inst').css('display','block');
					$('#ipol_mistakes').css('display','block');
				}
			}
		);
	}
	function B2CPL_doSbmt(e){
		if(e.keyCode==13)
			B2CPL_auth();
	}
	
	$(document).ready(function(){
		$('#B2CPL_login').on('keyup',B2CPL_doSbmt);
		$('#B2CPL_pass').on('keyup',B2CPL_doSbmt);
	});
</script>
<tr><td><?=GetMessage('B2CPL_OTP_client')?></td><td><input type='text' id='B2CPL_client'></td></tr>
<tr><td><?=GetMessage('B2CPL_OTP_key')?></td><td><input type='password' id='B2CPL_key'></td></tr>
<tr><td></td><td><input type='button' value='<?=GetMessage('B2CPL_LABEL_auth')?>' onclick='B2CPL_auth()'></td></tr>

<tr><td style="color:#555;" colspan="2">
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_API_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_API_DESCR')?></div>
</td></tr>
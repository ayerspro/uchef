<script>
	var B2Cpl_oldDelivs = {
		start: function(){
			$('#B2Cpl_OD_startScan').attr('disabled','disabled');
			$.ajax({
				url: '/bitrix/tools/<?=$module_id?>/ajax.php',
				data: {action:'scanAndDelete'},
				type: 'POST',
				dataType: 'json',
				success: function(data){
					$('#B2Cpl_OD_RESULT').css('display','block');
					var goodTbl = '';
					var badTbl  = '';
					for(var i in data)
						if(data[i].SUCCESS)
							goodTbl += '<tr><td>['+data[i].ID+']</td><td>'+data[i].NAME+'</td><td>'+data[i].CODE+'</td></tr>';
						else
							badTbl += '<tr><td>['+data[i].ID+']</td><td>'+data[i].NAME+'</td><td>'+data[i].CODE+'</td><td>'+data[i].ERROR+'</td></tr>';
					if(goodTbl)
						goodTbl = '<br><br><strong><?=GetMessage("B2CPL_LBL_DELETED")?></strong><br><table><tr><td>ID</td><td><?=GetMessage('B2CPL_LBL_LABEL')?></td><td><?=GetMessage('B2CPL_LBL_CODE')?></td></tr>'+goodTbl+'</table>';
					if(badTbl)
						badTbl = '<br><br><strong><?=GetMessage("B2CPL_LBL_PROBLEMS")?></strong><br><table id="B2Cpl_badDelivs"><tr><td>ID</td><td><?=GetMessage('B2CPL_LBL_LABEL')?></td><td><?=GetMessage('B2CPL_LBL_CODE')?></td><td><?=GetMessage('B2CPL_LBL_ERRORS')?></td></tr>'+badTbl+'</table>'
					$('#B2Cpl_OD_RESULT').append(goodTbl+badTbl);
				}
			});
		}
	};
</script>
<style>
#B2Cpl_OD_RESULT table {
  text-align: center
}
</style>

<?// FAQ?>
<tr><td style="color:#555;" colspan="2">
	<a class="ipol_header" onclick="$(this).next().toggle(); return false;"><?=GetMessage('B2CPL_FAQ_OLDDELIVS_TITLE')?></a>
	<div class="ipol_inst"><?=GetMessage('B2CPL_FAQ_OLDDELIVS_DESCR')?></div><br>
</td></tr>

<tr><td colspan="2"><input id='B2Cpl_OD_startScan' type='button' value='<?=GetMessage('B2CPL_LBL_STARTSCAN')?>' onclick='B2Cpl_oldDelivs.start()'></td></tr>

<tr><td colspan="2">
	<div id='B2Cpl_OD_RESULT' style='display:none'>
		<?=GetMessage('B2CPL_LBL_RESULTOFCLEAR')?>
	</div>
</td></tr>
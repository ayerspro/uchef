<?php

class Cb2cplOrder {

	// OLD
	public static function AddOrder($ID, $arFields) {
		if ($ID <= 0) {
			return false;
		}
		$ID = intval($ID);
		$arFields['ORDER_ID'] = $ID;
		foreach ($arFields as $k => $v) {
			$arFields[$k] = '"' . trim($GLOBALS['DB']->ForSQL($v)) . '"';
		}
		if (!$GLOBALS['DB']->Query('SELECT * FROM b_b2cpl_orders WHERE ORDER_ID=' . $ID)->Fetch()) {
			$GLOBALS['DB']->Insert('b_b2cpl_orders', $arFields, 'Error in Cb2cplDelivery on line ' . __LINE__);
		} else {
			$GLOBALS['DB']->Update('b_b2cpl_orders', $arFields, 'WHERE ORDER_ID=' . $ID, 'Error in Cb2cplDelivery on line ' . __LINE__);
		}
		return true;
	}
	
	public static function GetOrder($ID, $joinOrder=false) {
		if ($joinOrder) {
			return $GLOBALS['DB']->Query('SELECT * FROM b_b2cpl_orders B
											LEFT JOIN b_sale_order O ON (B.ORDER_ID=O.ID)
											WHERE ORDER_ID=' . intval($ID))->GetNext();
		} else {
			return $GLOBALS['DB']->Query('SELECT * FROM b_b2cpl_orders WHERE ORDER_ID=' . intval($ID))->GetNext();
		}
	}
	// ACTUAL 

	public function toLog($wat,$sign){Cb2cplHelper::toLog($wat,$sign);}
	private static $tableName = "b_b2cpl_orders";

	public static function Add($Data){
        // = $Data = format:
		// PARAMS - ALL INFO
		// ORDER_ID - corresponding order
		// STATUS - response from b2cpl
		// STICKERS_URL - url to stickers
		// B2CCODE - recieved from b2cpl
		// UPTIME - ����� ����������
		
		global $DB;
        
		if(!$Data['STATUS'])
			$Data['STATUS']='NEW';

		if(is_array($Data['PARAMS'])) {
			$Data['PARAMS'] = serialize($Data['PARAMS']);
		}
		
		$Data['UPTIME']=mktime();
			
		$rec = self::CheckRecord($Data['ORDER_ID']);
		if($rec){
			$strUpdate = $DB->PrepareUpdate(self::$tableName, $Data);
			$strSql = "UPDATE ".self::$tableName." SET ".$strUpdate." WHERE ORDER_ID=".$rec['ORDER_ID'];
			$DB->Query($strSql, false, $err_mess.__LINE__);
		}else{
			$arInsert = $DB->PrepareInsert(self::$tableName, $Data);
			$strSql =
				"INSERT INTO ".self::$tableName."(".$arInsert[0].") ".
				"VALUES(".$arInsert[1].")";
			$DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		}
		return self::CheckRecord($Data['ORDER_ID']); 
    }

	public static function select($arOrder=array("ORDER_ID","DESC"),$arFilter=array(),$arNavStartParams=array()){
		global $DB;

		$strSql='';

		$where='';
		if(strpos($arFilter['>=UPTIME'],".")!==false)
			$arFilter['>=UPTIME']=strtotime($arFilter['>=UPTIME']);
		if(strpos($arFilter['<=UPTIME'],".")!==false)
			$arFilter['<=UPTIME']=strtotime($arFilter['<=UPTIME']);

	 	if(count($arFilter)>0)
			foreach($arFilter as $field => $value){
				if(strpos($field,'!')!==false)
					$where.=' and '.substr($field,1).' != "'.$value.'"';
				elseif(strpos($field,'<=')!==false)
					$where.=' and '.substr($field,2).' <= "'.$value.'"';				
				elseif(strpos($field,'>=')!==false)
					$where.=' and '.substr($field,2).' >= "'.$value.'"';
				elseif(strpos($field,'>')!==false)
					$where.=' and '.substr($field,1).' > "'.$value.'"';				
				elseif(strpos($field,'<')!==false)
					$where.=' and '.substr($field,1).' < "'.$value.'"';
				else
				{
					if(is_array($value))
					{
						$where.=' and (';
						foreach($value as $val)
							$where.=$field.' = "'.$val.'" or ';
						$where=substr($where,0,strlen($where)-4).")";
					}
					else
						$where.=' and '.$field.' = "'.$value.'"';
				}
			}
		if($where) 
			$strSql.="
			WHERE ".substr($where,4);
			
		if(in_array($arOrder[0],array('ID','ORDER_ID','STATUS','UPTIME'))&&($arOrder[1]=='ASC'||$arOrder[1]=='DESC'))
			$strSql.="
			ORDER BY ".$arOrder[0]." ".$arOrder[1];
		
		$cnt=$DB->Query("SELECT COUNT(*) as C FROM ".self::$tableName." ".$strSql, false, $err_mess.__LINE__)->Fetch();
		
		if($arNavStartParams['nPageSize']==0)
			$arNavStartParams['nPageSize']=$cnt['C'];
		
		$strSql="SELECT * FROM ".self::$tableName." ".$strSql;

		$res = new CDBResult();
		$res->NavQuery($strSql,$cnt['C'],$arNavStartParams);

		return $res;
	}
	
	public static function GetByOI($orderId)
	{
		global $DB;
		$orderId=$DB->ForSql($orderId);
		$strSql =
            "SELECT PARAMS, STATUS, B2CCODE, STICKERS_URL ".
            "FROM ".self::$tableName." ".
			"WHERE ORDER_ID = '".$orderId."'";
		$res = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		$arReturn=array();
		if($arr = $res->Fetch())
			return $arr;
		else return false;
	}

	public static function Delete($orderId){
		global $DB;
		$orderId = $DB->ForSql($orderId);
		$strSql =
            "DELETE FROM ".self::$tableName." 
            WHERE ORDER_ID='".$orderId."'";
		$DB->Query($strSql, true);
        return true; 
    }

	public static function CheckRecord($orderId){
		global $DB;

		$orderId = $DB->ForSql($orderId);
        $strSql =
            "SELECT ORDER_ID, STATUS ".
            "FROM ".self::$tableName." ".
			"WHERE ORDER_ID = '".$orderId."'";

		$res = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		if($res && $arr = $res->Fetch())
			return $arr;
		return false;
	}

	public static function updateStatus($arParams){
		global $DB;
		foreach($arParams as $key => $val)
			$arParams[$key] = $DB->ForSql($val);

		$setStr = "STATUS ='".$arParams["STATUS"]."', UPTIME= '".mktime()."'";

		$strSql =
            "UPDATE ".self::$tableName." 
			SET ".$setStr."
			WHERE ORDER_ID = '".$arParams["ORDER_ID"]."'";
		if($DB->Query($strSql, true))
			return true;
		else 
			return false;
	}
}
<?

class Cb2cplLogs {

	protected static $tableName = "b_b2cpl_logs";

	/**
	 * ������ � ��� ���������� � ������
	 *
	 * @param int    $orderID
	 * @param string $message
	 * @param string $type
	 *
	 * @return bool
	 */
	public static function writeLog($orderID, $message, $type = "info") {
		if ( (int) $orderID <= 0 || !is_string($message) || empty($message) ) {
			return false;
		}
		global $DB;
		$arTypes = array("info", "error", "success");
		if ( !in_array($type, $arTypes) ) {
			$type = "info";
		}

		$arData = array(
			"ORDER_ID" => $orderID,
			"TYPE"     => strtoupper($type),
			"MESSAGE"  => $message,
		);
		$arInsert = $DB->PrepareInsert(self::$tableName, $arData);
		$sqlQuery = "INSERT INTO " . self::$tableName . "(" . $arInsert[0] . ") " .
			"VALUES(" . $arInsert[1] . ")";
		$res = $DB->Query($sqlQuery, true);

		if ( is_object($res) ) {
			$res = true;
		}

		return $res;
	}

	public static function getLog($orderID, $only_last_record = false) {
		if ( (int) $orderID <= 0 ) {
			return false;
		}
		global $DB;

		$sqlQuery = "SELECT * FROM ".self::$tableName." WHERE `ORDER_ID`={$orderID} ORDER BY `TIME` DESC";

		if($only_last_record){
			$sqlQuery .= " LIMIT 1";
		}

		$res = $DB->Query($sqlQuery);

		$arResult = array();
		while ($arRes = $res->Fetch()){
			$arResult[] = $arRes;
		}

		return $arResult;
	}

	public static function getLastStatus($orderID){
		if((int)$orderID<=0){
			return false;
		}

		$arStatus = self::getLog($orderID, true);
		if(count($arStatus)){
			return $arStatus[0]["TYPE"];
		} else {
			return false;
		}
	}

}

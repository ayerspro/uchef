<?
IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("sale");
use Bitrix\Sale;

class Cb2cplDelivery extends Cb2cplHelper{

	public static $city = false;
	public static $minDate = false;

	/*
			��������
		== Init ==  == GetConfig == == GetSettings ==  == SetSettings ==  == Calculate ==  == Compability ==  == GetProfiles ==  == CheckZip ==  == GetVariants ==
	*/


	public function Init() {
		if ($arCurrency = CCurrency::GetByID('RUR'))
			$base_currency = 'RUR';
		else
			$base_currency = 'RUB';

		return array(
			'SID' => 'b2cpl',
			'NAME' => GetMessage('B2CPL_DELIVERY_DOSTAVKA'),
			'DESCRIPTION' => '',
			'DESCRIPTION_INNER' => '', // description in admin.panel
			'BASE_CURRENCY' => $base_currency,
			'HANDLER' => __FILE__,
			'DBGETSETTINGS' => array('Cb2cplDelivery', 'GetSettings'),
			'DBSETSETTINGS' => array('Cb2cplDelivery', 'SetSettings'),
			// 'GETCONFIG' => array('Cb2cplDelivery', 'GetConfig'),
			'COMPABILITY' => array('Cb2cplDelivery', 'Compability'),
			'CALCULATOR' => array('Cb2cplDelivery', 'Calculate'),
			'PROFILES' => self::GetProfiles()
		);
	}

	public static function GetPvrForComponent($data, $forPicker = false){
		$arNeedKeys = array(
			"PRICE",
			"LOCATION_ZIP",
			"LOCATION_TO"
		);
		foreach ($arNeedKeys as $key){
			if(!array_key_exists($key, $data)){
				return;
			}
		}

		self::Compability($data, array());

		if(!$forPicker){
			$arPVZ = self::getPvz();
			$curPVZ = Cb2cplDelivery::$curPvzs;
			if(!$curPVZ)
				$curPVZ = array();
			return array(
				'allPVZ' => $arPVZ,
				'curPVZ' => $curPVZ
			);
		} else {
			if(self::$arVariable['kurer'][GetMessage('B2CPL_WIDG_availible')] === GetMessage('B2CPL_WIDG_yes')){
				$today = MakeTimeStamp(ConvertTimeStamp());
				$date = explode(" ", self::$arVariable['kurer'][GetMessage('B2CPL_WIDG_first_date')]);
				if($date){
					$B2CPL_date = MakeTimeStamp($date[0]);
					$nextDate = ($B2CPL_date - $today) / 86400;
					return array(
						'deliv' => true,
						'day' => $nextDate
					);
				} else {
					return array(
						'deliv' => false
					);
				}

			} else {
				return array(
					'deliv' => false
				);
			}
		}
	}

	public function GetSettings($strSettings) {
		return unserialize($strSettings);
	}

	public function SetSettings($arSettings) {
		return serialize($arSettings);
	}

	static $arDepthes;

	public function Compability($arOrder, $arConfig) {
		$cityTo = CSaleLocation::GetByID($arOrder['LOCATION_TO']);
		self::$city = ($cityTo) ? $cityTo['CITY_NAME_LANG'] : false;
		$checkPrice = false;
		self::$curPvzs = false;

		$paySys = false;
		if(strpos($_SERVER['REQUEST_URI'],'/bitrix/admin/sale_order_new.php') !== false){
			$order = CSaleOrder::GetById($_REQUEST['ID']);
			$checkPrice = ($order['PAYED'] == 'Y') ? 'bnal' : false;
			$paySys = $order['PAY_SYSTEM_ID'];
		}

		if(!$checkPrice)
			$checkPrice = self::checkPS($paySys);
		$arVariants = array();
		foreach($checkPrice as $mode){
			$_arVariants = self::GetVariants($arOrder,$mode);
			self::pvzVariants($_arVariants); // ���
			self::$arDepthes[$mode] = array_keys($_arVariants);
			$arVariants = array_merge($arVariants,$_arVariants);
		}

		self::$arVariable = $arVariants;

		return array_keys($arVariants);
	}

	public function Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false) {
		$mode = 'nal';
		if(self::$arDepthes && array_key_exists('bnal',self::$arDepthes) && in_array($profile,self::$arDepthes['bnal']))
			$mode = 'bnal';
		$arVariants = self::GetVariants($arOrder,$mode);
		$arReturn = array('RESULT' => 'ERROR');
		$chosen = $profile;
		if($profile == 'pickup'){
			if(!$_REQUEST['B2CPL_pvz'] && !$_REQUEST["order"]["B2CPL_pvz"]){ // ���� �� ������ ��� - "��������" ������ ����������
				foreach($arVariants as $key => $val)
					if(strpos($key,'pvz_') === 0){
						$chosen = $key;
						break;
					}
			}else {
				if(isset($_REQUEST["order"])){
					$chosen = $_REQUEST["order"]["B2CPL_pvz"];
					if(empty($arVariants[$chosen])){
						foreach($arVariants as $key => $val) {
							if ( strpos($key, 'pvz_') === 0 ) {
								$chosen = $key;
								break;
							}
						}
					}
				} else {
					$chosen = $_REQUEST['B2CPL_pvz'];
				}
			}
		}
		if(!empty($arVariants[$chosen]) && !empty($arVariants[$chosen][GetMessage('B2CPL_DELIVERY_STOIMOSTQ')])){
			$arReturn = array(
				'RESULT' => 'OK',
				'VALUE' => $arVariants[$chosen][GetMessage('B2CPL_DELIVERY_STOIMOSTQ')],
			);
			if(COption::getOptionString(self::$MODULE_ID,'termsOn') == 'Y' ){
				self::$minDate = $arVariants[$chosen]['TERM'] + intval(COption::getOptionString(self::$MODULE_ID,'termsInc'));
				if(strpos($chosen,'pr') === false)
					$arReturn['TRANSIT'] = self::$minDate;
			}
		}
		foreach(GetModuleEvents(self::$MODULE_ID, "onCalculate", true) as $arEvent)
			ExecuteModuleEventEx($arEvent,Array(&$arReturn,$profile,$arConfig,$arOrder));

		return $arReturn;
	}

	static $curPvzs = false;

	static $arVariable;

	public function GetProfiles($location_id = ''){
		$arProfiles = array(
			CUtil::translit(GetMessage('B2CPL_DELIVERY_KURQER'), 'ru') => array(
				'TITLE' => GetMessage('B2CPL_DELIVERY_KURQERSKAA_DOSTAVKA'),
				'DESCRIPTION' => ' ',
				'RESTRICTIONS_WEIGHT' => array(0),
				'RESTRICTIONS_SUM' => array(0),
			)
		);
		$arPVZ = self::getPVZ();
		if(count($arPVZ))
			$arProfiles['pickup'] = array(
				'TITLE' => GetMessage('B2CPL_DELIVERY_PVZ'),
				'DESCRIPTION' => GetMessage('B2CPL_pickup_descr'),
				'RESTRICTIONS_WEIGHT' => array(0),
				'RESTRICTIONS_SUM' => array(0),
			);

		for ($i=1; $i<=5; $i++) {
			$arProfiles[CUtil::translit(GetMessage('B2CPL_DELIVERY_PR'), 'ru').$i] = array(
				'TITLE' => GetMessage('B2CPL_DELIVERY_PR'.$i),
				'DESCRIPTION' => ' ',
				'RESTRICTIONS_WEIGHT' => array(0),
				'RESTRICTIONS_SUM' => array(0),
			);
		}
		if(COption::GetOptionString("main","~sale_converted_15",'N') != 'Y')
			self::bitrixDontShowOff($arProfiles);
		return $arProfiles;
	}

	public function CheckZip(&$arOrder) {
		if ( empty($arOrder['LOCATION_ZIP']) ) {
			if ( !empty($arOrder['LOCATION_TO']) ) {
				$rsZip = CSaleLocation::GetLocationZIP($arOrder['LOCATION_TO']);
				if ( $arZip = $rsZip->Fetch() ) {
					$arOrder['LOCATION_ZIP'] = $arZip['ZIP'];
					return;
				}

				if ( $arZip = $rsZip->fetch() ) {
					$arOrder['LOCATION_ZIP'] = $arZip['ZIP'];
					return;
				}
			}
			if ( !empty($arOrder['LOCATION_TO']) ) {
				if ( $arCity = CSaleLocation::GetByID($arOrder['LOCATION_TO']) ) {
					$cache = new CPHPCache();
					$cache_time = 3600;

					$md5 = md5(serialize($arCity));
					$cache_id = "LOCATION_ID|{$arOrder["LOCATION_TO"]}|$md5";
					if ( (!defined('B2CPL_NO_CACHE') || !B2CPL_NO_CACHE) && $cache->InitCache($cache_time, $cache_id, '/' . SITE_ID . self::$cachePath) ) {
						$data = $cache->GetVars();
					} else {
						$arQuery = array(
							'func'   => 'zipcity',
							'client' => COption::GetOptionString(self::$MODULE_ID, 'client', false),
							'key'    => COption::GetOptionString(self::$MODULE_ID, 'key', false),
							'city'   => urlencode($GLOBALS['APPLICATION']->ConvertCharset($arCity['CITY_NAME'], SITE_CHARSET, 'windows-1251'))
						);
						$data = self::Query($arQuery);
						$data = $GLOBALS['APPLICATION']->ConvertCharset($data, 'windows-1251', SITE_CHARSET);
						$data = CUtil::JsObjectToPhp($data);

						if ( is_array($data) ) {
							$cache->StartDataCache($cache_time, $cache_id);
							$cache->EndDataCache($data);
						}
					}

					$arOrder['DELIVERY_RESULT'] = $data;
					if ( $data['flag_error'] == 0 || $data['flag_error'] == 5) {
						$arOrder['LOCATION_ZIP'] = $data['zip'];
					}
					return;
				}
			}
		}
	}

	function checkPS($CPS=false){ // �������� ��������� ������: ���� ������ - ������ ������ ��������
		if(!$CPS)
			$CPS = (array_key_exists('PAY_SYSTEM_ID',$_REQUEST)) ? $_REQUEST['PAY_SYSTEM_ID'] : false;

		$bNalPSyS = unserialize(COption::GetOptionString('b2cpl.delivery','paySystems','a:{}'));

		if($CPS)
			$arRet[] = (in_array($CPS,$bNalPSyS)) ? 'bnal' : 'nal';
		else
			$arRet = array('nal','bnal');

		return $arRet;
	}

	public function GetVariants($arOrder,$mode='nal') {
		self::CheckZip($arOrder);

		if (empty($arOrder['LOCATION_ZIP'])) {
			if ($arOrder['DELIVERY_RESULT']['flag_error'] != 0) {
				//show error
			}
			return array();
		}

		$arConfig = array(
			'property_v_code' => COption::GetOptionString(self::$MODULE_ID,'property_v_code',false),
			'weight'          => COption::GetOptionString(self::$MODULE_ID,'weight',false),
			'post'			  => COption::GetOptionString(self::$MODULE_ID,'post','Y'),
			'around'		  => COption::GetOptionString(self::$MODULE_ID,'around','N'),
		);
		if($mode == 'bnal')
			$arConfig['price_assess'] = intval($arOrder['PRICE']);

		/**
		 * Get cart's products.
		 */
		$arCart = array();
		if (!defined('ADMIN_SECTION') || !ADMIN_SECTION){
			$rsList = CSaleBasket::GetList(false,array('ORDER_ID'=>'NULL','LID'=>SITE_ID,'FUSER_ID'=>CSaleBasket::GetBasketUserID(),'DELAY'=>'N','CAN_BUY'=>'Y'));
			while ($arItem = $rsList->Fetch())
				$arCart[] = $arItem;
		}elseif(isset($arOrder['ITEMS']))
			$arCart = $arOrder['ITEMS'];
		elseif(isset($arOrder['ORDER_ID'])){ // ��� orderDetail: ��� �������� WEIGHT
			$rsList = CSaleBasket::GetList(false,array('ORDER_ID'=>$arOrder['ORDER_ID'],'DELAY'=>'N','CAN_BUY'=>'Y'));
			while ($arItem = $rsList->Fetch())
				$arCart[] = $arItem;
		}
		/**
		 * Calculate the total volume of products
		 */
		$v_sum = 0;
		if(!empty($arConfig['property_v_code'])){
			$arGoodsV = array();
			$arGoodsCnt = array();
			foreach($arCart as $arItem)
				$arGoodsCnt[$arItem['PRODUCT_ID']] = $arItem['QUANTITY'];
			$arGoodsV = self::getGoodsProp($arGoodsCnt,$arConfig['property_v_code']);
			foreach($arGoodsV as $goodId => $volume)
				$v_sum += $volume * $arGoodsCnt[$goodId];
		}

		/**
		 * Recalc weight of order.
		 */
		if (!empty($arCart))
			$arOrder['WEIGHT'] = 0;
		if(!empty($arConfig['weight'])){
			$arGoods = array();
			foreach($arCart as $arItem)
				$arGoods[$arItem['PRODUCT_ID']] = true;
			$arGoods = self::getGoodsPropWeight($arGoods,$arConfig['weight']);
		}
		$defWeight = COption::GetOptionString(self::$MODULE_ID,'default_weight',1);
		$defWeight = ($defWeight) ? $defWeight : 1;
		foreach ($arCart as $arItem) {
			if($arConfig['weight'] && array_key_exists($arItem['PRODUCT_ID'],$arGoods))
				$arItem['WEIGHT'] = $arGoods[$arItem['PRODUCT_ID']];
			if ($arItem['WEIGHT'] <= 0)
				$arItem['WEIGHT'] = $defWeight;
			$arOrder['WEIGHT'] += $arItem['WEIGHT']*$arItem['QUANTITY'];
		}
		/**
		 * Request
		 */
		$cache = new CPHPCache;
		$cache_time = 3600;

		if(strpos($_SERVER['REQUEST_URI'],'/bitrix/admin/sale_order_new.php') !== false && $_REQUEST['ID']){
			$orderSelf = CSaleOrder::GetById($_REQUEST['ID']);
			if($orderSelf['PAYED'] == 'Y')
				$arOrder['PRICE'] = 0;
		}

		$cache_id = intval($arOrder['LOCATION_ZIP']).'|'.round($arOrder['WEIGHT']).'|'.intval($v_sum).'|'.serialize($arConfig).'|'.$mode;
		if ((!defined('B2CPL_NO_CACHE') || !B2CPL_NO_CACHE) && $cache->InitCache($cache_time, $cache_id, '/' . SITE_ID . self::$cachePath)) {
			$arVars = $cache->GetVars();
			$data = $arVars['DATA'];
			unset($arVars);
		}else{
			$cache->StartDataCache($cache_time, $cache_id);
			$arQuery = array(
				'client'  => COption::GetOptionString(self::$MODULE_ID,'client',false),
				'key'     => COption::GetOptionString(self::$MODULE_ID,'key',false),
				'func'    => 'tarif',
				'type'    => ($arConfig['post'] == 'Y' ? '+' : '-') . 'post,' . ($arConfig['around'] == 'Y' ? '+' : '-') . 'around',
				'weight'  => round($arOrder['WEIGHT']),
				'zip'     => $arOrder['LOCATION_ZIP'],
				'v'       => $v_sum,
				'price'   => ($mode == 'bnal') ? 0 : $arOrder['PRICE'],
				//'all_pvz' => (COption::GetOptionString(self::$MODULE_ID,'shortPVZ','Y') == 'Y') ? 0 : 1,
			);
			$regionFrom = COption::GetOptionString(self::$MODULE_ID,'senderRegion',false);
			if($regionFrom)
				$arQuery['region'] = $regionFrom;

			if($mode == 'bnal')
				$arQuery['price_assess'] = $arConfig['price_assess'];
			$data = self::Query($arQuery);
			$data = $GLOBALS['APPLICATION']->ConvertCharset($data, 'windows-1251', SITE_CHARSET);
			$data = CUtil::JsObjectToPhp($data);
			$term = $data['transport_days'];
			if (is_array($data) && is_array($data['delivery_ways'])) {
				$tmp = $data['delivery_ways'];
				$data['delivery_ways'] = array();
				foreach ($tmp as $k => $arDel) {
						$key = CUtil::translit($arDel[GetMessage('B2CPL_DELIVERY_CODE')], 'ru');
						$data['delivery_ways'][$key] = $arDel;
						$data['delivery_ways'][$key]['TERM'] = $term;
				}
			}
			if(!empty($data))
				$cache->EndDataCache(array('DATA' => $data));
		}
		if($data['flag_delivery'] == true) {
			self::checkOptVariants($data['delivery_ways'],$arOrder['LOCATION_TO']);
			self::modifyPrice($data['delivery_ways'],$arOrder['PRICE']);
			return $data['delivery_ways'];
		}
	}

	function pvzVariants(&$arVariants){ // �������� ������ �� ����������� ���, ������� ������ ��� compability
		foreach($arVariants as $varCode => $arVars)
			if(strpos($varCode,'pvz_')!==false){
				$arVariants['pickup'] = true;
				self::$curPvzs[$varCode] = array(
					'BASIC' => $arVars[GetMessage('B2CPL_pickup_basic_pvz')] == 1,
					'TERM'  => $arVars['TERM'],
					'PRICE' => $arVars[GetMessage('B2CPL_DELIVERY_STOIMOSTQ')]
				);
				unset($arVariants[$varCode]);
			}
	}

	function checkDepth(&$arResult, &$arUserResult){
		// ���� ������� - ��������� ������ �� - ��������, ��� ��� ������ ����/������� ���� �� ���� �����, ������ ��� � ��������.
		if(
			!self::isActive() ||
			COption::GetOptionString(self::$MODULE_ID,'paySystemsWatch','Y') == 'N' ||
			!is_array(self::$arDepthes)
		)
			return;

		$curMode = self::checkPS($arUserResult['PAY_SYSTEM_ID']);
		if(count($curMode) == 1){
			$checker = array_pop($curMode);
			if(self::isConverted()){
				foreach($arResult['DELIVERY'] as $id => $noth){
					$curProfile = self::defineDelivery($id);
					if($curProfile && !in_array($curProfile,self::$arDepthes[$checker])){
						unset($arResult['DELIVERY'][$id]);
						if($arUserResult['DELIVERY_ID'] == $id)
							$arUserResult['DELIVERY_ID'] = false;
					}
				}
			}elseif(
				array_key_exists('b2cpl',$arResult['DELIVERY']) &&
				count($arResult['DELIVERY']['b2cpl']['PROFILES'])
			){
				foreach($arResult['DELIVERY']['b2cpl']['PROFILES'] as $profile => $vals)
					if(!in_array($profile,self::$arDepthes[$checker])){
						unset($arResult['DELIVERY']['b2cpl']['PROFILES'][$profile]);
						if($arUserResult['DELIVERY_ID'] == 'b2cpl:'.$profile)
							$arUserResult['DELIVERY_ID'] = false;
					}
			}
		}elseif($_REQUEST['is_ajax_post'] != 'Y' && $_REQUEST["AJAX_CALL"] != 'Y' && !$_REQUEST["ORDER_AJAX"]) // some holy crap occurs
			echo "<script>setTimeout(function(){if(typeof(submitForm) == 'function'){submitForm();}},500)</script>";
	}

	/*
		����������� ��������
		== checkOptVariants ==  == modifyPrice ==
	*/

	function checkOptVariants(&$variants,$location=false){
		if(!$location) return $variants;

		$setup = false;
		$svdOpts = unserialize(COption::GetOptionString(self::$MODULE_ID,'citySetups','a:{}'));
		if(array_key_exists($location,$svdOpts))
			$setup = $svdOpts[$location];
		else{
			$svdGrps = unserialize(COption::GetOptionString(self::$MODULE_ID,'gmpSetups','a:{}'));
			if(count($svdGrps)){
				$allGroups = CSaleLocationGroup::GetLocationList(array(),array('LOCATION_ID'=>$location));
				while($group=$allGroups->Fetch()){
					if($group['LOCATION_ID'] == $location && array_key_exists($group['LOCATION_GROUP_ID'],$svdGrps)){
						$setup = $svdGrps[$group['LOCATION_GROUP_ID']];
						break;
					}
				}
			}
		}
		if($setup)
			switch($setup){
				case 'noSHOW'    : $variants = array(); break;
				case 'noCOURIER' : if(array_key_exists('kurer',$variants)) unset($variants['kurer']); break;
				case 'noPOST'    : foreach($variants as $key => $val)
									   if(strpos($key,'pr')===0)
											unset($variants[$key]);
								   break;
				case 'onPVZ'     : foreach($variants as $key => $val)
									   if(strpos($key,'pvz_')!==0)
											unset($variants[$key]);
								   break;
			}
	}

	function modifyPrice(&$variants,$price){
		if(empty($variants)) return;

		$modf = COption::GetOptionString(self::$MODULE_ID,'dostModType','');
		$free = floatval(COption::GetOptionString(self::$MODULE_ID,'freeDeliv',0));
		if($modf == '' && $free == 0) return;

		if($free != 0 && $price>=$free)
			$free = true;
		else
			$free = false;

		$value = COption::GetOptionString(self::$MODULE_ID,'dostModVal','0');
		if('exact' == $modf)
			$value = unserialize($value);
		foreach($variants as $profile => $descr){
			if($free)
				$newPrice = 0;
			else{
				$newPrice = floatval(str_replace(',','.',$variants[$profile][GetMessage('B2CPL_DELIVERY_STOIMOSTQ')]));
				switch($modf){
					case 'discount':
					case 'mark'    : if(COption::GetOptionString(self::$MODULE_ID,'dostModKhar','RUB') == 'RUB')
										$detr = $value;
									 else
										$detr = $descr[GetMessage('B2CPL_DELIVERY_STOIMOSTQ')] * $value / 100;
									 if($modf == 'discount')
										$newPrice -= $detr;
									 else
										$newPrice += $detr;
									 break;
					case 'exact'   : if($profile == 'kurer')
										$linker = 'Courier';
									 elseif(strpos($profile,'pvz') === 0)
										$linker = 'PVZ';
									 else
										$linker = 'POST';
									 if(array_key_exists($linker,$value) && $value[$linker])
										$newPrice =  $value[$linker];
									 break;
					case 'order'   : $newPrice += $price*$value/100;
									 break;
				}
			}
			$variants[$profile][GetMessage('B2CPL_DELIVERY_STOIMOSTQ')] = ($newPrice > 0) ? $newPrice : GetMessage('B2CPL_DELIVERY_FREE');
		}
	}

	/*
		���������� ������
		== OnSaleComponentOrderOneStepPersonType ==  == GetDefaultPersonType == 
	*/

		// ���������� �������
	public function OnSaleComponentOrderOneStepPersonType(&$arResult, &$arUserResult) {
			// ������������� ������ ���, ����� �������� ������, ����� ����������� �������� - ���� ��.
		if ($_POST['is_ajax_post'] != 'Y')
			CJSCore::Init(array('jquery'));
		else
			echo CJSCore::Init(array('jquery'), true);

		if ($PERSON_TYPE_ID = self::GetDefaultPersonType()) {
			$arLocationProp = CSaleOrderProps::GetList(array(), array('PERSON_TYPE_ID' => $PERSON_TYPE_ID, 'IS_ZIP' => 'Y'))->GetNext();
			echo '<script type="text/javascript">setTimeout("window.top.b2cpl_zip(' . intval($arLocationProp['ID']) . ');", 500);</script>';
		}
		$GLOBALS['APPLICATION']->AddHeadString(
				"
				<script type=\"text/javascript\">
					var jqueryVersion = $.fn.jquery.match('([0-9]{1})\.([0-9]{1,2})\.([0-9]{1,2})');
					if(jqueryVersion[1]==1){
						if(jqueryVersion[2]>7){
							jQuery.fn.extend({
								live: function( types, data, fn ) {
									jQuery( this.context ).on( types, this.selector, data, fn );
									return this;
								}
							});
						}
					 }
					var b2cpl_last_zip = '';
					b2cpl_zip = function(propID) {
						if(typeof(propID) == 'undefined')
							return false;
						$('#ORDER_PROP_' + propID).live('focus', function(){
							b2cpl_last_zip = $(this).val();
						}).live('blur', function(){
							if($(this).val() != b2cpl_last_zip) {
								b2cpl_last_zip = $(this).val();
								window.top.submitForm('N');
							}
						});
					}
				</script>
				"
		);
	}
	private function GetDefaultPersonType() {
		$PERSON_TYPE_ID = '';
		$rsPT = CSalePersonType::GetList(Array('SORT' => 'ASC', 'NAME' => 'ASC'), Array('LID' => SITE_ID, 'ACTIVE' => 'Y'));
		if ($arPT = $rsPT->Fetch())
			$PERSON_TYPE_ID = $arPT['ID'];
		return $PERSON_TYPE_ID;
	}

	/*
		������ ������ �������
		== timePickerLoader ==  == pvzLoader ==  == onBufferContent ==  == no_json ==  == onOrderCreate ==
	*/

	function timePickerLoader($arResult,$arUR){
		if(COption::GetOptionString(self::$MODULE_ID,'enTP','Y') == 'N' || !self::isActive())
			return;
		include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/tools/'.self::$MODULE_ID.'/timepicker.php';
	}
	function pvzLoader(){
		include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/tools/'.self::$MODULE_ID.'/pickuppicker.php';
	}
	function onBufferContent(&$content) {
		if(($_REQUEST['is_ajax_post'] == 'Y' || $_REQUEST["AJAX_CALL"] == 'Y' || $_REQUEST["ORDER_AJAX"]) && self::$city && self::no_json($content) && self::isActive()){
			$content .= '<input type="hidden" id="b2cpl_city"   name="b2cpl_city" value=\''.self::$city.'\' />';//����� ���������
			$content .= '<input type="hidden" id="b2cpl_minDate"   name="b2cpl_minDate" value=\''.self::$minDate.'\' />';
			$content .= '<input type="hidden" id="b2cpl_pvz"  name="b2cpl_pvz"  value=\''.$_REQUEST['B2CPL_pvz'].'\' />';//��������� ��������� ������� ��������
			$content .= '<input type="hidden" id="b2cpl_curpvzs"  name="b2cpl_curpvzs"  value="'.CUtil::PhpToJsObject(self::$curPvzs).'" />';//��������� ��������� ������� ��������
		}
	}
	function no_json($wat){
		return is_null(json_decode(self::zajsonit($wat),true));
	}

	// ���������� ������� ������
	function onOrderCreate($order){
		if(!$order)
			return;
		$profile = self::defineDelivery($_REQUEST['DELIVERY_ID']);
		if(!$profile)
			return;

		if($profile == 'kurer' || $profile == 'pickup'){
			self::controlProps();
			$pType=CSaleOrder::GetById($order);
			$pType=$pType['PERSON_TYPE_ID'];
			$arProp = false;
			if($_REQUEST['B2CPL_date'] && $profile == 'kurer'){
				$arProp = array(
					'CODE'  => 'B2CPL_DATEDELIV',
					'NAME'  => GetMessage('B2CPL_PROP_DATEDELIV_name'),
 					'VALUE' => $_REQUEST['B2CPL_date']." ".$_REQUEST['B2CPL_time']
				);
			}elseif($_REQUEST['B2CPL_pvz'] && $profile == 'pickup')
				$arProp = array(
					'CODE'  => 'B2CPL_PVZ',
					'NAME'  => GetMessage('B2CPL_PROP_PVZ_name'),
 					'VALUE' => $_REQUEST['B2CPL_pvz']
				);

			if($arProp){
				$prop = CSaleOrderProps::GetList(array(),array('CODE'=>$arProp['CODE'],'PERSON_TYPE_ID'=>$pType))->Fetch();
				if($prop)
					CSaleOrderPropsValue::Add(
					array(
						"ORDER_ID" => $order,
						"ORDER_PROPS_ID" => $prop['ID'],
						"NAME"  => $arProp['NAME'],
						"CODE"  => $arProp['CODE'],
						"VALUE" => $arProp['VALUE']
					)
				);
			}
		}
	}

	/*
		���������������
		== bitrixDontShowOff ==  == CASDH_Set ==
	*/
	private function bitrixDontShowOff($profs){
		if($_REQUEST['Update'] == 'Y' && strpos($_SERVER['REQUEST_URI'],'/bitrix/admin/sale_delivery_handler_edit.php')!==false)
			return;

		global $DB;
		$arDef = array(
			"RESTRICTIONS_WEIGHT" => Array(0.00,0.00),
			"RESTRICTIONS_SUM" => Array(0.00,0.00),
			"ACTIVE" => 'Y',
			"TAX_RATE" => 0,
			"RESTRICTIONS_DIMENSIONS_SUM" => 0,
			"RESTRICTIONS_MAX_SIZE" => 0,
			"RESTRICTIONS_DIMENSIONS" => Array(0,0,0),
		);

		$query = "
		SELECT HID AS SID, LID, ACTIVE, NAME, SORT, DESCRIPTION, HANDLER, SETTINGS, PROFILES, TAX_RATE, LOGOTIP, BASE_CURRENCY
		FROM b_sale_delivery_handler
		WHERE HID = 'b2cpl'";

		if(SITE_ID)
			$query .= "AND (LID='".$DB->ForSql(SITE_ID)."' OR LID='' OR LID IS NULL)";

		$dbRes = $DB->Query($query);
		while($arRes = $dbRes->Fetch()){
			$profsCopy = $profs;
			$profSaved = unserialize($arRes['PROFILES']);
			foreach($profSaved as $profId => $profVal){
				if(!array_key_exists($profId,$profsCopy))
					unset($profSaved[$profId]);
				else{
					$profSaved[$profId]['DESCRIPTION'] = $profsCopy[$profId]['DESCRIPTION'];
					unset($profsCopy[$profId]);
				}
			}
			foreach($profsCopy as $profId => $descr)
				$profSaved[$profId] = array_merge(array('TITLE'=>$descr['TITLE'],'DESCRIPTION'=>$descr['DESCRIPTION']),$arDef);
			$arRes['PROFILES'] = $profSaved;
			$arRes['SETTINGS'] = unserialize($arRes['SETTINGS']);
			include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/sale/general/delivery_handler.php');
			// CAllSaleDeliveryHandler::Set('b2cpl',$arRes,($arRes['LID'])?$arRes['LID']:false);
			self::CASDH_Set('b2cpl',$arRes,($arRes['LID'])?$arRes['LID']:false);
		}
	}

	private function CASDH_Set($SID, $arData, $SITE_ID = false){
		if ($SITE_ID == 'ALL')
			$SITE_ID = false;

		if (!defined('SALE_DH_INITIALIZED'))
			CSaleDeliveryHandler::Initialize();

		global $APPLICATION, $DB;

		$DB->StartTransaction();
		$arQueryFields = array();

		if ($SITE_ID)
			$arQueryFields["LID"] = "'".($SITE_ID == "ALL" ? "" : $DB->ForSql($SITE_ID))."'";
		else
			$arQueryFields["LID"] = "''";

		if (is_set($arData, "ACTIVE"))
			$arQueryFields["ACTIVE"] = $arData["ACTIVE"] == 'Y' ? "'Y'" : "'N'";
		else
			$arQueryFields["ACTIVE"] = "'N'";

		if(is_set($arData, "SORT"))          $arQueryFields["SORT"]          = "'".intval($arData["SORT"])."'";
		if(is_set($arData, "NAME"))          $arQueryFields["NAME"]          = "'".$DB->ForSql($arData["NAME"])."'";
		if(is_set($arData, "DESCRIPTION"))   $arQueryFields["DESCRIPTION"]   = "'".$DB->ForSql($arData["DESCRIPTION"])."'";
		if(is_set($arData, "HANDLER"))	     $arQueryFields["HANDLER"]       = "'".$DB->ForSql($arData["HANDLER"])."'";
		if(is_set($arData, "TAX_RATE"))      $arQueryFields["TAX_RATE"]      = "'".doubleval($arData["TAX_RATE"])."'";
		if(is_set($arData, "BASE_CURRENCY")) $arQueryFields["BASE_CURRENCY"] = "'".$DB->ForSql($arData["BASE_CURRENCY"])."'";
		if(is_set($arData, 'LOGOTIP') && intval($arData["LOGOTIP"]) > 0) $arQueryFields["LOGOTIP"] = $arData["LOGOTIP"];

		if(is_set($arData, "CONFIG")){
			if (!$strSettings = self::SetSettings($arData["CONFIG"])){
				$DB->Rollback();
				return false;
			}
			$arQueryFields["SETTINGS"] = "'".$DB->ForSql($strSettings)."'";
		}

		if (is_array($arData["PROFILES"]) && count($arData["PROFILES"]) > 0)
			$arQueryFields["PROFILES"] = "'".$DB->ForSql(serialize($arData["PROFILES"]))."'";

		$strWhere = "WHERE HID='".$DB->ForSql($SID)."'";
		if($SITE_ID) $strWhere .= " AND LID='".$DB->ForSql($SITE_ID)."'";

		$DB->Update("b_sale_delivery_handler", $arQueryFields, $strWhere);
		$DB->Commit();
	}


	// ������


	/*
		�������
		== IsFinalStatus ==  == StatusOrder ==  == StatusOrderAgent ==
	*/


	private function IsFinalStatus($status) {
		$status = trim(ToLower($status));
		if (strlen($status)) {
			return in_array($status, array(
						GetMessage('B2CPL_FINAL_STATUS_1'),
						GetMessage('B2CPL_FINAL_STATUS_2'),
						GetMessage('B2CPL_FINAL_STATUS_3'),
						GetMessage('B2CPL_FINAL_STATUS_4'),
						GetMessage('B2CPL_FINAL_STATUS_5'),
						GetMessage('B2CPL_FINAL_STATUS_6')
			));
		} else
			return false;
	}

	public function StatusOrder($orderID, &$final=false) {
		$orderID = intval($orderID);
		if (!$orderID)
			return false;

		$arQuery = array(
			'func'      => 'status',
			'client'    => COption::GetOptionString(self::$MODULE_ID, 'client', false),
			'key'       => COption::GetOptionString(self::$MODULE_ID, 'key', false),
			'code'      => $orderID,
			'code_type' => 'client',
		);
		$data = self::Query($arQuery);
		$data = $GLOBALS['APPLICATION']->ConvertCharset($data, 'windows-1251', SITE_CHARSET);
		$arData = CUtil::JsObjectToPhp($data);
		if (is_array($arData)) {
			$code = COption::GetOptionString(self::$MODULE_ID,'order_prop_for_b2ccode',false);
			if(strlen($code) && ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => $code))->Fetch())) {
				$arFields = array(
					'NAME' => $arProp['NAME'],
					'CODE' => $arProp['CODE'],
					'ORDER_PROPS_ID' => $arProp['ID'],
					'ORDER_ID' => $orderID,
					'VALUE' => $arData['status'],
				);
				if ($arPropVal = CSaleOrderPropsValue::GetList(array('ID' => 'DESC'), array('ORDER_PROPS_ID' => $arProp['ID'], 'ORDER_ID' => $orderID))->Fetch()) {
					$propID = $arPropVal['ID'];
					CSaleOrderPropsValue::Update($propID, $arFields);
				} else {
					$propID = CSaleOrderPropsValue::Add($arFields);
				}
				$final = self::IsFinalStatus($arData['status']);
				return $propID>0;
			}
		}
		return false;
	}

	/**
	 * ������� ������ ��� �������� ������ ��������
	 * @param $orderID
	 *
	 * @return string
	 */
	public function StatusOrderAgent($orderID) {
		$final = false;
		$orderID = intval($orderID);
		if (self::StatusOrder($orderID, $final) === false)
			return '';
		elseif ($final)
			return '';
		else
			return 'Cb2cplDelivery::StatusOrderAgent('.$orderID.');';
	}

	/**
	 * ������� ������ ��� �������� ������ ������� (�������� � ���������� call)
	 * @param $orderID
	 *
	 * @return string
	 */
	public function CallStatusOrderAgent($orderID) {
		global $APPLICATION;

		if(!CModule::IncludeModule("sale")){
			return 'Cb2cplDelivery::CallStatusOrderAgent('.$orderID.');';
		}
		$orderID = intval($orderID);
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		if (
			$orderID
		) {
			$rsOrder = CSaleOrder::GetList(array(), array("ID" => $orderID), false, false, array("ID", "PERSON_TYPE_ID"));
			if(!$arOrder = $rsOrder->GetNext()){
				return '';
			}
		} else {
			return '';
		}

		$arQuery = array(
			'func'   => 'order_call_report',
			'client' => COption::GetOptionString(self::$MODULE_ID,'client',false),
			'key'    => COption::GetOptionString(self::$MODULE_ID,'key',false),
			'order_codes'   => array($orderID)
		);

		$data = self::QueryPost($arQuery);
		$dataConv = $APPLICATION->ConvertCharset($data, 'UTF-8', SITE_CHARSET);
		$arData = CUtil::JsObjectToPhp($dataConv);
		if (is_array($arData) && $arData['success']) {
			$order = $arData["OrdersReport"][0];
			$call_res = strtolower($order["call_result"]);

			if ($call_res === strtolower(GetMessage("B2CPL_pereofr")) ) {
				self::ProcessCallStatus($orderID, $order);
				return '';
			} elseif(strpos($call_res, strtolower(GetMessage('B2CPL_cancel'))) === 0) {
				self::OrderCancel($orderID, $order["call_comment"]);
				return '';
			}
		}
		return 'Cb2cplDelivery::CallStatusOrderAgent('.$orderID.');';
	}

	protected function ProcessCallStatus($orderID, $data) {
		CModule::IncludeModule("sale");
		CModule::IncludeModule("catalog");
		CModule::IncludeModule("iblock");
		if ( (int) $orderID <= 0 || !is_array($data) ) {
			return;
		}

		$artnumber = COption::GetOptionString(self::$MODULE_ID, 'articul', false);
		$location = COption::GetOptionString(self::$MODULE_ID, 'city', false);
		$zip = COption::GetOptionString(self::$MODULE_ID, 'index', false);
		$address = COption::GetOptionString(self::$MODULE_ID, 'address', false);
		$fio = COption::GetOptionString(self::$MODULE_ID, 'fio', false);
		$mphone = COption::GetOptionString(self::$MODULE_ID, 'mphone', false);
		$email = COption::GetOptionString(self::$MODULE_ID, 'email', false);
		$uphone = COption::GetOptionString(self::$MODULE_ID, 'uphone', false);

		if(!$artnumber){
			return;
		}
		foreach ($data["order"]["items"] as $key => $product) {
			$data["order"]["items"][$product["prodcode"]] = $product;
			unset($data["order"]["items"][$key]);
		}
		if ( self::isConverted() ) {
			/**
			 * @var \Bitrix\Sale\BasketPropertiesCollection $oPropeties
			 */
			$order = \Bitrix\Sale\Order::load($orderID);
			$basket = $order->getBasket();
			$basketItems = $basket->getBasketItems();

			$arArtCompare = array();
			foreach ($basketItems as $key => $item) {
				unset($fromService);
				$oPropeties = $item->getPropertyCollection();
				$arProps = $oPropeties->getPropertyValues();

				if($arProps[$artnumber]["VALUE"]){
					$fromService = $data["order"]["items"][$arProps[$artnumber]["VALUE"]];
				} else {
					$productID = $item->getField("PRODUCT_ID");
					$rsElm = CIBlockElement::GetList(array(), array("ID" => $productID), false, false, array("ID", "IBLOCK_ID", "PROPERTY_{$artnumber}"));

					if($elm = $rsElm->GetNext()){
						if($num = $elm["PROPERTY_{$artnumber}_VALUE"]){
							$fromService = $data["order"]["items"][$num];
						}
					}
				}

				if ( isset($fromService) ) {
					$arArtCompare[] = $fromService["prodcode"];
					if ( (float) $item->getQuantity() !== (float) $fromService["quantity"] ) {
						$item->setField("QUANTITY", $fromService["quantity"]);
					}
					if ( (float) $item->getFinalPrice() !== (float) $fromService["price_pay"] ) {
						$item->setFields(
							array(
								"PRICE"                => $fromService["price_pay"],
								"CUSTOM_PRICE"         => "Y",
								"IGNORE_CALLBACK_FUNC" => "Y",
							)
						);
					}
					if((float) $item->getWeight() !== (float) $fromService['weight']){
						$item->setField("WEIGHT", $fromService["weight"]);
					}
					$item->save();
				} else {
					$item->delete();
				}
			}

			$shipment = $order->getShipmentCollection();
			if((float) $shipment->getPriceDelivery() !== (float) $data["order"]["price_delivery_pay"]){
				$arShipment = Bitrix\Sale\Delivery\Services\Manager::getActiveList();
				$arB2Shipments = array();
				foreach ($arShipment as $ship){
					if ( substr($ship["CODE"], 0, 5) === "b2cpl" ) {
						$arB2Shipments[] = $ship["ID"];
					}
				}
				foreach ($shipment->getIterator() as $ar){
					if(in_array($ar->getField("DELIVERY_ID"), $arB2Shipments)){
						$shipmentEntity = $ar;
						break;
					}
				}
				if(isset($shipmentEntity)){
					$shipmentEntity->setField("BASE_PRICE_DELIVERY", (float) $data["order"]["price_delivery_pay"]);
					$shipment->save();
				}
			}

			foreach ($data["order"]["items"] as $key => $item){
				if ( in_array($key, $arArtCompare) ) {
					continue;
				}
				$basketItem = $basket->createItem(self::$MODULE_ID, rand(1, 9999));
				$basketItem->setFields(
					array(
						"NAME"     => $item["prodname"],
						"QUANTITY" => $item["quantity"],
						"PRICE"    => $item["price_pay"],
						"WEIGHT"   => $item["weight"],
						"BASE_PRICE"    => $item["price_pay"],
						"CURRENCY" => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
						'LID'      => \Bitrix\Main\Context::getCurrent()->getSite(),
					)
				);
				$basketItem->save();

				$basketProps = $basketItem->getPropertyCollection();
				$artnumberProp = array();
				$artnumberProp[] = array(
					"NAME"  => GetMessage("B2CPL_product_artnumber"),
					"CODE"  => $artnumber,
					"VALUE" => $item["prodcode"],
				);
				$basketProps->setProperty($artnumberProp);
				$basketProps->setBasketItem($basketItem);
				$basketProps->save();
			}
			$basket->save();
			$order->doFinalAction(true);
			$order->save();
		} else {
			$flagNeedUpdate = false; //���� ������������� ��������� ������
			/** @noinspection PhpDynamicAsStaticMethodCallInspection */
			$rsOrderBasket = \CSaleBasket::GetList(array(), array("ORDER_ID" => $orderID));
			$arBasket = array();
			$arResult = array();
			$arProductFilter = array();
			while ($arItem = $rsOrderBasket->GetNext()) {
				$arBasket[$arItem["ID"]] = $arItem;
				$arProductFilter["BASKET_ID"][] = $arItem["ID"];
			}
			$arProductFilter["CODE"] = $artnumber;
			/** @noinspection PhpDynamicAsStaticMethodCallInspection */
			$rsProdProps = \CSaleBasket::GetPropsList(array(), $arProductFilter);
			while ($arProp = $rsProdProps->GetNext()){
				$arResult[$arProp["VALUE"]] = $arBasket[$arProp["BASKET_ID"]];
				unset($arBasket[$arProp["BASKET_ID"]]);
			}

			foreach ($arBasket as $key => $item){
				if($item["PRODUCT_ID"]){
					$rsElm = CIBlockElement::GetList(array(), array("ID" => (int) $item["PRODUCT_ID"]), false, false, array("ID", "IBLOCK_ID", "PROPERTY_{$artnumber}"));

					if($elm = $rsElm->GetNext()){
						if($num = $elm["PROPERTY_{$artnumber}_VALUE"]){
							$arResult[$num] = $item;
							unset($arBasket[$key]);
						}
					}
				}
			}

			$arArtCompare = array();
			if(count($arResult)){
				foreach ($arResult as $key => $item) {
					$fromService = $data["order"]["items"][$key];
					if ( isset($fromService) ) {
						$arArtCompare[] = $key;
						$newData = array();
						if((float) $fromService["quantity"] !== (float) $item["QUANTITY"]){
							$newData["QUANTITY"] = $fromService["quantity"];
						}
						if((float) $fromService["price"] !== (float) $item["PRICE"]){
							$newData["PRICE"] = $fromService["price"];
						}
						if(count($newData)){
							if(!$flagNeedUpdate){
								$flagNeedUpdate = true;
							}
							/** @noinspection PhpDynamicAsStaticMethodCallInspection */
							\CSaleBasket::Update($item["ID"], $newData);
						}
					}
				}
			}
			foreach ($arBasket as $key => $item){
				\CSaleBasket::Delete($key);
			}

			/** @noinspection PhpDynamicAsStaticMethodCallInspection */
			$arOrder = \CSaleOrder::GetList(array(), array("ID" => $orderID), false, false, array("LID", "ID", "PRICE_DELIVERY", "DELIVERY_ID"))->GetNext();
			if($data["order"]["price_delivery_pay"] != $arOrder["PRICE_DELIVERY"] && substr($arOrder["DELIVERY_ID"], 0, 5) === "b2cpl"){
				/** @noinspection PhpDynamicAsStaticMethodCallInspection */
				CSaleOrder::Update($orderID, array("PRICE_DELIVERY", $data["order"]["price_delivery_pay"]));
				if(!$flagNeedUpdate){
					$flagNeedUpdate = true;
				}
			}

			foreach ($data["order"]["items"] as $key => $item) {
				if ( in_array($key, $arArtCompare) ) {
					continue;
				}
				$arFields = array(
					"PRODUCT_ID" => rand(1, 9999),
					"PRODUCT_PRICE_ID" => 0,
					"CUSTOM_PRICE" => "Y",
					"ORDER_ID"   => $orderID,
					"MODULE"     => self::$MODULE_ID,
					"NAME"       => $item["prodname"],
					"PRICE"      => $item["price_pay"],
					"QUANTITY"   => $item["quantity"],
					"CURRENCY"   => \CCurrency::GetBaseCurrency(),
					"LID"        => $arOrder["LID"],
					"PROPS"      => array(
						array(
							"NAME"  => GetMessage("B2CPL_product_artnumber"),
							"CODE"  => $artnumber,
							"VALUE" => $item["prodcode"],
						),
					),
				);
				/** @noinspection PhpDynamicAsStaticMethodCallInspection */
				\CSaleBasket::Add($arFields);
				if(!$flagNeedUpdate){
					$flagNeedUpdate = true;
				}
			}

			if($flagNeedUpdate) {
				$contents = array();
				/** @noinspection PhpDynamicAsStaticMethodCallInspection */
				$rsOrder = \CSaleBasket::GetList(array(), array("ORDER_ID" => $orderID));
				while ($arItem = $rsOrder->GetNext()){
					$contents[] = $arItem;
				}
				$arDelivery = \CSaleOrder::GetList(array(), array("ID" => $orderID), false, false, array("ID", "PRICE_DELIVERY"))->GetNext();
				$sum = 0;
				foreach ($contents as $basket_item) {
					if ( $basket_item['DISCOUNT_PRICE'] > 0 ) {
						$sum += $basket_item['DISCOUNT_PRICE'] * $basket_item['QUANTITY'];
					} else {
						$sum += $basket_item['PRICE'] * $basket_item['QUANTITY'];
					}
				}
				$arFields = array(
					"PRICE" => $sum += $arDelivery["PRICE_DELIVERY"],
				);
				/** @noinspection PhpDynamicAsStaticMethodCallInspection */
				\CSaleOrder::Update($orderID, $arFields);
			}
		}

		if(!empty($location)){
			$prop = \CSaleOrderPropsValue::GetList(array(), array("ORDER_ID" => $orderID, "CODE"=>$location))->Fetch();
			$dt = $data["order"]["city"];
			if($prop["VALUE"] !== $dt){
				\CSaleOrderPropsValue::Update($prop["ID"], array("VALUE" => $dt));
			}
		}
		if(!empty($zip)){
			$prop = \CSaleOrderPropsValue::GetList(array(), array("ORDER_ID" => $orderID, "CODE"=>$zip))->Fetch();
			$dt = $data["order"]["zip"];
			if($prop["VALUE"] !== $dt){
				\CSaleOrderPropsValue::Update($prop["ID"], array("VALUE" => $dt));
			}
		}
		if(!empty($address)){
			$prop = \CSaleOrderPropsValue::GetList(array(), array("ORDER_ID" => $orderID, "CODE"=>$address))->Fetch();
			$dt = $data["order"]["addr"];
			if($prop["VALUE"] !== $dt){
				\CSaleOrderPropsValue::Update($prop["ID"], array("VALUE" => $dt));
			}
		}
		if(!empty($fio)){
			$prop = \CSaleOrderPropsValue::GetList(array(), array("ORDER_ID" => $orderID, "CODE"=>$fio))->Fetch();
			$dt = $data["order"]["fio"];
			if($prop["VALUE"] !== $dt){
				\CSaleOrderPropsValue::Update($prop["ID"], array("VALUE" => $dt));
			}
		}
		if(!empty($mphone)){
			$prop = \CSaleOrderPropsValue::GetList(array(), array("ORDER_ID" => $orderID, "CODE"=>$mphone))->Fetch();
			$dt = $data["order"]["phone1"];
			if($prop["VALUE"] !== $dt){
				\CSaleOrderPropsValue::Update($prop["ID"], array("VALUE" => $dt));
			}
		}
		if(!empty($email)){
			$prop = \CSaleOrderPropsValue::GetList(array(), array("ORDER_ID" => $orderID, "CODE"=>$email))->Fetch();
			$dt = $data["order"]["email"];
			if($prop["VALUE"] !== $dt){
				\CSaleOrderPropsValue::Update($prop["ID"], array("VALUE" => $dt));
			}
		}
		if(!empty($uphone)){
			$prop = \CSaleOrderPropsValue::GetList(array(), array("ORDER_ID" => $orderID, "CODE"=>$uphone))->Fetch();
			$dt = $data["order"]["phone2"];
			if($prop["VALUE"] !== $dt){
				\CSaleOrderPropsValue::Update($prop["ID"], array("VALUE" => $dt));
			}
		}
	}

	protected function OrderCancel($orderID, $reason){
		CModule::IncludeModule("sale");
		$rsOrder = CSaleOrder::GetList(array(), array("ID" => $orderID, "CANCELED" => "N"));
		if($rsOrder->SelectedRowsCount()){
			CEventLog::Log("INFO", "ORDER_CANCEL", self::$MODULE_ID, "orderCancel", GetMessage("B2CPL_cancel_order").$reason);
			if(!isset($USER)){
				global $USER;
				$arUser = CUser::GetList($by, $order, array("GROUPS_ID" => array(1)))->GetNext();

				$USER = new CUser($arUser["ID"]);
			}
			CSaleOrder::CancelOrder($orderID, "Y", $reason);
			unset($USER);
		}
	}

	// ������
	function OnAdminContextMenuShowHandler(&$items){
		if(class_exists('Cb2cplRequest')) return Cb2cplRequest::OnAdminContextMenuShowHandler($items);
	}
}
?>

<?
cmodule::includeModule('sale');
IncludeModuleLangFile(__FILE__);

class CB2CPLOpts extends Cb2cplHelper{
	function getMPSetups(){
		return array(
			'noPOST'    => GetMessage('B2CPL_LBL_noPOST'),
			'noCOURIER' => GetMessage('B2CPL_LBL_noCOURIER'),
			'noSHOW'    => GetMessage('B2CPL_LBL_noSHOW'),
			'onPVZ'     => GetMessage('B2CPL_LBL_onPVZ'),
		);
	}

	function addCitySetup($params){
		if(!$params['cName'])  $params['cName']=1;
		$arSetups = self::getMPSetups();

		echo "<tr><td style='text-align:center'>";

		if($params['cValue'] && ($mp = CSaleLocation::GetByID($params['cValue']))){
			echo $mp['COUNTRY_NAME_LANG'].", ".$mp['CITY_NAME_LANG']."<input name='b2cpl_citysetup_city_{$params["cName"]}' id='b2cpl_citysetup_city_{$params['cName']}' value='{$params["cValue"]}' type='hidden'>";
		}else{
			$GLOBALS['APPLICATION']->IncludeComponent(
				"bitrix:sale.ajax.locations",
				"popup",
				array(
					"AJAX_CALL" => "N",
					"COUNTRY_INPUT_NAME" => "COUNTRY_".$params['cName'],
					"REGION_INPUT_NAME" => "REGION_".$params['cName'],
					"CITY_INPUT_NAME" => "b2cpl_citysetup_city_".$params['cName'],
					"CITY_OUT_LOCATION" => "Y",
					"LOCATION_VALUE" => $params['cValue'],
					"ONCITYCHANGE" => "",
				),
				null,
				array('HIDE_ICONS' => 'Y')
			);
		}
		echo "</td><td style='text-align:center'><select name='b2cpl_citysetup_setup[{$params['cName']}]'><option value=''></option>";
		foreach($arSetups as $setCode => $setName)
			if($setCode == $params['cSetup'])
				echo "<option value='$setCode' selected>$setName</option>";
			else
				echo "<option value='$setCode'>$setName</option>";
		echo "</select></td></tr>";
	}

	function addGMPSetup($params = array()){
		if(!$params['mpName'])  $params['mpName']=1;
		
		$arSetups = self::getMPSetups();
		
		$strMpList = "";
		$dbMPGroups = CSaleLocationGroup::GetList();
		while($arMP=$dbMPGroups->Fetch())
			if($arMP['ID'] == $params['mpValue'])
				$strMpList.="<option selected value='{$arMP['ID']}'>{$arMP['NAME']}</option>";
			else
				$strMpList.="<option value='{$arMP['ID']}'>{$arMP['NAME']}</option>";

		echo "<tr>
				<td style='text-align:center'>
					<select name='b2cpl_mpsetup[{$params['mpName']}][mp]'>".
						$strMpList.
					"</select>
				</td><td style='text-align:center'>
					<select name='b2cpl_mpsetup[{$params['mpName']}][st]'>
						<option value=''></option>";
	foreach($arSetups as $setCode => $setName)
		if($setCode == $params['mpSetup'])
			echo "<option value='$setCode' selected>$setName</option>";
		else
			echo "<option value='$setCode'>$setName</option>";
		echo "</select></td></tr>";
	}

	function killCache(){
		$obCache = new CPHPCache();
		$obCache->CleanDir('/'.SITE_ID.self::$cachePath);
		echo GetMessage('B2CPL_LBL_cacheCleared');
	}

	/*
		�������
	*/

	function tableHandler($params){
		$arSelect[0]=($params['by'])?$params['by']:'ORDER_ID';
		$arSelect[1]=($params['sort'])?$params['sort']:'DESC';
		
		$arNavStartParams['iNumPage']=($params['page'])?$params['page']:1;
		$arNavStartParams['nPageSize']=($params['pgCnt']!==false)?$params['pgCnt']:1;
		
		foreach($params as $code => $val)
			if(strpos($code,'F')===0)
				$arFilter[substr($code,1)]=$val;

		$requests   = self::select($arSelect,$arFilter,$arNavStartParams);
		$arPVZ = self::getPVZ();
		$strHtml='';
		while($request=$requests->Fetch()){
			$reqParams=unserialize($request['PARAMS']);

			$paramsSrt='';
			foreach($reqParams as $parCode => $parVal){
				if($parCode == 'pack') $parVal = GetMessage("B2CPL_FORM_pack_$parVal");
				if($parCode == 'term') $parVal = GetMessage("B2CPL_FORM_term_$parVal");
				if($parCode == 'address' && strpos($parVal,'pvz_')===0) $parVal = $arPVZ[$parVal]['id']." (".$arPVZ[$parVal]['addr'].")";
				$paramsSrt.=GetMessage("B2CPL_TABLE_$parCode").": ".$parVal."<br>";
			}
			
			$addClass='';
			if($request['STATUS']=='OK')
				$addClass='B2CPL_TblStOk';
			if($request['STATUS']=='ERROR')
				$addClass='B2CPL_TblStErr';
			if($request['STATUS']=='TRANZT')
				$addClass='B2CPL_TblStTzt';	
			if($request['STATUS']=='DELETE')
				$addClass='B2CPL_TblStDel';
			if($request['STATUS']=='STORE')
				$addClass='B2CPL_TblStStr';			
			if($request['STATUS']=='CORIER')
				$addClass='B2CPL_TblStCor';		
			if($request['STATUS']=='PVZ')
				$addClass='B2CPL_TblStPVZ';			
			if($request['STATUS']=='OTKAZ')
				$addClass='B2CPL_TblStOtk';			
			if($request['STATUS']=='DELIVD')
				$addClass='B2CPL_TblStDvd';
			
 			$contMenu='<td class="adm-list-table-cell adm-list-table-popup-block" onclick="BX.adminList.ShowMenu(this.firstChild,[{\'DEFAULT\':true,\'GLOBAL_ICON\':\'adm-menu-edit\',\'DEFAULT\':true,\'TEXT\':\''.GetMessage('B2CPL_TABLE_TOORDR').'\',\'ONCLICK\':\'BX.adminPanel.Redirect([],\\\'sale_order_detail.php?ID='.$request['ORDER_ID'].'&lang=ru\\\', event);\'}';
			if($request['STATUS']=='ERROR' || $request['STATUS']=='NEW' || $request['STATUS']=='DELETE')
				$contMenu.=',{\'GLOBAL_ICON\':\'adm-menu-delete\',\'TEXT\':\''.GetMessage('B2CPL_TABLE_DELETE').'\',\'ONCLICK\':\'B2CPL_delSign('.$request['ORDER_ID'].')\'}';
			if($request['STATUS']=='OK'){
				$contMenu.=',{\'TEXT\':\''.GetMessage('B2CPL_TABLE_STICKER').'\',\'ONCLICK\':\'B2CPL_GETSTICKER(\\\''.$request['STICKERS_URL'].'\\\')\'}';
				// $contMenu.=',{\'GLOBAL_ICON\':\'adm-menu-delete\',\'TEXT\':\''.GetMessage('IPOLSDEK_JSC_SOD_DESTROY').'\',\'ONCLICK\':\'IPOLSDEK_killSign('.$request['ORDER_ID'].')\'}';
			}
			$contMenu.='])"><div class="adm-list-table-popup"></div></td>';
			$strHtml.='<tr class="adm-list-table-row '.$addClass.'">
							'.$contMenu.'
							<td class="adm-list-table-cell"><div><a href="/bitrix/admin/sale_order_detail.php?ID='.$request['ORDER_ID'].'&lang=ru" target="_blank">'.$request['ORDER_ID'].'</a></div></td>
							<td class="adm-list-table-cell"><div>'.$request['B2CCODE'].'</div></td>
							<td class="adm-list-table-cell"><div>'.$request['STATUS'].'</div></td>
							<td class="adm-list-table-cell"><div><a href="javascript:void(0)" onclick="B2CPL_shwPrms($(this).siblings(\'div\'))">'.GetMessage('B2CPL_FORM_SHOW').'</a><div style="height:0px; overflow:hidden">'.$paramsSrt.'</div></div></td>
							<td class="adm-list-table-cell"><div>'.date("d.m.y H:i",$request['UPTIME']).'</div></td>
						</tr>';
		}
		echo json_encode(
			self::zajsonit(
				array(
					'ttl'=>$requests->NavRecordCount,
					'mP'=>$requests->NavPageCount,
					'pC'=>$requests->NavPageSize,
					'cP'=>$requests->NavPageNomer,
					'sA'=>$requests->NavShowAll,
					'html'=>$strHtml
				)
			)
		);
	}

	function delReqOD($oid){// �������� ������ �� ��
		if(is_array($oid))
			$oid = $oid['oid'];
		if(self::CheckRecord($oid))
			self::Delete($oid);
		echo GetMessage("B2CPL_TABLE_DELETED");
	}

	// ���
	public function syncPvz(){
		$arQuery = array(
			'client' => COption::GetOptionString(self::$MODULE_ID,'client',false),
			'key'    => COption::GetOptionString(self::$MODULE_ID,'key',false),
			'func'   => 'info_pvz'
		);
		$data = self::Query($arQuery);
		$data = $GLOBALS['APPLICATION']->ConvertCharset($data, 'windows-1251', SITE_CHARSET);
		$arData = CUtil::JsObjectToPhp($data);

		$arPVZ = array();
		if(count($arData['pvz'])) {
			foreach ($arData['pvz'] as $id => $arDescr) {
				if ( $arDescr['gps'] ) {
					$gps = explode("; ", $arDescr["gps"]);
					$arDescr["lng"] = substr($gps[0], 4);
					$arDescr["lat"] = substr($gps[1], 4);
				}
				$arPVZ[CUtil::translit($arDescr['id'], 'ru')] = array(
					'id'       => $arDescr['id'],
					'zip'      => $arDescr['pvz_zip'],
					'workTime' => $arDescr['pvz_time'],
					'gps'      => $arDescr['gps'],
					'gps_lng'  => $arDescr['lng'],
					'gps_lat'  => $arDescr['lat'],
					'metro'    => $arDescr['metro'],
					'addr'     => $arDescr['addr'],
					'phone'    => $arDescr['phone'],
				);
			}
		}
		return $arPVZ;
	}

	// ���������� ���������� �� ������������ � ��.
	function getRegions(){
		$data = self::Query(array(
			'client' => COption::GetOptionString(self::$MODULE_ID,'client',false),
			'key'    => COption::GetOptionString(self::$MODULE_ID,'key',false),
			'func'   => 'info_cities_from'			
		));
		$data = $GLOBALS['APPLICATION']->ConvertCharset($data, 'windows-1251', SITE_CHARSET);
		$arData = CUtil::JsObjectToPhp($data);
		$arRegions = array();

		foreach($arData['cities_from'] as $val)
			$arRegions[$val['region']] = $val['city_name'];

		return $arRegions;
	}
	// ���������� �������� �����������
	

	// �����������
	function auth($params){
		$rez = self::Query(array(
			'func'   => 'tarif',
			'client' => $params['client'],
			'key'    => $params['key'],
			'zip' 	 => '190000',
			'weight' => '1001',
			'x'		 => '121',
			'y'		 => '1',
			'z'		 => '1',
			'price'	 => '1000',
			'price_assess' => '1000',
			'region' => '77',
		));
		if($rez){
			$rez = CUtil::JsObjectToPhp($GLOBALS['APPLICATION']->ConvertCharset($rez, 'windows-1251', SITE_CHARSET));
			if($rez['flag_error'])
				echo GetMessage("B2CPL_AUTH_ERROR");
			else{
				COption::SetOptionString(self::$MODULE_ID,'logged',true);
				COption::SetOptionString(self::$MODULE_ID,'client',$params['client']);
				COption::SetOptionString(self::$MODULE_ID,'key',$params['key']);
				echo "G".GetMessage("B2CPL_AUTH_GOOD");
			}
		}
	}
	function logoff(){
		COption::SetOptionString(self::$MODULE_ID,'logged',false);
		COption::SetOptionString(self::$MODULE_ID,'client',false);
		COption::SetOptionString(self::$MODULE_ID,'key',false);
	}

	// ������ � ����� ���
	function checkOldPVZ(){
		return (self::isConverted() && self::getOldDeliveries()->Fetch());
	}	
	function checkPickup(){
		$return = true;
		if(self::isConverted())
			$return = Bitrix\Sale\Delivery\Services\Table::getList(array(
				 'order'  => array('SORT' => 'ASC', 'NAME' => 'ASC'),
				 'filter' => array('%CODE' => 'b2cpl:pickup')
			))->Fetch();
		return $return;
	}

	function getOldDeliveries(){
		$dS = Bitrix\Sale\Delivery\Services\Table::getList(array(
			 'order'  => array('SORT' => 'ASC', 'NAME' => 'ASC'),
			 'filter' => array('%CODE' => 'b2cpl:pvz_')
		));
		return $dS;
	}

	function printOldDelivError(){?>
	<table><tr><td>
		<div class="adm-info-message-wrap adm-info-message-red">
		  <div class="adm-info-message">
			<div class="adm-info-message-title"><?=GetMessage('B2CPL_ERROR_hasOldDelivs_TITLE')?></div>
				<?=GetMessage('B2CPL_ERROR_hasOldDelivs_DESCR')?>
			<div class="adm-info-message-icon"></div>
		  </div>
		</div>
	</td></tr></table>
	<?}

	function printNpPickup(){?>
		<table><tr><td>
		<div class="adm-info-message-wrap adm-info-message-red">
		  <div class="adm-info-message">
			<div class="adm-info-message-title"><?=GetMessage('B2CPL_ERROR_noPickup_TITLE')?></div>
				<?=GetMessage('B2CPL_ERROR_noPickup_DESCR')?>
			<div class="adm-info-message-icon"></div>
		  </div>
		</div>
	</td></tr></table>
	<?}

	function scanAndDelete(){
		$delivs = self::getOldDeliveries();
		$arOldDelivs = array();
		while($delivery = $delivs->Fetch())
			$arOldDelivs[$delivery['ID']] = $delivery;

		foreach($arOldDelivs as $deliveryId => $delivery){
			$res = Bitrix\Sale\Delivery\Services\Table::delete($deliveryId);
			$arOldDelivs[$delivery['ID']]['SUCCESS'] = $res->isSuccess();
			if(!$res->isSuccess())
				$arOldDelivs[$delivery['ID']]['ERROR']   = $res->getErrorMessages();
		}
		
		echo json_encode(self::zajsonit($arOldDelivs));
	}

	// ������
	public static function select($arOrder=array("ORDER_ID","DESC"),$arFilter=array(),$arNavStartParams=array()){return Cb2cplOrder::select($arOrder,$arFilter,$arNavStartParams);} // �������
	public static function Delete($orderId){return Cb2cplOrder::Delete($orderId);} // �������� ���������� � ������
	public static function CheckRecord($orderId){return Cb2cplOrder::CheckRecord($orderId);} // �������� ������� ������ ��� ������
}
?>

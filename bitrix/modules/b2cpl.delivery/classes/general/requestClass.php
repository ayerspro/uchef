<?
IncludeModuleLangFile(__FILE__);

class Cb2cplRequest extends Cb2cplHelper{

	public static $checkStatusPeriod = 600;

	public static $module_id = 'b2cpl.delivery';

	private static $updated = false;

	function showForm(){//Ћтображение формы
		if(!cmodule::includeModule('sale'))
			return false;
		if(
			strpos($_SERVER['PHP_SELF'], "/bitrix/admin/sale_order_detail.php")!==false ||
			strpos($_SERVER['PHP_SELF'], "/bitrix/admin/sale_order_view.php")!==false
		){
			$orderinfo=CSaleOrder::getById($_REQUEST['ID']);

			if(
				COption::GetOptionString(self::$MODULE_ID,'showInOrders','Y') == 'N' &&
				!self::getDeliveryId($orderinfo['DELIVERY_ID'])
			)
				return;

			include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/tools/".self::$MODULE_ID."/orderDetail.php");
		}elseif(strpos($_SERVER['PHP_SELF'], "/bitrix/admin/sale_order_new.php")!==false){
			$orderSelf = CSaleOrder::GetById($_REQUEST['ID']);
			CJSCore::Init(array('jquery'));
			echo "
			<style>#B2CPL_warning{display:none;color:red;}</style>
			<script type='text/javascript'>	
				var B2CPL_checkPS = {
					oldPS: '{$orderSelf['PAY_SYSTEM_ID']}',
					check: function(){
						if(\$('#B2CPL_warning').length==0) \$('#DELIVERY_ID').after('<br><span id=\"B2CPL_warning\">".GetMessage("B2CPL_OREDEREDIT_paysysChenged")."</span>');
						if(\$('#DELIVERY_ID').val().indexOf('b2cpl:')===0 && \$('#PAY_SYSTEM_ID').val()!=B2CPL_checkPS.oldPS)
							\$('#B2CPL_warning').css('display','inline');
						else
							\$('#B2CPL_warning').css('display','none');
					},
				};	
				\$(document).ready(function(){
					\$('#DELIVERY_ID').on('change',B2CPL_checkPS.check);
					\$('#PAY_SYSTEM_ID').on('change',B2CPL_checkPS.check);
					B2CPL_checkPS.check();
					if(typeof BX !== 'undefined' && BX.addCustomEvent)
						BX.addCustomEvent('onAjaxSuccess',B2CPL_checkPS.check);
				});
			</script>";
		}
	}

	function checkPayed($params){
		$return = GetMessage('MAIN_NO');
		if(cmodule::includeModule('sale')){
			$order = CSaleOrder::GetById($params['oid']);
			if($order['PAYED'] == 'Y')
				$return = GetMessage('MAIN_YES');
		}
		echo $return;
	}

	/*
		ќкспорт заказа
		== saveAndSend ==  == updtOrder ==  == ExportOrder ==  == getGoods ==
	*/

	function saveAndSend($params,$sendType=false){
		if(self::updtOrder($params)){
			$respond = self::ExportOrder($params['orderId'],$sendType);
			switch($respond){
				case "B2CPL_OK" : $answer = ($params['action']) ? "B2CPL_OK" : true; break;
				case "noAuth"   : $answer = GetMessage('B2CPL_EXPORT_REZ_noAuth'); break;
				case "noVals"   : $answer = GetMessage('B2CPL_EXPORT_REZ_noVals'); break;
				case "noGoods"  : $answer = GetMessage('B2CPL_EXPORT_REZ_noGoods'); break;
				default         : $answer = $respond; break;
			}
		}else
			$answer = GetMessage('B2CPL_EXPORT_REZ_noUpdt');
		if($params['action'])
			echo $answer;
		else
			return $answer;
	}

	function updtOrder($params){ // сохраняем информацию о заЯвке в Ѓ„, возвращаем ее ID
		$params=self::zaDEjsonit($params);
		$arNeed = array('mphone','fio');
		if(strpos($params['pack'], "call") !== 0){
			$arNeed = array_merge($arNeed, array('index','city','address'));
		}
		foreach($arNeed as $need)
			if(!$params[$need]){echo GetMessage('B2CPL_UPDORD_'.$need); return false;}
		if(!$params['orderId']){echo GetMessage('B2CPL_UPDORD_orderif'); return false;}
		if(!$params['status'])
			$status='NEW';
		$orderId=$params['orderId'];
		unset($params['orderId']);
		unset($params['action']);

		if($newId=self::Add(array('ORDER_ID'=>$orderId,'PARAMS'=>serialize($params),'STATUS'=>$status)))
			return $newId;
		else{
			echo "CANTUPDATE";
			return false;
		}
	}

	public function ExportOrder($ID,$sendType=false) {
		if (CModule::IncludeModule('sale') && ($arOrder = CSaleOrder::GetByID($ID))) {
			$ID = intval($ID);
			$path = '/' . COption::GetOptionString('main', 'upload_dir', 'upload') . '/b2cpl/export_' . (md5(uniqid())) . '.csv';
			$type = self::defineDelivery($arOrder['DELIVERY_ID']);
			$client = COption::GetOptionString(self::$MODULE_ID,'client',false);
			$key    = COption::GetOptionString(self::$MODULE_ID,'key',false);
			$defaultWeight = COption::GetOptionString(self::$MODULE_ID,'default_weight',1);

			if (!strlen($client) || !strlen($key))
				return 'noAuth';

			$arSite = CSite::GetByID($arOrder['LID'])->Fetch();
			if (!strlen($arSite['SERVER_NAME']))
				$arSite['SERVER_NAME'] = $_SERVER['SERVER_NAME'];

			$orVals = self::GetByOI($ID);
			if(!$orVals)
				return 'noVals';
			$orParams = unserialize($orVals['PARAMS']);
			preg_match('/(\d{4})-(\d{2})-(\d{2})/', $arOrder['DATE_INSERT'],$orDate);

			$payed = ($arOrder['PAYED'] == 'Y' || in_array($type,array('pr2','pr3','pr5')));
			if(!$payed && $sendType == 'create'){
				$paySys = unserialize(COption::GetOptionString(self::$MODULE_ID,'paySystems','a:{}'));
				$payed = in_array($arOrder['PAY_SYSTEM_ID'],$paySys);
			}

			$arBaseLine = array(
				'ORDER_ID'  => $ID,
				'CLIENT_ID' => $arOrder['USER_ID'],
				'DATE'      => $orDate[3].".".$orDate[2].".".$orDate[1],
				'F_ZIP'     => $orParams['index'],
				'F_CITY'    => str_replace(';', ',', $orParams['city']),
				'F_ADDRESS' => str_replace(';', ',', $orParams['address']),
				'F_NAME'    => str_replace(';', ',', $orParams['fio']),
				'F_PHONE'   => $orParams['mphone'],
				'F_UPHONE'  => $orParams['uphone'],
				'F_EMAIL'   => $orParams['email'],
				'WEIGHT'    => 0,
				'DELIVERY_PRICE' => $arOrder['PRICE_DELIVERY'],
				'DELIVERY_PRICE_BALANCE' => $payed ? 0 : $arOrder['PRICE_DELIVERY'],
				'DELIVERY_TYPE' => '',
				'AVIA'       => '',
				'ASSESSED'   => $arOrder['PRICE'] - $arOrder['PRICE_DELIVERY'],
				'DATE_DELIV' => $orParams['issue'],
				'INTERVAL'   => GetMessage("B2CPL_FORM_term_".$orParams['term']),
				'PACK'       => GetMessage("B2CPL_FORM_pack_".$orParams['pack']),
			);
			$productFields = array(
				'ARTNUMBER'    => '',
				'PRODUCT_NAME' => '',
				'QUANTITY' => 1,
				'PRICE' => '',
				'PRICE_BALANCE' => '',
				'PRODUCT_WEIGHT' => 0,
			);

			if((COption::GetOptionString(self::$MODULE_ID,'assessed_no_include','N') == 'Y' && !in_array($type,array('pr1','pr3','pr4','pr5'))) || $type == 'pr2') // NO ASSESSED
				unset($arBaseLine['ASSESSED']);
			if(!$arBaseLine['F_UPHONE']) unset($arBaseLine['F_UPHONE']);
			if(!$arBaseLine['F_EMAIL'])  unset($arBaseLine['F_EMAIL']);
			if(!$arBaseLine['DATE_DELIV']){unset($arBaseLine['DATE_DELIV']);unset($arBaseLine['INTERVAL']);}
			if($type != 'kurer'){unset($arBaseLine['INTERVAL']);unset($arBaseLine['DATE_DELIV']);}

			$arNeed = array('F_NAME', 'F_PHONE');
			if(strpos($orParams["pack"], "call") !== 0){
				$arNeed[] = 'F_ZIP';
			}
			foreach($arNeed as $need)
				if(!$arBaseLine[$need])
					return GetMessage("B2CPL_EXPORT_UNGIVEN").GetMessage('B2CPL_EXPORT_'.$need);

			//@FIXME
			if ($type === 'pickup'){
				$arPropPVZ = CSaleOrderProps::GetList(array(), array("PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"], "CODE" => "B2CPL_PVZ"), false, false, array("ID"))->GetNext();
				$arPropValue = CSaleOrderPropsValue::GetList(array(), array("ORDER_PROPS_ID" => $arPropPVZ["ID"], "ORDER_ID" => $arOrder["ID"]))->GetNext();
				$pvzID = $arPropValue["VALUE"];
				if(!$pvzID && $_REQUEST["B2CPL_pvz"]){
					$pvzID = $_REQUEST["B2CPL_pvz"];
				}
				$arPvz = self::getPVZ($pvzID);
				$arBaseLine['DELIVERY_TYPE'] = $arPvz['id'];
				$arBaseLine['F_ADDRESS'] = str_replace(';', ',', $arPvz['addr']);
			}elseif ($type == 'kurer')
				$arBaseLine['DELIVERY_TYPE'] = GetMessage('B2CPL_DELIVERY_KURJER_TITLE');
			elseif (strpos($type, 'pr') !== false) {
				$arBaseLine['ASSESSED'] = $arOrder['PRICE'];// - $arOrder['PRICE_DELIVERY'];
				$arBaseLine['DELIVERY_TYPE'] = str_replace('pr', GetMessage('B2CPL_DELIVERY_PR'), $type);
			}
			if (
				strpos(ToLower($arBaseLine['F_CITY'], $arSite['LANGUAGE_ID']), GetMessage('B2CPL_SAKHALINSK_1'))!==false &&
				strpos(ToLower($arBaseLine['F_CITY'], $arSite['LANGUAGE_ID']), GetMessage('B2CPL_SAKHALINSK_2'))!==false
			) {
				//in Yuzhno-Sakhalinsk only Avia
				$arBaseLine['AVIA'] = GetMessage('B2CPL_EXPORT_AVIA');
			}

			//get order cart
			$goods = self::getGoods($ID,$defaultWeight,$payed);
			if(!count($goods['GOODS']))
				return 'noGoods';

			$arBaseLine['WEIGHT'] = $goods['WEIGHT'];
			if(array_key_exists('ASSESSED',$arBaseLine) && !$arBaseLine['ASSESSED'])
				$arBaseLine['ASSESSED'] = $goods['PRICE'];

			//write
			$exportStr = '';
			foreach (array_merge($arBaseLine,$productFields) as $k => $v)
				$exportStr .= GetMessage('B2CPL_EXPORT_' . $k).';';
			$exportStr = rtrim($exportStr, ';');
			$exportStr .= "\r\n";
			$exportStr .= implode(';', $arBaseLine).";";
			$fPased = false;
			foreach($goods['GOODS'] as $ind => $good){
				if($fPased){
					$exportStr .= $arBaseLine['ORDER_ID'].";";
					for($i=0;$i<(count($arBaseLine)-1);$i++)
						$exportStr .= ';';
				}
				$exportStr .= implode(';', $good)."\r\n";
				$fPased = true;
			}

			$exportStr = $GLOBALS['APPLICATION']->ConvertCharset($exportStr, SITE_CHARSET, 'UTF-8');
			// $exportStr = $GLOBALS['APPLICATION']->ConvertCharset($exportStr, SITE_CHARSET, 'windows-1251'); // check

			if (RewriteFile($_SERVER['DOCUMENT_ROOT'] . $path, $exportStr)) {
				$arQuery = array(
					'func' => 'upload',
					'client' => $client,
					'key' => $key,
					'report' => 1,
					'stickers' => 1,
					'file' => urlencode('http://' . $arSite['SERVER_NAME'] . $path)
				);
				$return = '';

				Cb2cplLogs::writeLog($ID, GetMessage("B2CPL_LOG_SEND_REQUEST"));
				$data = self::Query($arQuery);
				$data = $GLOBALS['APPLICATION']->ConvertCharset($data, 'windows-1251', SITE_CHARSET);
				$arData = CUtil::JsObjectToPhp($data);
				if(is_array($arData)){
					if (is_array($arData['packages']) && is_array($arData['packages'][0]))
						$arPackages = $arData['packages'][0];
					else
						$arPackages = array();
					if ($arData['cnt_ok']==1) {
						Cb2cplLogs::writeLog($ID, GetMessage("B2CPL_LOG_ORDER_CREATED"), "success");
						Cb2cplOrder::AddOrder($ID, array(
														'STICKERS_URL' => $arData['file_stickers'],
														'B2CCODE' => $arPackages['codeb2c'],
														'STATUS' => 'OK'
													));
						if(
							strpos($arBaseLine["PACK"], "call") === 0 &&
							COption::GetOptionString(self::$module_id, "needSync", "Y") === "Y"
						){
							CAgent::AddAgent('Cb2cplDelivery::CallStatusOrderAgent('.$ID.');', self::$module_id, 'N', self::$checkStatusPeriod, '', 'Y', date($GLOBALS['DB']->DateFormatToPHP(FORMAT_DATETIME), time()));
						}
						CAgent::AddAgent('Cb2cplDelivery::StatusOrderAgent('.$ID.');', self::$module_id, 'N', self::$checkStatusPeriod, '', 'Y', date($GLOBALS['DB']->DateFormatToPHP(FORMAT_DATETIME), time()));
						$return = 'B2CPL_OK';
					}elseif(strlen($arData['text_error'])) {
						Cb2cplLogs::writeLog($ID, $arData['text_error'], "error");
						$return = $arData['text_error'];
					}elseif($arData['cnt_error'] > 0){
						$return = '';
						foreach($arData['packages'] as $package)
							$return.=$package["errtxt"]." ";
						if(!$type && $orParams["pack"] != GetMessage("B2CPL_FORM_pack_callIM")){
							$return = GetMessage("B2CPL_DELIVERY_wrong_pack") .", ". $return;
						}
						Cb2cplLogs::writeLog($ID, $return, "error");
					}
				}
				unlink($_SERVER['DOCUMENT_ROOT'] . $path);
				return $return;
			}
		}
	}

	function getGoods($oId,$defWei=0,$paid=false){
		$hasIblock = cmodule::includemodule('iblock');
		$hasCatalog = cmodule::includemodule('catalog');
		$arGoods = array(
			'GOODS'  => array(),
			'WEIGHT' => 0,
			'PRICE'  => 0,
			'FAIL'   => array(),
		);
		$rsBasket = CSaleBasket::GetList(array(),array('ORDER_ID'=>$oId));
		$optARticul = COption::GetOptionString(self::$MODULE_ID,'articul',"ARTNUMBER");
		$arIDs = array();
		while($arBasket=$rsBasket->Fetch()){
			if($arBasket['WEIGHT'] <= 0)
				$arBasket['WEIGHT'] = $defWei;
			$arBasket['WEIGHT'] *= $arBasket['QUANTITY'];
			$articul = false;
			if($optARticul && $hasIblock){
				$gd = CIBlockElement::GetList(array(),array('ID'=>$arBasket['PRODUCT_ID'],'LID'=>$arBasket['LID']),false,false,array('ID','PROPERTY_'.$optARticul))->Fetch();
				if($gd && $gd["PROPERTY_{$optARticul}_VALUE"])
					$articul = $gd["PROPERTY_{$optARticul}_VALUE"];
			}

			$arIDs[$arBasket['PRODUCT_ID']] = true;

			$bPrice = $arBasket['PRICE'];
			if($bPrice == 0 && $hasCatalog){
				$price = CCatalogProduct::GetOptimalPrice($arBasket['PRODUCT_ID'],1);
				if($price)
					$bPrice = $price['RESULT_PRICE']['DISCOUNT_PRICE'];
			}
			$arGoods['GOODS'][$arBasket['PRODUCT_ID']] = array(
				'ARTICUL' => ($articul)?$articul:$arBasket['PRODUCT_ID'],
				'NAME'    => str_replace(';', ',', $arBasket['NAME']),
				'QNT'     => intval($arBasket['QUANTITY']),
				'PRICE'   => ($bPrice) ? $bPrice : 1, // чтобы не орали на бесплатную
				'PRICETP' => ($paid) ? 0 : $arBasket['PRICE'],
				'WEIGHT'  => $arBasket['WEIGHT'] / 1000,
			);
			if(!$articul)
				$arGoods['FAIL'][$arBasket['PRODUCT_ID']] = $arBasket['NAME'];
			$arGoods['WEIGHT'] += $arBasket['WEIGHT'] / 1000;
			$arGoods['PRICE'] += $bPrice * intval($arBasket['QUANTITY']);
		}

		$propWeight = COption::GetOptionString(self::$MODULE_ID,'weight',false);
		if(!empty($propWeight)){
			$gWeights = self::getGoodsPropWeight($arIDs,$propWeight);
			foreach($gWeights as $id => $params)
				if(array_key_exists($id,$arGoods['GOODS']))
					$arGoods['GOODS'][$id]['WEIGHT'] = $params['WEIGHT'] / 1000;
		}

		return $arGoods;
	}

	/* 
		ќкспорт по статусу и при создании заказа
		== OnSaleBeforeStatusOrderHandler ==  == getOrderFields ==  == checkFields ==
	*/

	public function OnSaleBeforeStatusOrderHandler($ID, $val){
		//hack для корректной работы нового магазина с включённым режимом совместимости.
		if(COption::GetOptionString("main", "~sale_converted_15", "N") === "Y"){
			return;
		}
		if ($ID>0 && strlen($val) && COption::GetOptionString(self::$MODULE_ID,'howSend','F') == 'S'){
			$arOrder = CSaleOrder::GetByID($ID);
			$delType = self::defineDelivery($arOrder['DELIVERY_ID']);
			if($delType && self::isActive()){
				$uploadStatus = COption::GetOptionString(self::$MODULE_ID,'order_status_for_upload',false);
				if ($val == $uploadStatus){
					$noErrors = true;
					$fields = self::getOrderFields($ID);
					if($delType == 'pickup' && $fields[1]['B2CPL_PVZ'])
						$fields[0]['address'] = $fields[1]['B2CPL_PVZ'];
					$pack = COption::GetOptionString(self::$MODULE_ID, 'defaultPack', '');
					if ( !empty($pack) ) {
						$fields[0]['pack'] = $pack;
					}
					$noErrors = self::checkFields($fields[0], (strpos($pack, "call") === 0));
					if ($noErrors === true){
						$respond = self::saveAndSend(array_merge(array('orderId' => $ID),self::zajsonit($fields[0])),'status');
						if($respond !== true)
							$noErrors = array($respond);
					}

					if($noErrors !== true){
						$GLOBALS['APPLICATION']->ThrowException('<br/><font color="red">'.implode(". ",$noErrors).'</font>');
						return false;
					}
				}
			}
		}
	}

	/**
	 * @param Bitrix\Main\Event $entity
	 *
	 * @return bool|void
	 */
	public function OnSaleOrderSavedNew($entity) {
		if(self::$updated){
			return;
		}
		$opt = COption::GetOptionString(self::$MODULE_ID, 'howSend', 'F');
		$entityFields = $entity->getFields();
		$status = $entityFields->getChangedValues();
		if ( $entity->isChanged()){
			$arOrder = $entityFields->getValues();
			$delType = self::defineDelivery($arOrder['DELIVERY_ID']);
			if (
				($opt === "O" && $entity->isNew()) ||
				($opt === "S" && isset($status["STATUS_ID"]) && $status["STATUS_ID"] === COption::GetOptionString(self::$MODULE_ID, 'order_status_for_upload', false))
			) {
				$noErrors = true;
				$pack = COption::GetOptionString(self::$MODULE_ID, 'defaultPack', '');

				$fields = self::getOrderFields($arOrder["ID"], array(), ($pack === 'callIM'));
				if ( $delType == 'pickup' && $fields[1]['B2CPL_PVZ'] ) {
					$fields[0]['address'] = $fields[1]['B2CPL_PVZ'];
				}
				if ( !empty($pack) ) {
					$fields[0]['pack'] = $pack;
				}
				$noErrors = self::checkFields($fields[0], (strpos($pack, "call") === 0));
				if ( $noErrors === true ) {
					$respond = self::saveAndSend(array_merge(array('orderId' => $arOrder["ID"]), self::zajsonit($fields[0])), ($opt === "O" && $entity->isNew()) ? "create" : "status");
					if ( $respond !== true ) {
						$noErrors = array($respond);
					} else {
						self::$updated = true;
					}
				}

				if ( $noErrors !== true ) {
					$GLOBALS['APPLICATION']->ThrowException('<br/><font color="red">' . implode(". ", $noErrors) . '</font>');
					Cb2cplLogs::writeLog($arOrder["ID"], implode(".<br> ", $noErrors), "error");

					return false;
				}
			}
		}
	}

	public function sendOnOrderCreate($oId,$arOrder){
		if(!$oId) return;
		if(COption::GetOptionString("main", "~sale_converted_15", "N") === "Y"){
			return;
		}
		$delType = self::defineDelivery($arOrder['DELIVERY_ID']);
		if(
			$delType &&
			COption::GetOptionString(self::$MODULE_ID,'howSend','F') == 'O'
		){
			$fields = self::getOrderFields($oId);
			if($delType == 'pickup' && $fields[1]['B2CPL_PVZ'])
				$fields[0]['address'] = $fields[1]['B2CPL_PVZ'];
			$pack = COption::GetOptionString(self::$MODULE_ID, 'defaultPack', '');
			if ( !empty($pack) ) {
				$fields[0]['pack'] = $pack;
			}
			$noErrors = self::checkFields($fields[0], (strpos($pack, "call") === 0));
			if ($noErrors === true)
				$respond = self::saveAndSend(array_merge(array('orderId' => $oId),self::zajsonit($fields[0])),'create');
		}
	}

	public function getOrderFields($orderId, $ordrVals = array(), $callIM = false) {
		$arFormProps = array(
			'fio',
			'mphone',
			'index',
			'city',
			'address',
			'uphone',
			'email',
		);

		$orderProps=CSaleOrderPropsValue::GetOrderProps($orderId);
		$orderPropsMas = array();
		while($orderProp=$orderProps->Fetch())
			$orderPropsMas[$orderProp['CODE']] = $orderProp['VALUE'];
		foreach($arFormProps as $prop){
			if(!$ordrVals[$prop] || $prop=='city'){
				$propCode = COption::GetOptionString(self::$MODULE_ID,$prop,'');
				if($prop!='city')
					$ordrVals[$prop] = ($propCode)?$orderPropsMas[$propCode]:false;
				elseif($propCode){
					$orderPropsMas[$propCode] = CSaleLocation::GetByID($orderPropsMas[$propCode]);
					if($orderPropsMas[$propCode])
						$ordrVals[$prop] = $orderPropsMas[$propCode]['CITY_NAME_LANG'];
				}
			}
		}
		return array($ordrVals,$orderPropsMas);
	}

	private function checkFields(&$fields, $call = false){
		$arReq = array('mphone' => 1);
		if ( !$call ) {
			$arReq = array_merge($arReq, array('city' => 1, 'address' => 1, 'index' => 1, 'fio' => 1));
		}
		$arErrFields = array();
		foreach($fields as $code => $field)
			if(!$field){
				if(array_key_exists($code,$arReq))
					$arErrFields[]=GetMessage('B2CPL_FORM_MESS_FILL')." ".GetMessage('B2CPL_FORM_'.$code);
				else
					unset($fields[$field]);
			}
			elseif(array_key_exists($code,$arReq))
				unset($arReq[$code]);
		if(count($arReq))
			foreach($arReq as $code => $nothing)
				$arErrFields[]=GetMessage('B2CPL_FORM_MESS_FILL')." ".GetMessage('B2CPL_FORM_'.$code);
		return (count($arErrFields)) ? $arErrFields : true;
	}


	/*
		‘тикеры из заказа
		
		== OnAdminContextMenuShowHandler ==  == getStickers ==  == displayPrintStickers ==  == OnBeforePrologHandler ==
	*/

	public function OnAdminContextMenuShowHandler(&$items) {
		$ID = intval($_REQUEST['ID']);
		if ($ID>0 && $GLOBALS['APPLICATION']->GetCurPage()=='/bitrix/admin/sale_order_detail.php') {
			if (($arB2CPL = Cb2cplOrder::GetOrder($ID)) && strlen($arB2CPL['STICKERS_URL'])) {
				$items[] = array('ICON' => 'b2cpl_stickers',
								'TEXT' => GetMessage('B2CPL_DELIVERY_DOWNLOAD_STICKER'),
								'TITLE' => GetMessage('B2CPL_DELIVERY_DOWNLOAD_STICKER_TITLE'),
								'LINK' => $arB2CPL['STICKERS_URL'],
								);
			}
		}
	}

	function displayPrintStickers(&$list){ // действие длЯ печати актов
		if (!empty($list->arActions))
			CJSCore::Init(array('B2CPL_PRINTSTICKERS'));
		if($GLOBALS['APPLICATION']->GetCurPage() == "/bitrix/admin/sale_order.php")
			$list->arActions['B2CPL_PRINTSTICKERS'] = GetMessage("B2CPL_SIGN_PRINTSTICKERS");
	}

	function OnBeforePrologHandler(){ // нажатие на печать актов
		if(!array_key_exists('action', $_REQUEST) || !array_key_exists('ID', $_REQUEST) || $_REQUEST['action'] != 'B2CPL_PRINTSTICKERS')
			return;
		$path = self::getStickers($_REQUEST['ID']);
		if($path)
			echo "<script type='text/javascript'>window.open('{$path}','_blank');</script>";
	}

	function getStickers($ids){
		$client = COption::GetOptionString(self::$MODULE_ID,'client',false);
		$key    = COption::GetOptionString(self::$MODULE_ID,'key',false);

		$orders = array("codes"=>array());
		$bdOrders = Cb2cplOrder::select(array(),array('ORDER_ID'=>$ids));
		while($element=$bdOrders->Fetch())
			if($element['B2CCODE'])
				$orders["codes"][] = $element['B2CCODE'];

		$arQuery = array(
			'func'   => 'labels',
			'client' => $client,
			'key'    => $key,
			'codes'  => CUtil::PhpToJSObject($orders),
		);
		$data = json_decode(self::Query($arQuery),true);
		return ($data["file_stickers"])?$data["file_stickers"]:false;
	}

	// свЯзки
	function Add($Data){
		if(class_exists('Cb2cplOrder')) return Cb2cplOrder::Add($Data);
	}
	function GetOrder($orderId){
		if(class_exists('Cb2cplOrder')) return Cb2cplOrder::GetOrder($orderId);
	}
	function GetByOI($orderId){
		if(class_exists('Cb2cplOrder')) return Cb2cplOrder::GetByOI($orderId);
	}
	function StatusOrderAgent($orderId){
		if(class_exists('Cb2cplDelivery')) return Cb2cplDelivery::StatusOrderAgent($orderId);
	}

	public static function getStatusLog(){
		$arProtokol = Cb2cplLogs::getLog($_POST["orderId"]);
		$html = "";
		if ( count($arProtokol) ) {
			foreach ($arProtokol as $key => $action) {
				$error = ($action["TYPE"] === "ERROR" ? "<span style='color:#ff0000;'>".GetMessage("B2CPL_DELIVERY_OSIBKA")."</span> " : ($action["TYPE"] === "SUCCESS" ? "<span style='color:#00ff4a;'?>".GetMessage("B2CPL_DELIVERY_USPEH")."</span> " : ""));
				$html .= ($key === 0) ? GetMessage("B2CPL_DELIVERY_POSLEDNAA_ZAPISQ") : "";
				$html .= "<p>[{$action["TIME"]}] {$error}{$action["MESSAGE"]}</p>";
				$html .= ($key === 0) ? "<hr>" : "";
			}
		} else {
			$html .= GetMessage("B2CPL_DELIVERY_OTPRAVKI_ZAKAZA_NE_O");
		}

		return $html;
	}
}

?>

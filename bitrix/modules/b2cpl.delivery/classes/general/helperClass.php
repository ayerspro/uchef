<?
class Cb2cplHelper{
	static $MODULE_ID = 'b2cpl.delivery';

	static $cachePath = '/delivery/b2cpl/';
	//�������
	static $tmpLogFile = false;
	function toLog($wat,$sign=''){
		if($sign) $sign.=" ";
		if(!self::$tmpLogFile){
			self::$tmpLogFile = fopen($_SERVER['DOCUMENT_ROOT'].'/B2CPLog.txt','w');
			fwrite(self::$tmpLogFile,"\n\n".date('H:i:s d.m')."\n");
		}
		fwrite(self::$tmpLogFile,$sign.print_r($wat,true)."\n");
	}
	// ������
	function Query($arQuery) {
		return QueryGetData('is.b2cpl.ru', '80', '/portal/client_api.ashx', http_build_query($arQuery), $error_number = 0, $error_text = '', 'GET');
	}

	function QueryPost($arQuery){
		return QueryGetData('is.b2cpl.ru', '80', '/portal/client_api_post.ashx', json_encode($arQuery), $error_number = 0, $error_text = '', 'POST', "", "json");
	}
	//���������
	function zajsonit($handle){
		if(LANG_CHARSET !== 'UTF-8'){
			if(is_array($handle))
				foreach($handle as $key => $val){
					unset($handle[$key]);
					$key=self::zajsonit($key);
					$handle[$key]=self::zajsonit($val);
				}
			else
				$handle=$GLOBALS['APPLICATION']->ConvertCharset($handle,LANG_CHARSET,'UTF-8');
		}
		return $handle;
	}
	function zaDEjsonit($handle){
		if(LANG_CHARSET !== 'UTF-8'){
			if(is_array($handle))
				foreach($handle as $key => $val){
					unset($handle[$key]);
					$key=self::zaDEjsonit($key);
					$handle[$key]=self::zaDEjsonit($val);
				}
			else
				$handle=$GLOBALS['APPLICATION']->ConvertCharset($handle,'UTF-8',LANG_CHARSET);
		}
		return $handle;
	}

	function writeToDebugCache($data){
		$cache = new CPHPCache();
		$cache_time = 600;

		$time = time();
		$cache_id = "delivery_cache|$time";

		$cache->StartDataCache($cache_time, $cache_id, "/delivery/debug_cache");
		$cache->EndDataCache(array("DATA" => $data, "TIME" => $time));
	}

	// �������� ���������� ��
	function isActive(){
		if(!cmodule::includeModule("sale")) return false;
			if(self::isConverted()){
				$dS = Bitrix\Sale\Delivery\Services\Table::getList(array(
					 'order'  => array('SORT' => 'ASC', 'NAME' => 'ASC'),
					 'filter' => array('CODE' => 'b2cpl')
				))->Fetch();
			}else
				$dS = CSaleDeliveryHandler::GetBySID('b2cpl')->Fetch();
		return ($dS && $dS['ACTIVE'] == 'Y');
	}

	// �������� ����������� ��������
	function isConverted(){
		return (COption::GetOptionString("main","~sale_converted_15",'N') == 'Y');
	}

	// �������� ������
	function controlProps($mode=1){//1-add/update, 2-delete
		if(!CModule::IncludeModule("sale"))
			return false;
		$arProps = array(
			array(// �������� ��� ���������� ������� / ��������� ��������
				'CODE'  => "B2CPL_DATEDELIV",
				'NAME'  => GetMessage('B2CPL_PROP_DATEDELIV_name'),
				'DESCR' => GetMessage('B2CPL_PROP_DATEDELIV_descr')
			),
			array(// �������� ������, ���� ������� ���
				'CODE'  => "B2CPL_PVZ",
				'NAME'  => GetMessage('B2CPL_PROP_PVZ_name'),
				'DESCR' => GetMessage('B2CPL_PROP_PVZ_descr')
			),
		);
		foreach($arProps as $prop)
			self::handleProp($prop,$mode);
	}

	protected function handleProp($arProp,$mode){
		$tmpGet=CSaleOrderProps::GetList(array("SORT" => "ASC"),array("CODE" => $arProp['CODE']));
		$existedProps=array();
		while($tmpElement=$tmpGet->Fetch())
			$existedProps[$tmpElement['PERSON_TYPE_ID']]=$tmpElement['ID'];
		if($mode=='1'){
			$return = true;
			$tmpGet = CSalePersonType::GetList(Array("SORT" => "ASC"), Array());
			$allPayers=array();
			while($tmpElement=$tmpGet->Fetch())
				$allPayers[]=$tmpElement['ID'];

			foreach($allPayers as $payer){
				$tmpGet = CSaleOrderPropsGroup::GetList(array("SORT" => "ASC"),array("PERSON_TYPE_ID" => $payer),false,array('nTopCount' => '1'));
				$tmpVal=$tmpGet->Fetch();
				$arFields = array(
				   "PERSON_TYPE_ID" => $payer,
				   "NAME" => $arProp['NAME'],
				   "TYPE" => "TEXT",
				   "REQUIED" => "N",
				   "DEFAULT_VALUE" => "",
				   "SORT" => 100,
				   "CODE" => $arProp['CODE'],
				   "USER_PROPS" => "N",
				   "IS_LOCATION" => "N",
				   "IS_LOCATION4TAX" => "N",
				   "PROPS_GROUP_ID" => $tmpVal['ID'],
				   "SIZE1" => 10,
				   "SIZE2" => 1,
				   "DESCRIPTION" => $arProp['DESCR'],
				   "IS_EMAIL" => "N",
				   "IS_PROFILE_NAME" => "N",
				   "IS_PAYER" => "N",
				   "IS_FILTERED" => "Y",
				   "IS_ZIP" => "N",
				   "UTIL" => "Y"
				);
				if(!array_key_exists($payer,$existedProps))
					if(!CSaleOrderProps::Add($arFields))
						$return = false;
			}
			return $return;
		}
		if($mode=='2'){
			foreach($existedProps as $existedPropId)
				if (!CSaleOrderProps::Delete($existedPropId))
					echo "Error delete CNTDTARIF-prop id".$existedPropId."<br>";
		}
	}

	// �������� ���� ������� �� ��������
	function getGoodsPropWeight($arGoods,$propWeight){
		if(!$propWeight)
			$propWeight = COption::GetOptionString(self::$MODULE_ID,'weight',false);
		$arGoods = self::getGoodsProp($arGoods,$propWeight);

		return $arGoods;
	}

	function getGoodsProp($arGoods,$prop){
		if(!CModule::IncludeModule('iblock')) return false;
		$arProductIDByIBID = array();
		$IDs = array_keys($arGoods);

		$rsList = CIBlockElement::GetList(false,array('ID'=>$IDs),false,false,array('IBLOCK_ID','ID'));
		while ($arItem = $rsList->Fetch())
			$arProductIDByIBID[$arItem['IBLOCK_ID']][] = $arItem['ID'];
		if(!empty($arProductIDByIBID))
			foreach($arProductIDByIBID as $IBLOCK_ID => $IDs){
				$rsList = CIBlockElement::GetList(false,array('IBLOCK_ID'=>$IBLOCK_ID,'ID'=>$IDs),false,false,array('ID','PROPERTY_'.$prop));
				while($arItem = $rsList->Fetch())
					$arGoods[$arItem['ID']] = floatval(str_replace(',', '.', $arItem['PROPERTY_' . trim($prop).'_VALUE']));
			}
		return $arGoods;
	}

	// ��������� id ����� ��������
	function getDeliveryId($profile,$sep=':'){
		$profiles = array();
		if(self::isConverted()){
			$dTS = Bitrix\Sale\Delivery\Services\Table::getList(array(
				 'order'  => array('SORT' => 'ASC', 'NAME' => 'ASC'),
				 'filter' => array('CODE' => 'b2cpl:'.$profile)
			));
			while($dPS = $dTS->Fetch())
				$profiles[]=$dPS['ID'];
		}else
			$profiles = array('b2cpl'.$sep.$profile);
		return $profiles;
	}

	function defineDelivery($id){
		if(self::isConverted() && strpos($id,':') === false){
			$dTS = Bitrix\Sale\Delivery\Services\Table::getList(array(
				 'order'  => array('SORT' => 'ASC', 'NAME' => 'ASC'),
				 'filter' => array('ID' => $id)
			))->Fetch();
			$delivery = $dTS['CODE'];
		}else
			$delivery = $id;
		$position = strpos($delivery,'b2cpl:');
		return ($position === 0) ? substr($delivery,6) : false;
	}

	public function getPVZForForm($zip, $weight = 1, $price = 0) {
		$obCache = new CPHPCache();
		$cacheTime = 86400;
		if ( $obCache->InitCache($cacheTime, 'B2CPL_PVZ_For_Form', serialize(array($zip, $weight, $price))) ) {
			$data = $obCache->GetVars();
		} else {
			$obCache->StartDataCache($cacheTime, 'B2CPL_PVZ_For_Form', serialize(array($zip, $weight, $price)));

			$data = array();
			$arParams = array(
				'client'  => COption::GetOptionString(self::$MODULE_ID, 'client', false),
				'key'     => COption::GetOptionString(self::$MODULE_ID, 'key', false),
				'func'    => 'tarif',
				'type'    => '-post',
				'price'   => $price,
				'weight'  => $weight,
				'zip'     => (int) $zip,
				'allpost' => 0,
				'all_pvz' => 0,
			);

			$query = self::Query($arParams);
			$query = $GLOBALS['APPLICATION']->ConvertCharset($query, 'windows-1251', SITE_CHARSET);
			$arData = CUtil::JsObjectToPhp($query);
			foreach ($arData["delivery_ways"] as $deliv) {
				$code = CUtil::translit($deliv[GetMessage('B2CPL_DELIVERY_CODE')], 'ru', array('replace_other' => '_', 'replace_space' => '_'));

				if ( strpos($code, "pvz") !== false ) {
					$add = array();
					foreach ($deliv as $key => $value){
						$add[CUtil::translit($key, 'ru', array('replace_other' => '_', 'replace_space' => '_'))] = $value;
					}
					$data[$code] = $add;
				}
			}

			if ( count($data) ) {
				$obCache->EndDataCache($data);
			} else {
				$obCache->AbortDataCache();
			}

		}

		return $data;
	}

	// ��������� ������ ���
	public function getPvz($keys = false){
		$cache = new CPHPCache;
		if ((!defined('B2CPL_NO_CACHE') || !B2CPL_NO_CACHE) && $cache->InitCache(86400,'B2CPL_PVZ', '/' . SITE_ID . self::$cachePath))
			$arPvz = $cache->GetVars();
		else
			$arPvz = CB2CPLOpts::syncPvz();
		if($keys){
			if(!is_array($keys))
				return (array_key_exists($keys,$arPvz)) ? $arPvz[$keys] : false;
			foreach($arPvz as $id => $vals)
				if(!in_array($id,$keys))
					unset($arPvz[$id]);
		}
		return $arPvz;
	}
}
?>

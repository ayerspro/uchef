CREATE TABLE IF NOT EXISTS `b_b2cpl_orders` (
  `ORDER_ID`     INT(11) NOT NULL,
  `B2CCODE`      VARCHAR(50)  DEFAULT NULL,
  `STICKERS_URL` VARCHAR(255) DEFAULT NULL,
  PARAMS         TEXT,
  STATUS         VARCHAR(6),
  UPTIME         VARCHAR(10),
  PRIMARY KEY (`ORDER_ID`)
);

CREATE TABLE IF NOT EXISTS `b_b2cpl_logs` (
  `ORDER_ID` INT                               NOT NULL,
  `TYPE`     ENUM ('ERROR', 'SUCCESS', 'INFO') NOT NULL,
  `TIME`     TIMESTAMP                         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MESSAGE`  TEXT                              NOT NULL
);

<?php

IncludeModuleLangFile(__FILE__);

class b2cpl_delivery extends CModule {

	var $MODULE_ID = "b2cpl.delivery";
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	public $PARTNER_NAME;
	public $PARTNER_URI;
	public $MODULE_GROUP_RIGHTS = 'N';
	public $NEED_MAIN_VERSION = '';
	public $NEED_MODULES = array('sale');

	public function __construct() {
		$arModuleVersion = array();

		$path = str_replace('\\', '/', __FILE__);
		$path = substr($path, 0, strlen($path) - strlen('/index.php'));
		include($path . '/version.php');

		if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
			$this->MODULE_VERSION = $arModuleVersion['VERSION'];
			$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		}

		$this->PARTNER_NAME = GetMessage('B2CPL_PARTNER_NAME');
		$this->PARTNER_URI = 'http://b2cpl.ru/';

		$this->MODULE_NAME = GetMessage('B2CPL_MODULE_NAME');
		$this->MODULE_DESCRIPTION = GetMessage('B2CPL_MODULE_DESCRIPTION');
	}

	function InstallFiles() {
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/images/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/images/".$this->MODULE_ID, true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/tools/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools/".$this->MODULE_ID, true, true);
		return true;
	}

	function UnInstallFiles()	{
		DeleteDirFilesEx("/bitrix/tools/".$this->MODULE_ID);
		DeleteDirFilesEx("/bitrix/images/".$this->MODULE_ID);
		return true;
	}

	function InstallEvents(){
		RegisterModuleDependences('sale', 'OnSaleBeforeStatusOrder', $this->MODULE_ID, 'Cb2cplRequest', 'OnSaleBeforeStatusOrderHandler');
		RegisterModuleDependences('sale', 'OnSaleOrderSaved', $this->MODULE_ID, 'Cb2cplRequest', 'OnSaleOrderSavedNew');
		RegisterModuleDependences('sale', 'onSaleDeliveryHandlersBuildList', $this->MODULE_ID, 'Cb2cplDelivery', 'Init');
		RegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepPersonType', $this->MODULE_ID, 'Cb2cplDelivery', 'OnSaleComponentOrderOneStepPersonType');
		RegisterModuleDependences('main', 'OnEpilog', $this->MODULE_ID, 'Cb2cplRequest', 'showForm');
		RegisterModuleDependences('main', 'OnAdminContextMenuShow', $this->MODULE_ID, 'Cb2cplRequest', 'OnAdminContextMenuShowHandler');
		RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", $this->MODULE_ID, "Cb2cplDelivery", "onOrderCreate");
		RegisterModuleDependences("main", "OnEndBufferContent", $this->MODULE_ID, "Cb2cplDelivery", "onBufferContent");
		RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepProcess", $this->MODULE_ID, "Cb2cplDelivery", "timePickerLoader",900);
		RegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepProcess', $this->MODULE_ID, 'Cb2cplDelivery', 'pvzLoader',900);
		RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepProcess", $this->MODULE_ID, "Cb2cplDelivery", "checkDepth");
		RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepFinal", $this->MODULE_ID, "Cb2cplRequest", "sendOnOrderCreate");
		// �������
		RegisterModuleDependences("main", "OnAdminListDisplay", $this->MODULE_ID, "Cb2cplRequest", "displayPrintStickers");
		RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "Cb2cplRequest", "OnBeforePrologHandler");
		return true;
	}

	function UnInstallEvents() {
		UnRegisterModuleDependences('sale', 'OnSaleBeforeStatusOrder', $this->MODULE_ID, 'Cb2cplRequest', 'OnSaleBeforeStatusOrderHandler');
		UnRegisterModuleDependences('sale', 'OnSaleOrderSaved', $this->MODULE_ID, 'Cb2cplRequest', 'OnSaleOrderSavedNew');
		UnRegisterModuleDependences('sale', 'onSaleDeliveryHandlersBuildList', $this->MODULE_ID, 'Cb2cplDelivery', 'Init');
		UnRegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepPersonType', $this->MODULE_ID, 'Cb2cplDelivery', 'OnSaleComponentOrderOneStepPersonType');
		UnRegisterModuleDependences('main', 'OnEpilog', $this->MODULE_ID, 'Cb2cplRequest', 'showForm');
		UnRegisterModuleDependences('main', 'OnAdminContextMenuShow', $this->MODULE_ID, 'Cb2cplRequest', 'OnAdminContextMenuShowHandler');
		UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", $this->MODULE_ID, "Cb2cplDelivery", "onOrderCreate");
		UnRegisterModuleDependences("main", "OnEndBufferContent", $this->MODULE_ID, "Cb2cplDelivery", "onBufferContent");
		UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepProcess", $this->MODULE_ID, "Cb2cplDelivery", "timePickerLoader");
		UnRegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepProcess', $this->MODULE_ID, 'Cb2cplDelivery', 'pvzLoader');
		UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepProcess", $this->MODULE_ID, "Cb2cplDelivery", "checkDepth");
		UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepFinal", $this->MODULE_ID, "Cb2cplRequest", "sendOnOrderCreate");
		 // �������
		UnRegisterModuleDependences("main", "OnAdminListDisplay", $this->MODULE_ID, "Cb2cplRequest", "displayPrintStickers");
		UnRegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "Cb2cplRequest", "OnBeforePrologHandler");
		Cb2cplHelper::controlProps(2);
		return true;
	}

	public function DoInstall() {
		if($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W')
			return;

		if(is_array($this->NEED_MODULES) && !empty($this->NEED_MODULES))
			foreach ($this->NEED_MODULES as $module)
				if (!IsModuleInstalled($module))
					$this->ShowForm('ERROR', GetMessage('B2CPL_NEED_MODULES', array('#MODULE#' => $module)));

		if (strlen($this->NEED_MAIN_VERSION) <= 0 || version_compare(SM_VERSION, $this->NEED_MAIN_VERSION) >= 0) {
			$this->InstallFiles();
			$this->InstallEvents();
			
			$GLOBALS['DB']->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/db/'.$GLOBALS['DBType'].'/install.sql');
			
			RegisterModule($this->MODULE_ID);
			$GLOBALS['APPLICATION']->IncludeAdminFile(GetMessage("B2CPL_INSTALL"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/step1.php");
		}else
			$this->ShowForm('ERROR', GetMessage('B2CPL_NEED_RIGHT_VER', array('#NEED#' => $this->NEED_MAIN_VERSION)));
	}

	public function DoUninstall() {
		if ($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W') {
			return;
		}

		if ($_REQUEST['step'] < 2) {
			$this->ShowDataSaveForm();
		} elseif ($_REQUEST['step'] == 2) {
			$this->UnInstallFiles();
			$this->UnInstallEvents();
			
			if ($_REQUEST['savedata'] != 'Y') {
				$GLOBALS['DB']->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/db/'.$GLOBALS['DBType'].'/uninstall.sql');
			}
			
			CAgent::RemoveModuleAgents($this->MODULE_ID);
			UnRegisterModule($this->MODULE_ID);
			$this->ShowForm('OK', GetMessage('MOD_UNINST_OK'));
		}
	}

	private function ShowForm($type, $message, $buttonName = '') {
		$keys = array_keys($GLOBALS);
		for ($i = 0; $i < count($keys); $i++) {
			if ($keys[$i] != 'i' && $keys[$i] != 'GLOBALS' && $keys[$i] != 'strTitle' && $keys[$i] != 'filepath') {
				global ${$keys[$i]};
			}
		}

		$PathInstall = str_replace('\\', '/', __FILE__);
		$PathInstall = substr($PathInstall, 0, strlen($PathInstall) - strlen('/index.php'));
		IncludeModuleLangFile($PathInstall . '/install.php');

		$APPLICATION->SetTitle(GetMessage('B2CPL_MODULE_NAME'));
		include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');
		echo CAdminMessage::ShowMessage(array('MESSAGE' => $message, 'TYPE' => $type));
		?>
		<?if ($mode == 'install'):?>
		<p><a href="sale_delivery_handler_edit.php?SID=b2cpl&amp;lang=<?= LANGUAGE_ID?>"><?= GetMessage('B2CPL_GOTO_SETTINGS')?></a></p>
		<?endif;?>
		<form action="<?= $APPLICATION->GetCurPage() ?>" method="get">
			<p>
				<input type="hidden" name="lang" value="<?= LANG ?>" />
				<input type="submit" value="<?= GetMessage('MOD_BACK') ?>" />
			</p>
		</form>
		<?
		include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
		die();
	}

	private function ShowDataSaveForm() {
		$keys = array_keys($GLOBALS);
		for ($i = 0; $i < count($keys); $i++) {
			if ($keys[$i] != 'i' && $keys[$i] != 'GLOBALS' && $keys[$i] != 'strTitle' && $keys[$i] != 'filepath') {
				global ${$keys[$i]};
			}
		}

		$PathInstall = str_replace('\\', '/', __FILE__);
		$PathInstall = substr($PathInstall, 0, strlen($PathInstall) - strlen('/index.php'));
		IncludeModuleLangFile($PathInstall . '/install.php');

		$APPLICATION->SetTitle(GetMessage('B2CPL_MODULE_NAME'));
		include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');
		?>
		<form action="<?= $APPLICATION->GetCurPage() ?>" method="get">
			<?= bitrix_sessid_post();?>
			<input type="hidden" name="lang" value="<?= LANG ?>" />
			<input type="hidden" name="id" value="<?= $this->MODULE_ID ?>" />
			<input type="hidden" name="uninstall" value="Y" />
			<input type="hidden" name="step" value="2" />
			<? CAdminMessage::ShowMessage(GetMessage('MOD_UNINST_WARN')) ?>
			<p><?echo GetMessage('MOD_UNINST_SAVE')?></p>
			 <p><input type="checkbox" name="savedata" id="savedata" value="Y" checked="checked" /><label for="savedata"><?echo GetMessage('MOD_UNINST_SAVE_TABLES')?></label><br /></p>
			<input type="submit" name="inst" value="<?echo GetMessage('MOD_UNINST_DEL');?>" />
		</form>
		<?
		include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
		die();
	}
}

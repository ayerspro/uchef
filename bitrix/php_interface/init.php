<?

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Service\GeoIp;
use Sotbit\Regions\Config\Option;
use Sotbit\Regions\Internals\RegionsTable;
use Sotbit\Regions\Location;

$baseDomain = 'yaeda.ru';

// Текущий регион
/*
$baseDomain = 'yaeda.ru';

$currentRegionID = false;
$context = Bitrix\Main\Application::getInstance()->getContext();
$server = $context->getServer();
$serverName = $server['SERVER_NAME'];

$regionInCookie = $context->getRequest()->getCookie('sotbit_regions_id');
$regionDomain = $context->getRequest()->getCookie('regions_domain');

if($regionInCookie && $regionDomain == $serverName) {
	
	$currentRegionID = $regionInCookie;
	
	Bitrix\Main\Loader::includeModule('sotbit.regions');
	
	$rs = RegionsTable::getList(array(
		'order' => array(
			'SORT' => 'asc'
		),
		'filter' => ['ID' => $currentRegionID],
		'cache' => array(
			'ttl' => 36000000,
		)
	));
	if($region = $rs->fetch()) {
		
		session_start();
		$_SESSION['SOTBIT_REGIONS_YAEDA'] = $region;
	}
	
	
	
} else {

	$arRegions = getRegionsListDS();

	foreach($arRegions as $region) {
		
		$tmpDomain = $region['CODE'];
		$tmpDomain = preg_replace('#^http.*?:\/\/#', '', $tmpDomain);
		
		if($serverName == $tmpDomain) {
			
			$currentRegionID = $region['ID'];
			
			session_start();
			$_SESSION['SOTBIT_REGIONS_YAEDA'] = $region;
			
			$APPLICATION->set_cookie("regions_domain", $serverName, false, '/', '.' . $baseDomain);
			$APPLICATION->set_cookie("sotbit_regions_city_choosed", "Y", false, '/', '.' . $baseDomain);
			$APPLICATION->set_cookie("sotbit_regions_id", $region['ID'] ? $region['ID'] : false, false, '/', '.' . $baseDomain);
		}
	}
}
*/



//require_once(dirname(__FILE__) ."/include/bitrix_prop_checkbox.php");
include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/classSimpleImage.php");

// Turn off all error reporting
error_reporting(0);

define('AGREEMENT_ID', 2);
define('PRICE_ROUND', 2);
define('PRICE_DECIMAL', ".");
define('PRICE_THOUSAND', " ");

$uchefSettings = array(

	'CATALOG_IBLOCK' => 5,
	'COUPONS_IBLOCK' => 16,
	'OF_DAY_IBLOCK' => 17,
	'REVIEW_PRODUCTS_IBLOCK' => 18,
	
	// Категории, в которых структура остается старая (в остальных двухуровневая)
	'SECTIONS_ID_OLD_STRUCTURE' => array(
		369,
		440,
	),
);


AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "DoIBlockAfterSave");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "DoIBlockAfterSave");
AddEventHandler("catalog", "OnPriceAdd", "DoIBlockAfterSave");
AddEventHandler("catalog", "OnPriceUpdate", "DoIBlockAfterSave");
function DoIBlockAfterSave($arg1, $arg2 = false)
{
 $ELEMENT_ID = false;
 $IBLOCK_ID = false;
 $OFFERS_IBLOCK_ID = false;
 $OFFERS_PROPERTY_ID = false;
 if (CModule::IncludeModule('currency'))
 $strDefaultCurrency = CCurrency::GetBaseCurrency();

 //Check for catalog event
 if(is_array($arg2) && $arg2["PRODUCT_ID"] > 0)
 {
 //Get iblock element
 $rsPriceElement = CIBlockElement::GetList(
 array(),
 array(
 "ID" => $arg2["PRODUCT_ID"],
 ),
 false,
 false,
 array("ID", "IBLOCK_ID")
 );
 if($arPriceElement = $rsPriceElement->Fetch())
 {
 $arCatalog = CCatalog::GetByID($arPriceElement["IBLOCK_ID"]);
 if(is_array($arCatalog))
 {
 //Check if it is offers iblock
 if($arCatalog["OFFERS"] == "Y")
 {
 //Find product element
 $rsElement = CIBlockElement::GetProperty(
 $arPriceElement["IBLOCK_ID"],
 $arPriceElement["ID"],
 "sort",
 "asc",
 array("ID" => $arCatalog["SKU_PROPERTY_ID"])
 );
 $arElement = $rsElement->Fetch();
 if($arElement && $arElement["VALUE"] > 0)
 {
 $ELEMENT_ID = $arElement["VALUE"];
 $IBLOCK_ID = $arCatalog["PRODUCT_IBLOCK_ID"];
 $OFFERS_IBLOCK_ID = $arCatalog["IBLOCK_ID"];
 $OFFERS_PROPERTY_ID = $arCatalog["SKU_PROPERTY_ID"];
 }
 }
 //or iblock which has offers
 elseif($arCatalog["OFFERS_IBLOCK_ID"] > 0)
 {
 $ELEMENT_ID = $arPriceElement["ID"];
 $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
 $OFFERS_IBLOCK_ID = $arCatalog["OFFERS_IBLOCK_ID"];
 $OFFERS_PROPERTY_ID = $arCatalog["OFFERS_PROPERTY_ID"];
 }
 //or it's regular catalog
 else
 {
 $ELEMENT_ID = $arPriceElement["ID"];
 $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
 $OFFERS_IBLOCK_ID = false;
 $OFFERS_PROPERTY_ID = false;
 }
 }
 }
 }
 //Check for iblock event
 elseif(is_array($arg1) && $arg1["ID"] > 0 && $arg1["IBLOCK_ID"] > 0)
 {
 //Check if iblock has offers
 $arOffers = CIBlockPriceTools::GetOffersIBlock($arg1["IBLOCK_ID"]);
 if(is_array($arOffers))
 {
 $ELEMENT_ID = $arg1["ID"];
 $IBLOCK_ID = $arg1["IBLOCK_ID"];
 $OFFERS_IBLOCK_ID = $arOffers["OFFERS_IBLOCK_ID"];
 $OFFERS_PROPERTY_ID = $arOffers["OFFERS_PROPERTY_ID"];
 }
 }

 if($ELEMENT_ID)
 {
 static $arPropCache = array();
 if(!array_key_exists($IBLOCK_ID, $arPropCache))
 {
 //Check for MINIMAL_PRICE property
 $rsProperty = CIBlockProperty::GetByID("MINIMUM_PRICE", $IBLOCK_ID);
 $arProperty = $rsProperty->Fetch();
 if($arProperty)
 $arPropCache[$IBLOCK_ID] = $arProperty["ID"];
 else
 $arPropCache[$IBLOCK_ID] = false;
 }

 if($arPropCache[$IBLOCK_ID])
 {
 //Compose elements filter
 if($OFFERS_IBLOCK_ID)
 {
 $rsOffers = CIBlockElement::GetList(
 array(),
 array(
 "IBLOCK_ID" => $OFFERS_IBLOCK_ID,
 "PROPERTY_".$OFFERS_PROPERTY_ID => $ELEMENT_ID,
 ),
 false,
 false,
 array("ID")
 );
 while($arOffer = $rsOffers->Fetch())
 $arProductID[] = $arOffer["ID"];

 if (!is_array($arProductID))
 $arProductID = array($ELEMENT_ID);
 }
 else
 $arProductID = array($ELEMENT_ID);

 $minPrice = false;
 $maxPrice = false;
 //Get prices
 $rsPrices = CPrice::GetList(
 array(),
 array(
 "PRODUCT_ID" => $arProductID,
 )
 );
 while($arPrice = $rsPrices->Fetch())
 {
 if (CModule::IncludeModule('currency') && $strDefaultCurrency != $arPrice['CURRENCY'])
 $arPrice["PRICE"] = CCurrencyRates::ConvertCurrency($arPrice["PRICE"], $arPrice["CURRENCY"], $strDefaultCurrency);

 $PRICE = $arPrice["PRICE"];

 if($minPrice === false || $minPrice > $PRICE)
 $minPrice = $PRICE;

 if($maxPrice === false || $maxPrice < $PRICE)
 $maxPrice = $PRICE;
 }

 //Save found minimal price into property
 if($minPrice !== false)
 {
 CIBlockElement::SetPropertyValuesEx(
 $ELEMENT_ID,
 $IBLOCK_ID,
 array(
 "MINIMUM_PRICE" => $minPrice,
 "MAXIMUM_PRICE" => $maxPrice,
 )
 );
 }
 }
 }
}

// достаем список скидок, применяемых к корзине из "Правила работы с корзиной"
if (!function_exists("getDiscountCartList")) {
    function getDiscountCartList($arOrder, $USER_ID = null, $ORDER_PRICE = 0, $ORDER_WEIGHT = 0)
    {
        $discountList = array();
        $arIDS = array();

        $groupDiscountIterator = Bitrix\Sale\Internals\DiscountGroupTable::getList(array(
            'select' => array('DISCOUNT_ID'),
            'filter' => array('@GROUP_ID' => CUser::GetUserGroup($USER_ID), '=ACTIVE' => 'Y')
        ));
        while ($groupDiscount = $groupDiscountIterator->fetch()) {
            $groupDiscount['DISCOUNT_ID'] = (int)$groupDiscount['DISCOUNT_ID'];
            if ($groupDiscount['DISCOUNT_ID'] > 0)
                   $arIDS[$groupDiscount['DISCOUNT_ID']] = $groupDiscount['DISCOUNT_ID'];
        }
        if ($arIDS) {
            $discountIterator = Bitrix\Sale\Internals\DiscountTable::getList(array(
                'select' => array(
                    "ID", "NAME", "PRIORITY", "SORT", "LAST_DISCOUNT", "UNPACK", "APPLICATION", "USE_COUPONS"
                ),
                'filter' => array(
                    '@ID' => $arIDS
                ),
                'order' => array(
                    "PRIORITY" => "DESC",
                    "SORT" => "ASC",
                    "ID" => "ASC"
                )
            ));
            foreach ($arOrder as &$order) {
               // $order['PRICE'] = $order['FULL_PRICE'];
			   $order['PRICE'] = 500000;
            }
            $arOrderAll = array(
                'SITE_ID' => SITE_ID,
                'USER_ID' => $USER_ID,
                'ORDER_PRICE' => $ORDER_PRICE,
                'ORDER_WEIGHT' => $ORDER_WEIGHT,
                'BASKET_ITEMS' => $arOrder
            );
            while ($discount = $discountIterator->fetch()) {
                $checkOrder = null;
                $strUnpack = $discount['UNPACK'];
                if (empty($strUnpack))
                    continue;
                eval('$checkOrder=' . $strUnpack . ';');
                if (!is_callable($checkOrder))
                    continue;
                $boolRes = $checkOrder($arOrderAll);
                unset($checkOrder);

                if ($boolRes) {
                    $discountList[] = $discount;
                }
            }
        }
        return $discountList;
    }
}



//AddEventHandler("sale", "OnBasketUpdate", "addBasketPack");
//AddEventHandler("sale", "OnBasketDelete", "addBasketPack");

function addBasketPack($ID, $arFields) {

   if(CModule::IncludeModule("iblock") && CModule::IncludeModule("sale")) {
	   $userID = \Bitrix\Sale\Fuser::getId();

		$itemId = 4851;
		$iBlock = 13;
		$allSum = 0;
		$allWeight = 0;
		$allPackWeight = 0;
		$needPacks = 0;
		$arBasketItemsId = array();
		$inBasket = false;
		$itemBasketId = 0;
		$maxSort = 0;

		$dbBasketItems = CSaleBasket::GetList(
			array(),
			array(
				'FUSER_ID' => $userID,
				'LID' => SITE_ID,
				'ORDER_ID' => 'NULL'
			),
			false,
			false,
			array()
		);

		while($arItem = $dbBasketItems->Fetch()) {

			if($arItem['PRODUCT_ID'] == $itemId){
				$inBasket = true;
				$itemBasketId = $arItem['ID'];
			}
			else{
				$arBasketItemsId[] = $arItem['PRODUCT_ID'];
				$allSum += ($arItem["PRICE"] * $arItem["QUANTITY"]);
				$allWeight += ($arItem["WEIGHT"] * $arItem["QUANTITY"]);
			}

			if($arItem['SORT'] > $maxSort){
				$maxSort = $arItem['SORT'];
			}
		}

		if(!empty($arBasketItemsId)) {
		
			$arFilter = array('IBLOCK_ID' => 13, 'SECTION_ID' => 374, 'ID' => $arBasketItemsId);
			$arSelect = array('ID', 'NAME', 'PRICE', 'PROPERTY_MAX_WEIGHT');

			$dbElements = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);

			while($arElement = $dbElements->GetNextElement()){
				$arFields = $arElement->GetFields();

				$allPackWeight += intval($arFields['PROPERTY_MAX_WEIGHT_VALUE']);
			}

			$allWeight -= $allPackWeight;


			$dbProp = CIBlockElement::GetProperty($iBlock, $itemId, array("sort" => "asc"), Array("CODE"=>"MAX_WEIGHT"));

			if($arProp = $dbProp->Fetch()){

				if(empty($arProp['VALUE']) && $arProp['VALUE'] == 0){
					$arProp['VALUE'] = 1;
				}

				$needPacks = ceil($allWeight / intval($arProp['VALUE']));


				if($needPacks > 0){
					if($inBasket){
						$maxSort++;
						CSaleBasket::Update($itemBasketId, array('QUANTITY' => $needPacks, 'SORT' => $maxSort));
					}
					else{
						Add2BasketByProductID($itemId, $needPacks, array());
					}
				}
				else{
					CSaleBasket::Delete($itemBasketId);
				}
			}
		}
	}
}



AddEventHandler("sale", "OnSaleComponentOrderOneStepPersonType", "selectSavedPersonType");
function selectSavedPersonType(&$arResult, &$arUserResult, $arParams)
{
   global $USER;

   # если пользователь авторизовн, то посмотрим, сохраннен ли для него тип плательщика
   if($USER->IsAuthorized())
   {
      $rsUser = $USER->GetByID($USER->GetID());
      $arUser = $rsUser->Fetch();

      $arResult['USER_INFO'] = $arUser;

      # если задан тип плательщика у пользователя
      if(isset($arResult['USER_INFO']['UF_SALE_PROFILE']) && !empty($arResult['USER_INFO']['UF_SALE_PROFILE']))
	  {
		$arResult['USER_INFO']['UF_SALE_PROFILE'] = json_decode($arResult['USER_INFO']['UF_SALE_PROFILE'], true);

         # снимем активность у выбранного компонетом типа
         $selectedType = 4;
         foreach($arResult['PERSON_TYPE'] as $key => $type){
            if($type['CHECKED'] == 'Y'){
               $selectedType = $key;
			}
		}

         $arResult['PERSON_TYPE'][$selectedType]['CHECKED'] = '';

         # и запишем наш, чтобы пользователь не смог его поменять
         $arResult['PERSON_TYPE'][$arResult['USER_INFO']['UF_SALE_PROFILE']['PROFILE_TYPE']]['CHECKED'] = 'Y';
         $arUserResult['PERSON_TYPE_ID'] = $arResult['USER_INFO']['UF_SALE_PROFILE']['PROFILE_TYPE'];
      }
   }
}

AddEventHandler("sale", "OnOrderNewSendEmail", array("CSendOrder", "OnOrderNewSendEmailHandler"));
AddEventHandler('main', 'OnBeforeUserAdd', array('CSendOrder', 'OnBeforeUserAddHandler'));

class CSendOrder{

	private static $newUserLogin = false;
	private static $newUserPass = false;

	public static function OnBeforeUserAddHandler($arFields) {
	  self::$newUserLogin = $arFields['LOGIN'];
	  self::$newUserPass = $arFields['PASSWORD'];
	}

	public static function OnOrderNewSendEmailHandler($orderID, $eventName, &$arFields){

		if(CModule::IncludeModule("main") && CModule::IncludeModule("sale")){
			global $USER;
			global $DB;

			$isAnonymousOrder = false;

			$strOrderList = "";
			$UserDelivery = "";
			$UserPaySystem = "";

			$dbBasketItems = CSaleBasket::GetList(
					array("NAME" => "ASC"),
					array("ORDER_ID" => $orderID),
					false,
					false,
					array("ID", "PRODUCT_ID", "NAME", "QUANTITY", "PRICE", "CURRENCY")
				);

			$strOrderList = "<table style=\"width:100%;\"><tr style=\"text-align:center;\"><td>Название</td><td>Колличество</td><td>Цена</td><td>Всего</td></tr>";


			while ($arBasketItems = $dbBasketItems->Fetch())
			{
				$strOrderList .= "<tr>";
				$strOrderList .= "<td style=\"padding:10px;\">" . $arBasketItems["NAME"] . "</td>";
				$strOrderList .= "<td style=\"padding:10px;text-align:center;\">" . $arBasketItems["QUANTITY"] . "</td>";
				$strOrderList .= "<td style=\"padding:10px;text-align:center;\">" . SaleFormatCurrency($arBasketItems["PRICE"], $arBasketItems["CURRENCY"]) . "</td>";
				$strOrderList .= "<td style=\"padding:10px;text-align:center;\">" . SaleFormatCurrency($arBasketItems["PRICE"] * $arBasketItems["QUANTITY"], $arBasketItems["CURRENCY"]) . "</td>";
				$strOrderList .= "</tr>";
			}

			$strOrderList .= "</table>";

			if($arOrder = CSaleOrder::GetByID($orderID)){

				if($arOrder['USER_ID'] == CSaleUser::GetAnonymousUserID()){
					$isAnonymousOrder = true;
				}

				if($arDelivery = CSaleDelivery::GetByID($arOrder["DELIVERY_ID"])){
					$UserDelivery = $arDelivery["NAME"].' (' . SaleFormatCurrency($arOrder["PRICE_DELIVERY"], $arOrder["CURRENCY"]) .')';
				}
				if($arPaySystem = CSalePaySystem::GetByID($arOrder['PAY_SYSTEM_ID'], $arOrder['PERSON_TYPE_ID'])){
					$UserPaySystem = $arPaySystem["NAME"];

					if($arPaySystem["ID"] == 8){
						$UserPaySystem .= ' <a href="' . getHostAddress() .'/personal/order/payment/?ORDER_ID='. $orderID .'?>&PAYMENT_ID='. $orderID .'/" target="_blank">Скачать счет</a>';
					}
				}


				$UserOrderProps='';
				$db_vals = CSaleOrderPropsValue::GetOrderProps($orderID);
				while($arVals = $db_vals->Fetch())
				{
					if($arVals["IS_LOCATION"]=="Y"):
						$arLocs = CSaleLocation::GetByID($arVals["VALUE"], LANGUAGE_ID);
						$UserOrderProps .= $arVals["NAME"].': '.$arLocs["COUNTRY_NAME"]." - ".$arLocs["REGION_NAME"]." - ".$arLocs["CITY_NAME"]."<br/>";
					else:
						$UserOrderProps .= $arVals["NAME"].': '.$arVals["VALUE"]."<br/>";
					endif;

					if($arVals['CODE'] == 'EMAIL'){
						if($isAnonymousOrder || (strlen($arOrder["USER_EMAIL"]) == 0 && strlen($USER->GetEmail()) == 0)){
							$arOrder["USER_EMAIL"] = $arVals["VALUE"];
						}
					}
					else if($isAnonymousOrder && $arVals['CODE'] == 'CUSTOMER_NAME'){
						$arOrder["USER_NAME"] = $arVals["VALUE"];
					}
				}

				$arFields = Array(
					"ORDER_ID" => $arOrder["ACCOUNT_NUMBER"],
					"ORDER_DATE" => Date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT", SITE_ID))),
					"ORDER_USER" => (strlen($arOrder["USER_NAME"]) > 0 ? $arOrder["USER_NAME"] : $USER->GetFormattedName(false)),
					"PRICE" => SaleFormatCurrency($arOrder["PRICE"], $arOrder["CURRENCY"]),
					"BCC" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
					"EMAIL" => (strlen($arOrder["USER_EMAIL"]) > 0 ? $arOrder["USER_EMAIL"] : $USER->GetEmail()),
					"ORDER_LIST" => $strOrderList,
					"SALE_EMAIL" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
					"DELIVERY_PRICE" => SaleFormatCurrency($arOrder["PRICE_DELIVERY"], $arOrder["CURRENCY"]),
					"DELIVERY" => $UserDelivery,
					"PAY_SYSTEM" => $UserPaySystem,
					"USER_ORDER_PROPS" => $UserOrderProps,
					"USER_DESCRIPTION" => $arUserResult["ORDER_DESCRIPTION"]
				);

				if (self::$newUserPass === false) {
					$arFields['PASSWORD'] = '';
				} else {
					$arFields['PASSWORD'] = '<br/>Ваш логин: '.self::$newUserLogin;
					$arFields['PASSWORD'] .= '<br/>Ваш пароль: '.self::$newUserPass;
					$arFields['PASSWORD'] .= '<br/>Воспользуйтесь этими данными при следующем заказе или посещении сайта для входа в Личный кабинет';
				}

				return true;
			}
			else{
				return false;
			}

		}
		else{
			return false;
		}

	}

}

AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");
function OnBeforeUserUpdateHandler(&$arFields)
{
	if(!empty($arFields["EMAIL"])){
        $arFields["LOGIN"] = $arFields["EMAIL"];
	}

	return $arFields;
}


function dump($array, $canShow = false){

	if(!$canShow && isDevUser()){
		$canShow = true;
	}

	if($canShow){
		echo '<br><pre>';
		print_r($array);
		echo '<br></pre>';
	}
}


function dumpToFile($array){

	$file = $_SERVER['DOCUMENT_ROOT'] . '/dump_to_file.txt';
	// Открываем файл для получения существующего содержимого
	$current = file_get_contents($file);
	// Добавляем нового человека в файл
	$current .= serialize($array) . PHP_EOL;
	// Пишем содержимое обратно в файл
	file_put_contents($file, $current);

}

function isDevUser(){

	global $USER;
	if ($USER->IsAuthorized() && $USER->IsAdmin()){
		return true;
	}
	else{
		return false;
	}

}

function getFormatedWeight($weight){
	$output = "";

	if($weight < 1000){
		$output = $weight . ' гр.';
	}
	else if($weight < 100000){
		$output = ($weight / 1000) . ' кг.';
	}
	else if($weight < 1000000){
		$output = ($weight / 100000) . ' ц.';
	}
	else{
		$output = ($weight / 1000000) . ' т.';
	}

	return $output;
}

function numberFormat($number, $round, $descSep, $tousSep){
	if(!isNumeric($number)){
		return $number;
	}

	$number = (float)$number;

	if(intval($number) == $number){
		$result = number_format($number, 0, $descSep, $tousSep);
	}
	else{
		$result = number_format($number, $round, $descSep, $tousSep);
	}

	return $result;
}

function priceFormat($price){
	if(!isNumeric($price)){
		$price = substr($price, 0, strlen($price) - 4);
	}
	$res = numberFormat($price, PRICE_ROUND, PRICE_DECIMAL, PRICE_THOUSAND);
	$res = preg_replace('#\.00$#', '', $res);
	return $res;
}

function isNumeric($value){
	return (is_float($value) || is_int($value));
}


function getDiscounts($arParams)
{
	global $USER;

	$arFilter = array('IBLOCK_ID' => 5, 'ACTIVE' => 'Y');

	if(is_array($arParams)){
		foreach($arParams as $key => $value){
			$arFilter[$key] = $value;
		}
	}

	$arDiscountIds = array();

	$dbElemens = CIBlockElement::GetList(array(), $arFilter, false, false, array('ID'));

	while($arElement = $dbElemens->Fetch()){

		$arDiscounts = CCatalogDiscount::GetDiscountByProduct(
			$arElement['ID'],
			$USER->GetUserGroupArray(),
			"N",
			array(),
			SITE_ID
		);


		if(!empty($arDiscounts)){
			$arDiscountIds[] = $arElement['ID'];
		}

	}

	return $arDiscountIds;
}

function getDiscountsSections($arProductsID) {
	
	$arSections = array();
	
	if($arProductsID) {
		
		$arSelect = Array("ID", "IBLOCK_SECTION_ID");
		$arFilter = Array('ID' => $arProductsID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		
		while($ob = $res->GetNextElement()) {
			
			$arFields = $ob->GetFields();
			$rootSection = getRootSection($arFields['IBLOCK_SECTION_ID']);
			
			$arSections[$rootSection['ID']] = $rootSection['ID'];
		}
	}
	return $arSections;
}

function getRootSection($id) {

    CModule::IncludeModule("iblock");
    $res = CIBlockSection::GetByID($id);

    if ($arRes = $res->GetNext()) {

        if ((int)$arRes['DEPTH_LEVEL'] > 1 && (int)$arRes['IBLOCK_SECTION_ID'] > 0) {

            return getRootSection($arRes['IBLOCK_SECTION_ID']);

        } else {

            return $arRes;
        }
    }
}

function getParrentSection($id) {

    CModule::IncludeModule("iblock");
    $res = CIBlockSection::GetByID($id);

    if ($arRes = $res->GetNext()) {

        if ((int)$arRes['DEPTH_LEVEL'] > 1 && (int)$arRes['IBLOCK_SECTION_ID'] > 0) {

			$resPar = CIBlockSection::GetByID($arRes['IBLOCK_SECTION_ID']);
			if ($arResPar = $resPar->GetNext()) {
				
				return $arResPar;
			}

        } else {

            return $arRes;
        }
    }
}

function replaceTplToken($token, $value, $tpl){
	return preg_replace_callback($token, function($matches) use ($value){
		ob_start();

		print $value;

		$retrunStr = @ob_get_contents();
		ob_get_clean();
		return $retrunStr;
	},
	$tpl);
}

function resizeImage($path, $width = 0, $height = 0){
   $path = urldecode($path);

   $arrPath = explode('/', $path);
   $iMax = count($arrPath);
   $fileName = "";
   $ext = "";
   $filePath = "";
   $newUrl = "";

   $filePath = $_SERVER['DOCUMENT_ROOT'];

   for($iZ = 0; $iZ < $iMax - 1; $iZ++){
	   if($iZ > 0){
		   $filePath .= "/";
		   $newUrl .= "/";
	   }

	   $filePath .=  $arrPath[$iZ];
	   $newUrl .=  $arrPath[$iZ];
   }

   $arrFileName = explode('.', $arrPath[$iMax - 1]);
   $iMax2 = count($arrFileName);

	for($iZ = 0; $iZ < $iMax2; $iZ++){
		if($iZ < $iMax2 - 1){
			if($iZ > 0){
				$fileName .= ".";
			}
			$fileName .=  $arrFileName[$iZ];
		}
		else{
			$ext = '.' . $arrFileName[$iZ];
		}
	}

	if(!file_exists($filePath . "/". $fileName . '_' . $width . '_' .  $height . $ext)){
		$image = new SimpleImage();
		$image->load($filePath . "/". $fileName . $ext);

		if($width > 0 && $height == 0){
			$image->resizeToWidth($width);
		}
		else if($width == 0 && $height > 0){
			$image->resizeToHeight($height);
		}
		else{
			$image->resize($width, $height);
		}


		$image->save($filePath . "/". $fileName . '_' . $width . '_' .  $height . $ext);
	}

	$newUrl .=  "/". $fileName . '_' . $width . '_' .  $height . $ext;

	return $newUrl;
}


function getHostAddress(){
	return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER["HTTP_HOST"];
}

/* PAGES FOR REDIRECT */

$arPagesForRedirect = array(
	'/services-view/predesign/',
	'/help/',
	'/portfolio-view/',
	'/product-category/',
	'/post_tag-sitemap.xml',
	'/portfolio_tag-sitemap.xml',
	'/slider-sitemap.xml',
	'/special_offers/',
	'/team-sitemap.xml',
	'/products/',
	'/basket/',
	'/page-sitemap.xml',
	'/post-sitemap.xml',
	'/product-category/',
	'/product-sitemap1.xml',
	'/shop_options-sitemap.xml',
	'/services-sitemap.xml',
	'/products/',
	'/recipes/',
	'/portfolio_category-sitemap.xml',
	'/shop/',
	'/fourchette_home/',
);

$currentUrl = \Bitrix\Main\Context::getCurrent()->getRequest()->getRequestUri();
$uri = new \Bitrix\Main\Web\Uri($currentUrl);
$currentPath = $uri->getPath();

if(in_array($currentPath, $arPagesForRedirect)) {
	
	LocalRedirect('/', true, '301 Moved permanently');
}

// noindex, nofollow FOR EMPTY PAGES

AddEventHandler("main", "OnEndBufferContent", "setMetaRobots");

function setMetaRobots(&$content) {

	if(preg_match('#<\!--CONTENT_START-->(.+?)<\!--CONTENT_END-->#ims', $content, $match)) {
		
		$data = preg_replace('#<h1>.*?<\/h1>#', '', $match[1]);
		$data = trim(strip_tags($data));
		
		if(!$data) {
			
			$content = str_replace('<meta name="robots" content="index, follow" />', '<meta name="robots" content="noindex, nofollow" />', $content);
		}
	}
}

/*Проверка If-Modified-Since и вывод 304 Not Modified */
//AddEventHandler('main', 'OnEpilog', array('CBDPEpilogHooks', 'CheckIfModifiedSince'));
class CBDPEpilogHooks
{
    function CheckIfModifiedSince()
    {
        GLOBAL $lastModified;
        
        if ($lastModified)
        {
            header("Cache-Control: public");
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $lastModified) . ' GMT');
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $lastModified) {
                $GLOBALS['APPLICATION']->RestartBuffer();
				CHTTP::SetStatus('304 Not Modified');
                exit();
            }
        }
    }
}

// Сортировка по полю CUSTOM_SORT
function sortByCustomSort($a, $b) {
	
    if ($a['CUSTOM_SORT'] == $b['CUSTOM_SORT']) {
        return 0;
    }
    return ($a['CUSTOM_SORT'] < $b['CUSTOM_SORT']) ? -1 : 1;
}

// Сортировка по полю SORT
function sortBySort($a, $b) {
	
    if ($a['SORT'] == $b['SORT']) {
        return 0;
    }
    return ($a['SORT'] < $b['SORT']) ? -1 : 1;
}


AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "productOfDay");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "productOfDay");

function productOfDay($arFields) {
	
	if($arFields['IBLOCK_ID'] == 5) {
		
		$arPropsCodes = array('OF_DAY', 'OF_DAY_DATE', 'OF_DAY_DISCOUNT');
		$arProps = array();
		
		foreach($arPropsCodes as $code) {
			
			$properties = CIBlockProperty::GetList(array("sort" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => $arFields['IBLOCK_ID'], 'CODE' => $code));
		
			if($propFields = $properties->GetNext()) {
				
				$arProps[$propFields["CODE"]] = $propFields;;
			}
		}
		
		/* Скидки */
			
			CModule::IncludeModule("sale");
			CModule::IncludeModule("catalog");
		
			$productID = $arFields['ID'];
					
			$dbDiscount = CCatalogDiscount::GetList(
				array("SORT" => "ASC"),
				array(
					  "XML_ID" => 'PRODUCT_OF_DAY_' . $productID, 
					),
				false,
				false,
				array()
			);
			
			$arDiscount = $dbDiscount->Fetch();
		
		$propOfDayID = $arProps['OF_DAY']['ID'];
		$propOfDayDateID = $arProps['OF_DAY_DATE']['ID'];
		$propOfDayDiscountID = $arProps['OF_DAY_DISCOUNT']['ID'];
		
		$propOfDay = array_shift($arFields['PROPERTY_VALUES'][$propOfDayID]);
		$propOfDayDate = array_shift($arFields['PROPERTY_VALUES'][$propOfDayDateID]);
		$propOfDayDiscount = array_shift($arFields['PROPERTY_VALUES'][$propOfDayDiscountID]);
		
		if($propOfDay['VALUE'] && $propOfDayDate['VALUE'] && $propOfDayDiscount['VALUE']) {
			
			$discountDate = $propOfDayDate['VALUE'];
			$discountValue = $propOfDayDiscount['VALUE'];
		}
		
		/*echo '<pre>';
		print_r($arFields['PROPERTY_VALUES']);
		print_r($arProps);
		echo '</pre>';
		die();*/
		
		
		
		if($discountValue > 0) {
			
			$arGroupID = array(2);

			\Bitrix\Main\Type\Collection::normalizeArrayValuesByInt($arGroupID, true);


			$fields = array(
				'XML_ID' => 'PRODUCT_OF_DAY_' . $productID,
				'SITE_ID' => 's1',
				'NAME' => 'Товар дня - ' . $arFields['NAME'],
				'CURRENCY' => 'RUB',
				'VALUE' => $discountValue,
				'VALUE_TYPE' => 'P',
				'ACTIVE' => 'Y',
				'USER_GROUPS' => $arGroupID,
				'ACTIVE_TO' => $discountDate,
				
				'CONDITIONS' => array(
					'CLASS_ID' => 'CondGroup',
					'DATA' => array(
						'All' => 'AND',
						'True' => 'True',
					),
					'CHILDREN' => array(
						'0' => array(
							'CLASS_ID' => 'CondIBElement',
							'DATA' => array (
								'logic' => 'Equal',
								'value' =>  $productID,
							),
						),
					),
				),
			);
			
			if($arDiscount['ID']) {
				
				var_dump(CCatalogDiscount::Update($arDiscount['ID'], $fields)); //die();
				
			} else {
				
				var_dump(CCatalogDiscount::Add($fields)); 

			}
			
		} elseif($arDiscount['ID']) {
			
			CCatalogDiscount::Delete($arDiscount['ID']);
		}
	}
}



class CUserTypeIntegerCoupons extends CUserTypeInteger {


	// инициализация пользовательского свойства для инфоблока
	function GetIBlockPropertyDescription() {
		return array(
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "coupon",
			"DESCRIPTION" => "Купон",
			'GetPropertyFieldHtml' => array('CUserTypeIntegerCoupons', 'GetPropertyFieldHtml'),
			'GetAdminListViewHTML' => array('CUserTypeIntegerCoupons', 'GetAdminListViewHTML'),
		);
	}

	// представление свойства
	function getViewHTML($name, $value) {
		return $value;
	}

	// редактирование свойства
	function getEditHTML($name, $value, $is_ajax = false) {
		
		CModule::IncludeModule("sale");
		
		$outHtml = '<select name="'.$name.'">';
		$outHtml .= '<option value="">- Выберите купон</option>';

		$dbCoupon = \Bitrix\Sale\Internals\DiscountCouponTable::getList(array()); 
				
		while($arCoupon = $dbCoupon->Fetch()) { 
		
			$selected = '';
			$arDiscount = CSaleDiscount::GetByID($arCoupon['DISCOUNT_ID']);
			
			
			if($arCoupon['ID'] == $value) {
				
				$selected = ' selected="selected"';
			}
			$outHtml .= '<option'.$selected.' value="'.$arCoupon['ID'].'">'.$arCoupon['COUPON'].' ('.$arDiscount['NAME'].')</option>';
		} 
		
		$outHtml .= '</select>';
		
		return $outHtml;
	}


	// представление свойства в списке (главный модуль, инфоблок)
	function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
		
		return self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
	}

	// редактирование свойства в форме и списке (инфоблок)
	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
		return $strHTMLControlName['MODE'] == 'FORM_FILL'
		? self::getEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false)
		: self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
	}
}

// добавляем тип для инфоблока
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CUserTypeIntegerCoupons", "GetIBlockPropertyDescription"));


// Пользовательское свойство Регион
class CUserTypeRegion extends CUserTypeInteger {
	
	// инициализация пользовательского свойства для главного модуля
	function GetUserTypeDescription() {
		return array(
			"USER_TYPE_ID" => "dsRegion",
			"CLASS_NAME" => "CUserTypeRegion",
			"DESCRIPTION" => "Регион",
			"BASE_TYPE" => "int",
		);
	}

	// инициализация пользовательского свойства для инфоблока
	function GetIBlockPropertyDescription() {
		return array(
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "dsRegion",
			"DESCRIPTION" => "Регион",
			'GetPropertyFieldHtml' => array('CUserTypeRegion', 'GetPropertyFieldHtml'),
			'GetAdminListViewHTML' => array('CUserTypeRegion', 'GetAdminListViewHTML'),
			/*'ConvertToDB'           	=> array('CUserTypeRegion', 'ConvertToDB'),
            'ConvertFromDB'         	=> array('CUserTypeRegion', 'ConvertFromDB'),*/
		);
	}

	// представление свойства
	function getViewHTML($name, $value) {
		return $value;
	}

	// редактирование свойства
	function getEditHTML($name, $value, $is_ajax = false) {

		$arRegions = getRegionsListDS();
		
		$outHtml = '<select name="'.$name.'">';
		$outHtml .= '<option value="">(не выбрано)</option>';
				
		foreach($arRegions as $region) { 
		
			$selected = '';
			
			if($region['ID'] == $value) {
				
				$selected = ' selected="selected"';
			}
			
			$outHtml .= '<option'.$selected.' value="'.$region['ID'].'">'.$region['NAME'].'</option>';
		} 
		
		$outHtml .= '</select>';
		
		return $outHtml;
	}

	// редактирование свойства в форме (главный модуль)
	function GetEditFormHTML($arUserField, $arHtmlControl) {
		
		return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
	}

	// редактирование свойства в списке (главный модуль)
	function GetAdminListEditHTML($arUserField, $arHtmlControl) {
		
		return self::getViewHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], true);
	}

	// представление свойства в списке (главный модуль, инфоблок)
	function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
		
		return self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
	}

	// редактирование свойства в форме и списке (инфоблок)
	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
		return $strHTMLControlName['MODE'] == 'FORM_FILL'
		? self::getEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false)
		: self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
	}
	
	/*//читаем из базы
	function ConvertFromDB($arProperty, $value) {
		//$value['VALUE'] = unserialize($value['VALUE']);
		$value = unserialize($value);
		return $value;
	}
	
	//сохраняем в базу
	function ConvertToDB($arProperty, $value) {

		//$value['VALUE'] = serialize($vals);
		$value = serialize($value);
	}*/
}

// добавляем тип для инфоблока
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CUserTypeRegion", "GetIBlockPropertyDescription"));

// добавляем тип для главного модуля
AddEventHandler("main", "OnUserTypeBuildList", array("CUserTypeRegion", "GetUserTypeDescription"));


// Получаем список регионов
function getRegionsListDS($current = false) {
	
	Bitrix\Main\Loader::includeModule('sotbit.regions');
	
	$return = array();
	$context = Bitrix\Main\Application::getInstance()->getContext();
	$server = $context->getServer();
	$requestUri = $server->getRequestUri();
	$i = 0;
	
	$rs = RegionsTable::getList(array(
		'order' => array(
			'SORT' => 'asc'
		),
		'filter' => ['%SITE_ID' => 's1'],
		'cache' => array(
			'ttl' => 36000000,
		)
	));
	while($region = $rs->fetch())
	{
		$return[$i]['ID'] = $region['ID'];
		$return[$i]['NAME'] = $region['NAME'];
		$return[$i]['URL'] = $region['CODE'] . $requestUri;
		$return[$i]['CODE'] = $region['CODE'];
		if($region['NAME'] == $current)
		{
			$return[$i]['CURRENT'] = 'Y';
		}
		++$i;
	}

	return $return;
}

function getProductsCurrentRegion($arFilter) {
	
	CModule::IncludeModule("iblock"); 
	$arRes = array();
	
	$arFilter[] = array(
	
		"LOGIC" => "OR",
		array(
			array(
				"LOGIC" => "OR",
				"PROPERTY_REGION" => false,
			),
		),
		array(
			array(
				"LOGIC" => "OR",
				"PROPERTY_REGION" => $_SESSION['SOTBIT_REGIONS']['ID'],
			),
		),
	);
	
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, array());
	
	while($ob = $res->GetNextElement()) {
		
		$arFields = $ob->GetFields();
		$arRes[] = $arFields['ID'];
	}
	
	return $arRes;
}

function showSectionByRegion($id) {
	
	$result = true;

	CModule::IncludeModule("iblock");
	
	$res = CIBlockSection::GetByID($id);

	if ($arRes = $res->GetNext()) {
		
		$arFilter = array('IBLOCK_ID' => $arRes['IBLOCK_ID'], 'ID' => $arRes['ID']);
		$rsSect = CIBlockSection::GetList(array('ID' => 'asc'), $arFilter, false, array('IBLOCK_ID', 'ID', 'IBLOCK_SECTION_ID', 'DEPTH_LEVEL', 'UF_REGION'));
		
		if($arSect = $rsSect->GetNext()) {

			if(!in_array($_SESSION['SOTBIT_REGIONS']['ID'], $arSect['UF_REGION']) && !empty($arSect['UF_REGION'])) {
				
				return false;
				
			} elseif((int)$arSect['DEPTH_LEVEL'] > 1 && (int)$arSect['IBLOCK_SECTION_ID'] > 0) {
				
				$result = showSectionByRegion($arSect['IBLOCK_SECTION_ID']);
			}
		}
	}
	
	return $result;
}


/*AddEventHandler("main", "onProlog", "setDedaultRegionByDomain", 150);

function setDedaultRegionByDomain() {
	
	if(!$_COOKIE['sotbit_regions_id']) {

		$application = \Bitrix\Main\Application::getInstance();
		$context = $application->getContext();
		$server = $context->getServer();
		$serverName = $server['SERVER_NAME'];
		
		$rsReg = Sotbit\Regions\Internals\RegionsTable::getList(array(
			'order' => array(
				'SORT' => 'asc'
			),
			'filter' => ['CODE' => 'https://' . $serverName],
			'cache' => array(
				'ttl' => 36000000,
			)
		));

		if($regionReg = $rsReg->fetch()) {
			session_start();
			$_SESSION['SOTBIT_REGIONS'] = $regionReg;
			
			global $currentRegionID;
			$currentRegionID = $regionReg['ID'];
		}
	}
	
	return;
}*/

// Есть ли паджинация на текущей странице
function isPagin() {
	
	$context = \Bitrix\Main\Context::getCurrent();
	$arRequest = $context->getRequest()->toArray();
	
	foreach($arRequest as $key => $val) {
		
		if(preg_match('#^PAGEN_(\d+?)$#', $key, $paginNum)) {
			
			$arResult = array(
				'PAGIN_NUM' => (int) $paginNum[1],
				'PAGIN_VAR' => $key,
				'PAGE_NUM' => (int) $val,
			);
			return $arResult;
		}
	}
	return false;
}


// Устанавливаем местоположение в заказ


$handler = Bitrix\Main\EventManager::getInstance()->addEventHandler( 
    "sale", 
    "OnSaleComponentOrderProperties", 
    "OnSaleComponentOrderPropertiesHandlerDs",
	150
); 

function OnSaleComponentOrderPropertiesHandlerDs(
	&$arUserResult,
	$request,
	&$arParams,
	&$arResult
) {
	$context = Application::getInstance()->getContext();
	$request = $context->getRequest();
	
	if(!$request->isAdminSection()) {
		
		$arRequest = $request->toArray();

		if(!Loader::includeModule('sotbit.regions') || \SotbitRegions::isDemoEnd()) {
			
			return true;
		}
		
		if(!Loader::includeModule('sale')) {
			
			return true;
		}
		
		$saleLocation = new Sotbit\Regions\Location\Sale($arUserResult['PERSON_TYPE_ID']);
		$propertyId = $saleLocation->getPropertyId();
		
		if(!$propertyId) {
			return true;
		}
		
		$Id = $saleLocation->getId();
		
		if($Id) {
			if(!$arRequest['ORDER_PROP_' . $propertyId]) {
				
				$arUserResult['ORDER_PROP'][$propertyId] = $Id;
			}
			return true;
			
		} else {
			
		}
		
		$res = \Bitrix\Sale\Location\LocationTable::getList(array(
			'filter' => array('NAME_RU' => $_SESSION['REASPEKT_GEOBASE']['CITY']),
			'select' => array('*', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE')
		));
		
		if($item = $res->fetch()) {
			
			$arUserResult['ORDER_PROP'][$propertyId] = $item['CODE'];
			return true;
		}

	}
}


?>
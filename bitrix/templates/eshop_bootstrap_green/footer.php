<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<!--CONTENT_END-->
					</div>
					<?if ($needSidebar):?>
					<div class="sidebar col-md-3 col-sm-4">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "sidebar",
								"AREA_FILE_RECURSIVE" => "Y",
								"EDIT_MODE" => "html",
							),
							false,
							Array('HIDE_ICONS' => 'Y')
						);?>
					</div><!--// sidebar -->
					<?endif?>
				</div><!--//row-->
				<?if ($curPage != SITE_DIR."index.php"  && !defined("ERROR_PAGE")) :
				$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "bottom",
						"AREA_FILE_RECURSIVE" => "N",
						"EDIT_MODE" => "html",
					),
					false,
					Array('HIDE_ICONS' => 'Y')
				);
				endif;?>
			</div><!--//container bx-content-seection-->
		</div><!--//workarea-->
		<footer class="bx-footer">
			<div class="bx-footer-line">
				<div class="bx-footer-section container">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_DIR."include/socnet_footer.php",
							"AREA_FILE_RECURSIVE" => "N",
							"EDIT_MODE" => "html",
						),
						false,
						Array('HIDE_ICONS' => 'Y')
					);?>
				</div>
			</div>
			<div class="bx-footer-section container bx-center-section">
				<div class="row">
					<div class="col-lg-2 col-md-2 menu_resp_1">
						<h4 class="bx-block-title">О нас</h4>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
								"ROOT_MENU_TYPE" => "bottom",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_TYPE" => "A",
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_TIME" => "36000000",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array(
								),
							),
							false
						);?>
						
						<a href="/catalog"><h4 class="bx-block-title">Каталог</h4></a>
					</div>
					<div class="col-lg-2 col-md-2 mobile_menu_resp">
						<h4 class="bx-block-title">Покупателям</h4>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
								"ROOT_MENU_TYPE" => "bottom2",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_TYPE" => "A",
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_TIME" => "36000000",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array(
								),
							),
							false
						);?>
					</div>
					<div class="col-lg-6 col-md-6 center_foot">
						<div class="bx-inclogofooter">
							<div class="bx-inclogofooter-block">
								<div class="bx-inclogofooter-tel">
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/telephone.php"
									), false);?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 pay_block">
						<h4 class="bx-block-title text-center">Способы оплаты</h4>
						<br><br>
						<img src="/images/footer-oplata.png" class="img-responsive" alt="">
					</div>
				</div>
			</div>
		</footer>
		<!-- <div class="col-xs-12 hidden-lg hidden-md hidden-sm">-->
			<?/*$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "", array(
					"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
					"PATH_TO_PERSONAL" => SITE_DIR."personal/",
					"SHOW_PERSONAL_LINK" => "N",
					"SHOW_NUM_PRODUCTS" => "Y",
					"SHOW_TOTAL_PRICE" => "Y",
					"SHOW_PRODUCTS" => "N",
					"POSITION_FIXED" =>"Y",
					"POSITION_HORIZONTAL" => "center",
					"POSITION_VERTICAL" => "bottom",
					"SHOW_AUTHOR" => "Y",
					"PATH_TO_REGISTER" => SITE_DIR."login/",
					"PATH_TO_PROFILE" => SITE_DIR."personal/"
				),
				false,
				array()
			);*/?>
		<!--</div> -->
	</div> <!-- //bx-wrapper -->
<?$APPLICATION->IncludeComponent("bitrix:main.feedback",
	"catalog_lk",
	Array(
        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
        "EMAIL_TO" => "anvlassov86@gmail.com",
        "REQUIRED_FIELDS" => Array(),
        "EVENT_MESSAGE_ID" => Array("7")
    )
);?>
<?if(!$USER->IsAuthorized()):?>
	<div id="register" class="modal-window" data-modal=".modal-bg">
		<div class="js-close-modal close-modal-btn"></div>
		<?$APPLICATION->IncludeComponent(
			"braino:main.register",
			"modal_window",
			Array(
			"AJAX_MODE" => "Y",
			"SEF_MODE" => "Y", 
			"SHOW_FIELDS" => Array("NAME"), 
			"REQUIRED_FIELDS" => Array("NAME", "EMAIL"), 
			"AUTH" => "Y", 
			"USE_BACKURL" => "Y", 
			"SUCCESS_PAGE" => "", 
			"SET_TITLE" => "N", 
			"USER_PROPERTY" => Array(), 
			"SEF_FOLDER" => "/", 
			"VARIABLE_ALIASES" => Array()
		)
		);?>
	</div>
	<div id="autoriz" class="modal-window" data-modal=".modal-bg">
		<div class="js-close-modal close-modal-btn"></div>
		<?$APPLICATION->IncludeComponent(
			"bitrix:system.auth.form",
			"auth",
			Array(
				"FORGOT_PASSWORD_URL" => "",
				"PROFILE_URL" => "",
				"REGISTER_URL" => "",
				"SHOW_ERRORS" => "Y"
			)
		);?>
	</div>
	<div id="forget" class="modal-window" data-modal=".modal-bg">
		<div class="js-close-modal close-modal-btn"></div>
		<?
		$APPLICATION->IncludeComponent(
			"bitrix:system.auth.forgotpasswd",
			"chef",
			Array(
				"SHOW_ERRORS" => "Y"
			)
		);?>
	</div>
<?endif;?>

<div class="modal-bg"></div>
<script>
	BX.ready(function(){
		var upButton = document.querySelector('[data-role="eshopUpButton"]');
		BX.bind(upButton, "click", function(){
			var windowScroll = BX.GetWindowScrollPos();
			(new BX.easing({
				duration : 500,
				start : { scroll : windowScroll.scrollTop },
				finish : { scroll : 0 },
				transition : BX.easing.makeEaseOut(BX.easing.transitions.quart),
				step : function(state){
					window.scrollTo(0, state.scroll);
				},
				complete: function() {
				}
			})).animate();
		})
	});
</script>

<?/*
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111365980-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111365980-1');
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47067204 = new Ya.Metrika({
                    id:47067204,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47067204" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
*/
if($_SESSION["SOTBIT_REGIONS"]["COUNTER"]) {
	echo $_SESSION["SOTBIT_REGIONS"]["COUNTER"];
}
?>

<script data-skip-moving="true">
        (function(w,d,u){
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b5529985/crm/site_button/loader_1_dds9dt.js');
</script>

</body>
</html>
<?

$lastModifiedExceptions = array(
	'/cart/',
	'/cart/order/',
	'/personal/',
	'/personal/favorite/',
	'/personal/private/',
	'/favorite/',
);

$currentUrl = \Bitrix\Main\Context::getCurrent()->getRequest()->getRequestUri();
$uri = new \Bitrix\Main\Web\Uri($currentUrl);
$currentPath = $uri->getPath();

$server = \Bitrix\Main\Context::getCurrent()->getServer();

if($server['SCRIPT_NAME'] != '/bitrix/urlrewrite.php' && !in_array($currentPath, $lastModifiedExceptions)) {
	
	$fileTime = filemtime($_SERVER['SCRIPT_FILENAME']);
	
	GLOBAL $lastModified;
	if (!$lastModified)
	   $lastModified = $fileTime;
	else
	   $lastModified = max($lastModified, $fileTime);
}
?>
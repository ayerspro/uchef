<?
header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
header("Access-Control-Allow-Credentials: true");
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;

global $baseDomain;

$module_id = "reaspekt.geobase";

if (!CModule::IncludeModule($module_id)) {
    ShowError("Error! Module no install");
	return;
}

$request = Application::getInstance()->getContext()->getRequest();

$strCITY_ID = htmlspecialchars(trim($request->getPost("CITY_ID")));

$resultCity["STATUS"] = "N";

if(strlen($strCITY_ID) >= 2) {
	
    $resultCity = ReaspGeoIP::SetCityManual($strCITY_ID);
	
	$currentRegion = false;
	
	Bitrix\Main\Loader::includeModule('sotbit.regions');
	
	$rs = Sotbit\Regions\Internals\RegionsTable::getList(array(
		'order' => array(
			'SORT' => 'asc'
		),
		'filter' => ['NAME' => $strCITY_ID],
		'cache' => array(
			'ttl' => 36000000,
		)
	));
	
	if($region = $rs->fetch()) {
		
		$currentRegion = $region;
	}
	
	if(!$currentRegion) {
		
		$rs = Sotbit\Regions\Internals\RegionsTable::getList(array(
			'order' => array(
				'SORT' => 'asc'
			),
			'filter' => ['CODE' => 'https://' . $baseDomain],
			'cache' => array(
				'ttl' => 36000000,
			)
		));
		
		if($region = $rs->fetch()) {
			
			$currentRegion = $region;
		}
	}
	
	$arData = ReaspGeoIP::GetDataName($strCITY_ID);
	$resultCity = array_merge($resultCity, $currentRegion);
}

echo json_encode($resultCity);
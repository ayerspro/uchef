<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

$module_id = "reaspekt.geobase";

if (!CModule::IncludeModule($module_id)) {
    ShowError("Error! Module no install");
	return;
}

$arData = ReaspGeoIP::GetAddr();

$arResult["DEFAULT_CITY"] = ReaspGeoIP::DefaultCityList();

$arCity = ReaspGeoIP::SelectQueryCity('');

global $DB;
$arAllCities = array();
$arAllCitiesOnly = array();

$data = $DB->Query("
	SELECT * FROM `reaspekt_geobase_cities`");

while ($arData = $data->Fetch()) {
	
	$arAllCities[$arData['UF_REGION_NAME']][$arData['UF_NAME']] = $arData;
	$arAllCitiesOnly[$arData['UF_NAME']] = $arData;
};

ksort($arAllCities);
ksort($arAllCitiesOnly);

foreach($arAllCities as &$city) {
	
	ksort($city);
}
unset($city);

?>


<div class="reaspektGeobaseWrapperPopup">
	<div class="js-geo-close-modal close-modal-btn"></div>
	<div class="geo__wrap">
		<div class="geo__row">
			<div class="geo__col">
				<div class="geo__col-content">
					<div class="geo__region-top">
						<div class="geo__str-region geo__str-region-active js-geo__str-region" data-region="all">
							Все города
						</div>
						<div class="geo__str-region js-geo__str-region" data-region="58f29773d7782d685c327cbd0a59045a">
							Санкт-Петербург
						</div>
					</div>
					<? $char = ''; ?>
					<? foreach($arAllCities as $region => $cityOnly) { ?>
						<? 
						$firstChar = substr($region, 0, 1); 
						if($firstChar != $char) {
							
							$char = $firstChar;
							?>
							<div class="geo__str-char"><?=$char;?></div>
							<?
						}
						?>
						<div class="geo__str-region js-geo__str-region" data-region="<?=md5($region);?>">
							<?=$region;?>
						</div>
					<? } ?>
				</div>
			</div>
			<div class="geo__col">
				<div class="geo__col-content js-geo__col-content-city">
					<? $char = ''; ?>
					<? foreach($arAllCitiesOnly as $cityOnly) { ?>
						<? 
						$firstChar = substr($cityOnly['UF_NAME'], 0, 1); 
						if($firstChar != $char) {
							
							$char = $firstChar;
							?>
							<div class="geo__str-char js-geo__str-char" data-char="<?=md5($char);?>"><?=$char;?></div>
							<?
						}
						?>
						<div class="geo__str-city js-geo__str-city" onclick="objJCReaspektGeobase.onClickReaspektGeobase('<?=$cityOnly["UF_NAME"]?>'); return false;" data-region="<?=md5($cityOnly['UF_REGION_NAME']);?>" data-char="<?=md5($char);?>">
							<?=$cityOnly['UF_NAME'];?>
						</div>
					<? } ?>
				</div>
			</div>
			<div class="geo__col">
				<div class="geo__col-content">
					<input class="geo__input js-geo__input" type="text" placeholder="Поиск города"/>
					<div class="geo__note">
						От выбранного города зависят наличие товара, цены и способ получения заказа.
					</div>
				</div>
			</div>
		</div>
	</div>



	<?/*?>
	
	<div class="reaspektGeobaseTitle"><?=Loc::getMessage("REASPEKT_TITLE_ENTER_CITY");?>:</div>				
	<div class="reaspektGeobaseCities reaspekt_clearfix">
		<div class="reaspekt_row">
		<?
        if ($arResult["DEFAULT_CITY"]) :
			$LINE_ELEMENT_COUNT = ceil(count($arResult["DEFAULT_CITY"])/3);
			$cellCol = 0;
            $cell = 1;
			
			foreach($arResult["DEFAULT_CITY"] as $arCity):?>
				<?if($cellCol%$LINE_ELEMENT_COUNT == 0 || $cellCol > $LINE_ELEMENT_COUNT):?>
					<div class="reaspekt_col-sm-4">
					<?$cellCol = 0;?>
				<?endif;?>
			
				<div class="reaspektGeobaseAct">
					<?if($arData["CITY"] == $arCity["CITY"]):?>
					<strong><?=$arCity["CITY"]?></strong>
					<?else:?>
					<a onclick="objJCReaspektGeobase.onClickReaspektGeobase('<?=$arCity["CITY"]?>'); return false;" id="reaspekt_geobase_list_<?=$cell?>" title="<?=$arCity["CITY"]?>" href="javascript:void(0);"><?=$arCity["CITY"]?></a>
					<?endif;?>
				</div>
				<?
				$cellCol++;
				if($cellCol%$LINE_ELEMENT_COUNT == 0):?>
					</div>
				<?endif;?>
			<?
                $cell++;
            endforeach;?>
			<?if($cellCol%$LINE_ELEMENT_COUNT != 0):?>
				</div>
			<?endif?>
        <?endif;?>
		</div>
	</div>
	
	<div class="reaspektGeobaseFind">
						<input type="text" onkeyup="objJCReaspektGeobase.inpKeyReaspektGeobase(event);" autocomplete="off" placeholder="<?=Loc::getMessage("REASPEKT_INPUT_ENTER_CITY");?>" name="reaspekt_geobase_search" id="reaspektGeobaseSearch">
					</div>
	<?*/?>
</div>
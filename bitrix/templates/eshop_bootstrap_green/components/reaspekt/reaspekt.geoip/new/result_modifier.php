<?
Bitrix\Main\Loader::includeModule('sotbit.regions');

$application = \Bitrix\Main\Application::getInstance();
$context = $application->getContext();
$server = $context->getServer();
$serverName = $server['SERVER_NAME'];
global $baseDomain;

$rs = Sotbit\Regions\Internals\RegionsTable::getList(array(

	'order' => array(
		'SORT' => 'asc'
	),
	'filter' => ['NAME' => $arResult["GEO_CITY"]["CITY"]],
	'cache' => array(
		'ttl' => 36000000,
	)
));

if($region = $rs->fetch()) {
	
	$currentRegion = $region;
	
	if('https://' . $serverName != $region['CODE']) {

		if($baseDomain != $serverName) {
			
			$rsReg = Sotbit\Regions\Internals\RegionsTable::getList(array(
				'order' => array(
					'SORT' => 'asc'
				),
				'filter' => ['CODE' => 'https://' . $serverName],
				'cache' => array(
					'ttl' => 36000000,
				)
			));
			
			if($regionReg = $rsReg->fetch()) {
				
				$currentRegion = $regionReg;
				ReaspGeoIP::SetCityManual($currentRegion['NAME']);
				$arResult["GEO_CITY"] = ReaspGeoIP::GetDataName($currentRegion['NAME']);
			}
			
		} else {
			
			$currentUrl = $context->getRequest()->getRequestUri();
			$uri = new Bitrix\Main\Web\Uri($currentUrl);
			$currentPathQuery = $uri->getPathQuery();
			
			LocalRedirect($region['CODE'] . $currentPathQuery);
		}
		
	}
}

if(!$currentRegion) {
	
	$rs = Sotbit\Regions\Internals\RegionsTable::getList(array(
		'order' => array(
			'SORT' => 'asc'
		),
		'filter' => ['CODE' => 'https://' . $baseDomain],
		'cache' => array(
			'ttl' => 36000000,
		)
	));
	
	if($region = $rs->fetch()) {
		
		$currentRegion = $region;
	}
}

$arResult['REGION_ID'] = $currentRegion['ID'];


//print_r($arResult);


/*
$isSetCity = $context->getRequest()->getCookie('DS_SET_CITY') == $_SERVER['SERVER_NAME'];

if(
	!$isSetCity &&
	Bitrix\Main\Loader::includeModule('sotbit.regions') 
	&& Bitrix\Main\Loader::includeModule('reaspekt.geobase')) {

	$arregionsList = getRegionsListDS();

	foreach($arregionsList as $region) {
		
		$tmpDomain = $region['CODE'];
		$tmpDomain = preg_replace('#^http.*?:\/\/#', '', $tmpDomain);
		
		if($_SERVER['SERVER_NAME'] == $tmpDomain) {
			
			ReaspGeoIP::SetCityManual($region['NAME']);
			$arResult["GEO_CITY"] = ReaspGeoIP::GetDataName($region['NAME']);;
			
			$cookie = new Bitrix\Main\Web\Cookie('DS_SET_CITY', $_SERVER['SERVER_NAME']);
			$cookie->setPath('/');
			$cookie->setDomain($_SERVER['SERVER_NAME']);
			$context->getResponse()->addCookie($cookie);
			$context->getResponse()->flush('');
			
			
			
		}
	}
}
*/
?>
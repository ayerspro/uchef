<?
header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
header("Access-Control-Allow-Credentials: true");
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $baseDomain;
global $APPLICATION;

$module_id = "reaspekt.geobase";

if (!CModule::IncludeModule($module_id)) {
    ShowError("Error! Module no install");
	return;
}

$arResult = ReaspGeoIP::SetCityYes();

$arResult = array();

Bitrix\Main\Loader::includeModule('sotbit.regions');

$context = \Bitrix\Main\Context::getCurrent();
$arRequest = $context->getRequest()->toArray();

$regionID = $arRequest['REGION_ID'];

$rsReg = Sotbit\Regions\Internals\RegionsTable::getList(array(
	'order' => array(
		'SORT' => 'asc'
	),
	'filter' => ['ID' => $regionID],
	'cache' => array(
		'ttl' => 36000000,
	)
));

if($regionReg = $rsReg->fetch()) {
	
	$currentUrl = $_SERVER['HTTP_REFERER'];
	$uri = new Bitrix\Main\Web\Uri($currentUrl);
	$currentPathQuery = $uri->getPathQuery();

	$arResult['URL'] = $regionReg['CODE'] . $currentPathQuery;	
}

$arResult['STATUS'] = 'Y';

$APPLICATION->set_cookie("REASPEKT_GEOBASE_SAVE", "Y", time() + 86400, '/', '.' . $baseDomain);

$application = \Bitrix\Main\Application::getInstance();
$context = $application->getContext();
$context->getResponse()->flush("");

echo json_encode($arResult);
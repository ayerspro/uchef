<?
$MESS['REASPEKT_GEOIP_TITLE_YOU_CITY'] = "Ваш город";
$MESS['REASPEKT_GEOIP_TITLE_YOU_CITY_NOT_FOUND'] = "Ваш город не определен";
$MESS['REASPEKT_GEOIP_BUTTON_N'] = "Нет";
$MESS['REASPEKT_GEOIP_BUTTON_Y'] = "Да";
$MESS['CITY_NOT_FOUND'] = "Не определен";
$MESS['CITY_NOT_FOUND_CHANGE'] = "Указать город";

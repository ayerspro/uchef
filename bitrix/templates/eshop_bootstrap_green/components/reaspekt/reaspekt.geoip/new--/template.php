<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$frame = $this->createFrame()->begin();?>
<?if($arResult["GEO_CITY"]):

    //путь до файлов обработчиков
    $arJSParams = array(
        "AJAX_URL" => array(
            "SELECT" => $templateFolder . "/ajax_select_city.php",
            "GET" => $templateFolder . "/ajax_geobase_get.php",
            "SAVE" => $templateFolder . "/ajax_geobase_save.php",
        ),
		"CLASS" => array(
			"WRAP_QUESTION_REASAPEKT" => "wrapQuestionReaspekt"
		)
    );
	
	$showCity = $arResult["GEO_CITY"]["CITY"] ? $arResult["GEO_CITY"]["CITY"] : GetMessage('CITY_NOT_FOUND');

    if ($arResult["SET_LOCAL_DB"] == "local_db") :?>
        <div class="wrapGeoIpReaspekt">
            <i class="fa fa-map-marker-alt" aria-hidden="true"></i> <span data-reaspektmodalbox-href="<?=$templateFolder?>/ajax_popup_city.php" class="cityLinkPopupReaspekt linkReaspekt"><?=$showCity;?></span> <span class="caret"></span>
            <?if (
				$arParams["CHANGE_CITY_MANUAL"] == "Y"
				&& $arResult["CHANGE_CITY"] == "N"
			) :?>
			<div class="<?=$arJSParams["CLASS"]["WRAP_QUESTION_REASAPEKT"]?> edit">
                <div class="questionYourCityReaspekt">
					<? if($arResult["GEO_CITY"]["CITY"]) { ?>
						<span><?=GetMessage("REASPEKT_GEOIP_TITLE_YOU_CITY");?></span> - <?=$arResult["GEO_CITY"]["CITY"]?>
					<? } else { ?>
						<span><?=GetMessage("REASPEKT_GEOIP_TITLE_YOU_CITY_NOT_FOUND");?></span>
					<? } ?>
				</div>
                <div class="questionButtonReaspekt reaspekt_clearfix">
					<? if($arResult["GEO_CITY"]["CITY"]) { ?>
                    <div class="questionYesReaspekt" onClick="objJCReaspektGeobase.onClickReaspektSaveCity('N');"><?=GetMessage("REASPEKT_GEOIP_BUTTON_Y");?></div>
                    <div class="questionNoReaspekt cityLinkPopupReaspekt" data-reaspektmodalbox-href="<?=$templateFolder?>/ajax_popup_city.php"><?=GetMessage("REASPEKT_GEOIP_BUTTON_N");?></div>
					<? } else { ?>
					<div class="questionNoReaspekt cityLinkPopupReaspekt full" data-reaspektmodalbox-href="<?=$templateFolder?>/ajax_popup_city.php"><?=GetMessage("CITY_NOT_FOUND_CHANGE");?></div>
					<? } ?>
                </div>
            </div>
			<?endif;?>
        </div>
        
        <script type="text/javascript">
            $('.cityLinkPopupReaspekt').ReaspektModalBox();
            var objJCReaspektGeobase = new JCReaspektGeobase(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
        </script>
    <?else:?>
		<div class="wrapGeoIpReaspekt">
			<strong><?=$arResult["GEO_CITY"]["CITY"]?></strong>, <?=$arResult["GEO_CITY"]["REGION"]?>
		</div>
    <?endif;
endif;?>
<?$frame->beginStub();?>
<?$frame->end();?>

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Sale\Delivery;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Delivery\Restrictions;
use Bitrix\Sale\Delivery\Services;
use Bitrix\Sale\Internals\Input;
use Bitrix\Sale\Delivery\DeliveryLocationTable;
use Bitrix\Sale\Location\LocationTable;

global $USER;
/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

if($USER->IsAuthorized()){
	$ID = $USER->GetID();

	$dbUser = CUser::GetByID($ID);
	if($arUser = $dbUser->Fetch()){
		if(isset($arUser['UF_SALE_PROFILE']) && !empty($arUser['UF_SALE_PROFILE'])){
			$userSaleProfile = json_decode($arUser['UF_SALE_PROFILE'], true);
			$userCountForm = json_decode($arUser['UF_COUNT_FORM'], true);
			
			$arResult['USER']['UF_SALE_PROFILE'] = $userSaleProfile;
			$arResult['JS_DATA']['UF_SALE_PROFILE'] = $userSaleProfile;
			
			$arResult['USER']['UF_COUNT_FORM'] = $userCountForm;
			$arResult['JS_DATA']['UF_COUNT_FORM'] = $userCountForm;
		} 
	}
}



$arPropsToSelect = array('PRE_ORDER', 'PRE_ORDER_DELIVERY_DAYS');

foreach($arResult['JS_DATA']['GRID']['ROWS'] as $rowID => $row){
	
	$dbElement = CIBlockElement::GetByID($row['data']['PRODUCT_ID']);
	
	if($arElement = $dbElement->Fetch()){
		$arResult['JS_DATA']['GRID']['ROWS'][$rowID]['data']['CATALOG_EL_PROPS'] = array();
		
		foreach($arPropsToSelect as $propsToSelect){	
			$dbProp = CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID'], array("sort" => "asc"), array('CODE' => $propsToSelect));
			
			
			if($arProp = $dbProp->Fetch()){
				$arResult['JS_DATA']['GRID']['ROWS'][$rowID]['data']['CATALOG_EL_PROPS'][$propsToSelect] = $arProp;
			}
			
		}
		
	}

}


$arResult['JS_DATA']['DELIVERY_SERVICES'] = array();


$tableId = 'table_delivery_restrictions';

$dbDeliveryList = Delivery\Services\Manager::getActiveList();

foreach($dbDeliveryList as $arDelivery){

	$dbRes = \Bitrix\Sale\Internals\ServiceRestrictionTable::getList(array(
		'filter' => array(
			'=SERVICE_ID' => $arDelivery['ID'],
			'=SERVICE_TYPE' => Delivery\Restrictions\Manager::SERVICE_TYPE_SHIPMENT
		),
		'order' => array('SORT' => 'ASC', 'ID' => 'DESC')
	));
	
	$arRes = $dbRes->fetchAll();
	
	
	foreach($arRes as $key => $res){
		
		if(empty($res['CLASS_NAME']) || !class_exists($res['CLASS_NAME']))
			continue;

		if(!is_subclass_of($res['CLASS_NAME'], 'Bitrix\Sale\Delivery\Restrictions\Base'))
			continue;
		
		
		if($res['CLASS_NAME'] == "\Bitrix\Sale\Delivery\Restrictions\ByLocation"){

			$locationsDeliveryIds = [];
			
			$deliveryLocationIterator = \Bitrix\Sale\Delivery\DeliveryLocationTable::getList([
											'select' => [
												'*',
												'LOCATION_ID' => 'LOCATION.ID',
												'LOCATION_GROUP_ID' => 'GROUP.ID',
												'LOCATION_GROUP_LOCATION_ID' => 'LOCATION_GROUP.LOCATION_ID',
											],
											'filter' => [
												'=DELIVERY_ID' => $arDelivery['ID'],
											],
											'runtime' => [
												'LOCATION_GROUP' => [
													'data_type' => '\Bitrix\Sale\Location\GroupLocationTable',
													'reference' => [
														'=this.LOCATION_GROUP_ID' => 'ref.LOCATION_GROUP_ID',
													],
													'join_type' => 'left'
												],
											]
										]);
			 
			while ($deliveryLocation = $deliveryLocationIterator->fetch()) {
			 
				$locationId = null;
			 
				if (
					isset($deliveryLocation['LOCATION_GROUP_ID'])
					&& $deliveryLocation['LOCATION_GROUP_ID'] > 0
				) {
			 
					if ($deliveryLocation['LOCATION_GROUP_LOCATION_ID'] <= 0) {
						continue;
					}
			 
					$locationId = $deliveryLocation['LOCATION_GROUP_LOCATION_ID'];
				} else if (
					isset($deliveryLocation['LOCATION_ID'])
					&& $deliveryLocation['LOCATION_ID'] > 0
				) {
					$locationId = $deliveryLocation['LOCATION_ID'];
				}
			 
				if (!isset($locationId)) {
					continue;
				}
			 
				$locationsDeliveryIds[] = $locationId;
			 
				$childrenIterator = \Bitrix\Sale\Location\LocationTable::getById($locationId, [
					'select' => [
						'ID',
						'CODE',
						'NAME.NAME',
						'PARENT.NAME.NAME',
					],
					'filter' => [
						'=NAME.LANGUAGE_ID' => LANGUAGE_ID,
						'=PARENT.NAME.LANGUAGE_ID' => LANGUAGE_ID,
					]
				]);
			 
				foreach ($childrenIterator->fetchAll() as $location) {
					$arRes[$key]["PARAMS"][$location['ID']] = $location;
				}
			 
			}
 					
		}
		else{
			if(!empty($res["PARAMS"])){
				$arRes[$key]["PARAMS"] = $res['CLASS_NAME']::prepareParamsValues($res["PARAMS"], $arDelivery['ID']);
			}
			
		}
		
		$arDelivery['RESTRICTIONS'] = $arRes;
	}
	
	$arResult['JS_DATA']['DELIVERY_SERVICES'][] = $arDelivery;
	
}




//dump($arResult['JS_DATA']['DELIVERY_SERVICES']);

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>


<div class="wrap braino-auth-reg">
	<?if($USER->IsAuthorized()):?>
		<div id="regis_ok"></div>

		<p style="color: green;"><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
	<?else:?>

		<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
		
		
		<div class="reg-note-message">
		<?
			if (count($arResult["ERRORS"]) > 0):
				foreach ($arResult["ERRORS"] as $key => $error)
					if (intval($key) == 0 && $key !== 0) 
						$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
			
				ShowError(implode("<br />", $arResult["ERRORS"]));
			
			elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
			?>
				<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
			<?endif?>
		</div>
		
		<?
		if($arResult["BACKURL"] <> ''):
		?>
			<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?
		endif;
		?>

			<div class="titles width-w">
				Впервые у нас? <a href="#">Уже были у нас</a>
			</div>
			
			
			<?foreach ($arResult["SHOW_FIELDS"] as $FIELD):?>
				<?if($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true):?>
					<tr>
						<td><?echo GetMessage("main_profile_time_zones_auto")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><span class="starrequired">*</span><?endif?></td>
						<td>
							<select name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
								<option value=""><?echo GetMessage("main_profile_time_zones_auto_def")?></option>
								<option value="Y"<?=$arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_yes")?></option>
								<option value="N"<?=$arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_no")?></option>
							</select>
						</td>
					</tr>
					<tr>
						<td><?echo GetMessage("main_profile_time_zones_zones")?></td>
						<td>
							<select name="REGISTER[TIME_ZONE]"<?if(!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"'?>>
					<?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name):?>
								<option value="<?=htmlspecialcharsbx($tz)?>"<?=$arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : ""?>><?=htmlspecialcharsbx($tz_name)?></option>
					<?endforeach?>
							</select>
						</td>
					</tr>
				<?elseif($FIELD == 'PROFILE_TYPE'):?>
					<div class="select width-w">
						<select name="REGISTER[<?=$FIELD?>]"> 
							<option value="" disabled selected>Каким типом покупателя вы являетесь?<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>*<?endif?></option>
							<?foreach($arResult['SALE_PROFILES'] as $profile): ?>
								<option value="<?=$profile['PROFILE_TYPE']['ID']?>"><?=$profile['PROFILE_TYPE']['NAME']?></option>
							<?endforeach;?>
						</select>
					</div>
				<?elseif(strpos($FIELD, 'PROFILE_PROP_') !== false ):?>
					
					<?
						switch($arResult['SALE_PROFILES'][2][$FIELD]['TYPE']){
							case 'TEXT':?>
								<div class="input width-w profile-props">
									<input size="30" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" placeholder="<?=$arResult['SALE_PROFILES'][2][$FIELD]['NAME']?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>*<?endif?>" />
								</div>
								<? break;
							case 'TEXTAREA':
								?>
								<div class="textarea width-w profile-props">
									<textarea name="REGISTER[<?=$FIELD?>]" placeholder="<?=$arResult['SALE_PROFILES'][2][$FIELD]['NAME']?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>*<?endif?>">
										<?=$arResult["VALUES"][$FIELD]?>
									</textarea>
								</div>
								
								<?
								break;
						}
					
					?>

				<?else:?>
					<div class="input width-w">
						
						<?
				switch ($FIELD)
				{
					case "PASSWORD":
						?>
						<input size="30" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" class="bx-auth-input" placeholder="<?=GetMessage("REGISTER_FIELD_".$FIELD)?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>*<?endif?>" />
			<?if($arResult["SECURE_AUTH"]):?>
							<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
								<div class="bx-auth-secure-icon"></div>
							</span>
							<noscript>
							<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
								<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
							</span>
							</noscript>
			<script type="text/javascript">
			document.getElementById('bx_auth_secure').style.display = 'inline-block';
			</script>
			<?endif?>
			<?
						break;
					case "CONFIRM_PASSWORD":
						?><input size="30" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" placeholder="<?=GetMessage("REGISTER_FIELD_".$FIELD)?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>*<?endif?>" /><?
						break;

					case "PERSONAL_GENDER":
						?><select name="REGISTER[<?=$FIELD?>]">
							<option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
							<option value="M"<?=$arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_MALE")?></option>
							<option value="F"<?=$arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
						</select><?
						break;

					case "PERSONAL_COUNTRY":
					case "WORK_COUNTRY":
						?><select name="REGISTER[<?=$FIELD?>]"><?
						foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value)
						{
							?><option value="<?=$value?>"<?if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<?endif?>><?=$arResult["COUNTRIES"]["reference"][$key]?></option>
						<?
						}
						?></select><?
						break;

					case "PERSONAL_PHOTO":
					case "WORK_LOGO":
						?><input size="30" type="file" name="REGISTER_FILES_<?=$FIELD?>" placeholder="<?=GetMessage("REGISTER_FIELD_".$FIELD)?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>*<?endif?>" /><?
						break;

					case "PERSONAL_NOTES":
					case "WORK_NOTES":
						?><textarea cols="30" rows="5" name="REGISTER[<?=$FIELD?>]"><?=$arResult["VALUES"][$FIELD]?></textarea><?
						break;
					default:
						if ($FIELD == "PERSONAL_BIRTHDAY"):?><small><?=$arResult["DATE_FORMAT"]?></small><br /><?endif;
						?><input size="30" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" placeholder="<?=GetMessage("REGISTER_FIELD_".$FIELD)?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>*<?endif?>" /><?
							if ($FIELD == "PERSONAL_BIRTHDAY")
								$APPLICATION->IncludeComponent(
									'bitrix:main.calendar',
									'',
									array(
										'SHOW_INPUT' => 'N',
										'FORM_NAME' => 'regform',
										'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
										'SHOW_TIME' => 'N'
									),
									null,
									array("HIDE_ICONS"=>"Y")
								);
							?><?
				}?>
					</div>
				<?endif?>
			<?endforeach?>
			
			
			<?// ********************* User properties ***************************************************?>
			<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
				<tr><td colspan="2"><?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></td></tr>
				<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
				<tr><td><?=$arUserField["EDIT_FORM_LABEL"]?>:<?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?></td><td>
						<?$APPLICATION->IncludeComponent(
							"bitrix:system.field.edit",
							$arUserField["USER_TYPE"]["USER_TYPE_ID"],
							array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
				<?endforeach;?>
			<?endif;?>
			<?// ******************** /User properties ***************************************************?>
			<?
			/* CAPTCHA */
			if ($arResult["USE_CAPTCHA"] == "Y")
			{
				?>
					<div class="input width-w">
						<b><?=GetMessage("REGISTER_CAPTCHA_TITLE")?></b><br>
				
						<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" /><br>
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br>
						<?=GetMessage("REGISTER_CAPTCHA_PROMT")?>:<span class="starrequired">*</span><br>
						<input type="text" name="captcha_word" maxlength="50" value="" /><br>
					</div>
				<?
			}
			/* !CAPTCHA */
			?>
			
			
			<div class="input width-w">
				<?$APPLICATION->IncludeComponent("bitrix:main.userconsent.request", "",
					array(
						"ID" => AGREEMENT_ID,
						"IS_CHECKED" => "Y",
						"AUTO_SAVE" => "N",
						"IS_LOADED" => "Y",
						"ORIGINATOR_ID" => $arResult["AGREEMENT_ORIGINATOR_ID"],
						"ORIGIN_ID" => $arResult["AGREEMENT_ORIGIN_ID"],
						"INPUT_NAME" => $arResult["AGREEMENT_INPUT_NAME"],
						"REPLACE" => array(
							"button_caption" => GetMessage("AUTH_REGISTER"),
							"fields" => array(
								rtrim(GetMessage("AUTH_NAME"), ":"),
								rtrim(GetMessage("AUTH_LAST_NAME"), ":"),
								rtrim(GetMessage("AUTH_LOGIN_MIN"), ":"),
								rtrim(GetMessage("AUTH_PASSWORD_REQ"), ":"),
								rtrim(GetMessage("AUTH_EMAIL"), ":"),
							)
						),
					)
				);?>
			</div>
			
			
			
			<div class="button-panel width-w">
				<input type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
				
				<?if($arResult["AUTH_SERVICES"]):?>
				
				<span>или</span>
				<?
					$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "chef", 
						array(
							"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
							"SUFFIX"=>"form",
						), 
						$component, 
						array("HIDE_ICONS"=>"Y")
					);
					?>
				<?endif;?>
			</div>
			<div class="input width-w">
				<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
				<p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>
			</div>
			<div class="input width-w">
				<a href="" class="forget">Я забыл пароль</a>
			</div>
			
		</form>
	<?endif?>

</div>

<?
	/*if($arResult["AUTH_SERVICES"]){
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
		array(
			"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
			"AUTH_URL"=>$arResult["AUTH_URL"],
			"POST"=>$arResult["POST"],
			"POPUP"=>"Y",
			"SUFFIX"=>"form",
		), 
		$component, 
		array("HIDE_ICONS"=>"Y")
	);
}*/
?>


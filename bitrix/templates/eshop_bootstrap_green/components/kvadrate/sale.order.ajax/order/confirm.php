<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="steps" style="padding-top: 1px;">
<?
if (!empty($arResult["ORDER"]))
{
	?>
	<div class="number">
        <span>3</span>Ваш заказ принят
    </div>
    <div class="wrap_deliv">
    	<div class="confirm_block">
    		Спасибо за заказ!
    		<div class="warning_cur completes">
				Ваш заказ <span>#<?=$_REQUEST['ORDER_ID']?></span> принят<br/>
				Все детали мы отправили Вам на почту<br/>
				<span><?=$arResult['ORDER_PROP']['EMAIL']['VALUE'];?></span>
    		</div>
    		<p>
    			Ожидайте доставку заказа в указанное время.<br/>
    			В день доставки курьер свяжется с Вами.
    		</p>
    		<p class="grey">
    			По всем вопросам, связанным с оформлением<br/>
    			и получением заказа звоните по телефонам<br/>
    			8-800-775-73-53, 8-812-607-38-37
    		</p>
    	</div>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
		?>
		<br /><br />

		<table class="sale_order_full_table">
			<!-- <tr>
				<td class="ps_logo">
					<div class="pay_name"><?=GetMessage("SOA_TEMPL_PAY")?></div>
					<?//=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>
					<div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div><br>
				</td>
			</tr> -->
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
			{
				?>
				<tr>
					<td>
						<?
						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
							</script>
							<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
							<?
							if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
							{
								?><br />
								<?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
								<?
							}
						}
						else
						{
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
							{
								try
								{
									include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
								}
								catch(\Bitrix\Main\SystemException $e)
								{
									if($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
										$message = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
									else
										$message = $e->getMessage();

									echo '<span style="color:red;">'.$message.'</span>';
								}
							}
						}
						?>
					</td>
				</tr>
				<?
			}
			?>
		</table>
	</div>
</div>
		<?
	}
}
else
{
	?>
	<div class="wrap_deliv">
    	<div class="confirm_block">
    		<?=GetMessage("SOA_TEMPL_ERROR_ORDER")?>
    		<div class="warning_cur completes">
				<p>
					<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?><br/>
					<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
				</p>
    		</div>
    		<p class="grey" style="margin-bottom: 30px;">
    			По всем вопросам, связанным с оформлением<br/>
    			и получением заказа звоните по телефону<br/>
    			(495) 640-9-640
    		</p>
    	</div>
    </div>
	<?
}
?>

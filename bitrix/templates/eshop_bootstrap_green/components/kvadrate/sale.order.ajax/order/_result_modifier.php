<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(isset($_REQUEST['ORDER_ID'])):
$arResult['ORDER_INFO'] = CSaleOrder::GetByID($_REQUEST['ORDER_ID']);
$orderP = CSaleOrderPropsValue::GetOrderProps($_REQUEST['ORDER_ID']);
while ($orderProp = $orderP->GetNext()) {
	$arResult['ORDER_PROP'][$orderProp['CODE']] = $orderProp;
}

$arResult['DELIVERY'] = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();
$dbBasketItems = CSaleBasket::GetList(
        array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
        array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => $_REQUEST['ORDER_ID']
            ),
        false,
        false,
        array()
    );
while ($arItems = $dbBasketItems->Fetch())
{
    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
    {
        CSaleBasket::UpdatePrice($arItems["ID"], 
                                 $arItems["CALLBACK_FUNC"], 
                                 $arItems["MODULE"], 
                                 $arItems["PRODUCT_ID"], 
                                 $arItems["QUANTITY"]);
        $arItems = CSaleBasket::GetByID($arItems["ID"]);
    }
    $arItems['SUM'] = CurrencyFormat($arItems['PRICE'], $arItems['CURRENCY']);
    $arResult['BASKET_ITEMS'][] = $arItems;
}
else:
if(!session_id()) session_start();
global $USER;
if (!CModule::IncludeModule('reaspekt.geobase')) {
    ShowError("Error! Module no install");
    return;
}

$arData = ReaspGeoIP::GetAddr();
if(!isset($_POST['is_ajax_post'])) {
    foreach ($arResult["ORDER_PROP"]["USER_PROPS_N"] as $key => $val) {
        if($val['CODE'] == 'CITY' && empty($val['VALUE'])) {
            $arResult["ORDER_PROP"]["USER_PROPS_N"][$key]['VALUE'] = $arData['ID'];
            break;
        }
    }
    foreach ($arResult["ORDER_PROP"]["USER_PROPS_Y"] as $key => $val) {
        if($val['CODE'] == 'CITY' && empty($val['VALUE'])) {
            $arResult["ORDER_PROP"]["USER_PROPS_Y"][$key]['VALUE'] = $arData['ID'];
            break;
        }
    }
}

// $res = CIBlockElement::GetList(Array(), Array('ID' => $arID, '!PROPERTY_TIME_OF_ORDER' => false), false, false, Array('PROPERTY_TIME_OF_ORDER'));
// if($result = $res->GetNext()) {
//     if($result['PROPERTY_TIME_OF_ORDER_VALUE'] > 1) $countDay = $result['PROPERTY_TIME_OF_ORDER_VALUE'];
// }
$checkOnline = false;
foreach ($arResult['BASKET_ITEMS'] as $key => $value) {
    if(!empty($value['PROPERTY_PAY_ONLINE_VALUE'])) {
        $checkOnline = true;
        break;
    }
}

if($checkOnline) {
    foreach ($arResult['PAY_SYSTEM'] as $key => $pay_s) {
        if($pay_s['ID'] == 1 || $pay_s['ID'] == 11 || $pay_s['ID'] == 12) {
            unset($arResult['PAY_SYSTEM'][$key]);
        }
    }
    if($_POST['PAY_SYSTEM_ID'] != 3 && $_POST['PAY_SYSTEM_ID'] != 4) {
        foreach ($arResult['PAY_SYSTEM'] as $key => $pay_s) {
            if($pay_s['ID'] == 4) {
                $arResult['PAY_SYSTEM'][$key]['CHECKED'] = 'Y';
                $_POST['PAY_SYSTEM_ID'] = 4;
                break;
            }
        }
    }
}

$detectDenidedCityOrRegion = false;
$dataDelivery = array();
$dataFindeDelivery = null;
if(!empty($_POST)) {
    $idProp = array();
    foreach ($_POST as $key => $value) {
        if(strpos($key, 'ORDER_PROP_') !== false) {
            $idProp[str_replace('ORDER_PROP_', '', $key)] = str_replace('ORDER_PROP_', '', $key);
        }
    }
    $saleIn = CSaleOrderProps::GetList(array(), array('ID' => $idProp), false, false, array());
    while ($saleInfo = $saleIn->Fetch()) {
        $_POST[$saleInfo['CODE']] = $saleInfo;
    }
    $_POST['ERROR'] = $arResult["ERROR"];
    $_POST['FIND_INFO_DELIVERY'] = null;

    if(!empty($_POST['ORDER_PROP_'.$_POST['CITY']['ID']])) {
        $arSort = Array('NAME', 'PROPERTY_MIN_PRICE', 'PROPERTY_FREE_DELIV', 'PROPERTY_COST_DELIV');
        $ci = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array('IBLOCK_ID' => 12, 'ACTIVE' => 'Y', 'SECTION_ID' => 371), false, false, $arSort);
        while($city = $ci->GetNext()) {
            $dataDelivery['city'][$city['NAME']]['min'] = $city['PROPERTY_MIN_PRICE_VALUE'];
            $dataDelivery['city'][$city['NAME']]['free'] = $city['PROPERTY_FREE_DELIV_VALUE'];
            $dataDelivery['city'][$city['NAME']]['cost'] = $city['PROPERTY_COST_DELIV_VALUE'];                    
        }
        $reg = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array('IBLOCK_ID' => 12, 'ACTIVE' => 'Y', 'SECTION_ID' => 372), false, false, $arSort);
        while($region = $reg->GetNext()) {
            $dataDelivery['region'][$region['NAME']]['min'] = $region['PROPERTY_MIN_PRICE_VALUE'];
            $dataDelivery['region'][$region['NAME']]['free'] = $region['PROPERTY_FREE_DELIV_VALUE'];
            $dataDelivery['region'][$region['NAME']]['cost'] = $region['PROPERTY_COST_DELIV_VALUE'];
        }
        if(!preg_match('/\D/', $_POST['ORDER_PROP_'.$_POST['CITY']['ID']])) {
            $c = ReaspGeoIP::SelectCityId($_POST['ORDER_PROP_'.$_POST['CITY']['ID']]);
            if(isset($dataDelivery['city'][$c['CITY']])) {
                $detectDenidedCityOrRegion = true;
                $dataFindeDelivery = $dataDelivery['city'][$c['CITY']];
            }
            elseif(!$detectDenidedCityOrRegion && isset($dataDelivery['region'][$c['REGION']])) {
                $detectDenidedCityOrRegion = true;
                $dataFindeDelivery = $dataDelivery['region'][$c['REGION']];
            }
        }
        $_POST['INFO_DELIVERY'] = $dataDelivery;
        $_POST['FIND_INFO_DELIVERY'] = $dataFindeDelivery;
    }
}
//check delivery
/*foreach ($arResult["DELIVERY"] as $key => $delivery) {
    if(!$detectDenidedCityOrRegion && $delivery['ID'] == 2) {
        unset($arResult["DELIVERY"][$key]);
        continue;
    } elseif($detectDenidedCityOrRegion && $delivery['ID'] == 2) {
        if(!is_null($_POST['FIND_INFO_DELIVERY'])) {
            $arResult["DELIVERY"][$key]['PRICE'] = (floatval($arResult['ORDER_TOTAL_PRICE']) > floatval($_POST['FIND_INFO_DELIVERY']['free']))?0:$_POST['FIND_INFO_DELIVERY']['cost'];
        }
    } elseif(!$detectDenidedCityOrRegion && $delivery['ID'] == 3) {
        $arResult["DELIVERY"][$key]['CHECKED'] = "Y";
        $arResult['CHECKED_DELIVERY'] = $delivery['ID'];
    } elseif(!empty($_POST) && empty($_POST['ORDER_PROP_'.$_POST['CITY']['ID']]) && $delivery['ID'] == 23) {
        unset($arResult["DELIVERY"][$key]);
        continue;
    } else {
        if(!isset($delivery['PRICE'])) {
            $del = CSaleDelivery::GetByID($delivery['ID']);
            $arResult["DELIVERY"][$key]['PRICE'] = $del['PRICE'];
        }
    }
        if($delivery['CHECKED'] == 'Y') $arResult['CHECKED_DELIVERY'] = $delivery['ID'];
}*/
 //print_r($arResult);
endif;?>
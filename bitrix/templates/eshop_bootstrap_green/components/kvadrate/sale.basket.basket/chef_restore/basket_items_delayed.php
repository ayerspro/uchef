<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */

$bPriceType  = false;
$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
?>
<div id="basket_items_delayed" class="bx_ordercart_order_table_container" style="display:none">
	<table id="delayed_items" class="data-table">

		<tbody>
			<?
			$skipHeaders = array('PROPS', 'DELAY', 'DELETE', 'TYPE');

			foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
					
					$pr = CIBlockElement::GetProperty(5, $arItem['PRODUCT_ID'], Array(), Array());
					while($property = $pr->Fetch()) $arItem['PROPS'][$property['CODE']] = $property;

					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
						$arHeaders[] = $arHeader["id"];
					endforeach;
					if ($arItem["CAN_BUY"] == "Y" && $arItem["DELAY"] == "Y"):
					?>
					<tr id="<?=$arItem["ID"]?>"
						 data-item-name="<?=$arItem["NAME"]?>"
						 data-item-brand="<?=$arItem[$arParams['BRAND_PROPERTY']."_VALUE"]?>"
						 data-item-price="<?=$arItem["PRICE"]?>"
						 data-item-currency="<?=$arItem["CURRENCY"]?>"
					>
						<?if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
							$url = $arItem["PREVIEW_PICTURE_SRC"];
						elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
							$url = $arItem["DETAIL_PICTURE_SRC"];
						else:
							$url = $templateFolder."/images/no_photo.png";
						endif;?>
						<td class="images">
							<span>
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
								<img src="<?=$url?>" alt="">
							<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
							</span>
						</td>
						<td class="name_tr">
							<span>
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" class="name-product"><?=$arItem["NAME"]?>
									<br/><br/>
									<b>
										<?if(!empty($arItem['PROPS']['ARTNUMBER']['VALUE'])):?>
											<?=$arItem['PROPS']['ARTNUMBER']['NAME']?> <?=$arItem['PROPS']['ARTNUMBER']['VALUE']?>
										<?endif;?>
									</b>
								</a>
								<?endif;?>
								
									<div class="mobile-block"><?=$arItem["PRICE_FORMATED"]?><br>
										<span class="catalog-price">
											<?=$arItem["PRICE_FORMATED"]?></span> <?if($arItem['PROPS']['SIZE']['VALUE']):?><sup>/<?=$arItem['PROPS']['SIZE']['VALUE']?></sup><?endif;?>	
									</div>
							</span>
						</td>
						<td>
							<span>
								<b class="old-price">
									<?if($arItem['PRICE'] != $arItem['BASE_PRICE']):?>
										<?=floatval($arItem['BASE_PRICE'])?> &#8381;
									<?endif;?>
								</b>
								<span class="catalog-price" id="current_price_<?=$arItem["ID"]?>"><?=$arItem["PRICE"]?> &#8381;</span> <?if($arItem['PROPS']['SIZE']['VALUE']):?><sup>/<?=$arItem['PROPS']['SIZE']['VALUE']?></sup><?endif;?>
							</span>
						</td>
						<td style="display: none;">
							<span>
							<?
								$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
								$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
								$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
							?>
							<div class="add_form_counter-line">
								<a class="MinusLists" href="javascript:void(0)" onclick="setQuantity(<?=$arItem["ID"]?>, <?=round($arItem["MEASURE_RATIO"])?>, 'down', <?=$useFloatQuantityJS?>);">-</a> 
									<input
										type="text"
										size="3"
										id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
										name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
										maxlength="18"
										style="max-width: 50px"
										value="<?=round($arItem["QUANTITY"])?>"
										onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=round($ratio)?>, <?=round($useFloatQuantityJS)?>)"
									>
								<a class="PlusLists" href="javascript:void(0)" onclick="setQuantity(<?=$arItem["ID"]?>, <?=round($arItem["MEASURE_RATIO"])?>, 'up', <?=$useFloatQuantityJS?>);">+</a>	
							</div>
							</span>
							<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
						</td>
						<td class="full_p">
							<span id="full_price_<?=$arItem['ID']?>">Всего: <?=$arItem['SUM']?></span>
						</td>
						<td class="del_product">
							<span style="justify-content: center;">
								<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["add"])?>" class="btn-buys"></a>
								<a href="javascript:void(0)" class="delete-list" data-id="<?=$arItem['ID']?>" onclick="return deleteProductRow(this)">
									×
								</a>
							</span>
						</td>
					</tr>
					<?
					endif;
				endforeach;
			?>
		</tbody>

	</table>
</div>
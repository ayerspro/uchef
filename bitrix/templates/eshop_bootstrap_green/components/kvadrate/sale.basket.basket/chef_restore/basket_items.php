<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
use Bitrix\Sale\DiscountCouponsManager;
if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;
$USER_ID = $GLOBALS["USER"]->GetID();
if ($normalCount > 0):

?>

<div id="basket_items_list">
	<div class="bx_ordercart_order_table_container">
		<table id="basket_items" class="data-table">
			<tbody>

				<tr style="display: none;"></tr>
				<?
				$skipHeaders = array('PROPS', 'DELAY', 'DELETE', 'TYPE');
				
				foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
					
					$pr = CIBlockElement::GetProperty(5, $arItem['PRODUCT_ID'], Array(), Array());
					while($property = $pr->Fetch()) $arItem['PROPS'][$property['CODE']] = $property;

					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
						$arHeaders[] = $arHeader["id"];
					endforeach;
					if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
					?>
					<tr id="<?=$arItem["ID"]?>"
						 data-item-name="<?=$arItem["NAME"]?>"
						 data-item-brand="<?=$arItem[$arParams['BRAND_PROPERTY']."_VALUE"]?>"
						 data-item-price="<?=$arItem["PRICE"]?>"
						 data-item-currency="<?=$arItem["CURRENCY"]?>"
					>
						<?if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
							$url = $arItem["PREVIEW_PICTURE_SRC"];
						elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
							$url = $arItem["DETAIL_PICTURE_SRC"];
						else:
							$url = $templateFolder."/images/no_photo.png";
						endif;?>
						<td class="images">
							<span>
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
								<img src="<?=$url?>" alt="">
							<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
							</span>
						</td>
						<td class="name_tr">
							<span>
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" class="name-product"><?=$arItem["NAME"]?>
									<br/><br/>
									<?if(!empty($arItem['PROPS']['ARTNUMBER']['VALUE'])):?>
										<b>
											<?=$arItem['PROPS']['ARTNUMBER']['NAME']?> <?=$arItem['PROPS']['ARTNUMBER']['VALUE']?>
										</b>
									<?endif;?>
								</a>
								<?endif;?>
								
									<div class="mobile-block"><?=$arItem["PRICE_FORMATED"]?><br>
										<span class="catalog-price">
											<?=$arItem["PRICE_FORMATED"]?></span> <?if($arItem['PROPS']['SIZE']['VALUE']):?><sup>/<?=$arItem['PROPS']['SIZE']['VALUE']?></sup><?endif;?>	
									</div>
							</span>
						</td>
						<td>
							<span>
								<b class="old-price">
									<?if($arItem['PRICE'] != $arItem['BASE_PRICE']):?>
										<?=floatval($arItem['BASE_PRICE'])?> &#8381;
									<?endif;?>
								</b>
								<span class="catalog-price" id="current_price_<?=$arItem["ID"]?>"><?=$arItem["PRICE"]?> &#8381;</span> <?if($arItem['PROPS']['SIZE']['VALUE']):?><sup>/<?=$arItem['PROPS']['SIZE']['VALUE']?></sup><?endif;?>
							</span>
						</td>
						<td>
							<span>
							<?
								$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
								$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
								$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
							?>
							<div class="add_form_counter-line">
								<a class="MinusLists" href="javascript:void(0)" onclick="setQuantity(<?=$arItem["ID"]?>, <?=round($arItem["MEASURE_RATIO"])?>, 'down', <?=$useFloatQuantityJS?>);">-</a> 
									<input
										type="text"
										size="3"
										id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
										name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
										maxlength="18"
										style="max-width: 50px"
										value="<?=round($arItem["QUANTITY"])?>"
										onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=round($ratio)?>, <?=round($useFloatQuantityJS)?>)"
									>
								<a class="PlusLists" href="javascript:void(0)" onclick="setQuantity(<?=$arItem["ID"]?>, <?=round($arItem["MEASURE_RATIO"])?>, 'up', <?=$useFloatQuantityJS?>);">+</a>	
							</div>
							</span>
							<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
						</td>
						<td class="full_p">
							<span id="full_price_<?=$arItem['ID']?>">Всего: <?=$arItem['SUM']?></span>
						</td>
						<td class="del_product">
							<span>
								<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" class="delete-list">
									×
								</a>
							</span>
						</td>
					</tr>
					<?
					endif;
				endforeach;
				?>
			</tbody>
		</table>
	</div>
	<input type="hidden" id="column_headers" value="<?=htmlspecialcharsbx(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=htmlspecialcharsbx($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />

	<div class="bx_ordercart_order_pay">
		<div class="block_s">
			<span class="all_sum">
				Итого <span id="all_sum_bottom"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span>
			</span>
			<a href="javascript:void(0)" onclick="checkOut();" class="btn btn-default btn-block">Оформить заказ</a>
		</div>
	</div>
</div>
<?
else:
?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
endif;
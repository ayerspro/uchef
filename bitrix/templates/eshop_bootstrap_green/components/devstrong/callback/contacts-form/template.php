<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<div class="ds-contacts-form" id="ds-callback">
	<div class="ds-form" id="<?=$arParams['COMPONENT_ID'];?>">
		<?=bitrix_sessid_post();?>
		<input type="hidden" name="DSCB_COMPONENT_ID" value="<?=$arParams['COMPONENT_ID'];?>"/>
		<? if(in_array('DSCB_NAME', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-name"><?=$arParams['NAME_TITLE'];?><? if(in_array('DSCB_NAME', $arParams['REQUIRED_FIELDS'])) { ?> <span class="ds-required">*</span><? } ?></label>
			<input class="contacts-form__input" type="text" id="<?=$arParams['COMPONENT_ID'];?>-name" name="DSCB_NAME"/>
		</div>
		<? } ?>
		<? if(in_array('DSCB_PHONE', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-phone"><?=$arParams['PHONE_TITLE'];?><? if(in_array('DSCB_PHONE', $arParams['REQUIRED_FIELDS'])) { ?> <span class="ds-required">*</span><? } ?></label>
			<input class="contacts-form__input" data-type="ds-phone" type="text" id="<?=$arParams['COMPONENT_ID'];?>-phone" name="DSCB_PHONE"/>
		</div>
		<? } ?>
		<? if(in_array('DSCB_EMAIL', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-email"><?=$arParams['EMAIL_TITLE'];?><? if(in_array('DSCB_EMAIL', $arParams['REQUIRED_FIELDS'])) { ?> <span class="ds-required">*</span><? } ?></label>
			<input class="contacts-form__input" type="text" id="<?=$arParams['COMPONENT_ID'];?>-email" name="DSCB_EMAIL"/>
		</div>
		<? } ?>
		<? if(in_array('DSCB_COMMENT', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-comment"><?=$arParams['COMMENT_PH'];?><? if(in_array('DSCB_COMMENT', $arParams['REQUIRED_FIELDS'])) { ?> <span class="ds-required">*</span><? } ?></label>
			<textarea class="contacts-form__input" name="DSCB_COMMENT" id="<?=$arParams['COMPONENT_ID'];?>-comment"></textarea>
		</div>
		<? } ?>
		<div class="row">
			<div class="col-sm-6">
				<? if($arParams['COMPONENT_ID']) { ?>
				<div class="ds-str">
					<div class="recaptcha-err">
						<div class="recaptcha-err__text">Подтвердите, что вы не робот</div>
						<div id="ds-contacts-recaptcha"></div>
					</div>
				</div>
				<? } ?>
			</div>
			<div class="col-sm-6">
				<div class="ds-str ds-str-btn text-right">
					<div class="ds-cb-send btn brown-bd-btn" data-type="ds-cb-send"><?=$arParams['SEND_BTN_TITLE'];?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var DSCB = <?=CUtil::PhpToJSObject(array('SUCCESS_MESS' => $arParams['SUCCESS_MESS']));?>
</script>
<?
$arTemplateParameters['IBLOCK_ID'] = array(
	'PARENT' => '',
	'NAME' => 'ID инфоблока сообщений',
	'TYPE' => 'TEXT',
	'MULTIPLE' => 'N',
	'REFRESH' => 'N',
	'DEFAULT' => ''
);

$arTemplateParameters['USE_RECAPTCHA'] = array(
	"NAME" => 'Использовать ReCaptcha',
	"TYPE" => "CHECKBOX",
	"DEFAULT" => "N",
);

$arTemplateParameters['RECAPTCHA_SECRET'] = array(
	'PARENT' => '',
	'NAME' => 'Секретный ключ ReCaptcha',
	'TYPE' => 'TEXT',
	'MULTIPLE' => 'N',
	'REFRESH' => 'N',
	'DEFAULT' => ''
);


?>
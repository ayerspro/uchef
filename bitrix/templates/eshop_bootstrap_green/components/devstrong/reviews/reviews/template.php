<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<? $this->setFrameMode(true); ?>

<div class="review-form">
	<div class="ds-form review-form__form" id="<?=$arParams['COMPONENT_ID'];?>">
		<?=bitrix_sessid_post();?>
		<input type="hidden" name="DSCB_COMPONENT_ID" value="<?=$arParams['COMPONENT_ID'];?>"/>
		<? if(in_array('DSCB_NAME', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-name"><?=GetMessage('DS_CALLBACK_NAME');?><? if(in_array('DSCB_NAME', $arParams['REQUIRED_FIELDS'])) { ?> *<? } ?></label>
			<input class="review-form__input" type="text" id="<?=$arParams['COMPONENT_ID'];?>-name" name="DSCB_NAME" placeholder="<?=GetMessage('DS_CALLBACK_NAME');?>"/>
		</div>
		<? } ?>
		<? if(in_array('DSCB_PHONE', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-phone"><?=GetMessage('DS_CALLBACK_PHONE');?><? if(in_array('DSCB_PHONE', $arParams['REQUIRED_FIELDS'])) { ?> *<? } ?></label>
			<input class="review-form__input" data-type="ds-phone" type="text" id="<?=$arParams['COMPONENT_ID'];?>-phone" name="DSCB_PHONE" placeholder="<?=GetMessage('DS_CALLBACK_PHONE');?>"/>
		</div>
		<? } ?>
		<? if(in_array('DSCB_EMAIL', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-email"><?=GetMessage('DS_CALLBACK_EMAIL');?><? if(in_array('DSCB_EMAIL', $arParams['REQUIRED_FIELDS'])) { ?> *<? } ?></label>
			<input class="review-form__input" type="text" id="<?=$arParams['COMPONENT_ID'];?>-email" name="DSCB_EMAIL" placeholder="<?=GetMessage('DS_CALLBACK_EMAIL');?>"/>
		</div>
		<? } ?>
		<? if(in_array('DSCB_RELATED', $arParams['FIELDS']) && $arResult['RELATED']) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-spec"><?=GetMessage('DS_CALLBACK_RELATED');?><? if(in_array('DSCB_RELATED', $arParams['REQUIRED_FIELDS'])) { ?> *<? } ?></label>
			<select name="DSCB_RELATED">
				<option value=""><?=GetMessage('DS_CALLBACK_RELATED_SELECT');?></option>
				<?foreach($arResult['RELATED'] as $spec) { ?>
				<option value="<?=$spec['ID']?>"><?=$spec['NAME']?></option>
				<? } ?>
			</select>
		</div>
		<? } ?>
		<? if(in_array('DSCB_COMMENT', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-comment"><?=GetMessage('DS_CALLBACK_COMMENT');?><? if(in_array('DSCB_COMMENT', $arParams['REQUIRED_FIELDS'])) { ?> *<? } ?></label>
			<textarea class="review-form__input" name="DSCB_COMMENT" id="<?=$arParams['COMPONENT_ID'];?>-comment" placeholder="<?=GetMessage('DS_CALLBACK_COMMENT');?>"></textarea>
		</div>
		<? } ?>
		<div class="row">
			<div class="col-sm-6">
				<? if($arParams['COMPONENT_ID']) { ?>
				<div class="ds-str">
					<div class="recaptcha-err">
						<div class="recaptcha-err__text">Подтвердите, что вы не робот</div>
						<div id="ds-review-recaptcha"></div>
					</div>
				</div>
				<? } ?>
			</div>
			<div class="col-sm-6">
				<div class="ds-str ds-str-btn text-right">
					<div class="ds-rv-send btn brown-bd-btn" data-type="ds-rv-send"><?=GetMessage('DS_CALLBACK_SEND');?></div>
				</div>
			</div>
		</div>
		
		
		<?/*?>
		<div class="ds-str">
			<div class="policy-checkbox-wrap">
				<input id="policy-checkbox" type="checkbox" class="policy-checkbox" checked="checked" />
				<label for="policy-checkbox"></label>
				<span>Согласен на <span data-fancybox data-type="ajax" data-src="/about/personal/?dsajax=Y" href="javascript:;" class="policy-popup-btn">обработку персональных данных</span>.</span>
			</div>
		</div>
		<?*/?>
	</div>
</div>
<script type="text/javascript">
	var DSRV = <?=CUtil::PhpToJSObject(array('SUCCESS_MESS' => $arParams['SUCCESS_MESS']));?>
</script>
<?

$relatedIBlock = 2;

CModule::IncludeModule('iblock');

$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>$relatedIBlock, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array('NAME' => 'ASC'), $arFilter, false, false, $arSelect);

$arResult['RELATED'] = array();

while($ob = $res->GetNextElement()) {
	
	$arFields = $ob->GetFields();
	$arResult['RELATED'][] = $arFields;
}

?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
global $APPLICATION;

if(isset($arResult['arResult'])) {
   $arResult =& $arResult['arResult'];
         // подключаем языковой файл
   global $MESS;
   include_once(GetLangFileName(dirname(__FILE__).'/lang/', '/template.php'));
} else {
   return;
}

?>

<h2 class="titles-leader">
	Лидеры продаж
</h2>
<div class="products-items popular-items">
<?
$arrFavorites = unserialize($APPLICATION->get_cookie("ANONIM_PROD_FAVORITES"));
$arBasketItems = array();
$arBasketItemsAll = array();

$dbBasketItems = CSaleBasket::GetList(
	array(
			"NAME" => "ASC",
			"ID" => "ASC"
		),
	array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
	false,
	false,
	array()
);
	
while ($arItems = $dbBasketItems->Fetch()) {
	
	if (strlen($arItems["CALLBACK_FUNC"]) > 0) {
		
		CSaleBasket::UpdatePrice($arItems["ID"],
								 $arItems["CALLBACK_FUNC"],
								 $arItems["MODULE"],
								 $arItems["PRODUCT_ID"],
								 $arItems["QUANTITY"]);
		$arItems = CSaleBasket::GetByID($arItems["ID"]);
	}

	$arBasketItemsAll[$arItems['PRODUCT_ID']] = $arItems;
	$arBasketItems[] = $arItems['PRODUCT_ID'];
}
	
foreach ($arResult['ITEMS'] as $arItem) {

	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$APPLICATION->IncludeComponent(
		'bitrix:catalog.item',
		'grid',
		array(
			'RESULT' => array(
				'ITEM' => $arItem,
				'AREA_ID' => $strMainID,
				'BASKET_ITEMS' => $arBasketItems,
				'BASKET_ITEMS_ALL' => $arBasketItemsAll,
				'FAVORITES_ITEMS' => $arrFavorites,
			),
			'PARAMS' => $arParams
				+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$arItem['IBLOCK_ID']])
		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);
}
?>
</div>


<?
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
CJSCore::Init(array('popup'));
?>
<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== TRUE) die();

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$isMain = ($_SERVER['REQUEST_URI'] == '/') ? TRUE : FALSE;

if ($isMain){
	$items = array();

	foreach ($arResult['ITEMS'] as $item) {  
	$res = CIBlockSection::GetByID($item["IBLOCK_SECTION_ID"]);
		if($ar_res = $res->GetNext()){
			$items[] = $ar_res;
		}
	}
}

$arResult["SECTIONS"] = $items;

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="reviews-list">
	<div class="flexible hcenter">
		<?if(isset($arResult["SECTIONS"])){?>
			<?foreach ($arResult["SECTIONS"] as $section){?>
				<div class="tag-link"><a href="<?=$section["SECTION_PAGE_URL"]?>"><?=$section["NAME"]?></a></div>
			<?}?>
		<?}?>
	</div>
	
	<? $zCnt = 1;
	foreach($arResult["ITEMS"] as $arItem):?>
		<div class="row reviews-block">
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>

			<div class="col-md-2">
				<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
						<img
							class="preview_picture img-responsive"
							border="0"
							src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
							width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
							height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
							alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
							title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						/>

				<?elseif($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["DETAIL_PICTURE"])):?>
						<img
							class="preview_picture img-responsive"
							border="0"
							src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
							width="<?=$arItem["DETAIL_PICTURE"]["WIDTH"]?>"
							height="<?=$arItem["DETAIL_PICTURE"]["HEIGHT"]?>"
							alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"
							title="<?=$arItem["DETAIL_PICTURE"]["TITLE"]?>"
						/>
				<?else:?>
					<i class="fa fa-comments"></i>
				<?endif;?>
			</div>
			<div class="col-md-10">
				<div class="reviews-block-title"><?=$arItem["NAME"]?></div>
				<div class="reviews-block-text">
					<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
						<?=$arItem["PREVIEW_TEXT"];?>
					<?elseif($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["DETAIL_TEXT"]):?>
						<?=$arItem["DETAIL_TEXT"];?>
					<?endif;?>
				</div>		
			</div>

		</div>
	<?endforeach;?>
</div>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="slider-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="slider-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);">
		<div class="clearfix">
			<div class="col-md-4 text_area text-center">
				<div class="inner">
					<div class="logo"><img src="/bitrix/templates/eshop_bootstrap_green/components/bitrix/news.list/main_slider/images/logo.png" alt=""></div>
					<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
						<h3><?=$arItem["PREVIEW_TEXT"];?></h3>
					<?endif;?>
					<div>
					<? if (!empty($arItem["PROPERTIES"]["SL_BUTTON_LINK"]["VALUE"])) :?>
					<a href="<?=$arItem["PROPERTIES"]["SL_BUTTON_LINK"]["VALUE"];?>" class="btn"><?=$arItem["PROPERTIES"]["SL_BUTTON_TEXT"]["VALUE"];?></a>
					<?endif;?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?endforeach;?>
</div>
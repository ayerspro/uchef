<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h1 class="text-center catalog-title"><?=$arResult['SECTION']['PATH'][0]["NAME"]?></h1>
<div class="recipes-list">
	<div class="flexible hcenter">
		<?if(isset($arResult["SECTIONS"])){?>
			<?foreach ($arResult["SECTIONS"] as $section){?>
				<div class="tag-link"><a href="<?=$section["SECTION_PAGE_URL"]?>"><?=$section["NAME"]?></a></div>
			<?}?>
		<?}?>
	</div>
	<div class="row">
	<? $zCnt = 1;
	foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="col-md-3 recipe-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="recipe-block">
			<div>
			<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
				<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="position: relative; width: 100%; display: block; overflow: hidden;">
					<img
						class="preview_picture img-responsive"
						border="0"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					/>
					<div class="recipe-overlay flexible vcenter">
						<div class="recipe-name"><?echo TruncateText($arItem["NAME"], 26);?></div>
						<div class="recipe-params">
							<div class="recipe-icon diff lvl-<?=$arItem["PROPERTIES"]["DIFFICULTY"]["VALUE"]?>">
								<span>Сложность<br>блюда</span>
							</div>
							<div class="recipe-icon time">
								<small><?=$arItem["PROPERTIES"]["PREPARE_TIME"]["VALUE"]?></small>
								<span>Время<br>готовки (мин.)</span>
							</div>
						</div>
					</div>
					</a>
				<?else:?>
					<img
						class="preview_picture img-responsive"
						border="0"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						/>
				<?endif;?>
			<?endif?>
			<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
				<div class="recipe-description">
					<?echo TruncateText($arItem["PREVIEW_TEXT"], 260);?>
				</div>
			<?endif;?>
			</div>
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="more-link"><?=getMessage("ELEMENT_DETAIL")?></a>
			</div>
		</div>
		<? if ($zCnt % 4 == 0) echo '</div><div class="row">'; ?>
	<? $zCnt++;
	endforeach;?>
	</div>
</div>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if($arParams['IS_CARD']) { ?>
		<div class="detail__coupons" <? if(!$arResult["ITEMS"]) { ?>style="display: none;"<? } ?>>
			<h2 class="title-element-bottom text-left">Купоны на скидку</h2>
			<div class="detail__coupons__wrap">
<? } ?>
<? if($arParams['IS_HOME']) { ?>
<section class="main-coupons__wrap" <? if(!$arResult["ITEMS"]) { ?>style="display: none;"<? } ?>>
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="section-h2">
				Купоны на скидку
			</div>
		</div>
	</div>
	<div class="coupons__wrap">
<? } ?>
<div class="coupons-list<? if($arParams['IS_SLIDER']) { ?> coupons-list__slider items-slider<? } ?>">
	<? if(!$arParams['IS_SLIDER']) { ?>
	<div class="row coupons-list-row">
	<? } ?>
	<?foreach($arResult["ITEMS"] as $arItem) { ?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 coupons-list-col">
			<?$frame = $this->createFrame()->begin('');?>
			<div class="coupons-list__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-coupon="<?=$arItem['ID'];?>">
				<div class="coupons-list__item-img" style="background-image:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');">
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""/>
				</div>
				<div class="coupons-list__item-info">
					<div class="coupons-list__item-name"><?=$arItem["NAME"];?></div>
					<div class="coupons-list__item-text"><?=$arItem["PREVIEW_TEXT"];?></div>
					<div class="coupons-list__item-btn-wrap">
						<div class="coupons-list__item-btn btn btn-default <? if($arResult['NEED_AUTORIZE']) { ?> js-coupon__item-autorize<? } else { ?> js-coupon__item-copy<? } ?>" <? if(!$arResult['NEED_AUTORIZE']) { ?> data-clipboard-text="<?=$arItem['COUPON'];?>"<? } ?>>Взять купон</div>
					</div>
				</div>
			</div>
			<?$frame->end();?>
		</div>
	<? } ?>
	<? if(!$arParams['IS_SLIDER']) { ?>
	</div>
	<? } ?>
</div>
<? if($arParams['IS_HOME']) { ?>
	</div>
</div>
 </section>
<? } ?>
<? if($arParams['IS_CARD']) { ?>
			</div>
		</div>
<? } ?>
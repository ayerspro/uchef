<?

global $USER;
$arResult['NEED_AUTORIZE'] = !$USER->IsAuthorized();

if(!$arResult['NEED_AUTORIZE']) {
	
	foreach($arResult["ITEMS"] as &$arItem) { 

		$coupon = $arItem['PROPERTIES']['COUPON']['VALUE'];
		$dbCoupon = \Bitrix\Sale\Internals\DiscountCouponTable::getList(array(
		
			'filter' => array('ID' => $coupon),
		)); 
			
		if($arCoupon = $dbCoupon->Fetch()) { 
			
			$arItem['COUPON'] = $arCoupon['COUPON'];
		}
	}
	unset($arItem);
}

?>
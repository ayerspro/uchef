<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="recipes-list-element slide col-md-5">
<?if(!empty($arResult["ITEMS"])):?>
<div class="title-element-bottom">Приготовьте дома</div>
	<div class="prepare-home">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<div class="prepare-item">
				<div class="img">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
					</a>
				</div>
				<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="name">
					<?=$arItem['NAME']?>
				</a>
				<a href="/retsepty/" class="link">Смотреть все рецепты <i class="fa fa-angle-right" aria-hidden="true"></i></a>
			</div>
		<?endforeach;?>
	</div>
<?endif;?>
</div>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component  */
$this->setFrameMode(TRUE);

Bitrix\Main\Loader::includeModule('services.tables');
?>

<?
$viewCatalogClass = 'card-list-plit';
if ($_COOKIE['view-catalog'] === 'list') {
    $viewCatalogClass = 'card-list-list';
}


?>
<div class="section-top-links" style="overflow:auto;">
<?
					 


if(!empty($arResult['UF_HAVE_ACTION']) && $arResult['UF_HAVE_ACTION']==1):
	
	$expPath = explode('/',$APPLICATION->GetCurDir());
	
	if(count($expPath)<6):
	
		?><div class="top-links"><?
		//foreach($arResult['PATH'] as $path):
		
			$path = $arResult['PATH'][count($arResult['PATH'])-1];
		
			?><a href="<?=str_replace('catalog', 'action-new', $path['SECTION_PAGE_URL'])?>"><?=$path['NAME']?> со скидкой</a>&nbsp&nbsp<?
		
		//endforeach;
		?></div><?
	
	else:
	
		if($USER->IsAdmin()):
		
			//echo '<PRE>';print_r($arResult);echo '</PRE>';
		
		endif;
	
		?><div class="top-links"><?
		//foreach($arResult['PATH'] as $path):
		
			$path = $arResult['PATH'][count($arResult['PATH'])-1];
		
			?><a href="<?=$arResult['SECTION_PAGE_URL']?>">Все <?=mb_strtolower($arResult['NAME'])?></a>&nbsp&nbsp<?
		
		//endforeach;
		?></div><?
		
		
	endif;
		
endif;	


?>


</div>

<ul id="category-list" class="<?= $viewCatalogClass ?> js-card-list">


    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?  
        $isDiscount = $arItem["PRICES"]["BASE"]["DISCOUNT_DIFF"] > 0;
        $imgSrc = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], [
            'width' => 250,
            'height' => 250
        ])['src'];
        $discountClass = $isDiscount ? '_discount' : '';


        ?>

        <li class="card-product <?= $catalogTopClass ?> <?= $stickerHitClass ?> ">
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="card-product__a">
                    <? if ($arItem["PROPERTIES"]["TOP_RIGHT"]['VALUE'] != '') { ?>
                        <img src="<?= CFile::GetPath($arItem["PROPERTIES"]["TOP_RIGHT"]['VALUE']) ?>"
                             class="card-product__label-top-right " style="top: -7px; right: -8px;"/>
                    <? } ?>
    
                <? if ($isDiscount && $arParams["SHOW_DISCOUNT_PERCENT"] == 'N') { ?>
                    <? if ($arItem["PROPERTIES"]["STICKER_DISCOUNT"]['VALUE'] != '') { ?>
                        <img src="<?= CFile::GetPath($arItem["PROPERTIES"]["STICKER_DISCOUNT"]['VALUE']) ?>"
                             class="card-product__label-top-left icon-label-discount"/>
                    <? } else { ?>
                        <div class="card-product__label-top-left icon-label-discount"></div>
                    <? } ?>
                <? } ?>
				<? if ($isDiscount && $arParams["SHOW_DISCOUNT_PERCENT"] == 'Y') {
                    if (0 < $arItem['MIN_PRICE']['DISCOUNT_DIFF']){?>
                    <div class="product-img__label-top-left" style="top: -10px; left: -10px;">
						<img src="/img/sprite/sale.PNG"/>
						<div class="bx_stick_disc" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>" ><? echo $arItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
                    </div>
                <? }
                 } ?>

                <? /*if ($arItem["PROPERTIES"]["CATALOG_TOP"]["VALUE"] == 'Y') { ?>
                    <div class="card-product__label-top-right icon-label-bestseller"></div>
                <? } */?>

                <div class="w-card-product__img">
                    <img class="card-product__img" src="<?= $imgSrc ?>" id="bigImg<?=$arItem["ID"]?>" title="<?=$arItem['IPROPERTY_VALUES']["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]?>" alt="<?=$arItem['IPROPERTY_VALUES']["ELEMENT_PREVIEW_PICTURE_FILE_ALT"]?>"/>
                </div>
				
				<div class="card-product__code">
					<span class="card-product__code-title">Код товара:</span>
					<span class="card-product__code-text">
						<?=$arItem["PROPERTIES"]["UNIQUE_CODE"]["VALUE"]?>
					</span>
				</div>
				 <?
					
				 ?>
                <div class="card-product__title"><?= $arItem["NAME"] ?></div>
            </a>

            <div class="product_additional_preview">
                <?= html_entity_decode( $arItem[ "PROPERTIES" ][ "DOP_ANONS" ][ "VALUE" ][ 'TEXT' ] ); ?>
            </div>
            <div class="card-product__description"><?= $arItem["PREVIEW_TEXT"] ?></div>
			<?
			if (isset($arItem[ "PROPERTIES" ][ "ABOVE_PRICE" ][ "VALUE" ][ 'TEXT' ]) && $arItem[ "PROPERTIES" ][ "ABOVE_PRICE" ][ "VALUE" ][ 'TEXT' ] != ''){?>
			<div class="product_additional_preview">
                <?= html_entity_decode( $arItem[ "PROPERTIES" ][ "ABOVE_PRICE" ][ "VALUE" ][ 'TEXT' ] ); ?>
            </div>			
			<?}
			elseif(count($arItem[ "PROPERTIES" ][ "PHOTOS" ]["VALUE"]) >= 2){
				$arItem["TSVETS"] = array(); 
				foreach($arItem[ "PROPERTIES" ][ "PHOTOS" ]["VALUE"] as $FILE) 
				{ 
					$FILE = CFile::GetFileArray($FILE); 
					if(is_array($FILE)) 
					$arItem["TSVETS"][]=$FILE; 
				}?> 
								
								
				<div class="h-hidden card-product__smalls thumbs<?=$arItem["ID"]?> js-card-product-smalls-slider">
					<div class="card-product__smalls-left js-slider-prev e-arrow-left"></div>
					<div class="card-product__smalls-right js-slider-next right e-arrow-right"></div>
					
						<div class="card-product__smalls-cnt js-slider-cnt">
							<?					
							foreach($arItem["TSVETS"] as $PHOTO): 
								$renderfile = CFile::ResizeImageGet($PHOTO, array('width'=>300), BX_RESIZE_IMAGE_EXACT, true); 
								$renderprewfile = CFile::ResizeImageGet($PHOTO, array('width'=>40, 'height'=>'34'), BX_RESIZE_IMAGE_EXACT, true); ?>
								
								<div class="card-product__small js-card-product-small"> 
									<a href="<?=$renderfile["src"]?>" class="card-product__small-a h-img-cover js-fancy" data-item-id="<?=$arItem["ID"]?>" rel="product-gallery-<?=$arItem["ID"]?>"> 
										<img src="<?=$renderprewfile["src"]?>" class="card-product__small-img"/> 
									</a> 
								</div> 
							<?endforeach?> 
					</div>					
					<div class="h-clearboth"></div>
				</div>
			<?}?>
            <div class="w-card-product__bottom">
				<?  if ($arItem['PROPERTIES' ]['PROMO_STIKER']['VALUE'] > 0){ ?>						
					<img class="card-product__sticker-hit" src="<?= CFile::GetPath($arItem['PROPERTIES' ]['PROMO_STIKER']['VALUE'])?>"/>			
				<? } ?>
				<?if ($arItem["PRICES"]["BASE"]["VALUE"] != 0){?>
                <div class="card-product__price <?= $discountClass ?>">
                    <div class="card-product__price-title">Цена:</div>
                    <div class="card-product__price-val">
					
                        <?= number_format(intval($arItem["PRICES"]["BASE"]["VALUE"]), 0, '', ' ') ?>
                    </div>
                    <div class="card-product__price-currency">руб</div>
                    <? if ($isDiscount) { ?>
                        <div class="card-product__price-discount">
                            <div class="card-product__price-discount-val">
                                <?= number_format(intval($arItem["PRICES"]["BASE"]["DISCOUNT_VALUE"]), 0, '', ' ') ?>
                            </div>
                            <div class="card-product__price-discount-currency">руб</div>
                        </div>
                    <? } ?>
                </div>
				<?} else{?>
					<div class="card-product__price-discount-currency">Цена по запросу</div>
				<?}?>
                <? if ($arItem['CATALOG_QUANTITY'] > 0 && ($arItem["PROPERTIES"]["IN_MARKET"]["VALUE"] == NULL || $arItem["PROPERTIES"]["IN_MARKET"]["VALUE"] == "N")) { ?>
                    <div class="card-product__available">В наличии</div>
                <? } if($arItem['CATALOG_QUANTITY'] == 0) { ?>
                    <div class="card-product__available _no-available">Под заказ</div>
                <? } ?>	
				
                
                </div>

                
            </div>
        </li>
    <? endforeach; ?>

</ul>


<? if (!count($arResult["ITEMS"])): ?>
    <div class="empty-search js-empty-search">
        <div class="empty-search__title h3">По вашему запросу ничего не найдено</div>
        <div class="empty-search__message">
            Квалифицированные специалисты нашей компании помогут оперативно подобрать Вам нужный товар! Введите свой
            номер
            телефона в форму ниже и мы Вам перезвоним.
        </div>
        
    </div>

<? else: ?>
    <? echo $arResult["NAV_STRING"]; ?>
<? endif; ?>
<div class="b-text" style="text-align: left; margin-top: 20px;"><? print($arResult["DESCRIPTION"]) ?></div>



<a href="#body-page" class="btn-up js-btn-up">НАВЕРХ</a>

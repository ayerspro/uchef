<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}

if(stristr($_SERVER["REQUEST_URI"], 'brand_') != FALSE){
	preg_match_all('#brand_(.+?)/#is', $_SERVER["REQUEST_URI"], $arr);
	$arSelect2 = Array("ID", "NAME");
	$arFilter2 = Array("IBLOCK_ID"=>5, "CODE"=>$arr[1][0]);
	$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize"=>1500), $arSelect2);
	while($ob2 = $res2->GetNext()){ 
		$manufacturer_name = $ob2["NAME"]; 
	}
$APPLICATION->SetTitle($arResult["NAME"].' '.$manufacturer_name);
$title = $arResult["NAME"].' '.$manufacturer_name.' - Официальный дилер. Предпродажная подготовка.';
$description = 'Любую модель из каталога '.$arResult["NAME"].' '.$manufacturer_name.' вы можете заказать с доставкой на дом, офис или сразу на объект. Интернет магазин - VS-ELECTRO - Всегда низкие цены!';
$keywords = $manufacturer_name.', '.$arResult["NAME"].' '.$manufacturer_name;
$APPLICATION->SetPageProperty('title', $title);
$APPLICATION->SetPageProperty('description', $description);
$APPLICATION->SetPageProperty('keywords', $keywords);
$APPLICATION->AddChainItem($arResult["NAME"].' '.$manufacturer_name);         
}
?>
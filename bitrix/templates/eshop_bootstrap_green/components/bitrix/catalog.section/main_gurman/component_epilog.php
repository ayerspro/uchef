<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */
use Bitrix\Main\Loader;
global $APPLICATION;

if(isset($arResult['arResult'])) {
   $arResult =& $arResult['arResult'];
         // подключаем языковой файл
   global $MESS;
   include_once(GetLangFileName(dirname(__FILE__).'/lang/', '/template.php'));
} else {
   return;
}

?>

<div class="main_gurman products-items">
<?
$arrFavorites = unserialize($APPLICATION->get_cookie("ANONIM_PROD_FAVORITES"));
$arBasketItems = array();
$arBasketItemsAll = array();

$dbBasketItems = CSaleBasket::GetList(
	array(
			"NAME" => "ASC",
			"ID" => "ASC"
		),
	array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
	false,
	false,
	array()
);
	
while ($arItems = $dbBasketItems->Fetch()) {
	
	if (strlen($arItems["CALLBACK_FUNC"]) > 0) {
		
		CSaleBasket::UpdatePrice($arItems["ID"],
								 $arItems["CALLBACK_FUNC"],
								 $arItems["MODULE"],
								 $arItems["PRODUCT_ID"],
								 $arItems["QUANTITY"]);
		$arItems = CSaleBasket::GetByID($arItems["ID"]);
	}

	$arBasketItemsAll[$arItems['PRODUCT_ID']] = $arItems;
	$arBasketItems[] = $arItems['PRODUCT_ID'];
}

foreach ($arResult['ITEMS'] as $arItem) {

	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$APPLICATION->IncludeComponent(
		'bitrix:catalog.item',
		'grid',
		array(
			'RESULT' => array(
				'ITEM' => $arItem,
				'AREA_ID' => $strMainID,
				'BASKET_ITEMS' => $arBasketItems,
				'BASKET_ITEMS_ALL' => $arBasketItemsAll,
				'FAVORITES_ITEMS' => $arrFavorites,
			),
			'PARAMS' => $arParams
				+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$arItem['IBLOCK_ID']])
		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);
}
?>
</div>

<?

$APPLICATION->SetAdditionalCSS('/bitrix/templates/eshop_bootstrap_green/components/bitrix/catalog/chef/bitrix/catalog.section/.default/themes/blue/style.css');

if (isset($templateData['TEMPLATE_THEME']))
{
	//$APPLICATION->SetAdditionalCSS($templateFolder.'/themes/'.$templateData['TEMPLATE_THEME'].'/style.css');
	//$APPLICATION->SetAdditionalCSS('/bitrix/templates/eshop_bootstrap_green/components/bitrix/catalog/chef/bitrix/catalog.section/.default/themes/'.$templateData['TEMPLATE_THEME'].'/style.css', true);
}

if (!empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
	{
		$loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
	}

	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);

	if ($loadCurrency)
	{
		?>
		<script>
			BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
		</script>
		<?
	}
}

//	lazy load and big data json answers
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if ($request->isAjaxRequest() && ($request->get('action') === 'showMore' || $request->get('action') === 'deferredLoad'))
{
	$content = ob_get_contents();
	ob_end_clean();

	list(, $itemsContainer) = explode('<!-- items-container -->', $content);
	list(, $paginationContainer) = explode('<!-- pagination-container -->', $content);

	if ($arParams['AJAX_MODE'] === 'Y')
	{
		$component->prepareLinks($paginationContainer);
	}

	$component::sendJsonAnswer(array(
		'items' => $itemsContainer,
		'pagination' => $paginationContainer
	));
}
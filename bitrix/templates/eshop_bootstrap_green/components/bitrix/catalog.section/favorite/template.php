<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
?>
<a href="#" style="opacity: 0;" data-type="iframe" data-src="/personal/favorite/ajax/createList.php" class="createList newList btn btn-default btn-block callback fancybox.iframe"></a>
<?
if($_REQUEST['view'] == 'block'){
	include "block.php";
	
}
else{
	include "list.php";
} 
?>

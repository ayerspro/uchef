<style>
	.data-table .card-product__actions-full {
		width: 75% !important;
	}
</style>
<div class="catalog-section favorite-catalog-section">
<table class="data-table" cellspacing="0" cellpadding="0" border="0" width="100%">
	<?
	$arBasketItems = array();
	$arBasketItemsAll = array();

	$dbBasketItems = CSaleBasket::GetList(
	        array(
	                "NAME" => "ASC",
	                "ID" => "ASC"
	            ),
	        array(
	                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
	                "LID" => SITE_ID,
	                "ORDER_ID" => "NULL"
	            ),
	        false,
	        false,
	        array()
	    );
	while ($arItems = $dbBasketItems->Fetch())
	{
	    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
	    {
	        CSaleBasket::UpdatePrice($arItems["ID"], 
	                                 $arItems["CALLBACK_FUNC"], 
	                                 $arItems["MODULE"], 
	                                 $arItems["PRODUCT_ID"], 
	                                 $arItems["QUANTITY"]);
	        $arItems = CSaleBasket::GetByID($arItems["ID"]);
	    }

	    $arBasketItemsAll[$arItems['PRODUCT_ID']] = $arItems;
	
	    $arBasketItems[] = $arItems['PRODUCT_ID'];
	}
	foreach($arResult["ITEMS"] as $arElement):?>

	<?
	$itemID = $arElement['ID'];

	$res = CCatalogSKU::getOffersList(
        $arElement['ID'], // массив ID товаров
        $iblockID = 0, // указываете ID инфоблока только в том случае, когда ВЕСЬ массив товаров из одного инфоблока и он известен
        $skuFilter = array(), // дополнительный фильтр предложений. по умолчанию пуст.
        $fields = array("SIZE"),  // массив полей предложений. даже если пуст - вернет ID и IBLOCK_ID
        $propertyFilter = array('CODE'=>array("SIZE")) /* свойства предложений. имеет 2 ключа:
                               ID - массив ID свойств предложений
                                      либо
                               CODE - массив символьных кодов свойств предложений
                               если указаны оба ключа, приоритет имеет ID*/
                             );
//echo'<pre>';print_r($arElement);echo'</pre>';
	if(isset($arElement['OFFERS']) && count($arElement['OFFERS']) > 0 && isset($arElement['OFFERS'][0])){
		$offerID = $arElement['OFFERS'][0]['ID'];
		$itemID = $offerID;
	}
	/*if(!empty($res)){
		$itemID=$res[$itemID];
		$i==0;
		foreach ($itemID as $key => $offers) {
			if($i==0){
				$itemID=$offers['ID'];
				$i++;
			}
		}
		echo'<pre>';print_r($itemID);echo'</pre>';
	}*/
//echo'<pre>';print_r($itemID);echo'</pre>';
	$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
	$img = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"]['ID'], array("width" => 250, "height" => 127), BX_RESIZE_IMAGE_PROPORTIONAL);
	$res = CCatalogSKU::getOffersList(array($arElement['ID']), 0, array(), array(), array());
        $offers = "";
        foreach ($res as $key) {
        	foreach ($key as $value) {
        		$offers = $value['ID'];
        		break;
        	}
        }
    if(!empty($offers) && in_array($offers, $arBasketItems)) $arBasketItems[] = $arElement['ID'];
	?>
	<tr id="product<?=$arElement['ID']?>">
		<td class="images">
			<span>
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>">
					<img src="<?=$img['src']?>" alt="<?=$arElement['NAME']?>">
				</a>
			</span>
		</td>
		<td>
			<span>
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="name-product"><?=$arElement["NAME"]?></a>
				<?if(count($arElement["SECTION"]["PATH"])>0):?>
					<br />
					<?foreach($arElement["SECTION"]["PATH"] as $arPath):?>
						/ <a href="<?=$arPath["SECTION_PAGE_URL"]?>"><?=$arPath["NAME"]?></a>
					<?endforeach?>
				<?endif?>
			<div class="mobile-block">
			<?foreach($arElement["DISPLAY_PROPERTIES"] as $pid=>$arProperty): $str = "";?>
				<?if(is_array($arProperty["DISPLAY_VALUE"]) && !empty($arProperty["DISPLAY_VALUE"]))
					$str .= implode(" | ", $arProperty["DISPLAY_VALUE"]);
				elseif(!empty($arProperty["DISPLAY_VALUE"]))
					$str .= $arProperty["DISPLAY_VALUE"] . ' | ';
					echo ($str != "") ? substr($str, 0, -2) : '';
				?>
			<?endforeach?><br/>
			
				<?foreach($arResult["PRICES"] as $code=>$arPrice):?>
					<?if($arPrice = $arElement["PRICES"][$code]):?>
						<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
							<s><?=$arPrice["PRINT_VALUE"]?></s><br /><span class="catalog-price"><?=priceFormat($arPrice["PRINT_DISCOUNT_VALUE"])?> <i class="fa fa-ruble-sign"></i></span>
						<?else:?>
							<span class="catalog-price"><?=priceFormat($arPrice["PRINT_VALUE"])?> <i class="fa fa-ruble-sign"></i></span> <?=(!empty($arElement['PROPERTIES']['SIZE']['VALUE']))?'<sup>/'.$arElement['PROPERTIES']['SIZE']['VALUE'].'</sup>':'';?>
						<?endif?>
					<?else:?>
						&nbsp;
					<?endif;?>
				<?endforeach;?>
			</div>
			</span>
		</td>
		
		<td class="properties">
			<span style="justify-content: center;">
			<?foreach($arElement["DISPLAY_PROPERTIES"] as $pid=>$arProperty): $str = "";?>
				<?if(is_array($arProperty["DISPLAY_VALUE"]) && !empty($arProperty["DISPLAY_VALUE"]))
					$str .= implode(" | ", $arProperty["DISPLAY_VALUE"]);
				elseif(!empty($arProperty["DISPLAY_VALUE"]))
					$str .= $arProperty["DISPLAY_VALUE"] . ' | ';
					echo ($str != "") ? substr($str, 0, -2) : '';
				?>
			<?endforeach?>
			<!-- <span class="warp_offer_prop">
				<?foreach ($arElement["OFFERS"] as $key => $OFFERS) {?>
					<a <?if($offerID==$OFFERS['ID']){?> class="active"<?}?> id="prop_offers_<?=$OFFERS['ID']?>" href="javascript: void(0);"><?=$OFFERS['DISPLAY_PROPERTIES']['SIZE']['VALUE']?></a>
				<?}?>
			</span> -->
			</span>
		</td>
		
		<td>
			<?if(!empty($arElement['OFFERS'])){?>
				<span>
				<? $i=0;
				$idoffers="";
				foreach ($arElement['OFFERS'] as $key => $arrOffers) {

					?>
					
					<?foreach($arrOffers["PRICES"] as $code=>$arPrice):?>
					<?if($arPrice = $arrOffers["PRICES"][$code]):?>
					<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
					<s><?=$arPrice["PRINT_VALUE"]?></s><br /><span class="catalog-price"><?=substr($arPrice["PRINT_DISCOUNT_VALUE"], 0, -4)?> &#8381;</span>
					<?else:?>
					<div class="offers_prop <?if($i==0){?>active<?}?>" id="offers_el_<?=$arrOffers['ID']?>" onclick="changeOggers(<?=$arElement['ID']?>, <?=$arrOffers['ID']?>);">
						<span   class="catalog-price"><?=substr($arPrice["PRINT_VALUE"], 0, -4)?> &#8381;</span> <?=(!empty($arrOffers['PROPERTIES']['SIZE']['VALUE']))?'<sup>/'.$arrOffers['PROPERTIES']['SIZE']['VALUE'].'</sup>':'';?>
						<?endif?>
							<?if($i==0){$idoffers=$arrOffers['ID'];}?>
					</div>
					<?else:?>
					&nbsp;
					<?endif;?>
					<?endforeach;?>
				<?$i++;} ?>
				<input type="hidden" id="offersActive" value="<?=$idoffers?>">
				</span>
			<?}else{?>
			<span <?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]){?> class="fix_sale_price" <?}?>>
			<?foreach($arResult["PRICES"] as $code=>$arPrice):?>
				<?if($arPrice = $arElement["PRICES"][$code]):?>
					<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
						<s><?=$arPrice["PRINT_VALUE"]?></s><br /><span class="catalog-price"><?=substr($arPrice["PRINT_DISCOUNT_VALUE"], 0, -4)?> &#8381;</span>
					<?else:?>
						<span class="catalog-price"><?=substr($arPrice["PRINT_VALUE"], 0, -4)?> &#8381;</span> <?=(!empty($arElement['PROPERTIES']['SIZE']['VALUE']))?'<sup>/'.$arElement['PROPERTIES']['SIZE']['VALUE'].'</sup>':'';?>
					<?endif?>
				<?else:?>
					&nbsp;
				<?endif;?>
			<?endforeach;?>
			</span>
			<?}?>
		</td>
		<td>
			<span>
			<?if(count($arResult["PRICES"]) > 0):?>
				<div class="card-product__actions-full" style="width: 75%; top: 25px; text-align: center; <?if(in_array($arElement['ID'], $arBasketItems)):?>/*margin-right: 52px;*/<?endif;?>">
					<div class="card-product__favorite">
						<a href="#" class="catalog-item-fav btn-fav favorite_check active" data-id="<?=$arElement['ID']?>"></a> 
					</div>
					<div class="js__warp_offers_change">
						<?if(!in_array($itemID, $arBasketItems)) :?>
		        	<div class="card-product__buy">
		            <a href="javascript:void(0)" data-id="<?=$itemID?>" data-id2="<?=$arElement['ID']?>" data-quantity="<?=(!empty($arBasketItemsAll)&&isset($arBasketItemsAll[$itemID]))?$arBasketItemsAll[$itemID]['QUANTITY']:$arElement["CATALOG_MEASURE_RATIO"]?>" class="btn-buy"></a>
		          </div>
		        <?endif;?>
          
            <form action="<?=POST_FORM_ACTION_URI?>" onsubmit="return false;" method="post" enctype="multipart/form-data" class="add_form<?if(in_array($itemID, $arBasketItems)) echo " active";?>">
            	<div class="add_form_counter-line ajax-count">
            		<a class="MinusList" href="javascript:void(0)">-</a> 
            		<input type="hidden" name="quantity_ratio" value="<?=$arElement["CATALOG_MEASURE_RATIO"]?>" />	
            		<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" data-id="<?=$itemID?>" value="<?=(!empty($arBasketItemsAll)&&isset($arBasketItemsAll[(!empty($offers))?$offers:$arElement['ID']]))?$arBasketItemsAll[(!empty($offers))?$offers:$itemID]['QUANTITY']:$arElement["CATALOG_MEASURE_RATIO"]?>" id="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?><?=$itemID?>"> 
            		<a class="PlusList" href="javascript:void(0)">+</a>	
            	</div>
            	<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
            	<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $itemID?>">
            	<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="Купить" style="display:none;">
            	<input class="add_form_btn" type="submit" id="link2card<?=$itemID?>" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="В корзине"/>
            </form>
          </div>
				</div>
			<?endif;?>
			</span>
		</td>
	</tr>
	<?endforeach;?>
</table>
<?$this->SetViewTarget('pagination_section');?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<div class="pagination-section"><?=$arResult["NAV_STRING"]?></div>
<?endif?>
<?$this->EndViewTarget();?> 
</div>
<div class="container-fluid pagination-bottom">
	<div class="pagination-section"><?=$arResult["NAV_STRING"]?></div>
</div>
 <?//echo'<pre>';print_r($arResult);echo'</pre>';?>
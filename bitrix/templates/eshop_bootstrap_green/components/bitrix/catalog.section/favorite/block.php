<div class="products-items section-all">
	<?
	$arBasketItems = array();
	$arBasketItemsAll = array();

	$dbBasketItems = CSaleBasket::GetList(
	        array(
	                "NAME" => "ASC",
	                "ID" => "ASC"
	            ),
	        array(
	                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
	                "LID" => SITE_ID,
	                "ORDER_ID" => "NULL"
	            ),
	        false,
	        false,
	        array()
	    );
	while ($arItems = $dbBasketItems->Fetch())
	{
	    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
	    {
	        CSaleBasket::UpdatePrice($arItems["ID"], 
	                                 $arItems["CALLBACK_FUNC"], 
	                                 $arItems["MODULE"], 
	                                 $arItems["PRODUCT_ID"], 
	                                 $arItems["QUANTITY"]);
	        $arItems = CSaleBasket::GetByID($arItems["ID"]);
	    }

	    $arBasketItemsAll[$arItems['PRODUCT_ID']] = $arItems;
	
	    $arBasketItems[] = $arItems['PRODUCT_ID'];
	}
foreach ($arResult['ITEMS'] as $arItem)
	{
		
		$imgTitle = isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $arItem['NAME'];
		
		$imgAlt = isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']
		: $arItem['NAME'];
		
		$minPrice = $arItem['MIN_PRICE'];
		$itemID = $arItem['ID'];
		$measure = $arItem['ITEM_MEASURE_RATIOS'][$arItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
		
		if($measure) {
			
			$minPrice['DISCOUNT_VALUE'] = $minPrice['DISCOUNT_VALUE'] * $measure;
			$minPrice['VALUE'] = $minPrice['VALUE'] * $measure;
		}

		if(isset($arItem['OFFERS']) && count($arItem['OFFERS']) > 0 && isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']])){
			$offerID = $arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['ID'];
			$itemID = $offerID;
		}

		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);
		?>
	    <div class="card-product slick-slide" id="product<?=$itemID;?>">
	    	<div class="card-product-wrap" id="<?=$strMainID?>">
	        <?$res = CIBlockElement::GetByID((int)$arItem["PROPERTIES"]["MAIN_STIKER"]["VALUE"]);
			if($ar_res = $res->GetNext()){ ?>
				<div class="w-card-product__category natural" style="left: -10px;top: -15px; float:left">
					<img src="<?=CFile::GetPath($ar_res["DETAIL_PICTURE"]);?>" alt="<?=$ar_res['NAME']?>" title="<?=$ar_res['NAME']?>">
				</div>
			<?}?>
			<div  class="w-card-product__discount" style="display:<? echo (0 < $minPrice['DISCOUNT_DIFF_PERCENT'] ? '' : 'none'); ?>;" >Скидка <? echo $minPrice['DISCOUNT_DIFF_PERCENT']; ?>%</div>
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="card-product__a">
				<div class="w-card-product__img">
					<?
					$photoResize = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array("width" => 200, "height" => 200), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
					?>
					<img class="card-product__img" src="<?=$photoResize["src"]?>" id="bigImg<?=$itemID?>" alt="<?=$imgAlt;?>" title="<?=$imgTitle;?>">
				</div>
				<?/*?>
				<div class="card-product__title"><?=$arItem["PROPERTIES"]["LONG_NAME"]["VALUE"]?></div>
				<div class="card-product__description"><?=$arItem["NAME"]?></div>
				<?*/?>
				<div class="card-product__title"><?=$arItem["NAME"]?></div>
			</a>
	        <div class="w-card-product__bottom clearfix">
				<div class="card-product__price-full">
	                <div class="card-product__price-discount">

					<?if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
					{
					?> <span><?=priceFormat($minPrice['VALUE']); ?></span>
						<div class="card-product__price-discount-currency"><span class="fa fa-ruble-sign"></span></div><?
					}?>

	                </div>
	                <div class="card-product__price">
	                    <div class="card-product__price-val"><?=priceFormat($minPrice['DISCOUNT_VALUE']);?></div>
	                    <div class="card-product__price-currency"><span class="fa fa-ruble-sign"></span></div>
	                </div>
					<div class="card-product__weight">
						<?
						
						
						/*if(!empty($arItem['PROPERTIES']['liquid']['VALUE'])){
							echo $arItem['PROPERTIES']['liquid']['VALUE'];
						}else{
								if(!empty($offerID)){
									print $arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['CATALOG_WEIGHT'] ? getFormatedWeight($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['CATALOG_WEIGHT']) : getFormatedWeight($arItem['CATALOG_WEIGHT']);
								}
								else{
									print getFormatedWeight($arItem['CATALOG_WEIGHT']);
								}
						}*/
						//echo'<pre>';print_r($arItem['ITEM_MEASURE_RATIOS'][$arItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']);echo'</pre>';
						echo $arItem['ITEM_MEASURE_RATIOS'][$arItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']." ".$arItem['ITEM_MEASURE']['TITLE'];
		//echo'<pre>';print_r($arItem['ITEM_MEASURE_RATIOS']);echo'</pre>';
		//echo'<pre>';print_r($arItem['ITEM_MEASURE']['TITLE']);echo'</pre>';
						?>
					</div>
				</div>
				<div class="card-product__actions-full"<?if(in_array($arItem['ID'], $arBasketItems)) :?> style="width: 58%; text-align: left;"<?endif;?>>
				<?if(in_array($USER->GetId(),$arItem["PROPERTIES"]["F_USER"]["VALUE"]) || in_array($arItem['ID'], $arrFavorites)){
					$check = " active";
				}
				else{
					$check = "";
				};?>

				<div class="card-product__favorite">
					<a href="#" class="catalog-item-fav btn-fav favorite_check <?=$check?>" data-id="<?=$arItem['ID']?>"></a>
				</div>
				<?if(!in_array($itemID, $arBasketItems)) :?>
	                <div class="card-product__buy">
	                  <a href="javascript:void(0)" data-id="<?=$itemID?>" data-quantity="<?=$arItem["CATALOG_MEASURE_RATIO"]?>" class="btn-buy"></a>
	                </div>
	            <?endif;?>
	            </div>

				<form action="<?=POST_FORM_ACTION_URI?>" onsubmit="return false;" method="post" enctype="multipart/form-data" class="add_form<?if(in_array($itemID, $arBasketItems)) echo " active";?>">
					<div class="add_form_counter-line ajax-count">
						<a class="MinusList" href="javascript:void(0)">-</a>
						<input type="hidden" name="quantity_ratio" value="<?=$arItem["CATALOG_MEASURE_RATIO"]?>" />
						<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" data-id="<?=$itemID?>" value="<?=(!empty($arBasketItemsAll)&&isset($arBasketItemsAll[$itemID]))?$arBasketItemsAll[$itemID]['QUANTITY']:$arItem["CATALOG_MEASURE_RATIO"]?>" id="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?><?=$itemID?>">
						<a class="PlusList" href="javascript:void(0)">+</a>
					</div>
					<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
					<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=$itemID?>">
					<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="Купить" style="display:none;">
					<input class="add_form_btn" type="submit" id="link2card<?=$itemID?>" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="В корзине" onClick="window.location.href = '/cart/'" />
				</form>
	        </div>
		    </div>
	    </div>

		<?
	}
?>
</div>
<?$this->SetViewTarget('pagination_section');?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<div class="pagination-section"><?=$arResult["NAV_STRING"]?></div>
<?endif?>
<?$this->EndViewTarget();?>
<div style="clear: both;"></div>
<div class="container-fluid pagination-bottom">
	<div class="pagination-section"><?=$arResult["NAV_STRING"]?></div>
</div>
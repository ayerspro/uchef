<div class="products-items section-all">
	<?
	$arBasketItems = array();
	$arBasketItemsAll = array();

	$dbBasketItems = CSaleBasket::GetList(
	        array(
	                "NAME" => "ASC",
	                "ID" => "ASC"
	            ),
	        array(
	                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
	                "LID" => SITE_ID,
	                "ORDER_ID" => "NULL"
	            ),
	        false,
	        false,
	        array()
	    );
	while ($arItems = $dbBasketItems->Fetch())
	{
	    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
	    {
	        CSaleBasket::UpdatePrice($arItems["ID"], 
	                                 $arItems["CALLBACK_FUNC"], 
	                                 $arItems["MODULE"], 
	                                 $arItems["PRODUCT_ID"], 
	                                 $arItems["QUANTITY"]);
	        $arItems = CSaleBasket::GetByID($arItems["ID"]);
	    }

	    $arBasketItemsAll[$arItems['PRODUCT_ID']] = $arItems;
	
	    $arBasketItems[] = $arItems['PRODUCT_ID'];
	}
foreach ($arResult['ITEMS'] as $arItem)
{
	$itemID = $arItem['ID'];
	//echo'<pre>';print_r($arItem);echo'</pre>';
	$minPrice = $arItem['MIN_PRICE'];
	// Выведем актуальную корзину для текущего пользователя
	$res = CCatalogSKU::getOffersList(array($arItem['ID']), 0, array(), array(), array());
        $offers = "";
        foreach ($res as $key) {
        	foreach ($key as $value) {
        		$offers = $value['ID'];
        		break;
        	}
        }
        if(!empty($offers) && in_array($offers, $arBasketItems)) $arBasketItems[] = $arItem['ID'];
    if(isset($arItem['OFFERS']) && count($arItem['OFFERS']) > 0 && isset($arItem['OFFERS'][0])){
		$offerID = $arItem['OFFERS'][0]['ID'];
		$itemID = $offerID;
	}
	?>
    <div class="card-product slick-slide" id="product<?= $arItem["ID"];?>">
        <?$res = CIBlockElement::GetByID((int)$arItem["PROPERTIES"]["MAIN_STIKER"]["VALUE"]);
		if($ar_res = $res->GetNext()){ ?>
			<div class="w-card-product__category natural" style="left: -10px;top: -15px; float:left">
				<img src="<?=CFile::GetPath($ar_res["DETAIL_PICTURE"]);?>" alt="">
			</div>
		<?}?>
		<div  class="w-card-product__discount" style="display:<? echo (0 < $minPrice['DISCOUNT_DIFF_PERCENT'] ? '' : 'none'); ?>;" >Скидка <? echo $minPrice['DISCOUNT_DIFF_PERCENT']; ?>%</div>
      
        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="card-product__a">
            <div class="w-card-product__img">
                <img class="card-product__img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" id="bigImg<?=$arItem["ID"]?>" alt="">
            </div>
            <div class="card-product__title"><?=$arItem["NAME"]?></div>
        </a>
        <div class="w-card-product__bottom clearfix">
       <?if(!empty($arItem['OFFERS'])){?>
       	<? $i=0;
       		$idoffers="";
       		foreach ($arItem['OFFERS'] as $key => $arrOffers) {?>
	       		<div id="offers_el_<?=$arrOffers['ID']?>" class="card-product__price-full offers_prop <?if($i==0){?>active<?}?>" onclick="changeOggers(<?=$arItem['ID']?>, <?=$arrOffers['ID']?>);">
	       			<?foreach($arrOffers["PRICES"] as $code=>$arPrice){?>
		       			<?if($arPrice = $arrOffers["PRICES"][$code]){?>
				       		<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]){?>
				       			<div class="card-product__price-discount">
				       				 <span><?=$arPrice["PRINT_VALUE"]?></span>
				       				<div class="card-product__price-discount-currency fa fa-ruble-sign"></div>
				       				<?echo'<pre>';print_r($arrOffers['PRICES']);echo'</pre>';?>
				       			</div>
				       			<div class="card-product__price">
				       				<div class="card-product__price-val"><?=priceFormat($arPrice['DISCOUNT_VALUE'])?></div>
				       				<div class="card-product__price-currency fa fa-ruble-sign"></div>
				       			</div>
			       			<?}else{?>
				       			<div class="card-product__price">
				       				<div class="card-product__price-val"><?=priceFormat($arPrice['DISCOUNT_VALUE'])?></div>
				       				<div class="card-product__price-currency fa fa-ruble-sign"></div>
				       			</div>
				       		<?}?>
		       			<?}else{?>
		       			&nbsp;
		       			<?}?>
	       			<?}?>

	       			
	       			<div class="card-product__weight">/
	       				<?
	       				//echo'<pre>';print_r($arrOffers);echo'</pre>';
	       				if(isset($arItem['OFFERS']) && count($arItem['OFFERS']) > 0 && isset($arItem['OFFERS'][$arItem['OFFER_ID_SELECTED']])){
	       					print getFormatedWeight($arrOffers['CATALOG_WEIGHT']); 
	       				} 
	       				else{
	       					print getFormatedWeight($arrOffers['CATALOG_WEIGHT']);;
	       				}
	       				?>
	       			</div>
	       		</div>
						<?if($i==0){$idoffers=$arrOffers['ID'];}?>
       		<?$i++;
       		} ?>
       		<input type="hidden" id="offersActive" value="<?=$idoffers?>">
       <?}else{?>
					<div class="card-product__price-full">
		                <div class="card-product__price-discount">
						<?if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
						{
						?> <span><?=priceFormat($minPrice['VALUE']); ?></span>
							<div class="card-product__price-discount-currency fa fa-ruble-sign"></div><?
						}?>
						<?//echo'<pre>';print_r($arParams['SHOW_OLD_PRICE']);echo'</pre>';?>
		                    
		                </div>
		                <div class="card-product__price">
		                    <div class="card-product__price-val"><?=priceFormat($minPrice['DISCOUNT_VALUE'])?></div>
		                    <div class="card-product__price-currency fa fa-ruble-sign"></div>
		                </div>
						<div class="card-product__weight">/
							<?
								if(isset($arItem['OFFERS']) && count($arItem['OFFERS']) > 0 && isset($arItem['OFFERS'][$arItem['OFFER_ID_SELECTED']])){
									print getFormatedWeight($arItem['OFFERS'][$arItem['OFFER_ID_SELECTED']]['CATALOG_WEIGHT']); 
								} 
								else{
									print getFormatedWeight($arItem['CATALOG_WEIGHT']);;
								}
							?>
						</div>
					</div>
				<?}?>
					<div class="card-product__actions-full"<?if(in_array($itemID, $arBasketItems)) :?> style="width: 58%; text-align: left;"<?endif;?>>

						<div class="card-product__favorite">
							<a href="#" class="catalog-item-fav btn-fav favorite_check active" data-id="<?=$itemID?>"></a> 
						</div>
						<div class="js__warp_offers_change">
							<?if(!in_array($itemID, $arBasketItems)) :?>
							<div class="card-product__buy">
								<a href="javascript:void(0)" data-id="<?=$itemID?>" data-id2="<?=$arElement['ID']?>" data-quantity="<?=(!empty($arBasketItemsAll)&&isset($arBasketItemsAll[$itemID]))?$arBasketItemsAll[$itemID]['QUANTITY']:$arItem["CATALOG_MEASURE_RATIO"]?>" class="btn-buy"></a>
							</div>
							<?endif;?>


							<form action="<?=POST_FORM_ACTION_URI?>" onsubmit="return false;" method="post" enctype="multipart/form-data" class="add_form<?if(in_array($itemID, $arBasketItems)) echo " active";?>">
								<div class="add_form_counter-line ajax-count">
									<a class="MinusList" href="javascript:void(0)">-</a> 
									<input type="hidden" name="quantity_ratio" value="<?=$arItem["CATALOG_MEASURE_RATIO"]?>" />	
									<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" data-id="<?=$itemID?>" value="<?=(!empty($arBasketItemsAll)&&isset($arBasketItemsAll[(!empty($offers))?$offers:$itemID]))?$arBasketItemsAll[(!empty($offers))?$offers:$itemID]['QUANTITY']:$arItem["CATALOG_MEASURE_RATIO"]?>" id="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?><?=$itemID?>"> 
									<a class="PlusList" href="javascript:void(0)">+</a>	
								</div>
								<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
								<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $itemID?>">
								<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="Купить" style="display:none;">
								<input class="add_form_btn" type="submit" id="link2card<?=$itemID?>" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="В корзине"/>
							</form>
						</div>
					</div>
        </div>
    </div>
   
	<?
}
?>
</div>
<?$this->SetViewTarget('pagination_section');?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<div class="pagination-section"><?=$arResult["NAV_STRING"]?></div>
<?endif?>
<?$this->EndViewTarget();?>
<div style="clear: both;"></div>
<div class="container-fluid pagination-bottom">
	<div class="pagination-section"><?=$arResult["NAV_STRING"]?></div>
</div>
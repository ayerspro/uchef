<? if ( !defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== TRUE ) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode( TRUE );

 //print_r($arResult);
// die();
Bitrix\Main\Loader::includeModule( 'services.tables' );
?>
<script>
function ShowminiImage(){
$(".js-card-product-smalls-slider2").each(function(){
		var t=$(this);
		t.removeClass("h-hidden"),
		t.find(".js-slider-cnt").slick({prevArrow:t.find(".js-slider-prev"),
		nextArrow:t.find(".js-slider-next"),
		infinite:!1,slidesToShow:4
		})
	});}
ShowminiImage();

</script>
<?/* if manufacturer*/
/*
if(stristr($_SERVER["REQUEST_URI"], 'brand_') != FALSE){
	preg_match_all('#brand_(.+?)/#is', $_SERVER["REQUEST_URI"], $arr);
	$arSelect2 = Array("ID", "NAME");
	$arFilter2 = Array("IBLOCK_ID"=>5, "CODE"=>$arr[1][0]);
	$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize"=>1500), $arSelect2);
	while($ob2 = $res2->GetNext()){ 
		$manufacturer_filter_id = $ob2["ID"];
		$manufacturer_name = $ob2["NAME"]; 
	}
	$GLOBALS['arrFilter']["PROPERTY_50_VALUE"]=$manufacturer_filter_id;
}*/

?>
 <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?   global $USER;
	if ($USER->IsAdmin())  {
	// var_dump($arItem["PROPERTIES"]["MAIN_PRICE_RUB"]["VALUE"]);
		$arPrice = CCatalogProduct::GetOptimalPrice( $arItem["ID"], 1, $USER->GetUserGroupArray(), "N");
	//var_dump($arPrice ["DISCOUNT_LIST"][0]['ID']);     id sale
	}
        $catalogTopClass = $arItem["PROPERTIES"]["CATALOG_TOP"]["VALUE"] == 'Y' ? 'catalog_top' : '';
        $isDiscount = $arItem["PRICES"]["BASE"]["DISCOUNT_DIFF"] > 0;
        $imgSrc = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], [
            'width' => 250,
            'height' => 250
        ])['src'];
        $discountClass = $isDiscount ? '_discount' : '';

        $isDiscountBtn = isset($arItem["PROPERTIES"]["CHANGE_BUTTON"]) && intval($arItem["PROPERTIES"]["CHANGE_BUTTON"]["PROPERTY_VALUE_ID"]) > 0;
        $isDiscoverLiveBtn = isset($arItem["PROPERTIES"]["CHANGE_BUTTON"]) && intval($arItem["PROPERTIES"]["CHANGE_BUTTON"]["PROPERTY_VALUE_ID"]) > 0;
		$stickerHitClass = $arItem['PROPERTIES' ]['PROMO_STIKER']['VALUE'] > 0 ? '_sticker-hit' : '';
		
        ?>

        <li class="card-product <?= $catalogTopClass ?> <?= $stickerHitClass ?> ">
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="card-product__a">
                    <? if ($arItem["PROPERTIES"]["TOP_RIGHT"]['VALUE'] != '') { ?>
                        <img src="<?= CFile::GetPath($arItem["PROPERTIES"]["TOP_RIGHT"]['VALUE']) ?>"
                             class="card-product__label-top-right " style="top: -7px; right: -8px;"/>
                    <? } ?>
    
                <? if ($isDiscount && $arParams["SHOW_DISCOUNT_PERCENT"] == 'N') { ?>
                    <? if ($arItem["PROPERTIES"]["STICKER_DISCOUNT"]['VALUE'] != '') { ?>
                        <img src="<?= CFile::GetPath($arItem["PROPERTIES"]["STICKER_DISCOUNT"]['VALUE']) ?>"
                             class="card-product__label-top-left icon-label-discount"/>
                    <? } else { ?>
                        <div class="card-product__label-top-left icon-label-discount"></div>
                    <? } ?>
                <? } ?>
				<? if ($isDiscount && $arParams["SHOW_DISCOUNT_PERCENT"] == 'Y') {
                    if (0 < $arItem['MIN_PRICE']['DISCOUNT_DIFF']){?>
                    <div class="product-img__label-top-left" style="top: -10px; left: -10px;">
						<img src="/img/sprite/sale.PNG"/>
						<div class="bx_stick_disc" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>" ><? echo $arItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
                    </div>
                <? }
                 } ?>

                <? /*if ($arItem["PROPERTIES"]["CATALOG_TOP"]["VALUE"] == 'Y') { ?>
                    <div class="card-product__label-top-right icon-label-bestseller"></div>
                <? } */?>

                <div class="w-card-product__img">
                    <img class="card-product__img" src="<?= $imgSrc ?>" id="bigImg<?=$arItem["ID"]?>"/>
                </div>
				
				<div class="card-product__code">
					<span class="card-product__code-title">Код товара:</span>
					<span class="card-product__code-text">
						<?=$arItem["PROPERTIES"]["UNIQUE_CODE"]["VALUE"]?>
					</span>
				</div>
				
                <div class="card-product__title"><?= $arItem["NAME"] ?></div>
            </a>

            <div class="product_additional_preview">
                <?= html_entity_decode( $arItem[ "PROPERTIES" ][ "DOP_ANONS" ][ "VALUE" ][ 'TEXT' ] ); ?>
            </div>
            <div class="card-product__description"><?= $arItem["PREVIEW_TEXT"] ?></div>
			<?
			if (isset($arItem[ "PROPERTIES" ][ "ABOVE_PRICE" ][ "VALUE" ][ 'TEXT' ]) && $arItem[ "PROPERTIES" ][ "ABOVE_PRICE" ][ "VALUE" ][ 'TEXT' ] != ''){?>
			<div class="product_additional_preview">
                <?= html_entity_decode( $arItem[ "PROPERTIES" ][ "ABOVE_PRICE" ][ "VALUE" ][ 'TEXT' ] ); ?>
            </div>			
			<?}
			elseif(count($arItem[ "PROPERTIES" ][ "PHOTOS" ]["VALUE"]) >= 2){
				$arItem["TSVETS"] = array(); 
				foreach($arItem[ "PROPERTIES" ][ "PHOTOS" ]["VALUE"] as $FILE) 
				{ 
					$FILE = CFile::GetFileArray($FILE); 
					if(is_array($FILE)) 
					$arItem["TSVETS"][]=$FILE; 
				}?> 
								
								
				<div class=" h-hidden card-product__smalls thumbs<?=$arItem["ID"]?> js-card-product-smalls-slider2" >
					<div class="card-product__smalls-left js-slider-prev e-arrow-left"></div>
					<div class="card-product__smalls-right js-slider-next right e-arrow-right"></div>
					
						<div class="card-product__smalls-cnt js-slider-cnt">
							<?					
							foreach($arItem["TSVETS"] as $PHOTO): 
								$renderfile = CFile::ResizeImageGet($PHOTO, array('width'=>300), BX_RESIZE_IMAGE_EXACT, true); 
								$renderprewfile = CFile::ResizeImageGet($PHOTO, array('width'=>40, 'height'=>'34'), BX_RESIZE_IMAGE_EXACT, true); ?>
								
								<div class="card-product__small js-card-product-small" > 
									<a href="<?=$renderfile["src"]?>" class="card-product__small-a h-img-cover js-fancy" data-item-id="<?=$arItem["ID"]?>" rel="product-gallery-<?=$arItem["ID"]?>"> 
										<img src="<?=$renderprewfile["src"]?>" class="card-product__small-img"/> 
									</a> 
								</div> 
							<?endforeach?> 
					</div>					
					<div class="h-clearboth"></div>
				</div>
			<?}?>
            <div class="w-card-product__bottom">
				<? if ($arItem['PROPERTIES' ]['PROMO_STIKER']['VALUE'] > 0){ ?>						
					<img class="card-product__sticker-hit" src="<?= CFile::GetPath($arItem['PROPERTIES' ]['PROMO_STIKER']['VALUE'])?>"/>			
				<? } ?>
				<?if ($arItem["PRICES"]["BASE"]["VALUE"] != 0){?>
                <div class="card-product__price <?= $discountClass ?>">
                    <div class="card-product__price-title">Цена:</div>
                    <div class="card-product__price-val">
					
                        <?= number_format(intval($arItem["PRICES"]["BASE"]["VALUE"]), 0, '', ' ') ?>
                    </div>
                    <div class="card-product__price-currency">руб</div>
                    <? if ($isDiscount) { ?>
                        <div class="card-product__price-discount">
                            <div class="card-product__price-discount-val">
                                <?= number_format(intval($arItem["PRICES"]["BASE"]["DISCOUNT_VALUE"]), 0, '', ' ') ?>
                            </div>
                            <div class="card-product__price-discount-currency">руб</div>
                        </div>
                    <? } ?>
                </div>
				<?} else{?>
					<div class="card-product__price-discount-currency">Цена по запросу</div>
				<?}?>
                <? if ($arItem['CATALOG_QUANTITY'] > 0 && ($arItem["PROPERTIES"]["IN_MARKET"]["VALUE"] == NULL || $arItem["PROPERTIES"]["IN_MARKET"]["VALUE"] == "N")) { ?>
                    <div class="card-product__available">В наличии</div>
                <? } if($arItem['CATALOG_QUANTITY'] == 0) { ?>
                    <div class="card-product__available _no-available">Под заказ</div>
                <? } ?>	
		<? if ($arItem['CATALOG_QUANTITY'] > 0 &&  $arItem["PROPERTIES"]["IN_MARKET"]["VALUE"] == "Y") { ?>
                    <div class="card-product__available">В магазине</div>
                <? } ?>				
                <div class="l-card-product__btn">
                   <? if ($arItem['CATALOG_QUANTITY'] < 1 && !$isDiscoverLiveBtn && !$isDiscountBtn) { 
			$data = json_encode([
                            "value" => $arItem["NAME"],
                            "model" => $arItem["NAME"],
                            "price" => intval($arItem["PRICES"]["BASE"]["VALUE"]),
                            "name" => 'Узнать поступление'
                        ]);
                          ?>

                        <a href="find_entry.html" class="card-product__btn e-btn-orange js-fancy-ajax-modal"
						style=" -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
								filter: grayscale(100%); margin-bottom: 5px"
                           data-tpl='<?= $data ?>'
                        >
                            Узнать поступление
                        </a>
			<?}?>
                    <? if ($isDiscoverLiveBtn) { ?>

                        <?
                        if (isset($arItem["PROPERTIES"]["FORM_IMAGE"]) && $arItem["PROPERTIES"]["FORM_IMAGE"]["VALUE"] > 0) {
                            $formImage = CFile::ResizeImageGet($arItem["PROPERTIES"]["FORM_IMAGE"]["VALUE"], [
                                "width" => 316,
                                "height" => 303
                            ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT);

                            $imgSrc = $formImage["src"];
                        } else {
                            $imgSrc = $renderImage["src"];
                        }

                        $data = json_encode([
                            "formHeader" => $arItem["PROPERTIES"]["FORM_HEADER_1"]["VALUE"] != '' ? $arItem["PROPERTIES"]["FORM_HEADER_1"]["VALUE"] : $arItem["NAME"],
                            "formHeader2" => $arItem["PROPERTIES"]["FORM_HEADER_2"]["VALUE"] != '' ? $arItem["PROPERTIES"]["FORM_HEADER_2"]["VALUE"] : $arItem["NAME"],
                            "productName" => $arItem["NAME"],
                            "imgSrc" => $imgSrc
                        ]);
                        ?>

                        <a href="product-discover-live.html"
                           class="card-product__btn e-btn-orange js-fancy-ajax-modal"
                           data-tpl='<?= $data ?>'
                           data-value="<?= $arItem["NAME"] ?>"
                           data-model="<?= $arItem["NAME"] ?>"
                           data-price="<?= intval($arItem["PRICES"]["BASE"]["VALUE"]) ?>"
                           data-name="Товар">
                            Ознакомиться ВЖИВУЮ
                        </a>
                    <? } elseif ($isDiscountBtn) { ?>
                        <?
                        $data = json_encode([
                            "name" => $arItem['NAME']
                        ]);
                        ?>

                        <a href="product-promocode.html"
                           class="card-product__btn e-btn-orange js-fancy-ajax-modal"
                           data-name="Товар"
                           data-value="<?= $arItem["NAME"] ?>"
                        >
                            <?= isset($arItem["PROPERTIES"]["CHANGE_BUTTON_DISCOUNT_TITLE"]) && '' != $arItem["PROPERTIES"]["CHANGE_BUTTON_DISCOUNT_TITLE"]['VALUE'] ? $arItem["PROPERTIES"]["CHANGE_BUTTON_DISCOUNT_TITLE"]['VALUE'] : 'Получить СКИДКУ' ?>
                        </a>
                    <? }  if($arItem['CATALOG_QUANTITY'] >0 && !$isDiscoverLiveBtn && $arItem["PRICES"]["BASE"]["VALUE"] != 0) { ?>
                        <?
                        $data = json_encode([
                            "value" => $arItem["NAME"],
                            "model" => $arItem["NAME"],
                            "price" => intval($arItem["PRICES"]["BASE"]["VALUE"]),
                            "name" => 'Товар'
                        ]);
                        ?>

                        <a href="quick-purchase.html" class="card-product__btn e-btn-green js-fancy-ajax-modal"
                           data-tpl='<?= $data ?>'
                        >
                            Купить в один клик
                        </a>
						</br>
						 <div class="card-product__btn e-btn-orange js-add-to-cart" data-add-to-cart-link="<?= $arItem["ADD_URL"] ?>" style="margin-top: 5px;">
							В корзину
						</div>
                    <? } if($arItem['CATALOG_QUANTITY'] >0 && $arItem["PRICES"]["BASE"]["VALUE"] == 0) { ?>
                        <?
                        $data = json_encode([
                            "value" => $arItem["NAME"],
                            "model" => $arItem["NAME"],
                            "price" => intval($arItem["PRICES"]["BASE"]["VALUE"]),
                            "name" => 'Товар'
                        ]);
                        ?>

                        <a href="find_price.html" class="card-product__btn e-btn-green js-fancy-ajax-modal"
                           data-tpl='<?= $data ?>'
                        >
                            Узнать стоимость
                        </a>
                    <? } ?>
		<? if($arItem['CATALOG_QUANTITY'] < 1 && !$isDiscoverLiveBtn && !$isDiscountBtn) { 
			$data = json_encode([
                            "value" => $arItem["NAME"],
                            "model" => $arItem["NAME"],
                            "price" => intval($arItem["PRICES"]["BASE"]["VALUE"]),
                            "name" => 'Подобрать аналог'
                        ]);
                          ?>

                        <a href="select_analogue.html" class="card-product__btn e-btn-green js-fancy-ajax-modal"
                           data-tpl='<?= $data ?>'
                        >
                           Подобрать аналог
                        </a>
			<?}?>
                </div>

                <div class="card-product__compare-more">
                    <a href="?action=ADD_TO_COMPARE_LIST&id=<?= $arItem['ID'] ?>"
                       class="card-product__compare-more-item js-add-to-compare">Сравнить</a>
                    /
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="card-product__compare-more-item">Подробнее</a>
                </div>
				
				<? $grade = round($arItem["PROPERTIES"]["YMARKET_RATING"]["VALUE"]);/*var_dump($arItem[ "PROPERTIES" ]);*/
                    if (empty($arItem["PROPERTIES"]["YMARKET_RATING"]["VALUE"])) $grade = 0;
                    ?>
                <div class="card-product__raiting" <?if ($grade == 0) {?> style="opacity: 0.4;"<?}?>>
                   
                    <? if ($grade > 0){ ?>
                        <? for ($i = 0; $i < $grade; $i++){ ?>
                            <i style="background:url('/upload/img/zvezd.png') -49px -15px;width: 15px;height: 15px;vertical-align: middle;display: inline-block;margin-right: 1px;"></i>
                        <?} ?>
                        <? for ($i = 0; $i < 5 - $grade; $i++){ ?>
                            <i style="background:url('/upload/img/zvezd.png') -49px 0px;width: 15px;height: 15px;vertical-align: middle;display: inline-block;margin-right: 1px;"></i>
                        <? } 
						} else{ ?>
                        <? for ($i = 0; $i < 5; $i++): ?>
                            <i style="background:url('/upload/img/zvezd.png') -49px -15px !important;width: 15px;height: 15px;vertical-align: middle;display: inline-block;margin-right: 1px;"></i>
                        <? endfor; ?>
          
                    <? } ?>
                </div>

                <div class="l-card-product__reviews">
                    <a href="#"
                       class="card-product__reviews js-reviews-a"
                       data-id="<?= $arItem['ID'] ?>"
                       data-name="<?= $arItem["NAME"] ?>"
                    >
                        Отзывы о товаре
                        <span class="js-opinions-count"></span>
                    </a>
                </div>
            </div>
        </li>
    <? endforeach; ?>
	
<script>
	ru.kvazarit.modules.fancybox.init();
	ru.kvazarit.modules.pageProduct.showOpinions();
	ru.kvazarit.modules.pageProduct.addToCompare();
	ru.kvazarit.modules.elements.imgTabFromCard();
	ru.kvazarit.modules.elements.tooltipInCardProduct();
	ru.kvazarit.modules.cardProduct.toggleCoverOnHover();
	ru.kvazarit.modules.cardProduct.initSliderSmalls();
	ru.kvazarit.modules.common.this.imgCoverClass();
	ru.kvazarit.modules.common.showDevHiddenBlocks();

	setReviewsAmount();
	function setReviewsAmount() {
		$('.js-reviews-a').each(function() {
			var el = $(this);
			$.post(
				window.location.origin + "/ajax.handler.php?request=get_opinions_count&model_id=" + el.attr('data-id'),
				null,
				function(data) {
					el.find('.js-opinions-count').html('(' + data + ' шт.)');
				}
			);
		});
	}
</script>



<? if  (count($arResult[ "ITEMS" ]) < 9 ): ?>
	<script>
		$('.show_9').remove();
	</script>
<? endif; ?>


$(document).ready(function() {
    $(document).on('change','select[name="gurman"]',function(){
        console.log('alla')
        $('#formx').submit();
    });
	$('.products-items').slick({
		dots: true,
		infinite: true,
		speed: 300,
	  	slidesToShow: 6,
	  	slidesToScroll: 6,
	  	responsive: [
    		{
      			breakpoint: 1800,
      			settings: {
        			slidesToShow: 5,
        			slidesToScroll: 5,
        		}
        	},
        	{
      			breakpoint: 1300,
      			settings: {
        			slidesToShow: 4,
        			slidesToScroll: 4,
        		}
        	},
        	{
      			breakpoint: 1024,
      			settings: {
        			slidesToShow: 3,
        			slidesToScroll: 3,
        		}
        	},{
            breakpoint: 750,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          },
          {
            breakpoint: 550,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            }
          },
  		]
	});
	$(".favorite_check").click(function(e){
  		e.preventDefault();
		$(this).toggleClass('active');

  		var $this = $(this);
  			$.get("/personal/favoriteajax.php", {
  				elid: $this.data('id')
  			}, function(data){
  				switch(data){
  					case 'done' : var response = 'Товар успешно добавлен в избранное';$this.addClass("checked");break;
  					case 'deleted' : var response = 'Товар успешно удален из избранного';$this.removeClass("checked");break;
  					case 'fail' : var response = 'Вы не авторизованы, либо Ваш запрос некорректен';$this.removeClass("checked");break;
  				}
  					$("#favorit_label").html(response).fadeIn().delay(5000).fadeOut();
  			});
  		});
         var options = {
         url: '/catalog/add2basket.php?RND='+Math.random(),
         type: "POST",
         target: '#basket-container',
         success:
            function(responseText) {
               alert("Товар добавлен в заказ");
            }
         };
         $(".add_form").ajaxForm(options);
});
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();

?>

<div class="wrap">

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$APPLICATION->GetCurPage()?>?login=yes" id="cart_auth_form">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
	<input type="hidden" name="USER_LOGIN">
	<div class="width-w error-bl">
		<?if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
			ShowMessage($arResult['ERROR_MESSAGE']);
		?>
	</div>
	<div class="input width-w">
		<input type="text" name="USER_EMAIL" placeholder="E-mail:">
	</div>
	<div class="input width-w">
		<input type="password" name="USER_PASSWORD" placeholder="Пароль:">
	</div>
	<div class="button-panel width-w">
		<input type="submit" name="Login" class="btn btn-default-chef brown" value="Войти">
		
		<?if($arResult["AUTH_SERVICES"]):?>
		<span>или</span>
		
		<?
			$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "chef", 
				array(
					"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
					"SUFFIX"=>"form",
				), 
				$component, 
				array("HIDE_ICONS"=>"Y")
			);
			?>
		<?endif;?>
	</div>
	<div class="input width-w">
		<a href="" class="forget">Я забыл пароль</a>
	</div>
<?/*if ($arResult["CAPTCHA_CODE"]):?>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
<?endif*/?>
</form>
</div>

<?
if($arResult["AUTH_SERVICES"]){
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
		array(
			"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
			"AUTH_URL"=>$arResult["AUTH_URL"],
			"POST"=>$arResult["POST"],
			"POPUP"=>"Y",
			"SUFFIX"=>"form",
		), 
		$component, 
		array("HIDE_ICONS"=>"Y")
	);
}
?>

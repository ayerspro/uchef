$(document).ready(function(){
	$('#auth_form').submit(function(){
		var th = $(this);
		var data = th.serialize();
		$.ajax({
			url: '/ajax/autoriz.php',
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(resp) {
				if(resp.SUCCESS == "YES") {
					th.empty().append('<p class="success">Вы успешно авторизовались</p>');
					setTimeout(function(){
						window.location.reload();
					}, 2000);
				} else {
					th.find('.error-bl').empty();
					$.each(resp.ERROR, function(k, v){
						th.find('.error-bl').append('<p><font class="errortext">'+v+'</font></p>');
					});
				}
			}
		});
		return false;
	});
});
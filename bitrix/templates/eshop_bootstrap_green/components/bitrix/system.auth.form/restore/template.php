<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if($_REQUEST['change_password'] != 'yes' && $USER->IsAuthorized()) LocalRedirect('/');
CJSCore::Init();
?>

<?dump($arResult);?>

<div class="wrap">

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
		<div class="titles width-w">
			<?if($arResult['SUCCESS'] != 'ok'):?>Форма восстановление пароля<?else:?>Ваш пароль успешно изменен<?endif;?>
		</div>
	<?if($arResult['SUCCESS'] != 'ok'):?>	
		<div class="width-w error-bl">
			<?if (!empty($arResult['ERROR_MESSAGE']))
				foreach ($arResult['ERROR_MESSAGE'] as $key => $value) ShowMessage($value);
			?>
		</div>
		<div class="input width-w">
			<input type="password" name="NEW_PASSWORD" placeholder="Новый пароль:">
		</div>
		<div class="input width-w">
			<input type="password" name="NEW_PASSWORD_CHECK" placeholder="Подтвердите пароль:">
		</div>
		<div class="button-panel width-w">
			<input type="submit" name="Login" value="Восстановить">
		</div>
	<?endif;?>
	<div class="input width-w">
		<a href="" class="login btn-log">Авторизоваться</a>
	</div>
<?/*if ($arResult["CAPTCHA_CODE"]):?>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
<?endif*/?>
</form>
</div>

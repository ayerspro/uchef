<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(empty($_REQUEST['NEW_PASSWORD'])) {
	$arResult['ERROR_MESSAGE'][] = 'Введите новый пароль';
} elseif(empty($_REQUEST['NEW_PASSWORD_CHECK'])) {
	$arResult['ERROR_MESSAGE'][] = 'Подтвердите пароль';
} elseif($_REQUEST['NEW_PASSWORD'] != $_REQUEST['NEW_PASSWORD_CHECK']) {
	$arResult['ERROR_MESSAGE'][] = 'Ваш пароль не совпадает с проверочным';
} else {
	global $USER;
	if(isset($_REQUEST['USER_LOGIN']) && !empty($_REQUEST['USER_LOGIN']) && isset($_REQUEST['EMAIL']) && !empty($_REQUEST['EMAIL']) && isset($_REQUEST['USER_CHECKWORD']) && !empty($_REQUEST['USER_CHECKWORD'])) {
		$GLOBALS["FILTER_logic"] = "or";
		$rsUsers = CUser::GetList($by="", $order="", array('=EMAIL' => $_REQUEST['EMAIL'], 'LOGIN_EQUAL' => $_REQUEST['USER_LOGIN']));
		if($resUsers = $rsUsers->Fetch()) {
			if($_REQUEST['USER_CHECKWORD'] == md5($resUsers['ID'].$resUsers['EMAIL']))
				if($USER->Update($resUsers['ID'], array("PASSWORD"=>trim($_REQUEST['NEW_PASSWORD'])))) $arResult['SUCCESS'] = 'ok';
	 	}
		
	}
}
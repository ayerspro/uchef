<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']):?>
<div class="auth-errors">
	<?=ShowMessage($arResult['ERROR_MESSAGE']);?>
</div>
<?endif;?>


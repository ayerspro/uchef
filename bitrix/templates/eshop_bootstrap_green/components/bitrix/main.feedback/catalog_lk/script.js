$(document).ready(function(){
	function UpdatePosition($item) {
		$item.css({
			marginLeft: -($item.outerWidth() / 2),
			marginTop: -($item.outerHeight() / 2)
		});
	}
	$('[data-modal-wrap]').on('click', function(event){
		event.preventDefault();
		var th = $(this);
		var $this = $(th.attr('data-modal-wrap'));
		var $modal = $($($this).attr('data-modal'));
		if($this.css('display') != 'block') {
			//UpdatePosition($this);
			//UpdatePosition($this);
			$this.fadeIn();
			$modal.fadeIn();
			var firstClick = true;
			$(document).bind('click.myClick', function(e){
				if(!firstClick && $(e.target).closest('.modal-window').length == 0 || $(e.target).closest('.close').length == 1) {
					$this.fadeOut();
					$modal.fadeOut();
					$(document).unbind('click.myClick');
				}
				firstClick = false;
			});
		}
	});
	$('.callback-form form').submit(function(){
		var $this = $(this);
		var data = new FormData($this[0]);
		// data.append('submit', '');
		// data.append('callback_form', 'yes');
		// if($(this).find('#checks').is(':checked'))
		// 	data.append('check_conf', 'yes');
		// else data.append('check_conf', 'on');
		$.ajax({
			url: '/ajax/callback_lk.php',
			processData: false,
        	contentType: false,
        	type: 'POST',
        	dataType: 'json',
        	data: data,
        	success: function (resp) {
        		console.log(resp);
        	    if(resp.ERROR_MESSAGE != '') {
        	    	$this.find('.mess').empty();
        	    	$.each(resp.ERROR_MESSAGE, function(k, v){
        	    		$this.find('.mess').append('<p class="error">'+v+'</p>');
        	    	});
        	    } else if(resp.SUCCESS == 'ok') {
        	    	$this.parents('.wrap').empty().append('<div class="success">Ваше сообщение отправлено</div>');
        	    	UpdatePosition($this);
        	    }
        	}
		});
		return false;
	});
});
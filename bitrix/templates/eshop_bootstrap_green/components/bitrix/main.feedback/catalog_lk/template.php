<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="modal-window callback-form" data-modal=".modal-bg">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<div class="wrap">
		<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
			<?=bitrix_sessid_post()?>
		<div class="mess"></div>
		
		<div class="titles width-w">
			Обратная связь
		</div>
		
		<div class="row">
			<div class="col-sm-6 left">
				<div class="titles-leader">
					О чём хотите написать?
				</div>
					<div class="input radio">
						<label>
							<input type="radio" name="subject-title" value="О качестве продуктов">
							О качестве продуктов
						</label>
					</div>
					<div class="input radio">
						<label>
							<input type="radio" name="subject-title" value="О работе магазина">
							О работе магазина
						</label>
					</div>
					<div class="input radio">
						<label>
							<input type="radio" name="subject-title" value="Обратная связь">
							Обратная связь
						</label>
					</div>
					<div class="input radio">
						<label>
							<input type="radio" name="subject-title" value="О доставке товаров">
							О доставке товаров
						</label>
					</div>
					<div class="input radio">
						<label>
							<input type="radio" name="subject-title" value="О сайте Шеф Гурме">
							О сайте Шеф Гурме
						</label>
					</div>
			</div>
			<div class="col-sm-6 right">
				<div class="titles-leader">
					Ваше сообщение
				</div>
				<div class="input">
					<textarea name="MESSAGE"></textarea>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="input width-w col-sm-12">
				<input name="user_name" placeholder="Ваши имя" value="" type="text">
			</div>
			<div class="input width-w col-sm-12">
				<input name="EMAIL_PHONE" placeholder="Телефон или email" value="" type="text">
				<input type="hidden" name="user_email">
				<?if($arParams["USE_CAPTCHA"] == "Y"):?>
					<div class="mf-captcha">
						<div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
						<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
						<div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
						<input type="text" name="captcha_word" size="30" maxlength="50" value="">
					</div>
				<?endif;?>
			</div>
			
			<div class="input width-w col-sm-12">		
				<?$APPLICATION->IncludeComponent("bitrix:main.userconsent.request", "",
					array(
						"ID" => AGREEMENT_ID,
						"IS_CHECKED" => "Y",
						"AUTO_SAVE" => "N",
						"IS_LOADED" => "Y",
						"ORIGINATOR_ID" => "main/feedback",
						"ORIGIN_ID" => "feedback",
						"INPUT_NAME" => "USER_AGREEMENT",
						"REPLACE" => array(
							"button_caption" => "Отправить",
							"fields" => array(
								rtrim(GetMessage("AUTH_NAME"), ":"),
								rtrim(GetMessage("AUTH_LAST_NAME"), ":"),
								rtrim(GetMessage("AUTH_LOGIN_MIN"), ":"),
								rtrim(GetMessage("AUTH_PASSWORD_REQ"), ":"),
								rtrim(GetMessage("AUTH_EMAIL"), ":"),
							)
						),
					)
				);?>
			</div>
			
			
			<div class="button-panel width-w col-sm-12" style="text-align: center;">
				<!-- <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>"> -->
				<input value="Отправить" type="submit">
			</div>
		</div>
		<input type="hidden" name="submit" value="Y">
		</form>
	</div>
</div>
<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)).'/top_template.php');

if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0)
{
?>
<script>
	 function removeBasketItem(idProduct) {
		 	offers=$('#product'+idProduct+' #offersActive').val();
		 	//console.log(offers);
		 	if(offers!==""){
		 		$.get("/ajax/changeFavOfers.php", {
		 			offersid: offers, id:idProduct, remov:"Y"
		 		}, function(data){
		 			$("#product"+idProduct+" .js__warp_offers_change").html(data);
	                    //$("#favorit_label").html(response).fadeIn().delay(5000).fadeOut();
	                  });
		 	}else{
		 		$('#product'+idProduct+' .add_form').removeClass('active');
		 		$('#product'+idProduct+' .card-product__actions-full').css({
		 			width: '', 
		 			textAlign: ''
		 		});
		 		$('#product'+idProduct+' .card-product__actions-full').append('<div class="card-product__buy"><a href="javascript:void(0)" data-id="'+idProduct+'" data-quantity="1" class="btn-buy" tabindex="0"></a></div>');
		 	}
       
        $('#cart_element_'+idProduct+' .counter-block').show();
        $('#cart_element_'+idProduct+' .bx_cart').attr('href', 'javascript:void(0)').text('В корзину');
    }
</script>
<div data-role="basket-item-list" class="bx-basket-item-list">

		<?if ($arParams["POSITION_FIXED"] == "Y"):?>
			<div id="<?=$cartId?>status" class="bx-basket-item-list-action hidden" onclick="<?=$cartId?>.toggleOpenCloseCart()"><?=GetMessage("TSB1_COLLAPSE")?></div>
		<?endif?>

		<?if ($arParams["PATH_TO_BASKET"] && $arResult["CATEGORIES"]["READY"]):?>
			<div class="bx-basket-item-list-button-container">
				<a href="<?=$arParams["PATH_TO_BASKET"]?>" class="btn btn-primary"><?=GetMessage("TSB1_2BASKET")?></a>
			</div>
		<?endif?>

		<div id="<?=$cartId?>products" class="bx-basket-item-list-container">
			<?foreach ($arResult["CATEGORIES"] as $category => $items):
				if (empty($items))
					continue;
				?>
				<div class="bx-basket-item-list-item-status"><?=GetMessage("TSB1_$category")?></div>
				<?foreach ($items as $v):
					$offer = CCatalogSku::GetProductInfo($v['PRODUCT_ID'], 0);
					if(!empty($offer['ID'])) $v['PRODUCT_ID'] = $offer['ID'];
				?>

					<div class="bx-basket-item-list-item">
						<div class="bx-basket-item-list-item-img">
							<?if ($arParams["SHOW_IMAGE"] == "Y" && $v["PICTURE_SRC"]):?>
								<?if($v["DETAIL_PAGE_URL"]):?>
									<a href="<?=$v["DETAIL_PAGE_URL"]?>"><img src="<?=$v["PICTURE_SRC"]?>" alt="<?=$v["NAME"]?>"></a>
								<?else:?>
									<img src="<?=$v["PICTURE_SRC"]?>" alt="<?=$v["NAME"]?>" />
								<?endif?>
							<?endif?>
							<div class="bx-basket-item-list-item-remove" onclick="<?=$cartId?>.removeItemFromCart(<?=$v['ID']?>); removeBasketItem(<?=$v['PRODUCT_ID']?>);" title="<?=GetMessage("TSB1_DELETE")?>"></div>
						</div>
						<div class="bx-basket-item-list-item-name">
							<?if ($v["DETAIL_PAGE_URL"]):?>
								<a href="<?=$v["DETAIL_PAGE_URL"]?>"><?=$v["NAME"]?></a>
							<?else:?>
								<?=$v["NAME"]?>
							<?endif?>
						</div>



						<?if (true):/*$category != "SUBSCRIBE") TODO */?>
							<div class="bx-basket-item-list-item-price-block">
								<?if ($arParams["SHOW_PRICE"] == "Y"):?>
									<div class="bx-basket-item-list-item-price"><strong><?=priceFormat($v["PRICE_FMT"])?> <span class="fa fa-ruble-sign"></span></strong></div>
									<?if ($v["FULL_PRICE"] != $v["PRICE_FMT"]):?>
										<div class="bx-basket-item-list-item-price-old"><?=priceFormat($v["FULL_PRICE"])?> <span class="fa fa-ruble-sign"></span></div>
									<?endif?>
								<?endif?>
								<?if ($arParams["SHOW_SUMMARY"] == "Y"):?>
									<div class="bx-basket-item-list-item-price-summ">
										<strong><?=$v["QUANTITY"]?></strong>/<strong><?=priceFormat($v["SUM"])?> <span class="fa fa-ruble-sign"></span></strong>
									</div>
								<?endif?>
							</div>
						<?endif?>
					</div>
					<div style="clear: both;"></div>
				<?endforeach?>
			<?endforeach?>
		</div>
	</div>

	<script>
		BX.ready(function(){
			<?=$cartId?>.fixCart();
			
			
			/*$(document).on('click', '#basket-inline-overlay', function(){
				console.log('click');
				<?=$cartId?>.toggleOpenCloseCart();
			});*/
			
			
		});
	</script>
<?
}
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h1 class="h3"><?=$arResult["NAME"]?></h1>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<p>Тип блюда :
	<?foreach($arResult["SECTION"]["PATH"] as $section)
		echo $section["NAME"];?>
	</p>
		<div class="card-product__actions-full">
			<?if(in_array($USER->GetId(),$arResult["PROPERTIES"]["F_USER"]["VALUE"])){
				$check = " active";
			}
			else{
				$check = "";
			};?>

			<div class="card-product__favorite">
				<a href="#" class="catalog-item-fav btn-fav favorite_check <?=$check?>" data-id="<?=$arResult['ID']?>"></a> 
			</div>
	</div>
	
	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?><br />
	<?endforeach;?>
	<table>
	<?
	foreach($arResult["DISPLAY_PROPERTIES"]["INGREDIENT_NAME"]["VALUE"] as $number=>$value){
				$arFilter = Array(
					"IBLOCK_ID"=>5, 
					"ID"=>(int)$value
				);?>
			<tr>
				<td><?=$arResult["DISPLAY_PROPERTIES"]["INGREDIENT_NAME"]["DISPLAY_VALUE"][(int)$number]?></td>
				<td><?=$arResult["DISPLAY_PROPERTIES"]["QUANTITY"]["DISPLAY_VALUE"][(int)$number]?></td>
			</tr>
	<?}?>
	<?//var_dump($arResult["DISPLAY_PROPERTIES"]["INGREDIENT_PRODUCT"]["DISPLAY_VALUE"]);
	foreach($arResult["DISPLAY_PROPERTIES"]["INGREDIENT_PRODUCT"]["VALUE"] as $number=>$value){?>
			<?$price = CPrice::GetBasePrice((int)$value);?>
			<tr>
				<td><?=$arResult["DISPLAY_PROPERTIES"]["INGREDIENT_PRODUCT"]["DISPLAY_VALUE"][(int)$number]?></td>
				<td><?=$arResult["DISPLAY_PROPERTIES"]["QUANTITY_PRODUCT"]["DISPLAY_VALUE"][(int)$number]?></td>
				<td><?=$price["PRICE"]?> руб.</td>
				<td><a href="#" class="buy" data-id="<?=$value?>">Купить</td>
			</tr>
	<?}?>
	</table>
	<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty): ?>
		<?if ($pid != "INGREDIENT_PRODUCT" && $pid != "QUANTITY_PRODUCT" && $pid != "INGREDIENT_NAME" && $pid != "QUANTITY"){?>
			<?=$arProperty["NAME"]?>:&nbsp;
			<?if(is_array($arProperty["DISPLAY_VALUE"])){?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?}else{?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?}
		}?>
		<br />
	<?endforeach;
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>
<script>
$(document).ready(function() {	
$(".favorite_check").click(function(e){
  		e.preventDefault();
		$(this).toggleClass('active');
  		var $this = $(this);
  			$.get("/personal/favoriteajax.php", {
  				elid: $this.data('id'),
				recept: "1"
  			}, function(data){
  				switch(data){
  					case 'done' : var response = 'Рецепт успешно добавлен в избранное';$this.addClass("checked");break;
  					case 'deleted' : var response = 'Рецепт успешно удален из избранного';$this.removeClass("checked");break;
  					case 'fail' : var response = 'Вы не авторизованы, либо Ваш запрос некорректен';$this.removeClass("checked");break;
  				}
  					$("#favorit_label").html(response).fadeIn().delay(5000).fadeOut();
  			});
  		});
    $(".buy").click(function(e2){
        e2.preventDefault();
  		var $this = $(this);
  			$.get("/recept/tobasketajax.php", {
  				elid: $this.data('id')
				}, function(data){
  				switch(data){
  					case 'done' : var response = 'Рецепт успешно добавлен в избранное';$this.addClass("checked");break;
  					case 'deleted' : var response = 'Рецепт успешно удален из избранного';$this.removeClass("checked");break;
  					case 'fail' : var response = 'Вы не авторизованы, либо Ваш запрос некорректен';$this.removeClass("checked");break;
  				}
  					$("#favorit_label").html(response).fadeIn().delay(5000).fadeOut();
  			})
  			});

})
</script>
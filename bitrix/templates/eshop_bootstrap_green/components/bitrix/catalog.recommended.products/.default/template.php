<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
echo 'ici';
$this->setFrameMode(true);
?>
<div class="products-items">
	<?
	$arBasketItems = array();
	$arBasketItemsAll = array();

	$dbBasketItems = CSaleBasket::GetList(
	        array(
	                "NAME" => "ASC",
	                "ID" => "ASC"
	            ),
	        array(
	                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
	                "LID" => SITE_ID,
	                "ORDER_ID" => "NULL"
	            ),
	        false,
	        false,
	        array()
	    );
	while ($arItems = $dbBasketItems->Fetch())
	{
	    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
	    {
	        CSaleBasket::UpdatePrice($arItems["ID"], 
	                                 $arItems["CALLBACK_FUNC"], 
	                                 $arItems["MODULE"], 
	                                 $arItems["PRODUCT_ID"], 
	                                 $arItems["QUANTITY"]);
	        $arItems = CSaleBasket::GetByID($arItems["ID"]);
	    }

	    $arBasketItemsAll[$arItems['PRODUCT_ID']] = $arItems;
	
	    $arBasketItems[] = $arItems['PRODUCT_ID'];
	}
foreach ($arResult['ITEMS'] as $arItem)
{
	$minPrice = $arItem['MIN_PRICE'];
	// Выведем актуальную корзину для текущего пользователя
	$res = CCatalogSKU::getOffersList(array($arItem['ID']), 0, array(), array(), array());
        $offers = "";
        foreach ($res as $key) {
        	foreach ($key as $value) {
        		$offers = $value['ID'];
        		break;
        	}
        }
        if(!empty($offers) && in_array($offers, $arBasketItems)) $arBasketItems[] = $arItem['ID'];
	?>
    <div class="card-product " id="product<?= $arItem["ID"];?>">
        <?$res = CIBlockElement::GetByID((int)$arItem["PROPERTIES"]["MAIN_STIKER"]["VALUE"]);
		if($ar_res = $res->GetNext()){?>
			<div class="w-card-product__category natural" style="float:left">
				<img src="<?=CFile::GetPath($ar_res["DETAIL_PICTURE"]);?>" alt="">
			</div>
		<?}?>
		<div  class="w-card-product__discount" style="display:<? echo (0 < $minPrice['DISCOUNT_DIFF_PERCENT'] ? '' : 'none'); ?>;" >Скидка <? echo $minPrice['DISCOUNT_DIFF_PERCENT']; ?>%</div>
      
        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="card-product__a">
            <div class="w-card-product__img">
                <img class="card-product__img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" id="bigImg<?=$arItem["ID"]?>" alt="">
            </div>
            <div class="card-product__title"><?=$arItem["NAME"]?></div>
        </a>
        <div class="w-card-product__bottom clearfix">
			<div class="card-product__price-full">
                <div class="card-product__price-discount">
				<?if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
				{
				?> <span><?=priceFormat($minPrice['VALUE']); ?></span>
					<div class="card-product__price-discount-currency"><span class="fa fa-ruble-sign"></span></div><?
				}?>
                    
                </div>
                <div class="card-product__price">
                    <div class="card-product__price-val"><?=priceFormat($minPrice['DISCOUNT_VALUE']);?></div>
                    <div class="card-product__price-currency"><span class="fa fa-ruble-sign"></span></div>
                </div>
				<div class="card-product__weight">
					<?
						if(isset($arItem['OFFERS']) && count($arItem['OFFERS']) > 0 && isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']])){
							print getFormatedWeight($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['CATALOG_WEIGHT']); 
						} 
						else{
							print getFormatedWeight($arItem['CATALOG_WEIGHT']);;
						}
					?>
				</div>
			</div>
			<div class="card-product__actions-full"<?if(in_array($arItem['ID'], $arBasketItems)) :?> style="width: 58%; text-align: left;"<?endif;?>>
			<?if(in_array($USER->GetId(),$arItem["PROPERTIES"]["F_USER"]["VALUE"])){
				$check = " active";
			}
			else{
				$check = "";
			};?>

			<div class="card-product__favorite">
				<a href="#" class="catalog-item-fav btn-fav favorite_check <?=$check?>" data-id="<?=$arItem['ID']?>"></a> 
			</div>
			<?if(!in_array($arItem['ID'], $arBasketItems)) :?>
                <div class="card-product__buy">
                  <a href="javascript:void(0)" data-id="<?=$arItem['ID']?>" data-quantity="<?=(!empty($arBasketItemsAll)&&isset($arBasketItemsAll[$arItem['ID']]))?$arBasketItemsAll[$arItem['ID']]['QUANTITY']:$arItem["CATALOG_MEASURE_RATIO"]?>" class="btn-buy"></a>
                </div>
            <?endif;?>
            </div>
				
					<form action="<?=POST_FORM_ACTION_URI?>" onsubmit="return false;" method="post" enctype="multipart/form-data" class="add_form<?if(in_array($arItem['ID'], $arBasketItems)) echo " active";?>">
						<div class="add_form_counter-line ajax-count">
							<a class="MinusList" href="javascript:void(0)">-</a> 
							<input type="hidden" name="quantity_ratio" value="<?=$arItem["CATALOG_MEASURE_RATIO"]?>" />	
							<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" data-id="<?=$arItem['ID']?>" value="<?=(!empty($arBasketItemsAll)&&isset($arBasketItemsAll[(!empty($offers))?$offers:$arItem['ID']]))?$arBasketItemsAll[(!empty($offers))?$offers:$arItem['ID']]['QUANTITY']:$arItem["CATALOG_MEASURE_RATIO"]?>" id="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?><?=$arItem['ID']?>"> 
							<a class="PlusList" href="javascript:void(0)">+</a>	
						</div>
						<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
						<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arItem["ID"]?>">
						<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="Купить" style="display:none;">
						<input class="add_form_btn" type="submit" id="link2card<?=$arItem['ID']?>" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="В корзине"/>
					</form>
        </div>
    </div>
   
	<?
}
?>
</div>



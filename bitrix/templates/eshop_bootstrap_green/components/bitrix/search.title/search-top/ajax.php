<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult["CATEGORIES"]))
	return;

if(!CModule::IncludeModule("search")){die();}
$obSearch = new CSearch;
$obSearch->Search(array(//при желании, фильтр можете еще сузить, см.документацию
   'QUERY' => $_REQUEST['q'],
   'SITE_ID' => SITE_ID,
   'MODULE_ID' => 'iblock',
   'PARAM2' => array(5)
));   
$count=0;
while ($row = $obSearch->fetch()) { 
	
	if($count<50){
		$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_PICTURE", "DETAIL_PICTURE", "DETAIL_PAGE_URL");
		$arFilter = Array("IBLOCK_ID"=>5, "ID"=>$row['ITEM_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$arrItems[$count]=$arFields;
			if(!empty($arFields['PREVIEW_PICTURE'])){
				$arrItems[$count]['PICTURE']=CFile::GetPath($arFields['PREVIEW_PICTURE']);
			}elseif(!empty($arFields['DETAIL_PICTURE'])){
				$arrItems[$count]['PICTURE']=CFile::GetPath($arFields['DETAIL_PICTURE']);
			}

			$dbPrice = CPrice::GetList(
				array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", 
					"SORT" => "ASC"),
				array("PRODUCT_ID" => $arFields['ID']),
				false,
				false,
				array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", 
					"QUANTITY_FROM", "QUANTITY_TO")
			);
			while ($arPrice = $dbPrice->Fetch())
			{
				$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
					$arPrice["ID"],
					$USER->GetUserGroupArray(),
					"N",
					SITE_ID
				);
				$discountPrice = CCatalogProduct::CountPriceWithDiscount(
					$arPrice["PRICE"],
					$arPrice["CURRENCY"],
					$arDiscounts
				);
			//echo'<pre>';print_r($discountPrice);echo'</pre>';
				$arPrice["DISCOUNT_PRICE"] = $discountPrice;
				$arrItems[$count]["PRICES"]=$arPrice;
				unset($arPrice);
			}

		}

		$count++;
	}else{
		break;
	}
}


?>

		<?//echo'<pre>';print_r($arResult['CATEGORIES']['all']['ITEMS'][0]["URL"]);echo'</pre>';
?>
<div class="bx_searche">
	<div class="bx_item_block all_result" style="text-align: center;">
		<div>
			<span class="all_result_title"><a href="<?echo $arResult['CATEGORIES']['all']['ITEMS'][0]["URL"]?>"><?echo $arResult['CATEGORIES']['all']['ITEMS'][0]["NAME"]?> (<?echo count($arrItems);?>)</a></span>
		</div>
		<div style="clear:both;"></div>
	</div>
	<? foreach ($arrItems as $key => $arItem) {?>
		<div class="bx-basket-item-list-items">
			<?if (!empty($arItem["PICTURE"])):?>
			<div class="bx-basket-item-list-item-img">
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
					<img src="<?echo $arItem["PICTURE"]?>" alt="<?=$arItem['NAME']?>">
				</a>
			</div>
			<?endif;?>
			<div class="bx-basket-item-list-item-name">
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>		

				<div class="bx-basket-item-list-item-price-block">
				<?
		/*echo'<pre>';print_r($arItem['PRICES']["PRICE"]);echo'</pre>';
		echo'<pre>';print_r($arItem['PRICES']["DISCOUNT_PRICE"]);echo'</pre>';
echo'<pre>';print_r($arItem['PRICES']);echo'</pre>';*/
						if($arItem['PRICES']["DISCOUNT_PRICE"] < round($arItem['PRICES']["PRICE"])):?>
							<div class="bx-basket-item-list-item-old-price">
								<?=$arItem['PRICES']["PRICE"]?> <i class="fa fa-ruble-sign"></i>
							</div>
							<div class="bx-basket-item-list-item-price">
								<?=numberFormat($arItem['PRICES']["DISCOUNT_PRICE"], 2, '.', ' ')?> <i class="fa fa-ruble-sign"></i>
							</div>
							<?else:?>
							<div class="bx-basket-item-list-item-price"><?=$arItem['PRICES']["PRICE"]?> <i class="fa fa-ruble-sign"></i></div>
							<?endif;
					
					?>
				</div>
			</div>
			
				<div style="clear:both;"></div>
			</div>
	<?} ?>
</div>
<!-- <div class="bx_searche">
<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
	<?foreach($arCategory["ITEMS"] as $i => $arItem):?>
		<?//echo $arCategory["TITLE"]?>
		<?if($category_id === "all"):?>
			<div class="bx_item_block all_result" style="text-align: center;">
				<div>
					<span class="all_result_title"><a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?> (<?echo $count; $count=0;?>)</a></span>
				</div>
				<div style="clear:both;"></div>
			</div>
		<?elseif(isset($arResult["ELEMENTS"][$arItem["ITEM_ID"]])):
			$arElement = $arResult["ELEMENTS"][$arItem["ITEM_ID"]];?>

			<div class="bx-basket-item-list-items">
				<?if (is_array($arElement["PICTURE"])):?>
					<div class="bx-basket-item-list-item-img">
						<a href="<?echo $arItem["URL"]?>">
							<img src="<?echo $arElement["PICTURE"]["src"]?>" alt="<?=$arItem['NAME']?>">
						</a>
					</div>
				<?endif;?>
				<div class="bx-basket-item-list-item-name">
					<a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></a>				
				</div>
				<div class="bx-basket-item-list-item-price-block">
						<?
					foreach($arElement["PRICES"] as $code=>$arPrice)
					{
						if ($arPrice["MIN_PRICE"] != "Y")
							continue;

						if($arPrice["CAN_ACCESS"])
						{
							if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
								<div class="bx-basket-item-list-item-old-price">
									<?=numberFormat($arPrice["VALUE"], 2, '.', ' ')?> <i class="fa fa-ruble-sign"></i>
								</div>
								<div class="bx-basket-item-list-item-price">
									<?=numberFormat($arPrice["DISCOUNT_VALUE"], 2, '.', ' ')?> <i class="fa fa-ruble-sign"></i>
								</div>
							<?else:?>
								<div class="bx-basket-item-list-item-price"><?=numberFormat($arPrice["VALUE"], 2, '.', ' ')?> <i class="fa fa-ruble-sign"></i></div>
							<?endif;
						}
						if ($arPrice["MIN_PRICE"] == "Y")
							break;
					}
					?>
					</div>
				<div style="clear:both;"></div>
			</div>
		<?else:?>
			<div class="bx-basket-item-list-items others_result">
				<div>
					<a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></a>
				</div>
				<div style="clear:both;"></div>
			</div>
		<?endif;?>
	<?endforeach;?>
<?endforeach;?>
</div> -->
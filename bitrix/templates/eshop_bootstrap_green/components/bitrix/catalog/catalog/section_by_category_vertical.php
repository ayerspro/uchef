<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader,
	Bitrix\Main\ModuleManager;
	

?>
<!--<div class="<?=(($isFilter || $isSidebar) ? "col-md-9 col-sm-8 col-sm-pull-4 col-md-pull-3" : "col-xs-12")?>">-->
<style>
	.section-main_about .container {
		margin: 0 !important;
		width: auto !important;
		padding: 0 !important;	
	}
</style>
<div class="col-xs-12">
	<div class="row">
		<div class="col-xs-12">
	<?
	if($arParams["USE_COMPARE"]=="Y")
	{
		?><?/*$APPLICATION->IncludeComponent(
			"bitrix:catalog.compare.list",
			"",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"NAME" => $arParams["COMPARE_NAME"],
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
				"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
				"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_ccl",
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
				'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);*/?><?
	}

	if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
		$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
	else
		$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');

	$intSectionID = 0;
	if (isset($_GET["filter"]))
		$GLOBALS['arrPropFilter'] = array("PROPERTY_STIKERS"=>$_GET["filter"]);
		?>
<?
$current_sort_type = $_GET['sort'];
$current_sort_order = $_GET['order'];

if(empty($current_sort_type) && empty($current_sort_order)){
	$current_sort_type = "price";
	$current_sort_order = "ubiv";
}

if($current_sort_type == "price"){
	$arParams["ELEMENT_SORT_FIELD"] = "catalog_PRICE_1";
}
else if($current_sort_type == 'date'){
	$arParams["ELEMENT_SORT_FIELD"] = 'desc';
}
else if($current_sort_type == "name"){
	$arParams["ELEMENT_SORT_FIELD"] = "NAME";
}

if ($current_sort_order == "ubiv"){ 
	$arParams["ELEMENT_SORT_ORDER"]= "desc";
}
else if ($current_sort_order == "vozvr") {
	$arParams["ELEMENT_SORT_ORDER"]= "asc";
}


$view = '.default';
if(isset($_REQUEST["view"])){ 
	$view = htmlspecialchars($_REQUEST["view"]); 
	$APPLICATION->set_cookie("view", $view); 
} 
else{ 
	$view = $APPLICATION->get_cookie("view")?$APPLICATION->get_cookie("view"):".default"; 
}


$sect = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array('IBLOCK_ID' => $arParams['IBLOCK_ID']), false, Array(), false)->GetNext();
$sectionName = $sect['NAME'];

$title = "";
if(isset($_REQUEST['CATEGORY'])){
	$dbElement = CIBlockElement::GetById($_REQUEST['CATEGORY']);
	
	if($arElement = $dbElement->GetNext()){
		$title = $arElement['NAME'];
	}
}



?> 
</div>
<div class="col-xs-12">
	<div class="row">
		<div class="mobile-visible">
			<?/*$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
					"START_FROM" => "0",
					"PATH" => "",
					"SITE_ID" => "-"
				),
				false,
				Array('HIDE_ICONS' => 'Y')
			);*/?>
		</div>
		<div class="col-lg-2 col-md-4 col-sm-4 cat-menu">
			<div class="wrap-menu">
			<div id="vertical-multilevel-menu">
				<!--<a class="btn btn-lg btn-default btn-block menu-title" role="button" data-toggle="collapse" href="#collapseRoot" aria-expanded="false" aria-controls="collapseRoot"><?=$sectionName?> <i class="fa fa-caret-right" aria-hidden="true"></i></a>-->
		
			<?php
				$APPLICATION->IncludeComponent("bitrix:menu", "user_vertical_multilevel2", Array(
					"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
						"MENU_CACHE_TYPE" => "A",	// Тип кеширования
						"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
						"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
						"MAX_LEVEL" => "1",	// Уровень вложенности меню
						"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
						"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						"DELAY" => "N",	// Откладывать выполнение шаблона меню
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
						"AJAX_MODE" => "Y",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "Y",
						"AJAX_OPTION_JUMP" => "Y",
						"AJAX_OPTION_STYLE" => "Y",
						"COMPONENT_TEMPLATE" => "user_vertical_multilevel"
					),
					false
				);
			?>
			</div>
			<div class="menu-second">
				<?/*$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"user_vertical_multilevel1", 
					array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "catalog",
						"DELAY" => "N",
						"MAX_LEVEL" => "1",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "catalog",
						"USE_EXT" => "N",
						"COMPONENT_TEMPLATE" => "user_vertical_multilevel"
					),
					false
				);*/?>
			</div>

			<? 
				
				if(isset($_REQUEST['CATEGORY'])){
					if(!is_array($GLOBALS[$arParams["FILTER_NAME"]])){
						$GLOBALS[$arParams["FILTER_NAME"]] = array();
					}
					
					$GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_CATEGORY'] = $_REQUEST['CATEGORY'];
				}

			?>			

			
			<?/*$APPLICATION->IncludeComponent(
				"bitrix:catalog.smart.filter",
				"",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"DISPLAY_ELEMENT_COUNT" => 'Y',
					"SAVE_IN_SESSION" => "N",
					"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
					"XML_EXPORT" => "Y",
					"SECTION_TITLE" => "NAME",
					"SECTION_DESCRIPTION" => "DESCRIPTION",
					'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
					"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					"SEF_MODE" => $arParams["SEF_MODE"],
					"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
					"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
					"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
					"INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
				),
				$component,
				array('HIDE_ICONS' => 'Y')
			);*/?>
		</div>
	</div>
		<div class="col-sm-8 col-md-8 col-lg-10">
			<div class="mobile-hide">
				<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
						"START_FROM" => "0",
						"PATH" => "",
						"SITE_ID" => "-"
					),
					false,
					Array('HIDE_ICONS' => 'Y')
				);?>
			</div>
			
			<br>
			
			
			<?if(!empty($title)):?>
			<h1 class="text-center catalog-title"><?=$title?></h1>
			<?endif;?>
			<div class="row">
				<?/*$APPLICATION->IncludeComponent("bitrix:sale.bestsellers", "", array(
					"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
					"PAGE_ELEMENT_COUNT" => "5",
					"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
					"PRODUCT_SUBSCRIPTION" => $arParams['PRODUCT_SUBSCRIPTION'],
					"SHOW_NAME" => "Y",
					"SHOW_IMAGE" => "Y",
					"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
					"MESS_BTN_DETAIL" => $arParams['MESS_BTN_DETAIL'],
					"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
					"MESS_BTN_SUBSCRIBE" => $arParams['MESS_BTN_SUBSCRIBE'],
					"LINE_ELEMENT_COUNT" => 5,
					"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"BY" => array(
						0 => "AMOUNT",
					),
					"PERIOD" => array(
						0 => "15",
					),
					"FILTER" => array(
						0 => "CANCELED",
						1 => "ALLOW_DELIVERY",
						2 => "PAYED",
						3 => "DEDUCTED",
						4 => "N",
						5 => "P",
						6 => "F",
					),
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"ORDER_FILTER_NAME" => "arOrderFilter",
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
					"CURRENCY_ID" => $arParams["CURRENCY_ID"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_slb",
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
					"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
					"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"] => "Y",
					"OFFER_TREE_PROPS_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
					"ADDITIONAL_PICT_PROP_".$arParams['IBLOCK_ID'] => $arParams['ADD_PICT_PROP'],
					"ADDITIONAL_PICT_PROP_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams['OFFER_ADD_PICT_PROP']
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);*/?>

			

			<div class="mobile-hide">
				<?/*$APPLICATION->IncludeComponent(
				"bitrix:catalog.section.list",
				"",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
					"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"VIEW_MODE" => "TILE",
					"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
					"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
					"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);*/?>
			</div>

			<div class="mobile-visible products-items list">
				<?/*$APPLICATION->IncludeComponent(
				"bitrix:catalog.section.list",
				"",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
					"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"VIEW_MODE" => "TILE",
					"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
					"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
					"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);*/?>
			</div>

			<?/*$APPLICATION->IncludeComponent("bitrix:menu", "cat-block-menu", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "catalog",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "catalog",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"COMPONENT_TEMPLATE" => "user_vertical_multilevel"
	),
	false
);*/?>

			<div class="container-fluid">
				<div class="flexible pull-left sort-line" style="margin-top:22px;">
					<div class="select-wrapp">
						<div class="dropdown">
							Сортировать по 
						  <a data-target="#sort-dropdown" href="#sort-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						    <?
									$sortByLabel = "";
									
									if($current_sort_type == "price"){
										$sortByLabel .= "Цене";
									}
									else if($current_sort_type == "date"){
										$sortByLabel .= "Дате";
									}
									else if($current_sort_type == "name"){
										$sortByLabel .= "Наименованию";
									}
									
									if($current_sort_order == "ubiv"){
										$sortByLabel .= " (убыв.)";
									}
									else if($current_sort_order == "vozvr"){
										$sortByLabel .= " (возвр.)";
									}
		
									echo $sortByLabel;
								
								?>
						  </a>
						  <span class="caret"></span>

						  <ul class="dropdown-menu" id="sort-dropdown">
						  	<li><a href="?">Дате</a></li>
						    <li><a href="?sort=price&order=ubiv">Цене (убыв.)</a></li>
						    <li><a href="?sort=price&order=vozvr">Цене (возвр.)</a></li>
						    <li><a href="?sort=name&order=ubiv">Наименованию (убыв.)</a></li>
						    <li><a href="?sort=name&order=vozvr">Наименованию (возвр.)</a></li>
						  </ul>
						</div>
					</div>
					<div class="view">
						<a href="?CATEGORY=<?=$_REQUEST['CATEGORY']?>&view=.default"><i class="fa fa-lg fa-th-large" aria-hidden="true"></i></a>
						<a href="?CATEGORY=<?=$_REQUEST['CATEGORY']?>&view=list"><i class="fa fa-lg fa-th-list" aria-hidden="true"></i></a>
					</div>
				</div>
				<div class="panel-pagination pull-right">
					<?$APPLICATION->ShowViewContent('pagination_section');?>
				</div>
			</div>
			<?if(isset($_REQUEST['show'])) $arParams["PAGE_ELEMENT_COUNT"] = $_REQUEST['show'];?>
			
			<? 
				if(isset($_REQUEST['CATEGORY'])){
					if(!is_array($GLOBALS[$arParams["FILTER_NAME"]])){
						$GLOBALS[$arParams["FILTER_NAME"]] = array();
					}
					
					$GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_CATEGORY'] = $_REQUEST['CATEGORY'];
				}
				
				if(isset($_REQUEST['SALES'])){
					$arDiscounts = getDiscounts($GLOBALS[$arParams["FILTER_NAME"]]);
					
					
					if(count($arDiscounts) == 0){
						$arDiscounts[] = 0;
					}
						
					
					if(!is_array($GLOBALS[$arParams["FILTER_NAME"]])){
						$GLOBALS[$arParams["FILTER_NAME"]] = array();
					}
					
					$GLOBALS[$arParams["FILTER_NAME"]]["=ID"] = $arDiscounts;
				}
				
				
			?>
			
			
			<?$intSectionID = $APPLICATION->IncludeComponent(
				"bitrix:catalog.section",
				$view,
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					
					"SHOW_ALL_WO_SECTION" => "Y",
					"SET_TITLE" => 'N',

					"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
					"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
					"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
					"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
					"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
					"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
					"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
					"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
					"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					"PAGE_ELEMENT_COUNT" => 16,
					"LINE_ELEMENT_COUNT" => 4,
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
					"SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],

					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
					"SECTION_ID" => $arParams["SECTION_ID"],
					"SECTION_CODE" => $arParams["SECTION_CODE"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],	
				),
				$component
			);?>
			</div>
			<?
				$GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
				unset($basketAction);

				if (ModuleManager::isModuleInstalled("sale"))
				{
					if (!empty($arRecomData))
					{
						if (!isset($arParams['USE_SALE_BESTSELLERS']) || $arParams['USE_SALE_BESTSELLERS'] != 'N')
						{
							?>
			
			<?}
				if (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N')
				{
					?>
			<?$APPLICATION->IncludeComponent("bitrix:catalog.bigdata.products", "", array(
					"LINE_ELEMENT_COUNT" => 5,
					"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_cbdp",
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
					"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
					"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
					"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
					"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
					"PRODUCT_SUBSCRIPTION" => $arParams['PRODUCT_SUBSCRIPTION'],
					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"SHOW_NAME" => "Y",
					"SHOW_IMAGE" => "Y",
					"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
					"MESS_BTN_DETAIL" => $arParams['MESS_BTN_DETAIL'],
					"MESS_BTN_SUBSCRIBE" => $arParams['MESS_BTN_SUBSCRIBE'],
					"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
					"PAGE_ELEMENT_COUNT" => 5,
					"SHOW_FROM_SECTION" => "Y",
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"DEPTH" => "2",
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"] => "Y",
					"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
					"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
					"CURRENCY_ID" => $arParams["CURRENCY_ID"],
					"SECTION_ID" => $intSectionID,
					"SECTION_CODE" => "",
					"SECTION_ELEMENT_ID" => "",
					"SECTION_ELEMENT_CODE" => "",
					"LABEL_PROP_".$arParams["IBLOCK_ID"] => $arParams['LABEL_PROP'],
					"PROPERTY_CODE_".$arParams["IBLOCK_ID"] => $arParams["LIST_PROPERTY_CODE"],
					"PROPERTY_CODE_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"CART_PROPERTIES_".$arParams["IBLOCK_ID"] => $arParams["PRODUCT_PROPERTIES"],
					"CART_PROPERTIES_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFERS_CART_PROPERTIES"],
					"ADDITIONAL_PICT_PROP_".$arParams["IBLOCK_ID"] => $arParams['ADD_PICT_PROP'],
					"ADDITIONAL_PICT_PROP_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams['OFFER_ADD_PICT_PROP'],
					"OFFER_TREE_PROPS_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
					"RCM_TYPE" => (isset($arParams['BIG_DATA_RCM_TYPE']) ? $arParams['BIG_DATA_RCM_TYPE'] : '')
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);?>
					<?
					}
				}
			}
			?>
			<?/*$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/about-company-txt.php"
	)
);*/?>
		</div>

	</div>
</div>

</div>
</div>

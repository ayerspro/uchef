<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
global $APPLICATION;

if (isset($arResult['arResult'])) {
   $arResult =& $arResult['arResult'];
   global $MESS;
   include_once(GetLangFileName(dirname(__FILE__).'/lang/', '/template.php'));
}
else {
   return;
}

$templateLibrary = array('popup');
$currencyList = '';
$showSliderControls = false;

if (!empty($arResult['CURRENCIES'])) {
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => __FILE__ .'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'TREE_ID' => $strMainID.'_skudiv',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers) {
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer) {
		if ($offer['MORE_PHOTO_COUNT'] > 1) {
			$showSliderControls = true;
			break;
		}
	}
}
else {
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$arBasketItems = array();

$dbBasketItems = CSaleBasket::GetList(
	array(
			"NAME" => "ASC",
			"ID" => "ASC"
		),
	array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
	false,
	false,
	array()
);

while ($arItems = $dbBasketItems->Fetch()) {
	if (strlen($arItems["CALLBACK_FUNC"]) > 0) {
		CSaleBasket::UpdatePrice($arItems["ID"],
								 $arItems["CALLBACK_FUNC"],
								 $arItems["MODULE"],
								 $arItems["PRODUCT_ID"],
								 $arItems["QUANTITY"]);
		$arItems = CSaleBasket::GetByID($arItems["ID"]);
	}
	$arBasketItems[] = array('PRODUCT_ID' => $arItems['PRODUCT_ID'], 'QUANTITY' => $arItems["QUANTITY"]);
}



$strTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult['NAME']
);
$strAlt = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult['NAME']
);
?>

<div class="bx_item_detail <?= $templateData['TEMPLATE_CLASS']; ?>" id="<?= $arItemIDs['ID']; ?>" itemscope itemtype="http://schema.org/Product">
	<?if ('Y' == $arParams['DISPLAY_NAME']) :?>
		<div class="bx_item_title"><h1><span><?= (
			isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
			? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
			: $arResult["NAME"]
		); ?>
		</span></h1></div>
	<?endif;?>
	<?
	reset($arResult['MORE_PHOTO']);
	$arFirstPhoto = current($arResult['MORE_PHOTO']);
	?>
	<div class="bx_item_container">
		<a href="<?= $arResult["SECTION"]["SECTION_PAGE_URL"]?>" class="back_catalog"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Назад к списку товаров</a>
		<div class="md-hidden">
		<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "-"
			),
			false,
			Array('HIDE_ICONS' => 'Y')
		);?>
		</div>
		<div class="container-fluid" id="cart_element_<?=$arResult['ID']?>">
			<br><br><br>
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-6">
							<div class="row element-prevu">
								<div class="col-lg-3 col-sm-3" style="padding-right: 0;">
									<div class="bx_slide">
										<?if ($showSliderControls) : ?>
											<?if ($haveOffers) : ?>
												<?foreach ($arResult['OFFERS'] as $keyOffer => $offer) : ?>
													<?
													if (!isset($offer['MORE_PHOTO_COUNT']) || $offer['MORE_PHOTO_COUNT'] <= 0) continue;
													$strVisible = $arResult['OFFERS_SELECTED'] == $keyOffer ? '' : 'none';
													?>
													<div id="<?=$arItemIDs['SLIDER_CONT_OF_ID'].$offer['ID']?>" style="display: <?=$strVisible?>;">
														<ul class="list-unstyled slider-list"  id="<?=$arItemIDs['SLIDER_LIST_OF_ID'].$offer['ID']?>">
															<?foreach ($offer['MORE_PHOTO'] as $keyPhoto => $photo) : ?>
																<li class="product-item-detail-slider-controls-image<?=($keyPhoto == 0 ? ' active' : '')?>"
																	data-entity="slider-control" data-value="<?=$offer['ID'].'_'.$photo['ID']?>">
																	<img class="cnt_item img-responsive img-rounded" src="<?=$photo['SRC']?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>">
																</li>
															<?endforeach;?>
														</ul>
													</div>
												<?endforeach;?>
											<?else:?>
												<ul class="list-unstyled slider-list" id="<?= $arItemIDs['SLIDER_LIST']; ?>">
													<?foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto) : ?>
														<li data-value="<?= $arOnePhoto['ID']; ?>">
															<img class="cnt_item img-responsive img-rounded" src="<?= $arOnePhoto['SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>">
														</li>
													<?endforeach;?>
												</ul>
											<?endif;?>
											<?
											unset($arOnePhoto);
											?>
										<?else:?>
											<ul class="list-unstyled">
												<?foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto) : ?>
													<li data-value="<?= $arOnePhoto['ID']; ?>">
														<img class="cnt_item img-responsive img-rounded" src="<?= $arOnePhoto['SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>">
													</li>
												<?endforeach;?>
											</ul>
										<?endif;?>
									</div>
								</div>
								<div class="col-lg-9 col-sm-9 image-element">
									<div class="image-element-wrapper img-rounded">
										<?
										$res = CIBlockElement::GetByID((int)$arResult["PROPERTIES"]["MAIN_STIKER"]["VALUE"]);
										?>
										<?if ($ar_res = $res->GetNext()) : ?>
											<div class="w-card-product__category natural" style="float:left;left: 5px;top: -25px;">
												<img src="<?= CFile::GetPath($ar_res["DETAIL_PICTURE"]);?>" alt="<?= $ar_res['NAME']?>" title="<?= $ar_res['NAME']?>">
											</div>
										<?endif;?>
										<img itemprop="image" id="<? echo $arItemIDs['PICT']; ?>" class="img-responsive img-rounded" src="<?= $arFirstPhoto['SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>">
										<?if ($arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) : ?>
											<div class="bx-stick-discount">Скидка <?= -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
										<?endif;?>
									</div>
								</div>
							</div>

							<?if (isset($arResult["PROPERTIES"]["STIKERS"])) : ?>
								<?if (count($arResult["PROPERTIES"]["STIKERS"]['VALUE']) > 0) : ?>
									<ul class="natural-icon">
										<?foreach($arResult["PROPERTIES"]["STIKERS"]['VALUE'] as $stikerId) : ?>
											<?
											$stikerRes = CIBlockElement::GetByID((int)$stikerId);
											?>
											<?if ($arStiker = $stikerRes->GetNext()) : ?>
												<li>
													<span class="wrap">
														<span class="icon">
															<img src="<?=CFile::GetPath($arStiker["DETAIL_PICTURE"]);?>" alt="<?=$arStiker['NAME']?>">
														</span>
														<span class="txt"><?=$arStiker['NAME']?></span>
													</span>
												</li>
											<?endif;?>
										<?endforeach;?>
									</ul>
								<?endif;?>
							<?endif;?>

						</div>
						<div class="col-lg-5 col-md-6 col-sm-6 text-center element-main-panel">
							<h1 class="product-name" itemprop="name">
								<?= (
									isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != '')
									? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
									: $arResult["NAME"];
								?>
							</h1>
							<div class="product-sku text-center">
								<?if (!empty($arResult['DISPLAY_PROPERTIES']['ARTNUMBER']['VALUE'])) : ?>
									Артикул: <?=$arResult['DISPLAY_PROPERTIES']['ARTNUMBER']['VALUE']; ?>
								<?endif;?>
								<?if ($arResult['PROPERTIES']['PRE_ORDER']['VALUE_ENUM'] == 'Y') : ?>
									<span class="offers-quantity">Только по предзаказу.</span>
								<?else:?>
									<?if (!empty($arResult['OFFERS'])) : ?>
										<span class="offers-quantity"><?= ($arResult['OFFERS'][0]['CATALOG_QUANTITY'] > 0)? 'В наличии': 'Нет в наличии'; ?></span>
									<?else:?>
										<span class="offers-quantity"><?= ($arResult['CATALOG_QUANTITY'] > 0)? 'В наличии': 'Нет в наличии'; ?></span>
									<?endif;?>
								<?endif;?>
							</div>
							<br/>
						<div class="item_price">
							<?
							$minPrice = $arResult['MIN_PRICE'];
							$boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);
							?>

							<?if ($arParams['SHOW_OLD_PRICE'] == 'Y') : ?>
								<div class="product-old-price" style="display: <?= ($boolDiscountShow ? '' : 'none'); ?>"><span id="<?= $arItemIDs['OLD_PRICE']; ?>"><?= ($boolDiscountShow ? priceFormat($minPrice['VALUE']) : ''); ?></span> <span class="fa fa-ruble-sign"></span></div>
							<?endif;?>
							<?if ($arParams['SHOW_OLD_PRICE'] == 'Y') : ?>
								<div class="product-new-price" id="<?= $arItemIDs['DISCOUNT_PRICE']; ?>" style="display: <?= ($boolDiscountShow ? '' : 'none'); ?>"><?= ($boolDiscountShow ? GetMessage('CT_BCE_CATALOG_ECONOMY_INFO', array('#ECONOMY#' => priceFormat($minPrice['DISCOUNT_DIFF']))) : ''); ?></div>
							<?endif;?>
							<div class="product-new-price" id="<?= $arItemIDs['BASIS_PRICE']; ?>"><?= priceFormat($minPrice['PRINT_DISCOUNT_VALUE']); ?> <span class="fa fa-ruble-sign"></span><span> / <?= $arResult['ITEM_MEASURE']['TITLE']?></span></div>
						</div>
						<div id="<?= $arItemIDs['TREE_ID']?>" class="bx_size_scroller_container">
							<?foreach ($arResult['SKU_PROPS'] as &$arProp) {
								if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']])) {
									continue;
								}
								$arSkuProps[] = array(
									'ID' => $arProp['ID'],
									'SHOW_MODE' => $arProp['SHOW_MODE'],
									'VALUES_COUNT' => $arProp['VALUES_COUNT'],
								);
								if ('TEXT' == $arProp['SHOW_MODE']) {?>
									<?if (5 < $arProp['VALUES_COUNT']) : ?>
										<?
										$strClass = 'bx_item_detail_size full';
										$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
										$strWidth = (20*$arProp['VALUES_COUNT']).'%';
										$strSlideStyle = '';
										?>
									<?else:?>
										<?
										$strClass = 'bx_item_detail_size';
										$strOneWidth = '20%';
										$strWidth = '100%';
										$strSlideStyle = 'display: none;';
										?>
									<?endif;?>
									<div class="bx_size" id="<?= $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
										<ul class="size" id="<?= $arItemIDs['PROP'].$arProp['ID']; ?>_list">
											<?
											$ii = 0;
											foreach ($arProp['VALUES'] as $arOneValue) {
												$ii++;
												$switchID = 0;
												$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
												foreach ($arResult['OFFERS'] as $offerKey => $offerValue) {
													if ($offerValue['TREE']['PROP_'.$arProp['ID']] == $arOneValue['ID']) {
														$switchID = $arResult['OFFERS'][$offerKey]['ID'];
													}
												}
												?>
												<li data-treevalue="<?= $arProp['ID'].'_'.$arOneValue['ID']; ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" <?if($arOneValue['ID'] == 0):?>style="display:none;"<?endif;?>  data-switchid="<?=$switchID;?>">
													<?= $arOneValue['NAME']; ?>
												</li>
											<?
											}
											?>
										</ul>
									</div>
								<?}
							}?>
						</div>

							<div class="item_info_section">
							<? if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
								$canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
							} else {
								$canBuy = $arResult['CAN_BUY'];
							}
							$buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
							$addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
							$notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
							$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
							$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

							if($arResult['CATALOG_SUBSCRIBE'] == 'Y')
								$showSubscribeBtn = true;
							else
								$showSubscribeBtn = false;
							$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));

							if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y')
							{
								?>
								<div class="item_buttons vam">
									<span class="item_buttons_counter_block counter-block">
										<a href="javascript:void(0)" id="<?=$arItemIDs['QUANTITY_DOWN']?>" class="bx_bt_button_type_2 bx_small bx_fwb btn-plus" >-</a>
										<input id="<?= $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input" value="<?= (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) ? 1 : $arResult['CATALOG_MEASURE_RATIO']); ?>" disabled>
										<a href="javascript:void(0)" id="<?=$arItemIDs['QUANTITY_UP']?>" class="bx_bt_button_type_2 bx_small bx_fwb btn-minus" >+</a>
									</span><span class="product-measure"><?=$arResult['ITEM_MEASURE']['TITLE']?>.</span>
									<input type="hidden" id="hide-count" value="1">
									<?if($showSubscribeBtn) : ?>
										<?$APPLICATION->includeComponent('bitrix:catalog.product.subscribe','',
											array(
												'PRODUCT_ID' => $arResult['ID'],
												'BUTTON_ID' => $arItemIDs['SUBSCRIBE_LINK'],
												'BUTTON_CLASS' => 'bx_big bx_bt_button',
												'DEFAULT_DISPLAY' => !$canBuy,
											),
											$component, array('HIDE_ICONS' => 'Y')
										);?>
									<?endif;?>
									<br>
								</div>

								<? if ('Y' == $arParams['SHOW_MAX_QUANTITY']) {
										if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) { ?>
								<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>" style="display: none;"><? echo GetMessage('OSTATOK'); ?>: <span></span></p>
								<? } else {
									if ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO']) { ?>
									<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>: <span><? echo $arResult['CATALOG_QUANTITY']; ?></span></p>
								<? } } } } else { ?>
									<div class="item_buttons vam">


								<? } ?>
									<?if($showSubscribeBtn) :?>
										<?$APPLICATION->IncludeComponent('bitrix:catalog.product.subscribe','',
											array(
												'PRODUCT_ID' => $arResult['ID'],
												'BUTTON_ID' => $arItemIDs['SUBSCRIBE_LINK'],
												'BUTTON_CLASS' => 'bx_big bx_bt_button',
												'DEFAULT_DISPLAY' => !$canBuy,
											),
											$component, array('HIDE_ICONS' => 'Y')
										);?>
									<?endif;?>
									<br>
									<span id="<?= $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable<?=($showSubscribeBtn ? ' bx_notavailable_subscribe' : ''); ?>" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
								<?if ($arParams['DISPLAY_COMPARE']) : ?>

								<?endif;?>
								</div>
							<div class="all-panel" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
								<?if (!empty($arResult['MIN_PRICE']['VALUE'])) : ?>
									<span class="txt-all-price">
										<?
										$minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
										?>
										Всего: <b id="<?= $arItemIDs['PRICE']; ?>" class="price"><?= priceFormat($minPrice['VALUE'] - $minPrice['DISCOUNT_DIFF']);?></b> <b><span class="fa fa-ruble-sign"></span></b>
									</span>
								<?endif;?>
								<div id="<?= $arItemIDs['BASKET_ACTIONS']?>" style="display: <?= ($canBuy ? '' : 'none')?>;">
									<?if ($showBuyBtn) : ?>
										<a href="javascript:void(0);" class="btn btn-default bx_cart" data-id="<?=$arResult['ID']?>" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><?= $buyBtnMessage ?></a>
									<?endif;?>
									<?if ($showAddBtn) : ?>
										<a href="javascript:void(0);" class="btn btn-default bx_cart" data-id="<?=$arResult['ID']?>" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
									<?endif;?>
								</div>
								<meta itemprop="price" content="<?=($minPrice['VALUE'] - $minPrice['DISCOUNT_DIFF']);?>">
								<meta itemprop="priceCurrency" content="RUB">
								<?if ($arResult['PROPERTIES']['PRE_ORDER']['VALUE_ENUM'] == 'Y') : ?>
									<link itemprop="availability" href="http://schema.org/PreOrder"/>
								<?else:?>
									<?if (!empty($arResult['OFFERS'])) : ?>
										<?=($arResult['OFFERS'][0]['CATALOG_QUANTITY'] > 0) ? '<link itemprop="availability" href="http://schema.org/InStock"/>': '<link itemprop="availability" href="http://schema.org/OutOfStock"/>';?>
									<?else:?>
										<?=($arResult['CATALOG_QUANTITY'] > 0) ? '<link itemprop="availability" href="http://schema.org/InStock"/>': '<link itemprop="availability" href="http://schema.org/OutOfStock"/>';?>
									<?endif;?>
								<?endif;?>
							</div>
							<div class="flexible hcenter">
								<div>
									<?/*$APPLICATION->IncludeComponent(
										"bitrix:main.include",
										"",
										Array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR."include/info.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "html",
										),
										false,
										Array('HIDE_ICONS' => 'N')
									);*/?>
								</div>
							</div>

							<div class="panel-btn">
								<ul>
									<li>
										<?
										if (in_array($USER->GetId(),$arResult["PROPERTIES"]["F_USER"]["VALUE"])) {
											$check = " active";
										}
										else {
											$check = "";
										}
										?>
										<a href="#" class="favorite favorite_check <?= $check?>" data-id="<?= $arResult['ID']?>"></a>
									</li>
									<li><a href="#" class="tell-friend"></a></li>
								</ul>

								<div class="share42init"></div>
								<script type="text/javascript" src="/share42/share42.js"></script>
							</div>
							
							<div class="blck-info">
								<?if (!empty($arResult['DISPLAY_PROPERTIES'])) : ?>
									<div class="row">
										<?foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp) : ?>
											<div class="col-xs-5"><?= $arOneProp['NAME']; ?>:</div>
											<div class="col-xs-7">
												<?= (
													is_array($arOneProp['DISPLAY_VALUE'])
													? implode(' / ', $arOneProp['DISPLAY_VALUE'])
													: $arOneProp['DISPLAY_VALUE']
												); ?>
											</div>
										<?endforeach;?>
										<?
										unset($arOneProp);
										?>
									</div>
								<?endif;?>




								<?/*<div class="text-center">
									<a class="propertiesLinkCollapse" role="button" data-toggle="collapse" href="#propertiesCollapse" aria-expanded="false" aria-controls="propertiesCollapse">
									  Пищевая ценность, состав продукта, аллергены <span class="caret"></span>
									</a>
								</div>
								<div class="collapse" id="propertiesCollapse">
									<div class="row">
										<? if(!empty($arResult['PROPERTIES']['BGY']['VALUE'])) { ?>
											<div class="col-xs-5"><?= $arResult['PROPERTIES']['BGY']['NAME']; ?>:</div>
											<div class="col-xs-7">
											<?=$arResult['PROPERTIES']['BGY']['VALUE'] ?>
											</div>
										<? } ?>
										<?if(!empty($arResult['PROPERTIES']['COMPOSITION']['VALUE'])) { ?>
											<div class="col-xs-5"><?= $arResult['PROPERTIES']['COMPOSITION']['NAME']; ?>:</div>
											<div class="col-xs-7">
											<?=$arResult['PROPERTIES']['COMPOSITION']['VALUE'] ?>
											</div>
										<? } ?>
									</div>
								</div>*/?>
							</div>
							
						</div>
						
						<div style="clear: both;"></div>
						<?
						unset($showAddBtn, $showBuyBtn);
						?>
					</div>
				</div>
				
				<div class="col-lg-12">
					<div class="container detail__coupons">
						<div class="coupons__wrap">
							<?$APPLICATION->IncludeComponent(
								"bitrix:news.list",
								"coupons",
								Array(
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"ADD_SECTIONS_CHAIN" => "N",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_ADDITIONAL" => "",
									"AJAX_OPTION_HISTORY" => "N",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "N",
									"CACHE_FILTER" => "N",
									"CACHE_GROUPS" => "N",
									"CACHE_TIME" => "36000000",
									"CACHE_TYPE" => "N",
									"CHECK_DATES" => "N",
									"DETAIL_URL" => "",
									"DISPLAY_BOTTOM_PAGER" => "N",
									"DISPLAY_DATE" => "Y",
									"DISPLAY_NAME" => "Y",
									"DISPLAY_PICTURE" => "Y",
									"DISPLAY_PREVIEW_TEXT" => "Y",
									"DISPLAY_TOP_PAGER" => "N",
									"FIELD_CODE" => array("",""),
									"FILTER_NAME" => "",
									"HIDE_LINK_WHEN_NO_DETAIL" => "N",
									"IBLOCK_ID" => "15",
									"IBLOCK_TYPE" => "coupons",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"INCLUDE_SUBSECTIONS" => "N",
									"MESSAGE_404" => "",
									"NEWS_COUNT" => "50",
									"PAGER_BASE_LINK_ENABLE" => "N",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL" => "N",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_TEMPLATE" => ".default",
									"PAGER_TITLE" => "Новости",
									"PARENT_SECTION" => "",
									"PARENT_SECTION_CODE" => "",
									"PREVIEW_TRUNCATE_LEN" => "",
									"PROPERTY_CODE" => array("COUPON",""),
									"SET_BROWSER_TITLE" => "N",
									"SET_LAST_MODIFIED" => "N",
									"SET_META_DESCRIPTION" => "N",
									"SET_META_KEYWORDS" => "N",
									"SET_STATUS_404" => "N",
									"SET_TITLE" => "N",
									"SHOW_404" => "N",
									"SORT_BY1" => "RAND",
									"SORT_BY2" => "SORT",
									"SORT_ORDER1" => "ASC",
									"SORT_ORDER2" => "ASC",
									"STRICT_SECTION_CHECK" => "N",
									"IS_SLIDER" => "Y",
								)
							);?>
						</div>
					</div>
				</div>
				
				<div class="col-lg-12">
					<div class="col-lg-6 col-md-6">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Описание</a></li>
							<?if (!empty($arResult["PROPERTIES"]["VIDEO"]["VALUE"])) : ?>
								<li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">Видео</a></li>
							<?endif;?>
							<?if (!empty($arResult["PROPERTIES"]["RECEPT"]["VALUE"])) : ?>
								<li role="presentation"><a href="#recipes" aria-controls="recipes" role="tab" data-toggle="tab">Рецепты</a></li>
							<?endif;?>
						</ul>
					  <div class="tab-content">
						<div role="tabpanel" class="tab-pane" id="description" itemprop="description">
							<?if ('html' == $arResult['DETAIL_TEXT_TYPE']) : ?>
								<?= $arResult['DETAIL_TEXT'];?>
							<?else:?>
								<p><?= $arResult['DETAIL_TEXT'];?></p>
							<?endif;?>
						</div>

						<?if(!empty($arResult["PROPERTIES"]["VIDEO"]["VALUE"])):?>
							<div role="tabpanel" class="tab-pane" id="video" style="height:600px;">
								<iframe src="<?= $arResult["PROPERTIES"]["VIDEO"]["VALUE"]?>" style="width:640px; height:480px; border:0; margin:auto;"></iframe>
							</div>
						<?endif;?>

						<?if (!empty($arResult["PROPERTIES"]["RECEPT"]["VALUE"])) : ?>
							<div role="tabpanel" class="tab-pane" id="recipes">
								<?foreach ($arResult["PROPERTIES"]["RECEPT"]["VALUE"] as $recept) : ?>
									<?
									$res = CIBlockElement::GetByID($recept);
									?>
									<?if ($ar_res = $res->GetNext()) : ?>
										<span class="recept"><a href="/recepty/<?= $ar_res["CODE"];?>/"><?= $ar_res["NAME"];?></a></span><br/>
									<?endif;?>
								<?endforeach;?>
							</div>
						<?endif;?>
					  </div>
					</div>
					<div class="col-lg-6 col-md-6 info-block">
						<? // Рекламный блок №1 ?>
						<div class="detail__advert-block detail__advert-block-1">
							<?
							
							if(!$arResult['PROPERTIES']['ADVERT_BLOCK_1']['VALUE']) {
								
								$arResult['PROPERTIES']['ADVERT_BLOCK_1']['VALUE'] = array(-1);
							}
							
							global ${'arFilterAdvertBlock1' . $arResult['ID']};
							${'arFilterAdvertBlock1' . $arResult['ID']} = array(
							
								'ID' => $arResult['PROPERTIES']['ADVERT_BLOCK_1']['VALUE'],
								'!PROPERTY_OF_DAY' => false,
								'>PROPERTY_OF_DAY_DATE' => date('Y-m-d H:i:s', time()),
							);
							?>
							<div class="detail__advert-block-slider detail__advert-block-1-slider">
								<?$APPLICATION->IncludeComponent(
									"bitrix:catalog.section",
									"catalog-grid",
									Array(
										"ACTION_VARIABLE" => "action",
										"ADD_PICT_PROP" => "-",
										"ADD_PROPERTIES_TO_BASKET" => "N",
										"ADD_SECTIONS_CHAIN" => "N",
										"ADD_TO_BASKET_ACTION" => "ADD",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_ADDITIONAL" => "",
										"AJAX_OPTION_HISTORY" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "N",
										"BACKGROUND_IMAGE" => "-",
										"BASKET_URL" => "/cart/",
										"BROWSER_TITLE" => "-",
										"CACHE_FILTER" => "N",
										"CACHE_GROUPS" => "N",
										"CACHE_TIME" => "36000000",
										"CACHE_TYPE" => "A",
										"COMPATIBLE_MODE" => "Y",
										"CONVERT_CURRENCY" => "N",
										"CUSTOM_FILTER" => "",
										"DETAIL_URL" => "",
										"DISABLE_INIT_JS_IN_COMPONENT" => "N",
										"DISPLAY_BOTTOM_PAGER" => "N",
										"DISPLAY_COMPARE" => "N",
										"DISPLAY_TOP_PAGER" => "N",
										"ELEMENT_SORT_FIELD" => "sort",
										"ELEMENT_SORT_FIELD2" => "id",
										"ELEMENT_SORT_ORDER" => "asc",
										"ELEMENT_SORT_ORDER2" => "desc",
										"ENLARGE_PRODUCT" => "STRICT",
										"FILTER_NAME" => 'arFilterAdvertBlock1' . $arResult['ID'],
										"HIDE_NOT_AVAILABLE" => "Y",
										"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
										"IBLOCK_ID" => "5",
										"IBLOCK_TYPE" => "catalog",
										"INCLUDE_SUBSECTIONS" => "Y",
										"LABEL_PROP" => array(),
										"LAZY_LOAD" => "N",
										"LINE_ELEMENT_COUNT" => "",
										"LOAD_ON_SCROLL" => "N",
										"MESSAGE_404" => "",
										"MESS_BTN_ADD_TO_BASKET" => "В корзину",
										"MESS_BTN_BUY" => "Купить",
										"MESS_BTN_DETAIL" => "Подробнее",
										"MESS_BTN_SUBSCRIBE" => "Подписаться",
										"MESS_NOT_AVAILABLE" => "Нет в наличии",
										"META_DESCRIPTION" => "-",
										"META_KEYWORDS" => "-",
										"OFFERS_CART_PROPERTIES" => array(""),
										"OFFERS_FIELD_CODE" => array("",""),
										"OFFERS_LIMIT" => "0",
										"OFFERS_PROPERTY_CODE" => array("",""),
										"OFFERS_SORT_FIELD" => "sort",
										"OFFERS_SORT_FIELD2" => "id",
										"OFFERS_SORT_ORDER" => "asc",
										"OFFERS_SORT_ORDER2" => "desc",
										"PAGER_BASE_LINK_ENABLE" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_TEMPLATE" => ".default",
										"PAGER_TITLE" => "Товары",
										"PAGE_ELEMENT_COUNT" => "10",
										"PARTIAL_PRODUCT_PROPERTIES" => "N",
										"PRICE_CODE" => array("BASE"),
										"PRICE_VAT_INCLUDE" => "N",
										"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
										"PRODUCT_DISPLAY_MODE" => "N",
										"PRODUCT_ID_VARIABLE" => "id",
										"PRODUCT_PROPERTIES" => array(""),
										"PRODUCT_PROPS_VARIABLE" => "prop",
										"PRODUCT_QUANTITY_VARIABLE" => "quantity",
										"PRODUCT_ROW_VARIANTS" => array("={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false"),
										"PRODUCT_SUBSCRIPTION" => "Y",
										"PROPERTY_CODE" => array("OF_DAY","OF_DAY_DATE","OF_DAY_DISCOUNT",""),
										"PROPERTY_CODE_MOBILE" => array(),
										"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
										"RCM_TYPE" => "personal",
										"SECTION_CODE" => "",
										"SECTION_ID" => "",
										"SECTION_ID_VARIABLE" => "SECTION_ID",
										"SECTION_URL" => "",
										"SECTION_USER_FIELDS" => array("",""),
										"SEF_MODE" => "N",
										"SET_BROWSER_TITLE" => "N",
										"SET_LAST_MODIFIED" => "N",
										"SET_META_DESCRIPTION" => "N",
										"SET_META_KEYWORDS" => "N",
										"SET_STATUS_404" => "N",
										"SET_TITLE" => "N",
										"SHOW_404" => "N",
										"SHOW_ALL_WO_SECTION" => "Y",
										"SHOW_CLOSE_POPUP" => "N",
										"SHOW_DISCOUNT_PERCENT" => "N",
										"SHOW_FROM_SECTION" => "N",
										"SHOW_MAX_QUANTITY" => "N",
										"SHOW_OLD_PRICE" => "N",
										"SHOW_PRICE_COUNT" => "1",
										"SHOW_SLIDER" => "Y",
										"SLIDER_INTERVAL" => "3000",
										"SLIDER_PROGRESS" => "N",
										"TEMPLATE_THEME" => "blue",
										"USE_ENHANCED_ECOMMERCE" => "N",
										"USE_MAIN_ELEMENT_SECTION" => "N",
										"USE_PRICE_COUNT" => "N",
										"USE_PRODUCT_QUANTITY" => "N",
										"RANDOM_ITEMS" => 'Y',
										"HIDE_EMPTY" => 'Y',
									)
								);?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-lg-12">
					<div class="col-lg-6 col-md-6">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Отзывы</a></li>
						</ul>
					  <div class="tab-content">

						<div role="tabpanel" class="tab-pane active" id="reviews">
							<?if ('Y' == $arParams['USE_COMMENTS']) : ?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:catalog.comments",
									"catalog",
									array(
										"ELEMENT_ID" => $arResult['ID'],
										"ELEMENT_CODE" => "",
										"IBLOCK_ID" => $arParams['IBLOCK_ID'],
										"SHOW_DEACTIVATED" => $arParams['SHOW_DEACTIVATED'],
										"URL_TO_COMMENT" => "",
										"WIDTH" => "",
										"COMMENTS_COUNT" => "5",
										"BLOG_USE" => $arParams['BLOG_USE'],
										"FB_USE" => $arParams['FB_USE'],
										"FB_APP_ID" => $arParams['FB_APP_ID'],
										"VK_USE" => $arParams['VK_USE'],
										"VK_API_ID" => $arParams['VK_API_ID'],
										"CACHE_TYPE" => $arParams['CACHE_TYPE'],
										"CACHE_TIME" => $arParams['CACHE_TIME'],
										'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
										"BLOG_TITLE" => "",
										"BLOG_URL" => $arParams['BLOG_URL'],
										"PATH_TO_SMILE" => "",
										"EMAIL_NOTIFY" => $arParams['BLOG_EMAIL_NOTIFY'],
										"AJAX_POST" => "Y",
										"SHOW_SPAM" => "Y",
										"SHOW_RATING" => "N",
										"FB_TITLE" => "",
										"FB_USER_ADMIN_ID" => "",
										"FB_COLORSCHEME" => "light",
										"FB_ORDER_BY" => "reverse_time",
										"VK_TITLE" => "",
										"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME']
									),
									$component,
									array("HIDE_ICONS" => "Y")
								);?>
							<?endif;?>
						</div>
					  </div>
					</div>
					<div class="col-lg-6 col-md-6 info-block">
						<? // Рекламный блок №2 ?>
						<div class="detail__advert-block detail__advert-block-2">
							<?
							
							if(!$arResult['PROPERTIES']['ADVERT_BLOCK_2']['VALUE']) {
								
								$arResult['PROPERTIES']['ADVERT_BLOCK_2']['VALUE'] = array(-1);
							}
							
							global ${'arFilterAdvertBlock2' . $arResult['ID']};
							${'arFilterAdvertBlock2' . $arResult['ID']} = array(
							
								'ID' => $arResult['PROPERTIES']['ADVERT_BLOCK_1']['VALUE'],
								'!PROPERTY_OF_DAY' => false,
								'>PROPERTY_OF_DAY_DATE' => date('Y-m-d H:i:s', time()),
							);
							?>
							<div class="detail__advert-block-slider detail__advert-block-2-slider">
								<?$APPLICATION->IncludeComponent(
									"bitrix:catalog.section",
									"catalog-grid",
									Array(
										"ACTION_VARIABLE" => "action",
										"ADD_PICT_PROP" => "-",
										"ADD_PROPERTIES_TO_BASKET" => "N",
										"ADD_SECTIONS_CHAIN" => "N",
										"ADD_TO_BASKET_ACTION" => "ADD",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_ADDITIONAL" => "",
										"AJAX_OPTION_HISTORY" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "N",
										"BACKGROUND_IMAGE" => "-",
										"BASKET_URL" => "/cart/",
										"BROWSER_TITLE" => "-",
										"CACHE_FILTER" => "N",
										"CACHE_GROUPS" => "N",
										"CACHE_TIME" => "36000000",
										"CACHE_TYPE" => "A",
										"COMPATIBLE_MODE" => "Y",
										"CONVERT_CURRENCY" => "N",
										"CUSTOM_FILTER" => "",
										"DETAIL_URL" => "",
										"DISABLE_INIT_JS_IN_COMPONENT" => "N",
										"DISPLAY_BOTTOM_PAGER" => "N",
										"DISPLAY_COMPARE" => "N",
										"DISPLAY_TOP_PAGER" => "N",
										"ELEMENT_SORT_FIELD" => "sort",
										"ELEMENT_SORT_FIELD2" => "id",
										"ELEMENT_SORT_ORDER" => "asc",
										"ELEMENT_SORT_ORDER2" => "desc",
										"ENLARGE_PRODUCT" => "STRICT",
										"FILTER_NAME" => 'arFilterAdvertBlock2' . $arResult['ID'],
										"HIDE_NOT_AVAILABLE" => "Y",
										"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
										"IBLOCK_ID" => "5",
										"IBLOCK_TYPE" => "catalog",
										"INCLUDE_SUBSECTIONS" => "Y",
										"LABEL_PROP" => array(),
										"LAZY_LOAD" => "N",
										"LINE_ELEMENT_COUNT" => "",
										"LOAD_ON_SCROLL" => "N",
										"MESSAGE_404" => "",
										"MESS_BTN_ADD_TO_BASKET" => "В корзину",
										"MESS_BTN_BUY" => "Купить",
										"MESS_BTN_DETAIL" => "Подробнее",
										"MESS_BTN_SUBSCRIBE" => "Подписаться",
										"MESS_NOT_AVAILABLE" => "Нет в наличии",
										"META_DESCRIPTION" => "-",
										"META_KEYWORDS" => "-",
										"OFFERS_CART_PROPERTIES" => array(""),
										"OFFERS_FIELD_CODE" => array("",""),
										"OFFERS_LIMIT" => "0",
										"OFFERS_PROPERTY_CODE" => array("",""),
										"OFFERS_SORT_FIELD" => "sort",
										"OFFERS_SORT_FIELD2" => "id",
										"OFFERS_SORT_ORDER" => "asc",
										"OFFERS_SORT_ORDER2" => "desc",
										"PAGER_BASE_LINK_ENABLE" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_TEMPLATE" => ".default",
										"PAGER_TITLE" => "Товары",
										"PAGE_ELEMENT_COUNT" => "10",
										"PARTIAL_PRODUCT_PROPERTIES" => "N",
										"PRICE_CODE" => array("BASE"),
										"PRICE_VAT_INCLUDE" => "N",
										"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
										"PRODUCT_DISPLAY_MODE" => "N",
										"PRODUCT_ID_VARIABLE" => "id",
										"PRODUCT_PROPERTIES" => array(""),
										"PRODUCT_PROPS_VARIABLE" => "prop",
										"PRODUCT_QUANTITY_VARIABLE" => "quantity",
										"PRODUCT_ROW_VARIANTS" => array("={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false"),
										"PRODUCT_SUBSCRIPTION" => "Y",
										"PROPERTY_CODE" => array("OF_DAY","OF_DAY_DATE","OF_DAY_DISCOUNT",""),
										"PROPERTY_CODE_MOBILE" => array(),
										"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
										"RCM_TYPE" => "personal",
										"SECTION_CODE" => "",
										"SECTION_ID" => "",
										"SECTION_ID_VARIABLE" => "SECTION_ID",
										"SECTION_URL" => "",
										"SECTION_USER_FIELDS" => array("",""),
										"SEF_MODE" => "N",
										"SET_BROWSER_TITLE" => "N",
										"SET_LAST_MODIFIED" => "N",
										"SET_META_DESCRIPTION" => "N",
										"SET_META_KEYWORDS" => "N",
										"SET_STATUS_404" => "N",
										"SET_TITLE" => "N",
										"SHOW_404" => "N",
										"SHOW_ALL_WO_SECTION" => "Y",
										"SHOW_CLOSE_POPUP" => "N",
										"SHOW_DISCOUNT_PERCENT" => "N",
										"SHOW_FROM_SECTION" => "N",
										"SHOW_MAX_QUANTITY" => "N",
										"SHOW_OLD_PRICE" => "N",
										"SHOW_PRICE_COUNT" => "1",
										"SHOW_SLIDER" => "Y",
										"SLIDER_INTERVAL" => "3000",
										"SLIDER_PROGRESS" => "N",
										"TEMPLATE_THEME" => "blue",
										"USE_ENHANCED_ECOMMERCE" => "N",
										"USE_MAIN_ELEMENT_SECTION" => "N",
										"USE_PRICE_COUNT" => "N",
										"USE_PRODUCT_QUANTITY" => "N",
										"RANDOM_ITEMS" => 'Y',
										"HIDE_EMPTY" => 'Y',
									)
								);?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	</div>
</div>
<div class="container">
		<div class="bx_md">

<div class="item_info_section">

<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	if ($arResult['OFFER_GROUP'])
	{
		foreach ($arResult['OFFER_GROUP_VALUES'] as $offerID)
		{
?>
	<span id="<? echo $arItemIDs['OFFER_GROUP'].$offerID; ?>" style="display: none;">
<?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arResult["OFFERS_IBLOCK"],
		"ELEMENT_ID" => $offerID,
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
?>
	</span>
<?
		}
	}
}
else
{
	if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP'])
	{
?><?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_ID" => $arResult["ID"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
	}
}

if ($arResult['CATALOG'] && $arParams['USE_GIFTS_DETAIL'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled("sale")) {
	$APPLICATION->IncludeComponent("bitrix:sale.gift.product", ".default", array(
			'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
			'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'SUBSCRIBE_URL_TEMPLATE' => $arResult['~SUBSCRIBE_URL_TEMPLATE'],
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],

			"SHOW_DISCOUNT_PERCENT" => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
			"SHOW_OLD_PRICE" => $arParams['GIFTS_SHOW_OLD_PRICE'],
			"PAGE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
			"LINE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
			"HIDE_BLOCK_TITLE" => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
			"BLOCK_TITLE" => $arParams['GIFTS_DETAIL_BLOCK_TITLE'],
			"TEXT_LABEL_GIFT" => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
			"SHOW_NAME" => $arParams['GIFTS_SHOW_NAME'],
			"SHOW_IMAGE" => $arParams['GIFTS_SHOW_IMAGE'],
			"MESS_BTN_BUY" => $arParams['GIFTS_MESS_BTN_BUY'],

			"SHOW_PRODUCTS_{$arParams['IBLOCK_ID']}" => "Y",
			"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
			"PRODUCT_SUBSCRIPTION" => $arParams["PRODUCT_SUBSCRIPTION"],
			"MESS_BTN_DETAIL" => $arParams["MESS_BTN_DETAIL"],
			"MESS_BTN_SUBSCRIBE" => $arParams["MESS_BTN_SUBSCRIBE"],
			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
			"USE_PRODUCT_QUANTITY" => 'N',
			"OFFER_TREE_PROPS_{$arResult['OFFERS_IBLOCK']}" => $arParams['OFFER_TREE_PROPS'],
			"CART_PROPERTIES_{$arResult['OFFERS_IBLOCK']}" => $arParams['OFFERS_CART_PROPERTIES'],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"POTENTIAL_PRODUCT_TO_BUY" => array(
				'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
				'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
				'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
				'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
				'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,

				'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][0]['ID']) ? $arResult['OFFERS'][0]['ID'] : null,
				'SECTION' => array(
					'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
					'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
					'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
					'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
				),
			)
		), $component, array("HIDE_ICONS" => "Y"));
}
if ($arResult['CATALOG'] && $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled("sale"))
{
	$APPLICATION->IncludeComponent(
			"bitrix:sale.gift.main.products",
			".default",
			array(
				"PAGE_ELEMENT_COUNT" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
				"BLOCK_TITLE" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],

				"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
				"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],

				"AJAX_MODE" => $arParams["AJAX_MODE"],
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],

				"ELEMENT_SORT_FIELD" => 'ID',
				"ELEMENT_SORT_ORDER" => 'DESC',
				//"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
				//"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
				"FILTER_NAME" => 'searchFilter',
				"SECTION_URL" => $arParams["SECTION_URL"],
				"DETAIL_URL" => $arParams["DETAIL_URL"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],

				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],

				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"SET_TITLE" => $arParams["SET_TITLE"],
				"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
				"CURRENCY_ID" => $arParams["CURRENCY_ID"],
				"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
				"TEMPLATE_THEME" => (isset($arParams["TEMPLATE_THEME"]) ? $arParams["TEMPLATE_THEME"] : ""),

				"ADD_PICT_PROP" => (isset($arParams["ADD_PICT_PROP"]) ? $arParams["ADD_PICT_PROP"] : ""),

				"LABEL_PROP" => (isset($arParams["LABEL_PROP"]) ? $arParams["LABEL_PROP"] : ""),
				"OFFER_ADD_PICT_PROP" => (isset($arParams["OFFER_ADD_PICT_PROP"]) ? $arParams["OFFER_ADD_PICT_PROP"] : ""),
				"OFFER_TREE_PROPS" => (isset($arParams["OFFER_TREE_PROPS"]) ? $arParams["OFFER_TREE_PROPS"] : ""),
				"SHOW_DISCOUNT_PERCENT" => (isset($arParams["SHOW_DISCOUNT_PERCENT"]) ? $arParams["SHOW_DISCOUNT_PERCENT"] : ""),
				"SHOW_OLD_PRICE" => (isset($arParams["SHOW_OLD_PRICE"]) ? $arParams["SHOW_OLD_PRICE"] : ""),
				"MESS_BTN_BUY" => (isset($arParams["MESS_BTN_BUY"]) ? $arParams["MESS_BTN_BUY"] : ""),
				"MESS_BTN_ADD_TO_BASKET" => (isset($arParams["MESS_BTN_ADD_TO_BASKET"]) ? $arParams["MESS_BTN_ADD_TO_BASKET"] : ""),
				"MESS_BTN_DETAIL" => (isset($arParams["MESS_BTN_DETAIL"]) ? $arParams["MESS_BTN_DETAIL"] : ""),
				"MESS_NOT_AVAILABLE" => (isset($arParams["MESS_NOT_AVAILABLE"]) ? $arParams["MESS_NOT_AVAILABLE"] : ""),
				'ADD_TO_BASKET_ACTION' => (isset($arParams["ADD_TO_BASKET_ACTION"]) ? $arParams["ADD_TO_BASKET_ACTION"] : ""),
				'SHOW_CLOSE_POPUP' => (isset($arParams["SHOW_CLOSE_POPUP"]) ? $arParams["SHOW_CLOSE_POPUP"] : ""),
				'DISPLAY_COMPARE' => (isset($arParams['DISPLAY_COMPARE']) ? $arParams['DISPLAY_COMPARE'] : ''),
				'COMPARE_PATH' => (isset($arParams['COMPARE_PATH']) ? $arParams['COMPARE_PATH'] : ''),
			)
			+ array(
				'OFFER_ID' => empty($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID']) ? $arResult['ID'] : $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'],
				'SECTION_ID' => $arResult['SECTION']['ID'],
				'ELEMENT_ID' => $arResult['ID'],
			),
			$component,
			array("HIDE_ICONS" => "Y")
	);
}
?>
</div>
		</div>
</div>
	<div class="clb"></div>
</div><?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	foreach ($arResult['JS_OFFERS'] as &$arOneJS)
	{
		if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE'])
		{
			$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
			$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
		}
		$strProps = '';
		if ($arResult['SHOW_OFFERS_PROPS'])
		{
			if (!empty($arOneJS['DISPLAY_PROPERTIES']))
			{
				foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp)
				{
					$strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
						is_array($arOneProp['VALUE'])
						? implode(' / ', $arOneProp['VALUE'])
						: $arOneProp['VALUE']
					).'</dd>';
				}
			}
		}
		$arOneJS['DISPLAY_PROPERTIES'] = $strProps;
	}
	if (isset($arOneJS))
		unset($arOneJS);
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
			'USE_STICKERS' => true,
			'USE_SUBSCRIBE' => $showSubscribeBtn,
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'NAME' => $arResult['~NAME']
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $arSkuProps,
		'BASKET_ITEMS' => $arBasketItems
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
}
else
{
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
	{
?>
<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
<?
		if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
		{
			foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
			{
?>
	<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
<?
				if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
					unset($arResult['PRODUCT_PROPERTIES'][$propID]);
			}
		}
		$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
		if (!$emptyProductProperties)
		{
?>
	<table>
<?
			foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo)
			{
?>
	<tr><td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
	<td>
<?
				if(
					'L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE']
					&& 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']
				)
				{
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br><?
					}
				}
				else
				{
					?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
					}
					?></select><?
				}
?>
	</td></tr>
<?
			}
?>
	</table>
<?
		}
?>
</div>
<?
	}
	if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE'])
	{
		$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
		$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
	}
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
			'USE_STICKERS' => true,
			'USE_SUBSCRIBE' => $showSubscribeBtn,
		),
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'PICT' => $arFirstPhoto,
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'PRICE' => $arResult['MIN_PRICE'],
			'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
		),
		'BASKET' => array(
			'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'BASKET_ITEMS' => $arBasketItems
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	unset($emptyProductProperties);
}
?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);

BX.message({
	ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
	BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	PRODUCT_GIFT_LABEL: '<? echo GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});


</script>




<?
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}
if (isset($templateData['JS_OBJ']))
{
?><script type="text/javascript">
BX.ready(BX.defer(function(){
	if (!!window.<? echo $templateData['JS_OBJ']; ?>)
	{
		window.<? echo $templateData['JS_OBJ']; ?>.allowViewedCount(true);
	}
}));
</script><?
}
?>

<script>
	var updateQueryStringParam = function (key, value) {

	    var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
	        urlQueryString = document.location.search,
	        newParam = key + '=' + value,
	        params = '?' + newParam;

	    if (urlQueryString) {
	        var updateRegex = new RegExp('([\?&])' + key + '[^&]*');
	        var removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

	        if( typeof value == 'undefined' || value == null || value == '' ) {
	            params = urlQueryString.replace(removeRegex, "$1");
	            params = params.replace( /[&;]$/, "" );

	        } else if (urlQueryString.match(updateRegex) !== null) {
	            params = urlQueryString.replace(updateRegex, "$1" + newParam);

	        } else {
	            params = urlQueryString + '&' + newParam;
	        }
	    }

	    params = params == '?' ? '' : params;

	    window.history.replaceState({}, "", baseUrl + params);
	};

	$(document).ready(function() {
		$('ul.size li').on('click', function(e) {
			updateQueryStringParam('offer', $(this).data('switchid'));
		});
	});
</script>

<?
GLOBAL $lastModified;
if (!$lastModified)
   $lastModified = MakeTimeStamp($arResult['TIMESTAMP_X']);
else
   $lastModified = max($lastModified, MakeTimeStamp($arResult['TIMESTAMP_X']));
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arrFavorites = unserialize($APPLICATION->get_cookie("ANONIM_PROD_FAVORITES"));
?>
<style>
	.data-table .card-product__actions-full {
		width: 100% !important;
	}
</style>
<div class="catalog-section list">
	<?
	$arBasketItems = array();
	$arBasketItemsAll = array();

	$dbBasketItems = CSaleBasket::GetList(
			array(
					"NAME" => "ASC",
					"ID" => "ASC"
				),
			array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(),
					"LID" => SITE_ID,
					"ORDER_ID" => "NULL"
				),
			false,
			false,
			array()
		);
	while ($arItems = $dbBasketItems->Fetch())
	{
		if (strlen($arItems["CALLBACK_FUNC"]) > 0)
		{
			CSaleBasket::UpdatePrice($arItems["ID"],
									 $arItems["CALLBACK_FUNC"],
									 $arItems["MODULE"],
									 $arItems["PRODUCT_ID"],
									 $arItems["QUANTITY"]);
			$arItems = CSaleBasket::GetByID($arItems["ID"]);
		}

		$arBasketItemsAll[$arItems['PRODUCT_ID']] = $arItems;

		$arBasketItems[] = $arItems['PRODUCT_ID'];
	}
	foreach($arResult["ITEMS"] as $arItem):?>
	<?
	
	$imgTitle = isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $arItem['NAME'];
		
	$imgAlt = isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']
		: $arItem['NAME'];
	
	$minPrice = $arItem['MIN_PRICE'];
	$itemID = $arItem['ID'];
	$measure = $arItem['ITEM_MEASURE_RATIOS'][$arItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
	
	if($measure) {
		
		$minPrice['DISCOUNT_VALUE'] = $minPrice['DISCOUNT_VALUE'] * $measure;
		$minPrice['VALUE'] = $minPrice['VALUE'] * $measure;
	}

	if(isset($arItem['OFFERS']) && count($arItem['OFFERS']) > 0 && isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']])){
		$offerID = $arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['ID'];
		$itemID = $offerID;
	}

	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
	$strMainID = $this->GetEditAreaId($arItem['ID']);
	// $img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]['ID'], array("width" => 250, "height" => 127), BX_RESIZE_IMAGE_PROPORTIONAL);
	// $res = CCatalogSKU::getOffersList(array($arItem['ID']), 0, array(), array(), array());
 //        $offers = "";
 //        foreach ($res as $key) {
 //        	foreach ($key as $value) {
 //        		$offers = $value['ID'];
 //        		break;
 //        	}
 //        }
 //    if(!empty($offers) && in_array($offers, $arBasketItems)) $arBasketItems[] = $arItem['ID'];
	?>

	<div id="product<?=$itemID?>" class="row card-product">
		<div class="card-product-wrap" id="<?=$strMainID?>">
		<div class="col-sm-2 bx-col images">
			<div class="bx-col-wrapper">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
					<?
					$photoResize = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array("width" => 130, "height" => 130), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
					?>
					<img src="<?=$photoResize["src"]?>" alt="<?=$arItem['NAME']?>" alt="<?=$imgAlt;?>" title="<?=$imgTitle;?>">
				</a>
			</div>
		</div>

		<div class="col-sm-8 bx-col">
			<div class="row bx-col-wrapper">
				<div class="col-md-8 bx-col">
					<div class="bx-col-wrapper">
						<div class="card-product_title">
							<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
								<?=$arItem["NAME"]?>
							<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
						</div>

						<?if(!empty($arItem['PROPERTY_ARTNUMBER_VALUE'])):?>
							<p>Артикул <?=$arItem['PROPERTY_ARTNUMBER_VALUE']?></p>
						<?endif;?>
					</div>
				</div>
				<?/*?>
				<div class="col-md-4 bx-col">
					<div class="bx-col-wrapper">
						<div class="card-product_description"><?=$arItem["NAME"]?></div>
					</div>
				</div>
				<?*/?>
				<div class="col-md-4 bx-col">
					<div class="bx-col-wrapper">

						<?if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {?>

						<s><?=priceFormat($minPrice['VALUE']); ?></s><br /><span class="card-product_price"><?=priceFormat($minPrice['DISCOUNT_VALUE']);?> <span class="fa fa-ruble-sign"></span></span>

						<?}
						else {?>
							<span class="card-product_price"><?=priceFormat($minPrice['DISCOUNT_VALUE']);?> <i class="fa fa-ruble-sign"></i></span> <?=(!empty($arItem['PROPERTIES']['SIZE']['VALUE']))?'<sup>/'.$arItem['PROPERTIES']['SIZE']['VALUE'].'</sup>':'';?>
						<?}?>



<?/*

						                <div class="card-product__price-discount">



						                </div>
						                <div class="card-product__price">
						                    <div class="card-product__price-val"><?=priceFormat($minPrice['DISCOUNT_VALUE']);?></div>
						                    <div class="card-product__price-currency"><span class="fa fa-ruble-sign"></span></div>
						                </div>



						<?foreach($arResult["PRICES"] as $code=>$arPrice):?>
							<?if($arPrice = $arItem["PRICES"][$code]):?>
								<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
									<s><?=$arPrice["PRINT_VALUE"]?></s><br /><span class="card-product_price"><?=priceFormat($arPrice["PRINT_DISCOUNT_VALUE"]);?> <span class="fa fa-ruble-sign"></span></span>
								<?else:?>
									<span class="card-product_price"><?=priceFormat($arPrice["PRINT_VALUE"])?> <i class="fa fa-ruble-sign"></i></span> <?=(!empty($arItem['PROPERTIES']['SIZE']['VALUE']))?'<sup>/'.$arItem['PROPERTIES']['SIZE']['VALUE'].'</sup>':'';?>
								<?endif?>
							<?else:?>
								&nbsp;
							<?endif;?>
						<?endforeach;?>
*/?>
						<span class="card-product_weight">/
							<?
							/*if(!empty($arItem['PROPERTIES']['liquid']['VALUE'])){
								echo $arItem['PROPERTIES']['liquid']['VALUE'];
							}else{
								if(!empty($offerID)){
									print getFormatedWeight($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['CATALOG_WEIGHT']);
								}
								else{
									print getFormatedWeight($arItem['CATALOG_WEIGHT']);;
								}
							}*/
							echo $arItem['ITEM_MEASURE_RATIOS'][$arItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']." ".$arItem['ITEM_MEASURE']['TITLE'];
							?>
						</span>
					</div>

				</div>

			</div>
		</div>

		<div class="col-sm-2 bx-col">
			<div class="bx-col-wrapper">
				<?if(count($arResult["PRICES"]) > 0):?>
					<div class="card-product__actions-full" style="width: 100%; text-align: left;">
					<?if(in_array($USER->GetId(),$arItem["PROPERTIES"]["F_USER"]["VALUE"]) || in_array($arItem['ID'], $arrFavorites)){
						$check = " active";
					}
					else{
						$check = "";
					};?>

					<div class="card-product__favorite">
						<a href="#" class="catalog-item-fav btn-fav favorite_check <?=$check?>" data-id="<?=$arItem['ID']?>"></a>
					</div>
					<?if(!in_array($itemID, $arBasketItems)) :?>
		                <div class="card-product__buy">
		                  <a href="javascript:void(0)" data-id="<?=$itemID?>" data-quantity="<?=$arItem["CATALOG_MEASURE_RATIO"]?>" class="btn-buy"></a>
		                </div>
		            <?endif;?>
		            </div>

					<form action="<?=POST_FORM_ACTION_URI?>" onsubmit="return false;" method="post" enctype="multipart/form-data" class="add_form<?if(in_array($itemID, $arBasketItems)) echo " active";?>">
						<div class="add_form_counter-line ajax-count">
							<a class="MinusList" href="javascript:void(0)">-</a>
							<input type="hidden" name="quantity_ratio" value="<?=$arItem["CATALOG_MEASURE_RATIO"]?>" />
							<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" data-id="<?=$itemID?>" value="<?=(!empty($arBasketItemsAll)&&isset($arBasketItemsAll[$itemID]))?$arBasketItemsAll[$itemID]['QUANTITY']:$arItem["CATALOG_MEASURE_RATIO"]?>" id="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?><?=$itemID?>">
							<a class="PlusList" href="javascript:void(0)">+</a>
						</div>
						<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
						<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=$itemID?>">
						<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="Купить" style="display:none;">
						<input class="add_form_btn" type="submit" id="link2card<?=$itemID?>" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="В корзине" onClick="window.location.href = '/cart/'" />
					</form>
				<?endif;?>
			</div>
		</div>
		</div>
	</div>
	<?endforeach;?>

<?$this->SetViewTarget('pagination_section');?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<div class="pagination-section"><?=$arResult["NAV_STRING"]?></div>
<?endif?>
<?$this->EndViewTarget();?>
</div>
<div class="container-fluid pagination-bottom">
	<div class="pagination-section"><?=$arResult["NAV_STRING"]?></div>
</div>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;

if (isset($arResult['arResult'])) {
   $arResult =& $arResult['arResult'];
   global $MESS;
   include_once(GetLangFileName(dirname(__FILE__).'/lang/', '/template.php'));
} else {
   return;
}

?>

<div class="products-items section-all">
<?
$arrFavorites = unserialize($APPLICATION->get_cookie("ANONIM_PROD_FAVORITES"));
$arBasketItems = array();
$arBasketItemsAll = array();

$dbBasketItems = CSaleBasket::GetList(
	array(
			"NAME" => "ASC",
			"ID" => "ASC"
		),
	array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
	false,
	false,
	array()
);
	
while ($arItems = $dbBasketItems->Fetch()) {
	
	if (strlen($arItems["CALLBACK_FUNC"]) > 0) {
		
		CSaleBasket::UpdatePrice($arItems["ID"],
								 $arItems["CALLBACK_FUNC"],
								 $arItems["MODULE"],
								 $arItems["PRODUCT_ID"],
								 $arItems["QUANTITY"]);
		$arItems = CSaleBasket::GetByID($arItems["ID"]);
	}

	$arBasketItemsAll[$arItems['PRODUCT_ID']] = $arItems;
	$arBasketItems[] = $arItems['PRODUCT_ID'];
}

if (count($arResult['ITEMS']) > 0) {
	
	foreach ($arResult['ITEMS'] as $arItem) {

		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$APPLICATION->IncludeComponent(
			'bitrix:catalog.item',
			'grid',
			array(
				'RESULT' => array(
					'ITEM' => $arItem,
					'AREA_ID' => $strMainID,
					'BASKET_ITEMS' => $arBasketItems,
					'BASKET_ITEMS_ALL' => $arBasketItemsAll,
					'FAVORITES_ITEMS' => $arrFavorites,
				),
				'PARAMS' => $arParams
					+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$arItem['IBLOCK_ID']])
			),
			$component,
			array('HIDE_ICONS' => 'Y')
		);
	}
	
} else {
	?>
	<div class="cat-block-menu">Товары не найдены</div>
<? } ?>
</div>
<div style="clear: both;"></div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<div class="container-fluid pagination-bottom">
	<div class="pagination-section"><?=$arResult["NAV_STRING"]?></div>
</div>
<?endif?>

<?if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}

if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}
?>

<?

if($arResult['TIME_UPDATES_MAX']) {
	GLOBAL $lastModified;
	if (!$lastModified)
	   $lastModified = $arResult['TIME_UPDATES_MAX'];
	else
	   $lastModified = max($lastModified, $arResult['TIME_UPDATES_MAX']);
}

?>
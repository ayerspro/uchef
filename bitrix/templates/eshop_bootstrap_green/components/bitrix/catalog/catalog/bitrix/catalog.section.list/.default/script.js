$(document).ready(function() {
	$('.mobile-visible.list ul').slick({
		dots: true,
		infinite: false,
		speed: 300,
		arrow: false,
	  	slidesToShow: 1,
	  	slidesToScroll: 1,
	  	responsive: [
    		{
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          },
          {
            breakpoint: 550,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            }
          },
  		]
	});
});
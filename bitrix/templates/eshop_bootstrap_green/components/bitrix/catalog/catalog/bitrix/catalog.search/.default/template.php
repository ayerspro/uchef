<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arElements = $APPLICATION->IncludeComponent(
	"bitrix:search.page",
	"catalog_search",
	Array(
		"RESTART" => $arParams["RESTART"],
		"NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
		"USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"arrFILTER" => array( 0 => "no"),
		"USE_TITLE_RANK" => "N",
		"DEFAULT_SORT" => "rank",
		"FILTER_NAME" => "",
		"SHOW_WHERE" => "N",
		"arrFILTER" => array("iblock_" . $arParams["IBLOCK_TYPE"]),
		"arrFILTER_iblock_" . $arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
		"SHOW_WHEN" => "N",
		"PAGE_RESULT_COUNT" => 50,
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
	),
	$component,
	array('HIDE_ICONS' => 'N')
);

$arElements = getProductsCurrentRegion(array('IBLOCK_ID' => $arParams["IBLOCK_ID"], "ID" => $arElements));

?>
<div class="row">
	<div class="col-lg-2 col-md-4 col-sm-4 cat-menu">
		<div class="md-filter-bg"></div>
		<div class="wrap-menu">
			<div class="md-filter-controll js-md-filter-controll"></div>
			<div id="vertical-multilevel-menu">
			<?
			if (!empty($arElements) && is_array($arElements)) {
				
				$sectionID = false;
				$context = \Bitrix\Main\Context::getCurrent();
				$arRequest = $context->getRequest()->toArray();
				$curentUrl = $context->getRequest()->getRequestUri();
				$uri = new \Bitrix\Main\Web\Uri($curentUrl);
				
				$arSections = array();
				$arSectionsCount = array();
			
				$arSelectEl = array("ID", "NAME", "IBLOCK_SECTION_ID");
				$arFilterEl = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arElements);
				$res = CIBlockElement::GetList(array(), $arFilterEl, false, false, $arSelectEl);
				
				$arLinks = array();
				
				while($ob = $res->GetNextElement()) {
					
					$arFields = $ob->GetFields();
					$arSections[$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
					$arLinks[$arFields['IBLOCK_SECTION_ID']] = array();
					
					if(!$arSectionsCount[$arFields['IBLOCK_SECTION_ID']]) {
						
						$arSectionsCount[$arFields['IBLOCK_SECTION_ID']] = 1;
						
					} else {
						
						$arSectionsCount[$arFields['IBLOCK_SECTION_ID']]++;
					}
				}
				
				if($arSections) {
					
					$arFilterSec = array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ID' => $arSections);
					$rsSections = CIBlockSection::GetList(array('SORT' => 'ASC'), $arFilterSec);
					
					while($arSection = $rsSections->Fetch()) {

						$uriClone = $uri;
						$uriClone->addParams(array('SECTION' => $arSection['ID']));
						$urlWithSection = $uriClone->getUri();
						
						$arLinks[$arSection['ID']] = array(
						
							'NAME' => $arSection['NAME'],
							'LINK' => $urlWithSection,
							'COUNT' => $arSectionsCount[$arSection['ID']],
						);
						
						/**/
						$countSum = $arSectionsCount[$arSection['ID']];
						$parentSection = $arSection['IBLOCK_SECTION_ID'];
						
						while($parentSection) {
							
							$resParent = CIBlockSection::GetByID($parentSection);
							if($arResParent = $resParent->GetNext()) {
								
								$parentSection = $arResParent['IBLOCK_SECTION_ID'];
								
								$uriClone = $uri;
								$uriClone->addParams(array('SECTION' => $arResParent['ID']));
								$urlWithSection = $uriClone->getUri();
								
								if(!$arLinks[$arResParent['ID']]) {
									
									$arLinks[$arResParent['ID']] = array(
									
										'NAME' => $arResParent['NAME'],
										'LINK' => $urlWithSection,
										'COUNT' => $countSum,
									);
									
								} else {
									
									$arLinks[$arResParent['ID']]['COUNT'] += $countSum;
								}
							}
						}
						
						
						/**/
					}
				}
				
				//print_r($arLinks);
				
				if($arLinks) { 
				
					if($arRequest['SECTION']) {
						
						$sectionID = $arRequest['SECTION'];
						
						$uri->deleteParams(array("SECTION"));
						$allCategories = $uri->getUri();
						
						?>
						<a href="<?=$allCategories;?>" class="btn btn-lg btn-default btn-block" style="margin-bottom: 20px;">
							<?=GetMessage("ALL_CATEGORIES_LINK_TITLE");?>
						</a>
						<?
						
						$arParrentSection = getParrentSection($sectionID);
						if($arParrentSection) {
							$link = $arLinks[$arParrentSection['ID']];
							?>
							<a href="<?=$link['LINK'];?>" class="btn btn-lg btn-default btn-block" style="margin-bottom: 20px;">
								<?=$link['NAME'];?> (<?=$link['COUNT'];?>)
							</a>
							<?
						}
					}
					
					$sectionMenuID = $sectionID;
					
					$arFilterSec = array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'SECTION_ID' => $sectionID);
					$rsSections = CIBlockSection::GetList(array('SORT' => 'ASC'), $arFilterSec);
					
					if(!$arSection = $rsSections->Fetch()) {
						
						$arParrentSection = getParrentSection($sectionID);
						$sectionMenuID = $arParrentSection['ID'];
					}
					
					?>

					<div class="list-group">
						<?$APPLICATION->IncludeComponent(
							"bitrix:catalog.section.list", 
							"search-left-categories", 
							array(
								"ADD_SECTIONS_CHAIN" => "N",
								"CACHE_GROUPS" => "N",
								"CACHE_TIME" => "36000000",
								"CACHE_TYPE" => "A",
								"COUNT_ELEMENTS" => "Y",
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"SECTION_CODE" => "",
								"SECTION_FIELDS" => array(
									0 => "",
									1 => "",
								),
								"SECTION_ID" => $sectionMenuID,
								"SECTION_URL" => "",
								"SECTION_USER_FIELDS" => array(
									0 => "",
									1 => "",
								),
								"SHOW_PARENT_NAME" => "N",
								"TOP_DEPTH" => "1",
								"VIEW_MODE" => "LINE",
								"COMPONENT_TEMPLATE" => "search-left-categories",
								'AR_FORCE_RESULT' => $arLinks,
								'CURRENT_SECTION_ID' => $sectionID,
							),
							false
						);?>
					</div>
				<? }
			}
			?>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-8 col-lg-10 is-catalog">
		<div class="md-filter-show-wrap">
			<div class="js-md-filter-controll md-filter-show-btn">Подразделы</div>
			<div style="display:none;"><? print_r($arElements); ?></div>
		</div>
		<?
		if (!empty($arElements) && is_array($arElements))
		{
			$GLOBALS['searchFilter'] = array("ID" => $arElements);
				
			?>
			<div class="warp_list_search">
				<div class="container-fluid">
					<div class="flexible pull-left sort-line" style="margin-top:22px;"></div>
					<div class="panel-pagination pull-right">
						<?$APPLICATION->ShowViewContent('pagination_section');?>
					</div>
				</div>
				<?$APPLICATION->IncludeComponent(
					"bitrix:catalog.section",
					"grid",
					array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => 5,
						"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
						"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
						"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
						"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
						"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
						"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
						"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
						"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
						"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
						"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
						"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
						"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
						"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
						"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
						"OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
						"SECTION_URL" => $arParams["SECTION_URL"],
						"DETAIL_URL" => $arParams["DETAIL_URL"],
						"BASKET_URL" => $arParams["BASKET_URL"],
						"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
						"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
						"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
						"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
						"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
						"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
						"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
						"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
						"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
						"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
						"CURRENCY_ID" => $arParams["CURRENCY_ID"],
						"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
						"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
						"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
						"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
						"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
						"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
						"FILTER_NAME" => "searchFilter",
						"SECTION_ID" => $sectionID,
						"SECTION_CODE" => "",
						"SECTION_USER_FIELDS" => array(),
						"INCLUDE_SUBSECTIONS" => "Y",
						"SHOW_ALL_WO_SECTION" => "Y",
						"META_KEYWORDS" => "",
						"META_DESCRIPTION" => "",
						"BROWSER_TITLE" => "",
						"ADD_SECTIONS_CHAIN" => "N",
						"SET_TITLE" => "N",
						"SET_STATUS_404" => "N",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",

						'LABEL_PROP' => $arParams['LABEL_PROP'],
						'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
						'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

						'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
						'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
						'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
						'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
						'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
						'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
						'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
						'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
						'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
						'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

						'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
						'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
						'SHOW_CLOSE_POPUP' => (isset($arParams['SHOW_CLOSE_POPUP']) ? $arParams['SHOW_CLOSE_POPUP'] : ''),
						'COMPARE_PATH' => $arParams['COMPARE_PATH'],
						'SEARCH_PAGE' => 'Y',
					),
					$arResult["THEME_COMPONENT"],
					array('HIDE_ICONS' => 'Y')
				);
			?>
			</div>
			<?
		}
		elseif (is_array($arElements))
		{
			echo GetMessage("CT_BCSE_NOT_FOUND");
		}
		?>
		
	</div>
</div>
<?

?>
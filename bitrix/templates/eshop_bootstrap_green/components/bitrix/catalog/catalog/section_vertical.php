<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader,
	Bitrix\Main\ModuleManager;

global $uchefSettings;

if(isset($_REQUEST['SECTION'])){
	$arr_child = array();
	$results = $DB->Query("SELECT * FROM `b_iblock_section` WHERE IBLOCK_SECTION_ID=".$_REQUEST['SECTION']);
	while ($row = $results->Fetch())
	{
		$arr_child[] = $row;
	}
}
?>
<style>
	.section-main_about .container {
		margin: 0 !important;
		width: auto !important;
		padding: 0 !important;
	}
</style>
<div class="col-xs-12">
	<div class="row">
		<div class="col-xs-12">
	<?
	if($arParams["USE_COMPARE"]=="Y")
	{
		?><?$APPLICATION->IncludeComponent(
			"bitrix:catalog.compare.list",
			"",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"NAME" => $arParams["COMPARE_NAME"],
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
				"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
				"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_ccl",
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
				'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?><?
	}

	if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y') {
		$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
	}
	else {
		$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
	}

	$intSectionID = 0;
	if (isset($_GET["filter"])) {
		$GLOBALS['arrPropFilter'] = array("PROPERTY_STIKERS"=>$_GET["filter"]);
	}


$current_sort_type = 'default';
if(isset($_REQUEST["sort"])){
	$current_sort_type = htmlspecialchars($_REQUEST["sort"]);
	$APPLICATION->set_cookie("sort", $current_sort_type);
}
else{
	$current_sort_type = $APPLICATION->get_cookie("sort") ? $APPLICATION->get_cookie("sort") : "default";
}

$current_sort_order = 'vozvr';
if(isset($_REQUEST["order"])){
	$current_sort_order = htmlspecialchars($_REQUEST["order"]);
	$APPLICATION->set_cookie("order", $current_sort_order);
}
else{
	$current_sort_order = $APPLICATION->get_cookie("order") ? $APPLICATION->get_cookie("order") : "vozvr";
}

if ($current_sort_order == "ubiv"){
	$arParams["ELEMENT_SORT_ORDER"]= "desc";
}
else if ($current_sort_order == "vozvr") {
	$arParams["ELEMENT_SORT_ORDER"]= "asc";
}

if($current_sort_type == "price"){
	$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_MINIMUM_PRICE";
}
else if($current_sort_type == 'date'){
	$arParams["ELEMENT_SORT_FIELD"] = 'ID';
	$arParams["ELEMENT_SORT_ORDER"]= "desc";
}
else if($current_sort_type == "name"){
	$arParams["ELEMENT_SORT_FIELD"] = "NAME";
}else if($current_sort_type == "default"){
	$arParams["ELEMENT_SORT_FIELD"] = "SORT";
	$arParams["ELEMENT_SORT_ORDER"]= "asc";
}

$view = 'grid';
if(isset($_REQUEST["view"])){
	$view = htmlspecialchars($_REQUEST["view"]);
	$APPLICATION->set_cookie("view", $view);
}
else{
	$view = $APPLICATION->get_cookie("view") && $APPLICATION->get_cookie("view") != '.default' ? $APPLICATION->get_cookie("view") : "grid";
}

$sectionName = "";
$sect = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array('CODE' => $arResult['VARIABLES']['SECTION_CODE'], 'IBLOCK_ID' => $arParams["IBLOCK_ID"]), false, Array('UF_REGION', 'ID', 'NAME', 'DESCRIPTION'), false)->GetNext();

//print_r($arResult['VARIABLES']['SECTION_CODE']); die();

if(!showSectionByRegion($sect['ID']) && $arResult['VARIABLES']['SECTION_CODE']) {
	
	global $APPLICATION;
	$APPLICATION->SetTitle('Страница не найдена');
	$APPLICATION->SetPageProperty('title', 'Страница не найдена');
	
	\Bitrix\Iblock\Component\Tools::process404(
		trim($arParams["MESSAGE_404"]) ?: GetMessage("CATALOG_ELEMENT_NOT_FOUND")
		,true
		,$arParams["SET_STATUS_404"] === "Y"
		,$arParams["SHOW_404"] === "Y"
		,$arParams["FILE_404"]
	);
}

$sectionName = $sect['NAME'];
?>
</div>
<div class="col-xs-12">
	<div class="row">
		<div class="mobile-visible">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
					"START_FROM" => "0",
					"PATH" => "",
					"SITE_ID" => "-"
				),
				false,
				Array('HIDE_ICONS' => 'Y')
			);?>
		</div>
		<div class="col-lg-2 col-md-4 col-sm-4 cat-menu">
			<div class="md-filter-bg"></div>
			<div class="wrap-menu">
			<div class="md-filter-controll js-md-filter-controll"></div>
			<div id="vertical-multilevel-menu">
			
				<?
				if(isset($_REQUEST['CATEGORY'])){
					if(!is_array($GLOBALS[$arParams["FILTER_NAME"]])){
						$GLOBALS[$arParams["FILTER_NAME"]] = array();
					}

					$GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_CATEGORY'] = $_REQUEST['CATEGORY'];
				}

				if(isset($_REQUEST['SALES'])){
					$arDiscounts = getDiscounts($GLOBALS[$arParams["FILTER_NAME"]]);
					
					$arSalesSections = getDiscountsSections($arDiscounts);

					if(count($arDiscounts) == 0){
						$arDiscounts[] = 0;
					}

					if(!is_array($GLOBALS[$arParams["FILTER_NAME"]])){
						$GLOBALS[$arParams["FILTER_NAME"]] = array();
					}

					$GLOBALS[$arParams["FILTER_NAME"]]["=ID"] = $arDiscounts;

					if($_REQUEST['SEC']) {
						
						$arResult["VARIABLES"]["SECTION_ID"] = $_REQUEST['SEC'];
					}
				}
				?>
			

				<?if (defined("IS_IN_SECTIONS") && (isset($_REQUEST['CATEGORY']) || isset($_REQUEST['SALES']))) :?>
					<?$APPLICATION->IncludeComponent("bitrix:menu", "user_vertical_multilevel2",
						array(
							"ROOT_MENU_TYPE" => "left_category",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => "",
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left_category",
							"USE_EXT" => "Y",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"AJAX_MODE" => "Y",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "Y",
							"AJAX_OPTION_JUMP" => "Y",
							"AJAX_OPTION_STYLE" => "Y",
							"COMPONENT_TEMPLATE" => "user_vertical_multilevel2",
							"IS_SALES" => $_REQUEST['SALES'],
							"SALES_SECTIONS" => $arSalesSections,
							"SALES_PAGE" => '/sales/',
						),
						false
					);?>
				<?else:?>
				
					<? //$rootSection = getRootSection($sect["ID"]); ?>
					<? //if(in_array($rootSection['ID'], $uchefSettings['SECTIONS_ID_OLD_STRUCTURE']) || $sect['DEPTH_LEVEL'] == 1) { ?>
				
						<?
						$arParrentSection = getParrentSection($sect["ID"]);
						if($arParrentSection) {
							
							?>
							<a href="<?=$arParrentSection["SECTION_PAGE_URL"]?>" class="btn btn-lg btn-default btn-block">
								<?=$arParrentSection["NAME"]?>
							</a>
							<?
						}
						?>

						<?/*$APPLICATION->IncludeComponent("bitrix:menu", "user_vertical_multilevel",
							array(
								"ROOT_MENU_TYPE" => "left",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_TIME" => "36000000",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => "",
								"MAX_LEVEL" => "4",
								"CHILD_MENU_TYPE" => "left",
								"USE_EXT" => "Y",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"AJAX_OPTION_HISTORY" => "Y",
								"AJAX_OPTION_JUMP" => "Y",
								"AJAX_OPTION_STYLE" => "Y",
								"COMPONENT_TEMPLATE" => "user_vertical_multilevel"
							),
							false
						);*/?>
						
						<?
						
						$sectionMenuID = $sect["ID"];
					
						$arFilterSec = array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'SECTION_ID' => $sect["ID"]);
						$rsSections = CIBlockSection::GetList(array('SORT' => 'ASC'), $arFilterSec);
						
						/*if(!$arSection = $rsSections->Fetch()) {
							
							$arParrentSection = getParrentSection($sect["ID"]);
							$sectionMenuID = $arParrentSection['ID'];
						}*/
						
						?>
						<?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("left-catalog-menu-frame");?>

						<?$APPLICATION->IncludeComponent(
							"bitrix:catalog.section.list",
							"sections-left-menu",
							array(
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"SECTION_ID" => $sectionMenuID,
								"SECTION_CODE" => false,
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
								"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
								"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
								"VIEW_MODE" => "TILE",
								"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
								"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_H1" => "Y",
								"CURRENT_SECTION" => $sect["ID"],
								"REGION" => $_SESSION['SOTBIT_REGIONS']['ID'],
							),
							$component,
							array("HIDE_ICONS" => "Y")
						);?>
						<?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("left-catalog-menu-frame", "");?>
					<?// } ?>
					
				<?endif;?>

			</div>

			<?if (!defined("IS_IN_SECTIONS")) :?>

				<div class="menu-second">
					<div class="list-group">
						<?
						$arDiscounts = getDiscounts(Array("IBLOCK_ID" => 5, "SECTION_ID" => $_REQUEST['SECTION'], "INCLUDE_SUBSECTIONS" => "Y"));

						if(count($arDiscounts) != 0){
							?>
							<a href="<?=$APPLICATION->GetCurPage(false);?>?SALES=Y" class="list-group-item ">Скидки</a>
						<?
						}
						?>

						<?
						$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 12, "ACTIVE" => "Y"), false, false, Array("CODE", "NAME", "ID", "DETAIL_PICTURE"));
						while($ar_fields = $res->GetNext()) {
							$arr_prop[] = $ar_fields;
							
						}
//echo'<pre>';print_r($arr_prop);echo'</pre>';
						if (isset($_REQUEST['SECTION'])) {
							foreach ($arr_prop as $prop_list) {
								$to_list = NULL;
								$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => 5, "ACTIVE" => "Y", "SECTION_ID" => $_REQUEST['SECTION'], "INCLUDE_SUBSECTIONS" => "Y", "PROPERTY_CATEGORY" => (int)$prop_list["ID"]), Array("NAME"));
								while ($ar_fields = $res->GetNext()) {
									if ($ar_fields["CNT"] != NULL) {
										$to_list = $ar_fields["CNT"];
									}
								}

								if ((int)$to_list > 0 && $to_list != NULL) {
									?>
									<a href="<?=$APPLICATION->GetCurPage(false);?>?CATEGORY=<?=$prop_list['ID']?>" class="list-group-item "><?=$prop_list['NAME']?></a>
									<?
								}
							}
						}
						else {
							foreach ($arr_prop as $prop_list) {
								?>
								<a href="<?=$APPLICATION->GetCurPage(false);?>?CATEGORY=<?=$prop_list['ID']?>" class="list-group-item "><?=$prop_list['NAME']?></a>
								<?
							}
						}
						?>
					</div>
				</div>

				<?$APPLICATION->IncludeComponent(
					"bitrix:catalog.smart.filter",
					"",
					array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"DISPLAY_ELEMENT_COUNT" => 'Y',
						"SAVE_IN_SESSION" => "N",
						"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
						"XML_EXPORT" => "Y",
						"SECTION_TITLE" => "NAME",
						"SECTION_DESCRIPTION" => "DESCRIPTION",
						'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
						"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
						'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
						'CURRENCY_ID' => $arParams['CURRENCY_ID'],
						"SEF_MODE" => $arParams["SEF_MODE"],
						"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
						"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
						"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
						"INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);?>
			<?endif;?>
			
			
			
			
			<div class="of-day-wrap">
			<?
			// Товары дня
			
			CModule::IncludeModule("iblock");

			$arSelect = array("ID", "NAME", "PROPERTY_PRODUCTS");
			$arFilter = array("IBLOCK_ID" => $uchefSettings['OF_DAY_IBLOCK'], "ACTIVE" => "Y");
			$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

			while($ob = $res->GetNextElement()) {
				
				$arFields = $ob->GetFields();
				
				if($arFields['PROPERTY_PRODUCTS_VALUE']) { 
				
					global ${'arFilterOfDay' . $arFields['ID']};
					${'arFilterOfDay' . $arFields['ID']} = array(
					
						'ID' => $arFields['PROPERTY_PRODUCTS_VALUE'],
						/*'!PROPERTY_OF_DAY' => false,
						'>PROPERTY_OF_DAY_DATE' => date('Y-m-d H:i:s', time()),*/
						array(
							"LOGIC" => "OR",
							array(
								array(
									"LOGIC" => "OR",
									"PROPERTY_REGION" => false,
								),
							),
							array(
								array(
									"LOGIC" => "OR",
									"PROPERTY_REGION" => $_SESSION['SOTBIT_REGIONS']['ID'],
								),
							),
						)
					);
					?>
					<div class="of-day">
						<?$APPLICATION->IncludeComponent(
							"bitrix:catalog.section",
							"catalog-grid",
							Array(
								"ACTION_VARIABLE" => "action",
								"ADD_PICT_PROP" => "-",
								"ADD_PROPERTIES_TO_BASKET" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"ADD_TO_BASKET_ACTION" => "ADD",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "N",
								"BACKGROUND_IMAGE" => "-",
								"BASKET_URL" => "/cart/",
								"BROWSER_TITLE" => "-",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "N",
								"CACHE_TIME" => "36000000",
								"CACHE_TYPE" => "A",
								"COMPATIBLE_MODE" => "Y",
								"CONVERT_CURRENCY" => "N",
								"CUSTOM_FILTER" => "",
								"DETAIL_URL" => "",
								"DISABLE_INIT_JS_IN_COMPONENT" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"DISPLAY_COMPARE" => "N",
								"DISPLAY_TOP_PAGER" => "N",
								"ELEMENT_SORT_FIELD" => "sort",
								"ELEMENT_SORT_FIELD2" => "id",
								"ELEMENT_SORT_ORDER" => "asc",
								"ELEMENT_SORT_ORDER2" => "desc",
								"ENLARGE_PRODUCT" => "STRICT",
								"FILTER_NAME" => 'arFilterOfDay' . $arFields['ID'],
								"HIDE_NOT_AVAILABLE" => "Y",
								"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
								"IBLOCK_ID" => "5",
								"IBLOCK_TYPE" => "catalog",
								"INCLUDE_SUBSECTIONS" => "Y",
								"LABEL_PROP" => array(),
								"LAZY_LOAD" => "N",
								"LINE_ELEMENT_COUNT" => "",
								"LOAD_ON_SCROLL" => "N",
								"MESSAGE_404" => "",
								"MESS_BTN_ADD_TO_BASKET" => "В корзину",
								"MESS_BTN_BUY" => "Купить",
								"MESS_BTN_DETAIL" => "Подробнее",
								"MESS_BTN_SUBSCRIBE" => "Подписаться",
								"MESS_NOT_AVAILABLE" => "Нет в наличии",
								"META_DESCRIPTION" => "-",
								"META_KEYWORDS" => "-",
								"OFFERS_CART_PROPERTIES" => array(""),
								"OFFERS_FIELD_CODE" => array("",""),
								"OFFERS_LIMIT" => "0",
								"OFFERS_PROPERTY_CODE" => array("",""),
								"OFFERS_SORT_FIELD" => "sort",
								"OFFERS_SORT_FIELD2" => "id",
								"OFFERS_SORT_ORDER" => "asc",
								"OFFERS_SORT_ORDER2" => "desc",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_TEMPLATE" => ".default",
								"PAGER_TITLE" => "Товары",
								"PAGE_ELEMENT_COUNT" => "10",
								"PARTIAL_PRODUCT_PROPERTIES" => "N",
								"PRICE_CODE" => array("BASE"),
								"PRICE_VAT_INCLUDE" => "N",
								"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
								"PRODUCT_DISPLAY_MODE" => "N",
								"PRODUCT_ID_VARIABLE" => "id",
								"PRODUCT_PROPERTIES" => array(""),
								"PRODUCT_PROPS_VARIABLE" => "prop",
								"PRODUCT_QUANTITY_VARIABLE" => "quantity",
								"PRODUCT_ROW_VARIANTS" => array("={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false","={{'VARIANT':'2'}","BIG_DATA':false"),
								"PRODUCT_SUBSCRIPTION" => "Y",
								"PROPERTY_CODE" => array("OF_DAY","OF_DAY_DATE","OF_DAY_DISCOUNT","STIKERS"),
								"PROPERTY_CODE_MOBILE" => array(),
								"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
								"RCM_TYPE" => "personal",
								"SECTION_CODE" => "",
								"SECTION_ID" => "",
								"SECTION_ID_VARIABLE" => "SECTION_ID",
								"SECTION_URL" => "",
								"SECTION_USER_FIELDS" => array("",""),
								"SEF_MODE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_LAST_MODIFIED" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_STATUS_404" => "N",
								"SET_TITLE" => "N",
								"SHOW_404" => "N",
								"SHOW_ALL_WO_SECTION" => "Y",
								"SHOW_CLOSE_POPUP" => "N",
								"SHOW_DISCOUNT_PERCENT" => "N",
								"SHOW_FROM_SECTION" => "N",
								"SHOW_MAX_QUANTITY" => "N",
								"SHOW_OLD_PRICE" => "N",
								"SHOW_PRICE_COUNT" => "1",
								"SHOW_SLIDER" => "Y",
								"SLIDER_INTERVAL" => "3000",
								"SLIDER_PROGRESS" => "N",
								"TEMPLATE_THEME" => "blue",
								"USE_ENHANCED_ECOMMERCE" => "N",
								"USE_MAIN_ELEMENT_SECTION" => "N",
								"USE_PRICE_COUNT" => "N",
								"USE_PRODUCT_QUANTITY" => "N",
								"RANDOM_ITEMS" => 'Y',
								"HIDE_EMPTY" => 'Y',
							)
						);?>
					</div>
					
					<?
				}
			}
			?>
			</div>
		</div>
	</div>
		<div class="col-sm-12 col-md-8 col-lg-10 is-catalog">
			<div class="mobile-hide">
				<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
						"START_FROM" => "0",
						"PATH" => "",
						"SITE_ID" => "-"
					),
					false,
					Array('HIDE_ICONS' => 'Y')
				);?>
			</div>

			<br>
			<?
			$title = "";
			if (defined("IS_IN_SECTIONS")) {
				if (isset($_REQUEST['SALES'])) {
					$title = "Скидки";
				}
				elseif (isset($_REQUEST['CATEGORY'])) {
					$dbElement = CIBlockElement::GetById($_REQUEST['CATEGORY']);

					if($arElement = $dbElement->GetNext()){
						$title = $arElement['NAME'];
					}
				}
			}
			?>
			<h1 class="text-center catalog-title"><?=$title ? $title : $APPLICATION->ShowTitle(false)?></h1>
			
			<div class="md-filter-show-wrap">
				<div class="js-md-filter-controll md-filter-show-btn">Фильтр и подразделы</div>
			</div>
			
			
			<div class="row">

				<?
					$arFilter = array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "CODE" => $arResult['VARIABLES']['SECTION_CODE']);
					$arSelect = array("UF_HEADER_DESC", "UF_FOOTER_DESC");

					$dbSectiob = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);

					if($arSection = $dbSectiob->Fetch()){
						$arResult['IBLOCK_SECTION'] = $arSection;
					}
				?>

				<?if(!isset($_REQUEST['PAGEN_1']) && isset($arResult['IBLOCK_SECTION'])):?>
					<?if(isset($arResult['IBLOCK_SECTION']['UF_HEADER_DESC'])):?>
						<p><?=$arResult['IBLOCK_SECTION']['UF_HEADER_DESC']?></p>
					<?endif;?>
				<?endif;?>


				<div class="container-fluid catalog-sort-view">
					<div class="flexible pull-left sort-line" style="margin-top:22px;">
						<div class="select-wrapp">
							<div class="dropdown">
								Сортировать по
							  <a data-target="#sort-dropdown" href="#sort-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

								<?
								$sortByLabel = "";
								if($current_sort_type == "default"){
									$sortByLabel .= "Умолчанию ";
								}
								if($current_sort_type == "price"){
									$sortByLabel .= "Цене";
								}
								else if($current_sort_type == "date"){
									$sortByLabel .= "Новизне";
								}
								else if($current_sort_type == "name"){
									$sortByLabel .= "Наименованию";
								}

								if ($current_sort_type != "date" AND $current_sort_type != "default") {

									if($current_sort_order == "ubiv"){
										$sortByLabel .= " (убыв.)";
									}
									else if($current_sort_order == "vozvr"){
										$sortByLabel .= " (возр.)";
									}
								}

								echo $sortByLabel;
								?>
							  </a>
							  <span class="caret"></span>

							  <ul class="dropdown-menu" id="sort-dropdown">
							  <li><a href="<?=$APPLICATION->GetCurPageParam("sort=default", array("sort", "order"));?>">Умолчанию</a></li>
								<li><a href="<?=$APPLICATION->GetCurPageParam("sort=date", array("sort", "order"));?>">Новизне</a></li>
								<li><a href="<?=$APPLICATION->GetCurPageParam("sort=price&order=ubiv", array("sort", "order"));?>">Цене (убыв.)</a></li>
								<li><a href="<?=$APPLICATION->GetCurPageParam("sort=price&order=vozvr", array("sort", "order"));?>">Цене (возр.)</a></li>
							  </ul>
							</div>
						</div>
						<div class="view">
							<a href="<?=$APPLICATION->GetCurPageParam("view=grid", array("view"));?>"><i class="fa fa-lg fa-th-large" aria-hidden="true"></i></a>
							<a href="<?=$APPLICATION->GetCurPageParam("view=list", array("view"));?>"><i class="fa fa-lg fa-th-list" aria-hidden="true"></i></a>
						</div>
					</div>
					<div class="panel-pagination pull-right">
						<?$APPLICATION->ShowViewContent('pagination_section');?>
					</div>
				</div>
				<?
				$current_show = $arParams["PAGE_ELEMENT_COUNT"];
				if(isset($_REQUEST["show"])){
					$current_show = htmlspecialchars($_REQUEST["show"]);
					$APPLICATION->set_cookie("show", $current_show);
				}
				else{
					$current_show = $APPLICATION->get_cookie("show") ? $APPLICATION->get_cookie("show") : $arParams["PAGE_ELEMENT_COUNT"];
				}
				$arParams["PAGE_ELEMENT_COUNT"] = $current_show;
				
				$GLOBALS[$arParams["FILTER_NAME"]][0] = array(
				
					array(
						"LOGIC" => "OR",
						array(
							array(
								"LOGIC" => "OR",
								"PROPERTY_REGION" => false,
							),
						),
						array(
							array(
								"LOGIC" => "OR",
								"PROPERTY_REGION" => $_SESSION['SOTBIT_REGIONS']['ID'],
							),
						),
					)
				);
				
				?>

				<?$intSectionID = $APPLICATION->IncludeComponent(
					"bitrix:catalog.section",
					$view,
					array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],

						"SHOW_ALL_WO_SECTION" => defined("IS_IN_SECTIONS") ? "Y" : "N",
						"SET_TITLE" => defined("IS_IN_SECTIONS") ? "N" : "Y",

						"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
						"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
						"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
						"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
						"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
						"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
						"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
						"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
						"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
						"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
						"BASKET_URL" => $arParams["BASKET_URL"],
						"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
						"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
						"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_FILTER" => $arParams["CACHE_FILTER"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"SET_TITLE" => $arParams["SET_TITLE"],
						"MESSAGE_404" => $arParams["MESSAGE_404"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"SHOW_404" => $arParams["SHOW_404"],
						"FILE_404" => $arParams["FILE_404"],
						"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
						"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
						"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
						"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

						"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
						"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
						"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
						"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
						"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

						"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
						"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
						"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
						"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
						"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
						"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
						"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
						"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

						"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
						"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
						"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
						"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
						"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
						"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
						"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
						"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
						"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
						"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
						'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
						'CURRENCY_ID' => $arParams['CURRENCY_ID'],
						'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

						'LABEL_PROP' => $arParams['LABEL_PROP'],
						'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
						'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

						'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
						'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
						'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
						'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
						'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
						'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
						'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
						'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
						'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
						'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

						'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
						"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
						'ADD_TO_BASKET_ACTION' => $basketAction,
						'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
						'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
						'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
						'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
					),
					$component
				);?>
				
				<?
				
				unset($GLOBALS[$arParams["FILTER_NAME"]][0]);
				
				$APPLICATION->IncludeComponent(
				   "sotbit:seo.meta",
				   ".default",
				   Array(
				   "FILTER_NAME" => $arParams["FILTER_NAME"],
						"SECTION_ID" => $arCurSection['ID'],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
				   )
				);
				
				?>
				
				
				<?
				
				$context = \Bitrix\Main\Context::getCurrent();
				$arRequest = $context->getRequest()->toArray();
				$isPagin = false;
				
				foreach($arRequest as $key => $val) {
					
					if(preg_match('#^PAGEN_#', $key)) {
						
						$isPagin = true;
					}
				}
				
				global $sotbitSeoMetaBottomDesc;
				?>
				<? if(($sotbitSeoMetaBottomDesc || $sect['DESCRIPTION']) && !$isPagin) { ?>
				<div class="filter-bottom-text">
					<?=$sotbitSeoMetaBottomDesc ? $sotbitSeoMetaBottomDesc : $sect['~DESCRIPTION'];?>
				</div>
				<? } ?>
				
				<?if (!defined("IS_IN_SECTIONS")) :?>
					<? $showBlock = true; ?>
						<?if(count($arDiscounts) != 0){?>
							<? if($showBlock) { $showBlock = false; ?><div class="cat-block-menu"><? } ?>
							<a href="<?=$APPLICATION->GetCurPage(false);?>?SALES=Y" class="deposit "><span class="icon"></span>Скидки</a>
						<?}?>

					<?
					if (isset($_REQUEST['SECTION'])) {
						foreach ($arr_prop as $prop_list) {
							$to_list = NULL;
							$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => 5, "ACTIVE" => "Y", "SECTION_ID" => $_REQUEST['SECTION'], "INCLUDE_SUBSECTIONS" => "Y", "PROPERTY_CATEGORY" => (int)$prop_list["ID"]), Array("NAME"));
							while ($ar_fields = $res->GetNext()) {
								if ($ar_fields["CNT"] != NULL) {
									$to_list = $ar_fields["CNT"];
								}
							}

							if ((int)$to_list > 0 && $to_list != NULL) {
								?>
								<? if($showBlock) { $showBlock = false; ?><div class="cat-block-menu"><? } ?>
								<a href="<?=$APPLICATION->GetCurPage(false);?>?CATEGORY=<?=$prop_list['ID']?>" class=" ">
									<span class="icon" style="background: url(<?=CFile::GetPath($prop_list['DETAIL_PICTURE'])?>) center no-repeat;"></span>
									<?=$prop_list['NAME']?>
								</a>
								<?
							}
						}
					}
					else {
						foreach ($arr_prop as $prop_list) {
							?>
							<? if($showBlock) { $showBlock = false; ?><div class="cat-block-menu"><? } ?>
							<a href="<?=$APPLICATION->GetCurPage(false);?>?CATEGORY=<?=$prop_list['ID']?>" class=" ">
								<span class="icon" style="background: url(<?=CFile::GetPath($prop_list['DETAIL_PICTURE'])?>) center no-repeat;"></span>
								<?=$prop_list['NAME']?>
							</a>
							<?
						}
					}
					?>
					<? if(!$showBlock) { ?></div><? } ?>
					

					<?/*?>
					<div class="mobile-hide">
						<?$APPLICATION->IncludeComponent(
						"bitrix:catalog.section.list",
						"",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
							"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
							"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
							"VIEW_MODE" => "TILE",
							"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
							"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
							"ADD_SECTIONS_CHAIN" => "N",
							"HIDE_H1" => "Y",
						),
						$component,
						array("HIDE_ICONS" => "Y")
					);?>
					</div>
					<?*/?>

					<div class="mobile-visible products-items list">
						<?$APPLICATION->IncludeComponent(
							"bitrix:catalog.section.list",
							"",
							array(
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
								"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
								"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
								"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
								"VIEW_MODE" => "TILE",
								"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
								"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_H1" => "Y",
							),
							$component,
							array("HIDE_ICONS" => "Y")
						);?>
					</div>

				<?endif;?>
				
				

				<?$APPLICATION->IncludeComponent("bitrix:sale.bestsellers", "",
					array(
						"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
						"PAGE_ELEMENT_COUNT" => "50",
						"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
						"PRODUCT_SUBSCRIPTION" => $arParams['PRODUCT_SUBSCRIPTION'],
						"SHOW_NAME" => "Y",
						"SHOW_IMAGE" => "Y",
						"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
						"MESS_BTN_DETAIL" => $arParams['MESS_BTN_DETAIL'],
						"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
						"MESS_BTN_SUBSCRIBE" => $arParams['MESS_BTN_SUBSCRIBE'],
						"LINE_ELEMENT_COUNT" => 50,
						"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
						"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
						"CACHE_TYPE" => "N",
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"BY" => array(
							0 => "AMOUNT",
						),
						"PERIOD" => array(
							0 => "15",
						),
						"FILTER" => array(
							0 => "CANCELED",
							1 => "ALLOW_DELIVERY",
							2 => "PAYED",
							3 => "DEDUCTED",
							4 => "N",
							5 => "P",
							6 => "F",
						),
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"ORDER_FILTER_NAME" => "arOrderFilter",
						"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
						"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
						"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
						"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
						"CURRENCY_ID" => $arParams["CURRENCY_ID"],
						"BASKET_URL" => $arParams["BASKET_URL"],
						"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_slb",
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
						"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
						"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
						"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
						"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
						"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"] => "Y",
						"OFFER_TREE_PROPS_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
						"ADDITIONAL_PICT_PROP_".$arParams['IBLOCK_ID'] => $arParams['ADD_PICT_PROP'],
						"ADDITIONAL_PICT_PROP_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams['OFFER_ADD_PICT_PROP'],
						"PROPERTY_CODE_5" => array(
							0 => "STIKERS",
							1 => "",
						),
					),
					$component,
					array("HIDE_ICONS" => "Y")
				);?>

				<?if(!isset($_REQUEST['PAGEN_1']) && isset($arResult['IBLOCK_SECTION'])):?>
					<?if(isset($arResult['IBLOCK_SECTION']['UF_FOOTER_DESC'])):?>
						<p><?=$arResult['IBLOCK_SECTION']['UF_FOOTER_DESC']?></p>
					<?endif;?>
				<?endif;?>

			</div>
			<?
				$GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
				unset($basketAction);

				if (ModuleManager::isModuleInstalled("sale"))
				{
					if (!empty($arRecomData))
					{
						if (!isset($arParams['USE_SALE_BESTSELLERS']) || $arParams['USE_SALE_BESTSELLERS'] != 'N')
						{
							?>

			<?}
				if (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N')
				{
					?>
			<?$APPLICATION->IncludeComponent("bitrix:catalog.bigdata.products", "", array(
					"LINE_ELEMENT_COUNT" => 5,
					"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_cbdp",
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
					"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
					"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
					"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
					"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
					"PRODUCT_SUBSCRIPTION" => $arParams['PRODUCT_SUBSCRIPTION'],
					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"SHOW_NAME" => "Y",
					"SHOW_IMAGE" => "Y",
					"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
					"MESS_BTN_DETAIL" => $arParams['MESS_BTN_DETAIL'],
					"MESS_BTN_SUBSCRIBE" => $arParams['MESS_BTN_SUBSCRIBE'],
					"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
					"PAGE_ELEMENT_COUNT" => 5,
					"SHOW_FROM_SECTION" => "Y",
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"DEPTH" => "2",
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"] => "Y",
					"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
					"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
					"CURRENCY_ID" => $arParams["CURRENCY_ID"],
					"SECTION_ID" => $intSectionID,
					"SECTION_CODE" => "",
					"SECTION_ELEMENT_ID" => "",
					"SECTION_ELEMENT_CODE" => "",
					"LABEL_PROP_".$arParams["IBLOCK_ID"] => $arParams['LABEL_PROP'],
					"PROPERTY_CODE_".$arParams["IBLOCK_ID"] => $arParams["LIST_PROPERTY_CODE"],
					"PROPERTY_CODE_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"CART_PROPERTIES_".$arParams["IBLOCK_ID"] => $arParams["PRODUCT_PROPERTIES"],
					"CART_PROPERTIES_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFERS_CART_PROPERTIES"],
					"ADDITIONAL_PICT_PROP_".$arParams["IBLOCK_ID"] => $arParams['ADD_PICT_PROP'],
					"ADDITIONAL_PICT_PROP_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams['OFFER_ADD_PICT_PROP'],
					"OFFER_TREE_PROPS_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
					"RCM_TYPE" => (isset($arParams['BIG_DATA_RCM_TYPE']) ? $arParams['BIG_DATA_RCM_TYPE'] : '')
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);?>
					<?
					}
				}
			}
			?>

		</div>
	</div>
</div>

</div>
</div>

function getError(str = '') {
	$('.error_block').empty().append(str);
}
function submitForm(val)
{
	if(val != 'Y')
		BX('confirmorder').value = 'N';
	else if(val == 'Y') BX('confirmorder').value = 'Y';

	var orderForm = BX('ORDER_FORM');

	BX.ajax.submitComponentForm(orderForm, 'news_body_104', true);
	BX.submit(orderForm);

	return true;
}
function changePaySystem(param)
{
	if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
	{
		if (param == 'account')
		{
			if (BX("PAY_CURRENT_ACCOUNT"))
			{
				BX("PAY_CURRENT_ACCOUNT").checked = true;
				BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
				BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

				// deselect all other
				var el = document.getElementsByName("PAY_SYSTEM_ID");
				for(var i=0; i<el.length; i++)
					el[i].checked = false;
			}
		}
		else
		{
			BX("PAY_CURRENT_ACCOUNT").checked = false;
			BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
			BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
		}
	}
	else if (BX("account_only") && BX("account_only").value == 'N')
	{
		if (param == 'account')
		{
			if (BX("PAY_CURRENT_ACCOUNT"))
			{
				BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

				if (BX("PAY_CURRENT_ACCOUNT").checked)
				{
					BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
					BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
				}
				else
				{
					BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
					BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
				}
			}
		}
	}

	submitForm();
}
$(document).ready(function(){
	// $(document).on('click', '[data-event="click"]', function(){
	// 	console.log('ok');
	// 	$('.ui-state-default').removeClass('check');
	// 	$(this).addClass('check');
	// });
	$(document).on('keyup', 'input.city_detected', function(){
		var $this = $(this);
		var $val = $this.val();
		$('.hint').remove();
		$('body').append('<div class="hint"></div>');
		var hint = $('.hint');
		$.ajax({
			url: '/ajax/city_ajax.php',
			method: 'POST',
			dataType: 'json',
			data: {
				city_name: $val
			},
			success: function(data) {
				if(data.DATA != '') {
					hint.empty().append('<ul></ul>');
					for(var i in data.DATA) {
						hint.find('ul').append('<li><input type="hidden" value="'+data.DATA[i].ID+'"><span>'+data.DATA[i].CITY+'</span></li>');
					}
					hint.css({
						top: $this.offset().top + $this.outerHeight(),
						left: $this.offset().left,
						width: $this.outerWidth()
					})
					.show(function(){
						$(this).find('li').bind('click', function(){
							var $thiss = $(this);
							$this.val($thiss.find('span').text());
							$this.parent().find('input:hidden').val($thiss.find('input:hidden').val());
							$(this).parents('.hint').remove();
							$('#next_level_1').click();
						});
					});
				}
			}
		});
	});
	// .on('blur', 'input.city_detected', function(){
	// 	$('.hint').remove();
 //  	});
    BX.addCustomEvent('onAjaxSuccess', afterFormReload);
	function afterFormReload() {
    	$store = $('.store');
    	if($store.length) {
    		if($('.store.active').length == 0) $store.eq(0).addClass('active').click();
    		else $('.store.active').click();
    	}
    	if($( "#datepicker" ).length) {
    		$.post('/ajax/check_time_of_order.php',
        	    {
        	       session: BX.bitrix_sessid(),
        	    },
        	    function(data){
        	        $.datepicker.setDefaults({
        			    closeText: 'Закрыть',
        			    prevText: '',
        			    currentText: 'Сегодня',
        			    monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        			        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        			    monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
        			        'Июл','Авг','Сен','Окт','Ноя','Дек'],
        			    dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        			    dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        			    dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        			    weekHeader: 'Не',
        			    dateFormat: 'dd.mm.yy',
        			    firstDay: 1,
        			    isRTL: false,
        			    showMonthAfterYear: false,
        			    hideIfNoPrevNext: true,
        			    yearSuffix: '',
        			    minDate: 'd+'+(Number.isInteger(data.COUNT_DAY))?data.COUNT_DAY:1+'d.mm.yy',
        			    onSelect: function($str, $obj) {
        			        $('.date_input_check').val($str);
        			    }
        			});

        			$( "#datepicker" ).datepicker();
        			var date = $( "#datepicker" ).datepicker('getDate');
        			var $str = date.getDate()+'.'+date.getMonth()+'.'+date.getFullYear();
        			$('.date_input_check').val($str);
        	    },
        	    'json'
        	);
    	}
        
        var tt = $('#news_body_104');
        if(new String(tt.text()).indexOf('success') !== -1) {
        	$redirect = tt.text();
        	tt.text('');
        	var event = JSON.parse($redirect, function(key, value) {
			 	return value;
			});
			window.location.href = event.redirect;
        }
	}
	$store = $('.store');
    if($store.length) {
    	if($('.store.active').length == 0) $store.eq(0).addClass('active').click();
    	else $('.store.active').click();
    }
	$(document).on('click', '.store', function(){
    	var $this = $(this);
    	$('.store').removeClass('active');
    	$('[name="ORDER_PROP_'+$this.data('id-prop')+'"]').val('Покупатель выбрал склад № '+$this.data('id') +' для самовывоза');
    	$('[name="NUMBER_STORE"]').val($this.data('id'));
    	$this.addClass('active');
    });

    $(document).on('change', '[name="DELIVERY_ID"], [name="PAY_SYSTEM_ID"]', function(){
    	var $this = $(this);
    	$('.bx_element').removeClass('active');
    	if($this.prop('checked') == true) {
    		$this.parents('.bx_element:eq(0)').addClass('active');
    	}
    });

    $(document).on('click', '.login_order', function(){
    	console.log('asdasd');
    	$('header .login').click();
    });

    $(document).on('change', '#select_date', function(){
    	var $val = $(this).find('option:selected').val();
    	$('.date_delivery').val($val).prop('selected', true);
    });

    $(document).on('click', '.order_one_click', function(e){
    	$('.modal-window, .modal-bg').hide();
        $(document).unbind('click.myClick');
        e.preventDefault();
        var register = $('.order_1_click_form');
        var modal = $('.modal-bg');
        if(register.css('display') != 'block') {
        	register.find('.error_block').empty();
            register.css({
                marginTop: -(register.height() / 2),
                marginLeft: -(register.width() / 2)
            }).fadeIn();
            $(modal).fadeIn();

            var firstClick = true;

            $(document).bind('click.myClick', function(event){
                if(!firstClick && $(event.target).closest(register).length == 0 || $(event.target).closest(register.find('.close')).length == 1) {
                    register.fadeOut();
                    $(modal).fadeOut();
                    $(document).unbind('click.myClick');
                }
                firstClick = false;
            }); 
        }
    });

    $(document).on('click', '#order_1_click_form .next-level', function(){
    	var $this = $('#order_1_click_form');
    	var $input = $this.find('input');
    	var $error = false;
    	if($input.val().length == 0) {
    		$error = true;
    		$this.find('.error_block').empty().append('<div style="color: red">Не введен номер телефона!</div>');
    	} else if(new String($input.val()).replace(/\D/g, '').length < 9) {
    		$error = true;
    		$this.find('.error_block').empty().append('<div style="color: red">Некорректный номер телефона</div>');
    	}
    	if(!$error) {
    		$this.find('.error_block').empty();
    		$.post('/ajax/order_1_click.php',
        	    {
        	       phone: $input.val(),
        	    },
        	    function(data){
        	        if(data.NUM_ORDER != '') {
        	        	$this.empty();
        	        	$this.append('<div class="width-w txt_order_1_click complete"><div class="width-w title">Спасибо за заказа!</div>Ваш заказ был успешно принят!<br/>№ заказа: <span>'+data.NUM_ORDER+'</span></div>');
        	        	setTimeout(function(){
        	        		window.location.href = "/catalog/";
        	        	}, 5000);
        	        }
        	    },
        	    'json'
        	);
    	}
    	return false;
    });
    
	$(document).on('blur', 'input.city_input', function(){
		var $this = $(this);
		$this.css('color', '#fff');
		setTimeout(function(){
			var city = $this.val().split(',');
			$this.val(city[0]);
			$('[name="'+$this.data('hidden')+'"]').val(city[0]);
			$this.css('color', '#7f5f56');
			$str = '';
    		$('#ORDER_FORM [require]').each(function(){
    			var $this = $(this);
    			console.log($this.attr('name'));
    			if($this.val() == 0) $str+= '<div style="color: red">Поле ' + $this.data('required') + ' обязательное для заполнения!</div>';
    		});
    		getError($str);
    		if($str != '') $('.addr_block.active .edit').click();
    		else {
    			submitForm();
    		}
		}, 500);
  		//$(this).val(placeResult.name);
  	});
  	$countsClick = 0;
	$(document).on('click', '.edit, .addr_block', function(){
		$countsClick++;
		var $this = $(this);
		var $parent = ($this.hasClass('edit'))?$this.parents('.addr_block'):$this;
		$('.addr_block').removeClass('active');
		$parent.addClass('active');
		$parent.find('input, textarea').each(function(){
			var $name = $(this).attr('name');
			$('.more_addr [name="'+$name+'"], .more_addr [data-hidden="'+$name+'"]').val($(this).val());
		});
		if($this.hasClass('edit')) {
			$('.more_addr .hidden-block').fadeIn();
			// $('.steps').bind('click.myCustom', function(event){
			// 	// if($(event.target).closest('.more_addr').length == 0) {
			// 	// 	//$('.more_addr .hidden-block').fadeOut();
			// 	// 	$(document).unbind('click.myCustom');
			// 	// }
			// });
		} else {
			if($countsClick != 1) submitForm();
		}
	});
	$(document).on('click', '.addInput', function(){
		$('.more_addr').find('input, textarea').each(function(){
			$(this).val('');
		});
		$('.more_addr .hidden-block').fadeIn();
		// $('.steps').bind('click.myCustom', function(event){
		// 	if($(event.target).closest('.more_addr').length == 0) {
		// 		$('.more_addr .hidden-block').fadeOut();
		// 		$(document).unbind('click.myCustom');
		// 	}
		// });
	});
	$(document).on('click', '#next_level_1', function(){
    	$str = '';
    	$('#ORDER_FORM [require]').each(function(){
    		var $this = $(this);
    		if($this.val() == 0) $str+= '<div style="color: red">Поле ' + $this.data('required') + ' обязательное для заполнения!</div>';
    	});
    	getError($str);
    	if($str != '') $('.addr_block.active .edit').click();
    	else {
    		submitForm();
    	}
    });
    $(document).on('click', '#next_level_2', function(){
    	$('#ORDER_FORM [name="third_step"]').remove();
    	$('#ORDER_FORM').append('<input type="hidden" name="third_step" value="Y">');
    	submitForm();
    });
	$('.addr_block.active').click();
});


BX.saleOrderAjax = { // bad solution, actually, a singleton at the page

	BXCallAllowed: false,

	options: {},
	indexCache: {},
	controls: {},

	modes: {},
	properties: {},

	// called once, on component load
	init: function(options)
	{
		var ctx = this;
		this.options = options;

		window.submitFormProxy = BX.proxy(function(){
			ctx.submitFormProxy.apply(ctx, arguments);
		}, this);

		BX(function(){
			ctx.initDeferredControl();
		});
		BX(function(){
			ctx.BXCallAllowed = true; // unlock form refresher
		});

		this.controls.scope = BX('order_form_div');

		// user presses "add location" when he cannot find location in popup mode
		BX.bindDelegate(this.controls.scope, 'click', {className: '-bx-popup-set-mode-add-loc'}, function(){

			var input = BX.create('input', {
				attrs: {
					type: 'hidden',
					name: 'PERMANENT_MODE_STEPS',
					value: '1'
				}
			});

			BX.prepend(input, BX('ORDER_FORM'));

			ctx.BXCallAllowed = false;
			submitForm();
		});
	},

	cleanUp: function(){

		for(var k in this.properties)
		{
			if (this.properties.hasOwnProperty(k))
			{
				if(typeof this.properties[k].input != 'undefined')
				{
					BX.unbindAll(this.properties[k].input);
					this.properties[k].input = null;
				}

				if(typeof this.properties[k].control != 'undefined')
					BX.unbindAll(this.properties[k].control);
			}
		}

		this.properties = {};
	},

	addPropertyDesc: function(desc){
		this.properties[desc.id] = desc.attributes;
		this.properties[desc.id].id = desc.id;
	},

	// called each time form refreshes
	initDeferredControl: function()
	{
		var ctx = this,
			k,
			row,
			input,
			locPropId,
			m,
			control,
			code,
			townInputFlag,
			adapter;

		// first, init all controls
		if(typeof window.BX.locationsDeferred != 'undefined'){

			this.BXCallAllowed = false;

			for(k in window.BX.locationsDeferred){

				window.BX.locationsDeferred[k].call(this);
				window.BX.locationsDeferred[k] = null;
				delete(window.BX.locationsDeferred[k]);

				this.properties[k].control = window.BX.locationSelectors[k];
				delete(window.BX.locationSelectors[k]);
			}
		}

		for(k in this.properties){

			// zip input handling
			if(this.properties[k].isZip){
				row = this.controls.scope.querySelector('[data-property-id-row="'+k+'"]');
				if(BX.type.isElementNode(row)){

					input = row.querySelector('input[type="text"]');
					if(BX.type.isElementNode(input)){
						this.properties[k].input = input;

						// set value for the first "location" property met
						locPropId = false;
						for(m in this.properties){
							if(this.properties[m].type == 'LOCATION'){
								locPropId = m;
								break;
							}
						}

						if(locPropId !== false){
							BX.bindDebouncedChange(input, function(value){

								input = null;
								row = null;

								if(BX.type.isNotEmptyString(value) && /^\s*\d+\s*$/.test(value) && value.length > 3){

									ctx.getLocationByZip(value, function(locationId){
										ctx.properties[locPropId].control.setValueByLocationId(locationId);
									}, function(){
										try{
											ctx.properties[locPropId].control.clearSelected(locationId);
										}catch(e){}
									});
								}
							});
						}
					}
				}
			}

			// location handling, town property, etc...
			if(this.properties[k].type == 'LOCATION')
			{

				if(typeof this.properties[k].control != 'undefined'){

					control = this.properties[k].control; // reference to sale.location.selector.*
					code = control.getSysCode();

					// we have town property (alternative location)
					if(typeof this.properties[k].altLocationPropId != 'undefined')
					{
						if(code == 'sls') // for sale.location.selector.search
						{
							// replace default boring "nothing found" label for popup with "-bx-popup-set-mode-add-loc" inside
							control.replaceTemplate('nothing-found', this.options.messages.notFoundPrompt);
						}

						if(code == 'slst')  // for sale.location.selector.steps
						{
							(function(k, control){

								// control can have "select other location" option
								control.setOption('pseudoValues', ['other']);

								// insert "other location" option to popup
								control.bindEvent('control-before-display-page', function(adapter){

									control = null;

									var parentValue = adapter.getParentValue();

									// you can choose "other" location only if parentNode is not root and is selectable
									if(parentValue == this.getOption('rootNodeValue') || !this.checkCanSelectItem(parentValue))
										return;

									var controlInApater = adapter.getControl();

									if(typeof controlInApater.vars.cache.nodes['other'] == 'undefined')
									{
										controlInApater.fillCache([{
											CODE:		'other', 
											DISPLAY:	ctx.options.messages.otherLocation, 
											IS_PARENT:	false,
											VALUE:		'other'
										}], {
											modifyOrigin:			true,
											modifyOriginPosition:	'prepend'
										});
									}
								});

								townInputFlag = BX('LOCATION_ALT_PROP_DISPLAY_MANUAL['+parseInt(k)+']');

								control.bindEvent('after-select-real-value', function(){

									// some location chosen
									if(BX.type.isDomNode(townInputFlag))
										townInputFlag.value = '0';
								});
								control.bindEvent('after-select-pseudo-value', function(){

									// option "other location" chosen
									if(BX.type.isDomNode(townInputFlag))
										townInputFlag.value = '1';
								});

								// when user click at default location or call .setValueByLocation*()
								control.bindEvent('before-set-value', function(){
									if(BX.type.isDomNode(townInputFlag))
										townInputFlag.value = '0';
								});

								// restore "other location" label on the last control
								if(BX.type.isDomNode(townInputFlag) && townInputFlag.value == '1'){

									// a little hack: set "other location" text display
									adapter = control.getAdapterAtPosition(control.getStackSize() - 1);

									if(typeof adapter != 'undefined' && adapter !== null)
										adapter.setValuePair('other', ctx.options.messages.otherLocation);
								}

							})(k, control);
						}
					}
				}
			}
		}

		this.BXCallAllowed = true;
	},

	checkMode: function(propId, mode){

		//if(typeof this.modes[propId] == 'undefined')
		//	this.modes[propId] = {};

		//if(typeof this.modes[propId] != 'undefined' && this.modes[propId][mode])
		//	return true;

		if(mode == 'altLocationChoosen'){

			if(this.checkAbility(propId, 'canHaveAltLocation')){

				var input = this.getInputByPropId(this.properties[propId].altLocationPropId);
				var altPropId = this.properties[propId].altLocationPropId;

				if(input !== false && input.value.length > 0 && !input.disabled && this.properties[altPropId].valueSource != 'default'){

					//this.modes[propId][mode] = true;
					return true;
				}
			}
		}

		return false;
	},

	checkAbility: function(propId, ability){

		if(typeof this.properties[propId] == 'undefined')
			this.properties[propId] = {};

		if(typeof this.properties[propId].abilities == 'undefined')
			this.properties[propId].abilities = {};

		if(typeof this.properties[propId].abilities != 'undefined' && this.properties[propId].abilities[ability])
			return true;

		if(ability == 'canHaveAltLocation'){

			if(this.properties[propId].type == 'LOCATION'){

				// try to find corresponding alternate location prop
				if(typeof this.properties[propId].altLocationPropId != 'undefined' && typeof this.properties[this.properties[propId].altLocationPropId]){

					var altLocPropId = this.properties[propId].altLocationPropId;

					if(typeof this.properties[propId].control != 'undefined' && this.properties[propId].control.getSysCode() == 'slst'){

						if(this.getInputByPropId(altLocPropId) !== false){
							this.properties[propId].abilities[ability] = true;
							return true;
						}
					}
				}
			}

		}

		return false;
	},

	getInputByPropId: function(propId){
		if(typeof this.properties[propId].input != 'undefined')
			return this.properties[propId].input;

		var row = this.getRowByPropId(propId);
		if(BX.type.isElementNode(row)){
			var input = row.querySelector('input[type="text"]');
			if(BX.type.isElementNode(input)){
				this.properties[propId].input = input;
				return input;
			}
		}

		return false;
	},

	getRowByPropId: function(propId){

		if(typeof this.properties[propId].row != 'undefined')
			return this.properties[propId].row;

		var row = this.controls.scope.querySelector('[data-property-id-row="'+propId+'"]');
		if(BX.type.isElementNode(row)){
			this.properties[propId].row = row;
			return row;
		}

		return false;
	},

	getAltLocPropByRealLocProp: function(propId){
		if(typeof this.properties[propId].altLocationPropId != 'undefined')
			return this.properties[this.properties[propId].altLocationPropId];

		return false;
	},

	toggleProperty: function(propId, way, dontModifyRow){

		var prop = this.properties[propId];

		if(typeof prop.row == 'undefined')
			prop.row = this.getRowByPropId(propId);

		if(typeof prop.input == 'undefined')
			prop.input = this.getInputByPropId(propId);

		if(!way){
			if(!dontModifyRow)
				BX.hide(prop.row);
			prop.input.disabled = true;
		}else{
			if(!dontModifyRow)
				BX.show(prop.row);
			prop.input.disabled = false;
		}
	},

	submitFormProxy: function(item, control)
	{
		var propId = false;
		for(var k in this.properties){
			if(typeof this.properties[k].control != 'undefined' && this.properties[k].control == control){
				propId = k;
				break;
			}
		}

		// turning LOCATION_ALT_PROP_DISPLAY_MANUAL on\off

		if(item != 'other'){

			if(this.BXCallAllowed){

				this.BXCallAllowed = false;
				submitForm();
			}

		}
	},

	getPreviousAdapterSelectedNode: function(control, adapter){

		var index = adapter.getIndex();
		var prevAdapter = control.getAdapterAtPosition(index - 1);

		if(typeof prevAdapter !== 'undefined' && prevAdapter != null){
			var prevValue = prevAdapter.getControl().getValue();

			if(typeof prevValue != 'undefined'){
				var node = control.getNodeByValue(prevValue);

				if(typeof node != 'undefined')
					return node;

				return false;
			}
		}

		return false;
	},
	getLocationByZip: function(value, successCallback, notFoundCallback)
	{
		if(typeof this.indexCache[value] != 'undefined')
		{
			successCallback.apply(this, [this.indexCache[value]]);
			return;
		}

		ShowWaitWindow();

		var ctx = this;

		BX.ajax({

			url: this.options.source,
			method: 'post',
			dataType: 'json',
			async: true,
			processData: true,
			emulateOnload: true,
			start: true,
			data: {'ACT': 'GET_LOC_BY_ZIP', 'ZIP': value},
			//cache: true,
			onsuccess: function(result){

				CloseWaitWindow();
				if(result.result){

					ctx.indexCache[value] = result.data.ID;

					successCallback.apply(ctx, [result.data.ID]);

				}else
					notFoundCallback.call(ctx);

			},
			onfailure: function(type, e){

				CloseWaitWindow();
				// on error do nothing
			}

		});
	}

}
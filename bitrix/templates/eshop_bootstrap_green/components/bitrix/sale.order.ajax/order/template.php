<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<a href="/catalog/" class="back_cat">&laquo;&nbsp;&nbsp;Продолжить покупки</a>
<div style="clear: both;"></div>
<div class="panel_top_basket">
    <div class="steps">
        <ul class="list">
            <li>
                <a href="/personal/cart/">Редактирование корзины</a>
            </li>
            <li <?if(!isset($_REQUEST['ORDER_ID'])):?>class="active"<?endif;?>>
                <a href="">Адрес доставки и оплата</a>
            </li>
            <li <?if(isset($_REQUEST['ORDER_ID'])):?>class="active"<?endif;?>>
                <a href="">Спасибо!</a>
            </li>
        </ul>
        <div style="clear: both;"></div>
    </div>
<div>
<?if(isset($_REQUEST['ORDER_ID'])):?>
    <?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");?>
<?else:?>
        <div class="b-order-cont order">
        	<div class="steps">
                    <div class="number">
                        <span>1</span>
                        Контактные данные
                    </div>
            <?$checkFields = false;
            if($USER->IsAuthorized()):
            	$rsUser = CUser::GetByID($USER->GetID());
				$arUser = $rsUser->Fetch();
				if(!empty($arUser['UF_COUNT_FORM']) && $arUser['UF_COUNT_FORM'] != '{}'):
                    $checkFields = true;
				    $arUserData = json_decode($arUser['UF_COUNT_FORM']);
				    $USER_PROPS_N = $arResult["ORDER_PROP"]["USER_PROPS_N"];
                    $USER_PROPS_Y = $arResult["ORDER_PROP"]["USER_PROPS_Y"];
                    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props_format.php");
            ?>
                    <div class="titles-leader">Адрес доставки</div>
                    <div class="container_order">
                    <?foreach ($arUserData as $k => $value):
                        $c = '';
                        if(!preg_match('/\D/', $value->{'town'})) {
                            $c = ReaspGeoIP::SelectCityId($value->{'town'});
                        }

                    	$town = (!empty($c))?$c['CITY']:'';
                    	$street = (!empty($value->{'street'}))?$value->{'street'}:'';
                    	$num_home = (!empty($value->{'num_home'}))?$value->{'num_home'}:'';
                    	$flat = (!empty($value->{'flat'}))?$value->{'flat'}:'';
                        $txt = (!empty($value->{'txt'}))?$value->{'txt'}:'';
                    	$strInfo = $town.', ' . $street . ', ' . $num_home . ', ' . $flat . ', ';
                    	$phone = (!empty($value->{'phone'}))?$value->{'phone'}:'';
                    	$nameLastName = (!empty($value->{'fio'}))?$value->{'fio'}:$arUser['NAME'].' '.$arUser['LAST_NAME'];
                    	   foreach ($USER_PROPS_N as $key => $val) {
                            if($val['CODE'] == 'COMMENT' && !empty($txt)) $USER_PROPS_N[$key]['VALUE'] = $txt;
                		  	if($val['CODE'] == 'CITY' && !empty($town)) $USER_PROPS_N[$key]['VALUE'] = $c['ID'];
                		  	if($val['CODE'] == 'STREET' && !empty($street)) $USER_PROPS_N[$key]['VALUE'] = $street;
                		  	if($val['CODE'] == 'NUM_HONE' && !empty($num_home)) $USER_PROPS_N[$key]['VALUE'] = $num_home;
                		  	if($val['CODE'] == 'NUM_FLAT' && !empty($flat)) $USER_PROPS_N[$key]['VALUE'] = $flat;
                            if($val['CODE'] == 'PHONE' && !empty($phone)) $USER_PROPS_N[$key]['VALUE'] = $phone;
                            if($val['CODE'] == 'FIO' && !empty($nameLastName)) $USER_PROPS_N[$key]['VALUE'] = $nameLastName;
                            if($val['CODE'] == 'ADDRESS') $USER_PROPS_N[$key]['VALUE'] = $strInfo;
                		  }
                		  foreach ($USER_PROPS_Y as $key => $val) {
                            if($val['CODE'] == 'COMMENT' && !empty($txt)) $USER_PROPS_Y[$key]['VALUE'] = $txt;
                		  	if($val['CODE'] == 'CITY' && !empty($town)) $USER_PROPS_Y[$key]['VALUE'] = $c['ID'];
                		  	if($val['CODE'] == 'STREET' && !empty($street)) $USER_PROPS_Y[$key]['VALUE'] = $street;
                		  	if($val['CODE'] == 'NUM_HONE' && !empty($num_home)) $USER_PROPS_Y[$key]['VALUE'] = $num_home;
                		  	if($val['CODE'] == 'NUM_FLAT' && !empty($flat)) $USER_PROPS_Y[$key]['VALUE'] = $flat;
                            if($val['CODE'] == 'PHONE' && !empty($phone)) $USER_PROPS_Y[$key]['VALUE'] = $phone;
                            if($val['CODE'] == 'FIO' && !empty($nameLastName)) $USER_PROPS_Y[$key]['VALUE'] = $nameLastName;
                            if($val['CODE'] == 'ADDRESS') $USER_PROPS_Y[$key]['VALUE'] = $strInfo;
                		  }
                    ?>
                        <div class="addr_block<?if($value->{'favorite'} == 'Y'):?> active<?endif;?>">
                            <span class="edit"></span>
                            <ul>
                                <?if(!empty($strInfo)):?><li><?=substr($strInfo, 0, -2);?></li><?endif?>
                                <?if(!empty($phone)):?><li><?=$phone?></li><?endif;?>
                                <li><?=$nameLastName?></li>
                            </ul>
                            <div class="more" style="display: none;">

                               <?
                                    echo PrintPropsForm($USER_PROPS_Y, $arParams["TEMPLATE_LOCATION"]);
                               		echo PrintPropsForm($USER_PROPS_N, $arParams["TEMPLATE_LOCATION"]);
                               ?>
                            </div>
                        </div>
                    <?endforeach;?>
                    </div>
                <?endif;?>
            <?endif;?>
            </div>
            <div class="modal-bg"></div>
            <div class="order_1_click_form modal-window">
                <div class="wrap">
                    <form id="order_1_click_form" method="POST">
                        <div class="close">&times;</div>
                        <div class="titles width-w">Заказ в один клик</div>
                        <div class="width-w txt_order_1_click">
                            Оставьте ваш номер телефона - наш менеджер сам свяжется с вами, оформит заказ и уточнит место и время доставки
                        </div>
                        <div class="error_block"></div>
                        <div class="width-w input_order_1_click">
                            <input type="text" name="phone" placeholder="Ваш номер телефона">
                        </div>
                        <div class="width-w">
                            <a href="javascript:void(0)" class="btn btn-default next-level btn-block">Отправить</a>
                        </div>
                    </form>
                </div>
            </div>
            <form action="<?= $APPLICATION->GetCurPage(); ?>" class="b-order-form order__form" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
                
                <?= bitrix_sessid_post() ?>
                <input type="hidden" id="PERSON_TYPE_1" name="PERSON_TYPE" value="1" checked="">
                <input type="hidden" name="PROFILE_ID" value="0">
                <input type="hidden" name="PERSON_TYPE_OLD" value="1">
                <input type="hidden" name="showProps" id="showProps" value="N">
                <input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="0">
                <input type="hidden" id="account_only" value="N">
                <input type="hidden" name="PAY_CURRENT_ACCOUNT" value="N">
                <input type="hidden" name="profile_change" id="profile_change" value="N">
                <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
                <input type="hidden" name="json" value="Y">
                <input type="submit" name="save" value="Y" style="display: none;">
                <input type="hidden" name="LOCATION" value="" class="bx-ui-slst-target">
                <?if(isset($_REQUEST['confirmorder'])) {
                    $APPLICATION->RestartBuffer();
                ?>
                    <?if(strlen($arResult["REDIRECT_URL"]) > 0)
                    {
                        ?>
                        <script type="text/javascript">
                        window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
                
                        </script>
                        <?
                        die();
                    }?> 
                <?
                }
                ?>
                <div id="news_body_104">
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                <script src="/js/jQuery.jscrollpane.js"></script>
                <script defer src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDxOMrGRCPvMmBIuRQr3mREJ45RuZKhmFc"></script>
                <script src="/js/jquery.geocomplete.min.js"></script>
                <!-- <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.fancybox.min.css">
                <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.fancybox.min.js"></script> -->
                <div class="steps" style="padding-top: 1px;">
                    <div class="error_block">
                        <?if(!empty($arResult["ERROR"]))
                        {
                            foreach($arResult["ERROR"] as $v) {
                                $_POST['ERROR'][] = $v;
                                echo ShowError($v);
                            }
            
                            ?>
                            <script type="text/javascript">
                                top.BX.scrollToNode(top.BX('ORDER_FORM'));
                            </script>
                            <?
                        }?>
                    </div>
                </div>
                <div class="steps">
                    <?
                        //print_r($_POST['ORDER_PROP_'.$_POST['CITY']['ID']]);
                    ?>
					<?//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
					//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
                    if($checkFields):?>
                        <div class="container_order">
                            <div class="addInput">Добавить другой адрес</div>
                            <div class="more_addr">
                            	<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");?>
                            </div>
                        </div>
                    <?if(isset($_POST['is_ajax_post'])):?>
						<style>
                        	.hidden-block {
                        		display: block;
                        	}
                        </style>
                    <?endif;?>
                    <?else:?>
                        <div class="user_without">  
                            <?if(!$USER->IsAuthorized()):?>
                                <ul class="link_with_user">
                                    <li>
                                        <a href="javascript:void(0)" class="active">Я новый покупатель</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="login_order">Я уже покупал у вас</a>
                                    </li>
                                </ul>
                            <?endif;?>
                            <?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");?>
                            <style>
                            	.hidden-block {
                            		display: block;
                            	}
                            </style>
                        </div>
                    <?endif;?>
                    
                        <a href="javascript:void(0)" id="next_level_1" class="btn btn-default next-level btn-block" style="max-width: 290px;<?if(isset($_POST['is_ajax_post']) && empty($_POST['ERROR'])):?>opacity: 0;<?endif;?>">Перейти к выбору способа доставки</a>
                </div>
                <?if(isset($_POST['is_ajax_post']) && empty($_POST['ERROR'])):
                ?>
                <div class="steps next-step deliv_bl">
                    <input type="hidden" name="step_second" value="Y">
                    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                    <link rel="stylesheet" href="/resources/demos/style.css">
                    <?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");?>
                    <div class="number">
                        <span>2</span>
                        Выберите способ<br/>
                        доставки
                    </div>
                    <div class="wrap_deliv">
                    <?if(!is_null($_POST['FIND_INFO_DELIVERY']) && floatval($arResult['ORDER_TOTAL_PRICE']) > floatval($_POST['FIND_INFO_DELIVERY']['min']) && $arResult['CHECKED_DELIVERY'] == 2):?>
                        <div class="col-md-6">
                            <div class="titles-d">Выберите дату</div>
                            <div id="datepicker"></div>
                        </div>
                        <div class="col-md-6">
                        <?
                        $deliverySelect = array();
                        foreach ($arResult["ORDER_PROP"]["USER_PROPS_N"] as $key => $val) {
                            if($val['CODE'] == 'DATE_DELIVERY') {
                                $deliverySelect = $arResult["ORDER_PROP"]["USER_PROPS_N"][$key];
                                break;
                            }
                        }
                        foreach ($arResult["ORDER_PROP"]["USER_PROPS_Y"] as $key => $val) {
                            if($val['CODE'] == 'DATE_DELIVERY') {
                                $deliverySelect = $arResult["ORDER_PROP"]["USER_PROPS_N"][$key];
                                break;
                            }
                        }
                        ?>
                            <div class="titles-d">Укажите время</div>
                            <div class="select">
                                <?if(!empty($deliverySelect['VARIANTS'])):?>
                                    <select id="select_date">
                                        <?foreach ($deliverySelect['VARIANTS'] as $key => $value):?>
                                            <option value="<?=$value['VALUE']?>" <?if($value['SELECTED'] == 'Y') echo "selected";?>><?=$value['NAME']?></option>
                                        <?endforeach;?>
                                    </select>
                                <?endif;?>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="cost_product">
                            <div class="cost_deliv">
                            	<?if(floatval($arResult['ORDER_TOTAL_PRICE']) > floatval($_POST['FIND_INFO_DELIVERY']['free'])):?>
                                	Стоимость доставки: 0 руб
                                <?else:?>
                                	Стоимость доставки: <?=$_POST['FIND_INFO_DELIVERY']['cost'].' руб';?>
                                <?endif;?>
                            </div>
                            <div class="free_cost">
                            	<?if(floatval($arResult['ORDER_TOTAL_PRICE']) > floatval($_POST['FIND_INFO_DELIVERY']['free'])):?>
                                	У вас бесплатная доставка!
                                <?else:?>
                                	Бесплатная доставка от <?=$_POST['FIND_INFO_DELIVERY']['free'].' рублей';?>
                                <?endif;?>
                            </div>
                            <a href="/catalog/">Добавить товар</a>
                        </div>
                        <a href="javascript:void(0)" id="next_level_2" class="btn btn-default next-level btn-block" style="max-width: 290px;<?if(isset($_POST['is_ajax_post']) && empty($_POST['ERROR']) && $_POST['third_step'] == 'Y'):?>opacity: 0;<?endif;?>">Перейти к выбору способа оплаты</a>
                    <?elseif(!is_null($_POST['FIND_INFO_DELIVERY']) && floatval($arResult['ORDER_TOTAL_PRICE']) < floatval($_POST['FIND_INFO_DELIVERY']['free']) && $arResult['CHECKED_DELIVERY'] == 2):?>
                        <?$_POST['ERROR']='Error';?>
                        <div class="warning_cur">
                            <p>
                                Для доставки собственным курьером Шеф Гурме<br/>
                                минимальная сумма заказа в вашем городе составляет:<br/>
                                <?=$_POST['FIND_INFO_DELIVERY']['min'].' рублей';?>
                            </p>
                            <p>
                                Добавьте к заказу товаров еще на сумму:<br/>
                                <?=floatval($_POST['FIND_INFO_DELIVERY']['min']) - floatval($arResult['ORDER_TOTAL_PRICE']) . ' рублей';?>
                            </p>
                        </div>
                        </div>
                    </div>
                    <div class="steps next-step last-steps">
                        <div class="wrap_deliv">
                            <div class="buy_block">
                                Текущая сумма заказа<br/>
                                <b><?=$arResult['ORDER_TOTAL_PRICE']?></b> рублей
                            </div>
                            <div class="buy_block">
                                Минимальная сумма заказа<br/>
                                <b><?=$_POST['FIND_INFO_DELIVERY']['min']?></b> рублей
                            </div>
                        <a href="/catalog/" class="btn btn-default next-level btn-block">Вернуться к покупкам</a>
                    <?elseif(isset($_POST['is_ajax_post']) && $arResult['CHECKED_DELIVERY'] == 3):?>
                        <div class="store_bl">
                        <input type="hidden" name="NUMBER_STORE" value="<?=$_POST['NUMBER_STORE']?>">
                        <?$sto = CCatalogStore::GetList( array(), array('ACTIVE' => 'Y'), false, false, array('UF_CART_LINK', 'UF_WORKTIME'));
                        while ($store = $sto->Fetch()) {?>
                            <div class="store<?if($store['ID'] == $_POST['NUMBER_STORE']) echo ' active';?>" data-id-prop="<?=$_POST['ADDRESS_STORE']['ID']?>" data-id="<?=$store['ID']?>">
                                <div class="wrap">
                                    <?if(!empty($store['TITLE'])):?>
                                        <?=$store['TITLE']?><br/>
                                    <?endif;?>
                                    <?if(!empty($store['ADDRESS'])):?>
                                        <?=$store['ADDRESS']?><br/><br/>
                                    <?endif;?>
                                    <?if(!empty($store['UF_WORKTIME'])):?>
                                        Время работы: <?=$store['UF_WORKTIME']?><br/>
                                    <?endif;?>
                                </div>
                                <?if(!empty($store['UF_CART_LINK'])):?>
                                    <a href="<?=$store['UF_CART_LINK']?>" target="_blank" class="callback fancybox.iframe">Показать на карте</a>
                                <?endif;?>
                            </div>
                        <?}?>
                        </div>
                        <div class="cost_del">Стоимость самовывоза 0 руб</div>
                        <a href="javascript:void(0)" id="next_level_2" class="btn btn-default next-level btn-block" style="max-width: 290px;<?if(isset($_POST['is_ajax_post']) && empty($_POST['ERROR']) && $_POST['third_step'] == 'Y'):?>opacity: 0;<?endif;?>">Перейти к выбору способа оплаты</a>
                    <?endif;?>
                    </div>
                </div>
                <?endif;?>
                <?if(isset($_POST['is_ajax_post']) && empty($_POST['ERROR']) && $_POST['third_step'] == 'Y'):?>
                <div class="steps next-step deliv_bl">
                    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                    <link rel="stylesheet" href="/resources/demos/style.css">
                    <?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");?>
                    <div class="number">
                        <span>3</span>
                        Выберите способ<br/>
                        оплаты
                    </div>
                    <div class="wrap_deliv">
                    <?if(!is_null($_POST['FIND_INFO_DELIVERY'] || $arResult['CHECKED_DELIVERY'] == 3)):?>
                        <div class="warning_cur completes">
                            <?
                                $costDeliverys = 0;
                                if(!is_null($_POST['FIND_INFO_DELIVERY']) && $arResult['CHECKED_DELIVERY'] != 3 && floatval($arResult['ORDER_TOTAL_PRICE']) < floatval($_POST['FIND_INFO_DELIVERY']['free'])) $costDeliverys = $_POST['FIND_INFO_DELIVERY']['cost'];
                            ?>
                            <p class="cost_p">
                            	Стоимость заказа: <?=$arResult['ORDER_TOTAL_PRICE']. ' рублей';?>
                            </p>
                            <p class="cost_p">
                            	<?if(floatval($arResult['ORDER_TOTAL_PRICE']) > floatval($_POST['FIND_INFO_DELIVERY']['free']) && $arResult['CHECKED_DELIVERY'] == 2):?>
                                	У вас бесплатная доставка!
                                <?else:
                                    $arResult['ORDER_TOTAL_PRICE'] = floatval($arResult['ORDER_TOTAL_PRICE']) + floatval($costDeliverys);
                                ?>
                                	<?if($arResult['CHECKED_DELIVERY'] == 3):?>Стоимость самовывоза:<?else:?>Стоимость доставки:<?endif;?> <?=$costDeliverys. ' рублей';?>
                                <?endif;?>
                            </p>
                            <p class="allSum">Итого: <?=floatval($arResult['ORDER_TOTAL_PRICE']). ' рублей'?></p>
                        </div>
                    <?endif;?>
                    <br/>
                    <a href="javascript:void(0)" id="confirm" onclick="submitForm('Y');" class="btn btn-default next-level btn-block">Оформить заказ</a>
                    </div>
                </div>
                <?endif;?>
                </div>
                <?if(isset($_REQUEST['confirmorder'])) die();?>
                <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
                <input type="hidden" name="PROFILE_ID" value="0">
                <input type="hidden" name="ORDER_PRICE" value="<?= $arResult["ORDER_PRICE"] ?>">
                <input type="hidden" name="ORDER_WEIGHT" value="<?= $arResult["ORDER_WEIGHT"] ?>">
                <input type="hidden" name="SKIP_FIRST_STEP" value="<?= $arResult["SKIP_FIRST_STEP"] ?>">
                <input type="hidden" name="SKIP_SECOND_STEP" value="<?= $arResult["SKIP_SECOND_STEP"] ?>">
                <input type="hidden" name="SKIP_THIRD_STEP" value="<?= $arResult["SKIP_THIRD_STEP"] ?>">
                <input type="hidden" name="SKIP_FORTH_STEP" value="<?= $arResult["SKIP_FORTH_STEP"] ?>">
        </form>
        </div>
<?endif;?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="cart-small-basket">
	<div class="cart-total-line">
		<div class="cart-t bold">Товаров на:</div>
		<div class="cart-d"><?=$arResult["allSum_FORMATED"]?></div>
	</div>
	<div class="cart-total-line">
		<div class="cart-t">Общий вес:</div>
		<div class="cart-d"><?=$arResult["allWeight_FORMATED"]?></div>
	</div>
	<div class="cart-total-line">
		<div class="cart-t bold">Итого:</div>
		<div class="cart-d"><?=$arResult["allSum_FORMATED"]?></div>
	</div>
</div>

<?
use Bitrix\Sale\DiscountCouponsManager;
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
);
$this->addExternalCss($templateData['TEMPLATE_THEME']);

$curPage = $APPLICATION->GetCurPage().'?'.$arParams["ACTION_VARIABLE"].'=';
$arUrls = array(
	"delete" => $curPage."delete&id=#ID#",
	"delay" => $curPage."delay&id=#ID#",
	"add" => $curPage."add&id=#ID#",
);
unset($curPage);

$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

$arBasketJSParams = array(
	'SALE_DELETE' => GetMessage("SALE_DELETE"),
	'SALE_DELAY' => GetMessage("SALE_DELAY"),
	'SALE_TYPE' => GetMessage("SALE_TYPE"),
	'TEMPLATE_FOLDER' => $templateFolder,
	'DELETE_URL' => $arUrls["delete"],
	'DELAY_URL' => $arUrls["delay"],
	'ADD_URL' => $arUrls["add"],
	'EVENT_ONCHANGE_ON_START' => (!empty($arResult['EVENT_ONCHANGE_ON_START']) && $arResult['EVENT_ONCHANGE_ON_START'] === 'Y') ? 'Y' : 'N',
	'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
	'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
	'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
);
?>
<script type="text/javascript">
	var basketJSParams = <?=CUtil::PhpToJSObject($arBasketJSParams);?>;
</script>
<?
$APPLICATION->AddHeadScript($templateFolder."/script.js");

if($arParams['USE_GIFTS'] === 'Y' && $arParams['GIFTS_PLACE'] === 'TOP')
{
	$APPLICATION->IncludeComponent(
		"bitrix:sale.gift.basket",
		".default",
		array(
			"SHOW_PRICE_COUNT" => 1,
			"PRODUCT_SUBSCRIPTION" => 'N',
			'PRODUCT_ID_VARIABLE' => 'id',
			"PARTIAL_PRODUCT_PROPERTIES" => 'N',
			"USE_PRODUCT_QUANTITY" => 'N',
			"ACTION_VARIABLE" => "actionGift",
			"ADD_PROPERTIES_TO_BASKET" => "Y",

			"BASKET_URL" => $APPLICATION->GetCurPage(),
			"APPLIED_DISCOUNT_LIST" => $arResult["APPLIED_DISCOUNT_LIST"],
			"FULL_DISCOUNT_LIST" => $arResult["FULL_DISCOUNT_LIST"],

			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_SHOW_VALUE"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],

			'BLOCK_TITLE' => $arParams['GIFTS_BLOCK_TITLE'],
			'HIDE_BLOCK_TITLE' => $arParams['GIFTS_HIDE_BLOCK_TITLE'],
			'TEXT_LABEL_GIFT' => $arParams['GIFTS_TEXT_LABEL_GIFT'],
			'PRODUCT_QUANTITY_VARIABLE' => $arParams['GIFTS_PRODUCT_QUANTITY_VARIABLE'],
			'PRODUCT_PROPS_VARIABLE' => $arParams['GIFTS_PRODUCT_PROPS_VARIABLE'],
			'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
			'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
			'SHOW_NAME' => $arParams['GIFTS_SHOW_NAME'],
			'SHOW_IMAGE' => $arParams['GIFTS_SHOW_IMAGE'],
			'MESS_BTN_BUY' => $arParams['GIFTS_MESS_BTN_BUY'],
			'MESS_BTN_DETAIL' => $arParams['GIFTS_MESS_BTN_DETAIL'],
			'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
			'CONVERT_CURRENCY' => $arParams['GIFTS_CONVERT_CURRENCY'],
			'HIDE_NOT_AVAILABLE' => $arParams['GIFTS_HIDE_NOT_AVAILABLE'],

			"LINE_ELEMENT_COUNT" => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
		),
		false
	);
}
if(!function_exists('BITGetDeclNum'))
{
    function BITGetDeclNum($value=1, $status= array('','а','ов'))
    {
     $array =array(2,0,1,1,1,2);
     return $status[($value%100>4 && $value%100<20)? 2 : $array[($value%10<5)?$value%10:5]];
    }
}
if (strlen($arResult["ERROR_MESSAGE"]) <= 0)
{
	if(isset($_REQUEST['basUpdate'])) {
		$APPLICATION->RestartBuffer();
		?>
	<div class="bx-content">
		<?}
	?>
	<div id="warning_message">
		<?
		if (!empty($arResult["WARNING_MESSAGE"]) && is_array($arResult["WARNING_MESSAGE"]))
		{
			foreach ($arResult["WARNING_MESSAGE"] as $v)
				ShowError($v);
		}
		?>
	</div>

	<?
	$normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
	$normalHidden = ($normalCount == 0) ? 'style="display:none;"' : '';

	$delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
	$delayHidden = ($delayCount == 0) ? 'style="display:none;"' : '';

	$subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
	$subscribeHidden = ($subscribeCount == 0) ? 'style="display:none;"' : '';

	$naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
	$naHidden = ($naCount == 0) ? 'style="display:none;"' : '';
	$str = preg_replace("/[^0-9]/", '', $discountListCart[1]["UNPACK"]);
	if ((int)$arResult["allSum"] < (int)$str)
	$dif = (int)$str - (int)$arResult["allSum"];

	foreach (array_keys($arResult['GRID']['HEADERS']) as $id)
	{
		$data = $arResult['GRID']['HEADERS'][$id];
		$headerName = (isset($data['name']) ? (string)$data['name'] : '');
		if ($headerName == '')
			$arResult['GRID']['HEADERS'][$id]['name'] = GetMessage('SALE_'.$data['id']);
		unset($headerName, $data);
	}
	unset($id);?>
	<a href="/catalog/" class="back_cat">&laquo;&nbsp;&nbsp;Продолжить покупки</a>
	<div style="clear: both;"></div>
	<a href="/personal/cart/" id="updateBasketLink"></a>
		<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_form">
			<?if(count($arResult["GRID"]["ROWS"]) > 0):?>
			<div class="panel_top_basket">
				<ul class="list">
					<li class="active"><a href="">Редактирование корзины</a></li>
					<li><a href="">Адрес доставки и оплата</a></li>
					<li><a href="">Спасибо!</a></li>
				</ul>
				<div class="wrap_top_panel">
					<div class="block_f basket-b">
						<div class="product_pr">
							<?=count($arResult['GRID']['ROWS']);?> товар<?=BITGetDeclNum(count($arResult['GRID']['ROWS']))?>
						</div>
					</div>
					<div class="block_s basket-b">
						<div class="input">
							<input type="text" id="coupon" name="COUPON" onblur="enterCoupon();" placeholder="Ввести промо-код">
							<span class="helper bx_ordercart_order_pay_left" id="coupons_block">
								<?if (!empty($arResult['COUPON_LIST']))
									{
										foreach ($arResult['COUPON_LIST'] as $oneCoupon)
										{
											$couponClass = 'disabled';
											switch ($oneCoupon['STATUS'])
											{
												case DiscountCouponsManager::STATUS_NOT_FOUND:
												case DiscountCouponsManager::STATUS_FREEZE:
													$couponClass = 'bad';
													break;
												case DiscountCouponsManager::STATUS_APPLYED:
													$couponClass = 'good';
													break;
											}
											?><div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>"><span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span></div><?
										}
										unset($couponClass, $oneCoupon);
									}?>
							</span>
						</div>
					</div>
					<div class="block_t basket-b">
						<div class="input">
							<?if(!session_id()) session_start();?>
							<textarea name="ADDICTION_ORDER" onblur="enterAddiction(this)" placeholder="Примечание к заказу"><?=$_SESSION['ADDICTION_ORDER']?></textarea>
						</div>
					</div>
					<?

						if (!CModule::IncludeModule('reaspekt.geobase')) {
						    ShowError("Error! Module no install");
							return;
						}
							$dataDelivery = array();
							$arSort = Array('NAME', 'PROPERTY_MIN_PRICE', 'PROPERTY_FREE_DELIV');
							$ci = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array('IBLOCK_ID' => 12, 'ACTIVE' => 'Y', 'SECTION_ID' => 371), false, false, $arSort);
							while($city = $ci->GetNext()) {
								$dataDelivery['city'][$city['NAME']]['min'] = $city['PROPERTY_MIN_PRICE_VALUE'];
								$dataDelivery['city'][$city['NAME']]['free'] = $city['PROPERTY_FREE_DELIV_VALUE'];
							}
							$reg = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array('IBLOCK_ID' => 12, 'ACTIVE' => 'Y', 'SECTION_ID' => 372), false, false, $arSort);
							while($region = $reg->GetNext()) {
								$dataDelivery['region'][$region['NAME']]['min'] = $region['PROPERTY_MIN_PRICE_VALUE'];
								$dataDelivery['region'][$region['NAME']]['free'] = $region['PROPERTY_FREE_DELIV_VALUE'];
							}
							$arData = ReaspGeoIP::GetAddr();
							
							$dataPrice = null;
							if(isset($dataDelivery['city'][$arData['CITY']])) $dataPrice = $dataDelivery['city'][$arData['CITY']];
							elseif(is_null($dataPrice) && isset($dataDelivery['region'][$arData['REGION']])) $dataPrice = $dataDelivery['region'][$arData['REGION']];

						if(!is_null($dataPrice)):?>
						<script type="text/javascript">
							var minSumPrice = <?=$dataPrice['min']?>;
							var freeSumPrice = <?=$dataPrice['free']?>;
						</script>
						<?endif;?>
					<div class="block_f basket-b" id="free_block">
						<?if(!is_null($dataPrice)):?>
							<?if($dataPrice['free'] > $arResult['allSum']):
							$free = intval($dataPrice['free']) - intval($arResult['allSum']);
							?>
								До бесплатной доставки осталось <span><?=$free?> р.</span><br/>
								<a href="/catalog/">Перейти в каталог</a>
							<?else:?>
								У Вас бесплатная доставка !
							<?endif?>
						<?endif;?>
					</div>
					<div class="block_fi basket-b">
						<div class="sum">Итого <span id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span></div>
					</div>
					<div class="block_s basket-b">
						<a href="javascript:void(0)" onclick="checkOut();" class="btn btn-default btn-block">Оформить заказ</a>
					</div>
				</div>
			</div>
			<?endif;?>
			<?$del = true;
			foreach ($arResult["GRID"]["ROWS"] as $key => $value) {
				if($value['DELAY'] == 'N') {
					$del = false;
					break;
				}
			}
			if(!$del):?>
			<div class="basket_param" id="basket_param">
				<ul>
					<?
					global $USER;
					if ($USER->IsAuthorized()):?>
					<li><a href="javascript:void(0)" onclick="delayBasket(this)">Сохранить корзину</a></li>
					<?endif;?>
					<li><a href="javascript:void(0)" onclick="deleteBasket(<?=($USER->GetID())?$USER->GetID():1?>)">Очистить корзину</a></li>
				</ul>
			</div>
			<?endif;?>
			<div id="basket_form_container">
				<div class="bx_ordercart <?=$templateData['TEMPLATE_CLASS']; ?>">
					<div class="block_s">
					<?if ($USER->IsAuthorized()):?>
					<div class="bx_sort_container">
						<span><?=GetMessage("SALE_ITEMS")?></span>
						<a href="javascript:void(0)" id="basket_toolbar_button" class="current btn btn-default btn-block" onclick="showBasketItemsList()"><?=GetMessage("SALE_BASKET_ITEMS")?><div id="normal_count" class="flat" style="display:none">&nbsp;(<?=$normalCount?>)</div></a>
						<a href="javascript:void(0)" id="basket_toolbar_button_delayed" class="btn btn-default btn-block" onclick="showBasketItemsList(2)" <?=$delayHidden?>><?=GetMessage("SALE_BASKET_ITEMS_DELAYED")?><div id="delay_count" class="flat">&nbsp;(<?=$delayCount?>)</div></a>
						<a href="javascript:void(0)" id="basket_toolbar_button_subscribed" class="btn btn-default btn-block" onclick="showBasketItemsList(3)" <?=$subscribeHidden?>><?=GetMessage("SALE_BASKET_ITEMS_SUBSCRIBED")?><div id="subscribe_count" class="flat">&nbsp;(<?=$subscribeCount?>)</div></a>
						<a href="javascript:void(0)" id="basket_toolbar_button_not_available" class="btn btn-default btn-block" onclick="showBasketItemsList(4)" <?=$naHidden?>><?=GetMessage("SALE_BASKET_ITEMS_NOT_AVAILABLE")?><div id="not_available_count" class="flat">&nbsp;(<?=$naCount?>)</div></a>
					</div>
					<?endif;?>
					</div>
					<?
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_delayed.php");
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_subscribed.php");
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_not_available.php");
					?>
				</div>
			</div>
			<input type="hidden" name="BasketOrder" value="BasketOrder" />
			<!-- <input type="hidden" name="ajax_post" id="ajax_post" value="Y"> -->
		</form>
<div class="basket_recommend">
	<?
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"similar_product", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => (!empty($arParams["ELEMENT_SORT_FIELD"]))?$arParams["ELEMENT_SORT_FIELD"]:"sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => (!empty($arParams["ELEMENT_SORT_ORDER"]))?$arParams["ELEMENT_SORT_ORDER"]:"asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "round",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "20",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "SIZE",
			1 => "NEWPRODUCT",
			2 => "SALELEADER",
			3 => "SPECIALOFFER",
			4 => "",
		),
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_CODE_PATH" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_SHS_PARSER",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SEF_RULE" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "green",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "similar_product",
		"CUSTOM_FILTER" => "",
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ARTNUMBER",
			1 => "SIZE",
			2 => "",
		),
		"PRODUCT_DISPLAY_MODE" => "N",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"MESS_BTN_COMPARE" => "Сравнить",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
</div>		
<?
}
else
{
	ShowError($arResult["ERROR_MESSAGE"]);
}
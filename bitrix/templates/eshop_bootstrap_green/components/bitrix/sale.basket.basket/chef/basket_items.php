<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;
$USER_ID = $GLOBALS["USER"]->GetID();
$discountListCart = getDiscountCartList($arResult["ITEMS"]["AnDelCanBuy"], $USER_ID, 500000, $arResult['allWeight']);
$str = preg_replace("/[^0-9]/", '', $discountListCart[1]["UNPACK"]);
if ((int)$arResult["allSum"] < (int)$str)
	$dif = (int)$str - (int)$arResult["allSum"];
if ($normalCount > 0):
if($dif > 0)
	echo 'До бесплатной доставки осталось '.$dif.' руб';
?>


<div id="basket_items_list">
	<div class="bx_ordercart_order_table_container">
		<div id="basket_items">
			<?
				foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader){
					$arHeaders[] = $arHeader["id"];
				}
			?>
		
			<?
			foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
				if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):?>
					<?
					$liquid="";
					$res = CIBlockElement::GetProperty("5", $arItem['PRODUCT_ID'], array("sort" => "asc"), array("CODE" => "liquid"));
					while ($ob = $res->GetNext())
					{
						$liquid=$ob['VALUE'];
						//echo'<pre>';print_r($ob);echo'</pre>';
					}
					?>
					<?//echo'<pre>';print_r($arItem);echo'</pre>';?>	
					<div id="<?=$arItem["ID"]?>" class="row basket-item">
						<div class="col-sm-2 bx-col">
							<div class="bx-col-wrapper">
								<div class="bx_ordercart_photo_container">
								<? if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
										$url = $arItem["PREVIEW_PICTURE_SRC"];
									elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
										$url = $arItem["DETAIL_PICTURE_SRC"];
									else:
										$url = $templateFolder."/images/no_photo.png";
									endif;

									if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
										<div class="bx_ordercart_photo" style="background-image:url('<?=$url?>')"></div>
									<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
								</div>
								<? if (!empty($arItem["BRAND"])): ?>
									<div class="bx_ordercart_brand">
										<img alt="" src="<?=$arItem["BRAND"]?>" />
									</div>
								<? endif; ?>
							</div>
						
						</div>
						<div class="col-sm-5 bx-col">
							<div class="bx-col-wrapper">
								<div class="row">
									<div class="col-md-7 bx-col inner">
										<div class="bx-col-wrapper inner">
											<div class="bx_ordercart_itemtitle">	

												<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
													<?=$arItem["NAME"]?> <?if(!empty($liquid)){ echo "(" . $liquid. ")";}else{?><?=($arItem["WEIGHT"] > 1 ? "(" . $arItem["WEIGHT_FORMATED"] . ")" : "")?> <?}?>
												<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
											</div>
											
											<?if(!empty($arItem['PROPERTY_ARTNUMBER_VALUE'])):?>
												<p>Артикул <?=$arItem['PROPERTY_ARTNUMBER_VALUE']?></p>
											<?endif;?>
										</div>
									</div>
									<div class="col-md-5 bx-col inner">
										<div class="bx-col-wrapper inner">
											<?if($arItem['FULL_PRICE'] != $arItem['PRICE']):?>
												<div class="bx_ordercart_base_price"><span id="old_price_<?=$arItem["ID"]?>"><?=priceFormat($arItem['FULL_PRICE'])?>&nbsp;<div class="fa fa-ruble-sign"></div></span></div>
											<?else:?>
												<div class="bx_ordercart_base_price"><span id="old_price_<?=$arItem["ID"]?>"></span></div>
												<?/*?><div class="bx_ordercart_base_price_empty">&nbsp;</div><?*/?>
											<?endif;?>
											<div class="bx_ordercart_price"><span id="current_price_<?=$arItem["ID"]?>"><?=priceFormat($arItem['PRICE'])?></span>&nbsp;<div class="fa fa-ruble-sign"></div></div>
											
											<div class="bx_ordercart_price_unit">/<?=$arItem['MEASURE_TEXT']?></div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						
						<div class="col-sm-4 bx-col">
							<div class="bx-col-wrapper">
								<div class="row">
									<div class="col-sm-6 bx-col inner">
										<div class="bx-col-wrapper inner">
											<div class="bx_ordercart_buttons_counter_block counter-block">
												<a href="javascript:void(0)" id="bx_117848907_4012_quant_down" class="bx_bt_button_type_2 bx_small bx_fwb btn-plus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);">-</a>
												<input id="QUANTITY_INPUT_<?=$arItem["ID"]?>" name="QUANTITY_INPUT_<?=$arItem["ID"]?>" type="text" class="tac transparent_input" value="<?=$arItem['QUANTITY']?>" onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$arItem["MEASURE_RATIO"]?>, <?=$useFloatQuantityJS?>)">
												<a href="javascript:void(0)" id="bx_117848907_4012_quant_up" class="bx_bt_button_type_2 bx_small bx_fwb btn-minus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);">+</a>
												<div class="bx_ordercart_qty_unit"><?=$arItem['MEASURE_TEXT']?></div>
												<input type="hidden" id="QUANTITY_<?=$arItem["ID"]?>" name="QUANTITY_<?=$arItem["ID"]?>" value="<?=$arItem['QUANTITY']?>" />
											</div>
										</div>
									</div>
									<div class="col-sm-6 bx-col inner">
										<div class="bx-col-wrapper inner">
											<div class="bx_ordercart_sum">Всего: <span id="sum_<?=$arItem["ID"]?>"><?=$arItem['SUM']?></span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-1 bx-col bx-col__controll">
							<div class="bx-col-wrapper text-center">
								<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" onclick="return deleteProductRow(this)" class="bx_ordercart_delete">
								×
								</a>
							</div>
						</div>
					</div>
				
				<?endif;
			
			endforeach;?>
		</div>
	</div>
	<input type="hidden" id="column_headers" value="<?=htmlspecialcharsbx(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=htmlspecialcharsbx($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />

	<div class="basket__bottom-wrap">
		<div class="basket__bottom">
			<div class="basket__bottom-total basket__bottom-col">
				<?=GetMessage("SALE_TOTAL")?>
				<span id="allSum_FORMATED"><?=$arResult["allSum_FORMATED"]?></span>
			</div>
			<div class="basket__bottom-btn basket__bottom-col">
				<?if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0):?>
					<?=$arResult["PREPAY_BUTTON"]?>
					<span><?=GetMessage("SALE_OR")?></span>
				<?endif;?>
				<?
				if ($arParams["AUTO_CALCULATION"] != "Y")
				{
					?>
					<a href="javascript:void(0)" onclick="updateBasket();" class="checkout refresh btn btn-default"><?=GetMessage("SALE_REFRESH")?></a>
					<?
				}
				?>
				<a href="javascript:void(0)" onclick="checkOut();" class="checkout btn white-btn"><?=GetMessage("SALE_ORDER")?></a>
			</div>
		</div>
	</div>
</div>
<?
else:
?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
endif;
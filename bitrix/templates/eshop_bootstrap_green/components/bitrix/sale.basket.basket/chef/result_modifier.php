<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use Bitrix\Main;
use Bitrix\Sale\Delivery;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Delivery\Restrictions;
use Bitrix\Sale\Delivery\Services;
use Bitrix\Sale\Internals\Input;
use Bitrix\Sale\Delivery\DeliveryLocationTable;
use Bitrix\Sale\Location\LocationTable;



global $USER;
global $packItemID;

$defaultParams = array(
	'TEMPLATE_THEME' => 'blue'
);
$arParams = array_merge($defaultParams, $arParams);
unset($defaultParams);

$arParams['TEMPLATE_THEME'] = (string)($arParams['TEMPLATE_THEME']);
if ('' != $arParams['TEMPLATE_THEME'])
{
	$arParams['TEMPLATE_THEME'] = preg_replace('/[^a-zA-Z0-9_\-\(\)\!]/', '', $arParams['TEMPLATE_THEME']);
	if ('site' == $arParams['TEMPLATE_THEME'])
	{
		$templateId = (string)Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', SITE_ID);
		$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? 'eshop_adapt' : $templateId;
		$arParams['TEMPLATE_THEME'] = (string)Main\Config\Option::get('main', 'wizard_'.$templateId.'_theme_id', 'blue', SITE_ID);
	}
	if ('' != $arParams['TEMPLATE_THEME'])
	{
		if (!is_file($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
			$arParams['TEMPLATE_THEME'] = '';
	}
}
if ('' == $arParams['TEMPLATE_THEME']){
	$arParams['TEMPLATE_THEME'] = 'blue';
}
if(CModule::IncludeModule ('iblock')){ 

	foreach ($arResult["GRID"]["ROWS"] as $k => $arItem){
		$arFilter=array ("ID"=>$arItems["PRODUCT_ID"]);   
		$arSelectedFields = array('PROPERTY_ARTNUMBER', 'PROPERTY_TARE');
		$dbCatItem = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, false, $arSelectedFields);

		if($arCatItem = $dbCatItem->Fetch()){ 
			$arResult["GRID"]["ROWS"][$k]['ITEM_PROPS'] = $arCatItem;             
		} 
	}	  
  
}


$arResult['DELIVERIES'] = array();

if($USER->IsAuthorized()){
	$ID = $USER->GetID();

	$dbUser = CUser::GetByID($ID);
	
	if($arUser = $dbUser->Fetch()){
		$arUfCountForm = json_decode($arUser['UF_COUNT_FORM'], true);
		
		if(isset($arUfCountForm['form_1'])){
			
			$dbDeliveryList = Delivery\Services\Manager::getActiveList();

			foreach($dbDeliveryList as $arDelivery){
				$hasLocation = false;
				$dbRes = \Bitrix\Sale\Internals\ServiceRestrictionTable::getList(array(
					'filter' => array(
						'=SERVICE_ID' => $arDelivery['ID'],
						'=SERVICE_TYPE' => Delivery\Restrictions\Manager::SERVICE_TYPE_SHIPMENT
					),
					'order' => array('SORT' => 'ASC', 'ID' => 'DESC')
				));
				
				$arRes = $dbRes->fetchAll();
				
				foreach($arRes as $key => $res){
					
					
					if(empty($res['CLASS_NAME']) || !class_exists($res['CLASS_NAME']))
						continue;

					if(!is_subclass_of($res['CLASS_NAME'], 'Bitrix\Sale\Delivery\Restrictions\Base'))
						continue;
					
					
					if($res['CLASS_NAME'] == "\Bitrix\Sale\Delivery\Restrictions\ByLocation"){
						$deliveryLocationIterator = \Bitrix\Sale\Delivery\DeliveryLocationTable::getList([
														'select' => [
															'*',
															'LOCATION_ID' => 'LOCATION.ID',
															'LOCATION_GROUP_ID' => 'GROUP.ID',
															'LOCATION_GROUP_LOCATION_ID' => 'LOCATION_GROUP.LOCATION_ID',
														],
														'filter' => [
															'=DELIVERY_ID' => $arDelivery['ID']
														],
														'runtime' => [
															'LOCATION_GROUP' => [
																'data_type' => '\Bitrix\Sale\Location\GroupLocationTable',
																'reference' => [
																	'=this.LOCATION_GROUP_ID' => 'ref.LOCATION_GROUP_ID',
																],
																'join_type' => 'left'
															],
														]
													]);
						 
						while ($deliveryLocation = $deliveryLocationIterator->fetch()) {
						 
							$locationId = null;
						 
							if (
								isset($deliveryLocation['LOCATION_GROUP_ID'])
								&& $deliveryLocation['LOCATION_GROUP_ID'] > 0
							) {
						 
								if ($deliveryLocation['LOCATION_GROUP_LOCATION_ID'] <= 0) {
									continue;
								}
						 
								$locationId = $deliveryLocation['LOCATION_GROUP_LOCATION_ID'];
							} else if (
								isset($deliveryLocation['LOCATION_ID'])
								&& $deliveryLocation['LOCATION_ID'] > 0
							) {
								$locationId = $deliveryLocation['LOCATION_ID'];
							}
						 
							if (!isset($locationId)) {
								continue;
							}

							if($locationId == $arUfCountForm['form_1']['location']){
								
								
								$childrenIterator = \Bitrix\Sale\Location\LocationTable::getById($locationId, [
									'select' => [
										'ID',
										'CODE',
										'NAME.NAME',
										'PARENT.NAME.NAME',
									],
									'filter' => [
										'=NAME.LANGUAGE_ID' => LANGUAGE_ID,
										'=PARENT.NAME.LANGUAGE_ID' => LANGUAGE_ID,
									]
								]);
							 
								foreach ($childrenIterator->fetchAll() as $location) {
									$hasLocation = true;								
									$arRes[$key]["PARAMS"][$location['ID']] = $location;
								}
							
							}
						 
						}
					}
					else{
						if(!empty($res["PARAMS"])){
							$arRes[$key]["PARAMS"] = $res['CLASS_NAME']::prepareParamsValues($res["PARAMS"], $arDelivery['ID']);
						}
					}
					
					$arDelivery['RESTRICTIONS'] = $arRes;
					
				}	
				
				if($hasLocation){
					$arResult['DELIVERIES'][] = $arDelivery;
				}
				
			}
			
		}
			
	}
	
	$minPrice = 99999999;
	$selectedDel = -1;

	foreach($arResult['DELIVERIES'] as $key => $arDelivery){
		foreach($arDelivery['RESTRICTIONS'] as $arRestriction){
			if($arRestriction['CLASS_NAME'] == "\Bitrix\Sale\Delivery\Restrictions\ByPrice"){
				if($minPrice > $arRestriction['PARAMS']['MIN_PRICE'] && $arRestriction['PARAMS']['MIN_PRICE'] > $arResult['allSum']){
					$minPrice = $arRestriction['PARAMS']['MIN_PRICE'];
					$selectedDel = $key;
				}
			}
		}
	}

	if($selectedDel > -1){
		if($arResult['DELIVERIES'][$selectedDel]['CONFIG']['MAIN']['MARGIN_VALUE'] == 0){
			$arResult['DELIVERY_MESSAGE'] = 'До бесплатной доставки осталось';
		}
		else{
			$arResult['DELIVERY_MESSAGE'] = 'До возможности сделать заказ осталось';
		}
		
		
		$arResult['DELIVERY_MESSAGE'] .= ' ' . ($minPrice - $arResult['allSum']) . ' <i class="fa fa-rub"></i>';
		$arResult['DELIV_PRICE']=$minPrice;
	}
	
}

$arResult['DELIV_PRICE'] = 5000;

/*

$userID = \Bitrix\Sale\Fuser::getId();

$dbBasketItems = CSaleBasket::GetList(
	array(),
	array(
		'FUSER_ID' => $userID,
		'LID' => SITE_ID,
		'ORDER_ID' => 'NULL'
	), 
	false, 
	false, 
	array()
);


$arBasketItemsId = array();


while($arItem = $dbBasketItems->Fetch()) {
	$arBasketItemsId[] = $arItem['PRODUCT_ID'];	
}


$arFilter = array('IBLOCK_ID' => 13, 'SECTION_ID' => 374, 'ID' => $arBasketItemsId);
$arSelect = array('ID', 'NAME', 'PRICE', 'PROPERTY_MAX_WEIGHT');

$dbElements = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
	
while($arElement = $dbElements->GetNextElement()){
	$arElement->GetFields();
}	
	
*/	
		

  
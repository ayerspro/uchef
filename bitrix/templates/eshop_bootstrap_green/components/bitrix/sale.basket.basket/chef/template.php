<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */

use Bitrix\Sale\DiscountCouponsManager;

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
);
$this->addExternalCss($templateData['TEMPLATE_THEME']);

$curPage = $APPLICATION->GetCurPage().'?'.$arParams["ACTION_VARIABLE"].'=';
$arUrls = array(
	"delete" => $curPage."delete&id=#ID#",
	"delay" => $curPage."delay&id=#ID#",
	"add" => $curPage."add&id=#ID#",
);
unset($curPage);

$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

$arBasketJSParams = array(
	'SALE_DELETE' => GetMessage("SALE_DELETE"),
	'SALE_DELAY' => GetMessage("SALE_DELAY"),
	'SALE_TYPE' => GetMessage("SALE_TYPE"),
	'TEMPLATE_FOLDER' => $templateFolder,
	'DELETE_URL' => $arUrls["delete"],
	'DELAY_URL' => $arUrls["delay"],
	'ADD_URL' => $arUrls["add"],
	'EVENT_ONCHANGE_ON_START' => (!empty($arResult['EVENT_ONCHANGE_ON_START']) && $arResult['EVENT_ONCHANGE_ON_START'] === 'Y') ? 'Y' : 'N',
	'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
	'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
	'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
);
?>
<script type="text/javascript">
	var basketJSParams = <?=CUtil::PhpToJSObject($arBasketJSParams);?>;
</script>
<?
$APPLICATION->AddHeadScript($templateFolder."/script.js");

if($arParams['USE_GIFTS'] === 'Y' && $arParams['GIFTS_PLACE'] === 'TOP')
{
	$APPLICATION->IncludeComponent(
		"bitrix:sale.gift.basket",
		".default",
		array(
			"SHOW_PRICE_COUNT" => 1,
			"PRODUCT_SUBSCRIPTION" => 'N',
			'PRODUCT_ID_VARIABLE' => 'id',
			"PARTIAL_PRODUCT_PROPERTIES" => 'N',
			"USE_PRODUCT_QUANTITY" => 'N',
			"ACTION_VARIABLE" => "actionGift",
			"ADD_PROPERTIES_TO_BASKET" => "Y",

			"BASKET_URL" => $APPLICATION->GetCurPage(),
			"APPLIED_DISCOUNT_LIST" => $arResult["APPLIED_DISCOUNT_LIST"],
			"FULL_DISCOUNT_LIST" => $arResult["FULL_DISCOUNT_LIST"],

			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_SHOW_VALUE"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],

			'BLOCK_TITLE' => $arParams['GIFTS_BLOCK_TITLE'],
			'HIDE_BLOCK_TITLE' => $arParams['GIFTS_HIDE_BLOCK_TITLE'],
			'TEXT_LABEL_GIFT' => $arParams['GIFTS_TEXT_LABEL_GIFT'],
			'PRODUCT_QUANTITY_VARIABLE' => $arParams['GIFTS_PRODUCT_QUANTITY_VARIABLE'],
			'PRODUCT_PROPS_VARIABLE' => $arParams['GIFTS_PRODUCT_PROPS_VARIABLE'],
			'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
			'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
			'SHOW_NAME' => $arParams['GIFTS_SHOW_NAME'],
			'SHOW_IMAGE' => $arParams['GIFTS_SHOW_IMAGE'],
			'MESS_BTN_BUY' => $arParams['GIFTS_MESS_BTN_BUY'],
			'MESS_BTN_DETAIL' => $arParams['GIFTS_MESS_BTN_DETAIL'],
			'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
			'CONVERT_CURRENCY' => $arParams['GIFTS_CONVERT_CURRENCY'],
			'HIDE_NOT_AVAILABLE' => $arParams['GIFTS_HIDE_NOT_AVAILABLE'],

			"LINE_ELEMENT_COUNT" => 6,
			"PAGE_ELEMENT_COUNT" => 20,
		),
		false
	);
}

if (strlen($arResult["ERROR_MESSAGE"]) <= 0)
{
	?>
	<div id="warning_message">
		<?
		if (!empty($arResult["WARNING_MESSAGE"]) && is_array($arResult["WARNING_MESSAGE"]))
		{
			foreach ($arResult["WARNING_MESSAGE"] as $v)
				ShowError($v);
		}
		?>
	</div>
	<?

	$normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
	$normalHidden = ($normalCount == 0) ? 'style="display:none;"' : '';

	$delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
	$delayHidden = ($delayCount == 0) ? 'style="display:none;"' : '';

	$subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
	$subscribeHidden = ($subscribeCount == 0) ? 'style="display:none;"' : '';

	$naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
	$naHidden = ($naCount == 0) ? 'style="display:none;"' : '';

	foreach (array_keys($arResult['GRID']['HEADERS']) as $id)
	{
		$data = $arResult['GRID']['HEADERS'][$id];
		$headerName = (isset($data['name']) ? (string)$data['name'] : '');
		if ($headerName == '')
			$arResult['GRID']['HEADERS'][$id]['name'] = GetMessage('SALE_'.$data['id']);
		unset($headerName, $data);
	}
	unset($id);?>
	<div id="basket-desc">
		<ul>
			<li class="active">Редактирование корзины ></li>
			<li>Адрес, доставка и оплата ></li>
			<li>Спасибо!</li>
		</ul>
		<div class="basket__top-wrap">
			<div class="basket__top-col">
				<span class="icon icon-shopping-cart"></span>
				<?=$normalCount?> товаров
			</div>
			<div class="basket__top-col">
				<?
			if ($arParams["HIDE_COUPON"] != "Y")
			{
			?>
				<div class="bx_ordercart_coupon-">
					<input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();" placeholder="<?=GetMessage("STB_COUPON_PROMT")?>" />
					<span class="helper bx_ordercart_order_pay_left" id="coupons_block">
					<?
					
					if (!empty($arResult['COUPON_LIST']))
					{
						foreach ($arResult['COUPON_LIST'] as $oneCoupon)
						{
							$couponClass = 'disabled';
							switch ($oneCoupon['STATUS'])
							{
								case DiscountCouponsManager::STATUS_NOT_FOUND:
								case DiscountCouponsManager::STATUS_FREEZE:
									$couponClass = 'bad';
									break;
								case DiscountCouponsManager::STATUS_APPLYED:
									$couponClass = 'good';
									break;
							}
							?>
							<div class="bx_ordercart_coupon">
								<input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>">
								<div class="bx_ordercart_coupon_notes"><?
							if (isset($oneCoupon['CHECK_CODE_TEXT']))
							{
								echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
							}
							?></div>
								<span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span>
								</div><?
						}
						unset($couponClass, $oneCoupon);
					}
				?>	</span>
				</div>
				<?
					
			}
			?>	
			</div>
			<div class="basket__top-col">
				<textarea id="basket_note" placeholder="Добавить примечание к заказу" onchange="addComment()"></textarea>
			</div>
			<div class="basket__top-col">
				<div id="deliv_price" date-prie="<?=$arResult['DELIV_PRICE']?>">
				<? if($arResult['DELIV_PRICE'] > $arResult['allSum']) { ?>
					До бесплатной доставки осталось<div class="text-center"><b> <?=($arResult['DELIV_PRICE'] - $arResult['allSum'])?> </b> <div class="fa fa-ruble-sign"></div></div> <a href="/catalog/" class="block-link">Перейти в каталог</a>
				<? } else { ?>
					У вас бесплатная доставка!
				<? } ?>
				</div>
			</div>
			<div class="basket__top-col">
				<div class="basket-sum">Итого: <span id="allSum_FORMATED_HEAD"><?=$arResult["allSum_FORMATED"];?></span></div>
			</div>
			<div class="basket__top-col">
				<a href="javascript:void(0)" onclick="checkOut();" class="checkout btn btn-default btn-block"><?=GetMessage("SALE_ORDER")?></a>
			</div>
		</div>
		<?/*?>
		<div class="row">
			<div class="col-lg-1 col-md-2 col-sm-6">
				
			</div>
			<div class="col-lg-2 col-md-3 col-sm-6">	
			
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12">	
				
			</div>
			<div class="col-lg-2 col-md-4 col-sm-12">
				
			</div>
			<div class="col-lg-offset-0 col-lg-2 col-md-offset-3 col-md-3 col-sm-6">	
				
			</div>
			<div class="col-lg-2 col-md-3 col-sm-6">	
				
			</div>
		</div>
		<?*/?>
	</div>
	<div class="basket__clear-wrap">
		<div class="basket__clear-btn js-basket-clear-btn" data-modal-wrap=".js-clear-basket">Очистить корзину</div>
	</div>
	
	<div class="modal-window js-clear-basket clear-basket-modal-window" data-modal=".modal-bg">
		<div class="js-close-modal close-modal-btn"></div>
		<div class="clear-basket__wrap">
			<h2 class="section-h2">Вы действительно хотите удалить все товары из корзины?</h2>
			<div class="clear-basket__btns">
				<button class="btn white-btn js-clear-basket-confirm">Да</button>
			</div>
		</div>
	</div>
	
	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_form">
		<div id="basket_form_container">
			<div class="bx_ordercart <?=$templateData['TEMPLATE_CLASS']; ?>">
				<?
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_delayed.php");
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_subscribed.php");
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_not_available.php");
				?>
			</div>
		</div>
		<input type="hidden" id="BasketOrder" name="BasketOrder" value="BasketOrder" />
		<!-- <input type="hidden" name="ajax_post" id="ajax_post" value="Y"> -->
	</form>
	<?

	if($arParams['USE_GIFTS'] === 'Y' && $arParams['GIFTS_PLACE'] === 'BOTTOM')
	{
		?>
		<div style="margin-top: 35px;"><? $APPLICATION->IncludeComponent(
			"bitrix:sale.gift.basket",
			".default",
			array(
				"SHOW_PRICE_COUNT" => 1,
				"PRODUCT_SUBSCRIPTION" => 'N',
				'PRODUCT_ID_VARIABLE' => 'id',
				"PARTIAL_PRODUCT_PROPERTIES" => 'N',
				"USE_PRODUCT_QUANTITY" => 'N',
				"ACTION_VARIABLE" => "actionGift",
				"ADD_PROPERTIES_TO_BASKET" => "Y",

				"BASKET_URL" => $APPLICATION->GetCurPage(),
				"APPLIED_DISCOUNT_LIST" => $arResult["APPLIED_DISCOUNT_LIST"],
				"FULL_DISCOUNT_LIST" => $arResult["FULL_DISCOUNT_LIST"],

				"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_SHOW_VALUE"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],

				'BLOCK_TITLE' => $arParams['GIFTS_BLOCK_TITLE'],
				'HIDE_BLOCK_TITLE' => $arParams['GIFTS_HIDE_BLOCK_TITLE'],
				'TEXT_LABEL_GIFT' => $arParams['GIFTS_TEXT_LABEL_GIFT'],
				'PRODUCT_QUANTITY_VARIABLE' => $arParams['GIFTS_PRODUCT_QUANTITY_VARIABLE'],
				'PRODUCT_PROPS_VARIABLE' => $arParams['GIFTS_PRODUCT_PROPS_VARIABLE'],
				'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
				'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
				'SHOW_NAME' => $arParams['GIFTS_SHOW_NAME'],
				'SHOW_IMAGE' => $arParams['GIFTS_SHOW_IMAGE'],
				'MESS_BTN_BUY' => $arParams['GIFTS_MESS_BTN_BUY'],
				'MESS_BTN_DETAIL' => $arParams['GIFTS_MESS_BTN_DETAIL'],
				'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
				'CONVERT_CURRENCY' => $arParams['GIFTS_CONVERT_CURRENCY'],
				'HIDE_NOT_AVAILABLE' => $arParams['GIFTS_HIDE_NOT_AVAILABLE'],

				"LINE_ELEMENT_COUNT" => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
			),
			false
		); ?>
		</div><?
	}
	
	
	/*
	// Блок с упаковочными материалами
	$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"main_gurman", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISCOUNT_PERCENT_POSITION" => "top-right",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "shows",
		"ELEMENT_SORT_FIELD2" => "shows",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "asc",
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "Y",
		"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
		"IBLOCK_ID" => "13",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_TYPE_ID" => "product",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
		),
		"LAZY_LOAD" => "Y",
		"LINE_ELEMENT_COUNT" => 3,
		"LOAD_ON_SCROLL" => "Y",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "SIZE",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "SIZE",
			2 => "",
		),
		"OFFERS_LIMIT" => "",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "SIZE",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "shows",
		"OFFERS_SORT_FIELD2" => "shows",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "asc",
		"OFFER_ADD_PICT_PROP" => "",
		"OFFER_TREE_PROPS" => array(
			0 => "SIZE",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => 12,
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
		"PRODUCT_DISPLAY_MODE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE_MOBILE" => array(
		),
		"SECTION_CODE" => "",
		"SECTION_CODE_PATH" => "",
		"SECTION_ID" => "374",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "Y",
		"SEF_RULE" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "1",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "site",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"MESS_BTN_LAZY_LOAD" => "Показать ещё",
		"MESS_BTN_COMPARE" => "Сравнить",
		"LABEL_PROP_MOBILE" => "",
		"LABEL_PROP_POSITION" => "top-left"
	),
	false
);*/
	
	
	
	
	
	$arItemsId = array();

	foreach($arResult['GRID']['ROWS'] as $arItem){
		$arItemsId[] = $arItem['PRODUCT_ID'];
	}

	?>
	<div class="cart-bigdata">
	<?
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.bigdata.products", 
		".default", 
		array(
			"ID" => $arItemsId,
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_ID" => "5",
			"HIDE_NOT_AVAILABLE" => "N",
			"SHOW_DISCOUNT_PERCENT" => "Y",
			"PRODUCT_SUBSCRIPTION" => "N",
			"SHOW_NAME" => "Y",
			"SHOW_IMAGE" => "Y",
			"MESS_BTN_BUY" => "Купить",
			"MESS_BTN_DETAIL" => "Подробнее",
			"MESS_BTN_SUBSCRIBE" => "Подписаться",
			"PAGE_ELEMENT_COUNT" => "30",
			"LINE_ELEMENT_COUNT" => "6",
			"TEMPLATE_THEME" => "blue",
			"DETAIL_URL" => "/#SECTION_CODE#/#ELEMENT_CODE#/",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_GROUPS" => "Y",
			"SHOW_OLD_PRICE" => "N",
			"PRICE_CODE" => array(
				0 => "BASE",
			),
			"SHOW_PRICE_COUNT" => "1",
			"PRICE_VAT_INCLUDE" => "Y",
			"CONVERT_CURRENCY" => "Y",
			"BASKET_URL" => "/cart/",
			"ACTION_VARIABLE" => "action",
			"PRODUCT_ID_VARIABLE" => "id",
			"ADD_PROPERTIES_TO_BASKET" => "Y",
			"PRODUCT_PROPS_VARIABLE" => "prop",
			"PARTIAL_PRODUCT_PROPERTIES" => "N",
			"USE_PRODUCT_QUANTITY" => "N",
			"SHOW_PRODUCTS_2" => "Y",
			"CURRENCY_ID" => "RUB",
			"PROPERTY_CODE_2" => array(
				0 => "NEWPRODUCT",
				1 => "MANUFACTURER",
				2 => "MATERIAL",
				3 => "COLOR",
				4 => "",
			),
			"CART_PROPERTIES_2" => array(
				0 => "NEWPRODUCT",
				1 => "",
			),
			"ADDITIONAL_PICT_PROP_2" => "MORE_PHOTO",
			"LABEL_PROP_2" => "NEWPRODUCT",
			"PROPERTY_CODE_3" => array(
				0 => "SIZES_SHOES",
				1 => "SIZES_CLOTHES",
				2 => "COLOR_REF",
				3 => "",
			),
			"CART_PROPERTIES_3" => array(
				0 => "SIZES_SHOES",
				1 => "SIZES_CLOTHES",
				2 => "COLOR_REF",
				3 => "",
			),
			"ADDITIONAL_PICT_PROP_3" => "MORE_PHOTO",
			"OFFER_TREE_PROPS_3" => array(
				0 => "SIZES_SHOES",
				1 => "SIZES_CLOTHES",
			),
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"COMPONENT_TEMPLATE" => ".default",
			"RCM_TYPE" => "bestsell",
			"SHOW_FROM_SECTION" => "N",
			"SECTION_ID" => "",
			"SECTION_CODE" => "",
			"SECTION_ELEMENT_ID" => "",
			"SECTION_ELEMENT_CODE" => "",
			"DEPTH" => "2",
			"SHOW_PRODUCTS_4" => "N",
			"PROPERTY_CODE_4" => array(
			),
			"CART_PROPERTIES_4" => array(
			),
			"ADDITIONAL_PICT_PROP_4" => "",
			"LABEL_PROP_4" => "-",
			"SHOW_PRODUCTS_5" => "Y",
			"PROPERTY_CODE_5" => array(
				0 => "STIKERS",
				1 => "",
			),
			"CART_PROPERTIES_5" => array(
				0 => "",
				1 => "",
			),
			"ADDITIONAL_PICT_PROP_5" => "CERTIF",
			"LABEL_PROP_5" => "-",
			"PROPERTY_CODE_6" => array(
				0 => "",
				1 => "",
			),
			"CART_PROPERTIES_6" => array(
				0 => "",
				1 => "",
			),
			"ADDITIONAL_PICT_PROP_6" => "",
			"OFFER_TREE_PROPS_6" => array(
			),
			"OTHER_TITLE" => "Возможно, вы забыли купить",
			"SLIDER_RESPONSIVE" => array("1600" => 6, "1199" => 4, "991" => 3, "768" => 2, "480" => 1)
		),
			false
	);
	?>
	</div>
	<?
}
else
{
	?>
	<div class="text-center">
		<? ShowError($arResult["ERROR_MESSAGE"]); ?>
	</div>
	<?
}
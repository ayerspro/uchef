$(document).ready(function(){
	
	$.fn.insertAt = function(index, element) {
	  var lastIndex = this.children().length;
	  if (index < 0) {
		index = Math.max(0, lastIndex + 1 + index);
	  }
	  this.append(element);
	  if (index < lastIndex) {
		this.children().eq(index).before(this.children().last());
	  }
	  return this;
	}
	
	
	$('form[name="form1"]').submit(function(){
		var $this = $(this);
		var data = $this.serialize();
		
		$.ajax({
			url: '/ajax/edit_main_profile.php',
			data: data,
    		processData: false,
			dataType: 'json',
			type: 'POST',
			success: function(resp) {
				console.log(resp);
				
				if(resp.strProfileError != '') {
					$('.texterror').remove();
					$this.find('.panel-o').append('<font class="texterror"><p>'+resp.strProfileError+'</p></font>');
				} else {
					$('.success, .texterror').remove();
					$this.find('.suc').append('<div class="success">Изменения сохранены</div>');
					setTimeout(function(){
						$('.success').fadeOut(function(){
							$('.success').remove();
						});
					}, 2000);
				}
			}
		});
		return false;
	});
	function sendAjax($this, $favorite = false) {
		var data = $this.serialize();
		data+=($favorite)?'&favorite=Y':'';
		//console.log(data);
		$.ajax({
			url: '/ajax/main_profile_result_form.php',
			data: data,
    		processData: false,
			dataType: 'json',
			type: 'POST',
			success: function(resp) {
				console.log(resp);

				if(resp.SET_CITY){
					$this.find('input[name="town"]').css('display', 'block');
				}
				else{
					$this.find('input[name="town"]').css('display', 'none');
				}
				
				if(resp.ERROR != '') {
					$('.texterror').remove();
					for(var i = 0; i < resp.ERROR.length; i++){
						$this.find('.panel-o').append('<font class="texterror"><p>'+resp.ERROR[i]+'</p></font>');
					}
				} 
				else {
					$('.success, .texterror').remove();
					$this.find('.suc').append('<div class="success">Изменения сохранены</div>');
						
					setTimeout(function(){
						$('.success').fadeOut(function(){
							$('.success').remove();
						});
					}, 2000);
				}
			},
			error: function(err){
				console.log(err);
			}
		});
	}
	$(document).on('submit', '.form-deliv form', function(){
		var $this = $(this);
		var $favorite = $this.find('.make-o.active');
		if($favorite.length > 0)
			sendAjax($this, true);
		else sendAjax($this, false);
		return false;
	});
	$(document).on('click', '.form-deliv form .cansel-deliv-form', function(){
		var $this = $(this).parents('form');
		var data = $this.serialize();
		$.ajax({
			url: '/ajax/delete_profile_result_form.php',
			data: data,
    		processData: false,
			dataType: 'json',
			type: 'POST',
			success: function(resp) {
				console.log(resp);
				if(resp.ERROR != '') {
					$('.texterror').remove();
					for(var i = 0; i < resp.ERROR.length; i++){
						$this.find('.panel-o').append('<font class="texterror"><p>'+resp.ERROR[i]+'</p></font>');
					}
				} else {
					$('.success, .texterror').remove();
					$this.fadeOut(function(){
						$this.remove();
						if($('.form-deliv form').length == 0) {
							$('.addForm').click();
							$('.make-o').addClass('active');
						}
					});
				}
			}
		});
		return false;
	});
	$(document).on('change', 'select[name=recomendation_shef]', function(){
		var recombl = $('.recom-bl');
		var $this = $(this);
		var data = 'recomendation_shef='+$this.val();
		$.ajax({
			url: '/ajax/recomendation.php',
			data: data,
    		processData: false,
			dataType: 'json',
			type: 'POST',
			success: function(resp) {
				console.log(resp);
				if(resp.hasOwnProperty('ERROR')) {
					alert(resp.ERROR);
				} else {
					recombl.empty().append('<div class="success" style="font-size: 13px;">Эти изменения будут отображены на <a href="/#formx" target="_blank" style="color: #402f2b;text-decoration: underline;">главной странице</a><br/>Блок &laquo;Советуем попробовать&raquo;</div>');
					// $this.fadeOut(function(){
					// 	$this.remove();
					// });
				}
			}
		});
	});
	$(document).on('change', '.pred-email', function(){
		var action;
		
		if($(this).is(':checked')){
			action = "SUBSCRIBE";
		}
		else{
			action = "UNSUBSCRIBE";
		}

		var data = {"ACTION":action,"SUBSCRIBE_LIST":$(this).val(), "EMAIL":$('form[name="form1"] input[name="EMAIL"]').val()};

		$.ajax({
			url: '/ajax/mailing-subscription.php',
			data: data,
			dataType: 'json',
			type: 'POST',
			success: function(resp) {
				console.log(resp);
			},
			error: function(error){
				console.log()
			}
		});
	});
	
	
	$(document).on('click', '.make-o', function(event) {
		var $make = $('.make-o.active');
		var $this = $(this);
		var $form = $this.parents('form');
		var $formID = $form.find('.num-deliv-form').text();
		var $parent =  $form.parent();
		
		$('.form-deliv form').each(function(){
			var $curFormId = $(this).find('.num-deliv-form').text();
			
			if($curFormId == $formID){
				$curFormId = 1;
				$(this).find('.make-o').addClass('active');
				$(this).find('.make-o').html('<i class="fa fa-check" aria-hidden="true"></i> Основной адрес');
			}
			else{
				if($curFormId < $formID){
					$curFormId++;
				}
				
				$(this).find('.make-o').removeClass('active');
				$(this).find('.make-o').html('<i class="fa fa-check" aria-hidden="true"></i> Сделать основным адресом');
			}
		
			$(this).find('.num-deliv-form').text($curFormId);
			$(this).find('input[name="number_form"]').val('form_' + $curFormId);
			
			$parent.insertAt($curFormId - 1, $(this));
			
			
		});
		
		
		$('.form-deliv form').each(function(){
			if($(this).find('.num-deliv-form').text() == 1){
				sendAjax($(this), true);
				
			}
			else{
				sendAjax($(this), false);
				
			}
		});
			
		
		/*if($this.hasClass('active') && $('.make-o').length > 1) {
			sendAjax($form, false);
			$make.removeClass('active');
			$this.removeClass('active');
		} else if($('.make-o').length > 1) { 
			sendAjax($make.parents('form'), false);
			$make.removeClass('active');
			sendAjax($form, true);
			$this.addClass('active');
		}*/
	});
	var $val = 0;
	$(document).on('focus', '.form-deliv form input, .form-deliv form textarea', function(event) {
		var $this = $(this);
		$($this.parents('form')).bind('mouseleave.myProfile', function(e){
			$this.parents('form').submit();
			$($this.parents('form')).unbind('mouseleave.myProfile');
		});
	});
	$('.addForm').on('click', function(){
		$
		
		
		$.ajax({
			url: '/ajax/get_profile_result_form.php',
			data: {num_deliv_form: ($('.form-deliv form').length+1)},
			type: 'POST',
			success: function(resp) {
				$('.form-deliv .wrap-o').append(resp);
				$('form.new_form').fadeIn(function(){
					$(this).removeClass('new_form');
				});
			}
		});
		
		
		
		
		
		/*$form = '<form class="new_form" style="display: none;">';
		$form += '<input type="hidden" name="number_form" value="form_'+($('.form-deliv form').length+1)+'">';
		$form += '<div class="panel-o col-xs-12"></div>';
		$form += '<div class="make-o"><i class="fa fa-check" aria-hidden="true"></i> Сделать основным адресом</div>';
		$form += '<div class="num-deliv-form">'+($('.form-deliv form').length+1)+'</div>';
		$form += '<div class="cansel-deliv-form"><i class="fa fa-ban" aria-hidden="true"></i></div>';
		$form += '<div class="form-group width-w">';
			$form += '<div class="col-sm-12">';
				$form += '<input type="text" name="town" placeholder="Город" value="" />';
			$form += '</div>';
		$form += '</div>';
		$form += '<div class="form-group width-w">';
			$form += '<div class="col-sm-6 col-p-l">';
				$form += '<input type="text" name="street" placeholder="Улица" value="" />';
			$form += '</div>';
			$form += '<div class="col-sm-6 col-p-r">';
				$form += '<input type="text" name="num_home" placeholder="Номер дома" value="" />';
			$form += '</div>';
		$form += '</div>';
		$form += '<div class="form-group width-w">';
			$form += '<div class="col-sm-12">';
				$form += '<input type="text" name="flat" placeholder="Квартира" value="" />';
			$form += '</div>';
		$form += '</div>';
		$form += '<div class="form-group width-w">';
			$form += '<div class="col-sm-12">';
				$form += '<input type="text" name="phone" placeholder="Телефон получателя заказа" value="" />';
			$form += '</div>';
		$form += '</div>';
		$form += '<div class="form-group width-w">';
			$form += '<div class="col-sm-12">';
				$form += '<input type="text" name="fio" placeholder="ФИО получателя" value="" />';
			$form += '</div>';
		$form += '</div>';
		$form += '<div class="form-group width-w">';
			$form += '<div class="col-sm-12">';
				$form += '<textarea name="txt" placeholder="Получатель - другой человек? К дому сложно подъехать? Пишите эти и другие важные примечания в этой форме"></textarea>';
			$form += '</div>';
		$form += '</div>';
		$form += '<div class="suc"></div>';
		$form += '</form>';*/

		/*$('.form-deliv .wrap-o').append($form);
		$('form.new_form').fadeIn(function(){
			$(this).removeClass('new_form');
		});*/
	});
	
	$('.sale_profile input[type="text"], .sale_profile textarea').change(function(){
		var newValues = { "PROFILE_TYPE": $('.sale_profile input[name="PROFILE_TYPE"]').val()};
		
		$('.sale_profile input[type="text"], .sale_profile textarea').each(function(){
			newValues[$(this).attr('name')] = $(this).val();
			
		});
		
			
		
		console.log(newValues);
		$('.sale_profile input[name="UF_SALE_PROFILE"]').val(JSON.stringify(newValues));
		
		
		
	});
	
	
	
	
});
<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Sender\Subscription;
use Bitrix\Sale\Location\LocationTable;

?>
<section style="padding: 15px 0;">
	<h1 class="text-center catalog-title">Личные данные</h1>
</section>

<div class="bx_profile">
	<div class="col-lg-3 col-md-4">
		<div class="titles-leader">
			Личные данные
		</div>
	<form method="post" name="form1" action="<?=$APPLICATION->GetCurUri()?>" enctype="multipart/form-data" role="form">
		<div class="panel-o col-sm-12">
			<?
		ShowError($arResult["strProfileError"]);
	
		?>
	</div>
		<?=$arResult["BX_SESSION_CHECK"]?>
		<input type="hidden" name="lang" value="<?=LANG?>" />
		<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
		<input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
		<div class="main-profile-block-shown" id="user_div_reg">
			<?
			if (!in_array(LANGUAGE_ID,array('ru', 'ua')))
			{
				?>
				<div class="form-group width-w">
					<div class="col-sm-12">
						<input type="text" name="TITLE" maxlength="50" id="main-profile-title" value="<?=$arResult["arUser"]["TITLE"]?>" />
					</div>
				</div>
				<?
			}
			?>
			<div class="form-group width-w">
				<div class="col-sm-12">
					<input type="text" name="NAME" maxlength="50" id="main-profile-name" placeholder="Имя и фамилия" value="<?=$arResult["arUser"]["NAME"]?>" />
				</div>
			</div>

			<div class="form-group width-w">
				<div class="col-sm-12">
					<input type="text" name="PERSONAL_BIRTHDAY" maxlength="50" placeholder="Дата рождения" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" />
				</div>
			</div>
			
			<div class="form-group width-w">
				<div class="col-sm-12">
					<input type="text" name="EMAIL" maxlength="50" id="main-profile-email" placeholder="E-mail" value="<?=$arResult["arUser"]["EMAIL"]?>" />
				</div>
			</div>
		</div>
		
		
		<div class="col-sm-12 button-panel width-w" style="margin-bottom:30px;">
			<input type="submit" data-toggle="collapse" data-target="#password-change" onclick="return false;" style="margin-bottom:10px;" value="Изменить пароль" />
			
			<div id="password-change" class="collapse">
				<div class="form-group width-w">
					<input type="text" name="NEW_PASSWORD" maxlength="50" placeholder="Новый пароль" value="<?=$arResult["arUser"]["NEW_PASSWORD"]?>" />
				</div>
				<div class="form-group width-w">
					<input type="text" name="NEW_PASSWORD_CONFIRM" maxlength="50" placeholder="Подтвердите пароль" value="<?=$arResult["arUser"]["NEW_PASSWORD_CONFIRM"]?>" />
				</div>
			</div>
		</div>
		
		<div class="sale_profile"> 
			<input type="hidden" name="UF_SALE_PROFILE" value='<?=$arResult['arUser']['~UF_SALE_PROFILE']?>' />
			
		
			<? if(!empty($arResult['arUser']['~UF_SALE_PROFILE'])){
				$json = json_decode($arResult['arUser']['~UF_SALE_PROFILE']);
				
				 if($json->PROFILE_TYPE == 2){
					?>
					<div class="titles-leader">Реквизиты компании</div>
					
					<input type="hidden" name="PROFILE_TYPE" value="<?=$json->PROFILE_TYPE?>" />
					
					<?
					
					if(CModule::IncludeModule("sale")){
						$arProperties = array();
						
						$orderPropertyList = CSaleOrderProps::GetList(
													array("SORT" => "ASC", "NAME" => "ASC"),
													array(
														"PERSON_TYPE_ID" => $json->PROFILE_TYPE,
														"USER_PROPS" => "Y", "ACTIVE" => "Y", "UTIL" => "N", "PROPS_GROUP_ID" => 3
													),
													false,
													false,
													array("ID", "NAME", "TYPE", "REQUIED", "MULTIPLE", "IS_LOCATION", "PROPS_GROUP_ID", "IS_EMAIL", "IS_PROFILE_NAME", "IS_PAYER", "IS_LOCATION4TAX", "CODE", "SORT")
												);
						
						
						while($orderProperty = $orderPropertyList->GetNext()){
							$arProperties[$orderProperty['ID']] = $orderProperty;
							$propName = "PROFILE_PROP_".$orderProperty['ID'];
							$propValue = "";
							
							if($json->{"PROFILE_PROP_".$orderProperty['ID']}){
								$propValue = $json->{"PROFILE_PROP_".$orderProperty['ID']};
							}
							
							
							
							switch($orderProperty['TYPE']){
									case 'TEXT':
										?>
											<div class="form-group width-w">
												<div class="col-sm-12">
													<input type="text" name="<?=$propName?>" maxlength="50" placeholder="<?=$orderProperty['NAME']?>" value="<?=$propValue?>" />
												</div>
											</div>
										<?
										break;
									case 'TEXTAREA':
										?>
											<div class="form-group width-w">
												<div class="col-sm-12">
													<textarea name="<?=$propName?>" maxlength="255" placeholder="<?=$orderProperty['NAME']?>"><?=$propValue?></textarea>
												</div>
											</div>
										<?
										break;
							}	
	
						}
			
					}

				}
			}?>
		
		</div>
		
		
		<div class="col-sm-12 button-panel width-w">
			<input type="submit" value="Сохранить">
			<div class="suc"></div>
			<input type="hidden" name="save" value="Y">
			&nbsp;
			<input type="submit" style="display: none;" class="btn btn-themes btn-default btn-md" value="<?echo GetMessage("MAIN_RESET")?>">
			<br/><br/>
			<div class="info-o">
				<span>Аккаунт соц. сетей</span>
				<br/><br/>
				<?$APPLICATION->IncludeComponent(
					"bitrix:socserv.auth.split", 
					"socserv", 
					array(
						"ALLOW_DELETE" => "Y",
						"SHOW_PROFILES" => "Y",
						"COMPONENT_TEMPLATE" => "socserv"
					),
					false
				);?>
				<?/*?>
				<div class="soc">
					<a href="#" class="vk"></a>
					<a href="#" class="fb"></a>
					<a href="#" class="ok"></a>
				</div>
				<div class="txt-o">
					<p>Свяжите Ваши аккаунты в соцсетях с аккаунтом на Chef Gourmet.</p>
					<p>Это значительно ускорит вход на сайт, комментирование и многое другое</p>
				</div>
				<?*/?>
			</div>
		</div>
	</form>
	<div class="clearfix"></div>
	</div>
	<div class="col-lg-6 col-md-8">
		<div class="titles-leader">
			Адрес доставки
		</div>
		<div class="form-deliv">
			<div class="wrap-o">
				<?
				if(!empty($arResult['arUser']['~UF_COUNT_FORM'])):
				$forms = json_decode($arResult['arUser']['~UF_COUNT_FORM']);
				$c_form = 1;
				foreach ($forms as $form):?>
				<form>
					<input type="hidden" name="number_form" value="<?=$form->number_form?>">
					<input type="hidden" name="profile_id" value="<?=$form->profile_id?>">
					<div class="panel-o col-xs-12"></div>
					<div class="make-o<?if($c_form == 1):?> active<?endif;?>"><i class="fa fa-check" aria-hidden="true"></i> <?if($c_form == 1):?> Основной адрес<?else:?>Сделать основным адресом<?endif;?></div>
					<div class="num-deliv-form"><?=$c_form?></div>
					<div class="cansel-deliv-form"><i class="fa fa-ban" aria-hidden="true"></i></div>
					<div class="form-group width-w">
						<div class="col-sm-12">
							<?
							if(CModule::IncludeModule("sale")){
								if(!empty($form->location)){
									$dbLocation = LocationTable::getById($form->location, [
											'select' => [
												'ID',
												'CODE',
												'NAME.NAME',
												'PARENT.NAME.NAME',
											],
											'filter' => [
												'=NAME.LANGUAGE_ID' => LANGUAGE_ID,
												'=PARENT.NAME.LANGUAGE_ID' => LANGUAGE_ID,
											]
										]);

									if($arLocation = $dbLocation->Fetch()){
										if(!empty($arLocation["CITY_ID"]) && !empty($form->town)){
											$form->town = "";
										}
									}	
								}
								
								
								$locationTemplate = ($arParams['USE_AJAX_LOCATIONS'] !== 'Y') ? "popup" : "";
								
								CSaleLocation::proxySaleAjaxLocationsComponent(
									array(
										"AJAX_CALL" => "Y",
										"CITY_OUT_LOCATION" => "Y",
										"COUNTRY_INPUT_NAME" => $form->location.'_COUNTRY',
										"CITY_INPUT_NAME" => 'location',
										"LOCATION_VALUE" => $form->location,
									),
									array(
									),
									$locationTemplate,
									true,
									'location-block-wrapper'
								);
								
							}?>
						</div>
					</div>
					<div class="form-group width-w">
						<div class="col-sm-12">
							<input type="text" name="town" placeholder="Город" value="<?=$form->town?>" <?if(empty($form->town)):?>style="display:none;"<?endif;?> />
						</div>
					</div>
					<div class="form-group width-w">
						<div class="col-sm-6 col-p-l">
							<input type="text" name="street" placeholder="Улица" value="<?=$form->street?>" />
						</div>
						<div class="col-sm-6 col-p-r">
							<input type="text" name="num_home" placeholder="Номер дома" value="<?=$form->num_home?>" />
						</div>
					</div>
					<div class="form-group width-w">
						<div class="col-sm-12">
							<input type="text" name="flat" placeholder="Квартира" value="<?=$form->flat?>" />
						</div>
					</div>
					<div class="form-group width-w">
						<div class="col-sm-12">
							<input type="text" class="form_phone" name="phone" placeholder="Телефон получателя заказа" value="<?=$form->phone?>" />
						</div>
					</div>
					<div class="form-group width-w">
						<div class="col-sm-12">
							<input type="text" name="fio" placeholder="ФИО получателя" value="<?=$form->fio?>" />
						</div>
					</div>
					<div class="form-group width-w">
						<div class="col-sm-12">
							<textarea name="txt" placeholder="Получатель - другой человек? К дому сложно подъехать? Пишите эти и другие важные примечания в этой форме"><?=$form->txt?></textarea>
						</div>
					</div>
					<div class="suc"></div>
				</form>

				<?$c_form++;endforeach;?>
				<?else:?>
					<form>
						<input type="hidden" name="number_form" value="<?=$form->number_form?>">
						<input type="hidden" name="profile_id" value="">
						<div class="panel-o col-xs-12"></div>
						<div class="make-o active"><i class="fa fa-check" aria-hidden="true"></i> Сделать основным адресом</div>
						<div class="num-deliv-form">1</div>
						<div class="cansel-deliv-form"><i class="fa fa-ban" aria-hidden="true"></i></div>
						<div class="form-group width-w">
							<div class="col-sm-12">
								<!--<input type="text" name="town" placeholder="Город" value="" />-->
								<?
								if(CModule::IncludeModule("sale")){
									$locationTemplate = ($arParams['USE_AJAX_LOCATIONS'] !== 'Y') ? "popup" : "";
									 //echo'<pre>';print_r($locationTemplate);echo'</pre>';
									CSaleLocation::proxySaleAjaxLocationsComponent(
										array(
											"AJAX_CALL" => "Y",
											'CITY_OUT_LOCATION' => 'Y',
											'CITY_INPUT_NAME' => 'location',
										),
										array(
										),
										$locationTemplate,
										true,
										'location-block-wrapper'
									);

								}?>
								
								
							</div>
						</div>
						<div class="form-group width-w">
							<div class="col-sm-12">
								<input type="text" name="town" placeholder="Город" value="" style="display:none;" />
							</div>
						</div>
						<div class="form-group width-w">
							<div class="col-sm-6 col-p-l">
								<input type="text" name="street" placeholder="Улица" value="" />
							</div>
							<div class="col-sm-6 col-p-r">
								<input type="text" name="num_home" placeholder="Номер дома" value="" />
							</div>
						</div>
						<div class="form-group width-w">
							<div class="col-sm-12">
								<input type="text" name="flat" placeholder="Квартира" value="" />
							</div>
						</div>
						<div class="form-group width-w">
							<div class="col-sm-12">
								<input type="text" class="form_phone" name="phone" placeholder="Телефон получателя заказа" value="" />
							</div>
						</div>
						<div class="form-group width-w">
							<div class="col-sm-12">
								<input type="text" name="fio" placeholder="ФИО получателя" value="" />
							</div>
						</div>
						<div class="form-group width-w">
							<div class="col-sm-12">
								<textarea name="txt" placeholder="Получатель - другой человек? К дому сложно подъехать? Пишите эти и другие важные примечания в этой форме"></textarea>
							</div>
						</div>
						<div class="suc"></div>
					</form>
				<?endif;?>
			</div>
			<div class="col-sm-12 button-panel date-o width-w">
				<input type="submit" class="addForm" value="Добавить другой адрес">
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-12">
		<form>
			<div class="row">
				<div class="col-lg-12 col-md-4">
					<div class="titles-leader">
						Рекомендации
					</div>
					<div class="form-group width-w">
						<div class="col-sm-12 select-s">
							<?$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>5, "CODE"=>"GURMAN"));?>
							<select name="recomendation_shef">
								<?while($enum_fields = $property_enums->GetNext()){?>
									<option id="<?=$enum_fields["ID"]?>" value="<?=$enum_fields["ID"]?>" <?if(((int)$arResult['arUser']['UF_RECOMENDATION'] == $enum_fields["ID"])) echo "selected"?>><?=$enum_fields["VALUE"]?></option>
								<?}?>
							</select>
							<div class="recom-bl"></div>
						</div>
					</div>

				</div>
				<div class="col-lg-12 col-md-8">
				
					<div class="form-group width-w">
						<div class="col-sm-12">
							<div class="txt-o">
								<p>Вы будете получать рекомендации, соответствующие Вашему вкусу</p>
							</div>
							<div class="titles-leader">
								Настройка уведомлений
							</div>
							
							<?
								$arFilter = array("SITE_ID" => SITE_ID, "IS_PUBLIC" => "Y");
								$mailingList = Subscription::getMailingList($arFilter);

								

							?>
							<?
							
							foreach($mailingList as $mailing):?>
							
								<?
									$arFilter = array("EMAIL" => $arResult["arUser"]["EMAIL"], "MAILING_ID" => $mailing['ID'], "TEST"=>"Y");
									$mailList = Subscription::getList($arFilter);
									//echo'<pre>';print_r($mailList);echo'</pre>';
								?>
							
							
								<div class="form-group width-w <?if(count($mailList) > 0):?>check<?endif;?>">
									<div class="col-sm-12">
										<label for="pred-email"><input type="checkbox" id="pred-email-<?=$mailing['ID']?>" name="pred-email-<?=$mailing['ID']?>" value="<?=$mailing['ID']?>" class="pred-email" <?if(count($mailList) > 0):?>checked<?endif;?>><?=$mailing['NAME']?></label>
									</div>
								</div>
							<?endforeach;?>
						</div>
					</div>
				
				
				</div>
			
			</div>
		</form>
		
	</div>
</div>




<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
/** @global CMain $APPLICATION */

$frame = $this->createFrame()->begin("");

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

$injectId = $arParams['UNIQ_COMPONENT_ID'];

if (isset($arResult['REQUEST_ITEMS']))
{
	// code to receive recommendations from the cloud
	CJSCore::Init(array('ajax'));

	// component parameters
	$signer = new \Bitrix\Main\Security\Sign\Signer;
	$signedParameters = $signer->sign(
		base64_encode(serialize($arResult['_ORIGINAL_PARAMS'])),
		'bx.bd.products.recommendation'
	);
	$signedTemplate = $signer->sign($arResult['RCM_TEMPLATE'], 'bx.bd.products.recommendation');

	?>

	<span id="<?=$injectId?>"></span>

	<script type="text/javascript">
		BX.ready(function(){
			bx_rcm_get_from_cloud(
				'<?=CUtil::JSEscape($injectId)?>',
				<?=CUtil::PhpToJSObject($arResult['RCM_PARAMS'])?>,
				{
					'parameters':'<?=CUtil::JSEscape($signedParameters)?>',
					'template': '<?=CUtil::JSEscape($signedTemplate)?>',
					'site_id': '<?=CUtil::JSEscape(SITE_ID)?>',
					'rcm': 'yes'
				}
			);
		});
	</script>
	<?
	$frame->end();
	return;

	// \ end of the code to receive recommendations from the cloud
}

$arItemsID = array();

foreach($arResult['ITEMS'] as $item) {
	
	$arItemsID[] = $item['ID'];
}

$arItemsID = getProductsCurrentRegion(array("ID" => $arItemsID));

foreach($arResult['ITEMS'] as $k => $item) {
	
	if(!in_array($item['ID'], $arItemsID)) {
		
		unset($arResult['ITEMS'][$k]);
	}
}

// regular template then
// if customized template, for better js performance don't forget to frame content with <span id="{$injectId}_items">...</span> 

if (!empty($arResult['ITEMS']))
{
	
	?>

	<div id="<?=$injectId?>_items">
		<span id="<?=$injectId?>_line_count" style="display:none;"><?=$arParams['LINE_ELEMENT_COUNT']?></span>
		<span id="<?=$injectId?>_slider_responsive" style="display:none;"><?=json_encode($arParams['SLIDER_RESPONSIVE'])?></span>
		<h2 class="title-element-bottom" style="text-align: left;"><?=(isset($arParams['OTHER_TITLE'])? $arParams['OTHER_TITLE']: "С этим товаром покупают")?></h2>
		<div class="products-items">
			<?
			$arrFavorites = unserialize($APPLICATION->get_cookie("ANONIM_PROD_FAVORITES"));
			$arBasketItems = array();
			$arBasketItemsAll = array();

			$dbBasketItems = CSaleBasket::GetList(
				array(
						"NAME" => "ASC",
						"ID" => "ASC"
					),
				array(
						"FUSER_ID" => CSaleBasket::GetBasketUserID(),
						"LID" => SITE_ID,
						"ORDER_ID" => "NULL"
					),
				false,
				false,
				array()
			);
				
			while ($arItems = $dbBasketItems->Fetch()) {
				
				if (strlen($arItems["CALLBACK_FUNC"]) > 0) {
					
					CSaleBasket::UpdatePrice($arItems["ID"],
											 $arItems["CALLBACK_FUNC"],
											 $arItems["MODULE"],
											 $arItems["PRODUCT_ID"],
											 $arItems["QUANTITY"]);
					$arItems = CSaleBasket::GetByID($arItems["ID"]);
				}

				$arBasketItemsAll[$arItems['PRODUCT_ID']] = $arItems;
				$arBasketItems[] = $arItems['PRODUCT_ID'];
			}

				
			foreach ($arResult['ITEMS'] as $arItem) {

				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
				$strMainID = $this->GetEditAreaId($arItem['ID']);
				
				$arItem['ITEM_MEASURE_RATIO_SELECTED'] = 'select';
				
				if(isset($arItem['OFFERS']) && count($arItem['OFFERS']) > 0 && isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']])){
					
					$arItem['ITEM_MEASURE_RATIOS'][$arItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'] = $arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['CATALOG_MEASURE_RATIO'];
					$arItem['ITEM_MEASURE']['TITLE'] = $arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['CATALOG_MEASURE_TITLE'];
				} 
				else{
					
					$arItem['ITEM_MEASURE_RATIOS'][$arItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'] = $arItem['CATALOG_MEASURE_RATIO'];
					$arItem['ITEM_MEASURE']['TITLE'] = $arItem['CATALOG_MEASURE_NAME'];
				}

				$APPLICATION->IncludeComponent(
					'bitrix:catalog.item',
					'grid',
					array(
						'RESULT' => array(
							'ITEM' => $arItem,
							'AREA_ID' => $strMainID,
							'BASKET_ITEMS' => $arBasketItems,
							'BASKET_ITEMS_ALL' => $arBasketItemsAll,
							'FAVORITES_ITEMS' => $arrFavorites,
						),
						'PARAMS' => $arParams
							+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$arItem['IBLOCK_ID']])
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);
			}
		?>
		</div>
	</div>
<?
}

$frame->end();
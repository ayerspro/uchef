<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$currentUrl = $APPLICATION->GetCurPage(false);

foreach($arResult as $key => $arItem){
	$arLink = explode("?", $arItem["LINK"]);
	$iMax = count($arLink);
	if($iMax == 2){
		$arResult[$key]["LINK"] = $currentUrl . "?" . $arLink[1];
	}
}
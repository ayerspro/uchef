<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?include 'functions.php';?>

<?if (!empty($arResult)):?>
	<div class="cat-block-menu">
		<?foreach($arResult as $key => $arItem):?>
			<a href="<?=$arItem["LINK"]?>" class="<?if ($key == 0):?>deposit<?endif?> <?if ($arItem["SELECTED"]):?>active<?endif?>">
				<span class="icon" <?if ($key != 0):?>style="background: url(<?=$arItem['DETAIL_PICTURE']['SRC']?>) center no-repeat;"<?endif?>></span>
				<?=$arItem["TEXT"]?>
			</a>
		<? endforeach; ?>
	</div>
<? endif; ?>

<?

if($arParams['IS_SALES'] == 'Y') {
	
	foreach($arResult as $key => &$item) {
		
		if(!in_array($item['PARAMS']['SECTION_ID'], $arParams['SALES_SECTIONS'])) {
			
			unset($arResult[$key]);
			
		} else {
			
			$item['LINK'] = $arParams['SALES_PAGE'] . '' . $item['PARAMS']['SECTION_ID'] . '/';
		}
	}
	
	unset($item);
}

//print_r($arResult);

?>
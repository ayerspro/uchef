<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$arItem = $arResult['ITEM'];
$areaId = $arResult['AREA_ID'];

$imgTitle = isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
: $arItem['NAME'];

$imgAlt = isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'] != ''
? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']
: $arItem['NAME'];

$minPrice = $arItem['MIN_PRICE'];
$itemID = $arItem['ID'];

$arItemRatio = array(
	'ITEM_MEASURE_RATIOS' => $arItem['ITEM_MEASURE_RATIOS'],
	'ITEM_MEASURE_RATIO_SELECTED' => $arItem['ITEM_MEASURE_RATIO_SELECTED'],
	'ITEM_MEASURE' => $arItem['ITEM_MEASURE'],
);

if($arItem['OFFERS']) {
	
	$arCurOffer = $arItem['OFFERS'][$arItem['OFFER_ID_SELECTED']];
	
	if($arCurOffer) {
		
		$arItemRatio = array(
			'ITEM_MEASURE_RATIOS' => $arCurOffer['ITEM_MEASURE_RATIOS'],
			'ITEM_MEASURE_RATIO_SELECTED' => $arCurOffer['ITEM_MEASURE_RATIO_SELECTED'],
			'ITEM_MEASURE' => $arCurOffer['ITEM_MEASURE'],
		);
	}
}

$measure = $arItemRatio['ITEM_MEASURE_RATIOS'][$arItemRatio['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];

// print_r($arItem);

if($measure && $measure < 1) {
	
	$minPrice['DISCOUNT_VALUE'] = $minPrice['DISCOUNT_VALUE'] * $measure;
	$minPrice['VALUE'] = $minPrice['VALUE'] * $measure;
}

if(isset($arItem['OFFERS']) && count($arItem['OFFERS']) > 0 && isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']])) {
	
	$offerID = $arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['ID'];
	$itemID = $offerID;
}

$arrFavorites = $arResult['FAVORITES_ITEMS'];
$arBasketItems = $arResult['BASKET_ITEMS'];
$arBasketItemsAll = $arResult['BASKET_ITEMS_ALL'];

?>

<div class="card-product slick-slide" id="product<?=$arItem['ID'];?>">
	<div class="card-product-wrap" id="<?=$areaId?>">
		<? if($arItem["DISPLAY_PROPERTIES"]["STIKERS"]['VALUE']) { ?>
		<div class="card-product__stickers">
			<? foreach($arItem["DISPLAY_PROPERTIES"]["STIKERS"]['VALUE'] as $slicker) { ?>
				<? 
				$res = CIBlockElement::GetByID($slicker);
				if($arResEl = $res->GetNext()) {
					?>
					<div class="card-product__sticker-item">
						<img src="<?=CFile::GetPath($arResEl["DETAIL_PICTURE"]);?>" alt="<?=$arResEl['NAME'];?>" title="<?=$arResEl['NAME'];?>"/>
					</div>
					<?
				} ?>
			<? } ?>
		</div>
		<? } ?>
		<? if($arItem["PROPERTIES"]["OF_DAY"]["VALUE"]) { ?>
			<?
			$objDateTime = new DateTime($arItem["PROPERTIES"]["OF_DAY_DATE"]['VALUE']);
			$actionEnd = $objDateTime->getTimestamp();
			$dayStr = new Bitrix\Main\Grid\Declension('день', 'дня', 'дней'); 
			
			if($actionEnd > time()) {
				$dayLeft = (int) (($actionEnd - time()) / (60*60*24));
				?>
				<div class="product-of-day">
					<div class="product-of-day__title">
						<div class="product-of-day-col">
							<div class="product-of-day__title-label">Товар дня</div>
						</div>
						<div class="product-of-day-col">
							<div class="product-of-day__countdown" data-countdown="<?=$objDateTime->format("Y/m/d H:i:s");?>">
								<span class="day"></span>
								<?=$dayStr->get($dayLeft);?>
								<span class="time"></span>
							</div>
						</div>
					</div>
				</div>
				<?
			}
			?>
		<? } 
		if(0 < $minPrice['DISCOUNT_DIFF_PERCENT']) { ?>
		<div class="w-card-product__discount">Скидка <? echo $minPrice['DISCOUNT_DIFF_PERCENT']; ?>%</div>
		<? } ?>
		<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="card-product__a">
			<?
			$photoResize = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array("width" => 200, "height" => 200), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
			?>
			<div class="w-card-product__img" <?/*?>style="background-image:url('<?=$photoResize['src'];?>');"<?*/?>>
				<img class="card-product__img" src="<?=$photoResize["src"]?>" id="bigImg<?=$itemID?>" alt="<?=$imgAlt;?>" title="<?=$imgTitle;?>">
			</div>
			<div class="card-product__title"><?=$arItem["NAME"]?></div>
		</a>
		<div class="w-card-product__bottom clearfix">
			<div class="card-product__price-full">
				<div class="card-product__price-discount">
				<?if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) { ?> 
					<span><?=priceFormat($minPrice['VALUE']); ?></span>
					<div class="card-product__price-discount-currency"><span class="fa fa-ruble-sign"></span></div><?
				}?>
				</div>
				<div class="card-product__price">
					<div class="card-product__price-val"><?=priceFormat($minPrice['DISCOUNT_VALUE']);?></div>
					<div class="card-product__price-currency"><span class="fa fa-ruble-sign"></span><? if($measure > 1) { ?>/<?=$arItemRatio['ITEM_MEASURE']['TITLE'];?><? } ?></div>
				</div>
				<div class="card-product__weight">
					<?=$arItemRatio['ITEM_MEASURE_RATIOS'][$arItemRatio['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']." ".$arItemRatio['ITEM_MEASURE']['TITLE'];?>
				</div>
			</div>
			<div class="card-product__actions-full"<?if(in_array($arItem['ID'], $arBasketItems)) :?> style="width: 58%; text-align: left;"<?endif;?>>
			<?if(in_array($USER->GetId(), $arItem["PROPERTIES"]["F_USER"]["VALUE"]) || in_array($arItem['ID'], $arrFavorites)){
				$check = " active";
			}
			else{
				$check = "";
			};?>
			<div class="card-product__favorite">
				<a href="#" class="catalog-item-fav btn-fav favorite_check <?=$check?>" data-id="<?=$arItem['ID']?>"></a>
			</div>
			<?if(!in_array($itemID, $arBasketItems)) :?>
				<div class="card-product__buy">
				  <a href="javascript:void(0)" data-id="<?=$itemID?>" data-quantity="<?=$arItem["CATALOG_MEASURE_RATIO"]?>" class="btn-buy"></a>
				</div>
			<?endif;?>
			</div>
			<form action="<?=POST_FORM_ACTION_URI?>" onsubmit="return false;" method="post" enctype="multipart/form-data" class="add_form<?if(in_array($itemID, $arBasketItems)) echo " active";?>">
				<div class="add_form_counter-line ajax-count">
					<a class="MinusList" href="javascript:void(0)">-</a>
					<input type="hidden" name="quantity_ratio" value="<?=$arItem["CATALOG_MEASURE_RATIO"]?>" />
					<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" data-id="<?=$itemID?>" value="<?=(!empty($arBasketItemsAll)&&isset($arBasketItemsAll[$itemID]))?$arBasketItemsAll[$itemID]['QUANTITY']:$arItem["CATALOG_MEASURE_RATIO"]?>" id="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?><?=$itemID?>">
					<a class="PlusList" href="javascript:void(0)">+</a>
				</div>
				<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
				<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?=$itemID?>">
				<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="Купить" style="display:none;">
				<input class="add_form_btn" type="submit" id="link2card<?=$itemID?>" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="В корзине" onClick="window.location.href = '/cart/'" />
			</form>
		</div>
	</div>
</div>
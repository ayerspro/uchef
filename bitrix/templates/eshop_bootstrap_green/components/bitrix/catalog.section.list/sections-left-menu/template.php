<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>
<? if($arResult['SECTIONS']) { ?>
<div class="list-group">
	<? foreach ($arResult['SECTIONS'] as $arSection) {
		
		if(!showSectionByRegion($arSection['ID'])) {
			
			continue;
		}
		
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

		?>
			<a href="<?echo $arSection['SECTION_PAGE_URL'];?>" class="list-group-item <? if($arParams['CURRENT_SECTION'] == $arSection['ID']) { ?>active<? } ?>"><? echo $arSection['NAME']; ?></a>
		<?
	} ?>
</div>
<? } ?>
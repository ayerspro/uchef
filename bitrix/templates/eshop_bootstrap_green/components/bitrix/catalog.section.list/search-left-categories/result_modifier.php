<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach($arResult['SECTIONS'] as $k => &$arSection) {
	
	if($arParams['AR_FORCE_RESULT'][$arSection['ID']]) {
		
		$arSection['LINK'] = $arParams['AR_FORCE_RESULT'][$arSection['ID']]['LINK'];
		$arSection['COUNT'] = $arParams['AR_FORCE_RESULT'][$arSection['ID']]['COUNT'];
		
	} else {
		
		unset($arResult['SECTIONS'][$k]);
	}
}

unset($arSection);
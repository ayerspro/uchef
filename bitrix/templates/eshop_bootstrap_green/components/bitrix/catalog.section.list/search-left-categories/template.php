<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

foreach($arResult['SECTIONS'] as $arSection) {
	
	if(!showSectionByRegion($arSection['ID'])) {
		
		continue;
	}
	
	?>
	<a href="<?=$arSection['LINK'];?>" class="list-group-item<? if($arParams['CURRENT_SECTION_ID'] == $arSection['ID']) { ?> active<? } ?>"><?=$arSection['NAME'];?> (<?=$arSection['COUNT'];?>)</a>
	<?
}
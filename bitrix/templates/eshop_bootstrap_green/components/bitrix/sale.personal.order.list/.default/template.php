<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}

}
else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	$clearFromLink = array('filter_history', 'show_canceled');
	?>
	<section style="text-align: center; padding: 15px;">
		<h1 class="text-center catalog-title">Мои заказы</h1>
	</section>
	<div class="tab-l">
		<a class="sale-order-history-link<?=($_REQUEST['filter_history'] != 'Y' && $_REQUEST['show_canceled'] != 'Y')?' active':'';?>" href="<?=$APPLICATION->GetCurPageParam("", $clearFromLink, false)?>">
			Активные заказы
		</a>
		<a class="sale-order-history-link<?=($_REQUEST['filter_history'] == 'Y' && $_REQUEST['show_canceled'] != 'Y')?' active':'';?>" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false)?>">
			Прошлые покупки
		</a>
		<!--<a class="sale-order-history-link<?=($_REQUEST['filter_history'] == 'Y' && $_REQUEST['show_canceled'] == 'Y')?' active':'';?>" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y", $clearFromLink, false)?>">
			Отложенные товары
		</a>-->
	</div>
	<div style="clear: both;"></div>
<div class="form-o">
	<?
	if (!count($arResult['ORDERS']))
	{
		?>
		<div class="row col-md-12 col-sm-12">
			<a href="javascript:void(0)" class="sale-order-history-link" style="cursor: default;">
				<?if(!isset($_REQUEST['filter_history']) && !isset($_REQUEST['show_canceled'])) :?>У вас нет активных заказов<?endif;?>
				<?if(isset($_REQUEST['filter_history']) && !isset($_REQUEST['show_canceled'])) :?>Вы еще не совершали заказов<?endif;?>
				<?if(isset($_REQUEST['filter_history']) && isset($_REQUEST['show_canceled'])) :?>У вас нет отложенных товаров<?endif;?>
			</a>
		</div>
		<?
	}

	if ($_REQUEST["filter_history"] !== 'Y')
	{
		$paymentChangeData = array();
		$orderHeaderStatus = null;

		foreach ($arResult['ORDERS'] as $key => $order)
		{
			?>
			<div class="sale-order-list-container">
				<a href="<?=$order['ORDER']['URL_TO_DETAIL']?>">
					<div class="row">
						<div class="row-o col-sm-1 col-xs-6">
							<?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
						</div>
						<div class="row-o col-sm-2 col-xs-6">
							<?=$order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?>
						</div>
						<div class="row-o col-sm-2 col-xs-6">
						<?
							if ($shipment['DEDUCTED'] == 'Y')
							{
								?>
								<?=Loc::getMessage('SPOL_TPL_LOADED');?>
								<?
							}
							else
							{
								?>
								<?=Loc::getMessage('SPOL_TPL_NOTLOADED');?>
								<?
							}
						?>
						</div>
						<div class="row-o col-sm-4 col-xs-6">
							<?
								?>
									<?=$payment['PAY_SYSTEM_NAME']?>
								<?
								if ($payment['PAID'] === 'Y')
								{
									?>
									<?=Loc::getMessage('SPOL_TPL_PAID')?>
									<?
								}
								elseif ($order['ORDER']['IS_ALLOW_PAY'] == 'N')
								{
									?>
									<?=Loc::getMessage('SPOL_TPL_RESTRICTED_PAID')?>
									<?
								}
								else
								{
									?>
									<?=Loc::getMessage('SPOL_TPL_NOTPAID')?>
									<?
								}
							?>
						</div>
						<div class="row-o col-sm-3 col-xs-6">
							<?$showDelimeter = false;
						foreach ($order['PAYMENT'] as $payment)
						{
							if ($order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
							{
								$paymentChangeData[$payment['ACCOUNT_NUMBER']] = array(
									"order" => htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']),
									"payment" => htmlspecialcharsbx($payment['ACCOUNT_NUMBER']),
									"allow_inner" => $arParams['ALLOW_INNER'],
									"only_inner_full" => $arParams['ONLY_INNER_FULL']
								);
							}
							?>
							<span class="sale-order-list-payment-number"><?=$payment['FORMATED_SUM']?></span>
							<?
						}?>
						</div>
					</div>
				</a>
			</div>
			<?
		}
	}
	else
	{
		$orderHeaderStatus = null;
		foreach ($arResult['ORDERS'] as $key => $order)
		{
			?>
			<div class="col-md-12 col-sm-12 sale-order-list-container">
				<div class="row">
					<a href="<?=$order['ORDER']['URL_TO_DETAIL']?>">
						<div class="row-o col-sm-1 col-xs-6">
							<?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
						</div>
						<div class="row-o col-sm-2 col-xs-6">
							<?=$order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?>
						</div>
						<!-- <div class="row-o col-sm-2 col-xs-6">
						<?/*
							if ($shipment['DEDUCTED'] == 'Y')
							{
								?>
								<?=Loc::getMessage('SPOL_TPL_LOADED');?>
								<?
							}
							else
							{
								?>
								<?=Loc::getMessage('SPOL_TPL_NOTLOADED');?>
								<?
							}
						*/?>
						</div> -->
						<div class="row-o col-sm-6 col-xs-6">
							<?
								if ($_REQUEST["show_canceled"] !== 'Y')
								{
									?>
										<?= Loc::getMessage('SPOL_TPL_ORDER_FINISHED')?>
									<?
								}
								else
								{
									?>
										<?= Loc::getMessage('SPOL_TPL_ORDER_CANCELED')?>
									<?
								}
								?>
						</div>
						<div class="row-o col-sm-3 col-xs-6">
							<?= $order['ORDER']['DATE_STATUS_FORMATED'] ?>
						</div>
					</a>
				</div>
			</div>
			<?
		}
	}
	?>
</div>
	<div class="clearfix"></div>
	<?
	echo $arResult["NAV_STRING"];

	if ($_REQUEST["filter_history"] !== 'Y')
	{
		$javascriptParams = array(
			"url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
			"templateFolder" => CUtil::JSEscape($templateFolder),
			"paymentList" => $paymentChangeData
		);
		$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
		?>
		<script>
			BX.Sale.PersonalOrderComponent.PersonalOrderList.init(<?=$javascriptParams?>);
		</script>
		<?
	}
}
?>

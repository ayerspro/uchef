$(function() {

	$(".form_phone").mask("+7 (999) 999-99-99");
	$(".form_phone").focus();
	$(".form_phone").get(0);
	$("#soa-property-3").mask("+7 (999) 999-99-99");
	$("#soa-property-3").focus();
	$("#soa-property-3").get(0);
	
	$('[name="ORDER_PROP_3"]').mask("+7 (999) 999-99-99");
	$('[name="phone"]').mask("+7 (999) 999-99-99");
	
	$('.w-card-product__category img').tooltip({
		placement: 'bottom',
	});
	
	$('.card-product__sticker-item img').tooltip({
		placement: 'bottom'
	});
	
	$('.benefits__col-wrap a').hover(
	
		function() {
			
			$(this).closest('.benefits__col-wrap').addClass('benefits__col-wrap-active');
			
		}, function() {
			
			$(this).closest('.benefits__col-wrap').removeClass('benefits__col-wrap-active');
		}
	);
	
	$('.bx-filter-block-scroll').scrollbar();
	
	// Clear Basket
	$('body').on('click', '.js-clear-basket-confirm', function() {
		$.ajax({
			type: 'post',
			url: '/ajax/clearBasket.php',
			data: {},
			
			success: function(result) {
				
				window.location.reload();
			},
		});
	});
	
	// Reviews
	$('body').on('click', '[data-type="ds-rv-send"]', function() {
		
		var thisBtn = $(this);
		var thisFormID = $(this).closest('.ds-form').attr('id');
		var thisUrl = window.location.href;
		var postData = new Object()
		
		if(thisBtn.hasClass('load') || thisBtn.hasClass('disabled')) {
			return false;
		}
		
		thisBtn.addClass('load');
		
		$('#'+thisFormID+' input, #'+thisFormID+' textarea, #'+thisFormID+' select').each(function(i, elem) {
			
			var fieldName = $(elem).attr('name');
			
			if(!!fieldName) {
				postData[fieldName] = $(elem).val();
			}
		});
		
		postData['ds-cb-ajax'] = 'Y';
		postData['ncc'] = 'Y';
		
		$.ajax({
			type: 'post',
			url: thisUrl,
			data: postData,
			dataType: 'json',
			
			success: function(result) {
			
				if(result == null) {
					alert('Error');
				}
				
				else if(result.errors) {
					$.each(result.errors, function(key, val) {
						if(key == 'RECAPTCHA') {
							
							$('#'+thisFormID+' .recaptcha-err').addClass('inpErr');
							
						} else {
							
							$('#'+thisFormID+' [name="'+key+'"]').addClass('inpErr');
						}
					});
				}
				
				else if(result.success) {

					$('#'+thisFormID).html('<div class="dscb-result review-form__result">'+DSRV.SUCCESS_MESS+'</div>');
				
					thisBtn.removeClass('load');
					return true;

				}
				
				thisBtn.removeClass('load');
			},
			
			error: function() {
				thisBtn.removeClass('load');
				alert('Error');
			}
		});
	});
	
	// Contacts
	$('body').on('click', '[data-type="ds-cb-send"]', function() {
		
		var thisBtn = $(this);
		var thisFormID = $(this).closest('.ds-form').attr('id');
		var thisUrl = window.location.href;
		var postData = new Object()
		
		if(thisBtn.hasClass('load')) {
			return false;
		}
		
		thisBtn.addClass('load');
		
		$('#'+thisFormID+' input, #'+thisFormID+' textarea').each(function(i, elem) {
			
			var fieldName = $(elem).attr('name');
			
			if(!!fieldName) {
				postData[fieldName] = $(elem).val();
			}
		});
		
		postData['ds-cb-ajax'] = 'Y';
		postData['ncc'] = 'Y';
		
		$.ajax({
			type: 'post',
			url: thisUrl,
			data: postData,
			dataType: 'json',
			
			success: function(result) {
			
				if(result == null) {
					alert('Error');
				}
				
				else if(result.errors) {
					$.each(result.errors, function(key, val){
						if(key == 'RECAPTCHA') {
							
							$('#'+thisFormID+' .recaptcha-err').addClass('inpErr');
							
						} else {
							
							$('#'+thisFormID+' [name="'+key+'"]').addClass('inpErr');
						}
					});
				}
				
				else if(result.success) {

					$('#'+thisFormID).html('<div class="dscb-result">'+DSCB.SUCCESS_MESS+'</div>');
				
					thisBtn.removeClass('load');
					return true;

				}
				
				thisBtn.removeClass('load');
			},
			
			error: function() {
				thisBtn.removeClass('load');
				alert('Error');
			}
		});
	});
	
	$('.of-day .products-items').slick({
		dots: true,
		infinite: true,
		speed: 300,
	  	slidesToShow: 6,
	  	slidesToScroll: 6,
	  	slidesToShow: 1,
        slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 7000,
	});
	
	$('.detail__advert-block-slider .products-items').slick({
		dots: true,
		infinite: true,
		speed: 300,
	  	slidesToShow: 6,
	  	slidesToScroll: 6,
	  	slidesToShow: 2,
        slidesToScroll: 2,
		autoplay: true,
		autoplaySpeed: 7000,
		responsive: [
    		
        	{
      			breakpoint: 1200,
      			settings: {
        			slidesToShow: 1,
        			slidesToScroll: 1,
        		}
        	},
        	{
      			breakpoint: 768,
      			settings: {
        			slidesToShow: 3,
        			slidesToScroll: 3,
        		}
        	},
			{
      			breakpoint: 750,
      			settings: {
        			slidesToShow: 2,
        			slidesToScroll: 2,
        		}
        	},
			{
				breakpoint: 550,
				settings: {
				  slidesToShow: 1,
				  slidesToScroll: 1,
				}
			  },
  		]
	});
	
	$('.coupons-list__slider').slick({
		dots: true,
		infinite: true,
		speed: 300,
	  	slidesToShow: 5,
        slidesToScroll: 5,
		autoplay: true,
		autoplaySpeed: 7000,
		responsive: [
    		
        	{
      			breakpoint: 1370,
      			settings: {
        			slidesToShow: 4	,
        			slidesToScroll: 4,
        		}
        	},
        	{
      			breakpoint: 1025,
      			settings: {
        			slidesToShow: 3,
        			slidesToScroll: 3,
        		}
        	},{
            breakpoint: 750,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          },
          {
            breakpoint: 550,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            }
          },
  		]
	});
	
	$('.slider-list-vertical').slick({
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 3,
        autoplay: false,
    });
	
	// Советуем попробовать на главной
	
	$('.main_gurman.products-items').slick({
		dots: true,
		infinite: true,
		speed: 300,
	  	slidesToShow: 6,
	  	slidesToScroll: 6,
	  	responsive: [
    		{
      			breakpoint: 1800,
      			settings: {
        			slidesToShow: 5,
        			slidesToScroll: 5,
        		}
        	},
        	{
      			breakpoint: 1300,
      			settings: {
        			slidesToShow: 4,
        			slidesToScroll: 4,
        		}
        	},
        	{
      			breakpoint: 1024,
      			settings: {
        			slidesToShow: 3,
        			slidesToScroll: 3,
        		}
        	},{
            breakpoint: 750,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 550,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          },
  		]
	});
	
	// Лидеры продаж
	
	$('.popular-items').slick({
		dots: true,
		infinite: true,
		speed: 300,
	  	slidesToShow: 6,
	  	slidesToScroll: 6,
	  	responsive: [
    		{
      			breakpoint: 1800,
      			settings: {
        			slidesToShow: 5,
        			slidesToScroll: 5,
        		}
        	},
        	{
      			breakpoint: 1300,
      			settings: {
        			slidesToShow: 4,
        			slidesToScroll: 4,
        		}
        	},
        	{
      			breakpoint: 1024,
      			settings: {
        			slidesToShow: 3,
        			slidesToScroll: 3,
        		}
        	},{
            breakpoint: 750,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
            }
          },
          {
            breakpoint: 550,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          },
  		]
	});
	
	
	$('body').on('focus', '.inpErr', function() {
		$(this).removeClass('inpErr');
	});
	

	$('body').on('click', '.md-filter-bg', function() {
		
		$(this).closest('.md-filter-active').removeClass('md-filter-active');
	});

	$('body').on('click', '.js-md-filter-controll', function() {
		
		var isActive = $(this).closest('.cat-menu').hasClass('md-filter-active');
		
		$('.cat-menu').toggleClass('md-filter-active');
		//$('.cat-menu').fadeToggle(500);
	});
	
	$('body').on('click', '.js-show-reviews-form', function() {
		
		$(this).closest('.detail__reviews-btn').fadeOut();
		$('.detail__reviews-form').slideDown(500);
	});
	
	
	$('[data-countdown]').each(function() {
		
		var $this = $(this), finalDate = $(this).data('countdown');
		
		$this.countdown(finalDate, function(event) {
			
			$this.find('.day').html(event.strftime('%-D'));
			$this.find('.time').html(event.strftime('%H:%M:%S'));
		});
	});
	
});


var onloadRecaptchaHandler = function() {
	
	var reCaptchaKey = '6Lcrz24UAAAAAKO5aKUhLbecS1e9elfHVqWQNh7l';
	
	if(document.getElementById('ds-contacts-recaptcha')) {
		grecaptcha.render('ds-contacts-recaptcha', {
			'sitekey' : reCaptchaKey,
			'callback' : function() {
				$('.recaptcha-err').removeClass('inpErr');
			},
		});
	}
	
	if(document.getElementById('ds-review-recaptcha')) {
		grecaptcha.render('ds-review-recaptcha', {
			'sitekey' : reCaptchaKey,
			'callback' : function() {
				$('.recaptcha-err').removeClass('inpErr');
			},
		});
	}
};
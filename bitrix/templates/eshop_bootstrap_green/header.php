<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
//CJSCore::Init(array("fx", "jquery"));
$curPage = $APPLICATION->GetCurPage(true);
$theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);

$isMain = CSite::InDir(SITE_DIR.'index.php');
$isCatalog = CSite::InDir(SITE_DIR.'catalog/');

global $USER;
$isAdmin = $USER->IsAdmin();

?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />

	<title><?$APPLICATION->ShowTitle()?></title>

	<!-- JS INCLUDES -->
	<?
		if($_SESSION["SOTBIT_REGIONS"]["UF_HEADER_MORE_INFO"]) {
		
			echo $_SESSION["SOTBIT_REGIONS"]["UF_HEADER_MORE_INFO"];
		}

		$APPLICATION->AddHeadScript('https://code.jquery.com/jquery-3.2.1.min.js');
		$APPLICATION->AddHeadScript('https://code.jquery.com/ui/1.12.1/jquery-ui.js');
		$APPLICATION->AddHeadScript('https://www.google.com/recaptcha/api.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jQuery.jscrollpane.js');
		$APPLICATION->AddHeadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
		$APPLICATION->AddHeadScript('//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.scrollbar.min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.matchHeight.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.jgrowl.min.js');
		//$APPLICATION->AddHeadScript('https://code.jquery.com/jquery-3.2.1.min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/ion.rangeSlider.min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.colorbox.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.colorbox-min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.touchSwipe.min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.touchSwipe.min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/lib/maskedinput/jquery.maskedinput.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.countdown.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/clipboard.js");
		
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.form.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/common.js");
	?>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/jquery.jgrowl.css" media="screen" />

	<!-- /JS INCLUDES -->
	<?$APPLICATION->ShowHead();?>
	<?
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/colors.css", true);
	$APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap.css");
	$APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.scrollbar.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/ion.rangeSlider.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/ion.rangeSlider.skinFlat.css");
	//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/new_year.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/colorbox.css");
	
	$asset = \Bitrix\Main\Page\Asset::getInstance();
	
	if($_SESSION["SOTBIT_REGIONS"]["UF_YANDEX_VERIFICAT"]) {
		
		$asset->addString('<meta name="yandex-verification" content="'.$_SESSION["SOTBIT_REGIONS"]["UF_YANDEX_VERIFICAT"].'" />', true);
	}



	?>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/ie.css"/>
	
	<script src="https://www.google.com/recaptcha/api.js?onload=onloadRecaptchaHandler&render=explicit" async defer></script>

</head>
<body class="bx-background-image bx-theme-<?=$theme?><? if($isAdmin) { ?> is-admin<? } ?>" <?=$APPLICATION->ShowProperty("backgroundImage")?>>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?$APPLICATION->IncludeComponent(
	"bitrix:eshop.banner",
	".default",
	array(
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>

<div class="bx-wrapper" id="bx_eshop_wrap">
	<header class="bx-header">
		<div class="mobile-top-fixed-bg"></div>
		<br>
		<div class="bx-header-section container-fluid">
			<div class="row">
				<div class="col-lg-2 visible-lg">
					<div class="bx-logo">
						<a class="bx-logo-block" href="<?=SITE_DIR?>">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								".default",
								array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/company_logo.php",
									"COMPONENT_TEMPLATE" => ".default",
									"EDIT_TEMPLATE" => ""
								),
								false
							);?>
						</a>
					</div>
				</div>
				<div class="col-lg-10 col-md-12">
					<div class="row flexible">
						<div class="col-md-2 hidden-lg">
							<div class="bx-logo">
								<a class="bx-logo-block" href="<?=SITE_DIR?>">
									<?$APPLICATION->IncludeComponent(
										"bitrix:main.include",
										".default",
										array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR."include/company_logo.php",
											"COMPONENT_TEMPLATE" => ".default",
											"EDIT_TEMPLATE" => ""
										),
										false
									);?>
								</a>
							</div>
						</div>
						<div class="txt-mob-header" style="display: none;">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								".default",
								array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/txt-header.php",
									"COMPONENT_TEMPLATE" => ".default",
									"EDIT_TEMPLATE" => ""
								),
								false
							);?>
						</div>
						<div class="col-lg-6 col-md-6 flexible vcenter col-nonpad">
							<div id="top_menu"><?if($USER->IsAuthorized() && strpos($APPLICATION->GetCurPage(), '/personal/') !== false):?>
						<nav class="bx-top-nav-container hidden-md hidden-lg hidden-sm visible-xs">
							<ul class="bx-nav-list-1-lvl">
							<li class="bx-nav-1-lvl bx-nav-list-0-col icon-l order-i"><a href="/personal/orders/?filter_history=Y">
								<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 128 160" x="0px" y="0px"><path class="cls-1" d="M102,40H80a8,8,0,0,1-8-8V10" style="fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:8px;" /><path class="cls-1" d="M72.69,8H32a8,8,0,0,0-8,8v96a8,8,0,0,0,8,8H96a8,8,0,0,0,8-8V39.31a8,8,0,0,0-2.34-5.66L78.34,10.34A8,8,0,0,0,72.69,8Z"/></svg>
								</span>
								Мои заказы</a></li>
							<li class="bx-nav-1-lvl bx-nav-list-0-col icon-l"><a href="/personal/cart/">
								<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 48 60" style="enable-background:new 0 0 48 48;" xml:space="preserve"><g><path d="M45.2,16.5h-3.5L31.2,3.2c-0.6-0.8-1.8-0.9-2.6-0.3c-0.8,0.6-0.9,1.7-0.3,2.5c0,0,0,0,0,0L37,16.5H10.8   l9.1-11.1c0.6-0.8,0.5-1.9-0.2-2.5c0,0,0,0,0,0c-0.8-0.6-2-0.5-2.6,0.3L6.1,16.5H2.8c-1.5,0-2.7,1.2-2.8,2.7V21   c0,1.4,1.1,2.5,2.4,2.7l5,18.5c0.2,0.8,1,3.2,3.2,3.2h26.7c1.7,0,2.8-1.5,3.2-3l5-18.7c1.4-0.1,2.5-1.3,2.5-2.7v-1.8   C48,17.7,46.7,16.5,45.2,16.5z M18,36.4c0,1.1-0.9,2-2,2s-2-0.9-2-2v-7.2c0-1.1,0.9-2,2-2s2,0.9,2,2V36.4L18,36.4z M26,36.4   c0,1.1-0.9,2-2,2s-2-0.9-2-2v-7.2c0-1.1,0.9-2,2-2s2,0.9,2,2V36.4L26,36.4z M34,36.4c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2v-7.2   c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2V36.4L34,36.4z"/></g></svg>
								</span>
								Корзина</a></li>
							<li class="bx-nav-1-lvl bx-nav-list-0-col icon-l"><a href="/personal/private/">
								<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 125"><path d="M337,225 l526,0 c0,0,24,100,-23,125 l-134,71 c-25,13,-36,28,-36,41 c0,32,62,75,62,184 c0,92,-49,154,-131,154 c-92,0,-133,-77,-133,-154 c0,-101,61,-147,61,-181 c0,-14,-9,-31,-34,-44 l-134,-71 c-47,-25,-24,-125,-24,-125 Z M50,0 c-50,0,-50,0,-50,50 l0,1000 c0,50,0,50,50,50 l300,0 l75,-100 l725,0 c50,0,50,0,50,-50 l0,-900 c0,-50,0,-50,-50,-50 Z M50,0 " fill="#000000" stroke="none" stroke-dasharray="none" stroke-linecap="inherit" stroke-linejoin="inherit" stroke-width="1" transform="matrix(0.0666666666667,0.0,0.0,-0.0666666666667,10.0,90.0)"/></svg></span>
								Личные данные
							</a></li>
						   <?/*<li class="bx-nav-1-lvl bx-nav-list-0-col icon-l">
								<a href="#">
									<span class="ic"><svg xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 90 112.5" enable-background="new 0 0 90 90" xml:space="preserve"><switch><foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"/><g i:extraneous="self"><g><path d="M42.796,40.815c0,3.537-1.256,6.617-3.425,8.329l-4.105,3.193c-1.027,0.799-1.71,2.282-1.597,4.564L34.694,89H24.429     l0.913-32.099c0.113-2.282-0.571-3.652-1.597-4.45l-4.108-3.308c-2.165-1.712-3.308-4.676-3.308-8.329     c0-15.97,1.255-25.894,1.826-35.818c0.228-2.509,0.684-3.652,1.824-3.652c1.142,0,1.599,1.143,1.825,3.652l1.255,20.076     c0.229,1.14,0.912,1.939,1.598,1.939c0.685,0,1.37-0.799,1.597-1.939l1.598-20.076c0.112-2.625,0.684-3.652,1.71-3.652     c1.027,0,1.484,1.027,1.711,3.652l1.599,20.076c0,1.14,0.685,1.939,1.482,1.939c0.912,0,1.597-0.799,1.597-1.939l1.368-20.076     c0.115-2.509,0.687-3.652,1.826-3.652c1.027,0,1.597,1.143,1.711,3.652C41.54,14.921,42.796,24.845,42.796,40.815z"/><path d="M72.757,57.242c0,4.563,0.686,26.282,0.914,31.758H62.148l1.256-27.307c0-1.712-0.571-3.082-1.597-3.768     c-1.597-1.024-6.844-2.85-6.844-6.845c0-24.868,5.359-44.603,13.459-49.394c2.511-1.596,4.334-0.455,4.334,3.879V57.242z"/></g></g></switch></svg></span>
									Рецепты
								</a></li>*/?>
							<li class="bx-nav-1-lvl bx-nav-list-0-col icon-l">
								<a href="/personal/favorite/">
									<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 125" style="enable-background:new 0 0 100 100;" xml:space="preserve"><g><path d="M87.3,19.1C78.1,8.3,61.5,3.5,49.9,16c-0.1,0.1-0.2,0.1-0.3,0c-12.8-12.6-29-7.7-37.4,3.1C-1.9,37.4,6,50.8,22.3,67.2   l27.4,25.2l27.5-25.3C93.6,50.8,102.4,36.6,87.3,19.1z M83.9,40.7c-1.4,4.9-4.8,3.7-3.5,0.2c1.7-4.5,0.5-8.3-3.2-12.6   c-2.8-3.3-6.8-5.3-10.4-5.3c-2.7,0-5.1,1.1-7.2,3.2c-2.5,2.5-4-0.6-2.3-2.3c2.9-3,6.2-4.2,9.5-4.2c4.8,0,9.6,2.6,12.9,6.4   C84.2,31.3,85.2,36,83.9,40.7z"/></g></svg></span>
									 Избранное
								</a></li>
							<?/*<li class="bx-nav-1-lvl bx-nav-list-0-col icon-l">
								<a href="#">
									<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 125" enable-background="new 0 0 100 100" xml:space="preserve"><path d="M68.496,31.25c0.529-0.354,1.043-0.73,1.523-1.146c1.294-1.131,2.401-2.498,3.201-4.077c0.799-1.576,1.28-3.378,1.279-5.276  c0.001-3.457-1.243-6.699-3.467-9.098c-1.109-1.197-2.465-2.182-4.001-2.857C65.498,8.115,63.785,7.748,62,7.75  c-1.832,0-3.559,0.229-5.161,0.714c-2.402,0.72-4.513,2.062-6.086,3.866c-0.841,0.959-1.52,2.035-2.071,3.187  c-2.039-1.725-4.746-2.777-7.682-2.767c-2.889-0.002-5.535,1.18-7.425,3.075c-1.895,1.89-3.077,4.536-3.075,7.425  c-0.002,1.594,0.433,3.095,1.12,4.375c0.79,1.468,1.888,2.671,3.151,3.625H10v20h80v-20H68.496z M54.02,22.316  c0.05-0.437,0.102-0.834,0.153-1.162c0.051-0.326,0.104-0.588,0.137-0.71c0.262-1.03,0.615-1.881,1.031-2.57  c0.634-1.034,1.366-1.726,2.386-2.255c1.02-0.521,2.393-0.868,4.273-0.869c0.838,0.001,1.562,0.166,2.207,0.449  c0.962,0.426,1.763,1.131,2.351,2.078c0.585,0.947,0.942,2.131,0.942,3.473c-0.001,0.725-0.176,1.422-0.526,2.118  c-0.521,1.04-1.474,2.052-2.659,2.765c-1.181,0.719-2.56,1.121-3.814,1.117c-1.38,0-6.651,0-6.833,0  C53.695,26.262,53.921,23.191,54.02,22.316z M38.525,20.775c0.644-0.639,1.497-1.023,2.475-1.025  c1.354,0.01,2.355,0.429,3.222,1.157c0.856,0.731,1.543,1.831,1.883,3.191c0.01,0.037,0.046,0.216,0.079,0.45  c0.059,0.416,0.182,2.015,0.193,2.201c-1.648,0-3.435,0-4.377,0c-0.493,0.001-1.069-0.105-1.637-0.322  c-0.854-0.318-1.663-0.896-2.162-1.504c-0.253-0.303-0.432-0.607-0.542-0.887c-0.111-0.28-0.158-0.531-0.159-0.787  C37.502,22.271,37.887,21.419,38.525,20.775z"/><g><path d="M15,56.25v26c0,5.5,4.5,10,10,10h50c5.5,0,10-4.5,10-10v-26H15z"/></g></svg></span>
								 Бонусы
								</a></li>*/?>
							<li class="bx-nav-1-lvl bx-nav-list-0-col icon-l">
								<a href="#" data-modal-wrap=".callback-form">
									<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 122.5" version="1.1" x="0px" y="0px"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g fill="#000000"><g transform="translate(23.000000, 57.000000)"><path d="M6.4551,3.1904 C6.4551,4.7834 5.1641,6.0734 3.5721,6.0734 C1.9781,6.0734 0.6871,4.7834 0.6871,3.1904 C0.6871,1.5984 1.9781,0.3064 3.5721,0.3064 C5.1641,0.3064 6.4551,1.5984 6.4551,3.1904"/><path d="M17.832,3.1904 C17.832,4.7834 16.54,6.0734 14.948,6.0734 C13.355,6.0734 12.065,4.7834 12.065,3.1904 C12.065,1.5984 13.355,0.3064 14.948,0.3064 C16.54,0.3064 17.832,1.5984 17.832,3.1904"/><path d="M29.208,3.1904 C29.208,4.7834 27.918,6.0734 26.325,6.0734 C24.734,6.0734 23.442,4.7834 23.442,3.1904 C23.442,1.5984 24.734,0.3064 26.325,0.3064 C27.918,0.3064 29.208,1.5984 29.208,3.1904"/></g><g transform="translate(7.000000, 11.399400)"><path d="M72.1758,22.2607 C70.5838,22.2607 69.2928,20.9687 69.2928,19.3757 C69.2928,17.7837 70.5838,16.4947 72.1758,16.4947 C73.7698,16.4947 75.0588,17.7837 75.0588,19.3757 C75.0588,20.9687 73.7698,22.2607 72.1758,22.2607 M60.7998,22.2607 C59.2058,22.2607 57.9158,20.9687 57.9158,19.3757 C57.9158,17.7837 59.2058,16.4947 60.7998,16.4947 C62.3928,16.4947 63.6828,17.7837 63.6828,19.3757 C63.6828,20.9687 62.3928,22.2607 60.7998,22.2607 M32.6438,67.0157 C25.9638,67.0157 19.8948,65.5087 14.6088,62.5347 C14.2588,62.3397 13.8328,62.3417 13.4878,62.5427 C13.1418,62.7437 12.9288,63.1137 12.9268,63.5117 L12.8838,71.9117 C9.7428,68.2127 3.7658,60.4947 2.6318,54.1527 C2.0748,51.0417 1.3628,43.1107 6.7508,36.6737 C11.7188,30.7397 20.5298,27.7307 32.9418,27.7307 C47.0878,27.7307 56.9678,35.4407 56.9678,46.4827 C56.9678,56.4367 48.4438,67.0157 32.6438,67.0157 M49.4228,16.4947 C51.0138,16.4947 52.3058,17.7837 52.3058,19.3757 C52.3058,20.9687 51.0138,22.2607 49.4228,22.2607 C47.8298,22.2607 46.5398,20.9687 46.5398,19.3757 C46.5398,17.7837 47.8298,16.4947 49.4228,16.4947 M82.1568,9.8237 C77.0338,3.7037 68.1568,0.6007 55.7738,0.6007 C41.1368,0.6007 30.9138,8.7797 30.9138,20.4897 C30.9138,22.1577 31.1538,23.8407 31.5988,25.4947 C19.1938,25.7147 10.2498,28.9807 5.0218,35.2267 C-0.9772,42.3917 -0.2012,51.1267 0.4098,54.5497 C2.0728,63.8487 12.7258,75.1697 13.1778,75.6477 C13.3958,75.8767 13.6938,75.9997 13.9968,75.9997 C14.1368,75.9997 14.2778,75.9737 14.4118,75.9197 C14.8398,75.7507 15.1218,75.3377 15.1248,74.8777 L15.1728,65.3827 C20.3938,67.9637 26.2608,69.2727 32.6438,69.2727 C49.9078,69.2727 59.2248,57.5307 59.2248,46.4827 C59.2248,44.9057 59.0468,43.3907 58.7078,41.9457 C63.7128,41.6347 68.5248,40.4717 72.6308,38.5367 L71.4888,47.9437 C71.4318,48.4237 71.6868,48.8887 72.1238,49.0967 C72.2798,49.1707 72.4458,49.2067 72.6108,49.2067 C72.9078,49.2067 73.2008,49.0877 73.4188,48.8647 C73.8898,48.3807 84.9478,36.9097 86.5228,28.1117 C87.1008,24.8717 87.8348,16.6077 82.1568,9.8237"/></g></g></g></svg></span>
									 Обратная связь
								</a></li>
							<li class="bx-nav-1-lvl bx-nav-list-0-col icon-l">
								<a href="/?logout=yes">
									<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 16 20" version="1.1" x="0px" y="0px"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g fill="#000000"><polygon points="12 10 8 6 4 10 7 10 7 16 9 16 9 10"/><polygon points="0 14 4 14 4 12 2 12 2 2 14 2 14 12 12 12 12 14 16 14 16 0 0 0"/></g></g></svg></span>
									Выход
								</a></li>
							</ul>
						</nav>
						   <div class="hidden-xs">
								<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", array(
										"ROOT_MENU_TYPE" => "top",
										"MAX_LEVEL" => "1",
										"MENU_CACHE_TYPE" => "A",
										"CACHE_SELECTED_ITEMS" => "N",
										"MENU_CACHE_TIME" => "36000000",
										"MENU_CACHE_USE_GROUPS" => "Y",
										"MENU_CACHE_GET_VARS" => array(
										),
									),
									false
								);?>
						   </div>
						   <?else:?>
								<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", array(
										"ROOT_MENU_TYPE" => "top",
										"MAX_LEVEL" => "1",
										"MENU_CACHE_TYPE" => "A",
										"CACHE_SELECTED_ITEMS" => "N",
										"MENU_CACHE_TIME" => "36000000",
										"MENU_CACHE_USE_GROUPS" => "Y",
										"MENU_CACHE_GET_VARS" => array(
										),
									),
									false
								);?>
							<?endif;?>
							</div>
						</div>
						<div class="visible-lg-flex col-lg-2 flexible vcenter hcenter col-nonpad">
							<div class="bx-worktime">
								<div class="bx-worktime-prop">
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule.php"), false);?>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 flexible vcenter hcenter col-nonpad panel-geoip">
							<?$APPLICATION->IncludeComponent(
								"reaspekt:reaspekt.geoip", 
								"new", 
								array(
									"CHANGE_CITY_MANUAL" => "Y",
									"COMPONENT_TEMPLATE" => "new",
									"MAIN_DOMAIN" => "https://yaeda.ru",
								),
								false
							);?>
						</div>
						<div class="col-lg-2 col-md-2 flexible vcenter btn-callback">
							<a href="#" class="btn btn-block btn-default" data-modal-wrap=".callback-form">Обратная связь</a>
						</div>
					</div>
					<div class="visible-lg-block header-separatop"></div>
					<div class="row flexible ln2header">
						<div class="col-md-2 catalog-top">
							<div class="dropdown text-center">
								<button id="mobileCategoryDropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-block">
									Каталог <span class="hamburger"></span>
								</button>
								<?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("top-catalof-menu-frame");?>
								<div class="dropdown-menu category-menu" aria-labelledby="mobileCategoryDropdown">
									<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", array(
											"ROOT_MENU_TYPE" => "left",
											"MENU_CACHE_TYPE" => "N",
											"MENU_CACHE_TIME" => "36000000",
											"MENU_CACHE_USE_GROUPS" => "Y",
											"MENU_THEME" => "site",
											"CACHE_SELECTED_ITEMS" => "N",
											"MENU_CACHE_GET_VARS" => array(
											),
											"MAX_LEVEL" => "3",
											"CHILD_MENU_TYPE" => "left",
											"USE_EXT" => "Y",
											"DELAY" => "N",
											"ALLOW_MULTI_SELECT" => "N",
											"REGION" => $_SESSION['SOTBIT_REGIONS']['ID'],
										),
										false
									);?>
								</div>
								<?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("top-catalof-menu-frame", "");?>
							</div>
						</div>
						<div class="col-lg-5 col-md-4 flexible vcenter bas-content">
							<?$APPLICATION->IncludeComponent(
	"bitrix:search.title", 
	"search-top", 
	array(
		"NUM_CATEGORIES" => "1",
		"TOP_COUNT" => "500",
		"CHECK_DATES" => "N",
		"SHOW_OTHERS" => "N",
		"PAGE" => SITE_DIR."",
		"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
		"CATEGORY_0" => array(
			0 => "iblock_catalog",
		),
		"CATEGORY_0_iblock_catalog" => array(
			0 => "all",
		),
		"CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "title-search-input",
		"CONTAINER_ID" => "search",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"SHOW_PREVIEW" => "Y",
		"PREVIEW_WIDTH" => "75",
		"PREVIEW_HEIGHT" => "75",
		"CONVERT_CURRENCY" => "Y",
		"COMPONENT_TEMPLATE" => "search-top",
		"PLACEHOLDER" => "Поиск товаров",
		"USE_LANGUAGE_GUESS" => "Y",
		"ORDER" => "date",
		"PRICE_VAT_INCLUDE" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "250",
		"CURRENCY_ID" => "RUB"
	),
	false
);?>
						</div>
						<div class="col-lg-3 col-md-2 flexible vcenter hcenter mc300-14 col-nonpad text-center panel-log">
							<span>
							<?if ($USER->IsAuthorized()):
								$name = trim($USER->GetFullName());
								if (! $name)
									$name = trim($USER->GetLogin());
								if (strlen($name) > 15)
									$name = substr($name, 0, 12).'...';
								?>
								<a href="/personal/favorite/">Избранное</a> &nbsp;&nbsp;| &nbsp;
								<a href="/personal/private/"><?=htmlspecialcharsbx($name)?></a>
							<?else:
								$arParamsToDelete = array(
									"login",
									"login_form",
									"logout",
									"register",
									"forgot_password",
									"change_password",
									"confirm_registration",
									"confirm_code",
									"confirm_user_id",
									"logout_butt",
									"auth_service_id",
									"clear_cache"
								);

								$currentUrl = urlencode($APPLICATION->GetCurPageParam("", $arParamsToDelete));
								if ($arParams['AJAX'] == 'N')
								{
									?><script type="text/javascript"><?=$cartId?>.currentUrl = '<?=$currentUrl?>';</script><?
								}
								else
								{
									$currentUrl = '#CURRENT_URL#';
								}
								?>
								<a class="login-open" data-form-text="Для просмотра избранных товаров необходимо авторизоваться на сайте" href="#">Избранное</a> &nbsp;&nbsp;| &nbsp;
								<a href="#" class="login">Вход<span class="visible-lg-inline"> в личный кабинет</span></a>
							<?endif?>
							</span>
						</div>
						<div class="col-lg-2 col-md-2 flexible vcenter hcenter col-nonpad" id="basket-container">
							<?$APPLICATION->IncludeComponent(
								"bitrix:sale.basket.basket.line",
								"template1",
								array(
									"PATH_TO_BASKET" => SITE_DIR."cart/",
									"PATH_TO_PERSONAL" => SITE_DIR."personal/",
									"SHOW_PERSONAL_LINK" => "N",
									"SHOW_NUM_PRODUCTS" => "Y",
									"SHOW_TOTAL_PRICE" => "Y",
									"SHOW_PRODUCTS" => "Y",
									"POSITION_FIXED" => "Y",
									"SHOW_AUTHOR" => "Y",
									"PATH_TO_REGISTER" => SITE_DIR."login/",
									"PATH_TO_PROFILE" => SITE_DIR."personal/",
									"COMPONENT_TEMPLATE" => "template1",
									"PATH_TO_ORDER" => SITE_DIR."cart/order/",
									"SHOW_EMPTY_VALUES" => "Y",
									"PATH_TO_AUTHORIZE" => "",
									"SHOW_DELAY" => "N",
									"SHOW_NOTAVAIL" => "N",
									"SHOW_SUBSCRIBE" => "N",
									"SHOW_IMAGE" => "Y",
									"SHOW_PRICE" => "Y",
									"SHOW_SUMMARY" => "Y",
									"POSITION_HORIZONTAL" => "left",
									"POSITION_VERTICAL" => "top",
									"HIDE_ON_BASKET_PAGES" => "N"
								),
								false
							);?>
						</div>
						<div class="col-lg-2 col-md-2 flexible vcenter oform">
							<a href="/cart/order/" class="btn btn-default btn-block">Оформить заказ</a>
						</div>
					</div>
				</div>
			</div>
			<?if ($curPage != SITE_DIR."index.php"):?>
			<div class="row">
			<!--	<div class="col-lg-12">
					<?$APPLICATION->IncludeComponent("bitrix:search.title", "visual", array(
							"NUM_CATEGORIES" => "1",
							"TOP_COUNT" => "5",
							"CHECK_DATES" => "N",
							"SHOW_OTHERS" => "N",
							"PAGE" => SITE_DIR."",
							"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS") ,
							"CATEGORY_0" => array(
								0 => "iblock_catalog",
							),
							"CATEGORY_0_iblock_catalog" => array(
								0 => "all",
							),
							"CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
							"SHOW_INPUT" => "Y",
							"INPUT_ID" => "title-search-input",
							"CONTAINER_ID" => "search",
							"PRICE_CODE" => array(
								0 => "BASE",
							),
							"SHOW_PREVIEW" => "Y",
							"PREVIEW_WIDTH" => "75",
							"PREVIEW_HEIGHT" => "75",
							"CONVERT_CURRENCY" => "Y",
							"USE_LANGUAGE_GUESS" => "N",
						),
						false
					);?>
				</div>-->
			</div>
			<?endif?>
		</div>
		<br>
		<div class="white-bg">
			<div class="container-fluid">
				<div class="row" style="background: #fff;">
					<div class="col-md-12 hidden-xs kavin">
						<nav class="navbar navbar-default">
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_menu" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						</div>
						<div class="collapse navbar-collapse" id="main_menu">
						  <ul class="nav navbar-nav">
							<li class="dropdown visible-lg-block js-catalog-menu-btn catalog-menu-btn" style="position: initial; width: 16.66666667%;">
							  <a href="#" class="dropdown-toggle text-center" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="min-width: 200px;">Каталог <span class="hamburger"></span></a>
							  <?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("top-catalof-menu-frame-top");?>
							  <div class="dropdown-menu category-menu">
								<?
									$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", array(
										"ROOT_MENU_TYPE" => "left",
										"MENU_CACHE_TYPE" => "N",
										"MENU_CACHE_TIME" => "36000000",
										"MENU_CACHE_USE_GROUPS" => "Y",
										"MENU_THEME" => "site",
										"CACHE_SELECTED_ITEMS" => "N",
										"MENU_CACHE_GET_VARS" => array(
										),
										"MAX_LEVEL" => "3",
										"CHILD_MENU_TYPE" => "left",
										"USE_EXT" => "Y",
										"DELAY" => "N",
										"ALLOW_MULTI_SELECT" => "N",
										"REGION" => $_SESSION['SOTBIT_REGIONS']['ID'],
									),
									false
								);?>
							  </div>
							  <?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("top-catalof-menu-frame-top", "");?>
							</li>
						   <?if($USER->IsAuthorized() && strpos($APPLICATION->GetCurPage(), '/personal/') !== false):?>
								<li class="icon-l order-i"><a href="/personal/orders/?filter_history=Y">
									<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 128 160" x="0px" y="0px"><path class="cls-1" d="M102,40H80a8,8,0,0,1-8-8V10" style="fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:8px;" /><path class="cls-1" d="M72.69,8H32a8,8,0,0,0-8,8v96a8,8,0,0,0,8,8H96a8,8,0,0,0,8-8V39.31a8,8,0,0,0-2.34-5.66L78.34,10.34A8,8,0,0,0,72.69,8Z" style="fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:8px;" /></svg>
									</span>
									Мои заказы</a></li>
								<li class="icon-l"><a href="/personal/cart/">
									<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 48 60" style="enable-background:new 0 0 48 48;" xml:space="preserve"><g><path d="M45.2,16.5h-3.5L31.2,3.2c-0.6-0.8-1.8-0.9-2.6-0.3c-0.8,0.6-0.9,1.7-0.3,2.5c0,0,0,0,0,0L37,16.5H10.8   l9.1-11.1c0.6-0.8,0.5-1.9-0.2-2.5c0,0,0,0,0,0c-0.8-0.6-2-0.5-2.6,0.3L6.1,16.5H2.8c-1.5,0-2.7,1.2-2.8,2.7V21   c0,1.4,1.1,2.5,2.4,2.7l5,18.5c0.2,0.8,1,3.2,3.2,3.2h26.7c1.7,0,2.8-1.5,3.2-3l5-18.7c1.4-0.1,2.5-1.3,2.5-2.7v-1.8   C48,17.7,46.7,16.5,45.2,16.5z M18,36.4c0,1.1-0.9,2-2,2s-2-0.9-2-2v-7.2c0-1.1,0.9-2,2-2s2,0.9,2,2V36.4L18,36.4z M26,36.4   c0,1.1-0.9,2-2,2s-2-0.9-2-2v-7.2c0-1.1,0.9-2,2-2s2,0.9,2,2V36.4L26,36.4z M34,36.4c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2v-7.2   c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2V36.4L34,36.4z"/></g></svg>
									</span>
									Корзина</a></li>
								<li class="icon-l"><a href="/personal/private/">
									<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 125"><path d="M337,225 l526,0 c0,0,24,100,-23,125 l-134,71 c-25,13,-36,28,-36,41 c0,32,62,75,62,184 c0,92,-49,154,-131,154 c-92,0,-133,-77,-133,-154 c0,-101,61,-147,61,-181 c0,-14,-9,-31,-34,-44 l-134,-71 c-47,-25,-24,-125,-24,-125 Z M50,0 c-50,0,-50,0,-50,50 l0,1000 c0,50,0,50,50,50 l300,0 l75,-100 l725,0 c50,0,50,0,50,-50 l0,-900 c0,-50,0,-50,-50,-50 Z M50,0 " fill="#000000" stroke="none" stroke-dasharray="none" stroke-linecap="inherit" stroke-linejoin="inherit" stroke-width="1" transform="matrix(0.0666666666667,0.0,0.0,-0.0666666666667,10.0,90.0)"/></svg></span>
									Личные данные
								</a></li>
								<!--<li class="icon-l">
									<a href="#">
										<span class="ic"><svg xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 90 112.5" enable-background="new 0 0 90 90" xml:space="preserve"><switch><foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"/><g i:extraneous="self"><g><path d="M42.796,40.815c0,3.537-1.256,6.617-3.425,8.329l-4.105,3.193c-1.027,0.799-1.71,2.282-1.597,4.564L34.694,89H24.429     l0.913-32.099c0.113-2.282-0.571-3.652-1.597-4.45l-4.108-3.308c-2.165-1.712-3.308-4.676-3.308-8.329     c0-15.97,1.255-25.894,1.826-35.818c0.228-2.509,0.684-3.652,1.824-3.652c1.142,0,1.599,1.143,1.825,3.652l1.255,20.076     c0.229,1.14,0.912,1.939,1.598,1.939c0.685,0,1.37-0.799,1.597-1.939l1.598-20.076c0.112-2.625,0.684-3.652,1.71-3.652     c1.027,0,1.484,1.027,1.711,3.652l1.599,20.076c0,1.14,0.685,1.939,1.482,1.939c0.912,0,1.597-0.799,1.597-1.939l1.368-20.076     c0.115-2.509,0.687-3.652,1.826-3.652c1.027,0,1.597,1.143,1.711,3.652C41.54,14.921,42.796,24.845,42.796,40.815z"/><path d="M72.757,57.242c0,4.563,0.686,26.282,0.914,31.758H62.148l1.256-27.307c0-1.712-0.571-3.082-1.597-3.768     c-1.597-1.024-6.844-2.85-6.844-6.845c0-24.868,5.359-44.603,13.459-49.394c2.511-1.596,4.334-0.455,4.334,3.879V57.242z"/></g></g></switch></svg></span>
										Рецепты
									</a></li>-->
								<li class="icon-l">
									<a href="/personal/favorite/">
										<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 125" style="enable-background:new 0 0 100 100;" xml:space="preserve"><g><path d="M87.3,19.1C78.1,8.3,61.5,3.5,49.9,16c-0.1,0.1-0.2,0.1-0.3,0c-12.8-12.6-29-7.7-37.4,3.1C-1.9,37.4,6,50.8,22.3,67.2   l27.4,25.2l27.5-25.3C93.6,50.8,102.4,36.6,87.3,19.1z M83.9,40.7c-1.4,4.9-4.8,3.7-3.5,0.2c1.7-4.5,0.5-8.3-3.2-12.6   c-2.8-3.3-6.8-5.3-10.4-5.3c-2.7,0-5.1,1.1-7.2,3.2c-2.5,2.5-4-0.6-2.3-2.3c2.9-3,6.2-4.2,9.5-4.2c4.8,0,9.6,2.6,12.9,6.4   C84.2,31.3,85.2,36,83.9,40.7z"/></g></svg></span>
										 Избранное
									</a></li>
								<!--<li class="icon-l">
									<a href="#">
										<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 125" enable-background="new 0 0 100 100" xml:space="preserve"><path d="M68.496,31.25c0.529-0.354,1.043-0.73,1.523-1.146c1.294-1.131,2.401-2.498,3.201-4.077c0.799-1.576,1.28-3.378,1.279-5.276  c0.001-3.457-1.243-6.699-3.467-9.098c-1.109-1.197-2.465-2.182-4.001-2.857C65.498,8.115,63.785,7.748,62,7.75  c-1.832,0-3.559,0.229-5.161,0.714c-2.402,0.72-4.513,2.062-6.086,3.866c-0.841,0.959-1.52,2.035-2.071,3.187  c-2.039-1.725-4.746-2.777-7.682-2.767c-2.889-0.002-5.535,1.18-7.425,3.075c-1.895,1.89-3.077,4.536-3.075,7.425  c-0.002,1.594,0.433,3.095,1.12,4.375c0.79,1.468,1.888,2.671,3.151,3.625H10v20h80v-20H68.496z M54.02,22.316  c0.05-0.437,0.102-0.834,0.153-1.162c0.051-0.326,0.104-0.588,0.137-0.71c0.262-1.03,0.615-1.881,1.031-2.57  c0.634-1.034,1.366-1.726,2.386-2.255c1.02-0.521,2.393-0.868,4.273-0.869c0.838,0.001,1.562,0.166,2.207,0.449  c0.962,0.426,1.763,1.131,2.351,2.078c0.585,0.947,0.942,2.131,0.942,3.473c-0.001,0.725-0.176,1.422-0.526,2.118  c-0.521,1.04-1.474,2.052-2.659,2.765c-1.181,0.719-2.56,1.121-3.814,1.117c-1.38,0-6.651,0-6.833,0  C53.695,26.262,53.921,23.191,54.02,22.316z M38.525,20.775c0.644-0.639,1.497-1.023,2.475-1.025  c1.354,0.01,2.355,0.429,3.222,1.157c0.856,0.731,1.543,1.831,1.883,3.191c0.01,0.037,0.046,0.216,0.079,0.45  c0.059,0.416,0.182,2.015,0.193,2.201c-1.648,0-3.435,0-4.377,0c-0.493,0.001-1.069-0.105-1.637-0.322  c-0.854-0.318-1.663-0.896-2.162-1.504c-0.253-0.303-0.432-0.607-0.542-0.887c-0.111-0.28-0.158-0.531-0.159-0.787  C37.502,22.271,37.887,21.419,38.525,20.775z"/><g><path d="M15,56.25v26c0,5.5,4.5,10,10,10h50c5.5,0,10-4.5,10-10v-26H15z"/></g></svg></span>
									 Бонусы
									</a></li>-->
								<li class="icon-l">
									<a href="#" data-modal-wrap=".callback-form">
										<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 122.5" version="1.1" x="0px" y="0px"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g fill="#000000"><g transform="translate(23.000000, 57.000000)"><path d="M6.4551,3.1904 C6.4551,4.7834 5.1641,6.0734 3.5721,6.0734 C1.9781,6.0734 0.6871,4.7834 0.6871,3.1904 C0.6871,1.5984 1.9781,0.3064 3.5721,0.3064 C5.1641,0.3064 6.4551,1.5984 6.4551,3.1904"/><path d="M17.832,3.1904 C17.832,4.7834 16.54,6.0734 14.948,6.0734 C13.355,6.0734 12.065,4.7834 12.065,3.1904 C12.065,1.5984 13.355,0.3064 14.948,0.3064 C16.54,0.3064 17.832,1.5984 17.832,3.1904"/><path d="M29.208,3.1904 C29.208,4.7834 27.918,6.0734 26.325,6.0734 C24.734,6.0734 23.442,4.7834 23.442,3.1904 C23.442,1.5984 24.734,0.3064 26.325,0.3064 C27.918,0.3064 29.208,1.5984 29.208,3.1904"/></g><g transform="translate(7.000000, 11.399400)"><path d="M72.1758,22.2607 C70.5838,22.2607 69.2928,20.9687 69.2928,19.3757 C69.2928,17.7837 70.5838,16.4947 72.1758,16.4947 C73.7698,16.4947 75.0588,17.7837 75.0588,19.3757 C75.0588,20.9687 73.7698,22.2607 72.1758,22.2607 M60.7998,22.2607 C59.2058,22.2607 57.9158,20.9687 57.9158,19.3757 C57.9158,17.7837 59.2058,16.4947 60.7998,16.4947 C62.3928,16.4947 63.6828,17.7837 63.6828,19.3757 C63.6828,20.9687 62.3928,22.2607 60.7998,22.2607 M32.6438,67.0157 C25.9638,67.0157 19.8948,65.5087 14.6088,62.5347 C14.2588,62.3397 13.8328,62.3417 13.4878,62.5427 C13.1418,62.7437 12.9288,63.1137 12.9268,63.5117 L12.8838,71.9117 C9.7428,68.2127 3.7658,60.4947 2.6318,54.1527 C2.0748,51.0417 1.3628,43.1107 6.7508,36.6737 C11.7188,30.7397 20.5298,27.7307 32.9418,27.7307 C47.0878,27.7307 56.9678,35.4407 56.9678,46.4827 C56.9678,56.4367 48.4438,67.0157 32.6438,67.0157 M49.4228,16.4947 C51.0138,16.4947 52.3058,17.7837 52.3058,19.3757 C52.3058,20.9687 51.0138,22.2607 49.4228,22.2607 C47.8298,22.2607 46.5398,20.9687 46.5398,19.3757 C46.5398,17.7837 47.8298,16.4947 49.4228,16.4947 M82.1568,9.8237 C77.0338,3.7037 68.1568,0.6007 55.7738,0.6007 C41.1368,0.6007 30.9138,8.7797 30.9138,20.4897 C30.9138,22.1577 31.1538,23.8407 31.5988,25.4947 C19.1938,25.7147 10.2498,28.9807 5.0218,35.2267 C-0.9772,42.3917 -0.2012,51.1267 0.4098,54.5497 C2.0728,63.8487 12.7258,75.1697 13.1778,75.6477 C13.3958,75.8767 13.6938,75.9997 13.9968,75.9997 C14.1368,75.9997 14.2778,75.9737 14.4118,75.9197 C14.8398,75.7507 15.1218,75.3377 15.1248,74.8777 L15.1728,65.3827 C20.3938,67.9637 26.2608,69.2727 32.6438,69.2727 C49.9078,69.2727 59.2248,57.5307 59.2248,46.4827 C59.2248,44.9057 59.0468,43.3907 58.7078,41.9457 C63.7128,41.6347 68.5248,40.4717 72.6308,38.5367 L71.4888,47.9437 C71.4318,48.4237 71.6868,48.8887 72.1238,49.0967 C72.2798,49.1707 72.4458,49.2067 72.6108,49.2067 C72.9078,49.2067 73.2008,49.0877 73.4188,48.8647 C73.8898,48.3807 84.9478,36.9097 86.5228,28.1117 C87.1008,24.8717 87.8348,16.6077 82.1568,9.8237"/></g></g></g></svg></span>
										 Обратная связь
									</a></li>
								<li class="icon-l">
									<a href="/?logout=yes">
										<span class="ic"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 16 20" version="1.1" x="0px" y="0px"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g fill="#000000"><polygon points="12 10 8 6 4 10 7 10 7 16 9 16 9 10"/><polygon points="0 14 4 14 4 12 2 12 2 2 14 2 14 12 12 12 12 14 16 14 16 0 0 0"/></g></g></svg></span>
										Выход
									</a>
								</li>
						   <?else:?>
								<li><a href="/coupons/">Купоны</a></li>
								<li><a href="/sales/">Скидки</a></li>
							   <?
								$currentUrl = "/";
								$iZ = 1;
								$iMax = 5;
								$arFilter = Array("IBLOCK_ID" => 12, "ACTIVE" => "Y");
								$dbMenuList = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("CODE", "NAME", "ID"));
								$arMenu = array('DISPLAY_ITEMS' => array(), 'HIDDEN_ITEMS' => array());

								while($arMenuItem = $dbMenuList->GetNext()){

									if($iZ < $iMax){
										$arMenu['DISPLAY_ITEMS'][] = $arMenuItem;
									}
									else{
										$arMenu['HIDDEN_ITEMS'][] = $arMenuItem;
									}
									$iZ++;
								}

								foreach($arMenu['DISPLAY_ITEMS'] as $arMenuItem){?>
									<li><a href="<?=$currentUrl . 'category-' . $arMenuItem['ID']?>/"><?=$arMenuItem['NAME']?></a></li>
								<?}?>

								<?if(count($arMenu['HIDDEN_ITEMS']) > 0){?>
								<li class="dropdown right-aligned" style="position: initial;">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ещё <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<?foreach($arMenu['HIDDEN_ITEMS'] as $arMenuItem){?>
										<li><a href="<?=$currentUrl . 'category-' . $arMenuItem['ID']?>/"><?=$arMenuItem['NAME']?></a></li>
										<?}?>
									</ul>
								</li>
								<?}?>
							<?endif;?>
						  </ul>
						</div><!-- /.navbar-collapse -->
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>
	<script type="text/javascript">
		window.onscroll = function(){
			if($(window).width() > 768) {
				if($(window).scrollTop() >= 138){
					if(!$('.ln2header').hasClass("fixed-top-menu")){
						$('.ln2header').addClass('fixed-top-menu');
					}
					if(!$('.js-catalog-menu-btn').hasClass("fixed")){
						$('.js-catalog-menu-btn').addClass('fixed');
					}
				} else {
					$('.ln2header').removeClass('fixed-top-menu');
					$('.js-catalog-menu-btn').removeClass('fixed');
				}
			}
		};
	</script>
	<div class="container-fluid">
		<?if($curPage != SITE_DIR."index.php" || isset($_GET['q'])):?>
		<br>
		<?/*div class="row">
			<div class="col-lg-2">
				<a href="#" class="btn btn-lg btn-default btn-block"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Назад к списку товаров</a>
			</div>
			<div class="col-lg-10" id="navigation">?>
				<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
						"START_FROM" => "0",
						"PATH" => $APPLICATION->GetCurPage(),
						"SITE_ID" => SITE
					),
					false,
					Array('HIDE_ICONS' => 'Y')
				);?>
			</div>
		</div>
		<h1 class="bx-title dbg_title" id="pagetitle"><?=$APPLICATION->ShowTitle(false);?></h1>*/?>
		<?endif?>
	</div>

	<div class="workarea">
		<? if(!$isCatalog && !$isMain) { ?>
		<div class="breadcrumbs-wrap<?$APPLICATION->ShowProperty("BREADCRUMBS_STYLE");?>">
			<div class="container">
				<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
						"START_FROM" => "0",
						"PATH" => "",
						"SITE_ID" => "-"
					),
					false,
					Array('HIDE_ICONS' => 'Y')
				);?>
			</div>
		</div>
		<? } ?>
	<? //if ($curPage == "/index.php") :
		//$needSidebar = true;
	//else : ?>
		<div class="container-fluid bx-content-seection"><!--div class="container bx-content-seection"-->
			<div class="row">
			<?$needSidebar = false; //preg_match("~^".SITE_DIR."(catalog|cart|personal)/~", $curPage);?>
				<div class="bx-content <?=(!$needSidebar ? "col-sm-12" : "col-md-9 col-sm-8")?>">
				<!--CONTENT_START-->
	<? //endif; ?>

<?
// SET CANONICAL 

$currentUrl = \Bitrix\Main\Context::getCurrent()->getRequest()->getRequestUri();
$server = \Bitrix\Main\Context::getCurrent()->getServer();
$uri = new \Bitrix\Main\Web\Uri($currentUrl);
$currentPath = $uri->getPath();

if(preg_match('#^.+?\/filter\/.+?\/apply\/$#', $currentPath)) {
	
	$currentPath = preg_replace('#filter\/.+?\/apply\/$#', '', $currentPath);
}

$canonical = $server['HTTP_X_FORWARDED_SCHEME'] . '://' . $server['SERVER_NAME'] . $currentPath;

$APPLICATION->SetPageProperty('canonical', $canonical);
?>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SPPA_DEFAULT_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("SPPA_DEFAULT_TEMPLATE_DESCRIPTION"),
	"ICON" => "/images/sale_profile_add.gif",
	"PATH" => array(
		"ID" => "yenisite",
        "NAME" => GetMessage("YENISITE_COMPONENTS"),
		"CHILD" => array(
			"ID" => "personal_ys",
			"NAME" => GetMessage("YENISITE_DESC_PERSONAL"),
			"SORT" => 30
				),
			),
);
?>
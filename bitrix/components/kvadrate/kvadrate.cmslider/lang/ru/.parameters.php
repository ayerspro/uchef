<?
$MESS['IBLOCK_PARAMS_NAME'] = "Параметры инфоблока";
$MESS['IBLOCK_ID_NAME'] = "Инфоблок";
$MESS['CM_SETTINGS_NAME'] = "Настройки слайдера";
$MESS['CM_SETTINGS_AUTO'] = "Автоматическая прокрутка";
$MESS['CM_SETTINGS_AUTO_YES'] = "Включить";
$MESS['Cm_SETTINGS_AUTO_NO'] = "Выключить";
$MESS['CM_SETTINGS_TIME'] = "Время задержки слайда в секундах";
$MESS["IBLOCK_ELEMENT_SORT_FIELD"] = "По какому полю сортируем слайды";
$MESS["IBLOCK_ELEMENT_SORT_ORDER"] = "Порядок сортировки слайдов";
$MESS["IBLOCK_SORT_SHOWS"] = "количество просмотров в среднем";
$MESS["IBLOCK_SORT_SORT"] = "индекс сортировки";
$MESS["IBLOCK_SORT_TIMESTAMP"] = "дата изменения";
$MESS["IBLOCK_SORT_NAME"] = "название";
$MESS["IBLOCK_SORT_ID"] = "ID";
$MESS["IBLOCK_SORT_ACTIVE_FROM"] = "дата активности (с)";
$MESS["IBLOCK_SORT_ACTIVE_TO"] = "дата активности (по)";
$MESS["IBLOCK_SORT_ASC"] = "по возрастанию";
$MESS["IBLOCK_SORT_DESC"] = "по убыванию";
?>
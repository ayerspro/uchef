<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!CModule::IncludeModule("iblock"))
        return;
$arIBlock=array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("ACTIVE"=>"Y"));
$arAscDesc = array(
	"asc" => GetMessage("IBLOCK_SORT_ASC"),
	"desc" => GetMessage("IBLOCK_SORT_DESC"),
);
while($arr=$rsIBlock->Fetch())
{
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}
$arComponentParameters = array(
	"GROUPS" => array(
		"IBLOCK_PARAMS" => array(
			"NAME" => GetMessage("IBLOCK_PARAMS_NAME")
		),
		"CM_SETTINGS" => array(
			"NAME" => GetMessage("CM_SETTINGS_NAME")
		)
	),
	"PARAMETERS" => array(
		"IBLOCK_ID" => array(
			"PARENT" => "IBLOCK_PARAMS",
			"NAME" => GetMessage("IBLOCK_ID_NAME"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y"
		),
		"CM_SETTINGS_AUTO" => array(
			"PARENT" => "CM_SETTINGS",
			"NAME" => GetMessage("CM_SETTINGS_AUTO"),
			"TYPE" => "LIST",
			"VALUES" => array(
				1 => GetMessage("CM_SETTINGS_AUTO_YES"),
				0 => GetMessage("CM_SETTINGS_AUTO_NO"),
			),
			"DEFAULT" => "1",
		),
		"CM_SETTINGS_TIME" => array(
			"PARENT" => "CM_SETTINGS",
			"NAME" => GetMessage("CM_SETTINGS_TIME"),
			"TYPE" => "LIST",
			"VALUES" => array(
				"3000" => 3,
				"4000" => 4,
				"5000" => 5,
				"6000" => 6,
				"7000" => 7,
				"8000" => 8,
				"9000" => 9,
				"10000" => 10,
				"11000" => 11,
				"12000" => 12,
				"13000" => 13,
				"14000" => 14,
				"15000" => 15,
			),
			"DEFAULT" => "10000",
		),
		"ELEMENT_SORT_FIELD" => array(
			"PARENT" => "IBLOCK_PARAMS",
			"NAME" => GetMessage("IBLOCK_ELEMENT_SORT_FIELD"),
			"TYPE" => "LIST",
			"VALUES" => array(
				"shows" => GetMessage("IBLOCK_SORT_SHOWS"),
				"sort" => GetMessage("IBLOCK_SORT_SORT"),
				"timestamp_x" => GetMessage("IBLOCK_SORT_TIMESTAMP"),
				"name" => GetMessage("IBLOCK_SORT_NAME"),
				"id" => GetMessage("IBLOCK_SORT_ID"),
				"active_from" => GetMessage("IBLOCK_SORT_ACTIVE_FROM"),
				"active_to" => GetMessage("IBLOCK_SORT_ACTIVE_TO"),
			),
			"ADDITIONAL_VALUES" => "Y",
			"DEFAULT" => "sort",
		),
		"ELEMENT_SORT_ORDER" => array(
			"PARENT" => "IBLOCK_PARAMS",
			"NAME" => GetMessage("IBLOCK_ELEMENT_SORT_ORDER"),
			"TYPE" => "LIST",
			"VALUES" => $arAscDesc,
			"DEFAULT" => "asc",
			"ADDITIONAL_VALUES" => "Y",
		),		
		"CACHE_TIME" => array(),
   )
);
?>
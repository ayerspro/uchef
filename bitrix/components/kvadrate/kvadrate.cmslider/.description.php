<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("NAME"),
    "DESCRIPTION" => GetMessage("DESCR"),
    "VERSION" => "1.5.3",
	"PATH" => array("ID" => "carmine",
        "NAME" => GetMessage("KVADRATE_GROUP_NAME"),
		"CHILD" => array("ID" => "system",
		    "NAME" => GetMessage("SYSTEM_GROUP_NAME"),
	    ),
	),
);
?>
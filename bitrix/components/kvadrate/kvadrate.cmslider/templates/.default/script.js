
function cmSlider() {

    // SETTINGS

    var auto = cmAuto,
        interval = cmInterval,
        lastSlideNum = 5;

    // SIZES & PROPORTIONS

    var slWidth = $('#cm-slider').width();

    if(slWidth > 768) {

        var slHeight = Math.round(slWidth / 2.9),
        slTitlePos = Math.round(slHeight / 8),
        slMaxWidth = Math.round((slWidth / 2) - (slWidth / 10)),
        slTitleMaxHeight = Math.round(slHeight / 7),
        slDescrMaxHeight = Math.round((slHeight / 13) * 4),
        margTopDescr = Math.round(slHeight / 30),
        margTopButton = Math.round(slHeight / 12),
        slTitleSize = Math.round((slHeight / 12)) + 'px',
        slDescrSize = Math.round((slHeight / 18)) + 'px',
        slButtonFont = Math.round((slHeight / 22)) + 'px',
        slButtonSizeV = Math.round((slHeight / 38)) + 'px',
        slButtonSizeH = Math.round((slHeight / 18)) + 'px',
        slImgWidth = Math.round($('.sl-img').width() ),
        slImgMaxHeight = Math.round(slHeight - (slTitlePos * 2)),
        slBlockLeftPos = Math.round((slWidth / 2) + (slWidth / 30)),
        slImgLeftPos = Math.round((slWidth / 2) - slMaxWidth);

    } else if((slWidth <= 768) && (slWidth > 480)) {

        var slHeight = Math.round(slWidth / 2),
        slTitlePos = Math.round(slHeight / 9),
        slMaxWidth = Math.round((slWidth / 2) - (slWidth / 10)),
        slTitleMaxHeight = Math.round(slHeight / 7),
        slDescrMaxHeight = Math.round((slHeight / 13) * 4),
        margTopDescr = Math.round(slHeight / 30),
        margTopButton = Math.round(slHeight / 12),
        slTitleSize = Math.round((slHeight / 16)) + 'px',
        slDescrSize = Math.round((slHeight / 22)) + 'px',
        slButtonFont = Math.round((slHeight / 22)) + 'px',
        slButtonSizeV = Math.round((slHeight / 38)) + 'px',
        slButtonSizeH = Math.round((slHeight / 18)) + 'px',
        slImgWidth = Math.round($('.sl-img').width() ),
        slImgMaxHeight = Math.round(slHeight - (slTitlePos * 2)),
        slBlockLeftPos = Math.round((slWidth / 2) + (slWidth / 30)),
        slImgLeftPos = Math.round((slWidth / 2) - slMaxWidth);

    } else if(slWidth <= 480) {

        var slHeight = Math.round(slWidth / 1.5),
        slTitlePos = Math.round(slHeight / 9),
        slMaxWidth = Math.round((slWidth / 2) - (slWidth / 10)),
        slTitleMaxHeight = Math.round(slHeight / 7),
        slDescrMaxHeight = Math.round((slHeight / 13) * 4),
        margTopDescr = Math.round(slHeight / 30),
        margTopButton = Math.round(slHeight / 12),
        slTitleSize = Math.round((slHeight / 16)) + 'px',
        slDescrSize = Math.round((slHeight / 22)) + 'px',
        slButtonFont = Math.round((slHeight / 22)) + 'px',
        slButtonSizeV = Math.round((slHeight / 38)) + 'px',
        slButtonSizeH = Math.round((slHeight / 18)) + 'px',
        slImgWidth = Math.round($('.sl-img').width() ),
        slImgMaxHeight = Math.round(slHeight - (slTitlePos * 2)),
        slBlockLeftPos = Math.round((slWidth / 2) + (slWidth / 30)),
        slImgLeftPos = Math.round((slWidth / 2) - slMaxWidth);

    };

    // CSS 

    $('.cm-slide').first().addClass('cm-slide-active');
    $('.cm-slide-button').first().addClass('cm-slide-button-active');
    $('#cm-slider').css({'height' : slHeight});
    $('.sl-title').css({'font-size' : slTitleSize, 'max-width' : slMaxWidth, 'max-height' : slTitleMaxHeight});
    $('.sl-descr').css({'font-size' : slDescrSize, 'max-width' : slMaxWidth, 'max-height' : slDescrMaxHeight});
    $('.sl-button').css({'paddingTop' : slButtonSizeV, 'paddingBottom' : slButtonSizeV, 'paddingLeft' : slButtonSizeH, 'paddingRight' : slButtonSizeH, 'fontSize' : slButtonFont}),
    slTitleHeight = slTitleSize,
    slDescrPos = Math.round(slTitlePos + slTitleMaxHeight + margTopDescr),
    slButtonPos = Math.round(slDescrPos + slDescrMaxHeight + margTopButton);       
    $('.sl-img').css({'top' : slTitlePos, 'left' : slImgLeftPos, 'max-width' : slMaxWidth, 'max-height' : slImgMaxHeight});
    $('.sl-title').css({'top' : slTitlePos, 'left' : slBlockLeftPos});              
    $('.sl-descr').css({'top' : slDescrPos, 'left' : slBlockLeftPos});        
    $('.sl-button').css({'top' : slButtonPos, 'left' : slBlockLeftPos});

    // Terms

    if(auto == 1){
        id = setInterval(autoStart, interval);
    };

    slRun();

    $('body').delegate('#cm-next-slide', 'click', function(){
        clearInterval(id);
        autoStart();
    });

    $('body').delegate('#cm-prew-slide', 'click', function(){
        clearInterval(id);
        slHide();
        var clickedElem = $('.cm-slide-active').index() - 1;
        $('.cm-slide-button').removeClass('cm-slide-button-active');
        $('.cm-slide-button').eq(clickedElem).addClass('cm-slide-button-active');
        $('.cm-slide-before').removeClass('cm-slide-before');
        $('.cm-slide-active').addClass('cm-slide-before').removeClass('cm-slide-active');
        $('.cm-slide-item').eq(clickedElem).addClass('cm-slide-active');
        $('.cm-slide-before').animate({left: '+=100%'}, 400, 'easeOutExpo');
        $('.cm-slide-active').animate({left: '-100%'}, 0).animate({left: '+=100%'}, 400, 'easeOutExpo');

        slRun();
    });

    $('body').delegate('.cm-slide-button', 'click', function(){
        clearInterval(id);
        slHide();
        var clickedElem = $(this).index();
        $('.cm-slide-before').removeClass('cm-slide-before');
        $('.cm-slide-active').addClass('cm-slide-before').removeClass('cm-slide-active');
        $('.cm-slide-item').eq(clickedElem).addClass('cm-slide-active');
        $('.cm-slide-button').removeClass('cm-slide-button-active');
        $(this).addClass('cm-slide-button-active');
        if(clickedElem > $('.cm-slide-before').index()){
            $('.cm-slide-before').animate({left: '-=100%'}, 400, 'easeOutExpo');
            $('.cm-slide-active').animate({left: '100%'}, 0).animate({left: '-=100%'}, 400, 'easeOutExpo');
        }
        else{
            $('.cm-slide-before').animate({left: '+=100%'}, 400, 'easeOutExpo');
            $('.cm-slide-active').animate({left: '-100%'}, 0).animate({left: '+=100%'}, 400, 'easeOutExpo');
        };

        slRun();

    }); 

    // Functions

    function autoStart(){

        slHide();

        var clickedElem = $('.cm-slide-active').index() + 1;

        $('.cm-slide-button').removeClass('cm-slide-button-active');

        if(clickedElem == $('.cm-slide-item').size()){
            $('.cm-slide-button').eq(0).addClass('cm-slide-button-active');
            $('.cm-slide-before').removeClass('cm-slide-before');
            $('.cm-slide-active').addClass('cm-slide-before').removeClass('cm-slide-active');
            $('.cm-slide-item').eq(0).addClass('cm-slide-active');
        }
        else {
            $('.cm-slide-button').eq(clickedElem).addClass('cm-slide-button-active');
            $('.cm-slide-before').removeClass('cm-slide-before');
            $('.cm-slide-active').addClass('cm-slide-before').removeClass('cm-slide-active');
            $('.cm-slide-item').eq(clickedElem).addClass('cm-slide-active');
        };

        $('.cm-slide-before').animate({left: '-=100%'}, 400, 'easeOutExpo');
        $('.cm-slide-active').animate({left: '100%'}, 0).animate({left: '-=100%'}, 400, 'easeOutExpo');

        slRun();

    };

    function slRun(){

        var slWidth = $('#cm-slider').width(),
        slMaxWidth = Math.round((slWidth / 2) - (slWidth / 10)),
        slImgLeftPos = Math.round((slWidth / 2) - slMaxWidth),
        slBlockLeftPos = Math.round((slWidth / 2) + (slWidth / 30));
        $('.sl-img').delay(300).animate({left : '-100%'}, 0).animate({opacity: '1', left: slImgLeftPos}, 300, 'easeOutExpo');   
        $('.sl-title').delay(300).animate({left : slWidth}, 0).animate({opacity: '1', left: slBlockLeftPos}, 300, 'easeOutExpo');  
        $('.sl-descr').delay(400).animate({left : slWidth}, 0).animate({opacity: '1', left: slBlockLeftPos}, 300, 'easeOutExpo');      
        $('.sl-button').delay(500).animate({left : slWidth}, 0).animate({opacity: '1', left: slBlockLeftPos}, 300, 'easeOutExpo');		//$('.sl-button').delay(500).animate({right : '10%'}, 0).animate({opacity: '1', right: '10%'}, 300, 'easeOutExpo');

    };

    function slHide(){     

        $('.sl-img').animate({opacity: '0', left : '-100%'}, 0);     
        $('.sl-title').animate({opacity: '0', left: slWidth}, 0);       
        $('.sl-descr').animate({opacity: '0', left: slWidth}, 0);     
        $('.sl-button').animate({opacity: '0', left: slWidth}, 0);  

    };

    // RESIZING

    $(window).resize(function(){

    // SIZES & PROPORTIONS

        var slWidth = $('#cm-slider').width();
        
        if(slWidth > 768) {

            var slHeight = Math.round(slWidth / 2.9),
            slTitlePos = Math.round(slHeight / 8),
            slMaxWidth = Math.round((slWidth / 2) - (slWidth / 10)),
            slTitleMaxHeight = Math.round(slHeight / 7),
            slDescrMaxHeight = Math.round((slHeight / 13) * 4),
            margTopDescr = Math.round(slHeight / 30),
            margTopButton = Math.round(slHeight / 12),
            slTitleSize = Math.round((slHeight / 12)) + 'px',
            slDescrSize = Math.round((slHeight / 18)) + 'px',
            slButtonFont = Math.round((slHeight / 22)) + 'px',
            slButtonSizeV = Math.round((slHeight / 38)) + 'px',
            slButtonSizeH = Math.round((slHeight / 18)) + 'px',
            slImgWidth = Math.round($('.sl-img').width() ),
            slImgMaxHeight = Math.round(slHeight - (slTitlePos * 2)),
            slBlockLeftPos = Math.round((slWidth / 2) + (slWidth / 30)),
            slImgLeftPos = Math.round((slWidth / 2) - slMaxWidth);

        } else if((slWidth <= 768) && (slWidth > 480)) {

            var slHeight = Math.round(slWidth / 2),
            slTitlePos = Math.round(slHeight / 9),
            slMaxWidth = Math.round((slWidth / 2) - (slWidth / 10)),
            slTitleMaxHeight = Math.round(slHeight / 7),
            slDescrMaxHeight = Math.round((slHeight / 13) * 4),
            margTopDescr = Math.round(slHeight / 30),
            margTopButton = Math.round(slHeight / 12),
            slTitleSize = Math.round((slHeight / 16)) + 'px',
            slDescrSize = Math.round((slHeight / 22)) + 'px',
            slButtonFont = Math.round((slHeight / 22)) + 'px',
            slButtonSizeV = Math.round((slHeight / 38)) + 'px',
            slButtonSizeH = Math.round((slHeight / 18)) + 'px',
            slImgWidth = Math.round($('.sl-img').width() ),
            slImgMaxHeight = Math.round(slHeight - (slTitlePos * 2)),
            slBlockLeftPos = Math.round((slWidth / 2) + (slWidth / 30)),
            slImgLeftPos = Math.round((slWidth / 2) - slMaxWidth);

        } else if(slWidth <= 480) {

            var slHeight = Math.round(slWidth / 1.5),
            slTitlePos = Math.round(slHeight / 9),
            slMaxWidth = Math.round((slWidth / 2) - (slWidth / 10)),
            slTitleMaxHeight = Math.round(slHeight / 7),
            slDescrMaxHeight = Math.round((slHeight / 13) * 4),
            margTopDescr = Math.round(slHeight / 30),
            margTopButton = Math.round(slHeight / 12),
            slTitleSize = Math.round((slHeight / 16)) + 'px',
            slDescrSize = Math.round((slHeight / 22)) + 'px',
            slButtonFont = Math.round((slHeight / 22)) + 'px',
            slButtonSizeV = Math.round((slHeight / 38)) + 'px',
            slButtonSizeH = Math.round((slHeight / 18)) + 'px',
            slImgWidth = Math.round($('.sl-img').width() ),
            slImgMaxHeight = Math.round(slHeight - (slTitlePos * 2)),
            slBlockLeftPos = Math.round((slWidth / 2) + (slWidth / 30)),
            slImgLeftPos = Math.round((slWidth / 2) - slMaxWidth);

        };

        // CSS 

        $('.cm-slide').first().addClass('cm-slide-active');
        $('.cm-slide-button').first().addClass('cm-slide-button-active');
        $('#cm-slider').css({'height' : slHeight});
        $('.sl-title').css({'font-size' : slTitleSize, 'max-width' : slMaxWidth, 'max-height' : slTitleMaxHeight});
        $('.sl-descr').css({'font-size' : slDescrSize, 'max-width' : slMaxWidth, 'max-height' : slDescrMaxHeight});
        $('.sl-button').css({'paddingTop' : slButtonSizeV, 'paddingBottom' : slButtonSizeV, 'paddingLeft' : slButtonSizeH, 'paddingRight' : slButtonSizeH, 'fontSize' : slButtonFont}),
        slTitleHeight = slTitleSize,
        slDescrPos = Math.round(slTitlePos + slTitleMaxHeight + margTopDescr),
        slButtonPos = Math.round(slDescrPos + slDescrMaxHeight + margTopButton);       
        $('.sl-img').css({'top' : slTitlePos, 'left' : slImgLeftPos, 'max-width' : slMaxWidth, 'max-height' : slImgMaxHeight});
        $('.sl-title').css({'top' : slTitlePos, 'left' : slBlockLeftPos});              
        $('.sl-descr').css({'top' : slDescrPos, 'left' : slBlockLeftPos});        
        $('.sl-button').css({'top' : slButtonPos, 'left' : slBlockLeftPos});

    });

};
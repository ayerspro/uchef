<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div id="cm-slider">    
    <?foreach($arResult as $res):?>
        <div class="cm-slide cm-slide-item" style="background: url(<?=CFile::GetPath($res['PROPERTY_SL_BACKGROUND_VALUE']);?>) no-repeat; background-size: 100% 100%; background-position: 50% 50%;">
          
            <div class="sl-descr"><span><?=$res["PROPERTY_SL_DESCRIPTION_VALUE"];?></span></div>
            <?if($res['PROPERTY_SL_BUTTON_LINK_VALUE'] && $res['PROPERTY_SL_BUTTON_TEXT_VALUE']):?>
            <a href="<?=$res['PROPERTY_SL_BUTTON_LINK_VALUE'];?>">
                <span class="sl-button"><?=$res['PROPERTY_SL_BUTTON_TEXT_VALUE'];?></span>
            </a>
            <?endif;?>
            <?if($res['PROPERTY_SL_BUTTON_LINK_VALUE'] && $res['PROPERTY_SL_BUTTON_TEXT_VALUE']):?>
            <a href="<?=$res['PROPERTY_SL_BUTTON_LINK_VALUE'];?>">
                <img class="sl-img" src="<?=CFile::GetPath($res['PROPERTY_SL_IMG_VALUE']);?>">
            </a>
            <?/*else:?>           
            <img class="sl-img"  src="<?=CFile::GetPath($res['PROPERTY_SL_IMG_VALUE']);?>">
            <?*/endif;?>
        </div>        
    <?endforeach;?>
    <span id="slide-manage">
        <?foreach($arResult as $res):?>
        <span class="cm-slide-button"></span>
        <?endforeach;?>
    </span>
    <span id="cm-prew-slide"></span>
    <span id="cm-next-slide"></span>
</div>
<script>
$(window).load(function(){
    cmAuto = 1;
    cmInterval = 5000;
    cmSlider();
});
</script>
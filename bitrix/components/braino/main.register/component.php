<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponent $this
 */

if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
	die();



global $USER_FIELD_MANAGER;

// apply default param values
$arDefaultValues = array(
	"SHOW_FIELDS" => array(),
	"REQUIRED_FIELDS" => array(),
	"AUTH" => "Y",
	"USE_BACKURL" => "Y",
	"SUCCESS_PAGE" => "",
);

foreach ($arDefaultValues as $key => $value)
{
	if (!is_set($arParams, $key))
		$arParams[$key] = $value;
}
if(!is_array($arParams["SHOW_FIELDS"]))
	$arParams["SHOW_FIELDS"] = array();
if(!is_array($arParams["REQUIRED_FIELDS"]))
	$arParams["REQUIRED_FIELDS"] = array();

// if user registration blocked - return auth form
if (COption::GetOptionString("main", "new_user_registration", "N") == "N")
	$APPLICATION->AuthForm(array());

$arResult["EMAIL_REQUIRED"] = (COption::GetOptionString("main", "new_user_email_required", "Y") <> "N");
$arResult["USE_EMAIL_CONFIRMATION"] = (COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y" && $arResult["EMAIL_REQUIRED"]? "Y" : "N");

// apply core fields to user defined

$arDefaultFields = array(
	"NAME",
	"EMAIL",
	"PASSWORD",
	"CONFIRM_PASSWORD"
);


$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
if($def_group <> "")
	$arResult["GROUP_POLICY"] = CUser::GetGroupPolicy(explode(",", $def_group));
else
	$arResult["GROUP_POLICY"] = CUser::GetGroupPolicy(array());

$arResult["SHOW_FIELDS"] = array_unique(array_merge($arDefaultFields, $arParams["SHOW_FIELDS"]));
$arResult["REQUIRED_FIELDS"] = array_unique(array_merge($arDefaultFields, $arParams["REQUIRED_FIELDS"]));


$arResult["AGREEMENT_ORIGINATOR_ID"] = "main/reg";
$arResult["AGREEMENT_ORIGIN_ID"] = "register";
$arResult["AGREEMENT_INPUT_NAME"] = "USER_AGREEMENT";


// ********************* User profile properties ***************************************************

$arResult["SALE_PROFILES"] = array();

$arResult["SHOW_FIELDS"][] = "PROFILE_TYPE";
$arResult["REQUIRED_FIELDS"][] = "PROFILE_TYPE";

$db_ptype = CSalePersonType::GetList(Array("SORT" => "ASC"), Array("LID"=>SITE_ID));
$bFirst = True;
while ($ptype = $db_ptype->Fetch())
{
	$ProfId = $ptype['ID'];
	
	$arResult["SALE_PROFILES"][$ProfId] = array();
	$arResult["SALE_PROFILES"][$ProfId]['PROFILE_TYPE'] = $ptype;
   
	if($ProfId == 2){
	   $orderPropertiesList = CSaleOrderProps::GetList(
			array("SORT" => "ASC", "NAME" => "ASC"),
			array(
				"PERSON_TYPE_ID" => $ProfId,
				"USER_PROPS" => "Y", "ACTIVE" => "Y", "UTIL" => "N", "PROPS_GROUP_ID" => 3
			),
			false,
			false,
			array("ID", "NAME", "TYPE", "REQUIED", "MULTIPLE", "IS_LOCATION", "PROPS_GROUP_ID", "IS_EMAIL", "IS_PROFILE_NAME", "IS_PAYER", "IS_LOCATION4TAX", "CODE", "SORT")
		);
		
		$arResult["SALE_PROFILES"][$ProfId]['PROFILE_PROPS'] = array();
		while ($orderProperty = $orderPropertiesList->GetNext())
		{
			$arResult["SHOW_FIELDS"][] = "PROFILE_PROP_" . $orderProperty['ID']; 
			$arResult["SALE_PROFILES"][$ProfId]["PROFILE_PROP_" . $orderProperty['ID']] = $orderProperty;
		}
	}
}
// ********************* /User profile properties ***************************************************


// use captcha?
$arResult["USE_CAPTCHA"] = COption::GetOptionString("main", "captcha_registration", "N") == "Y" ? "Y" : "N";

// start values
$arResult["VALUES"] = array();
$arResult["ERRORS"] = array();
$register_done = false;

// register user
if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_REQUEST["register_submit_button"]) && !$USER->IsAuthorized())
{	
	if(COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
	{
		//possible encrypted user password
		$sec = new CRsaSecurity();
		if(($arKeys = $sec->LoadKeys()))
		{
			$sec->SetKeys($arKeys);
			$errno = $sec->AcceptFromForm(array('REGISTER'));
			if($errno == CRsaSecurity::ERROR_SESS_CHECK)
				$arResult["ERRORS"][] = GetMessage("main_register_sess_expired");
			elseif($errno < 0)
				$arResult["ERRORS"][] = GetMessage("main_register_decode_err", array("#ERRCODE#"=>$errno));
		}
	}

	// check emptiness of required fields
	foreach ($arResult["SHOW_FIELDS"] as $key)
	{
		if ($key != "PERSONAL_PHOTO" && $key != "WORK_LOGO")
		{
			$arResult["VALUES"][$key] = $_REQUEST["REGISTER"][$key];
			if (in_array($key, $arResult["REQUIRED_FIELDS"]) && trim($arResult["VALUES"][$key]) == '')
				$arResult["ERRORS"][$key] = GetMessage("REGISTER_FIELD_REQUIRED");
		}
		else
		{
			$_FILES["REGISTER_FILES_".$key]["MODULE_ID"] = "main";
			$arResult["VALUES"][$key] = $_FILES["REGISTER_FILES_".$key];
			if (in_array($key, $arResult["REQUIRED_FIELDS"]) && !is_uploaded_file($_FILES["REGISTER_FILES_".$key]["tmp_name"]))
				$arResult["ERRORS"][$key] = GetMessage("REGISTER_FIELD_REQUIRED");
		}
	}

	if(isset($_REQUEST["REGISTER"]["TIME_ZONE"]))
		$arResult["VALUES"]["TIME_ZONE"] = $_REQUEST["REGISTER"]["TIME_ZONE"];

	$USER_FIELD_MANAGER->EditFormAddFields("USER", $arResult["VALUES"]);

	//this is a part of CheckFields() to show errors about user defined fields
	if (!$USER_FIELD_MANAGER->CheckFields("USER", 0, $arResult["VALUES"]))
	{
		$e = $APPLICATION->GetException();
		$arResult["ERRORS"][] = substr($e->GetString(), 0, -4); //cutting "<br>"
		$APPLICATION->ResetException();
	}

	// check captcha
	if ($arResult["USE_CAPTCHA"] == "Y")
	{
		if (!$APPLICATION->CaptchaCheckCode($_REQUEST["captcha_word"], $_REQUEST["captcha_sid"]))
			$arResult["ERRORS"][] = GetMessage("REGISTER_WRONG_CAPTCHA");
	}

	if(count($arResult["ERRORS"]) > 0)
	{
		if(COption::GetOptionString("main", "event_log_register_fail", "N") === "Y")
		{
			$arError = $arResult["ERRORS"];
			foreach($arError as $key => $error)
				if(intval($key) == 0 && $key !== 0) 
					$arError[$key] = str_replace("#FIELD_NAME#", '"'.$key.'"', $error);
			CEventLog::Log("SECURITY", "USER_REGISTER_FAIL", "main", false, implode("<br>", $arError));
		}
	}
	else // if there;s no any errors - create user
	{
		$bConfirmReq = (COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y" && $arResult["EMAIL_REQUIRED"]);

		$arResult['VALUES']["CHECKWORD"] = md5(CMain::GetServerUniqID().uniqid());
		$arResult['VALUES']["~CHECKWORD_TIME"] = $DB->CurrentTimeFunction();
		$arResult['VALUES']["ACTIVE"] = $bConfirmReq? "N": "Y";
		$arResult['VALUES']["CONFIRM_CODE"] = $bConfirmReq? randString(8): "";
		$arResult['VALUES']["LID"] = SITE_ID;
		$arResult['VALUES']["LANGUAGE_ID"] = LANGUAGE_ID;
		
		$arResult["VALUES"]['LOGIN'] = $arResult["VALUES"]["EMAIL"];

		$arResult['VALUES']["USER_IP"] = $_SERVER["REMOTE_ADDR"];
		$arResult['VALUES']["USER_HOST"] = @gethostbyaddr($_SERVER["REMOTE_ADDR"]);
		
		$arSaleProfile = array();
		
		foreach($arResult['VALUES'] as $key => $value){
			if($key == 'PROFILE_TYPE'){
				$arSaleProfile[$key] = $value;	
				unset($arResult['VALUES'][$key]);
			}
			else if(strpos($key , 'PROFILE_PROP_') !== false && $arSaleProfile['PROFILE_TYPE'] == 2){
				$arSaleProfile[$key] = $value;	
				unset($arResult['VALUES'][$key]);
			}
		}
		$arResult['VALUES']['UF_SALE_PROFILE'] = json_encode($arSaleProfile);
		
		if($arResult["VALUES"]["AUTO_TIME_ZONE"] <> "Y" && $arResult["VALUES"]["AUTO_TIME_ZONE"] <> "N")
			$arResult["VALUES"]["AUTO_TIME_ZONE"] = "";

		$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
		if($def_group != "")
			$arResult['VALUES']["GROUP_ID"] = explode(",", $def_group);

		$bOk = true;

		$events = GetModuleEvents("main", "OnBeforeUserRegister", true);
		foreach($events as $arEvent)
		{
			if(ExecuteModuleEventEx($arEvent, array(&$arResult['VALUES'])) === false)
			{
				if($err = $APPLICATION->GetException())
					$arResult['ERRORS'][] = $err->GetString();

				$bOk = false;
				break;
			}
		}

		$ID = 0;
		$user = new CUser();
		if ($bOk)
		{
			$ID = $user->Add($arResult["VALUES"]);
		}

		if (intval($ID) > 0)
		{
			
			if(CModule::IncludeModule("sale")){
				$profileID  = CSaleOrderUserProps::Add(array("NAME" => $arResult['VALUES']['NAME'], "USER_ID" => $ID, 'PERSON_TYPE_ID' => $arSaleProfile['PROFILE_TYPE']));
				
				if($profileID){
					
					$orderPropertiesList = CSaleOrderProps::GetList(
						array("SORT" => "ASC", "NAME" => "ASC"),
						array(
							"PERSON_TYPE_ID" => $arSaleProfile["PROFILE_TYPE"],
							"USER_PROPS" => "Y", "ACTIVE" => "Y", "UTIL" => "N"
						),
						false,
						false,
						array("ID", "NAME", "TYPE", "REQUIED", "MULTIPLE", "IS_LOCATION", "PROPS_GROUP_ID", "IS_EMAIL", "IS_PROFILE_NAME", "IS_PAYER", "IS_LOCATION4TAX", "CODE", "SORT")
					);

					
					while($orderProperty = $orderPropertiesList->GetNext()){
						$currentValue = "";

						if($arSaleProfile["PROFILE_PROP_" . $orderProperty["ID"]]){
							$currentValue = $arSaleProfile["PROFILE_PROP_" . $orderProperty["ID"]];
						}
						else{
							switch($orderProperty['CODE']){
								case "LOCATION":
									$currentValue = $form["location"];
									break;
								case "CITY":
									$currentValue = $form["town"];
									break;
								case "STREET":
									$currentValue = $form["street"];
									break;
								case "HOUSE":
									$currentValue = $form["num_home"];
									break;	
								case "APARTMENT":
									$currentValue = $form["flat"];
									break;
								case "PHONE":
									$currentValue = $form["phone"];
									break;
							}
						}
						
						
						if(checkProperty($orderProperty, $currentValue)){
							
							
							$fieldValues[$orderProperty["ID"]] = array(
								"USER_PROPS_ID" => $profileID,
								"ORDER_PROPS_ID" => $orderProperty["ID"],
								"NAME" => $orderProperty["NAME"],
								'MULTIPLE' => $orderProperty["MULTIPLE"]
							);

							if ($orderProperty['TYPE'] == "MULTISELECT")
							{
								$fieldValues[$orderProperty["ID"]]['VALUE'] = implode(',',$currentValue);
							}
							else
							{
								$fieldValues[$orderProperty["ID"]]['VALUE'] = $currentValue;
							}
							
						}

					}
					
					$updatedValues = array();
					$userPropertiesList = CSaleOrderUserPropsValue::GetList(
						array("SORT" => "ASC"),
						array("USER_PROPS_ID" => $profileID),
						false,
						false,
						array("ID", "ORDER_PROPS_ID", "VALUE", "SORT", "PROP_TYPE")
					);
					
	
					while ($propertyValues = $userPropertiesList->Fetch())
					{
						if (isset($fieldValues[$propertyValues["ORDER_PROPS_ID"]]['VALUE']))
						{
							CSaleOrderUserPropsValue::Update(
								$propertyValues["ID"],
								array("VALUE" => $fieldValues[$propertyValues["ORDER_PROPS_ID"]]['VALUE'])
							);
						}

						$updatedValues[$propertyValues["ORDER_PROPS_ID"]] = $fieldValues[$propertyValues["ORDER_PROPS_ID"]];
					}

					if ($newValues = array_diff_key($fieldValues, $updatedValues))
					{
						foreach ($newValues as $value)
						{
							unset($value['MULTIPLE']);
							CSaleOrderUserPropsValue::Add($value);
						}
					}

				}

			}
			
			
			
			

			$register_done = true;

			// authorize user
			if ($arParams["AUTH"] == "Y" && $arResult["VALUES"]["ACTIVE"] == "Y")
			{
				if (!$arAuthResult = $USER->Login($arResult["VALUES"]["LOGIN"], $arResult["VALUES"]["PASSWORD"]))
					$arResult["ERRORS"][] = $arAuthResult;
			}

			$arResult['VALUES']["USER_ID"] = $ID;

			$arEventFields = $arResult['VALUES'];
			unset($arEventFields["PASSWORD"]);
			unset($arEventFields["CONFIRM_PASSWORD"]);

			$event = new CEvent;
			$event->SendImmediate("NEW_USER", SITE_ID, $arEventFields);
			if($bConfirmReq)
				$event->SendImmediate("NEW_USER_CONFIRM", SITE_ID, $arEventFields);
		}
		else
		{
			$arResult["ERRORS"][] = $user->LAST_ERROR;
		}

		if(count($arResult["ERRORS"]) <= 0)
		{
			if(COption::GetOptionString("main", "event_log_register", "N") === "Y")
				CEventLog::Log("SECURITY", "USER_REGISTER", "main", $ID);
		}
		else
		{
			if(COption::GetOptionString("main", "event_log_register_fail", "N") === "Y")
				CEventLog::Log("SECURITY", "USER_REGISTER_FAIL", "main", $ID, implode("<br>", $arResult["ERRORS"]));
		}

		$events = GetModuleEvents("main", "OnAfterUserRegister", true);
		foreach ($events as $arEvent)
			ExecuteModuleEventEx($arEvent, array(&$arResult['VALUES']));
	}
}

// if user is registered - redirect him to backurl or to success_page; currently added users too
if($register_done)
{
	if($arParams["USE_BACKURL"] == "Y" && $_REQUEST["backurl"] <> '')
		LocalRedirect($_REQUEST["backurl"]);
	elseif($arParams["SUCCESS_PAGE"] <> '')
		LocalRedirect($arParams["SUCCESS_PAGE"]);
}

$arResult["VALUES"] = htmlspecialcharsEx($arResult["VALUES"]);

// redefine required list - for better use in template
$arResult["REQUIRED_FIELDS_FLAGS"] = array();
foreach ($arResult["REQUIRED_FIELDS"] as $field)
	$arResult["REQUIRED_FIELDS_FLAGS"][$field] = "Y";

// check backurl existance
$arResult["BACKURL"] = htmlspecialcharsbx($_REQUEST["backurl"]);

// get countries list
if (in_array("PERSONAL_COUNTRY", $arResult["SHOW_FIELDS"]) || in_array("WORK_COUNTRY", $arResult["SHOW_FIELDS"])) 
	$arResult["COUNTRIES"] = GetCountryArray();

// get date format
if (in_array("PERSONAL_BIRTHDAY", $arResult["SHOW_FIELDS"])) 
	$arResult["DATE_FORMAT"] = CLang::GetDateFormat("SHORT");

// ********************* User properties ***************************************************
$arResult["USER_PROPERTIES"] = array("SHOW" => "N");
$arUserFields = $USER_FIELD_MANAGER->GetUserFields("USER", 0, LANGUAGE_ID);
if (is_array($arUserFields) && count($arUserFields) > 0)
{
	if (!is_array($arParams["USER_PROPERTY"]))
		$arParams["USER_PROPERTY"] = array($arParams["USER_PROPERTY"]);

	foreach ($arUserFields as $FIELD_NAME => $arUserField)
	{
		if (!in_array($FIELD_NAME, $arParams["USER_PROPERTY"]) && $arUserField["MANDATORY"] != "Y")
			continue;

		$arUserField["EDIT_FORM_LABEL"] = strLen($arUserField["EDIT_FORM_LABEL"]) > 0 ? $arUserField["EDIT_FORM_LABEL"] : $arUserField["FIELD_NAME"];
		$arUserField["EDIT_FORM_LABEL"] = htmlspecialcharsEx($arUserField["EDIT_FORM_LABEL"]);
		$arUserField["~EDIT_FORM_LABEL"] = $arUserField["EDIT_FORM_LABEL"];
		$arResult["USER_PROPERTIES"]["DATA"][$FIELD_NAME] = $arUserField;
	}
}
if (!empty($arResult["USER_PROPERTIES"]["DATA"]))
{
	$arResult["USER_PROPERTIES"]["SHOW"] = "Y";
	$arResult["bVarsFromForm"] = (count($arResult['ERRORS']) <= 0) ? false : true;
}
// ******************** /User properties ***************************************************


$arResult["AUTH_SERVICES"] = false;
$arResult["CURRENT_SERVICE"] = false;
if(!$USER->IsAuthorized() && CModule::IncludeModule("socialservices"))
{
	$oAuthManager = new CSocServAuthManager();
	$arServices = $oAuthManager->GetActiveAuthServices($arResult);

	if(!empty($arServices))
	{
		$arResult["AUTH_SERVICES"] = $arServices;
		if(isset($_REQUEST["auth_service_id"]) && $_REQUEST["auth_service_id"] <> '' && isset($arResult["AUTH_SERVICES"][$_REQUEST["auth_service_id"]]))
		{
			$arResult["CURRENT_SERVICE"] = $_REQUEST["auth_service_id"];
			if(isset($_REQUEST["auth_service_error"]) && $_REQUEST["auth_service_error"] <> '')
			{
				$arResult['ERROR_MESSAGE'] = $oAuthManager->GetError($arResult["CURRENT_SERVICE"], $_REQUEST["auth_service_error"]);
			}
			elseif(!$oAuthManager->Authorize($_REQUEST["auth_service_id"]))
			{
				$ex = $APPLICATION->GetException();
				if ($ex)
					$arResult['ERROR_MESSAGE'] = $ex->GetString();
			}
		}
	}
}

// initialize captcha
if ($arResult["USE_CAPTCHA"] == "Y")
	$arResult["CAPTCHA_CODE"] = htmlspecialcharsbx($APPLICATION->CaptchaGetCode());

// set title
if ($arParams["SET_TITLE"] == "Y") 
	$APPLICATION->SetTitle(GetMessage("REGISTER_DEFAULT_TITLE"));

//time zones
$arResult["TIME_ZONE_ENABLED"] = CTimeZone::Enabled();
if($arResult["TIME_ZONE_ENABLED"])
	$arResult["TIME_ZONE_LIST"] = CTimeZone::GetZones();

$arResult["SECURE_AUTH"] = false;
if(!CMain::IsHTTPS() && COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
{
	$sec = new CRsaSecurity();
	if(($arKeys = $sec->LoadKeys()))
	{
		$sec->SetKeys($arKeys);
		$sec->AddToForm('regform', array('REGISTER[PASSWORD]', 'REGISTER[CONFIRM_PASSWORD]'));
		$arResult["SECURE_AUTH"] = true;
	}
}

// all done
$this->IncludeComponentTemplate();


function checkProperty($property, $currentValue)
{
		if ($property["TYPE"] == "LOCATION" && $property["IS_LOCATION"] == "Y")
		{
			if ((int)($currentValue) <= 0)
				return false;
		}
		elseif ($property["IS_PROFILE_NAME"] == "Y")
		{
			if (strlen(trim($currentValue)) <= 0)
				return false;
		}
		elseif ($property["IS_PAYER"] == "Y")
		{
			if (strlen(trim($currentValue)) <= 0)
				return false;
		}
		elseif ($property["IS_EMAIL"] == "Y")
		{
			if (strlen(trim($currentValue)) <= 0 || !check_email(trim($currentValue)))
				return false;
		}
		elseif ($property["REQUIED"] == "Y")
		{
			if ($property["TYPE"] == "LOCATION")
			{
				if ((int)($currentValue) <= 0)
					return false;
			}
			elseif ($property["TYPE"] == "MULTISELECT")
			{
				if (!is_array($currentValue) || count($currentValue) <= 0)
					return false;
			}
			else
			{
				if (strlen($currentValue) <= 0)
					return false;
			}
		}

		return true;
}

<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arCurrentValues */

$arrFields = array(
	'DSCB_NAME' => GetMessage("DEVSTRONG_FIELD_DSCB_NAME"),
	'DSCB_PHONE' => GetMessage("DEVSTRONG_FIELD_DSCB_PHONE"),
	'DSCB_EMAIL' => GetMessage("DEVSTRONG_FIELD_DSCB_EMAIL"),
	'DSCB_COMMENT' => GetMessage("DEVSTRONG_FIELD_DSCB_COMMENT"),
	'DSCB_SPECIALIST' => GetMessage("DEVSTRONG_FIELD_DSCB_SPECIALIST"),
);

$arRequired = array();

foreach($arCurrentValues['REQUIRED_FIELDS'] as $k => $field) {
	
	if(!in_array($field, $arCurrentValues['FIELDS'])) {
		
		unset($arCurrentValues['REQUIRED_FIELDS'][$k]);
	}
}

if($arCurrentValues['FIELDS']) {
	
	foreach($arCurrentValues['FIELDS'] as $field) {
		
		$arRequired[$field] = GetMessage("DEVSTRONG_FIELD_".$field);
	}
	
} else {
	
	$field = 'DSCB_PHONE';
	$arrFields[$field] .= ' - ' . GetMessage("DEVSTRONG_SHOW");
	$arCurrentValues['FIELDS'] = array($field);
}

if(empty($arCurrentValues['REQUIRED_FIELDS'])) {
	
	$field = $arCurrentValues['FIELDS'][0];
	$arRequired[$field] = GetMessage("DEVSTRONG_FIELD_".$field) . ' - ' . GetMessage("DEVSTRONG_REQUIRED");
}

$arComponentParameters = array( 

	"GROUPS" => array(),

	"PARAMETERS" => array(
	
		"FIELDS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_SHOW_FIELDS"),
			"TYPE" => "LIST",
			"VALUES" => $arrFields,
			"DEFAULT" => "",
			"MULTIPLE" => "Y",
			"REFRESH" => "Y",
		),
		
		"REQUIRED_FIELDS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_REQUIRED_FIELDS"),
			"TYPE" => "LIST",
			"VALUES" => $arRequired,
			"DEFAULT" => "",
			"MULTIPLE" => "Y",
			"REFRESH" => "Y",
		),
		
		"SUCCESS_MESS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_SUCCESS_MESS_TITLE"),
			"TYPE" => "STRING",
			"DEFAULT" => GetMessage("DEVSTRONG_SUCCESS_MESS"),
		),
		
		"INCLUDE_MAGNIFIC" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_INCLUDE_MAGNIFIC"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		
		"INCLUDE_INPUTMASK" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_INCLUDE_INPUTMASK"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		
		"COMPONENT_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_COMPONENT_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => 'ds-' . substr(md5(time()), 0, 7),
		),
	)
);
?>

$(document).on('ready', function() {
	
	$('.ds-callback-btn').magnificPopup({
		type: 'inline',
		mainClass: 'mfp-fade',
		removalDelay: 300,
		items: {
			src: '#ds-callback'
		},
		
		callbacks: {

		}
	});
	
	$('input[data-type="ds-phone"]').inputmask("+7(999)9999999");
	
	$('body').on('click', '[data-type="ds-cb-send"]', function() {
		
		var thisBtn = $(this);
		var thisFormID = $(this).closest('.ds-form').attr('id');
		var thisUrl = window.location.href;
		var postData = new Object()
		
		if(thisBtn.hasClass('load')) {
			return false;
		}
		
		thisBtn.addClass('load');
		
		$('#'+thisFormID+' input, #'+thisFormID+' textarea').each(function(i, elem) {
			
			var fieldName = $(elem).attr('name');
			
			if(!!fieldName) {
				postData[fieldName] = $(elem).val();
			}
		});
		
		postData['ds-cb-ajax'] = 'Y';
		postData['ncc'] = 'Y';
		
		$.ajax({
			type: 'post',
			url: thisUrl,
			data: postData,
			dataType: 'json',
			
			success: function(result) {
			
				if(result == null) {
					alert('Error');
				}
				
				else if(result.errors) {
					$.each(result.errors, function(key, val){
						$('#'+thisFormID+' [name="'+key+'"]').addClass('inpErr');
					});
				}
				
				else if(result.success) {

					$('#'+thisFormID).html('<div class="dscb-result">'+DSCB.SUCCESS_MESS+'</div>');
				
					thisBtn.removeClass('load');
					return true;

				}
				
				thisBtn.removeClass('load');
			},
			
			error: function() {
				thisBtn.removeClass('load');
				alert('Error');
			}
		});
	});
	
	/*$('body').on('click', '#ds-cb-send', function() {
		
		var thisBtn = $(this);
		var thisUrl = window.location.href;
		var postData = new Object()
		
		if(thisBtn.hasClass('load')) {
			return false;
		}
		
		thisBtn.addClass('load');
		
		$('#ds-callback input, #ds-callback textarea').each(function(i, elem) {
			
			var fieldName = $(elem).attr('name');
			
			if(!!fieldName) {
				postData[fieldName] = $(elem).val();
			}
		});
		
		postData['ds-cb-ajax'] = 'Y';
		postData['ncc'] = 'Y';
		
		$.ajax({
			type: 'post',
			url: thisUrl,
			data: postData,
			dataType: 'json',
			
			success: function(result) {
			
				if(result == null) {
					alert('Error');
				}
				
				else if(result.errors) {
					$.each(result.errors, function(key, val){
						$('#ds-callback [name="'+key+'"]').addClass('inpErr');
					});
				}
				
				else if(result.success) {

					$('#ds-callback .ds-form').html('<div class="dscb-result">'+DSCB.SUCCESS_MESS+'</div>');
				
					thisBtn.removeClass('load');
					return true;

				}
				
				thisBtn.removeClass('load');
			},
			
			error: function() {
				thisBtn.removeClass('load');
				alert('Error');
			}
		});
	});*/
	
	$('body').on('focus', '.inpErr', function() {
		$(this).removeClass('inpErr');
	});
	
});
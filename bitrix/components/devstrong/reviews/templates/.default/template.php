<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
if($arParams['INCLUDE_MAGNIFIC'] == 'Y') {
	$APPLICATION->AddHeadScript($component->getPath().'/libs/magnific-popup/jquery.magnific-popup.js');
	$APPLICATION->SetAdditionalCSS($component->getPath().'/libs/magnific-popup/magnific-popup.css');
}
if($arParams['INCLUDE_INPUTMASK'] == 'Y') {
	$APPLICATION->AddHeadScript($component->getPath().'/libs/inputmask/jquery.inputmask.bundle.min.js');
}
?>

<div id="ds-callback">
	<div class="ds-title"><?=GetMessage('DS_CALLBACK');?></div>
	<div class="ds-form" id="<?=$arParams['COMPONENT_ID'];?>">
		<?=bitrix_sessid_post();?>
		<input type="hidden" name="DSCB_COMPONENT_ID" value="<?=$arParams['COMPONENT_ID'];?>"/>
		<? if(in_array('DSCB_NAME', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-name"><?=GetMessage('DS_CALLBACK_NAME');?><? if(in_array('DSCB_NAME', $arParams['REQUIRED_FIELDS'])) { ?> *<? } ?></label>
			<input type="text" id="<?=$arParams['COMPONENT_ID'];?>-name" name="DSCB_NAME"/>
		</div>
		<? } ?>
		<? if(in_array('DSCB_PHONE', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-phone"><?=GetMessage('DS_CALLBACK_PHONE');?><? if(in_array('DSCB_PHONE', $arParams['REQUIRED_FIELDS'])) { ?> *<? } ?></label>
			<input data-type="ds-phone" type="text" id="<?=$arParams['COMPONENT_ID'];?>-phone" name="DSCB_PHONE"/>
		</div>
		<? } ?>
		<? if(in_array('DSCB_EMAIL', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-email"><?=GetMessage('DS_CALLBACK_EMAIL');?><? if(in_array('DSCB_EMAIL', $arParams['REQUIRED_FIELDS'])) { ?> *<? } ?></label>
			<input type="text" id="<?=$arParams['COMPONENT_ID'];?>-email" name="DSCB_EMAIL"/>
		</div>
		<? } ?>
		<? if(in_array('DSCB_COMMENT', $arParams['FIELDS'])) { ?>
		<div class="ds-str">
			<label for="<?=$arParams['COMPONENT_ID'];?>-comment"><?=GetMessage('DS_CALLBACK_COMMENT');?><? if(in_array('DSCB_COMMENT', $arParams['REQUIRED_FIELDS'])) { ?> *<? } ?></label>
			<textarea name="DSCB_COMMENT" id="<?=$arParams['COMPONENT_ID'];?>-comment"></textarea>
		</div>
		<? } ?>
		<div class="ds-str ds-btn">
			<div class="ds-cb-send" data-type="ds-cb-send"><?=GetMessage('DS_CALLBACK_SEND');?></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var DSCB = <?=CUtil::PhpToJSObject(array('SUCCESS_MESS' => $arParams['SUCCESS_MESS']));?>
</script>
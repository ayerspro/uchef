<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arAllFields = array(
	'DSCB_TYPE',
	'DSCB_NAME',
	'DSCB_PHONE',
	'DSCB_EMAIL',
	'DSCB_COMMENT',
	'DSCB_RELATED',
);

if(!isset($arParams['FIELDS']) || empty($arParams['FIELDS'])) {
	
	$arParams['FIELDS'] = array('DSCB_PHONE');
}

if(!isset($arParams['REQUIRED_FIELDS']) || empty($arParams['REQUIRED_FIELDS'])) {
	
	$arParams['REQUIRED_FIELDS'] = array($arParams['FIELDS'][0]);
}

if(isset($_REQUEST['ds-cb-ajax']) && $_REQUEST['ds-cb-ajax'] == 'Y' && $_REQUEST['DSCB_COMPONENT_ID'] == $arParams['COMPONENT_ID']) {
	
	global $APPLICATION;
	
	if(SITE_CHARSET == 'windows-1251') {
		
		foreach($arParams['FIELDS'] as $field) {
			
			$_REQUEST[$field] = iconv('UTF-8', 'windows-1251', $_REQUEST[$field]);
		}
	}
	
	$ajaxResult = array();
	$ajaxResult['errors'] = array();
	
	foreach($arParams['REQUIRED_FIELDS'] as $field) {
		
		if(!isset($_REQUEST[$field]) || empty($_REQUEST[$field])) {
			
			$ajaxResult['errors'][$field] = true;
			
		} elseif($field == 'DSCB_EMAIL' && !preg_match("#^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$#i", $_REQUEST[$field])) {
			
			$ajaxResult['errors'][$field] = true;
		}
	}
	
	if($arParams['USE_RECAPTCHA'] == 'Y' && empty($ajaxResult['errors'])) {
		
		/*$paramsArray = array(
		
			'secret' => $arParams["RECAPTCHA_SECRET"], 
			'response' => $_POST['g-recaptcha-response'],
			'remoteip' => $_SERVER['HTTP_X_REAL_IP'],
		); 

		$vars = http_build_query($paramsArray);

		$options = array(
		
			'http' => array(  
				'method'  => 'POST',
				'content' => $vars,
			)  
		);  
		
		$context  = stream_context_create($options);
		
		$result = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
		*/
		
		if($curl = curl_init() ) {
			curl_setopt($curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, 'secret='.$arParams["RECAPTCHA_SECRET"].'&response='. $_POST['g-recaptcha-response'].'&remoteip='.$_SERVER['HTTP_X_REAL_IP']);
			$result = curl_exec($curl);
			curl_close($curl);
		}
		
		$result = json_decode($result, true);
		
		if ($result['success'] !== true) {
			
			$ajaxResult['errors']['RECAPTCHA'] = true;
		}
		
	}
	
	if(empty($ajaxResult['errors'])) {
		
		unset($ajaxResult['errors']);
		
		$eventName = 'DS_REVIEWS_EVENT';
		
		$arFields = array(
		
			'DATE_TIME' => date('d.m.Y H:i:s'),
			'URL' => $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
			'IP' => $_SERVER['REMOTE_ADDR'],
		);
		
		$PROP = array();
		
		foreach($arAllFields as $field) {
			
			$arFields[$field] = isset($_REQUEST[$field]) ? $_REQUEST[$field] : '';
			$PROP[$field] = $arFields[$field];
		}
		
		/* IBLOCK SAVE */
		
		CModule::IncludeModule('iblock');
		
		$iblockID = $arParams['IBLOCK_ID'];
		$arFields['IBLOCK_ID'] = $arParams['IBLOCK_ID'];
		
		$el = new CIBlockElement;

		$arLoadProductArray = Array(
			"IBLOCK_ID"      => $iblockID,
			"NAME"           => $arFields['DSCB_NAME'],
			"ACTIVE"         => 'N',
			"ACTIVE_FROM"    => $arFields['DATE_TIME'],
			"DETAIL_TEXT"   => $PROP['DSCB_COMMENT'],
			"PROPERTY_VALUES"=> $PROP,
		);

		$elID = $el->Add($arLoadProductArray);
		
		if($elID) {
			
			$arFields['ELID'] = $elID;
		} 
		
		/* IBLOCK SAVE END */

		CEvent::Send($eventName, SITE_ID, $arFields);
		CEvent::CheckEvents();
		
		$ajaxResult['success'] = true;
	}
	
	$APPLICATION->RestartBuffer();
	echo json_encode($ajaxResult);
	die();
}

$this->IncludeComponentTemplate();

?>

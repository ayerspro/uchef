<?

$MESS['DEVSTRONG_FIELD_DSCB_NAME'] = "Имя";
$MESS['DEVSTRONG_FIELD_DSCB_PHONE'] = "Номер телефона";
$MESS['DEVSTRONG_FIELD_DSCB_EMAIL'] = "E-mail";
$MESS['DEVSTRONG_FIELD_DSCB_COMMENT'] = "Комментарий";

$MESS['DEVSTRONG_SHOW'] = "это поле будет выведено";
$MESS['DEVSTRONG_REQUIRED'] = "это поле будет обязательным";
$MESS['DEVSTRONG_SHOW_ALWAYS'] = "выводится всегда";
$MESS['DEVSTRONG_REQUIRED_ALWAYS'] = "обязательно всегда";

$MESS['DEVSTRONG_SHOW_FIELDS'] = "Поля, выводимые в форме заказа звонка";
$MESS['DEVSTRONG_REQUIRED_FIELDS'] = "Обязательные поля";

$MESS['DEVSTRONG_INCLUDE_MAGNIFIC'] = "Подключить библиотеку Magnific Popup (снимите галочку, если библиотека уже подключена)";
$MESS['DEVSTRONG_INCLUDE_INPUTMASK'] = "Подключить библиотеку Inputmask (снимите галочку, если библиотека уже подключена)";

$MESS['DEVSTRONG_SUCCESS_MESS_TITLE'] = "Сообщение об успешной отправке";
$MESS['DEVSTRONG_SUCCESS_MESS'] = "Ваша заявка принята! В ближайшее время мы свяжемся с вами.";

$MESS['DEVSTRONG_COMPONENT_ID'] = "Идентификатор (Если на странице сайта установлено несколько компонентов - у каждого должен быть уникальный идентификатор)";

$MESS['DEVSTRONG_FORM_TITLE'] = 'Заголовок формы';
$MESS['DEVSTRONG_REQUIRED_TITLE'] = 'Подпись обязательного поля';
$MESS['DEVSTRONG_REQUIRED_FIELDS_ERR'] = 'Текст ошибки при незаполнении обязательного поля';
$MESS['DEVSTRONG_NAME_TITLE'] = 'Подпись поля Имя';
$MESS['DEVSTRONG_NAME_PH'] = 'Плейсхолдер поля Имя';
$MESS['DEVSTRONG_EMAIL_TITLE'] = 'Подпись поля E-mail';
$MESS['DEVSTRONG_EMAIL_PH'] = 'Плейсхолдер поля E-mail';
$MESS['DEVSTRONG_PHONE_TITLE'] = 'Подпись поля Телефон';
$MESS['DEVSTRONG_PHONE_PH'] = 'Плейсхолдер поля Телефон';
$MESS['DEVSTRONG_COMMENT_PH'] = 'Плейсхолдер поля Комментарий';
$MESS['DEVSTRONG_SEND_BTN_TITLE'] = 'Подпись кнопки';
$MESS['DEVSTRONG_SUCCESS_MESS'] = 'Сообщение об успешной отправке формы';
$MESS['DEVSTRONG_SUCCESS_FORM_TITLE'] = 'Заголовок после успешной отправки';
$MESS['DEVSTRONG_RECAPTCHA_ERROR'] = 'Текст ошибки Recaptcha';

?>

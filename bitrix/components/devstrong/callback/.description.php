<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("DEVSTRONG_CALLBACK_NAME"),
	"DESCRIPTION" => GetMessage("DEVSTRONG_CALLBACK_DESCRIPTION"),
	"ICON" => "/images/subscr_form.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "devstrong_components",
		"NAME" => GetMessage("DEVSTRONG_COMPONENTS_PATH_NAME"),
		/*"CHILD" => array(
			"ID" => "devstrong_subscribe",
			"NAME" => GetMessage("DEVSTRONG_CALLBACK_PATH_NAME")
		)*/
	),
);

?>
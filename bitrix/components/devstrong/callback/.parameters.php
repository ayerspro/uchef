<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arCurrentValues */

$arrFields = array(
	'DSCB_NAME' => GetMessage("DEVSTRONG_FIELD_DSCB_NAME"),
	'DSCB_PHONE' => GetMessage("DEVSTRONG_FIELD_DSCB_PHONE"),
	'DSCB_EMAIL' => GetMessage("DEVSTRONG_FIELD_DSCB_EMAIL"),
	'DSCB_COMMENT' => GetMessage("DEVSTRONG_FIELD_DSCB_COMMENT"),
);

$arRequired = array();

foreach($arCurrentValues['REQUIRED_FIELDS'] as $k => $field) {
	
	if(!in_array($field, $arCurrentValues['FIELDS'])) {
		
		unset($arCurrentValues['REQUIRED_FIELDS'][$k]);
	}
}

if($arCurrentValues['FIELDS']) {
	
	foreach($arCurrentValues['FIELDS'] as $field) {
		
		$arRequired[$field] = GetMessage("DEVSTRONG_FIELD_".$field);
	}
	
} else {
	
	$field = 'DSCB_PHONE';
	$arrFields[$field] .= ' - ' . GetMessage("DEVSTRONG_SHOW");
	$arCurrentValues['FIELDS'] = array($field);
}

if(empty($arCurrentValues['REQUIRED_FIELDS'])) {
	
	$field = $arCurrentValues['FIELDS'][0];
	$arRequired[$field] = GetMessage("DEVSTRONG_FIELD_".$field) . ' - ' . GetMessage("DEVSTRONG_REQUIRED");
}

$arComponentParameters = array( 

	"GROUPS" => array(),

	"PARAMETERS" => array(
	
		"FIELDS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_SHOW_FIELDS"),
			"TYPE" => "LIST",
			"VALUES" => $arrFields,
			"DEFAULT" => "",
			"MULTIPLE" => "Y",
			"REFRESH" => "Y",
		),
		
		"REQUIRED_FIELDS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_REQUIRED_FIELDS"),
			"TYPE" => "LIST",
			"VALUES" => $arRequired,
			"DEFAULT" => "",
			"MULTIPLE" => "Y",
			"REFRESH" => "Y",
		),
		
		"SUCCESS_MESS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_SUCCESS_MESS_TITLE"),
			"TYPE" => "STRING",
			"DEFAULT" => GetMessage("DEVSTRONG_SUCCESS_MESS"),
		),
		
		"INCLUDE_MAGNIFIC" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_INCLUDE_MAGNIFIC"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		
		"INCLUDE_INPUTMASK" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_INCLUDE_INPUTMASK"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		
		"COMPONENT_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_COMPONENT_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => 'ds-' . substr(md5(time()), 0, 7),
		),
		
		"FORM_TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_FORM_TITLE"),
			"TYPE" => "STRING",
		),
		
		"REQUIRED_TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_REQUIRED_TITLE"),
			"TYPE" => "STRING",
		),
		
		"REQUIRED_FIELDS_ERR" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_REQUIRED_FIELDS_ERR"),
			"TYPE" => "STRING",
		),
		
		"NAME_TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_NAME_TITLE"),
			"TYPE" => "STRING",
		),
		
		"NAME_PH" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_NAME_PH"),
			"TYPE" => "STRING",
		),
		
		"EMAIL_TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_EMAIL_TITLE"),
			"TYPE" => "STRING",
		),
		
		"EMAIL_PH" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_EMAIL_PH"),
			"TYPE" => "STRING",
		),
		
		"PHONE_TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_PHONE_TITLE"),
			"TYPE" => "STRING",
		),
		
		"PHONE_PH" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_PHONE_PH"),
			"TYPE" => "STRING",
		),
		
		"COMMENT_PH" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_COMMENT_PH"),
			"TYPE" => "STRING",
		),
		
		"SEND_BTN_TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_SEND_BTN_TITLE"),
			"TYPE" => "STRING",
		),
		
		"SUCCESS_MESS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_SUCCESS_MESS"),
			"TYPE" => "STRING",
		),
		
		"RECAPTCHA_ERROR" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_RECAPTCHA_ERROR"),
			"TYPE" => "STRING",
		),
		
		"SUCCESS_FORM_TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DEVSTRONG_SUCCESS_FORM_TITLE"),
			"TYPE" => "STRING",
		),
	)
);
?>

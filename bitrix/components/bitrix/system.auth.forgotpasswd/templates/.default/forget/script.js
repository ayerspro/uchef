$(document).ready(function(){
	$('#forget_form').submit(function(){
		var th = $(this);
		var data = th.serialize();
		$.ajax({
			url: '/ajax/autoriz.php',
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(resp) {
				if(resp.hasOwnProperty('ERROR')) {
					th.find('.error-bl').empty();
					$.each(resp.ERROR, function(k, v){
						th.find('.error-bl').append('<p><font class="errortext">'+v+'</font></p>');
					});
				} else {
					th.empty().append('<p class="success">На ваш email было отправлено сообщение с инструкцией о смене пароля</p>');
				}
			}
		});
		return false;
	});
});
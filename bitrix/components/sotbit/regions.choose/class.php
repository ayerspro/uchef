<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Service\GeoIp;
use Sotbit\Regions\Config\Option;
use Sotbit\Regions\Internals\RegionsTable;
use Sotbit\Regions\Location;


if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * Class SotbitRegionsChooseComponent
 * @author Sergey Danilkin <s.danilkin@sotbit.ru>
 */
class SotbitRegionsChooseComponent extends \CBitrixComponent
{
	/**
	 * @return mixed|void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function executeComponent()
	{
		if(Loader::includeModule('sotbit.regions') && !\SotbitRegions::isDemoEnd())
		{
			$domain = new Location\Domain();

			$this->arResult['REGION_LIST'] = $this->getRegionsList($domain);

			$context = Application::getInstance()->getContext();
			$server = $context->getServer();
			$serverDomain = $server->getServerName();
			$this->arResult['ROOT_DOMAIN'] = $serverDomain;

			if(Option::get('SINGLE_DOMAIN',SITE_ID) != 'Y')
			{
				$arDomain = explode('.',$this->arResult['ROOT_DOMAIN']);
				$count = count($arDomain);
				$rDomain = $arDomain[$count-2].'.'.$arDomain[$count-1];
				$rDomain = str_replace(array('https://','http://'),'',$rDomain);
				$this->arResult['ROOT_DOMAIN'] = $rDomain;
			}

			if(Option::get('SINGLE_DOMAIN',SITE_ID) != 'Y')
			{
				foreach($this->arResult['REGION_LIST'] as $region)
				{
					if($region['CURRENT'])
					{
						$this->arResult['USER_REGION_ID'] = $region['ID'];
						$this->arResult['USER_REGION_NAME'] = $region['NAME'];
						break;
					}
				}
			}
			else
			{
				if($_COOKIE['sotbit_regions_id'] > 0)
				{
					$this->arResult['USER_REGION_ID'] = $_COOKIE['sotbit_regions_id'];
					foreach($this->arResult['REGION_LIST'] as $region)
					{
						if($region['ID'] == $this->arResult['USER_REGION_ID'])
						{
							$this->arResult['USER_REGION_NAME'] = $region['NAME'];
						}
					}
				}
				else
				{
					$this->arResult['USER_REGION_ID'] = $this->arResult['REGION_LIST'][0]['ID'];
					$this->arResult['USER_REGION_NAME'] = $this->arResult['REGION_LIST'][0]['NAME'];
				}
			}

			if($this->needChooseLocation())
			{
				$this->arResult['SHOW_POPUP'] = 'Y';
				$userCity=$this->findUserLocation();
				$this->arResult['USER_REGION_NAME_LOCATION'] = $userCity['NAME'];
				 $this->arResult['USER_REGION_ID'] = $userCity['ID'];
			}
			else
			{
				$this->arResult['SHOW_POPUP'] = 'N';

			}
			$this->includeComponentTemplate();
		}
	}

	/**
	 * @return string
	 */
	private function findUserLocation()
	{
		$return = '';

		$userLocation = new Location\User();
		$userCity = $userLocation->getUserCity();
		foreach ($this->arResult['REGION_LIST'] as $region)
		{
			if($region['NAME'] == $userCity)
			{
				$return = $region;
			}
		}
		if(!$return)
		{
			foreach($this->arResult['REGION_LIST'] as $region)
			{
				if($region['CURRENT'] == 'Y')
				{
					$return = $region;
				}
			}
		}

		if(!$return)
		{
			$return = $this->arResult['REGION_LIST'][0];
		}

		return $return;
	}

	/**
	 * @return bool
	 * @throws \Bitrix\Main\SystemException
	 */
	private function needChooseLocation()
	{
		$return = true;
		if($_COOKIE['sotbit_regions_city_choosed'] == 'Y')
		{
			$return = false;
		}

		return $return;
	}

	/**
	 * @param Location\Domain $domain
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\SystemException
	 */
	private function getRegionsList(\Sotbit\Regions\Location\Domain $domain)
	{
		$return = array();
		$context = Application::getInstance()->getContext();
		$server = $context->getServer();
		$requestUri = $server->getRequestUri();
		$i = 0;

		$rs = RegionsTable::getList(array(
			'order' => array(
				'SORT' => 'asc'
			),
			'filter' => ['%SITE_ID' => SITE_ID],
			'cache' => array(
				'ttl' => 36000000,
			)
		));
		while($region = $rs->fetch())
		{
			$return[$i]['ID'] = $region['ID'];
			$return[$i]['NAME'] = $region['NAME'];
			$return[$i]['URL'] = $region['CODE'] . $requestUri;
			$return[$i]['CODE'] = $region['CODE'];
			if($region['CODE'] == $domain->getProp('CODE'))
			{
				$return[$i]['CURRENT'] = 'Y';
			}
			++$i;
		}

		return $return;
	}
}
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "Корзина");
$APPLICATION->SetPageProperty("keywords_inner", "Корзина");
$APPLICATION->SetPageProperty("title", "Корзина");
$APPLICATION->SetPageProperty("keywords", "Корзина");
$APPLICATION->SetPageProperty("description", "Корзина");
$APPLICATION->SetTitle("Корзина");

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->set(['pageType' => 'cart']);

?>

<h1 class="text-center catalog-title">Корзина покупок</h1>

<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket", 
	"chef", 
	array(
		"ACTION_VARIABLE" => "action",
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "DELETE",
			2 => "DELAY",
			3 => "TYPE",
			4 => "PRICE",
			5 => "QUANTITY",
			6 => "SUM",
		),
		"COMPONENT_TEMPLATE" => "chef",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"HIDE_COUPON" => "N",
		"OFFERS_PROPS" => array(
			0 => "ARTNUMBER",
		),
		"PATH_TO_ORDER" => "/cart/order/",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"USE_PREPAYMENT" => "N",
		"CORRECT_RATIO" => "N",
		"AUTO_CALCULATION" => "Y",
		"USE_GIFTS" => "Y",
		"GIFTS_PLACE" => "BOTTOM",
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
		"GIFTS_SHOW_OLD_PRICE" => "N",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_CONVERT_CURRENCY" => "N",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "Оформление заказа");
$APPLICATION->SetPageProperty("keywords_inner", "Оформление заказа");
$APPLICATION->SetPageProperty("title", "Оформление заказа");
$APPLICATION->SetPageProperty("keywords", "Оформление заказа");
$APPLICATION->SetPageProperty("description", "Оформление заказа");
$APPLICATION->SetTitle("Оформление заказа");

?>
<h1 class="text-center catalog-title">Оформление заказа</h1>
<?

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->set(['pageType' => 'purchase']);

global $USER; 

if(isset($_GET["ORDER_ID"])){ 
	LocalRedirect("/cart/order/thanks/?ORDER_ID=" . $_GET["ORDER_ID"]);
}
else if(!$USER->IsAuthorized() && !$APPLICATION->get_cookie("CART_ORDER_NON_REGISTER")){
	LocalRedirect("/cart/auth/");
}
else{
	$nonRegister = "N";
	
	if($APPLICATION->get_cookie("CART_ORDER_NON_REGISTER")){
		$nonRegister = strval($APPLICATION->get_cookie("CART_ORDER_NON_REGISTER"));
	}
	
	if(!$USER->IsAuthorized() && $nonRegister == "Y"){
		//$USER->Authorize(77);
	}
	else if($USER->getId() != 77){
		$nonRegister = "N";
	}
	
	
	$APPLICATION->IncludeComponent(
		"braino:sale.order.ajax", 
		"chef", 
		array(
			"ALLOW_AUTO_REGISTER" => "Y",
			"ALLOW_NEW_PROFILE" => "N",
			"COMPONENT_TEMPLATE" => ".default",
			"COUNT_DELIVERY_TAX" => "N",
			"DELIVERY_NO_AJAX" => "Y",
			"DELIVERY_NO_SESSION" => "N",
			"DELIVERY_TO_PAYSYSTEM" => "d2p",
			"DISABLE_BASKET_REDIRECT" => "Y",
			"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
			"PATH_TO_AUTH" => "/auth/",
			"PATH_TO_BASKET" => "/cart/",
			"PATH_TO_PAYMENT" => "/cart/order/payment/",
			"PATH_TO_PERSONAL" => "/personal/",
			"PAY_FROM_ACCOUNT" => "Y",
			"PRODUCT_COLUMNS" => "",
			"SEND_NEW_USER_NOTIFY" => "Y",
			"SET_TITLE" => "Y",
			"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
			"SHOW_STORES_IMAGES" => "N",
			"TEMPLATE_LOCATION" => "popup",
			"USE_PREPAYMENT" => "N",
			"ALLOW_APPEND_ORDER" => "Y",
			"SHOW_NOT_CALCULATED_DELIVERIES" => "L",
			"SHOW_VAT_PRICE" => "Y",
			"COMPATIBLE_MODE" => "Y",
			"USE_PRELOAD" => ($nonRegister == "Y" ? "N" : "Y"),
			"ALLOW_USER_PROFILES" => ($nonRegister == "Y" ? "N" : "Y"),
			"TEMPLATE_THEME" => "site",
			"SHOW_ORDER_BUTTON" => "final_step",
			"SHOW_TOTAL_ORDER_BUTTON" => "N",
			"SHOW_PAY_SYSTEM_LIST_NAMES" => "Y",
			"SHOW_PAY_SYSTEM_INFO_NAME" => "Y",
			"SHOW_DELIVERY_LIST_NAMES" => "Y",
			"SHOW_DELIVERY_INFO_NAME" => "Y",
			"SHOW_DELIVERY_PARENT_NAMES" => "Y",
			"SKIP_USELESS_BLOCK" => "Y",
			"BASKET_POSITION" => "after",
			"SHOW_BASKET_HEADERS" => "N",
			"DELIVERY_FADE_EXTRA_SERVICES" => "N",
			"SHOW_COUPONS_BASKET" => "Y",
			"SHOW_COUPONS_DELIVERY" => "N",
			"SHOW_COUPONS_PAY_SYSTEM" => "N",
			"SHOW_NEAREST_PICKUP" => "N",
			"DELIVERIES_PER_PAGE" => "9",
			"PAY_SYSTEMS_PER_PAGE" => "9",
			"PICKUPS_PER_PAGE" => "5",
			"SHOW_MAP_IN_PROPS" => "N",
			"PROPS_FADE_LIST_1" => array(
				0 => "1",
			),
			"PROPS_FADE_LIST_2" => "",
			"ACTION_VARIABLE" => "actionOrder",
			"PRODUCT_COLUMNS_VISIBLE" => array(
				0 => "PREVIEW_PICTURE",
				1 => "PROPS",
			),
			"ADDITIONAL_PICT_PROP_2" => "-",
			"ADDITIONAL_PICT_PROP_3" => "-",
			"ADDITIONAL_PICT_PROP_4" => "-",
			"ADDITIONAL_PICT_PROP_5" => "-",
			"ADDITIONAL_PICT_PROP_6" => "-",
			"BASKET_IMAGES_SCALING" => "adaptive",
			"SERVICES_IMAGES_SCALING" => "adaptive",
			"PRODUCT_COLUMNS_HIDDEN" => array(),
			"USE_YM_GOALS" => "N",
			"USE_ENHANCED_ECOMMERCE" => "N",
			"USE_CUSTOM_MAIN_MESSAGES" => "N",
			"USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
			"USE_CUSTOM_ERROR_MESSAGES" => "N",
			"USER_CONSENT" => "N",
			"USER_CONSENT_ID" => "0",
			"USER_CONSENT_IS_CHECKED" => "Y",
			"USER_CONSENT_IS_LOADED" => "N",
			"SHOW_PICKUP_MAP" => "Y",
			"CART_ORDER_NON_REGISTER" => $nonRegister
		),
		false
	);
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
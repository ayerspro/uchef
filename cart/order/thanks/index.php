<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Спасибо");

global $USER;

$orderID = 0;
$userID = $USER->GetID() ? $USER->GetID() : CSaleUser::GetAnonymousUserID();

if(isset($_GET["ORDER_ID"]) && is_numeric($_GET["ORDER_ID"])){
	$orderID = intval($_GET["ORDER_ID"]);	
}

$arOrder = CSaleOrder::GetList(array(),array('ID' => $orderID, 'USER_ID' => $userID),false,false,array())->Fetch();
$eMail = $USER->GetEmail();

$isAnonymousOrder = false;

if($arOrder){
	if($arOrder['USER_ID'] == CSaleUser::GetAnonymousUserID()){
		$isAnonymousOrder = true;
	}

	$db_vals = CSaleOrderPropsValue::GetOrderProps($orderID);
	while($arVals = $db_vals->Fetch())
	{
		if($arVals["IS_LOCATION"]=="Y"):
			$arLocs = CSaleLocation::GetByID($arVals["VALUE"], LANGUAGE_ID);
			$UserOrderProps .= $arVals["NAME"].': '.$arLocs["COUNTRY_NAME"]." - ".$arLocs["REGION_NAME"]." - ".$arLocs["CITY_NAME"]."\n";
		else:
			$UserOrderProps .= $arVals["NAME"].': '.$arVals["VALUE"]."\n";
		endif;
		
		if($arVals['CODE'] == 'EMAIL'){
			if($isAnonymousOrder || (strlen($arOrder["USER_EMAIL"]) == 0 && strlen($USER->GetEmail()) == 0)){
				$eMail = $arVals["VALUE"];
			}
		}
	}
}
?>

<div id="cart-order-thanks">
	<ul class="cart-order-steps">
		<li>Редактирование корзины &gt;</li>
		<li>Адрес, доставка и оплата &gt;</li>
		<li class="active">Спасибо!</li>
	</ul>
	
	<div class="row">
		<div class="col-md-4">
			<h3>3 Ваш заказ принят</h3>
		</div>
		<div class="col-md-4">
			<h4>Спасибо за заказ!</h4>
			<div class="cart-order-thanks-desc">
				<p><?=($arOrder ?"Ваш заказ <span class=\"bold\">№" . $orderID . "</span> принят":"Спасибо за покупку, оплата прошла успешно")?></p>
				<h5>Все детали мы отправили Вам на почту<br><span class="bold"><?=$eMail?></span></h5>
			</div>
			
			<p class="cl-grey">По всем вопросам, связанным с оформлением и получением заказа звоните по телефону 8-800-775-73-53</p>
			<?if($arOrder && $arOrder['PAY_SYSTEM_ID'] == 10):?>	
				<iframe id="cart-order-thanks-payments" src="/cart/order/payment/?ORDER_ID=<?=$orderID?>" frameBorder="0">
					Ваш браузер не поддерживает встроенные фреймы!
				</iframe>
			<?elseif($arOrder && $arOrder['PAY_SYSTEM_ID'] == 8):?>	
				<a href="/personal/order/payment/?ORDER_ID=<?=$orderID?>&PAYMENT_ID=<?=$orderID?>/" class="btn btn-default-chef" target="_blank">Оплатить</a>
			<?endif;?>
		</div>
	
	</div>

	
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ошибка");
?>

<div class="b-block">
	<div class="b-thank">
		<div>
			<h2><?=(isset($_GET["order"]))?"Ошибка создания заказа":"Оплата не произошла"?></h2>
			<h3><?=(isset($_GET["order"]))?"Попробуйте связаться с нами по телефону или <a href='#' class='fancy b-return-link' data-block='#b-popup-callback'>закажите обратный звонок</a>":"Наш менеджер свяжется с Вами в течение 30 минут для устранения возникшей проблемы"?></h3>
		</div>
	</div>
	<div class="title" style="padding-top: 20px;">
		<span></span>
		<h2>Продолжить покупки</h2>
	</div>
	<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "main", Array(
		"COMPONENT_TEMPLATE" => ".default",
			"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
			"IBLOCK_ID" => "2",	// Инфоблок
			"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
			"SECTION_CODE" => "",	// Код раздела
			"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
			"TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов
			"SECTION_FIELDS" => array(	// Поля разделов
				0 => "",
				1 => "",
			),
			"SECTION_USER_FIELDS" => array(	// Свойства разделов
				0 => "",
				1 => "",
			),
			"VIEW_MODE" => "LIST",	// Вид списка подразделов
			"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
			"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
			"CACHE_TYPE" => "A",	// Тип кеширования
			"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
			"CACHE_GROUPS" => "Y",	// Учитывать права доступа
			"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
		),
		false
	);?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
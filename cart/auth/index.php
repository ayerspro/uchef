<?if($_SERVER['REQUEST_METHOD'] == 'POST'){
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
	$arResult = array("SUCCESS" => "N", "ERROR" => array());
	
	if(isset($_POST['ACTION'])){
		switch($_POST['ACTION']){
			case "SET_REGISTER":
				
				if(isset($_POST['NON_REGISTER']) && !empty($_POST['NON_REGISTER'])){
					$APPLICATION->set_cookie("CART_ORDER_NON_REGISTER", $_POST['NON_REGISTER']);
					
					$application = \Bitrix\Main\Application::getInstance();
					$context = $application->getContext();
					$context->getResponse()->flush("");
					
					$arResult["SUCCESS"] = "Y";				
				}
				else{
					$arResult["ERROR"][] = "Значение регистрации пусто.";
				}

				break;
		}
	}
	
	echo json_encode($arResult);
	return;
}
else{	
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

	$APPLICATION->SetPageProperty("tags", "Авторизация");
	$APPLICATION->SetPageProperty("keywords_inner", "Авторизация");
	$APPLICATION->SetPageProperty("title", "Авторизация");
	$APPLICATION->SetPageProperty("keywords", "Авторизация");
	$APPLICATION->SetPageProperty("description", "Авторизация");
	$APPLICATION->SetTitle("Авторизация");
?>

	<div class="container cart-auth">
		<h1><?=$APPLICATION->Gettitle();?></h1>
		<div class="row">
			<div class="col-md-4">
				<div class="white-block-item">
					<h2>Вы у нас уже были</h2>
					<?$APPLICATION->IncludeComponent(
						"bitrix:system.auth.form",
						"cart_auth",
						Array(
							"FORGOT_PASSWORD_URL" => "",
							"PROFILE_URL" => "",
							"REGISTER_URL" => "",
							"SHOW_ERRORS" => "Y"
						)
					);?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="white-block-item">
					<h2>Вы у нас впервые</h2>
					<a href="#" class="register btn btn-default-chef brown btn-block">Зарегистрироваться</a>
					<br>
					<a href="#" class="order-non-register btn btn-default-chef btn-block">Купить без регистрирации</a>
					<div class="order-non-register-errors"></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="white-block-item">
					<?$APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket", 
						"cart_small_basket", 
						array(
							"ACTION_VARIABLE" => "action",
							"COLUMNS_LIST" => array(
								0 => "NAME",
								1 => "DELETE",
								2 => "DELAY",
								3 => "TYPE",
								4 => "PRICE",
								5 => "QUANTITY",
								6 => "SUM",
							),
							"COMPONENT_TEMPLATE" => "cart_small_basket",
							"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
							"HIDE_COUPON" => "N",
							"OFFERS_PROPS" => array(
								0 => "ARTNUMBER",
							),
							"PATH_TO_ORDER" => "/cart/order/",
							"PRICE_VAT_SHOW_VALUE" => "N",
							"QUANTITY_FLOAT" => "N",
							"SET_TITLE" => "Y",
							"USE_PREPAYMENT" => "N",
							"CORRECT_RATIO" => "N",
							"AUTO_CALCULATION" => "Y",
							"USE_GIFTS" => "Y",
							"GIFTS_PLACE" => "BOTTOM",
							"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
							"GIFTS_HIDE_BLOCK_TITLE" => "N",
							"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
							"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
							"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
							"GIFTS_SHOW_OLD_PRICE" => "N",
							"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
							"GIFTS_SHOW_NAME" => "Y",
							"GIFTS_SHOW_IMAGE" => "Y",
							"GIFTS_MESS_BTN_BUY" => "Выбрать",
							"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
							"GIFTS_PAGE_ELEMENT_COUNT" => "4",
							"GIFTS_CONVERT_CURRENCY" => "N",
							"GIFTS_HIDE_NOT_AVAILABLE" => "N",
							"TEMPLATE_THEME" => "blue",
							"USE_ENHANCED_ECOMMERCE" => "N"
						),
						false
					);?>
				</div>
			</div>
		</div>
	</div>

	<script>
		BX.ready(function(){
			$('.cart-auth .white-block-item').matchHeight();
			
			
			$('.order-non-register').click(function(e){
				e.preventDefault();
				
				var data = { "ACTION":"SET_REGISTER", "NON_REGISTER":"Y" };
				
				$.ajax({
					url: '/cart/auth/',
					type: 'POST',
					data: data,
					dataType: 'json',
					success: function(resp) {
						console.log(resp);
						if(resp.SUCCESS == "Y") {
							window.location.href = "/cart/order/";
						} else {
							$('.order-non-register-errors').empty();
							$.each(resp.ERROR, function(k, v){
								$('.order-non-register-errors').append('<p><font class="errortext">'+v+'</font></p>');
							});
						}
					},
					error: function(ex){
						console.log(ex);
					}
				});
				
				
			});
		});
	</script>
<?
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
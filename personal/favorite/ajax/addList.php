<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
	<?
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/colors.css", true);
	$APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap.css");
	
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.scrollbar.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/ion.rangeSlider.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/ion.rangeSlider.skinFlat.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/fonts/css/fontawesome-all.min.css");
	//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/new_year.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/colorbox.css");
	$APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");
	?>
	<!-- JS INCLUDES -->
	<?
	$APPLICATION->AddHeadScript('https://code.jquery.com/jquery-3.3.1.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jQuery.jscrollpane.js');
	$APPLICATION->AddHeadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
	$APPLICATION->AddHeadScript('//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.scrollbar.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.form.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.jgrowl.min.js');
	//$APPLICATION->AddHeadScript('https://code.jquery.com/jquery-3.2.1.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/ion.rangeSlider.min.js');
	?>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/jquery.jgrowl.css" media="screen" />

	<!-- /JS INCLUDES -->
	<?$APPLICATION->ShowHead();
	$APPLICATION->AddHeadScript();?>
	<?
	
	$IDS = array();

	//echo'<pre>';print_r($_SESSION["CHECKLIST_PRODUCT_MAIN"]);echo'</pre>';

	
	if(isset($_SESSION["CHECKLIST_PRODUCT_MAIN"])){ 
		$IDS = $_SESSION["CHECKLIST_PRODUCT_MAIN"];
	}
	
	?>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
	

	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
<div class="container-fluid">
<br/><br/><br/>
<?//echo'<pre>';print_r($_GET);echo'</pre>';?>
<div class="list-favorite">
	<div class="list-wrap">
		<?if(isset($_GET['list'])):?>
			<a class="txt-favor">
				Название списка:
			</a>
			<div class="in">
				<input type="text" class="input-l" value="<?=htmlspecialchars($_REQUEST['list'])?>">
				<input type="hidden" class="input-edit" value="<?=htmlspecialchars($_REQUEST['list'])?>">
				<a href="#" class="btn btn-default btn-block" id="createIframe">Добавить товар</a>
				<a href="#" class="btn btn-default btn-block" id="saveList">Сохранить</a>
			</div>
		<?elseif(!empty($IDS) OR $_GET["new"]=="Y"):?>
		<input type="hidden" id="controlNew" value="Y">
			<a class="txt-favor">
				Введите название списка:
			</a>
			<div class="in">
				<input type="text" class="input-l" value="<?=htmlspecialchars($_SESSION['CHECKLIST_PRODUCT_MAIN_NAME_LIST'])?>">
				<a href="#" class="btn btn-default btn-block" id="createIframe">Добавить товар</a>
			</div>
			<a href="#" id="createComplete" class="btn btn-default btn-block">Создать список</a>
		<?else:?>
			<a href="#" class="btn btn-default btn-block" id="createIframe">Добавить товар</a>
		<?endif;?>
	</div>
</div>
	<div class="titles-leader">В списке:</div>
	<!-- <div class="panel-pagination">
		<?$APPLICATION->ShowViewContent('pagination_section');?>
	</div> -->
</div>
<?if(isset($_REQUEST['show'])) $arParams["PAGE_ELEMENT_COUNT"] = $_REQUEST['show'];?>
<?$els = $APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"addList", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "round",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"PROPERTY_CODE" => array(
			0 => "SIZE",
			1 => "NEWPRODUCT",
			2 => "SALELEADER",
			3 => "SPECIALOFFER",
			4 => "",
		),
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_CODE_PATH" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SEF_RULE" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "green",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "favorite",
		"CUSTOM_FILTER" => "",
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PRODUCT_DISPLAY_MODE" => "N",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"MESS_BTN_COMPARE" => "Сравнить",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
<br/>
</body>
</html>
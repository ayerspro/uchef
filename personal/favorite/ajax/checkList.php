<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
if($_SERVER['REQUEST_METHOD'] == "POST") {
	global $APPLICATION;
	
	$arResult = array(
		'ERROR' => array(),
		'TYPE' => ( $_REQUEST['TYPES'] ? $_REQUEST['TYPES'] : '' ),
		'SUCCESS' => '',
	);
	

	if($_REQUEST['TYPES'] == 'ADDED' && !empty($_REQUEST['ID'][0])) {

		if($_REQUEST['edit'] != 'Y') {
			$arr = $_REQUEST['ID'][0];
			
			//echo'<pre>';print_r($_REQUEST);echo'</pre>';
			$_SESSION["CHECKLIST_PRODUCT_MAIN"] = $arr;
			$_SESSION["CHECKLIST_PRODUCT_MAIN_NAME_LIST"] = $_REQUEST['list'];
			
			$arResult['SUCCESS'] = 'ok';
		} else {
			if(isset($_REQUEST['list']) && !empty($_REQUEST['list'])) {		
				$arr = $_REQUEST['ID'][0];
				$IDUSER = $USER->GetID();
				$rsUser = CUser::GetByID($IDUSER);
				$arUser = $rsUser->Fetch();
				$list = unserialize($arUser['UF_LISTS']);	

				
				
				if(isset($list[$_REQUEST['list']])) {
					$list[$_REQUEST['list']] = $arr;
					$user = new CUser;
					if($user->Update($IDUSER, array('UF_LISTS' => serialize($list)))) $arResult['SUCCESS'] = 'ok';
					else $arResult[] = 'Произошла ошибка при записи в БД';
				}
			}
		}
	}
	if($_REQUEST['TYPES'] == 'DELETES' && !empty($_REQUEST['ID'])) {
		if(!isset($_REQUEST['edit'])) {
			if(isset($_SESSION["CHECKLIST_PRODUCT_MAIN"])){
				$data = $_SESSION["CHECKLIST_PRODUCT_MAIN"];
				if(in_array($_REQUEST['ID'], $data)) unset($data[$_REQUEST['ID']]);
				$APPLICATION->set_cookie("CHECKLIST_PRODUCT_MAIN", serialize($data));
				$arResult['ID'] = $_REQUEST['ID'];
			}
			else $arResult[] = 'Произошла ошибка при записи кука';
		} elseif(isset($_REQUEST['edit']) && isset($_REQUEST['list'])) {
			$IDUSER = $USER->GetID();
			$rsUser = CUser::GetByID($IDUSER);
			$arUser = $rsUser->Fetch();
			$list = unserialize($arUser['UF_LISTS']);
			if(isset($list[$_REQUEST['list']][$_REQUEST['ID']])) {
				unset($list[$_REQUEST['list']][$_REQUEST['ID']]);
				$user = new CUser;
				if($user->Update($IDUSER, array('UF_LISTS' => serialize($list)))) $arResult['ID'] = $_REQUEST['ID'];
				else $arResult[] = 'Произошла ошибка при записи';
			}
		}
	}
	
	if($_REQUEST['TYPES'] == 'COMPLETE' && !empty($_REQUEST['ID'][0]) && !empty($_REQUEST['NAME'])) {
		$name = trim($_REQUEST['NAME']);
		$arr = $_REQUEST['ID'][0];
		global $USER;
		$IDUSER = $USER->GetID();

		$rsUser = CUser::GetByID($IDUSER);
		$arUser = $rsUser->Fetch();

		if(!empty($arUser['UF_LISTS'])) {
			$arUSER = unserialize($arUser['UF_LISTS']);
			if(!isset($arUSER[$name])) {
				foreach ($arr as $key => $value) {
					$arUSER[$name][$key] = $value;
				}
			} else {
				foreach ($arr as $key => $value) {
					if(!in_array($value, $arUSER[$name])) {
						$arUSER[$name][$key] = $value;
					}
				}
			}
		} else {
			$arUSER = array();
			foreach ($arr as $key => $value) {
				$arUSER[$name][$key] = $value;
			}
		}

		$arFields = array(
			"UF_LISTS" => serialize($arUSER)
		);

		$obUser = new CUser;

		if(!$obUser->Update($IDUSER, $arFields)) {
			$arResult['ERROR'][] = $obUser->LAST_ERROR;
		} else {
			$arResult['COMPLETE'] = 'Y';
			unset($_SESSION);
		}
	}
	//кнопка сохранит
	if($_REQUEST['TYPES'] == 'DELETES' AND !empty($_REQUEST['NAMEEDIT']) AND !empty($_REQUEST['NAME'])) {
		$nameNew = trim($_REQUEST['NAME']);
		$nameOLD = trim($_REQUEST['NAMEEDIT']);
		$arr = $_REQUEST['ID'][0];
		global $USER;
		$IDUSER = $USER->GetID();

		$rsUser = CUser::GetByID($IDUSER);
		$arUser = $rsUser->Fetch();

		if(!empty($arUser['UF_LISTS'])) {
			$arUSER = unserialize($arUser['UF_LISTS']);
			foreach ($arUSER as $key => $value) {
				if($key==$nameOLD){
					$arrNewUser[$nameNew]=$arUSER[$key];
					$_SESSION['NEW_NAME_LIST']=$nameNew;
				}else{
					$arrNewUser[$key]=$arUSER[$key];
				}
			}
			$arUSER=$arrNewUser;
		} else {
			$arUSER = array();
			foreach ($arr as $key => $value) {
				$arUSER[$name][$key] = $value;
			}
		}
//echo'<pre>';print_r($arUSER);echo'</pre>';
		$arFields = array(
			"UF_LISTS" => serialize($arUSER)
		);

		$obUser = new CUser;

		if(!$obUser->Update($IDUSER, $arFields)) {
			$arResult['ERROR'][] = $obUser->LAST_ERROR;
		} else {
			$arResult['COMPLETE_NAMEEDIT'] = 'Y';
			$arResult['SUCCESS'] = 'OK';
			
			unset($_SESSION);
		}
	}
	echo json_encode($arResult);
}

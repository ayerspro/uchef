<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (CModule::IncludeModule("iblock")) {
	global $USER;

	$elId = intval($_REQUEST ["elid"]);

	if($USER->IsAuthorized()){
		$userId = $USER->GetID();
		if (isset($_REQUEST["recept"]) && $_REQUEST["recept"] == "1"){
			$iblock = 9;
		}
		else{
			$iblock = 5;
		}

		  $dbEl = CIBlockElement::GetList ( Array(), Array ("ID" => $elId, "IBLOCK_ID" => $iblock ) );

		  if ($obEl = $dbEl->GetNextElement() AND !empty($userId)) {
			  $props = $obEl->GetProperties();
			  $UserList = array_unique($props["F_USER"]["VALUE"]);
				  if(!in_array($userId, $UserList)) {
					  $UserList[] = $userId;
					  CIBlockElement::SetPropertyValueCode ($elId, "F_USER", $UserList);
					  echo "done";
					  $del="N";
				  }
				  else {
					  $key = array_search($userId, $UserList);
					  unset($UserList[$key]);
					  CIBlockElement::SetPropertyValueCode ($elId, "F_USER", $UserList);
					  echo "deleted";
					  $del="Y";
				  }
		  }
		  else {
			  echo "fail";
			  $del="N";
		  }
		  //удаляем у авторизованного пользователя товары из избранного
		  if(isset($_REQUEST['elid']) && !isset($_REQUEST['list'])&& $del=="Y") {
		  	//echo'<pre>';print_r($_REQUEST);echo'</pre>';
		  	$IDUSER = $USER->GetID();
		  	$rsUser = CUser::GetByID($IDUSER);
		  	$arUser = $rsUser->Fetch();
		  	$list = unserialize($arUser['UF_LISTS']);
		  	//echo'<pre>';print_r($list);echo'</pre>';
		  	foreach ($list as $key => $elList) {
		  		if(isset($elList[$_REQUEST['elid']])) {
		  			unset($elList[$_REQUEST['elid']]);
		  		}
		  		$newList[$key]=$elList;
		  		
		  	}
		  	//echo'<pre>';print_r($newList);echo'</pre>';
		  		unset($list[$_REQUEST['list']][$_REQUEST['elid']]);
		  		//echo'<pre>';print_r($list);echo'</pre>';
		  		$user = new CUser;
		  		if($user->Update($IDUSER, array('UF_LISTS' => serialize($newList)))){

		  		}else{ $arResult[] = 'Произошла ошибка при записи';}
		  }

		  if(isset($_REQUEST['elid']) && isset($_REQUEST['list'])) {
		  	//echo'<pre>';print_r($_REQUEST);echo'</pre>';
		  	$IDUSER = $USER->GetID();
		  	$rsUser = CUser::GetByID($IDUSER);
		  	$arUser = $rsUser->Fetch();
		  	$list = unserialize($arUser['UF_LISTS']);
		  	if(isset($list[$_REQUEST['list']][$_REQUEST['elid']])) {
		  		unset($list[$_REQUEST['list']][$_REQUEST['elid']]);
		  		//echo'<pre>';print_r($list);echo'</pre>';
		  		$user = new CUser;
		  		if($user->Update($IDUSER, array('UF_LISTS' => serialize($list)))){

		  		}else{ $arResult[] = 'Произошла ошибка при записи';}
		  	}
		  }
	}
	else{
		
		echo "fail";
		die();
		
		$cookieType = "ANONIM_PROD_FAVORITES";
		if (isset($_REQUEST["recept"]) && $_REQUEST["recept"] == "1"){
			$cookieType = "ANONIM_RECEPT_FAVORITES";
		}

		$arList = unserialize($APPLICATION->get_cookie($cookieType));

		if(empty($arList)){
			$arList = array();
		}
		$add = false;

		if(!in_array($elId, $arList)){
			$arList[] = $elId;
			$add = true;
		}
		else{
			$key = array_search($elId, $arList);
			unset($arList[$key]);
		}
		$APPLICATION->set_cookie($cookieType, serialize($arList), time()+60*60*24*30*12*2);
		echo $add ? "done" : "deleted";
	}
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>
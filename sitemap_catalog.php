<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header("Content-Type: text/xml");
header("Pragma: no-cache");

echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

<?

global $uchefSettings;

Bitrix\Main\Loader::includeModule("sotbit.regions");
Bitrix\Main\Loader::includeModule("iblock");

$domain = new \Sotbit\Regions\Location\Domain();
$domainCode = $domain->getProp("CODE");
$regionID = $domain->getProp("ID");

// SECTIONS //
$arSections = array();

$arFilter = array('IBLOCK_ID' => $uchefSettings['CATALOG_IBLOCK'], 'ACTIVE' => 'Y');
$rsSect = CIBlockSection::GetList(array('SORT' => 'asc'), $arFilter);

while($arSect = $rsSect->GetNext()) {
	
	if(showSectionByRegion($arSect['ID'])) {
		
		$arSections[$arSect['ID']] = $arSect;
	}
}

// PRODUCTS //
$arProducts = array();

$arSelect = array("ID", "NAME", "DETAIL_PAGE_URL");
$arFilter = array(
	"IBLOCK_ID" => $uchefSettings['CATALOG_IBLOCK'], 
	"ACTIVE" => "Y",
	"SECTION_ID" => array_keys($arSections),
	array(
		"LOGIC" => "OR",
		array(
			array(
				"LOGIC" => "OR",
				"PROPERTY_REGION" => false,
			),
		),
		array(
			array(
				"LOGIC" => "OR",
				"PROPERTY_REGION" => $regionID,
			),
		),
	)
);

$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

while($ob = $res->GetNextElement()) {
	
	$arFields = $ob->GetFields();
	$arProducts[$arFields['ID']] = $arFields;
}

foreach($arSections as $sectiom) {
	
	?><url><loc><?=$domainCode;?><?=$sectiom['SECTION_PAGE_URL'];?></loc></url><?
}

foreach($arProducts as $product) {
	
	?><url><loc><?=$domainCode;?><?=$product['DETAIL_PAGE_URL'];?></loc></url><?
}
?>

</urlset>